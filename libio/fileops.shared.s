	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _IO_new_do_write,_IO_do_write@@GLIBC_2.2.5
	.symver _IO_new_file_attach,_IO_file_attach@@GLIBC_2.2.5
	.symver _IO_new_file_close_it,_IO_file_close_it@@GLIBC_2.2.5
	.symver _IO_new_file_finish,_IO_file_finish@@GLIBC_2.2.5
	.symver _IO_new_file_fopen,_IO_file_fopen@@GLIBC_2.2.5
	.symver _IO_new_file_init,_IO_file_init@@GLIBC_2.2.5
	.symver _IO_new_file_setbuf,_IO_file_setbuf@@GLIBC_2.2.5
	.symver _IO_new_file_sync,_IO_file_sync@@GLIBC_2.2.5
	.symver _IO_new_file_overflow,_IO_file_overflow@@GLIBC_2.2.5
	.symver _IO_new_file_seekoff,_IO_file_seekoff@@GLIBC_2.2.5
	.symver _IO_new_file_underflow,_IO_file_underflow@@GLIBC_2.2.5
	.symver _IO_new_file_write,_IO_file_write@@GLIBC_2.2.5
	.symver _IO_new_file_xsputn,_IO_file_xsputn@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_IO_new_file_sync
	.type	_IO_new_file_sync, @function
_IO_new_file_sync:
.LFB102:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rsi, %rdx
	jbe	.L7
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L18
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	__GI__IO_wdo_write
	testl	%eax, %eax
	setne	%al
.L6:
	testb	%al, %al
	jne	.L12
.L7:
	movq	8(%rbx), %rsi
	subq	16(%rbx), %rsi
	je	.L9
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L19
.L10:
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L11
	movq	8(%rbx), %rax
	movq	%rax, 16(%rbx)
.L9:
	movq	$-1, 144(%rbx)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	testl	%eax, %eax
	setne	%al
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L9
.L12:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rsi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rsi
	jmp	.L10
.LFE102:
	.size	_IO_new_file_sync, .-_IO_new_file_sync
	.globl	__GI__IO_file_sync
	.set	__GI__IO_file_sync,_IO_new_file_sync
	.p2align 4,,15
	.type	_IO_file_seekoff_maybe_mmap, @function
_IO_file_seekoff_maybe_mmap:
.LFB107:
	pushq	%rbp
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rcx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	216(%rdi), %rbp
	subq	%rcx, %rax
	movq	%rbp, %rdi
	subq	%rcx, %rdi
	cmpq	%rdi, %rax
	jbe	.L25
.L21:
	movq	%rbx, %rdi
	call	*128(%rbp)
	testq	%rax, %rax
	js	.L23
	movq	%rax, 144(%rbx)
.L20:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%edx, 12(%rsp)
	movq	%rsi, (%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %edx
	movq	(%rsp), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	movq	$-1, %rax
	jmp	.L20
.LFE107:
	.size	_IO_file_seekoff_maybe_mmap, .-_IO_file_seekoff_maybe_mmap
	.p2align 4,,15
	.globl	__GI__IO_file_close
	.hidden	__GI__IO_file_close
	.type	__GI__IO_file_close, @function
__GI__IO_file_close:
.LFB112:
	movl	112(%rdi), %edi
	jmp	__GI___close_nocancel
.LFE112:
	.size	__GI__IO_file_close, .-__GI__IO_file_close
	.globl	_IO_file_close
	.set	_IO_file_close,__GI__IO_file_close
	.p2align 4,,15
	.globl	_IO_new_file_setbuf
	.type	_IO_new_file_setbuf, @function
_IO_new_file_setbuf:
.LFB92:
	pushq	%rbx
	movq	%rdi, %rbx
	call	_IO_default_setbuf@PLT
	testq	%rax, %rax
	je	.L27
	movq	56(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
.L27:
	popq	%rbx
	ret
.LFE92:
	.size	_IO_new_file_setbuf, .-_IO_new_file_setbuf
	.globl	__GI__IO_file_setbuf
	.set	__GI__IO_file_setbuf,_IO_new_file_setbuf
	.p2align 4,,15
	.globl	_IO_file_setbuf_mmap
	.type	_IO_file_setbuf_mmap, @function
_IO_file_setbuf_mmap:
.LFB93:
	leaq	__GI__IO_file_jumps(%rip), %rax
	pushq	%rbx
	leaq	__GI__IO_wfile_jumps(%rip), %rcx
	movq	%rdi, %rbx
	movq	%rax, 216(%rdi)
	movq	160(%rdi), %rax
	movq	%rcx, 224(%rax)
	call	_IO_new_file_setbuf@PLT
	testq	%rax, %rax
	je	.L36
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	160(%rbx), %rdx
	leaq	_IO_file_jumps_mmap(%rip), %rcx
	leaq	_IO_wfile_jumps_mmap(%rip), %rdi
	movq	%rcx, 216(%rbx)
	movq	%rdi, 224(%rdx)
	popq	%rbx
	ret
.LFE93:
	.size	_IO_file_setbuf_mmap, .-_IO_file_setbuf_mmap
	.p2align 4,,15
	.type	new_do_write, @function
new_do_write:
.LFB95:
	pushq	%r15
	pushq	%r14
	leaq	__start___libc_IO_vtables(%rip), %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	__stop___libc_IO_vtables(%rip), %rbp
	movq	%rdi, %rbx
	movq	%rdx, %r15
	subq	$8, %rsp
	movq	216(%rdi), %r12
	subq	%r14, %rbp
	movq	%r12, %rax
	subq	%r14, %rax
	testl	$4096, (%rdi)
	je	.L38
	movq	$-1, 144(%rdi)
.L39:
	cmpq	%rax, %rbp
	jbe	.L55
.L42:
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	*120(%r12)
	testq	%rax, %rax
	movq	%rax, %r12
	movzwl	128(%rbx), %edi
	je	.L43
	testw	%di, %di
	jne	.L56
.L43:
	movl	192(%rbx), %edx
	movq	56(%rbx), %rax
	testl	%edx, %edx
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	jle	.L57
.L44:
	movq	64(%rbx), %rax
.L45:
	movq	%rax, 48(%rbx)
.L37:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	16(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L39
	cmpq	%rax, %rbp
	jbe	.L58
.L40:
	subq	%rdx, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	call	*128(%r12)
	xorl	%r12d, %r12d
	cmpq	$-1, %rax
	je	.L37
	movq	216(%rbx), %r12
	movq	%rax, 144(%rbx)
	movq	%r12, %rax
	subq	%r14, %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L57:
	testl	$514, (%rbx)
	jne	.L45
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L56:
	subl	$1, %edi
	movl	%eax, %edx
	movq	%r13, %rsi
	call	__GI__IO_adjust_column
	addl	$1, %eax
	movw	%ax, 128(%rbx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	call	_IO_vtable_check
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L58:
	call	_IO_vtable_check
	movq	32(%rbx), %rsi
	movq	16(%rbx), %rdx
	jmp	.L40
.LFE95:
	.size	new_do_write, .-new_do_write
	.p2align 4,,15
	.globl	_IO_file_close_mmap
	.type	_IO_file_close_mmap, @function
_IO_file_close_mmap:
.LFB111:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	movq	64(%rbx), %rsi
	subq	%rdi, %rsi
	call	__GI___munmap
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movl	112(%rbx), %edi
	popq	%rbx
	jmp	__GI___close_nocancel
.LFE111:
	.size	_IO_file_close_mmap, .-_IO_file_close_mmap
	.p2align 4,,15
	.type	mmap_remap_check, @function
mmap_remap_check:
.LFB97:
	pushq	%r12
	pushq	%rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	pushq	%rbx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	216(%rdi), %rbp
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L79
.L62:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	call	*144(%rbp)
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	movl	%eax, %ebp
	subq	%rdi, %rsi
	testl	%eax, %eax
	jne	.L64
	movl	40(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L80
.L64:
	call	__GI___munmap
.L69:
	movl	192(%rbx), %eax
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	testl	%eax, %eax
	jle	.L81
	leaq	__GI__IO_wfile_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
.L74:
	movq	160(%rbx), %rdx
	movl	$1, %ebp
	movq	%rax, 224(%rdx)
.L61:
	addq	$160, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movq	64(%rsp), %r12
	testq	%r12, %r12
	je	.L64
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__GI___getpagesize
	movq	8(%rsp), %rsi
	cltq
	movq	(%rsp), %rdi
	movq	%rax, %rcx
	leaq	-1(%r12,%rax), %rdx
	negq	%rcx
	leaq	-1(%rax,%rsi), %rsi
	andq	%rcx, %rdx
	andq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jb	.L82
	ja	.L83
	addq	%rdi, %r12
	movq	%r12, 64(%rbx)
.L66:
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	144(%rbx), %rcx
	subq	%rdi, %rsi
	movq	%rdi, 24(%rbx)
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, 144(%rbx)
	jge	.L70
	addq	%rcx, %rdi
	xorl	%edx, %edx
	movq	%r12, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movl	112(%rbx), %edi
	call	__lseek64
	movq	64(%rbx), %rdx
	subq	56(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L71
	orl	$32, (%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	__GI__IO_file_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	__GI__IO_wfile_jumps(%rip), %rax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L79:
	call	_IO_vtable_check
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, 8(%rbx)
	movq	%r12, 16(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rdx, 144(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L82:
	addq	%rdx, %rdi
	subq	%rdx, %rsi
	call	__GI___munmap
	movq	56(%rbx), %rdi
	movq	64(%rsp), %r12
	addq	%rdi, %r12
	movq	%r12, 64(%rbx)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%eax, %eax
	movl	$1, %ecx
	call	__GI___mremap
	cmpq	$-1, %rax
	movq	%rax, %rdi
	je	.L84
	movq	64(%rsp), %r12
	movq	%rax, 56(%rbx)
	addq	%rax, %r12
	movq	%r12, 64(%rbx)
	jmp	.L66
.L84:
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	subq	%rdi, %rsi
	call	__GI___munmap
	jmp	.L69
.LFE97:
	.size	mmap_remap_check, .-mmap_remap_check
	.p2align 4,,15
	.globl	__GI__IO_file_seek
	.hidden	__GI__IO_file_seek
	.type	__GI__IO_file_seek, @function
__GI__IO_file_seek:
.LFB109:
	movl	112(%rdi), %edi
	jmp	__lseek64
.LFE109:
	.size	__GI__IO_file_seek, .-__GI__IO_file_seek
	.globl	_IO_file_seek
	.set	_IO_file_seek,__GI__IO_file_seek
	.p2align 4,,15
	.type	_IO_file_sync_mmap, @function
_IO_file_sync_mmap:
.LFB103:
	pushq	%rbx
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%rdx, %rax
	subq	56(%rdi), %rax
	cmpq	16(%rdi), %rdx
	je	.L87
	movl	112(%rdi), %edi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	__lseek64
	movq	8(%rbx), %rdx
	subq	56(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L90
.L87:
	movq	%rax, 144(%rbx)
	movq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	orl	$32, (%rbx)
	movl	$-1, %eax
	popq	%rbx
	ret
.LFE103:
	.size	_IO_file_sync_mmap, .-_IO_file_sync_mmap
	.p2align 4,,15
	.type	decide_maybe_mmap, @function
decide_maybe_mmap:
.LFB99:
	pushq	%rbp
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	216(%rdi), %rbp
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L115
.L92:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	call	*144(%rbp)
	testl	%eax, %eax
	jne	.L96
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L116
.L96:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L117
	leaq	__GI__IO_wfile_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
.L102:
	movq	160(%rbx), %rdx
	movq	%rax, 224(%rdx)
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	__GI__IO_file_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	__GI__IO_wfile_jumps(%rip), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L116:
	movq	48(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L96
	movq	144(%rbx), %rax
	cmpq	%rax, %rsi
	jge	.L104
	cmpq	$-1, %rax
	jne	.L96
.L104:
	movl	112(%rbx), %r8d
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$1, %ecx
	movl	$1, %edx
	call	__GI___mmap64
	cmpq	$-1, %rax
	movq	%rax, %rbp
	je	.L96
	movq	48(%rsp), %rsi
	movl	112(%rbx), %edi
	xorl	%edx, %edx
	call	__lseek64
	movq	48(%rsp), %rsi
	cmpq	%rax, %rsi
	je	.L95
	movq	%rbp, %rdi
	call	__GI___munmap
	movq	$-1, 144(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L115:
	call	_IO_vtable_check
	jmp	.L92
.L95:
	leaq	0(%rbp,%rsi), %rdx
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI__IO_setb
	movq	144(%rbx), %rdx
	movq	%rbp, 24(%rbx)
	leaq	0(%rbp,%rdx), %rax
	cmpq	$-1, %rdx
	movl	192(%rbx), %edx
	cmove	%rbp, %rax
	movq	%rax, 8(%rbx)
	movq	48(%rsp), %rax
	addq	%rax, %rbp
	testl	%edx, %edx
	movq	%rax, 144(%rbx)
	movq	%rbp, 16(%rbx)
	jle	.L118
	leaq	_IO_wfile_jumps_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
	jmp	.L102
.L118:
	leaq	_IO_file_jumps_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	_IO_wfile_jumps_mmap(%rip), %rax
	jmp	.L102
.LFE99:
	.size	decide_maybe_mmap, .-decide_maybe_mmap
	.p2align 4,,15
	.globl	_IO_file_underflow_maybe_mmap
	.type	_IO_file_underflow_maybe_mmap, @function
_IO_file_underflow_maybe_mmap:
.LFB100:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	decide_maybe_mmap
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L122
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L122:
	call	_IO_vtable_check
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
.LFE100:
	.size	_IO_file_underflow_maybe_mmap, .-_IO_file_underflow_maybe_mmap
	.p2align 4,,15
	.type	_IO_file_xsgetn_maybe_mmap, @function
_IO_file_xsgetn_maybe_mmap:
.LFB117:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	subq	$8, %rsp
	call	decide_maybe_mmap
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rcx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rcx, %rax
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	jbe	.L126
.L124:
	movq	64(%rbp), %rax
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L126:
	call	_IO_vtable_check
	jmp	.L124
.LFE117:
	.size	_IO_file_xsgetn_maybe_mmap, .-_IO_file_xsgetn_maybe_mmap
	.p2align 4,,15
	.globl	_IO_new_file_seekoff
	.type	_IO_new_file_seekoff, @function
_IO_new_file_seekoff:
.LFB105:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$184, %rsp
	testl	%ecx, %ecx
	je	.L196
	movl	%edx, %r13d
	movq	32(%rdi), %rax
	movq	40(%rdi), %rdx
	movq	16(%rdi), %rdi
	cmpq	%rdi, 24(%rbx)
	movq	%rsi, %rbp
	je	.L197
.L142:
	xorl	%r15d, %r15d
	cmpq	%rax, %rdx
	jbe	.L143
.L144:
	movq	%rbx, %rdi
	call	__GI__IO_switch_to_get_mode
	testl	%eax, %eax
	jne	.L194
	cmpq	$0, 56(%rbx)
	je	.L198
.L145:
	cmpl	$1, %r13d
	je	.L148
.L202:
	cmpl	$2, %r13d
	jne	.L147
	movq	216(%rbx), %rax
	leaq	__start___libc_IO_vtables(%rip), %r14
	leaq	__stop___libc_IO_vtables(%rip), %r12
	subq	%r14, %r12
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	%rdx, %r12
	jbe	.L199
.L152:
	leaq	32(%rsp), %rsi
	movq	%rbx, %rdi
	call	*144(%rax)
	testl	%eax, %eax
	je	.L200
.L154:
	movq	%rbx, %rdi
	call	__GI__IO_unsave_markers
	movq	216(%rbx), %r15
	movq	%r15, %rax
	subq	%r14, %rax
	cmpq	%r12, %rax
	jnb	.L201
.L164:
	movl	%r13d, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*128(%r15)
	cmpq	$-1, %rax
	je	.L127
	movq	56(%rbx), %rdx
	andl	$-17, (%rbx)
	movq	%rax, 144(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 48(%rbx)
.L127:
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	cmpq	%rax, %rdx
	jne	.L142
	movl	$1, %r15d
	.p2align 4,,10
	.p2align 3
.L143:
	testl	$2048, (%rbx)
	jne	.L144
	cmpq	$0, 56(%rbx)
	jne	.L145
.L198:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	free@PLT
	andl	$-257, (%rbx)
.L146:
	movq	%rbx, %rdi
	call	__GI__IO_doallocbuf
	movq	56(%rbx), %rax
	cmpl	$1, %r13d
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	jne	.L202
.L148:
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	subq	%rax, %rbp
	movq	144(%rbx), %rax
	cmpq	$-1, %rax
	je	.L150
	addq	%rax, %rbp
	js	.L151
.L193:
	xorl	%r13d, %r13d
.L147:
	movq	%rbx, %rdi
	call	__GI__IO_free_backup_area
	movq	144(%rbx), %rsi
	movl	(%rbx), %eax
	cmpq	$-1, %rsi
	je	.L156
	cmpq	$0, 24(%rbx)
	je	.L156
	testb	$1, %ah
	jne	.L156
	movq	56(%rbx), %rdx
	movq	%rdx, %rcx
	subq	16(%rbx), %rcx
	addq	%rsi, %rcx
	cmpq	%rcx, %rbp
	jl	.L156
	cmpq	%rbp, %rsi
	jg	.L203
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	__start___libc_IO_vtables(%rip), %rcx
	leaq	__stop___libc_IO_vtables(%rip), %r12
	subq	%rcx, %r12
	testb	$4, %al
	movq	%rcx, %r14
	jne	.L154
	movq	56(%rbx), %rdx
	movq	64(%rbx), %rax
	movq	%rbp, %r13
	movq	%rdx, %rsi
	subq	%rax, %rsi
	subq	%rdx, %rax
	andq	%rbp, %rsi
	subq	%rsi, %r13
	cmpq	%r13, %rax
	jge	.L158
	movq	%rbp, %rsi
	xorl	%r13d, %r13d
.L158:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%r12, %rdx
	jnb	.L204
.L159:
	xorl	%edx, %edx
	movq	%rcx, 8(%rsp)
	movq	%rbx, %rdi
	call	*128(%rax)
	testq	%rax, %rax
	movq	%rax, %r8
	js	.L194
	testq	%r13, %r13
	je	.L170
	movq	216(%rbx), %rax
	movq	8(%rsp), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%r12, %rdx
	jnb	.L205
.L161:
	testl	%r15d, %r15d
	movq	112(%rax), %rax
	movq	56(%rbx), %rsi
	movq	%r13, %rdx
	jne	.L162
	movq	64(%rbx), %rdx
	subq	%rsi, %rdx
.L162:
	movq	%r8, 8(%rsp)
	movq	%rbx, %rdi
	call	*%rax
	cmpq	%rax, %r13
	jle	.L206
	cmpq	$-1, %rax
	movq	%r13, %rbp
	movl	$1, %r13d
	je	.L154
	subq	%rax, %rbp
	movl	$1, %r13d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L196:
	cmpq	$0, 56(%rdi)
	je	.L207
	movq	40(%rdi), %r13
	movq	32(%rdi), %rbp
	movl	(%rdi), %r12d
	andl	$4096, %r12d
	cmpq	%rbp, %r13
	jbe	.L173
	testl	%r12d, %r12d
	je	.L173
	movq	216(%rdi), %r14
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L208
.L134:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	*128(%r14)
	cmpq	$-1, %rax
	je	.L194
	cmpq	%rbp, %r13
	movq	%rax, 144(%rbx)
	ja	.L137
.L210:
	movq	8(%rbx), %rbp
	subq	16(%rbx), %rbp
.L130:
	cmpq	$-1, %rax
	je	.L209
.L139:
	addq	%rbp, %rax
	jns	.L127
.L151:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L194:
	movq	$-1, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L200:
	movl	56(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L154
	addq	80(%rsp), %rbp
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	__start___libc_IO_vtables(%rip), %r14
	leaq	__stop___libc_IO_vtables(%rip), %r12
	subq	%r14, %r12
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L173:
	cmpq	%rbp, %r13
	movq	144(%rbx), %rax
	jbe	.L210
.L137:
	testl	%r12d, %r12d
	movq	40(%rbx), %rbp
	je	.L138
	subq	32(%rbx), %rbp
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L209:
	movq	216(%rbx), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r12, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L211
.L140:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*128(%r12)
	cmpq	$-1, %rax
	je	.L194
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L170:
	xorl	%eax, %eax
.L160:
	movq	56(%rbx), %rdx
	andl	$-17, (%rbx)
	movq	%r8, 144(%rbx)
	addq	%rdx, %r13
	addq	%rdx, %rax
	movq	%rdx, 24(%rbx)
	movq	%r13, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 48(%rbx)
.L195:
	movq	%rbp, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L207:
	movq	144(%rdi), %rax
	xorl	%ebp, %ebp
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L201:
	call	_IO_vtable_check
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L138:
	subq	16(%rbx), %rbp
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%rbp, %rdi
	andl	$-17, %eax
	movq	%rdx, 24(%rbx)
	subq	%rcx, %rdi
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdi, %rcx
	movq	%rdx, 48(%rbx)
	movl	%eax, (%rbx)
	addq	%rdx, %rcx
	testq	%rsi, %rsi
	movq	%rcx, 8(%rbx)
	js	.L195
	movq	216(%rbx), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r12, %rdi
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L212
.L165:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	*128(%r12)
	movq	%rbp, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rcx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rsi
	movq	8(%rsp), %rax
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L206:
	movq	8(%rsp), %r8
	addq	%rax, %r8
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r8, 16(%rsp)
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %r8
	movq	8(%rsp), %rax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L208:
	call	_IO_vtable_check
	jmp	.L134
.L211:
	call	_IO_vtable_check
	jmp	.L140
.L212:
	call	_IO_vtable_check
	movq	144(%rbx), %rsi
	jmp	.L165
.LFE105:
	.size	_IO_new_file_seekoff, .-_IO_new_file_seekoff
	.globl	__GI__IO_file_seekoff
	.set	__GI__IO_file_seekoff,_IO_new_file_seekoff
	.p2align 4,,15
	.globl	__GI__IO_file_stat
	.hidden	__GI__IO_file_stat
	.type	__GI__IO_file_stat, @function
__GI__IO_file_stat:
.LFB110:
	movl	112(%rdi), %edi
	jmp	__GI___fstat64
.LFE110:
	.size	__GI__IO_file_stat, .-__GI__IO_file_stat
	.globl	_IO_file_stat
	.set	_IO_file_stat,__GI__IO_file_stat
	.p2align 4,,15
	.globl	_IO_new_file_write
	.type	_IO_new_file_write, @function
_IO_new_file_write:
.LFB113:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	jle	.L221
	movq	%rsi, %rbp
	movq	%rdx, %r13
	movq	%rdx, %rbx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L216:
	call	__GI___write
	testq	%rax, %rax
	js	.L224
.L218:
	subq	%rax, %rbx
	addq	%rax, %rbp
	testq	%rbx, %rbx
	jle	.L225
.L219:
	testb	$2, 116(%r12)
	movl	112(%r12), %edi
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	je	.L216
	call	__GI___write_nocancel
	testq	%rax, %rax
	jns	.L218
	.p2align 4,,10
	.p2align 3
.L224:
	orl	$32, (%r12)
	movq	%r13, %rax
	subq	%rbx, %rax
.L215:
	movq	144(%r12), %rdx
	testq	%rdx, %rdx
	js	.L214
	addq	%rax, %rdx
	movq	%rdx, 144(%r12)
.L214:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r13, %rax
	subq	%rbx, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%eax, %eax
	jmp	.L215
.LFE113:
	.size	_IO_new_file_write, .-_IO_new_file_write
	.p2align 4,,15
	.type	_IO_file_xsgetn_mmap, @function
_IO_file_xsgetn_mmap:
.LFB116:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbp
	subq	$24, %rsp
	movq	8(%rdi), %r13
	movq	16(%rdi), %rbx
	subq	%r13, %rbx
	cmpq	%rdx, %rbx
	jnb	.L227
	testl	$256, (%rdi)
	movq	%rsi, %rdx
	jne	.L242
.L228:
	movq	%r12, %rdi
	movq	%rdx, (%rsp)
	call	mmap_remap_check
	testl	%eax, %eax
	movq	(%rsp), %rdx
	jne	.L243
	movq	8(%r12), %r13
	movq	16(%r12), %rbx
	subq	%r13, %rbx
	cmpq	%rbx, %r14
	jbe	.L236
	orl	$16, (%r12)
	movq	%rdx, %rbp
.L227:
	testq	%rbx, %rbx
	jne	.L229
.L233:
	movq	%rbp, %rax
	subq	%r15, %rax
.L226:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__GI_mempcpy@PLT
	movq	%r12, %rdi
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	subq	%rbx, %r14
	call	_IO_switch_to_main_get_area@PLT
	movq	8(%r12), %r13
	movq	16(%r12), %rbx
	movq	(%rsp), %rdx
	movq	8(%rsp), %rax
	subq	%r13, %rbx
	cmpq	%rbx, %r14
	ja	.L228
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L229:
	cmpq	%r14, %rbx
	movq	%rbp, %rdi
	movq	%r13, %rsi
	cmova	%r14, %rbx
	movq	%rbx, %rdx
	addq	%r13, %rbx
	call	__GI_mempcpy@PLT
	movq	%rbx, 8(%r12)
	movq	%rax, %rbp
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L243:
	movq	216(%r12), %r13
	subq	%rbp, %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdx, %rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	movq	%r13, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L244
.L231:
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	*64(%r13)
	addq	%rbx, %rax
	jmp	.L226
.L244:
	call	_IO_vtable_check
	jmp	.L231
.L236:
	movq	%rdx, %rbp
	jmp	.L227
.LFE116:
	.size	_IO_file_xsgetn_mmap, .-_IO_file_xsgetn_mmap
	.p2align 4,,15
	.globl	__GI__IO_file_xsgetn
	.hidden	__GI__IO_file_xsgetn
	.type	__GI__IO_file_xsgetn, @function
__GI__IO_file_xsgetn:
.LFB115:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 56(%rdi)
	je	.L277
.L246:
	testq	%r13, %r13
	je	.L245
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rbp
	movq	%r13, %r15
	subq	%rsi, %rbp
	cmpq	%rbp, %r13
	jbe	.L250
	leaq	__stop___libc_IO_vtables(%rip), %r14
	leaq	__start___libc_IO_vtables(%rip), %rax
	subq	%rax, %r14
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L252:
	testl	$256, (%rbx)
	jne	.L278
.L253:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	je	.L255
	movq	64(%rbx), %rsi
	subq	%rax, %rsi
	cmpq	%r15, %rsi
	ja	.L279
	cmpq	$127, %rsi
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 48(%rbx)
	jbe	.L276
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	%r15, %rax
	subq	%rdx, %rax
	movq	%rax, %rdx
.L263:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	%rbp, %rax
	subq	%rcx, %rax
	cmpq	%rax, %r14
	jbe	.L280
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*112(%rbp)
	testq	%rax, %rax
	jle	.L281
.L258:
	movq	144(%rbx), %rdx
	addq	%rax, %r12
	subq	%rax, %r15
	cmpq	$-1, %rdx
	je	.L254
	addq	%rdx, %rax
	movq	%rax, 144(%rbx)
.L254:
	testq	%r15, %r15
	je	.L245
.L260:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rbp
	subq	%rsi, %rbp
	cmpq	%rbp, %r15
	jbe	.L250
.L251:
	testq	%rbp, %rbp
	je	.L252
	movq	%r12, %rdi
	movq	%rbp, %rdx
	subq	%rbp, %r15
	call	__GI_mempcpy@PLT
	addq	%rbp, 8(%rbx)
	testl	$256, (%rbx)
	movq	%rax, %r12
	je	.L253
.L278:
	movq	%rbx, %rdi
	call	_IO_switch_to_main_get_area@PLT
	testq	%r15, %r15
	jne	.L260
.L245:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
.L276:
	movq	%r15, %rdx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rbx, %rdi
	call	__GI___underflow
	cmpl	$-1, %eax
	jne	.L254
	subq	%r15, %r13
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%rdx, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*112(%rbp)
	testq	%rax, %rax
	jg	.L258
	.p2align 4,,10
	.p2align 3
.L281:
	movl	(%rbx), %edx
	subq	%r15, %r13
	movl	%edx, %ecx
	orl	$16, %edx
	orl	$32, %ecx
	testq	%rax, %rax
	cmovne	%ecx, %edx
	movl	%edx, (%rbx)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L277:
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L247
	call	free@PLT
	andl	$-257, (%rbx)
.L247:
	movq	%rbx, %rdi
	call	__GI__IO_doallocbuf
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	__GI_memcpy@PLT
	addq	%r15, 8(%rbx)
	jmp	.L245
.LFE115:
	.size	__GI__IO_file_xsgetn, .-__GI__IO_file_xsgetn
	.globl	_IO_file_xsgetn
	.set	_IO_file_xsgetn,__GI__IO_file_xsgetn
	.p2align 4,,15
	.globl	_IO_file_seekoff_mmap
	.type	_IO_file_seekoff_mmap, @function
_IO_file_seekoff_mmap:
.LFB106:
	testl	%ecx, %ecx
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L296
	cmpl	$1, %edx
	je	.L286
	cmpl	$2, %edx
	je	.L287
	movq	%rsi, %rbp
.L285:
	testq	%rbp, %rbp
	js	.L297
	movq	216(%rdi), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	%rdx, %rax
	movq	%r12, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L298
.L289:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*128(%r12)
	testq	%rax, %rax
	js	.L293
	movq	64(%rbx), %rcx
	movq	56(%rbx), %rdx
	movq	%rcx, %rsi
	movq	%rdx, 24(%rbx)
	subq	%rdx, %rsi
	cmpq	%rsi, %rbp
	jle	.L290
	movq	%rcx, 8(%rbx)
	movq	%rcx, 16(%rbx)
.L291:
	andl	$-17, (%rbx)
	movq	%rax, 144(%rbx)
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	movq	144(%rdi), %rbp
	subq	%rax, %rbp
.L282:
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	movq	64(%rdi), %rbp
	subq	56(%rdi), %rbp
	addq	%rsi, %rbp
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%rdi), %rbp
	subq	24(%rdi), %rbp
	addq	%rsi, %rbp
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L290:
	addq	%rbp, %rdx
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L298:
	call	_IO_vtable_check
	jmp	.L289
.L297:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbp
	movl	$22, %fs:(%rax)
	jmp	.L282
.L293:
	movq	$-1, %rbp
	jmp	.L282
.LFE106:
	.size	_IO_file_seekoff_mmap, .-_IO_file_seekoff_mmap
	.p2align 4,,15
	.globl	_IO_file_underflow_mmap
	.type	_IO_file_underflow_mmap, @function
_IO_file_underflow_mmap:
.LFB98:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L300
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	mmap_remap_check
	testl	%eax, %eax
	jne	.L308
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jnb	.L304
	movzbl	(%rax), %eax
.L299:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L309
.L303:
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
.L304:
	orl	$16, (%rbx)
	movl	$-1, %eax
	jmp	.L299
.L309:
	call	_IO_vtable_check
	jmp	.L303
.LFE98:
	.size	_IO_file_underflow_mmap, .-_IO_file_underflow_mmap
	.p2align 4,,15
	.globl	__GI__IO_file_read
	.hidden	__GI__IO_file_read
	.type	__GI__IO_file_read, @function
__GI__IO_file_read:
.LFB108:
	movq	%rdi, %rax
	movl	112(%rdi), %edi
	testb	$2, 116(%rax)
	jne	.L312
	jmp	__GI___read
	.p2align 4,,10
	.p2align 3
.L312:
	jmp	__GI___read_nocancel
.LFE108:
	.size	__GI__IO_file_read, .-__GI__IO_file_read
	.globl	_IO_file_read
	.set	_IO_file_read,__GI__IO_file_read
	.p2align 4,,15
	.globl	_IO_new_file_xsputn
	.type	_IO_new_file_xsputn, @function
_IO_new_file_xsputn:
.LFB114:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L313
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %rbp
	movl	(%rbx), %eax
	movq	40(%rdi), %rdi
	andl	$2560, %eax
	cmpl	$2560, %eax
	je	.L344
	movq	48(%rbx), %rdx
	cmpq	%rdi, %rdx
	jbe	.L331
	subq	%rdi, %rdx
.L316:
	testq	%rdx, %rdx
	je	.L331
.L335:
	xorl	%r14d, %r14d
.L317:
	cmpq	%rdx, %rbp
	movq	%r13, %rsi
	movq	%rbp, %r15
	cmovbe	%rbp, %rdx
	movq	%rdx, %r12
	addq	%r12, %r13
	subq	%r12, %r15
	call	__GI_mempcpy@PLT
	movq	%rax, 40(%rbx)
.L320:
	addq	%r15, %r14
	jne	.L322
.L343:
	subq	%r15, %rbp
	movq	%rbp, %r12
.L313:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%rbp, %r15
.L322:
	movq	216(%rbx), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r12, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L345
.L324:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%r12)
	cmpl	$-1, %eax
	je	.L346
	movq	64(%rbx), %rcx
	subq	56(%rbx), %rcx
	movq	%r15, %r14
	cmpq	$127, %rcx
	jbe	.L326
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rcx
	subq	%rdx, %r14
.L326:
	testq	%r14, %r14
	jne	.L347
.L327:
	testq	%r15, %r15
	movq	%rbp, %r12
	je	.L313
	leaq	0(%r13,%r14), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	__GI__IO_default_xsputn
	subq	%rax, %r15
	subq	%r15, %r12
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L344:
	movq	64(%rbx), %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, %rbp
	ja	.L316
	leaq	(%rsi,%rbp), %rcx
	cmpq	%rcx, %rsi
	jnb	.L335
	cmpb	$10, -1(%rcx)
	leaq	-1(%rcx), %rax
	jne	.L319
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L321:
	subq	$1, %rax
	cmpb	$10, (%rax)
	je	.L318
.L319:
	cmpq	%rax, %r13
	jne	.L321
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L346:
	subq	%r15, %rbp
	movq	$-1, %r12
	testq	%r15, %r15
	cmovne	%rbp, %r12
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	new_do_write
	subq	%rax, %r15
	cmpq	%r14, %rax
	jnb	.L327
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L318:
	subq	%r13, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	jne	.L330
	movq	%rbp, %r15
	movl	$1, %r14d
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L345:
	call	_IO_vtable_check
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L330:
	movl	$1, %r14d
	jmp	.L317
.LFE114:
	.size	_IO_new_file_xsputn, .-_IO_new_file_xsputn
	.globl	__GI__IO_file_xsputn
	.set	__GI__IO_file_xsputn,_IO_new_file_xsputn
	.p2align 4,,15
	.globl	_IO_new_file_init_internal
	.hidden	_IO_new_file_init_internal
	.type	_IO_new_file_init_internal, @function
_IO_new_file_init_internal:
.LFB85:
	pushq	%rbp
	pushq	%rbx
	movq	$-1, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	orl	$9228, (%rdi)
	movq	%rbp, 144(%rdi)
	call	__GI__IO_link_in
	movl	%ebp, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE85:
	.size	_IO_new_file_init_internal, .-_IO_new_file_init_internal
	.p2align 4,,15
	.globl	_IO_new_file_init
	.type	_IO_new_file_init, @function
_IO_new_file_init:
.LFB86:
	pushq	%rbp
	pushq	%rbx
	leaq	_IO_vtable_check(%rip), %rax
#APP
# 915 "libioP.h" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	subq	$8, %rsp
	movq	$-1, %rbp
	movq	%rdi, %rbx
	movq	%rax, IO_accept_foreign_vtables(%rip)
	orl	$9228, (%rdi)
	movq	%rbp, 144(%rdi)
	call	__GI__IO_link_in
	movl	%ebp, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE86:
	.size	_IO_new_file_init, .-_IO_new_file_init
	.p2align 4,,15
	.globl	_IO_new_file_close_it
	.type	_IO_new_file_close_it, @function
_IO_new_file_close_it:
.LFB87:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	112(%rdi), %ebp
	cmpl	$-1, %ebp
	je	.L352
	movl	(%rdi), %eax
	xorl	%r12d, %r12d
	movq	%rdi, %rbx
	andl	$2056, %eax
	cmpl	$2048, %eax
	je	.L366
.L354:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	__GI__IO_unsave_markers
	testb	$32, 116(%rbx)
	jne	.L356
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L367
.L357:
	movq	%rbx, %rdi
	call	*136(%rbp)
	movl	%eax, %ebp
.L356:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L358
	movq	160(%rbx), %rax
	cmpq	$0, 64(%rax)
	je	.L359
	movq	%rbx, %rdi
	call	__GI__IO_free_wbackup_area
.L359:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__GI__IO_wsetb
	movq	160(%rbx), %rax
	movq	$0, 16(%rax)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 24(%rax)
	movq	$0, 40(%rax)
.L358:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__GI__IO_setb
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
	call	__GI__IO_un_link
	testl	%ebp, %ebp
	movl	$-72539124, (%rbx)
	movl	$-1, 112(%rbx)
	movq	$-1, 144(%rbx)
	cmove	%r12d, %ebp
.L352:
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jle	.L368
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	__GI__IO_wdo_write
	movl	%eax, %r12d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L368:
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	movl	%eax, %r12d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L367:
	call	_IO_vtable_check
	jmp	.L357
.LFE87:
	.size	_IO_new_file_close_it, .-_IO_new_file_close_it
	.globl	__GI__IO_file_close_it
	.set	__GI__IO_file_close_it,_IO_new_file_close_it
	.p2align 4,,15
	.globl	_IO_new_file_finish
	.type	_IO_new_file_finish, @function
_IO_new_file_finish:
.LFB88:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-1, 112(%rdi)
	je	.L371
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L377
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	__GI__IO_wdo_write
.L373:
	testb	$64, (%rbx)
	jne	.L371
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L378
.L375:
	movq	%rbx, %rdi
	call	*136(%rbp)
.L371:
	addq	$8, %rsp
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	__GI__IO_default_finish
	.p2align 4,,10
	.p2align 3
.L377:
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L378:
	call	_IO_vtable_check
	jmp	.L375
.LFE88:
	.size	_IO_new_file_finish, .-_IO_new_file_finish
	.globl	__GI__IO_file_finish
	.set	__GI__IO_file_finish,_IO_new_file_finish
	.p2align 4,,15
	.globl	__GI__IO_file_open
	.hidden	__GI__IO_file_open
	.type	__GI__IO_file_open, @function
__GI__IO_file_open:
.LFB89:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%r8d, %ebp
	testb	$2, 116(%rbx)
	movq	%rsi, %rdi
	movl	%edx, %esi
	movl	%ecx, %edx
	jne	.L387
	call	__GI___open
	movl	%eax, %r12d
.L381:
	testl	%r12d, %r12d
	js	.L385
	movl	(%rbx), %eax
	movl	%ebp, %edx
	andl	$4100, %ebp
	andl	$4108, %edx
	movl	%r12d, 112(%rbx)
	andl	$-4109, %eax
	orl	%edx, %eax
	cmpl	$4100, %ebp
	movl	%eax, (%rbx)
	jne	.L383
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L388
.L384:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L389
.L383:
	movq	%rbx, %rdi
	call	__GI__IO_link_in
	movq	%rbx, %rax
.L379:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L383
	movl	%r12d, %edi
	call	__GI___close_nocancel
	xorl	%eax, %eax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L387:
	call	__GI___open_nocancel
	movl	%eax, %r12d
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L385:
	xorl	%eax, %eax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L388:
	call	_IO_vtable_check
	jmp	.L384
.LFE89:
	.size	__GI__IO_file_open, .-__GI__IO_file_open
	.globl	_IO_file_open
	.set	_IO_file_open,__GI__IO_file_open
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	",ccs="
.LC1:
	.string	"fileops.c"
.LC2:
	.string	"fcts.towc_nsteps == 1"
.LC3:
	.string	"fcts.tomb_nsteps == 1"
	.text
	.p2align 4,,15
	.globl	_IO_new_file_fopen
	.type	_IO_new_file_fopen, @function
_IO_new_file_fopen:
.LFB90:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpl	$-1, 112(%rdi)
	jne	.L409
	movzbl	(%rdx), %eax
	movq	%rdi, %rbx
	cmpb	$114, %al
	je	.L394
	cmpb	$119, %al
	je	.L423
	cmpb	$97, %al
	je	.L443
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$22, %fs:(%rax)
.L390:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$4, %r8d
	movl	$1, %r9d
	movl	$576, %r10d
.L395:
	leaq	6(%rdx), %rdi
	movq	%rdx, %r12
.L406:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	cmpb	$99, %al
	je	.L398
	jg	.L399
	cmpb	$43, %al
	je	.L400
	cmpb	$98, %al
	je	.L442
	testb	%al, %al
	je	.L402
	.p2align 4,,10
	.p2align 3
.L397:
	cmpq	%rdi, %rdx
	jne	.L406
.L402:
	movl	%r10d, %edx
	movq	%rbx, %rdi
	orl	%r9d, %edx
	movl	%ecx, %r9d
	movl	$438, %ecx
	call	__GI__IO_file_open
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L409
	leaq	1(%r12), %rdi
	leaq	.LC0(%rip), %rsi
	call	__GI_strstr
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L390
	leaq	5(%rax), %r15
	movl	$44, %esi
	movq	%r15, %rdi
	call	__strchrnul@PLT
	subq	%r15, %rax
	leaq	3(%rax), %rdi
	movq	%rax, %r14
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L444
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L445
	movq	104+_nl_C_locobj(%rip), %r11
	movq	120+_nl_C_locobj(%rip), %r14
	movq	%r12, %rdi
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	movabsq	$2251799813701639, %r10
	.p2align 4,,10
	.p2align 3
.L416:
	movsbq	%dl, %r8
	leal	-44(%rdx), %ecx
	movzwl	(%r11,%r8,2), %eax
	shrw	$3, %ax
	andl	$1, %eax
	cmpb	$51, %cl
	ja	.L412
	movq	%r10, %r15
	shrq	%cl, %r15
	movq	%r15, %rcx
	andl	$1, %ecx
	orl	%ecx, %eax
.L412:
	testb	%al, %al
	leal	1(%r9), %ecx
	je	.L413
	movl	(%r14,%r8,4), %eax
	addq	$1, %rsi
	movb	%al, -1(%rsi)
.L414:
	addq	$1, %rdi
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	jne	.L416
	cmpl	$1, %r9d
	jg	.L415
	cmpl	$1, %ecx
	leaq	1(%rsi), %rax
	movb	$47, (%rsi)
	jne	.L424
.L411:
	leaq	1(%rax), %rsi
	movb	$47, (%rax)
.L415:
	movb	$0, (%rsi)
	cmpb	$0, 2(%r12)
	je	.L446
.L418:
	movq	%rsp, %rdi
	movq	%r12, %rsi
	call	__wcsmbs_named_conv
	testl	%eax, %eax
	jne	.L447
	movq	%r12, %rdi
	call	free@PLT
	cmpq	$1, 8(%rsp)
	jne	.L448
	cmpq	$1, 24(%rsp)
	jne	.L449
	movq	160(%rbx), %rax
	movq	16(%rsp), %rcx
	movq	8(%rax), %rdx
	movq	$0, 88(%rax)
	movq	%rdx, (%rax)
	movq	24(%rax), %rdx
	movq	%rdx, 32(%rax)
	movq	160(%rbx), %rax
	movq	$0, 96(%rax)
	movq	160(%rbx), %rax
	movq	160(%rbp), %rsi
	leaq	104(%rax), %rdx
	movq	%rdx, 152(%rbx)
	movq	(%rsp), %rdx
	movq	$1, 128(%rax)
	movl	$1, 136(%rax)
	movq	%rcx, 160(%rax)
	movq	$9, 184(%rax)
	movq	%rdx, 104(%rax)
	leaq	88(%rsi), %rdx
	movl	$1, 192(%rax)
	movq	%rdx, 144(%rax)
	movq	%rdx, 200(%rax)
	movq	224(%rax), %rax
	movq	%rax, 216(%rbx)
	movl	$1, 192(%rbp)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L399:
	cmpb	$109, %al
	je	.L403
	cmpb	$120, %al
	je	.L404
	cmpb	$101, %al
	jne	.L397
	orl	$524288, %r10d
	orl	$64, 116(%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L404:
	orb	$-128, %r10b
.L442:
	movq	%rdx, %r12
	jmp	.L397
.L444:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%rbx, %rdi
	movl	%fs:0(%rbp), %r12d
	call	__GI__IO_file_close_it
	movl	%r12d, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L409:
	xorl	%ebp, %ebp
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L403:
	orl	$1, 116(%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L400:
	andl	$4096, %r8d
	movq	%rdx, %r12
	movl	$2, %r9d
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L398:
	orl	$2, 116(%rbx)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$8, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L443:
	movl	$4100, %r8d
	movl	$1, %r9d
	movl	$1088, %r10d
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L446:
	movq	120+_nl_C_locobj(%rip), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L419:
	movsbq	5(%r13,%rax), %rdx
	movl	(%rcx,%rdx,4), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L419
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L413:
	cmpb	$47, %dl
	jne	.L414
	cmpl	$3, %ecx
	je	.L415
	leal	2(%r9), %eax
	movb	$47, (%rsi)
	movl	%ecx, %r9d
	addq	$1, %rsi
	movl	%eax, %ecx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rax, %rsi
	jmp	.L415
.L447:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	__GI__IO_file_close_it
	movq	%r12, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L390
.L445:
	leaq	1(%r12), %rax
	movb	$47, (%r12)
	jmp	.L411
.L448:
	leaq	__PRETTY_FUNCTION__.12613(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$322, %edx
	call	__GI___assert_fail
.L449:
	leaq	__PRETTY_FUNCTION__.12613(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$323, %edx
	call	__GI___assert_fail
.LFE90:
	.size	_IO_new_file_fopen, .-_IO_new_file_fopen
	.globl	__GI__IO_file_fopen
	.set	__GI__IO_file_fopen,_IO_new_file_fopen
	.p2align 4,,15
	.globl	_IO_new_file_attach
	.type	_IO_new_file_attach, @function
_IO_new_file_attach:
.LFB91:
	cmpl	$-1, 112(%rdi)
	jne	.L459
	pushq	%r13
	pushq	%r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	movq	216(%rdi), %r12
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%esi, 112(%rdi)
	movq	$-1, 144(%rdi)
	andl	$-13, %eax
	movq	%r12, %rcx
	orl	$64, %eax
	subq	%rdx, %rcx
	movl	%fs:0(%rbp), %r13d
	movl	%eax, (%rdi)
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L460
.L453:
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*72(%r12)
	cmpq	$-1, %rax
	je	.L461
.L454:
	movl	%r13d, %fs:0(%rbp)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	cmpl	$29, %fs:0(%rbp)
	je	.L454
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L460:
	call	_IO_vtable_check
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L459:
	xorl	%eax, %eax
	ret
.LFE91:
	.size	_IO_new_file_attach, .-_IO_new_file_attach
	.globl	__GI__IO_file_attach
	.set	__GI__IO_file_attach,_IO_new_file_attach
	.p2align 4,,15
	.globl	_IO_new_do_write
	.type	_IO_new_do_write, @function
_IO_new_do_write:
.LFB94:
	testq	%rdx, %rdx
	jne	.L471
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	pushq	%rbx
	movq	%rdx, %rbx
	call	new_do_write
	cmpq	%rax, %rbx
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	popq	%rbx
	ret
.LFE94:
	.size	_IO_new_do_write, .-_IO_new_do_write
	.globl	__GI__IO_do_write
	.set	__GI__IO_do_write,_IO_new_do_write
	.p2align 4,,15
	.globl	_IO_new_file_underflow
	.type	_IO_new_file_underflow, @function
_IO_new_file_underflow:
.LFB96:
	movl	(%rdi), %eax
	testb	$16, %al
	jne	.L501
	testb	$4, %al
	jne	.L511
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	jb	.L512
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, 56(%rdi)
	je	.L513
	testl	$514, %eax
	je	.L514
.L478:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	(%rax), %r12
	movl	(%r12), %edx
	movl	%edx, %esi
	andl	$32768, %esi
	je	.L515
	movq	%r12, %rdi
.L480:
	leaq	__stop___libc_IO_vtables(%rip), %r15
	leaq	__start___libc_IO_vtables(%rip), %rbp
	andl	$648, %edx
	movq	%r15, %r13
	movq	%rbp, %r14
	subq	%rbp, %r13
	cmpl	$640, %edx
	je	.L486
.L490:
	testl	$32768, (%r12)
	jne	.L488
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L488
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L491
	subl	$1, (%rdi)
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%rbx, %rdi
.LEHB0:
	call	__GI__IO_switch_to_get_mode
	movq	216(%rbx), %rbp
	movq	56(%rbx), %rsi
	movq	%rbp, %rax
	subq	%r14, %rax
	movq	%rsi, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	%r13, %rax
	movq	%rsi, 16(%rbx)
	movq	%rsi, 48(%rbx)
	movq	%rsi, 40(%rbx)
	movq	%rsi, 32(%rbx)
	jnb	.L516
.L492:
	movq	64(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	*112(%rbp)
.LEHE0:
	testq	%rax, %rax
	jle	.L517
	movq	144(%rbx), %rdx
	addq	%rax, 16(%rbx)
	cmpq	$-1, %rdx
	je	.L497
	addq	%rdx, %rax
	movq	%rax, 144(%rbx)
.L497:
	movq	8(%rbx), %rax
	movzbl	(%rax), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	movzbl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	movq	136(%r12), %rcx
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rcx)
	je	.L503
#APP
# 497 "fileops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L482
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L483:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	136(%r12), %rcx
	movq	(%rax), %rdi
	movq	%rbp, 8(%rcx)
	movl	(%rdi), %edx
.L481:
	addl	$1, 4(%rcx)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L514:
	leaq	__stop___libc_IO_vtables(%rip), %r15
	leaq	__start___libc_IO_vtables(%rip), %rbp
.L488:
	subq	%rbp, %r15
	leaq	__start___libc_IO_vtables(%rip), %r14
	movq	%r15, %r13
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L513:
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L477
	call	free@PLT
	andl	$-257, (%rbx)
.L477:
	movq	%rbx, %rdi
	call	__GI__IO_doallocbuf
	movl	(%rbx), %eax
	testl	$514, %eax
	jne	.L478
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L517:
	movl	(%rbx), %edx
	movl	%edx, %ecx
	orl	$16, %edx
	orl	$32, %ecx
	testq	%rax, %rax
	movq	$-1, %rax
	cmovne	%ecx, %edx
	movq	%rax, 144(%rbx)
	movl	%edx, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	movq	216(%rdi), %rax
	movq	%rax, 8(%rsp)
	subq	%rbp, %rax
	cmpq	%r13, %rax
	jnb	.L518
.L489:
	movq	8(%rsp), %rax
	movl	$-1, %esi
.LEHB1:
	call	*24(%rax)
.LEHE1:
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L503:
	movq	%r12, %rdi
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L516:
.LEHB2:
	call	_IO_vtable_check
.LEHE2:
	movq	56(%rbx), %rsi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L501:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	orl	$32, %eax
	movl	%eax, (%rdi)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	movl	%esi, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L483
	movq	%rcx, %rdi
.LEHB3:
	call	__lll_lock_wait_private
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L518:
	call	_IO_vtable_check
.LEHE3:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	jmp	.L489
.L491:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L488
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L479
.L504:
	testl	$32768, (%r12)
	movq	%rax, %r8
	jne	.L499
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L499
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L500
	subl	$1, (%rdi)
.L499:
	movq	%r8, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
.L500:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L499
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L499
.LFE96:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA96:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE96-.LLSDACSB96
.LLSDACSB96:
	.uleb128 .LEHB0-.LFB96
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB96
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L504-.LFB96
	.uleb128 0
	.uleb128 .LEHB2-.LFB96
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB96
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L504-.LFB96
	.uleb128 0
	.uleb128 .LEHB4-.LFB96
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE96:
	.text
	.size	_IO_new_file_underflow, .-_IO_new_file_underflow
	.globl	__GI__IO_file_underflow
	.set	__GI__IO_file_underflow,_IO_new_file_underflow
	.p2align 4,,15
	.globl	_IO_new_file_overflow
	.type	_IO_new_file_overflow, @function
_IO_new_file_overflow:
.LFB101:
	movl	(%rdi), %ecx
	testb	$8, %cl
	jne	.L561
	testb	$8, %ch
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rax
	je	.L522
	testq	%rax, %rax
	je	.L523
	movq	40(%rdi), %rdx
.L524:
	cmpl	$-1, %ebp
	je	.L562
.L531:
	cmpq	%rdx, 64(%rbx)
	je	.L563
.L532:
	leaq	1(%rdx), %rax
	movq	%rax, 40(%rbx)
	movb	%bpl, (%rdx)
	movl	(%rbx), %eax
	testb	$2, %al
	jne	.L536
	testb	$2, %ah
	je	.L539
	cmpl	$10, %ebp
	je	.L536
.L539:
	movzbl	%bpl, %eax
.L519:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	testq	%rax, %rax
	je	.L523
	movq	8(%rdi), %rdx
.L525:
	testb	$1, %ch
	jne	.L564
.L526:
	movq	64(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L529
	movq	16(%rbx), %rsi
.L530:
	movq	%rsi, 8(%rbx)
	movq	%rsi, 24(%rbx)
	movl	192(%rbx), %esi
	movq	%rax, 48(%rbx)
	movl	%ecx, %eax
	orb	$8, %ah
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movl	%eax, (%rbx)
	testl	%esi, %esi
	jg	.L524
	andl	$514, %ecx
	je	.L524
	cmpl	$-1, %ebp
	movq	%rdx, 48(%rbx)
	jne	.L531
.L562:
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	subq	%rsi, %rdx
	jmp	__GI__IO_do_write
	.p2align 4,,10
	.p2align 3
.L536:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	cmpl	$-1, %eax
	jne	.L539
.L538:
	movl	$-1, %eax
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L563:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L565
	movq	160(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	__GI__IO_wdo_write
	cmpl	$-1, %eax
	sete	%al
.L534:
	testb	%al, %al
	jne	.L538
	movq	40(%rbx), %rdx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L565:
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	cmpl	$-1, %eax
	sete	%al
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L529:
	movq	56(%rbx), %rdx
	movq	%rdx, 16(%rbx)
	movq	%rdx, %rsi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%rbx, %rdi
	call	__GI__IO_doallocbuf
	movq	56(%rbx), %rdx
	movl	(%rbx), %ecx
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L564:
	movq	16(%rbx), %r12
	movq	%rbx, %rdi
	subq	%rdx, %r12
	call	__GI__IO_free_backup_area
	movq	24(%rbx), %rdx
	movq	%r12, %rsi
	negq	%rsi
	movq	%rdx, %rcx
	subq	56(%rbx), %rcx
	movq	%rcx, %rax
	negq	%rax
	cmpq	%r12, %rcx
	movl	(%rbx), %ecx
	cmova	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L561:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, %ecx
	movl	%ecx, (%rdi)
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
.LFE101:
	.size	_IO_new_file_overflow, .-_IO_new_file_overflow
	.globl	__GI__IO_file_overflow
	.set	__GI__IO_file_overflow,_IO_new_file_overflow
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12613, @object
	.size	__PRETTY_FUNCTION__.12613, 19
__PRETTY_FUNCTION__.12613:
	.string	"_IO_new_file_fopen"
	.hidden	_IO_file_jumps_maybe_mmap
	.globl	_IO_file_jumps_maybe_mmap
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_file_jumps_maybe_mmap, @object
	.size	_IO_file_jumps_maybe_mmap, 168
_IO_file_jumps_maybe_mmap:
	.quad	0
	.quad	0
	.quad	__GI__IO_file_finish
	.quad	__GI__IO_file_overflow
	.quad	_IO_file_underflow_maybe_mmap
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	_IO_file_xsgetn_maybe_mmap
	.quad	_IO_file_seekoff_maybe_mmap
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_new_file_sync
	.quad	__GI__IO_file_doallocate
	.quad	__GI__IO_file_read
	.quad	_IO_new_file_write
	.quad	__GI__IO_file_seek
	.quad	__GI__IO_file_close
	.quad	__GI__IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_file_jumps_mmap
	.globl	_IO_file_jumps_mmap
	.align 32
	.type	_IO_file_jumps_mmap, @object
	.size	_IO_file_jumps_mmap, 168
_IO_file_jumps_mmap:
	.quad	0
	.quad	0
	.quad	__GI__IO_file_finish
	.quad	__GI__IO_file_overflow
	.quad	_IO_file_underflow_mmap
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	_IO_file_xsgetn_mmap
	.quad	_IO_file_seekoff_mmap
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_file_sync_mmap
	.quad	__GI__IO_file_doallocate
	.quad	__GI__IO_file_read
	.quad	_IO_new_file_write
	.quad	__GI__IO_file_seek
	.quad	_IO_file_close_mmap
	.quad	__GI__IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	__GI__IO_file_jumps
	.globl	__GI__IO_file_jumps
	.align 32
	.type	__GI__IO_file_jumps, @object
	.size	__GI__IO_file_jumps, 168
__GI__IO_file_jumps:
	.quad	0
	.quad	0
	.quad	__GI__IO_file_finish
	.quad	__GI__IO_file_overflow
	.quad	__GI__IO_file_underflow
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_default_pbackfail
	.quad	__GI__IO_file_xsputn
	.quad	__GI__IO_file_xsgetn
	.quad	_IO_new_file_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_new_file_setbuf
	.quad	_IO_new_file_sync
	.quad	__GI__IO_file_doallocate
	.quad	__GI__IO_file_read
	.quad	_IO_new_file_write
	.quad	__GI__IO_file_seek
	.quad	__GI__IO_file_close
	.quad	__GI__IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.globl	_IO_file_jumps
	.set	_IO_file_jumps,__GI__IO_file_jumps
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__wcsmbs_named_conv
	.hidden	_nl_C_locobj
	.hidden	IO_accept_foreign_vtables
	.hidden	__lseek64
	.hidden	_IO_wfile_jumps_mmap
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
