	.text
	.p2align 4,,15
	.globl	_IO_vscanf
	.type	_IO_vscanf, @function
_IO_vscanf:
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdin(%rip), %rdi
	xorl	%ecx, %ecx
	jmp	__vfscanf_internal
	.size	_IO_vscanf, .-_IO_vscanf
	.weak	vscanf
	.set	vscanf,_IO_vscanf
	.hidden	__vfscanf_internal
