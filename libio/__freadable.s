	.text
	.p2align 4,,15
	.globl	__freadable
	.type	__freadable, @function
__freadable:
	xorl	%eax, %eax
	testb	$4, (%rdi)
	sete	%al
	ret
	.size	__freadable, .-__freadable
