	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fpurge
	.type	__fpurge, @function
__fpurge:
	movl	(%rdi), %eax
	movl	192(%rdi), %edx
	pushq	%rbx
	movq	%rdi, %rbx
	andl	$256, %eax
	testl	%edx, %edx
	jle	.L2
	testl	%eax, %eax
	jne	.L14
.L3:
	movq	160(%rbx), %rax
	movq	(%rax), %rdx
	movq	%rdx, 8(%rax)
	movq	24(%rax), %rdx
	movq	%rdx, 32(%rax)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	call	__GI__IO_free_wbackup_area
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%eax, %eax
	jne	.L15
.L5:
	movq	8(%rbx), %rax
	movq	%rax, 16(%rbx)
	movq	32(%rbx), %rax
	movq	%rax, 40(%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	call	__GI__IO_free_backup_area
	jmp	.L5
	.size	__fpurge, .-__fpurge
