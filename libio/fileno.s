	.text
	.p2align 4,,15
	.globl	__fileno
	.hidden	__fileno
	.type	__fileno, @function
__fileno:
	testl	$8192, (%rdi)
	je	.L2
	movl	112(%rdi), %eax
	testl	%eax, %eax
	js	.L2
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__fileno, .-__fileno
	.weak	fileno_unlocked
	.set	fileno_unlocked,__fileno
	.weak	fileno
	.hidden	fileno
	.set	fileno,__fileno
