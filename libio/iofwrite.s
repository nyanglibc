	.text
	.p2align 4,,15
	.globl	_IO_fwrite
	.hidden	_IO_fwrite
	.type	_IO_fwrite, @function
_IO_fwrite:
.LFB68:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	imulq	%rdx, %rbp
	subq	$8, %rsp
	testq	%rbp, %rbp
	je	.L1
	movq	%rdx, %r13
	movl	(%rcx), %edx
	movq	%rcx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r14
	andl	$32768, %edx
	jne	.L3
	movq	136(%rcx), %rdi
	movq	%fs:16, %r15
	cmpq	%r15, 8(%rdi)
	je	.L4
#APP
# 37 "iofwrite.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L6:
	movq	136(%rbx), %rdi
	movq	%r15, 8(%rdi)
.L4:
	addl	$1, 4(%rdi)
.L3:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L9
	movl	$-1, 192(%rbx)
.L10:
	movq	216(%rbx), %r15
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r15, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L12
.L14:
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	*56(%r15)
	cmpq	$-1, %rax
	movq	%rax, %r8
	sete	%r9b
	testl	$32768, (%rbx)
	jne	.L15
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	jne	.L15
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$-1, %eax
	je	.L10
	testl	$32768, (%rbx)
	jne	.L23
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	jne	.L23
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
.L22:
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L16
	subl	$1, (%rdi)
.L15:
	cmpq	%r8, %rbp
	je	.L24
	testb	%r9b, %r9b
	je	.L33
.L24:
	movq	%r13, %rbp
.L1:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%ebp, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	call	_IO_vtable_check
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%r12
	movq	%rax, %rbp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L6
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
.L26:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L18
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L18
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L19
	subl	$1, (%rdi)
.L18:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L19:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L18
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L18
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L26-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_fwrite, .-_IO_fwrite
	.weak	fwrite
	.hidden	fwrite
	.set	fwrite,_IO_fwrite
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
