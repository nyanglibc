	.text
	.p2align 4,,15
	.globl	fputc
	.type	fputc, @function
fputc:
.LFB68:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	testb	$-128, 116(%rsi)
	movq	%rsi, %rbx
	jne	.L2
	movq	40(%rsi), %rdx
	cmpq	48(%rsi), %rdx
	movzbl	%dil, %r8d
	jnb	.L20
	leaq	1(%rdx), %rcx
	movq	%rcx, 40(%rsi)
	movb	%dil, (%rdx)
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	(%rsi), %edx
	movl	%edi, %ebp
	andl	$32768, %edx
	jne	.L5
	movq	136(%rsi), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L6
#APP
# 37 "fputc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L6:
	addl	$1, 4(%rdi)
.L5:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	movzbl	%bpl, %r8d
	jnb	.L21
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%bpl, (%rax)
.L12:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L13
	subl	$1, (%rdi)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r8d, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	__overflow
.LEHE0:
	movl	%eax, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbx, %rdi
	movl	%r8d, %esi
	popq	%rbx
	popq	%rbp
	popq	%r12
.LEHB1:
	jmp	__overflow
.LEHE1:
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L8
.LEHB2:
	call	__lll_lock_wait_private
.LEHE2:
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L17:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L15
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L15
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L16
	subl	$1, (%rdi)
.L15:
	movq	%r8, %rdi
.LEHB3:
	call	_Unwind_Resume@PLT
.LEHE3:
.L16:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L17-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB2-.LFB68
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L17-.LFB68
	.uleb128 0
	.uleb128 .LEHB3-.LFB68
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	fputc, .-fputc
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__overflow
