	.text
	.p2align 4,,15
	.globl	__fputs_unlocked
	.hidden	__fputs_unlocked
	.type	__fputs_unlocked, @function
__fputs_unlocked:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	strlen
	movq	%rax, %r12
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L2
	movl	$-1, 192(%rbx)
.L3:
	movq	216(%rbx), %r13
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r13, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L12
.L5:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r12, %rax
	jne	.L6
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$-1, %eax
	je	.L3
.L6:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	call	_IO_vtable_check
	jmp	.L5
	.size	__fputs_unlocked, .-__fputs_unlocked
	.weak	fputs_unlocked
	.hidden	fputs_unlocked
	.set	fputs_unlocked,__fputs_unlocked
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	_IO_vtable_check
	.hidden	strlen
