	.text
	.p2align 4,,15
	.globl	__getc_unlocked
	.hidden	__getc_unlocked
	.type	__getc_unlocked, @function
__getc_unlocked:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L4
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	__uflow
	.size	__getc_unlocked, .-__getc_unlocked
	.weak	getc_unlocked
	.hidden	getc_unlocked
	.set	getc_unlocked,__getc_unlocked
	.weak	fgetc_unlocked
	.set	fgetc_unlocked,__getc_unlocked
	.hidden	__uflow
