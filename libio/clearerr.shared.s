	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	clearerr
	.type	clearerr, @function
clearerr:
	movl	(%rdi), %edx
	movl	%edx, %esi
	andl	$32768, %esi
	jne	.L2
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	136(%rdi), %rcx
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rcx)
	je	.L3
	movq	%rdi, %rbx
#APP
# 25 "clearerr.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L4
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rcx
	movl	(%rbx), %edx
	movl	4(%rcx), %eax
	movq	%rbp, 8(%rcx)
	leal	1(%rax), %esi
	movl	%esi, 4(%rcx)
	movl	%edx, %esi
	andl	$-49, %esi
	andb	$-128, %dh
	movl	%esi, (%rbx)
	jne	.L1
.L6:
	testl	%eax, %eax
	movl	%eax, 4(%rcx)
	jne	.L1
	movq	$0, 8(%rcx)
#APP
# 27 "clearerr.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L9
	subl	$1, (%rcx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	andl	$-49, %edx
	movl	%edx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	4(%rcx), %eax
	andl	$-49, %edx
	leal	1(%rax), %esi
	movl	%esi, 4(%rcx)
	movl	%edx, (%rdi)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%esi, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L5
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
#APP
# 27 "clearerr.c" 1
	xchgl %eax, (%rcx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	$202, %eax
#APP
# 27 "clearerr.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	clearerr, .-clearerr
	.hidden	__lll_lock_wait_private
