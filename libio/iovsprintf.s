	.text
	.p2align 4,,15
	.type	_IO_str_chk_overflow, @function
_IO_str_chk_overflow:
	subq	$8, %rsp
	call	__chk_fail
	.size	_IO_str_chk_overflow, .-_IO_str_chk_overflow
	.p2align 4,,15
	.globl	__vsprintf_internal
	.hidden	__vsprintf_internal
	.type	__vsprintf_internal, @function
__vsprintf_internal:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movl	%r8d, %r13d
	pushq	%rbp
	pushq	%rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	$248, %rsp
	movq	%rdx, %r14
	movl	$32768, %esi
	movq	%rsp, %rbp
	movl	$-1, %edx
	movq	$0, 136(%rsp)
	movq	%rbp, %rdi
	call	_IO_no_init@PLT
	testb	$4, %r13b
	je	.L5
	leaq	_IO_str_chk_jumps(%rip), %rax
	movb	$0, (%r12)
	movq	%rax, 216(%rsp)
.L6:
	xorl	%eax, %eax
	cmpq	$-1, %rbx
	movq	%r12, %rcx
	setne	%al
	movq	%r12, %rsi
	movq	%rbp, %rdi
	subq	%rax, %rbx
	movq	%rbx, %rdx
	call	_IO_str_init_static_internal@PLT
	movq	%r15, %rdx
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	__vfprintf_internal
	movq	40(%rsp), %rdx
	movb	$0, (%rdx)
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	_IO_str_jumps(%rip), %rax
	movq	%rax, 216(%rsp)
	jmp	.L6
	.size	__vsprintf_internal, .-__vsprintf_internal
	.p2align 4,,15
	.globl	__vsprintf
	.type	__vsprintf, @function
__vsprintf:
	movq	%rdx, %rcx
	xorl	%r8d, %r8d
	movq	%rsi, %rdx
	movq	$-1, %rsi
	jmp	__vsprintf_internal
	.size	__vsprintf, .-__vsprintf
	.weak	vsprintf
	.set	vsprintf,__vsprintf
	.globl	_IO_vsprintf
	.set	_IO_vsprintf,__vsprintf
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_str_chk_jumps, @object
	.size	_IO_str_chk_jumps, 168
_IO_str_chk_jumps:
	.quad	0
	.quad	0
	.quad	_IO_str_finish
	.quad	_IO_str_chk_overflow
	.quad	_IO_str_underflow
	.quad	_IO_default_uflow
	.quad	_IO_str_pbackfail
	.quad	_IO_default_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_str_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_default_doallocate
	.hidden	_IO_str_seekoff
	.hidden	_IO_default_xsgetn
	.hidden	_IO_default_xsputn
	.hidden	_IO_str_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_str_underflow
	.hidden	_IO_str_jumps
	.hidden	__vfprintf_internal
	.hidden	__chk_fail
