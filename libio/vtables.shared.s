	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal error: glibc detected an invalid stdio handle\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	_IO_vtable_check
	.hidden	_IO_vtable_check
	.type	_IO_vtable_check, @function
_IO_vtable_check:
	leaq	_IO_vtable_check(%rip), %rdi
	movq	IO_accept_foreign_vtables(%rip), %rax
#APP
# 46 "vtables.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	cmpq	%rdi, %rax
	je	.L9
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	je	.L9
	subq	$56, %rsp
	xorl	%ecx, %ecx
	leaq	8(%rsp), %rdx
	leaq	16(%rsp), %rsi
	call	__GI__dl_addr
	testl	%eax, %eax
	je	.L4
	movq	8(%rsp), %rax
	cmpq	$0, 48(%rax)
	je	.L4
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	rep ret
.L4:
	leaq	.LC0(%rip), %rdi
	call	__GI___libc_fatal
	.size	_IO_vtable_check, .-_IO_vtable_check
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	check_stdfiles_vtables, @function
check_stdfiles_vtables:
	movq	_IO_2_1_stdin_@GOTPCREL(%rip), %rdx
	leaq	__GI__IO_file_jumps(%rip), %rax
	cmpq	%rax, 216(%rdx)
	je	.L17
.L14:
	leaq	_IO_vtable_check(%rip), %rax
#APP
# 915 "../libio/libioP.h" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, IO_accept_foreign_vtables(%rip)
	ret
.L17:
	movq	_IO_2_1_stdout_@GOTPCREL(%rip), %rdx
	cmpq	%rax, 216(%rdx)
	jne	.L14
	movq	_IO_2_1_stderr_@GOTPCREL(%rip), %rdx
	cmpq	%rax, 216(%rdx)
	jne	.L14
	ret
	.size	check_stdfiles_vtables, .-check_stdfiles_vtables
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	check_stdfiles_vtables
	.hidden	IO_accept_foreign_vtables
	.comm	IO_accept_foreign_vtables,8,8
