	.text
	.p2align 4,,15
	.globl	__fflush_unlocked
	.hidden	__fflush_unlocked
	.type	__fflush_unlocked, @function
__fflush_unlocked:
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbx
	subq	$16, %rsp
	movq	216(%rdi), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L8
.L3:
	call	*96(%rbx)
	testl	%eax, %eax
	setne	%al
	addq	$16, %rsp
	movzbl	%al, %eax
	negl	%eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	jmp	_IO_flush_all
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rdi
	jmp	.L3
	.size	__fflush_unlocked, .-__fflush_unlocked
	.weak	fflush_unlocked
	.hidden	fflush_unlocked
	.set	fflush_unlocked,__fflush_unlocked
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	_IO_vtable_check
	.hidden	_IO_flush_all
