	.text
	.p2align 4,,15
	.globl	freopen
	.type	freopen, @function
freopen:
.LFB68:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	movq	%rdi, %rbp
	subq	$40, %rsp
	movl	(%rdx), %edx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rbx), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L3
#APP
# 44 "freopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%r13, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	movq	216(%rbx), %r13
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r13, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L8
.L10:
	movq	%rbx, %rdi
.LEHB0:
	call	*96(%r13)
	movl	(%rbx), %eax
	testb	$32, %ah
	je	.L27
	testq	%rbp, %rbp
	movl	112(%rbx), %r13d
	je	.L40
.L12:
	orl	$32, 116(%rbx)
	movq	%rbx, %rdi
	call	_IO_file_close_it
	leaq	_IO_file_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L13
	leaq	_IO_wfile_jumps(%rip), %rsi
	movq	%rsi, 224(%rax)
.L13:
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_file_fopen
	testq	%rax, %rax
	je	.L14
	movq	%rax, %rdi
	call	__fopen_maybe_mmap
	andl	$-33, 116(%rbx)
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L15
	cmpl	$-1, %r13d
	movl	$0, 192(%rax)
	je	.L37
	movl	112(%rax), %edi
	cmpl	%r13d, %edi
	je	.L37
	movl	116(%rax), %edx
	movl	%r13d, %esi
	sall	$13, %edx
	andl	$524288, %edx
	call	__dup3
	cmpl	$-1, %eax
	je	.L41
	movl	112(%rbp), %edi
	call	__close
	movl	%r13d, 112(%rbp)
.L37:
	movl	(%rbx), %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	andl	$-33, 116(%rbx)
.L15:
	cmpl	$-1, %r13d
	je	.L38
	movl	%r13d, %edi
	call	__close
.L38:
	movl	(%rbx), %eax
	xorl	%ebp, %ebp
.L11:
	testb	$-128, %ah
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L23
	subl	$1, (%rdi)
.L1:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rsp, %rsi
	movl	%r13d, %edi
	call	__fd_to_filename
	movq	%rax, %rbp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbp, %rdi
	call	_IO_file_close_it
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L8:
	call	_IO_vtable_check
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%ebp, %ebp
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L23:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L28:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L25
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L25
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L26
	subl	$1, (%rdi)
.L25:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L26:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L25
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L25
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L28-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	freopen, .-freopen
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	__fd_to_filename
	.hidden	__close
	.hidden	__dup3
	.hidden	__fopen_maybe_mmap
	.hidden	_IO_file_fopen
	.hidden	_IO_wfile_jumps
	.hidden	_IO_file_jumps
	.hidden	_IO_file_close_it
