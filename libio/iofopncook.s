	.text
	.p2align 4,,15
	.type	_IO_cookie_read, @function
_IO_cookie_read:
	movq	232(%rdi), %rax
#APP
# 38 "iofopncook.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L2
	movq	224(%rdi), %rdi
	jmp	*%rax
.L2:
	movq	$-1, %rax
	ret
	.size	_IO_cookie_read, .-_IO_cookie_read
	.p2align 4,,15
	.type	_IO_cookie_write, @function
_IO_cookie_write:
	movq	240(%rdi), %rax
#APP
# 53 "iofopncook.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L10
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	224(%rdi), %rdi
	call	*%rax
	cmpq	%rax, %rbp
	jle	.L4
	orl	$32, (%rbx)
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	orl	$32, (%rdi)
	ret
	.size	_IO_cookie_write, .-_IO_cookie_write
	.p2align 4,,15
	.type	_IO_cookie_seek, @function
_IO_cookie_seek:
	subq	$24, %rsp
	movq	248(%rdi), %rax
#APP
# 75 "iofopncook.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	movq	%rsi, 8(%rsp)
	je	.L14
	leaq	8(%rsp), %rsi
	movq	224(%rdi), %rdi
	call	*%rax
	cmpl	$-1, %eax
	je	.L14
	movq	8(%rsp), %rax
.L11:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	$-1, %rax
	jmp	.L11
	.size	_IO_cookie_seek, .-_IO_cookie_seek
	.p2align 4,,15
	.type	_IO_cookie_close, @function
_IO_cookie_close:
	movq	256(%rdi), %rax
#APP
# 91 "iofopncook.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L20
	movq	224(%rdi), %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	ret
	.size	_IO_cookie_close, .-_IO_cookie_close
	.p2align 4,,15
	.type	_IO_cookie_seekoff, @function
_IO_cookie_seekoff:
	movq	$-1, 144(%rdi)
	jmp	_IO_file_seekoff
	.size	_IO_cookie_seekoff, .-_IO_cookie_seekoff
	.p2align 4,,15
	.globl	_IO_cookie_init
	.type	_IO_cookie_init, @function
_IO_cookie_init:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	movq	%rdx, %r12
	andl	$4108, %ebp
	call	_IO_init_internal
	leaq	_IO_cookie_jumps(%rip), %rax
	movq	32(%rsp), %rsi
	movq	40(%rsp), %rcx
	movq	48(%rsp), %rdx
	movq	%r12, 224(%rbx)
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	movq	56(%rsp), %rax
#APP
# 142 "iofopncook.c" 1
	xor %fs:48, %rsi
rol $2*8+1, %rsi
# 0 "" 2
# 143 "iofopncook.c" 1
	xor %fs:48, %rcx
rol $2*8+1, %rcx
# 0 "" 2
#NO_APP
	movq	%rsi, 232(%rbx)
	movq	%rcx, 240(%rbx)
#APP
# 144 "iofopncook.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
# 145 "iofopncook.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rdx, 248(%rbx)
	movq	%rax, 256(%rbx)
	call	_IO_new_file_init_internal
	movl	(%rbx), %esi
	orl	$128, 116(%rbx)
	movl	$-2, 112(%rbx)
	andl	$-4109, %esi
	orl	%ebp, %esi
	movl	%esi, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_IO_cookie_init, .-_IO_cookie_init
	.p2align 4,,15
	.globl	_IO_fopencookie
	.type	_IO_fopencookie, @function
_IO_fopencookie:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movzbl	(%rsi), %eax
	cmpb	$114, %al
	je	.L26
	cmpb	$119, %al
	je	.L27
	cmpb	$97, %al
	je	.L41
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$22, %fs:(%rax)
.L24:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$4100, %r12d
.L28:
	movzbl	1(%rsi), %eax
	cmpb	$43, %al
	je	.L30
	cmpb	$98, %al
	je	.L42
.L31:
	movq	%rdi, %rbp
	movl	$280, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L24
	leaq	264(%rax), %rax
	movq	%rbp, %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	%rax, 136(%rbx)
	pushq	56(%rsp)
	pushq	56(%rsp)
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	_IO_cookie_init
	addq	$32, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	cmpb	$43, 2(%rsi)
	jne	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	andl	$4096, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$4, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$8, %r12d
	jmp	.L28
	.size	_IO_fopencookie, .-_IO_fopencookie
	.weak	fopencookie
	.set	fopencookie,_IO_fopencookie
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_cookie_jumps, @object
	.size	_IO_cookie_jumps, 168
_IO_cookie_jumps:
	.quad	0
	.quad	0
	.quad	_IO_file_finish
	.quad	_IO_file_overflow
	.quad	_IO_file_underflow
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_file_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_cookie_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf
	.quad	_IO_file_sync
	.quad	_IO_file_doallocate
	.quad	_IO_cookie_read
	.quad	_IO_cookie_write
	.quad	_IO_cookie_seek
	.quad	_IO_cookie_close
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_file_doallocate
	.hidden	_IO_file_sync
	.hidden	_IO_file_setbuf
	.hidden	_IO_default_xsgetn
	.hidden	_IO_file_xsputn
	.hidden	_IO_default_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_file_underflow
	.hidden	_IO_file_overflow
	.hidden	_IO_file_finish
	.hidden	_IO_new_file_init_internal
	.hidden	_IO_init_internal
	.hidden	_IO_file_seekoff
