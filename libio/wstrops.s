	.text
	.p2align 4,,15
	.globl	_IO_wstr_underflow
	.type	_IO_wstr_underflow, @function
_IO_wstr_underflow:
	movq	160(%rdi), %rax
	movq	32(%rax), %rdx
	movq	8(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L2
	movq	%rdx, 8(%rax)
	movq	%rdx, %rcx
.L2:
	movl	(%rdi), %esi
	movl	%esi, %r8d
	andl	$3072, %r8d
	cmpl	$3072, %r8d
	je	.L3
	movq	(%rax), %rdx
.L4:
	cmpq	%rcx, %rdx
	movl	$-1, %eax
	jnb	.L1
	movl	(%rdx), %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L3:
	andl	$-2049, %esi
	movl	%esi, (%rdi)
	movq	40(%rax), %rsi
	movq	%rdx, (%rax)
	movq	%rsi, 32(%rax)
	jmp	.L4
	.size	_IO_wstr_underflow, .-_IO_wstr_underflow
	.p2align 4,,15
	.globl	_IO_wstr_overflow
	.type	_IO_wstr_overflow, @function
_IO_wstr_overflow:
	movl	(%rdi), %ecx
	testb	$8, %cl
	je	.L9
	xorl	%eax, %eax
	cmpl	$-1, %esi
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%r15
	movl	%ecx, %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	andl	$3072, %eax
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$1024, %eax
	movq	160(%rdi), %rdx
	je	.L11
	movq	32(%rdx), %rcx
.L12:
	movq	48(%rdx), %r13
	movq	56(%rdx), %rbp
	xorl	%r8d, %r8d
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movl	%esi, %r12d
	subq	%r13, %rbp
	movq	%rbp, %r14
	sarq	$2, %r14
	cmpl	$-1, %esi
	sete	%r8b
	subq	24(%rdx), %rdi
	addq	%r14, %r8
	sarq	$2, %rdi
	cmpq	%rdi, %r8
	jbe	.L13
	movq	8(%rdx), %rsi
.L14:
	cmpl	$-1, %r12d
	je	.L18
	leaq	4(%rcx), %rax
	movq	%rax, 32(%rdx)
	movl	%r12d, (%rcx)
	movq	%rax, %rcx
.L18:
	cmpq	%rsi, %rcx
	movl	%r12d, %eax
	jbe	.L8
	movq	%rcx, 8(%rdx)
.L8:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testb	$8, 116(%rbx)
	jne	.L16
	leaq	50(%r14), %r15
	movabsq	$4611686018427387903, %rax
	leaq	(%r15,%r15), %rcx
	cmpq	%rax, %rcx
	ja	.L16
	cmpq	%rcx, %r14
	movq	%rcx, (%rsp)
	ja	.L16
	leaq	0(,%r15,8), %rax
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L16
	testq	%r13, %r13
	movq	(%rsp), %rcx
	je	.L17
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__wmemcpy
	movq	%r13, %rdi
	call	free@PLT
	movq	160(%rbx), %rax
	movq	(%rsp), %rcx
	movq	$0, 48(%rax)
.L17:
	movq	%rcx, %rdx
	leaq	(%r15,%rbp), %rdi
	xorl	%esi, %esi
	subq	%r14, %rdx
	call	__wmemset
	movq	8(%rsp), %rdx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	%r15, %rdx
	call	_IO_wsetb
	movq	160(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	8(%rdx), %rsi
	movq	32(%rdx), %rcx
	movq	%r15, 24(%rdx)
	subq	%r13, %rax
	subq	%r13, %rsi
	addq	%r15, %rax
	subq	%r13, %rcx
	addq	%r15, %rsi
	movq	%rax, 16(%rdx)
	movq	(%rdx), %rax
	addq	%r15, %rcx
	movq	%rsi, 8(%rdx)
	movq	%rcx, 32(%rdx)
	subq	%r13, %rax
	addq	%r15, %rax
	movq	%rax, (%rdx)
	movq	56(%rdx), %rax
	movq	%rax, 40(%rdx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	orb	$8, %ch
	movq	8(%rdx), %rax
	movl	%ecx, (%rdi)
	movq	(%rdx), %rcx
	movq	%rax, (%rdx)
	movq	%rcx, 32(%rdx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	_IO_wstr_overflow, .-_IO_wstr_overflow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wstrops.c"
.LC1:
	.string	"offset >= oldend"
	.text
	.p2align 4,,15
	.type	enlarge_userbuf, @function
enlarge_userbuf:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	160(%rdi), %rbx
	movq	48(%rbx), %r15
	movq	56(%rbx), %rcx
	subq	%r15, %rcx
	sarq	$2, %rcx
	cmpq	%rsi, %rcx
	jge	.L41
	movl	116(%rdi), %ebp
	andl	$8, %ebp
	jne	.L35
	leaq	100(%rsi), %r12
	movabsq	$4611686018427387903, %rax
	movq	%rcx, 40(%rsp)
	cmpq	%rax, %r12
	ja	.L35
	movq	24(%rbx), %rax
	salq	$2, %r12
	movq	%rdi, 24(%rsp)
	movq	%r12, %rdi
	movl	%edx, 36(%rsp)
	movq	%rsi, 8(%rsp)
	movq	40(%rbx), %r14
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L35
	testq	%r15, %r15
	je	.L36
	movq	40(%rsp), %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rcx, %rdx
	call	__wmemcpy
	movq	%r15, %rdi
	call	free@PLT
	movq	$0, 48(%rbx)
.L36:
	subq	16(%rsp), %r14
	movq	24(%rsp), %rdi
	leaq	0(%r13,%r12), %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %r8
	sarq	$2, %r8
	movq	%r8, 16(%rsp)
	call	_IO_wsetb
	movl	36(%rsp), %eax
	movq	16(%rsp), %r8
	testl	%eax, %eax
	je	.L37
	movq	24(%rbx), %rax
	movq	%r13, 16(%rbx)
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 24(%rbx)
	movq	32(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 40(%rbx)
	movq	(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	cmpq	%r8, 8(%rsp)
	movq	%rax, (%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 8(%rbx)
	jl	.L40
	movq	8(%rsp), %rdx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	subq	%r8, %rdx
	call	__wmemset
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %ebp
.L32:
	addq	$56, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%ebp, %ebp
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 16(%rbx)
	movq	(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 8(%rbx)
	movq	32(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	cmpq	%r8, 8(%rsp)
	movq	%rax, 32(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 40(%rbx)
	jl	.L40
	movq	8(%rsp), %rdx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	subq	%r8, %rdx
	call	__wmemset
	jmp	.L32
.L40:
	leaq	__PRETTY_FUNCTION__.11554(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$225, %edx
	call	__assert_fail
	.size	enlarge_userbuf, .-enlarge_userbuf
	.p2align 4,,15
	.globl	_IO_wstr_seekoff
	.type	_IO_wstr_seekoff, @function
_IO_wstr_seekoff:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	160(%rdi), %rdx
	testl	%ecx, %ecx
	movl	(%rdi), %esi
	movq	32(%rdx), %rax
	movq	24(%rdx), %rdi
	jne	.L50
	movl	%esi, %ecx
	andl	$1024, %ecx
	je	.L51
	testl	$2048, %esi
	jne	.L76
	cmpq	%rax, %rdi
	jb	.L80
	cmpq	%rax, 8(%rdx)
	movq	16(%rdx), %rsi
	movl	$1, %ecx
	cmovnb	8(%rdx), %rax
	subq	%rsi, %rax
	movq	%rax, %r14
	movq	%rax, %r15
	sarq	$2, %r14
.L75:
	testl	%r13d, %r13d
	je	.L78
	cmpl	$1, %r13d
	je	.L61
	movq	%r14, %rdi
	movabsq	$2305843009213693951, %rax
	movq	%r14, %rbx
	negq	%rdi
	subq	%r14, %rax
.L60:
	cmpq	%rdi, %r12
	jl	.L69
	cmpq	%rax, %r12
	jg	.L69
	addq	%r12, %rbx
	cmpq	%r14, %rbx
	jg	.L89
.L64:
	leaq	(%rsi,%rbx,4), %rax
	movq	%r12, %rbx
	movq	%rax, (%rdx)
	leaq	(%rsi,%r15), %rax
	movq	%rax, 8(%rdx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L50:
	cmpq	%rax, %rdi
	jnb	.L90
.L52:
	testl	$256, %esi
	jne	.L73
.L91:
	movq	48(%rdx), %r8
	movq	%r8, 16(%rdx)
.L56:
	andl	$-2049, %esi
	testl	%ecx, %ecx
	movq	%rax, (%rdx)
	movq	%rax, 8(%rdx)
	movl	%esi, 0(%rbp)
	jne	.L55
.L54:
	subq	%rdi, %rax
	sarq	$2, %rax
	movq	%rax, %rbx
.L49:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	72(%rdx), %r8
	movq	%r8, 16(%rdx)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L90:
	testl	$2048, %esi
	jne	.L52
.L55:
	cmpq	%rax, 8(%rdx)
	movq	16(%rdx), %rsi
	movq	$-1, %rbx
	cmovnb	8(%rdx), %rax
	subq	%rsi, %rax
	movq	%rax, %r14
	movq	%rax, %r15
	sarq	$2, %r14
	testb	$1, %cl
	jne	.L75
.L58:
	andl	$2, %ecx
	je	.L49
	testl	%r13d, %r13d
	je	.L79
	cmpl	$1, %r13d
	jne	.L86
	movq	32(%rdx), %rbx
	subq	24(%rdx), %rbx
	movabsq	$2305843009213693951, %rax
	sarq	$2, %rbx
	movq	%rbx, %rcx
	subq	%rbx, %rax
	negq	%rcx
.L67:
	cmpq	%rcx, %r12
	jl	.L69
	cmpq	%rax, %r12
	jg	.L69
	addq	%r12, %rbx
	cmpq	%r14, %rbx
	jg	.L70
.L72:
	movq	24(%rdx), %rax
	leaq	(%rax,%rbx,4), %rax
	movq	%rax, 32(%rdx)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L51:
	cmpq	%rax, %rdi
	jb	.L52
	testl	$2048, %esi
	je	.L54
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L76:
	testl	$256, %esi
	movl	$2, %ecx
	jne	.L73
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L79:
	movabsq	$2305843009213693951, %rax
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L78:
	movabsq	$2305843009213693951, %rax
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r14, %rcx
	movabsq	$2305843009213693951, %rax
	movq	%r14, %rbx
	negq	%rcx
	subq	%r14, %rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	enlarge_userbuf
	testl	%eax, %eax
	jne	.L71
	movq	160(%rbp), %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%rdx), %rbx
	movabsq	$2305843009213693951, %rax
	subq	%rsi, %rbx
	sarq	$2, %rbx
	movq	%rbx, %rdi
	subq	%rbx, %rax
	negq	%rdi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	%ecx, 12(%rsp)
	call	enlarge_userbuf
	testl	%eax, %eax
	movl	12(%rsp), %ecx
	jne	.L71
	movq	160(%rbp), %rdx
	movq	16(%rdx), %rsi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1, %ecx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L69:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbx
	movl	$22, %fs:(%rax)
	jmp	.L49
.L71:
	movq	$-1, %rbx
	jmp	.L49
	.size	_IO_wstr_seekoff, .-_IO_wstr_seekoff
	.p2align 4,,15
	.globl	_IO_wstr_pbackfail
	.type	_IO_wstr_pbackfail, @function
_IO_wstr_pbackfail:
	testb	$8, (%rdi)
	je	.L95
	cmpl	$-1, %esi
	jne	.L93
.L95:
	jmp	_IO_wdefault_pbackfail
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$-1, %eax
	ret
	.size	_IO_wstr_pbackfail, .-_IO_wstr_pbackfail
	.p2align 4,,15
	.globl	_IO_wstr_finish
	.type	_IO_wstr_finish, @function
_IO_wstr_finish:
	pushq	%rbx
	movq	160(%rdi), %rax
	movq	%rdi, %rbx
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L100
	testb	$8, 116(%rbx)
	je	.L105
.L100:
	movq	$0, 48(%rax)
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	jmp	_IO_wdefault_finish
	.p2align 4,,10
	.p2align 3
.L105:
	call	free@PLT
	movq	160(%rbx), %rax
	jmp	.L100
	.size	_IO_wstr_finish, .-_IO_wstr_finish
	.p2align 4,,15
	.globl	_IO_wstr_init_static
	.type	_IO_wstr_init_static, @function
_IO_wstr_init_static:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	jne	.L107
	movq	%rsi, %rdi
	call	__wcslen@PLT
	leaq	(%rbx,%rax,4), %rbp
.L108:
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_IO_wsetb
	movq	160(%r12), %rax
	testq	%r13, %r13
	movq	%rbx, 24(%rax)
	movq	%rbx, 16(%rax)
	movq	%rbx, (%rax)
	je	.L110
	movq	%r13, 32(%rax)
	movq	%rbp, 40(%rax)
	movq	%r13, 8(%rax)
.L111:
	movq	$0, 224(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	(%rsi,%rdx,4), %rbp
	cmpq	%rbp, %rsi
	jb	.L108
	movq	%rsi, %rbp
	notq	%rbp
	andq	$-4, %rbp
	addq	%rsi, %rbp
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rbx, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	%rbp, 8(%rax)
	jmp	.L111
	.size	_IO_wstr_init_static, .-_IO_wstr_init_static
	.p2align 4,,15
	.globl	_IO_wstr_count
	.type	_IO_wstr_count, @function
_IO_wstr_count:
	movq	160(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, 8(%rdx)
	cmovnb	8(%rdx), %rax
	subq	16(%rdx), %rax
	sarq	$2, %rax
	ret
	.size	_IO_wstr_count, .-_IO_wstr_count
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11554, @object
	.size	__PRETTY_FUNCTION__.11554, 16
__PRETTY_FUNCTION__.11554:
	.string	"enlarge_userbuf"
	.hidden	_IO_wstr_jumps
	.globl	_IO_wstr_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_wstr_jumps, @object
	.size	_IO_wstr_jumps, 168
_IO_wstr_jumps:
	.quad	0
	.quad	0
	.quad	_IO_wstr_finish
	.quad	_IO_wstr_overflow
	.quad	_IO_wstr_underflow
	.quad	_IO_wdefault_uflow
	.quad	_IO_wstr_pbackfail
	.quad	_IO_wdefault_xsputn
	.quad	_IO_wdefault_xsgetn
	.quad	_IO_wstr_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_wdefault_doallocate
	.hidden	_IO_wdefault_xsgetn
	.hidden	_IO_wdefault_xsputn
	.hidden	_IO_wdefault_uflow
	.hidden	_IO_wdefault_finish
	.hidden	_IO_wdefault_pbackfail
	.hidden	__assert_fail
	.hidden	_IO_wsetb
	.hidden	__wmemset
	.hidden	__wmemcpy
