	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __old_fmemopen,fmemopen@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	fmemopen_seek, @function
fmemopen_seek:
	cmpl	$1, %edx
	je	.L3
	cmpl	$2, %edx
	je	.L4
	testl	%edx, %edx
	je	.L14
.L11:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rsi), %rax
.L6:
	testq	%rax, %rax
	js	.L11
	cmpq	%rax, 16(%rdi)
	jb	.L11
	movq	%rax, 24(%rdi)
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jne	.L15
	movq	32(%rdi), %rax
.L8:
	subq	(%rsi), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rsi), %rax
	addq	24(%rdi), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	movq	16(%rdi), %rax
	jmp	.L8
	.size	fmemopen_seek, .-fmemopen_seek
	.p2align 4,,15
	.type	fmemopen_close, @function
fmemopen_close:
	pushq	%rbx
	movl	8(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L17
	movq	(%rdi), %rdi
	call	free@PLT
.L17:
	movq	%rbx, %rdi
	call	free@PLT
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	fmemopen_close, .-fmemopen_close
	.p2align 4,,15
	.type	fmemopen_write, @function
fmemopen_write:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	12(%rdi), %eax
	movq	%rdx, %rbx
	testl	%eax, %eax
	jne	.L27
	testq	%rdx, %rdx
	jne	.L34
	movl	$1, %ecx
	movl	$1, %r12d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
.L20:
	movq	24(%rbp), %rdi
	movq	16(%rbp), %rax
	leaq	(%rdi,%rbx), %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rdx
	jbe	.L21
	movslq	%r12d, %rdx
	addq	%rdi, %rdx
	cmpq	%rdx, %rax
	jbe	.L35
	subq	%rdi, %rax
	subq	%rcx, %rax
	movq	%rax, %rbx
.L21:
	addq	0(%rbp), %rdi
	movq	%rbx, %rdx
	call	__GI_memcpy@PLT
	movq	24(%rbp), %rax
	addq	%rbx, %rax
	cmpq	32(%rbp), %rax
	movq	%rax, 24(%rbp)
	ja	.L36
.L25:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%r12d, %r12d
	movq	%rax, 32(%rbp)
	je	.L25
	movq	0(%rbp), %rdx
	movb	$0, (%rdx,%rax)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	cmpb	$1, -1(%rsi,%rdx)
	sbbq	%rcx, %rcx
	xorl	%r12d, %r12d
	addq	$1, %rcx
	cmpb	$0, -1(%rsi,%rdx)
	setne	%r12b
	jmp	.L20
	.size	fmemopen_write, .-fmemopen_write
	.p2align 4,,15
	.type	fmemopen_read, @function
fmemopen_read:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	24(%rbp), %rsi
	movq	16(%rbp), %rdx
	leaq	(%rsi,%rbx), %rax
	cmpq	%rdx, %rax
	jbe	.L38
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	je	.L37
	movq	%rdx, %rbx
	subq	%rsi, %rbx
.L38:
	addq	0(%rbp), %rsi
	movq	%rbx, %rdx
	call	__GI_memcpy@PLT
	movq	24(%rbp), %rax
	addq	%rbx, %rax
	cmpq	32(%rbp), %rax
	movq	%rax, 24(%rbp)
	jbe	.L40
	movq	%rax, 32(%rbp)
.L40:
	movq	%rbx, %rax
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fmemopen_read, .-fmemopen_read
	.p2align 4,,15
	.globl	__old_fmemopen
	.type	__old_fmemopen, @function
__old_fmemopen:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$48, %rsp
	testq	%rsi, %rsi
	je	.L45
	movq	%rdi, %rbp
	movl	$40, %edi
	movq	%rdx, %r13
	movq	%rsi, %r12
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L56
	xorl	%eax, %eax
	testq	%rbp, %rbp
	sete	%al
	movl	%eax, 8(%rbx)
	je	.L60
	movq	%rbp, %rax
	negq	%rax
	cmpq	%r12, %rax
	jb	.L61
	movzbl	0(%r13), %r14d
	movq	%rbp, (%rbx)
	cmpb	$119, %r14b
	je	.L62
.L51:
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI_strnlen
	movq	%rax, 32(%rbx)
.L49:
	cmpb	$97, %r14b
	movq	%r12, 16(%rbx)
	je	.L63
	xorl	%eax, %eax
	testb	%r14b, %r14b
	movq	$0, 24(%rbx)
	jne	.L53
.L54:
	movl	%eax, 12(%rbx)
	leaq	fmemopen_read(%rip), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, 16(%rsp)
	leaq	fmemopen_write(%rip), %rax
	movq	%rax, 24(%rsp)
	leaq	fmemopen_seek(%rip), %rax
	movq	%rax, 32(%rsp)
	leaq	fmemopen_close(%rip), %rax
	movq	%rax, 40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	_IO_fopencookie@PLT
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L64
.L43:
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movq	32(%rbx), %rax
	movq	%rax, 24(%rbx)
.L53:
	xorl	%eax, %eax
	cmpb	$98, 1(%r13)
	sete	%al
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L65
	movb	$0, (%rax)
	movq	$0, 32(%rbx)
	movzbl	0(%r13), %r14d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L62:
	movb	$0, 0(%rbp)
	movzbl	0(%r13), %r14d
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rbx, %rdi
	call	free@PLT
.L45:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$48, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jne	.L66
.L55:
	movq	%rbx, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%rbx), %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	jmp	.L55
.L65:
	movq	%rbx, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L43
	.size	__old_fmemopen, .-__old_fmemopen
