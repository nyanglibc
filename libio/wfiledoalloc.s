	.text
	.p2align 4,,15
	.globl	_IO_wfile_doallocate
	.type	_IO_wfile_doallocate, @function
_IO_wfile_doallocate:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L11
.L2:
	movq	64(%rbp), %rdx
	subq	%rax, %rdx
	leaq	3(%rdx), %rax
	shrq	$2, %rax
	testb	$1, 0(%rbp)
	cmovne	%rax, %rdx
	leaq	0(,%rdx,4), %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	movl	$-1, %eax
	testq	%rsi, %rsi
	je	.L1
	leaq	(%rsi,%rbx), %rdx
	movl	$1, %ecx
	movq	%rbp, %rdi
	call	_IO_wsetb
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	call	_IO_file_doallocate
	movq	56(%rbp), %rax
	jmp	.L2
	.size	_IO_wfile_doallocate, .-_IO_wfile_doallocate
	.hidden	_IO_file_doallocate
	.hidden	_IO_wsetb
