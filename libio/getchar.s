	.text
	.p2align 4,,15
	.globl	getchar
	.type	getchar, @function
getchar:
.LFB68:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	stdin(%rip), %rbx
	testb	$-128, 116(%rbx)
	jne	.L2
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jnb	.L22
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movzbl	(%rax), %r8d
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	(%rbx), %edx
	andl	$32768, %edx
	je	.L23
	movq	%rbx, %rdi
.L5:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L24
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %r8d
.L12:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L13
	subl	$1, (%rdi)
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	136(%rbx), %rcx
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rcx)
	je	.L18
#APP
# 38 "getchar.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%rbx), %rcx
	movq	stdin(%rip), %rdi
	movq	%rbp, 8(%rcx)
.L6:
	addl	$1, 4(%rcx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rbx, %rdi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
.LEHB0:
	jmp	__uflow
.LEHE0:
	.p2align 4,,10
	.p2align 3
.L24:
.LEHB1:
	call	__uflow
	movl	%eax, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L8
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
.LEHE1:
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L19:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L15
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L15
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L16
	subl	$1, (%rdi)
.L15:
	movq	%r8, %rdi
.LEHB2:
	call	_Unwind_Resume@PLT
.LEHE2:
.L16:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L19-.LFB68
	.uleb128 0
	.uleb128 .LEHB2-.LFB68
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	getchar, .-getchar
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__uflow
