	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vwprintf
	.type	__vwprintf, @function
__vwprintf:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfwprintf_internal
	.size	__vwprintf, .-__vwprintf
	.globl	vwprintf
	.set	vwprintf,__vwprintf
	.hidden	__vfwprintf_internal
