	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__putc_unlocked
	.hidden	__putc_unlocked
	.type	__putc_unlocked, @function
__putc_unlocked:
	movq	40(%rsi), %rcx
	cmpq	48(%rsi), %rcx
	movq	%rsi, %rdx
	movzbl	%dil, %eax
	jnb	.L4
	leaq	1(%rcx), %rsi
	movq	%rsi, 40(%rdx)
	movb	%dil, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %esi
	movq	%rdx, %rdi
	jmp	__GI___overflow
	.size	__putc_unlocked, .-__putc_unlocked
	.weak	__GI_putc_unlocked
	.hidden	__GI_putc_unlocked
	.set	__GI_putc_unlocked,__putc_unlocked
	.weak	putc_unlocked
	.set	putc_unlocked,__GI_putc_unlocked
