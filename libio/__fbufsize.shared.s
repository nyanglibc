	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fbufsize
	.type	__fbufsize, @function
__fbufsize:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L2
	movq	160(%rdi), %rdx
	movq	56(%rdx), %rax
	subq	48(%rdx), %rax
	sarq	$2, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	64(%rdi), %rax
	subq	56(%rdi), %rax
	ret
	.size	__fbufsize, .-__fbufsize
