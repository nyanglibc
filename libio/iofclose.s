	.text
	.p2align 4,,15
	.globl	_IO_new_fclose
	.type	_IO_new_fclose, @function
_IO_new_fclose:
.LFB77:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	testb	$32, %ah
	jne	.L36
	movl	%eax, %edx
	andl	$32768, %edx
	je	.L27
.L28:
	sall	$26, %eax
	sarl	$31, %eax
	movl	%eax, %ebp
.L10:
	testl	%edx, %edx
	jne	.L12
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	je	.L37
.L12:
	movq	216(%rbx), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L38
.L15:
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	*16(%r12)
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L16
	movq	152(%rbx), %r12
#APP
# 64 "iofclose.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L17
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
.L18:
	movq	(%r12), %rdi
	call	__gconv_release_step
	movq	56(%r12), %rdi
	call	__gconv_release_step
.LEHE0:
#APP
# 67 "iofclose.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	subl	$1, __gconv_lock(%rip)
.L20:
	leaq	_IO_2_1_stdout_(%rip), %rax
	cmpq	%rax, %rbx
	leaq	_IO_2_1_stdin_(%rip), %rax
	sete	%dl
	cmpq	%rax, %rbx
	sete	%al
	orb	%al, %dl
	jne	.L1
	leaq	_IO_2_1_stderr_(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L39
.L1:
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	call	_IO_un_link
	movl	(%rbx), %eax
	testb	$-128, %ah
	jne	.L3
.L27:
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L4
#APP
# 51 "iofclose.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L6:
	movq	136(%rbx), %rdi
	movl	(%rbx), %eax
	movq	%rbp, 8(%rdi)
.L4:
	addl	$1, 4(%rdi)
.L3:
	movl	%eax, %edx
	andl	$32768, %edx
	testb	$32, %ah
	je	.L28
	movq	%rbx, %rdi
.LEHB1:
	call	_IO_file_close_it
.LEHE1:
	movl	(%rbx), %edx
	movl	%eax, %ebp
	andl	$32768, %edx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rbx, %rdi
	call	free@PLT
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	subl	$1, (%rdi)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	$0, 72(%rbx)
	je	.L20
	movq	%rbx, %rdi
	call	_IO_free_backup_area
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
.LEHB2:
	call	_IO_vtable_check
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
#APP
# 67 "iofclose.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L20
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__gconv_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 67 "iofclose.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __gconv_lock(%rip)
	je	.L18
	leaq	__gconv_lock(%rip), %rdi
	call	__lll_lock_wait_private
.LEHE2:
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L6
.LEHB3:
	call	__lll_lock_wait_private
.LEHE3:
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 885 "libioP.h" 1
	xchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
.L29:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L25
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L25
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L26
	subl	$1, (%rdi)
.L25:
	movq	%r8, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
.L26:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L25
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L25
.LFE77:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA77:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE77-.LLSDACSB77
.LLSDACSB77:
	.uleb128 .LEHB0-.LFB77
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB77
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L29-.LFB77
	.uleb128 0
	.uleb128 .LEHB2-.LFB77
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB77
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L29-.LFB77
	.uleb128 0
	.uleb128 .LEHB4-.LFB77
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE77:
	.text
	.size	_IO_new_fclose, .-_IO_new_fclose
	.globl	__new_fclose
	.set	__new_fclose,_IO_new_fclose
	.weak	fclose
	.set	fclose,__new_fclose
	.weak	_IO_fclose
	.set	_IO_fclose,_IO_new_fclose
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	_IO_free_backup_area
	.hidden	_IO_file_close_it
	.hidden	_IO_un_link
	.hidden	__gconv_release_step
	.hidden	__gconv_lock
