	.text
	.p2align 4,,15
	.globl	putwchar_unlocked
	.type	putwchar_unlocked, @function
putwchar_unlocked:
	movl	%edi, %eax
	movq	stdout(%rip), %rdi
	movq	160(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L2
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %esi
	jmp	__woverflow
	.size	putwchar_unlocked, .-putwchar_unlocked
	.hidden	__woverflow
