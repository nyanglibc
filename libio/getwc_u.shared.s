	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getwc_unlocked
	.type	__getwc_unlocked, @function
__getwc_unlocked:
	movq	160(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L2
	leaq	4(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	jmp	__GI___wuflow
	.size	__getwc_unlocked, .-__getwc_unlocked
	.weak	fgetwc_unlocked
	.set	fgetwc_unlocked,__getwc_unlocked
	.weak	getwc_unlocked
	.set	getwc_unlocked,__getwc_unlocked
