	.text
	.p2align 4,,15
	.globl	__fsetlocking
	.hidden	__fsetlocking
	.type	__fsetlocking, @function
__fsetlocking:
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testb	$-128, %dh
	setne	%al
	addl	$1, %eax
	testl	%esi, %esi
	je	.L1
	andb	$127, %dh
	movl	%edx, %ecx
	orb	$-128, %ch
	cmpl	$2, %esi
	cmove	%ecx, %edx
	movl	%edx, (%rdi)
.L1:
	rep ret
	.size	__fsetlocking, .-__fsetlocking
