	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__IO_padn
	.hidden	__GI__IO_padn
	.type	__GI__IO_padn, @function
__GI__IO_padn:
	pushq	%r15
	pushq	%r14
	leaq	blanks(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$40, %rsp
	cmpl	$32, %esi
	je	.L2
	cmpl	$48, %esi
	leaq	zeroes(%rip), %r14
	je	.L2
	leaq	16(%rsp), %r14
	leaq	15(%rsp), %rcx
	leaq	15(%r14), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movb	%sil, (%rax)
	subq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L3
.L2:
	cmpl	$15, %edx
	movl	%edx, %r12d
	jle	.L13
	leaq	__start___libc_IO_vtables(%rip), %r13
	leaq	__stop___libc_IO_vtables(%rip), %r15
	movl	%edx, %eax
	andl	$15, %eax
	xorl	%ebx, %ebx
	subq	%r13, %r15
	movl	%eax, 4(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	*56(%rax)
	addq	%rax, %rbx
	cmpq	$16, %rax
	jne	.L1
	subl	$16, %r12d
	cmpl	4(%rsp), %r12d
	je	.L4
.L8:
	movq	216(%rbp), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	%rdx, %r15
	ja	.L5
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r12d, %r12d
	jle	.L1
	movq	216(%rbp), %r13
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L20
.L10:
	movslq	%r12d, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	*56(%r13)
	addq	%rax, %rbx
.L1:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L20:
	call	_IO_vtable_check
	jmp	.L10
	.size	__GI__IO_padn, .-__GI__IO_padn
	.globl	_IO_padn
	.set	_IO_padn,__GI__IO_padn
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	zeroes, @object
	.size	zeroes, 16
zeroes:
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.align 16
	.type	blanks, @object
	.size	blanks, 16
blanks:
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
