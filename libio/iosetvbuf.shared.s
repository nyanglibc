	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__IO_setvbuf
	.hidden	__GI__IO_setvbuf
	.type	__GI__IO_setvbuf, @function
__GI__IO_setvbuf:
.LFB68:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r12
	pushq	%rbx
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	movl	%edx, %ecx
	andl	$32768, %ecx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %r14
	cmpq	%r14, 8(%rdi)
	je	.L3
#APP
# 38 "iosetvbuf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L4
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movl	(%rbx), %edx
	movq	%r14, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
	cmpl	$1, %r13d
	je	.L9
	cmpl	$2, %r13d
	je	.L19
	testl	%r13d, %r13d
	je	.L11
.L29:
	andb	$-128, %dh
	movl	$-1, %r8d
	je	.L53
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1, %r13d
	je	.L9
	cmpl	$2, %r13d
	je	.L19
	testl	%r13d, %r13d
	movl	$-1, %r8d
	jne	.L1
.L11:
	andl	$-515, %edx
	testq	%rbp, %rbp
	movl	%edx, (%rbx)
	je	.L54
.L14:
	movq	216(%rbx), %r13
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r13, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L20
.L22:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	*88(%r13)
	xorl	%r8d, %r8d
	movl	(%rbx), %edx
	testq	%rax, %rax
	sete	%r8b
	negl	%r8d
.L12:
	andb	$-128, %dh
	jne	.L1
.L53:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L24
	subl	$1, (%rdi)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	andl	$-3, %edx
	orb	$2, %dh
	testq	%rbp, %rbp
	movl	%edx, (%rbx)
	jne	.L14
	xorl	%r8d, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	andb	$-3, %dh
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	orl	$2, %edx
	movl	%edx, (%rbx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%r8d, %r8d
	cmpq	$0, 56(%rbx)
	jne	.L12
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L15
.L17:
	movq	%rbx, %rdi
	call	*104(%rbp)
	testl	%eax, %eax
	movl	(%rbx), %edx
	js	.L29
	andb	$-3, %dh
	xorl	%r8d, %r8d
	movl	%edx, (%rbx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	call	_IO_vtable_check
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L24:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L15:
	call	_IO_vtable_check
.LEHE0:
	jmp	.L17
.L32:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L26
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L26
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L27
	subl	$1, (%rdi)
.L26:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L27:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L26
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L26
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L32-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__GI__IO_setvbuf, .-__GI__IO_setvbuf
	.globl	_IO_setvbuf
	.set	_IO_setvbuf,__GI__IO_setvbuf
	.weak	setvbuf
	.set	setvbuf,_IO_setvbuf
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
