	.text
	.p2align 4,,15
	.globl	__vasprintf_internal
	.hidden	__vasprintf_internal
	.type	__vasprintf_internal, @function
__vasprintf_internal:
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	$100, %edi
	movq	%rsi, %r13
	movq	%rdx, %r14
	subq	$248, %rsp
	call	malloc@PLT
	testq	%rax, %rax
	je	.L8
	movq	%rsp, %rbp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movl	$-1, %edx
	movl	$32768, %esi
	movq	%rax, %rbx
	movq	$0, 136(%rsp)
	call	_IO_no_init@PLT
	leaq	_IO_str_jumps(%rip), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	movl	$100, %edx
	movq	%rbx, %rsi
	movq	%rax, 216(%rsp)
	call	_IO_str_init_static_internal@PLT
	movq	malloc@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	andl	$-2, (%rsp)
	movq	%rax, 224(%rsp)
	movq	free@GOTPCREL(%rip), %rax
	movq	%rax, 232(%rsp)
	call	__vfprintf_internal
	testl	%eax, %eax
	movl	%eax, %ebp
	js	.L11
	movq	32(%rsp), %rax
	movq	40(%rsp), %rbx
	movq	48(%rsp), %rcx
	movq	56(%rsp), %r14
	subq	%rax, %rbx
	subq	%rax, %rcx
	leaq	1(%rbx), %r13
	movq	%rcx, %rax
	shrq	%rax
	cmpq	%r13, %rax
	ja	.L4
.L6:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L12
.L7:
	movb	$0, (%rax,%rbx)
.L1:
	addq	$248, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L6
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r14, %rdi
	call	free@PLT
	movq	(%r12), %rax
	testq	%rax, %rax
	jne	.L7
.L12:
	movq	56(%rsp), %rax
	movq	%rax, (%r12)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	movq	56(%rsp), %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$-1, %ebp
	jmp	.L1
	.size	__vasprintf_internal, .-__vasprintf_internal
	.p2align 4,,15
	.globl	__vasprintf
	.type	__vasprintf, @function
__vasprintf:
	xorl	%ecx, %ecx
	jmp	__vasprintf_internal
	.size	__vasprintf, .-__vasprintf
	.weak	vasprintf
	.set	vasprintf,__vasprintf
	.hidden	__vfprintf_internal
	.hidden	_IO_str_jumps
