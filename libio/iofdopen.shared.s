	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _IO_new_fdopen,_IO_fdopen@@GLIBC_2.2.5
	.symver __new_fdopen,fdopen@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_IO_new_fdopen
	.type	_IO_new_fdopen, @function
_IO_new_fdopen:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edi, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movzbl	(%rsi), %eax
	cmpb	$114, %al
	je	.L3
	cmpb	$119, %al
	je	.L23
	cmpb	$97, %al
	movl	$4100, %ebp
	je	.L4
.L14:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L45:
	xorl	%r15d, %r15d
.L1:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$4, %ebp
.L4:
	leaq	4(%rsi), %rdx
	xorl	%r13d, %r13d
.L11:
	addq	$1, %rsi
	movzbl	(%rsi), %eax
	cmpb	$43, %al
	je	.L8
	cmpb	$109, %al
	je	.L9
	testb	%al, %al
	je	.L10
	cmpq	%rdx, %rsi
	jne	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$3, %esi
	movl	%r12d, %edi
	call	__GI___fcntl
	cmpl	$-1, %eax
	je	.L45
	movl	%eax, %edx
	andl	$3, %edx
	je	.L47
	cmpl	$1, %edx
	jne	.L15
	testb	$4, %bpl
	je	.L14
.L15:
	xorl	%r14d, %r14d
	testl	$4096, %ebp
	je	.L16
	testb	$4, %ah
	je	.L48
.L16:
	movl	$472, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L45
	leaq	224(%rax), %rax
	testl	%r13d, %r13d
	movq	%rbx, %r15
	leaq	240(%rbx), %rcx
	movq	%rax, 136(%rbx)
	je	.L18
	testb	$8, %bpl
	jne	.L19
.L18:
	leaq	__GI__IO_wfile_jumps(%rip), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	leaq	__GI__IO_file_jumps(%rip), %rax
.L22:
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	call	_IO_new_file_init_internal
	movl	(%rbx), %eax
	movl	%r12d, 112(%rbx)
	andl	$-4173, %eax
	orl	%ebp, %eax
	testb	%r14b, %r14b
	movl	%eax, (%rbx)
	je	.L1
	andl	$4100, %ebp
	cmpl	$4100, %ebp
	jne	.L1
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L49
.L21:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	jne	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L1
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L47:
	testb	$8, %bpl
	jne	.L15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%rdx, %rsi
	movl	$1, %r13d
	jne	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	andl	$4096, %ebp
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$8, %ebp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	_IO_wfile_jumps_maybe_mmap(%rip), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	leaq	_IO_file_jumps_maybe_mmap(%rip), %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L48:
	orb	$4, %ah
	movl	$4, %esi
	movl	%r12d, %edi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	__GI___fcntl
	cmpl	$-1, %eax
	je	.L45
	movl	$1, %r14d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L49:
	call	_IO_vtable_check
	jmp	.L21
	.size	_IO_new_fdopen, .-_IO_new_fdopen
	.globl	__new_fdopen
	.set	__new_fdopen,_IO_new_fdopen
	.globl	__GI__IO_fdopen
	.set	__GI__IO_fdopen,_IO_new_fdopen
	.hidden	_IO_vtable_check
	.hidden	_IO_file_jumps_maybe_mmap
	.hidden	_IO_wfile_jumps_maybe_mmap
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
	.hidden	_IO_new_file_init_internal
