	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__IO_ftell
	.hidden	__GI__IO_ftell
	.type	__GI__IO_ftell, @function
__GI__IO_ftell:
.LFB68:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L3
#APP
# 37 "ioftell.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekoff_unlocked
	movq	%rax, %r8
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$32768, %edx
	testb	$1, %ah
	je	.L8
	cmpq	$-1, %r8
	je	.L9
	movl	192(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L32
	testl	%edx, %edx
	jne	.L1
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	je	.L18
.L1:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	88(%rbx), %rax
	subq	72(%rbx), %rax
	subq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%edx, %edx
	jne	.L11
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	jne	.L11
.L18:
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, (%rdi)
.L11:
	cmpq	$-1, %r8
	jne	.L1
.L17:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r8
	movl	%fs:(%rax), %edx
	testl	%edx, %edx
	jne	.L1
	movl	$5, %fs:(%rax)
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%edx, %edx
	jne	.L17
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	je	.L18
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L11
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L11
.L20:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L15
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L15
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L16
	subl	$1, (%rdi)
.L15:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L16:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L20-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__GI__IO_ftell, .-__GI__IO_ftell
	.globl	_IO_ftell
	.set	_IO_ftell,__GI__IO_ftell
	.weak	ftell
	.set	ftell,_IO_ftell
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_seekoff_unlocked
