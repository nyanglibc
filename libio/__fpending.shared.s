	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fpending
	.type	__fpending, @function
__fpending:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L2
	movq	160(%rdi), %rdx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	sarq	$2, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	40(%rdi), %rax
	subq	32(%rdi), %rax
	ret
	.size	__fpending, .-__fpending
