	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	buffer_free, @function
buffer_free:
.LFB105:
	pushq	%rbx
	movq	freeres_list(%rip), %rbx
	movb	$1, dealloc_buffers(%rip)
	testq	%rbx, %rbx
	je	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	176(%rbx), %rdi
	call	free@PLT
	movq	168(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rbx, freeres_list(%rip)
	jne	.L3
.L1:
	popq	%rbx
	ret
.LFE105:
	.size	buffer_free, .-buffer_free
	.text
	.p2align 4,,15
	.type	flush_cleanup, @function
flush_cleanup:
.LFB68:
	movq	run_fp(%rip), %rax
	testq	%rax, %rax
	je	.L12
	testl	$32768, (%rax)
	je	.L19
.L12:
	movl	4+list_all_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L10
	movq	$0, 8+list_all_lock(%rip)
#APP
# 47 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, list_all_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 47 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L10
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 47 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L10:
	rep ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	136(%rax), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L12
	movq	$0, 8(%rdi)
#APP
# 46 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L14
	subl	$1, (%rdi)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 46 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 46 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
.LFE68:
	.size	flush_cleanup, .-flush_cleanup
	.p2align 4,,15
	.type	__GI__IO_un_link.part.1, @function
__GI__IO_un_link.part.1:
.LFB128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	__libc_pthread_functions_init(%rip), %ebp
	testl	%ebp, %ebp
	je	.L21
	movq	184+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%edx, %edx
#APP
# 58 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	flush_cleanup(%rip), %rsi
	call	*%rax
.L22:
	movq	%fs:16, %r12
	cmpq	8+list_all_lock(%rip), %r12
	je	.L23
#APP
# 59 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L24
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L25:
	movq	%r12, 8+list_all_lock(%rip)
.L23:
	movl	(%rbx), %esi
	movl	4+list_all_lock(%rip), %edx
	movq	%rbx, run_fp(%rip)
	movl	%esi, %r8d
	addl	$1, %edx
	andl	$32768, %r8d
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L26
	movq	136(%rbx), %rdi
	movq	%fs:16, %r12
	cmpq	8(%rdi), %r12
	je	.L27
#APP
# 61 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L28
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L29:
	movl	(%rbx), %esi
	movq	136(%rbx), %rdi
	movl	%esi, %r8d
	movq	%r12, 8(%rdi)
	andl	$32768, %r8d
.L27:
	addl	$1, 4(%rdi)
	movq	__GI__IO_list_all(%rip), %rdi
	testq	%rdi, %rdi
	je	.L30
.L46:
	cmpq	%rdi, %rbx
	je	.L61
	movq	104(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L30
	cmpq	%rcx, %rbx
	jne	.L33
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%rdx, %rbx
	je	.L63
	movq	%rdx, %rcx
.L33:
	movq	104(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L35
.L30:
	andb	$127, %sil
	testl	%r8d, %r8d
	movl	%esi, (%rbx)
	jne	.L60
.L36:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	leal	-1(%rax), %ecx
	testl	%ecx, %ecx
	movl	%ecx, 4(%rdi)
	movl	4+list_all_lock(%rip), %edx
	jne	.L37
	movq	$0, 8(%rdi)
#APP
# 76 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L39
	subl	$1, (%rdi)
	movl	4+list_all_lock(%rip), %edx
.L37:
	subl	$1, %edx
	movq	$0, run_fp(%rip)
	testl	%edx, %edx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L41
	movq	$0, 8+list_all_lock(%rip)
#APP
# 78 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L42
	subl	$1, list_all_lock(%rip)
.L41:
	testl	%ebp, %ebp
	je	.L20
	movq	192+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%esi, %esi
#APP
# 79 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L20:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	__GI__IO_list_all(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L46
	andb	$127, %sil
	movl	%esi, (%rbx)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L61:
	movq	104(%rbx), %rax
	andb	$127, %sil
	testl	%r8d, %r8d
	movq	%rax, __GI__IO_list_all(%rip)
	movl	%esi, (%rbx)
	je	.L36
.L60:
	movl	4+list_all_lock(%rip), %edx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$104, %rcx
.L34:
	movq	104(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L42:
#APP
# 78 "genops.c" 1
	xchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L41
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 78 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L25
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%r8d, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L29
	call	__lll_lock_wait_private
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	104(%rdi), %rcx
	jmp	.L34
.L39:
#APP
# 76 "genops.c" 1
	xchgl %r8d, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %r8d
	movl	4+list_all_lock(%rip), %edx
	jle	.L37
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 76 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L60
.LFE128:
	.size	__GI__IO_un_link.part.1, .-__GI__IO_un_link.part.1
	.p2align 4,,15
	.globl	__GI__IO_un_link
	.hidden	__GI__IO_un_link
	.type	__GI__IO_un_link, @function
__GI__IO_un_link:
.LFB69:
	testb	$-128, (%rdi)
	je	.L64
	jmp	__GI__IO_un_link.part.1
	.p2align 4,,10
	.p2align 3
.L64:
	rep ret
.LFE69:
	.size	__GI__IO_un_link, .-__GI__IO_un_link
	.globl	_IO_un_link
	.set	_IO_un_link,__GI__IO_un_link
	.p2align 4,,15
	.globl	__GI__IO_link_in
	.hidden	__GI__IO_link_in
	.type	__GI__IO_link_in, @function
__GI__IO_link_in:
.LFB70:
	movl	(%rdi), %eax
	testb	$-128, %al
	jne	.L95
	pushq	%r12
	pushq	%rbp
	orb	$-128, %al
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	__libc_pthread_functions_init(%rip), %ebp
	movl	%eax, (%rdi)
	testl	%ebp, %ebp
	je	.L68
	movq	184+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%edx, %edx
#APP
# 92 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	flush_cleanup(%rip), %rsi
	call	*%rax
.L69:
	movq	%fs:16, %r12
	cmpq	%r12, 8+list_all_lock(%rip)
	je	.L70
#APP
# 93 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L71
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L72:
	movq	%r12, 8+list_all_lock(%rip)
.L70:
	movl	4+list_all_lock(%rip), %edx
	movl	(%rbx), %ecx
	movq	%rbx, run_fp(%rip)
	addl	$1, %edx
	andl	$32768, %ecx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L73
	movq	136(%rbx), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L99
#APP
# 95 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L76
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L77:
	movl	(%rbx), %eax
	movq	136(%rbx), %rdi
	movq	__GI__IO_list_all(%rip), %rdx
	addl	$1, 4(%rdi)
	testb	$-128, %ah
	movq	%r12, 8(%rdi)
	movq	%rbx, __GI__IO_list_all(%rip)
	movq	%rdx, 104(%rbx)
	jne	.L98
.L75:
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	movl	4+list_all_lock(%rip), %edx
	jne	.L78
	movq	$0, 8(%rdi)
#APP
# 100 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L80
	subl	$1, (%rdi)
	movl	4+list_all_lock(%rip), %edx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L95:
	rep ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	__GI__IO_list_all(%rip), %rax
	movq	%rbx, __GI__IO_list_all(%rip)
	movq	%rax, 104(%rbx)
.L78:
	subl	$1, %edx
	movq	$0, run_fp(%rip)
	testl	%edx, %edx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L82
	movq	$0, 8+list_all_lock(%rip)
#APP
# 102 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L83
	subl	$1, list_all_lock(%rip)
.L82:
	testl	%ebp, %ebp
	je	.L66
	movq	192+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%esi, %esi
#APP
# 103 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L66:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	__GI__IO_list_all(%rip), %rax
	addl	$1, 4(%rdi)
	movq	%rbx, __GI__IO_list_all(%rip)
	movq	%rax, 104(%rbx)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L72
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L83:
#APP
# 102 "genops.c" 1
	xchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L82
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 102 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L76:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L77
	call	__lll_lock_wait_private
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
#APP
# 100 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	4+list_all_lock(%rip), %edx
	jle	.L78
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 100 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L98:
	movl	4+list_all_lock(%rip), %edx
	jmp	.L78
.LFE70:
	.size	__GI__IO_link_in, .-__GI__IO_link_in
	.globl	_IO_link_in
	.set	_IO_link_in,__GI__IO_link_in
	.p2align 4,,15
	.globl	_IO_least_marker
	.type	_IO_least_marker, @function
_IO_least_marker:
.LFB71:
	movq	96(%rdi), %rdx
	movq	%rsi, %rax
	subq	24(%rdi), %rax
	testq	%rdx, %rdx
	je	.L100
	.p2align 4,,10
	.p2align 3
.L102:
	movslq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
	testq	%rdx, %rdx
	jne	.L102
.L100:
	rep ret
.LFE71:
	.size	_IO_least_marker, .-_IO_least_marker
	.p2align 4,,15
	.type	save_for_backup, @function
save_for_backup:
.LFB77:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%r13, %rbx
	subq	$40, %rsp
	call	_IO_least_marker@PLT
	movq	24(%rbp), %r10
	movq	88(%rbp), %r14
	movq	%rax, %rcx
	movq	72(%rbp), %r8
	subq	%r10, %rbx
	movq	%rbx, %r12
	subq	%rax, %r12
	movq	%r14, %rax
	subq	%r8, %rax
	cmpq	%rax, %r12
	ja	.L124
	subq	%r12, %rax
	testq	%rcx, %rcx
	movq	%rax, %r15
	leaq	(%r8,%rax), %r9
	js	.L125
	testq	%r12, %r12
	jne	.L126
.L112:
	movq	96(%rbp), %rax
	movq	%r9, 80(%rbp)
	movl	%ebx, %edx
	testq	%rax, %rax
	je	.L116
	.p2align 4,,10
	.p2align 3
.L115:
	subl	%edx, 16(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L115
.L116:
	xorl	%eax, %eax
.L107:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	100(%r12), %rax
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r10, 8(%rsp)
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L117
	movq	24(%rsp), %rcx
	leaq	100(%rax), %r9
	movq	8(%rsp), %r10
	movq	16(%rsp), %r8
	testq	%rcx, %rcx
	js	.L127
	leaq	(%r10,%rcx), %rsi
	movq	%r9, %rdi
	movq	%r12, %rdx
	movq	%r8, 8(%rsp)
	call	__GI_memcpy@PLT
	movq	8(%rsp), %r8
	movq	%rax, %r9
.L111:
	movq	%r8, %rdi
	movq	%r9, 8(%rsp)
	movq	%r13, %rbx
	call	free@PLT
	movq	%r15, 72(%rbp)
	addq	(%rsp), %r15
	subq	24(%rbp), %rbx
	movq	8(%rsp), %r9
	movq	%r15, 88(%rbp)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	(%r10,%rcx), %rsi
	movq	%r9, %rdi
	movq	%r12, %rdx
	movq	%r13, %rbx
	call	__GI_memcpy@PLT
	movq	72(%rbp), %r9
	subq	24(%rbp), %rbx
	addq	%r15, %r9
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%rcx, %rdx
	leaq	(%r14,%rcx), %rsi
	movq	%r9, %rdi
	negq	%rdx
	movq	%rcx, (%rsp)
	movq	%r13, %rbx
	call	__GI_memmove
	movq	(%rsp), %rcx
	movq	%r15, %rdi
	movq	24(%rbp), %rsi
	movq	%r13, %rdx
	subq	%rcx, %rdi
	addq	72(%rbp), %rdi
	subq	%rsi, %rdx
	call	__GI_memcpy@PLT
	addq	72(%rbp), %r15
	subq	24(%rbp), %rbx
	movq	%r15, %r9
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rcx, %rdx
	leaq	(%r14,%rcx), %rsi
	movq	%r9, %rdi
	negq	%rdx
	movq	%r8, 24(%rsp)
	movq	%r9, 8(%rsp)
	movq	%r10, 16(%rsp)
	call	__GI_mempcpy@PLT
	movq	16(%rsp), %r10
	movq	%rbx, %rdx
	movq	%rax, %rdi
	movq	%r10, %rsi
	call	__GI_memcpy@PLT
	movq	8(%rsp), %r9
	movq	24(%rsp), %r8
	jmp	.L111
.L117:
	movl	$-1, %eax
	jmp	.L107
.LFE77:
	.size	save_for_backup, .-save_for_backup
	.p2align 4,,15
	.globl	_IO_switch_to_main_get_area
	.type	_IO_switch_to_main_get_area, @function
_IO_switch_to_main_get_area:
.LFB72:
	movq	16(%rdi), %rax
	movq	88(%rdi), %rdx
	andl	$-257, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rax, 88(%rdi)
	movq	24(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdx, 72(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rax, 8(%rdi)
	ret
.LFE72:
	.size	_IO_switch_to_main_get_area, .-_IO_switch_to_main_get_area
	.p2align 4,,15
	.globl	_IO_switch_to_backup_area
	.type	_IO_switch_to_backup_area, @function
_IO_switch_to_backup_area:
.LFB73:
	movq	16(%rdi), %rdx
	movq	88(%rdi), %rax
	movq	72(%rdi), %rcx
	orl	$256, (%rdi)
	movq	%rdx, 88(%rdi)
	movq	24(%rdi), %rdx
	movq	%rax, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%rdx, 72(%rdi)
	movq	%rax, 8(%rdi)
	ret
.LFE73:
	.size	_IO_switch_to_backup_area, .-_IO_switch_to_backup_area
	.p2align 4,,15
	.globl	__GI__IO_switch_to_get_mode
	.hidden	__GI__IO_switch_to_get_mode
	.type	__GI__IO_switch_to_get_mode, @function
__GI__IO_switch_to_get_mode:
.LFB74:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rax
	cmpq	32(%rdi), %rax
	jbe	.L131
	movq	216(%rdi), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L140
.L132:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%rbp)
	cmpl	$-1, %eax
	je	.L130
	movq	40(%rbx), %rax
.L131:
	movl	(%rbx), %edx
	testb	$1, %dh
	jne	.L141
	cmpq	%rax, 16(%rbx)
	movq	56(%rbx), %rcx
	movq	%rcx, 24(%rbx)
	jnb	.L135
	movq	%rax, 16(%rbx)
.L135:
	andb	$-9, %dh
	movq	%rax, 8(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, 32(%rbx)
	movl	%edx, (%rbx)
	xorl	%eax, %eax
.L130:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movq	80(%rbx), %rcx
	movq	%rcx, 24(%rbx)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L140:
	call	_IO_vtable_check
	jmp	.L132
.LFE74:
	.size	__GI__IO_switch_to_get_mode, .-__GI__IO_switch_to_get_mode
	.globl	_IO_switch_to_get_mode
	.set	_IO_switch_to_get_mode,__GI__IO_switch_to_get_mode
	.p2align 4,,15
	.globl	__GI__IO_free_backup_area
	.hidden	__GI__IO_free_backup_area
	.type	__GI__IO_free_backup_area, @function
__GI__IO_free_backup_area:
.LFB75:
	pushq	%rbx
	testl	$256, (%rdi)
	movq	%rdi, %rbx
	je	.L143
	call	_IO_switch_to_main_get_area@PLT
.L143:
	movq	72(%rbx), %rdi
	call	free@PLT
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	popq	%rbx
	ret
.LFE75:
	.size	__GI__IO_free_backup_area, .-__GI__IO_free_backup_area
	.globl	_IO_free_backup_area
	.set	_IO_free_backup_area,__GI__IO_free_backup_area
	.p2align 4,,15
	.globl	__GI___overflow
	.hidden	__GI___overflow
	.type	__GI___overflow, @function
__GI___overflow:
.LFB76:
	pushq	%rbx
	subq	$16, %rsp
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L149
	movl	$-1, 192(%rdi)
.L149:
	movq	216(%rdi), %rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L152
	movq	24(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%esi, 12(%rsp)
	movq	%rdi, (%rsp)
	call	_IO_vtable_check
	movq	24(%rbx), %rax
	movl	12(%rsp), %esi
	movq	(%rsp), %rdi
	addq	$16, %rsp
	popq	%rbx
	jmp	*%rax
.LFE76:
	.size	__GI___overflow, .-__GI___overflow
	.globl	__overflow
	.set	__overflow,__GI___overflow
	.p2align 4,,15
	.globl	__GI___underflow
	.hidden	__GI___underflow
	.type	__GI___underflow, @function
__GI___underflow:
.LFB78:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L154
	movl	$-1, 192(%rdi)
.L155:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	$2048, (%rdi)
	jne	.L157
.L161:
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jb	.L180
	testl	$256, (%rbx)
	jne	.L181
.L162:
	cmpq	$0, 96(%rbx)
	je	.L163
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L160
.L164:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L182
.L166:
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%rbx, %rdi
	call	_IO_switch_to_main_get_area@PLT
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jnb	.L162
	.p2align 4,,10
	.p2align 3
.L180:
	movzbl	(%rax), %eax
.L153:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	cmpl	$-1, %eax
	je	.L155
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	call	__GI__IO_switch_to_get_mode
	cmpl	$-1, %eax
	jne	.L161
.L160:
	movl	$-1, %eax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L163:
	cmpq	$0, 72(%rbx)
	je	.L164
	movq	%rbx, %rdi
	call	__GI__IO_free_backup_area
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L182:
	call	_IO_vtable_check
	jmp	.L166
.LFE78:
	.size	__GI___underflow, .-__GI___underflow
	.globl	__underflow
	.set	__underflow,__GI___underflow
	.p2align 4,,15
	.globl	__GI___uflow
	.hidden	__GI___uflow
	.type	__GI___uflow, @function
__GI___uflow:
.LFB79:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L184
	movl	$-1, 192(%rdi)
.L185:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	$2048, (%rdi)
	jne	.L187
.L191:
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jb	.L210
	testl	$256, (%rbx)
	jne	.L211
.L192:
	cmpq	$0, 96(%rbx)
	je	.L193
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L190
.L194:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L212
.L196:
	movq	40(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%rbx, %rdi
	call	_IO_switch_to_main_get_area@PLT
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jnb	.L192
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movzbl	(%rax), %eax
.L183:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$-1, %eax
	je	.L185
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	call	__GI__IO_switch_to_get_mode
	cmpl	$-1, %eax
	jne	.L191
.L190:
	movl	$-1, %eax
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L193:
	cmpq	$0, 72(%rbx)
	je	.L194
	movq	%rbx, %rdi
	call	__GI__IO_free_backup_area
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L212:
	call	_IO_vtable_check
	jmp	.L196
.LFE79:
	.size	__GI___uflow, .-__GI___uflow
	.globl	__uflow
	.set	__uflow,__GI___uflow
	.p2align 4,,15
	.globl	__GI__IO_setb
	.hidden	__GI__IO_setb
	.type	__GI__IO_setb, @function
__GI__IO_setb:
.LFB80:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	56(%rdi), %rdi
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L214
	testb	$1, %al
	je	.L221
.L214:
	movq	%rdx, 64(%rbx)
	movl	%eax, %edx
	andl	$-2, %eax
	orl	$1, %edx
	testl	%ecx, %ecx
	movq	%rsi, 56(%rbx)
	cmove	%edx, %eax
	movl	%eax, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%ecx, 28(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	free@PLT
	movl	(%rbx), %eax
	movl	28(%rsp), %ecx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L214
.LFE80:
	.size	__GI__IO_setb, .-__GI__IO_setb
	.globl	_IO_setb
	.set	_IO_setb,__GI__IO_setb
	.p2align 4,,15
	.globl	__GI__IO_doallocbuf
	.hidden	__GI__IO_doallocbuf
	.type	__GI__IO_doallocbuf, @function
__GI__IO_doallocbuf:
.LFB81:
	cmpq	$0, 56(%rdi)
	je	.L240
	rep ret
	.p2align 4,,10
	.p2align 3
.L240:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	testb	$2, %al
	je	.L225
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jg	.L225
	leaq	132(%rdi), %r12
	leaq	131(%rdi), %rbp
.L229:
	orl	$1, %eax
	movq	%rbp, 56(%rbx)
	movq	%r12, 64(%rbx)
	movl	%eax, (%rbx)
.L222:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L241
.L227:
	movq	%rbx, %rdi
	call	*104(%rbp)
	cmpl	$-1, %eax
	jne	.L222
	movq	56(%rbx), %rdi
	leaq	132(%rbx), %r12
	leaq	131(%rbx), %rbp
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L229
	testb	$1, %al
	jne	.L229
	call	free@PLT
	movl	(%rbx), %eax
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L241:
	call	_IO_vtable_check
	jmp	.L227
.LFE81:
	.size	__GI__IO_doallocbuf, .-__GI__IO_doallocbuf
	.globl	_IO_doallocbuf
	.set	_IO_doallocbuf,__GI__IO_doallocbuf
	.p2align 4,,15
	.globl	_IO_default_underflow
	.type	_IO_default_underflow, @function
_IO_default_underflow:
.LFB82:
	movl	$-1, %eax
	ret
.LFE82:
	.size	_IO_default_underflow, .-_IO_default_underflow
	.p2align 4,,15
	.globl	__GI__IO_default_uflow
	.hidden	__GI__IO_default_uflow
	.type	__GI__IO_default_uflow, @function
__GI__IO_default_uflow:
.LFB83:
	pushq	%rbp
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	216(%rdi), %rbp
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L250
.L244:
	movq	%rbx, %rdi
	call	*32(%rbp)
	cmpl	$-1, %eax
	je	.L243
	movq	8(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movzbl	(%rax), %eax
.L243:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	call	_IO_vtable_check
	jmp	.L244
.LFE83:
	.size	__GI__IO_default_uflow, .-__GI__IO_default_uflow
	.globl	_IO_default_uflow
	.set	_IO_default_uflow,__GI__IO_default_uflow
	.p2align 4,,15
	.globl	__GI__IO_default_xsputn
	.hidden	__GI__IO_default_xsputn
	.type	__GI__IO_default_xsputn, @function
__GI__IO_default_xsputn:
.LFB84:
	testq	%rdx, %rdx
	je	.L270
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	leaq	__start___libc_IO_vtables(%rip), %r12
	pushq	%rbp
	leaq	__stop___libc_IO_vtables(%rip), %rbp
	pushq	%rbx
	movq	%rsi, %rcx
	movq	%rdx, %r13
	movq	%rdx, %r14
	subq	$24, %rsp
	subq	%r12, %rbp
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L254:
	testq	%rbx, %rbx
	jne	.L275
.L255:
	subq	%rbx, %r14
.L253:
	testq	%r14, %r14
	je	.L257
	movq	216(%r15), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rdx, %rbp
	jbe	.L276
.L258:
	movzbl	(%rcx), %esi
	leaq	1(%rcx), %rbx
	movq	%r15, %rdi
	call	*24(%rax)
	cmpl	$-1, %eax
	je	.L277
	subq	$1, %r14
	movq	%rbx, %rcx
.L260:
	movq	40(%r15), %rdi
	movq	48(%r15), %rbx
	cmpq	%rbx, %rdi
	jnb	.L253
	subq	%rdi, %rbx
	cmpq	%r14, %rbx
	cmova	%r14, %rbx
	cmpq	$20, %rbx
	jbe	.L254
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	movq	%rcx, (%rsp)
	call	__GI_mempcpy@PLT
	movq	(%rsp), %rcx
	movq	%rax, 40(%r15)
	addq	%rbx, %rcx
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L277:
	subq	%r14, %r13
.L257:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L256:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rbx, %rax
	jne	.L256
	addq	%rbx, %rdi
	addq	%rbx, %rcx
	movq	%rdi, 40(%r15)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%rcx, 8(%rsp)
	movq	%rax, (%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rcx
	movq	(%rsp), %rax
	jmp	.L258
.LFE84:
	.size	__GI__IO_default_xsputn, .-__GI__IO_default_xsputn
	.globl	_IO_default_xsputn
	.set	_IO_default_xsputn,__GI__IO_default_xsputn
	.p2align 4,,15
	.globl	__GI__IO_sgetn
	.hidden	__GI__IO_sgetn
	.type	__GI__IO_sgetn, @function
__GI__IO_sgetn:
.LFB85:
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rcx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	$32, %rsp
	movq	216(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r8
	subq	%rcx, %r8
	cmpq	%r8, %rax
	jbe	.L281
	movq	64(%rbx), %rax
	addq	$32, %rsp
	popq	%rbx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rdx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	64(%rbx), %rax
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	8(%rsp), %rdi
	addq	$32, %rsp
	popq	%rbx
	jmp	*%rax
.LFE85:
	.size	__GI__IO_sgetn, .-__GI__IO_sgetn
	.globl	_IO_sgetn
	.set	_IO_sgetn,__GI__IO_sgetn
	.p2align 4,,15
	.globl	__GI__IO_default_xsgetn
	.hidden	__GI__IO_default_xsgetn
	.type	__GI__IO_default_xsgetn, @function
__GI__IO_default_xsgetn:
.LFB86:
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L284:
	testq	%rax, %rax
	jne	.L298
.L285:
	subq	%rbx, %r14
.L283:
	testq	%r14, %r14
	je	.L287
	movq	%r13, %rdi
	call	__GI___underflow
	cmpl	$-1, %eax
	je	.L299
.L288:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	cmpq	%rax, %rsi
	jnb	.L283
	subq	%rsi, %rax
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	cmpq	$20, %rax
	movq	%rax, %rbx
	jbe	.L284
	movq	%rbp, %rdi
	movq	%rax, %rdx
	call	__GI_mempcpy@PLT
	addq	%rbx, 8(%r13)
	movq	%rax, %rbp
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L299:
	subq	%r14, %r12
.L287:
	popq	%rbx
	movq	%r12, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	leal	-1(%rax), %ecx
	xorl	%eax, %eax
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L286:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, 0(%rbp,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L286
	addq	%rax, %rsi
	addq	%rax, %rbp
	movq	%rsi, 8(%r13)
	jmp	.L285
.LFE86:
	.size	__GI__IO_default_xsgetn, .-__GI__IO_default_xsgetn
	.globl	_IO_default_xsgetn
	.set	_IO_default_xsgetn,__GI__IO_default_xsgetn
	.p2align 4,,15
	.globl	_IO_default_setbuf
	.type	_IO_default_setbuf, @function
_IO_default_setbuf:
.LFB87:
	pushq	%r13
	pushq	%r12
	leaq	__stop___libc_IO_vtables(%rip), %rax
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	movq	%rdi, %rbx
	movq	%rsi, %r13
	subq	$8, %rsp
	movq	216(%rdi), %r12
	subq	%rdx, %rax
	movq	%r12, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L317
.L301:
	movq	%rbx, %rdi
	call	*96(%r12)
	cmpl	$-1, %eax
	je	.L308
	movl	(%rbx), %eax
	testq	%r13, %r13
	movq	56(%rbx), %rdi
	movl	%eax, %edx
	je	.L309
	testq	%rbp, %rbp
	je	.L309
	andl	$-3, %edx
	addq	%r13, %rbp
	testq	%rdi, %rdi
	movl	%edx, (%rbx)
	je	.L307
	testb	$1, %al
	je	.L318
.L307:
	orl	$1, %edx
	movq	%r13, 56(%rbx)
	movq	%rbp, 64(%rbx)
	movl	%edx, (%rbx)
.L306:
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	%rbx, %rax
	movq	$0, 32(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 24(%rbx)
.L300:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	orl	$2, %edx
	testq	%rdi, %rdi
	leaq	132(%rbx), %rbp
	movl	%edx, (%rbx)
	leaq	131(%rbx), %r12
	je	.L305
	testb	$1, %al
	je	.L319
.L305:
	orl	$1, %edx
	movq	%r12, 56(%rbx)
	movq	%rbp, 64(%rbx)
	movl	%edx, (%rbx)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L318:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L319:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L317:
	call	_IO_vtable_check
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L308:
	xorl	%eax, %eax
	jmp	.L300
.LFE87:
	.size	_IO_default_setbuf, .-_IO_default_setbuf
	.p2align 4,,15
	.globl	_IO_default_seekpos
	.type	_IO_default_seekpos, @function
_IO_default_seekpos:
.LFB88:
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %r8
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	$32, %rsp
	movq	216(%rdi), %rbx
	subq	%r8, %rax
	movq	%rbx, %rcx
	subq	%r8, %rcx
	cmpq	%rcx, %rax
	jbe	.L323
	movq	72(%rbx), %rax
	addq	$32, %rsp
	movl	%edx, %ecx
	popq	%rbx
	xorl	%edx, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L323:
	movl	%edx, 28(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movl	28(%rsp), %edx
	movq	72(%rbx), %rax
	movq	16(%rsp), %rsi
	movq	8(%rsp), %rdi
	addq	$32, %rsp
	popq	%rbx
	movl	%edx, %ecx
	xorl	%edx, %edx
	jmp	*%rax
.LFE88:
	.size	_IO_default_seekpos, .-_IO_default_seekpos
	.p2align 4,,15
	.globl	__GI__IO_default_doallocate
	.hidden	__GI__IO_default_doallocate
	.type	__GI__IO_default_doallocate, @function
__GI__IO_default_doallocate:
.LFB89:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$8192, %edi
	call	malloc@PLT
	movq	%rax, %rbp
	movl	$-1, %eax
	testq	%rbp, %rbp
	je	.L324
	movq	56(%rbx), %rdi
	leaq	8192(%rbp), %r12
	movl	(%rbx), %edx
	testq	%rdi, %rdi
	je	.L326
	testb	$1, %dl
	je	.L334
.L326:
	andl	$-2, %edx
	movq	%rbp, 56(%rbx)
	movq	%r12, 64(%rbx)
	movl	%edx, (%rbx)
	movl	$1, %eax
.L324:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L326
.LFE89:
	.size	__GI__IO_default_doallocate, .-__GI__IO_default_doallocate
	.globl	_IO_default_doallocate
	.set	_IO_default_doallocate,__GI__IO_default_doallocate
	.p2align 4,,15
	.globl	__GI__IO_enable_locks
	.hidden	__GI__IO_enable_locks
	.type	__GI__IO_enable_locks, @function
__GI__IO_enable_locks:
.LFB92:
	movl	stdio_needs_locking(%rip), %eax
	testl	%eax, %eax
	jne	.L335
	movq	__GI__IO_list_all(%rip), %rax
	movl	$1, stdio_needs_locking(%rip)
	testq	%rax, %rax
	je	.L335
	.p2align 4,,10
	.p2align 3
.L337:
	orl	$128, 116(%rax)
	movq	104(%rax), %rax
	testq	%rax, %rax
	jne	.L337
.L335:
	rep ret
.LFE92:
	.size	__GI__IO_enable_locks, .-__GI__IO_enable_locks
	.globl	_IO_enable_locks
	.set	_IO_enable_locks,__GI__IO_enable_locks
	.p2align 4,,15
	.globl	_IO_old_init
	.type	_IO_old_init, @function
_IO_old_init:
.LFB93:
	movl	stdio_needs_locking(%rip), %edx
	orl	$-72548352, %esi
	movl	$0, 116(%rdi)
	movl	%esi, (%rdi)
	testl	%edx, %edx
	je	.L343
	movl	$128, 116(%rdi)
.L343:
	xorl	%eax, %eax
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movw	%ax, 128(%rdi)
	movq	136(%rdi), %rax
	movq	$0, 24(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	testq	%rax, %rax
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	je	.L342
	movq	$0, (%rax)
	movq	$0, 8(%rax)
.L342:
	rep ret
.LFE93:
	.size	_IO_old_init, .-_IO_old_init
	.p2align 4,,15
	.globl	_IO_no_init
	.type	_IO_no_init, @function
_IO_no_init:
.LFB94:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rcx, %rbx
	movq	%r8, %r13
	subq	$8, %rsp
	call	_IO_old_init@PLT
	testl	%r12d, %r12d
	movl	%r12d, 192(%rbp)
	js	.L349
	movq	%rbx, 160(%rbp)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	%r13, 224(%rbx)
.L350:
	movq	$0, 168(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	movq	$-1, 160(%rbp)
	jmp	.L350
.LFE94:
	.size	_IO_no_init, .-_IO_no_init
	.p2align 4,,15
	.globl	_IO_init_internal
	.hidden	_IO_init_internal
	.type	_IO_init_internal, @function
_IO_init_internal:
.LFB90:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$-1, %edx
	jmp	_IO_no_init@PLT
.LFE90:
	.size	_IO_init_internal, .-_IO_init_internal
	.p2align 4,,15
	.globl	_IO_init
	.type	_IO_init, @function
_IO_init:
.LFB91:
	leaq	_IO_vtable_check(%rip), %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
#APP
# 915 "libioP.h" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, IO_accept_foreign_vtables(%rip)
	movl	$-1, %edx
	jmp	_IO_no_init@PLT
.LFE91:
	.size	_IO_init, .-_IO_init
	.p2align 4,,15
	.globl	_IO_default_sync
	.type	_IO_default_sync, @function
_IO_default_sync:
.LFB95:
	xorl	%eax, %eax
	ret
.LFE95:
	.size	_IO_default_sync, .-_IO_default_sync
	.p2align 4,,15
	.globl	__GI__IO_default_finish
	.hidden	__GI__IO_default_finish
	.type	__GI__IO_default_finish, @function
__GI__IO_default_finish:
.LFB96:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L356
	testb	$1, (%rbx)
	je	.L372
.L356:
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L357
	.p2align 4,,10
	.p2align 3
.L358:
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L358
.L357:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L359
	call	free@PLT
	movq	$0, 72(%rbx)
.L359:
	testb	$-128, (%rbx)
	jne	.L373
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	call	free@PLT
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__GI__IO_un_link.part.1
.LFE96:
	.size	__GI__IO_default_finish, .-__GI__IO_default_finish
	.globl	_IO_default_finish
	.set	_IO_default_finish,__GI__IO_default_finish
	.p2align 4,,15
	.globl	_IO_default_seekoff
	.type	_IO_default_seekoff, @function
_IO_default_seekoff:
.LFB97:
	movq	$-1, %rax
	ret
.LFE97:
	.size	_IO_default_seekoff, .-_IO_default_seekoff
	.p2align 4,,15
	.globl	__GI__IO_sputbackc
	.hidden	__GI__IO_sputbackc
	.type	__GI__IO_sputbackc, @function
__GI__IO_sputbackc:
.LFB98:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	jbe	.L376
	cmpb	%sil, -1(%rax)
	je	.L384
.L376:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L385
.L378:
	movq	%rbx, %rdi
	call	*48(%rbp)
	cmpl	$-1, %eax
	je	.L375
.L377:
	andl	$-17, (%rbx)
.L375:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	subq	$1, %rax
	movq	%rax, 8(%rdi)
	movzbl	%sil, %eax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L385:
	movl	%esi, 12(%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %esi
	jmp	.L378
.LFE98:
	.size	__GI__IO_sputbackc, .-__GI__IO_sputbackc
	.globl	_IO_sputbackc
	.set	_IO_sputbackc,__GI__IO_sputbackc
	.p2align 4,,15
	.globl	_IO_sungetc
	.type	_IO_sungetc, @function
_IO_sungetc:
.LFB99:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	jbe	.L387
	leaq	-1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	-1(%rax), %eax
.L388:
	andl	$-17, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	movq	216(%rdi), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L396
.L389:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*48(%rbp)
	cmpl	$-1, %eax
	jne	.L388
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	call	_IO_vtable_check
	jmp	.L389
.LFE99:
	.size	_IO_sungetc, .-_IO_sungetc
	.p2align 4,,15
	.globl	__GI__IO_adjust_column
	.hidden	__GI__IO_adjust_column
	.type	__GI__IO_adjust_column, @function
__GI__IO_adjust_column:
.LFB100:
	movslq	%edx, %r8
	addq	%rsi, %r8
	cmpq	%r8, %rsi
	jnb	.L398
	cmpb	$10, -1(%r8)
	leaq	-1(%r8), %rcx
	jne	.L400
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L402:
	subq	$1, %rcx
	cmpb	$10, (%rcx)
	je	.L399
.L400:
	cmpq	%rcx, %rsi
	jne	.L402
.L398:
	leal	(%rdx,%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	subq	%rcx, %r8
	leal	-1(%r8), %eax
	ret
.LFE100:
	.size	__GI__IO_adjust_column, .-__GI__IO_adjust_column
	.globl	_IO_adjust_column
	.set	_IO_adjust_column,__GI__IO_adjust_column
	.p2align 4,,15
	.globl	_IO_flush_all_lockp
	.type	_IO_flush_all_lockp, @function
_IO_flush_all_lockp:
.LFB101:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r15d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	__libc_pthread_functions_init(%rip), %r14d
	testl	%r14d, %r14d
	je	.L405
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%edx, %edx
#APP
# 691 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	flush_cleanup(%rip), %rsi
	call	*%rax
.L406:
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L407
#APP
# 692 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L408
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L409:
	movq	%rbx, 8+list_all_lock(%rip)
.L407:
	movl	4+list_all_lock(%rip), %eax
	movq	__GI__IO_list_all(%rip), %rbx
	xorl	%ebp, %ebp
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L410
	leaq	__start___libc_IO_vtables(%rip), %r13
	leaq	__stop___libc_IO_vtables(%rip), %r12
	subq	%r13, %r12
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L415:
	movq	160(%rbx), %rax
	movq	24(%rax), %rcx
	cmpq	%rcx, 32(%rax)
	jbe	.L417
.L416:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	%rdx, %r12
	jbe	.L439
.L418:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%rax)
	cmpl	$-1, %eax
	movl	$-1, %eax
	cmove	%eax, %ebp
.L417:
	testl	%r15d, %r15d
	je	.L420
	testl	$32768, (%rbx)
	jne	.L420
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L420
	movq	$0, 8(%rdi)
#APP
# 710 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L422
	subl	$1, (%rdi)
	.p2align 4,,10
	.p2align 3
.L420:
	movq	104(%rbx), %rbx
	movq	$0, run_fp(%rip)
	testq	%rbx, %rbx
	je	.L440
.L423:
	testl	%r15d, %r15d
	movq	%rbx, run_fp(%rip)
	je	.L411
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L411
	movq	136(%rbx), %rdi
	movq	%fs:16, %rcx
	cmpq	%rcx, 8(%rdi)
	je	.L412
#APP
# 699 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L413
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, (%rdi)
# 0 "" 2
#NO_APP
.L414:
	movq	136(%rbx), %rdi
	movq	%rcx, 8(%rdi)
.L412:
	addl	$1, 4(%rdi)
.L411:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jg	.L415
	movq	32(%rbx), %rax
	cmpq	%rax, 40(%rbx)
	ja	.L416
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L440:
	movl	4+list_all_lock(%rip), %eax
.L410:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L425
	movq	$0, 8+list_all_lock(%rip)
#APP
# 715 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L426
	subl	$1, list_all_lock(%rip)
.L425:
	testl	%r14d, %r14d
	je	.L404
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 716 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L404:
	addq	$56, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L418
.L426:
#APP
# 715 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L425
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 715 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L405:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 24(%rsp)
	movq	%rax, 16(%rsp)
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L413:
	movl	%edx, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, (%rdi)
	je	.L414
	movq	%rcx, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rcx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L422:
#APP
# 710 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L420
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 710 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L408:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L409
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L409
.LFE101:
	.size	_IO_flush_all_lockp, .-_IO_flush_all_lockp
	.p2align 4,,15
	.globl	_IO_cleanup
	.type	_IO_cleanup, @function
_IO_cleanup:
.LFB106:
	pushq	%r15
	pushq	%r14
	xorl	%edi, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	call	_IO_flush_all_lockp@PLT
	movl	__libc_pthread_functions_init(%rip), %r12d
	movl	%eax, %r14d
	testl	%r12d, %r12d
	je	.L442
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%edx, %edx
#APP
# 786 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	flush_cleanup(%rip), %rsi
	call	*%rax
.L443:
	movq	%fs:16, %rbx
	cmpq	8+list_all_lock(%rip), %rbx
	je	.L444
#APP
# 787 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L445
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L446:
	movq	%rbx, 8+list_all_lock(%rip)
.L444:
	movl	4+list_all_lock(%rip), %eax
	movq	__GI__IO_list_all(%rip), %rbx
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L447
	leaq	__start___libc_IO_vtables(%rip), %r13
	leaq	__stop___libc_IO_vtables(%rip), %rbp
	subq	%r13, %rbp
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$-1, 192(%rbx)
	movq	104(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L485
.L461:
	movl	(%rbx), %r15d
	andl	$2, %r15d
	jne	.L449
	movl	192(%rbx), %edx
	testl	%edx, %edx
	je	.L449
.L456:
	movq	136(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L451
	movq	%fs:16, %rsi
	cmpq	8(%rdx), %rsi
	je	.L452
#APP
# 807 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %ecx
	jne	.L453
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, (%rdx)
# 0 "" 2
#NO_APP
.L454:
	testl	%eax, %eax
	je	.L455
	call	__GI___sched_yield
	cmpl	$1, %r15d
	jne	.L467
	movl	$2, %r15d
.L451:
	cmpb	$0, dealloc_buffers(%rip)
	je	.L486
.L457:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	%rdx, %rbp
	jbe	.L487
.L458:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*88(%rax)
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L459
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__GI__IO_wsetb
.L459:
	cmpl	$2, %r15d
	je	.L449
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L449
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L449
	movq	$0, 8(%rdi)
#APP
# 831 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L460
	subl	$1, (%rdi)
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L485:
	movl	4+list_all_lock(%rip), %eax
.L447:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L463
	movq	$0, 8+list_all_lock(%rip)
#APP
# 842 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L464
	subl	$1, list_all_lock(%rip)
.L463:
	testl	%r12d, %r12d
	je	.L441
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 843 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L441:
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	movl	$1, %r15d
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L486:
	movl	(%rbx), %eax
	testb	$1, %al
	jne	.L457
	orl	$1, %eax
	movl	%eax, (%rbx)
	movq	freeres_list(%rip), %rax
	movq	%rbx, freeres_list(%rip)
	movq	%rax, 168(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 176(%rbx)
	jmp	.L457
.L464:
#APP
# 842 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L463
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 842 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L453:
	xorl	%eax, %eax
	lock cmpxchgl	%ecx, (%rdx)
	setne	%al
	movzbl	%al, %eax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 24(%rsp)
	movq	%rax, 16(%rsp)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L445:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L446
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L452:
	addl	$1, 4(%rdx)
	jmp	.L451
.L455:
	movq	136(%rbx), %rax
	movq	%rsi, 8(%rax)
	movl	$1, 4(%rax)
	jmp	.L451
.L460:
#APP
# 831 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L449
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 831 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L449
.LFE106:
	.size	_IO_cleanup, .-_IO_cleanup
	.p2align 4,,15
	.globl	__GI__IO_flush_all
	.hidden	__GI__IO_flush_all
	.type	__GI__IO_flush_all, @function
__GI__IO_flush_all:
.LFB102:
	movl	$1, %edi
	jmp	_IO_flush_all_lockp@PLT
.LFE102:
	.size	__GI__IO_flush_all, .-__GI__IO_flush_all
	.globl	_IO_flush_all
	.set	_IO_flush_all,__GI__IO_flush_all
	.p2align 4,,15
	.globl	__GI__IO_flush_all_linebuffered
	.hidden	__GI__IO_flush_all_linebuffered
	.type	__GI__IO_flush_all_linebuffered, @function
__GI__IO_flush_all_linebuffered:
.LFB103:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	__libc_pthread_functions_init(%rip), %r13d
	testl	%r13d, %r13d
	je	.L490
	movq	184+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%edx, %edx
#APP
# 737 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	flush_cleanup(%rip), %rsi
	call	*%rax
.L491:
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L492
#APP
# 738 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L493
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L494:
	movq	%rbx, 8+list_all_lock(%rip)
.L492:
	movl	4+list_all_lock(%rip), %eax
	movq	__GI__IO_list_all(%rip), %rbx
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L495
	leaq	__start___libc_IO_vtables(%rip), %r12
	leaq	__stop___libc_IO_vtables(%rip), %rbp
	movl	$202, %r14d
	subq	%r12, %rbp
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L525:
	movq	136(%rbx), %rdi
	movq	%fs:16, %r15
	cmpq	%r15, 8(%rdi)
	je	.L497
#APP
# 744 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L498
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L499:
	movq	136(%rbx), %rdi
	movq	%r15, 8(%rdi)
.L497:
	movl	(%rbx), %eax
	addl	$1, 4(%rdi)
	andl	$520, %eax
	cmpl	$512, %eax
	je	.L512
.L500:
	testl	$32768, (%rbx)
	jne	.L503
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L503
	movq	$0, 8(%rdi)
#APP
# 749 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L505
	subl	$1, (%rdi)
.L503:
	movq	104(%rbx), %rbx
	movq	$0, run_fp(%rip)
	testq	%rbx, %rbx
	je	.L524
.L506:
	movl	(%rbx), %edx
	movq	%rbx, run_fp(%rip)
	movl	%edx, %ecx
	andl	$32768, %ecx
	je	.L525
	andl	$520, %edx
	cmpl	$512, %edx
	jne	.L503
.L512:
	movq	216(%rbx), %r15
	movq	%r15, %rax
	subq	%r12, %rax
	cmpq	%rax, %rbp
	jbe	.L526
.L501:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%r15)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L524:
	movl	4+list_all_lock(%rip), %eax
.L495:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L508
	movq	$0, 8+list_all_lock(%rip)
#APP
# 754 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L509
	subl	$1, list_all_lock(%rip)
.L508:
	testl	%r13d, %r13d
	je	.L489
	movq	192+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%esi, %esi
#APP
# 755 "genops.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L489:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L509:
#APP
# 754 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L508
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 754 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L498:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L499
	call	__lll_lock_wait_private
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L526:
	call	_IO_vtable_check
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L505:
#APP
# 749 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L503
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	%r14d, %eax
#APP
# 749 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L493:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L494
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L494
.LFE103:
	.size	__GI__IO_flush_all_linebuffered, .-__GI__IO_flush_all_linebuffered
	.globl	_IO_flush_all_linebuffered
	.set	_IO_flush_all_linebuffered,__GI__IO_flush_all_linebuffered
	.weak	_flushlbf
	.set	_flushlbf,_IO_flush_all_linebuffered
	.p2align 4,,15
	.globl	_IO_init_marker
	.type	_IO_init_marker, @function
_IO_init_marker:
.LFB107:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rsi), %edx
	movq	%rsi, 8(%rdi)
	testb	$8, %dh
	jne	.L535
.L528:
	andb	$1, %dh
	movq	8(%rsi), %rax
	jne	.L536
	subq	24(%rsi), %rax
	movl	%eax, 16(%rbx)
.L530:
	movq	96(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 96(%rsi)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	subq	16(%rsi), %rax
	movl	%eax, 16(%rbx)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	__GI__IO_switch_to_get_mode
	movq	8(%rsp), %rsi
	movl	(%rsi), %edx
	jmp	.L528
.LFE107:
	.size	_IO_init_marker, .-_IO_init_marker
	.p2align 4,,15
	.globl	_IO_remove_marker
	.type	_IO_remove_marker, @function
_IO_remove_marker:
.LFB108:
	movq	8(%rdi), %rax
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L537
	cmpq	%rdi, %rdx
	jne	.L540
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L542:
	cmpq	%rax, %rdi
	je	.L541
	movq	%rax, %rdx
.L540:
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L542
.L537:
	rep ret
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	96(%rax), %rdx
.L541:
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
	ret
.LFE108:
	.size	_IO_remove_marker, .-_IO_remove_marker
	.p2align 4,,15
	.globl	_IO_marker_difference
	.type	_IO_marker_difference, @function
_IO_marker_difference:
.LFB109:
	movl	16(%rdi), %eax
	subl	16(%rsi), %eax
	ret
.LFE109:
	.size	_IO_marker_difference, .-_IO_marker_difference
	.p2align 4,,15
	.globl	_IO_marker_delta
	.type	_IO_marker_delta, @function
_IO_marker_delta:
.LFB110:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L553
	testl	$256, (%rax)
	movq	8(%rax), %rdx
	jne	.L554
	subl	24(%rax), %edx
	movl	16(%rdi), %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	subl	16(%rax), %edx
	movl	16(%rdi), %eax
	subl	%edx, %eax
	ret
.L553:
	movl	$-1, %eax
	ret
.LFE110:
	.size	_IO_marker_delta, .-_IO_marker_delta
	.p2align 4,,15
	.globl	_IO_seekmark
	.type	_IO_seekmark, @function
_IO_seekmark:
.LFB111:
	pushq	%rbp
	pushq	%rbx
	movl	$-1, %eax
	subq	$8, %rsp
	movq	8(%rsi), %rbp
	cmpq	%rdi, %rbp
	jne	.L555
	movslq	16(%rsi), %rax
	movl	0(%rbp), %edx
	movq	%rsi, %rbx
	andl	$256, %edx
	testl	%eax, %eax
	js	.L557
	testl	%edx, %edx
	jne	.L565
.L558:
	addq	24(%rbp), %rax
	movq	%rax, 8(%rbp)
	xorl	%eax, %eax
.L555:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%rbp, %rdi
	call	_IO_switch_to_main_get_area@PLT
	movslq	16(%rbx), %rax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L557:
	testl	%edx, %edx
	je	.L566
.L559:
	addq	16(%rbp), %rax
	movq	%rax, 8(%rbp)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%rbp, %rdi
	call	_IO_switch_to_backup_area@PLT
	movslq	16(%rbx), %rax
	jmp	.L559
.LFE111:
	.size	_IO_seekmark, .-_IO_seekmark
	.p2align 4,,15
	.globl	__GI__IO_unsave_markers
	.hidden	__GI__IO_unsave_markers
	.type	__GI__IO_unsave_markers, @function
__GI__IO_unsave_markers:
.LFB112:
	cmpq	$0, 96(%rdi)
	je	.L568
	movq	$0, 96(%rdi)
.L568:
	cmpq	$0, 72(%rdi)
	je	.L567
	jmp	__GI__IO_free_backup_area
	.p2align 4,,10
	.p2align 3
.L567:
	rep ret
.LFE112:
	.size	__GI__IO_unsave_markers, .-__GI__IO_unsave_markers
	.globl	_IO_unsave_markers
	.set	_IO_unsave_markers,__GI__IO_unsave_markers
	.p2align 4,,15
	.globl	__GI__IO_default_pbackfail
	.hidden	__GI__IO_default_pbackfail
	.type	__GI__IO_default_pbackfail, @function
__GI__IO_default_pbackfail:
.LFB113:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rbp
	movq	24(%rdi), %r13
	movl	(%rdi), %eax
	andl	$256, %eax
	cmpq	%r13, %rbp
	jbe	.L571
	testl	%eax, %eax
	je	.L590
.L572:
	leaq	-1(%rbp), %rax
	movq	%rax, 8(%rbx)
	movb	%r12b, -1(%rbp)
.L574:
	movzbl	%r12b, %eax
.L570:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	testl	%eax, %eax
	je	.L591
	movq	16(%rbx), %rdx
	subq	%r13, %rdx
	leaq	(%rdx,%rdx), %r15
	movq	%rdx, 8(%rsp)
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L575
	movq	8(%rsp), %rdx
	movq	%r15, %rbp
	movq	%r13, %rsi
	subq	%rdx, %rbp
	addq	%rax, %rbp
	movq	%rbp, %rdi
	call	__GI_memcpy@PLT
	movq	%r13, %rdi
	call	free@PLT
	movq	%r14, 24(%rbx)
	addq	%r15, %r14
	movq	%rbp, 80(%rbx)
	movq	%r14, 16(%rbx)
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L590:
	movzbl	-1(%rbp), %eax
	cmpl	%esi, %eax
	je	.L592
	cmpq	$0, 72(%rdi)
	je	.L581
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L575
	movq	8(%rbx), %rbp
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L591:
	cmpq	$0, 72(%rbx)
	je	.L581
.L576:
	movq	%rbp, 24(%rbx)
	movq	%rbx, %rdi
	call	_IO_switch_to_backup_area@PLT
	movq	8(%rbx), %rbp
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L581:
	movl	$128, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L575
	movq	%rax, 72(%rbx)
	subq	$-128, %rax
	movq	%rax, 88(%rbx)
	movq	%rax, 80(%rbx)
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L592:
	subq	$1, %rbp
	movq	%rbp, 8(%rdi)
	jmp	.L574
.L575:
	movl	$-1, %eax
	jmp	.L570
.LFE113:
	.size	__GI__IO_default_pbackfail, .-__GI__IO_default_pbackfail
	.globl	_IO_default_pbackfail
	.set	_IO_default_pbackfail,__GI__IO_default_pbackfail
	.p2align 4,,15
	.globl	_IO_default_seek
	.type	_IO_default_seek, @function
_IO_default_seek:
.LFB114:
	movq	$-1, %rax
	ret
.LFE114:
	.size	_IO_default_seek, .-_IO_default_seek
	.p2align 4,,15
	.globl	_IO_default_stat
	.type	_IO_default_stat, @function
_IO_default_stat:
.LFB115:
	movl	$-1, %eax
	ret
.LFE115:
	.size	_IO_default_stat, .-_IO_default_stat
	.p2align 4,,15
	.globl	_IO_default_read
	.type	_IO_default_read, @function
_IO_default_read:
.LFB116:
	movq	$-1, %rax
	ret
.LFE116:
	.size	_IO_default_read, .-_IO_default_read
	.p2align 4,,15
	.globl	_IO_default_write
	.type	_IO_default_write, @function
_IO_default_write:
.LFB117:
	xorl	%eax, %eax
	ret
.LFE117:
	.size	_IO_default_write, .-_IO_default_write
	.p2align 4,,15
	.globl	_IO_default_showmanyc
	.type	_IO_default_showmanyc, @function
_IO_default_showmanyc:
.LFB136:
	movl	$-1, %eax
	ret
.LFE136:
	.size	_IO_default_showmanyc, .-_IO_default_showmanyc
	.p2align 4,,15
	.globl	_IO_default_imbue
	.type	_IO_default_imbue, @function
_IO_default_imbue:
.LFB119:
	rep ret
.LFE119:
	.size	_IO_default_imbue, .-_IO_default_imbue
	.p2align 4,,15
	.globl	__GI__IO_iter_begin
	.hidden	__GI__IO_iter_begin
	.type	__GI__IO_iter_begin, @function
__GI__IO_iter_begin:
.LFB120:
	movq	__GI__IO_list_all(%rip), %rax
	ret
.LFE120:
	.size	__GI__IO_iter_begin, .-__GI__IO_iter_begin
	.globl	_IO_iter_begin
	.set	_IO_iter_begin,__GI__IO_iter_begin
	.p2align 4,,15
	.globl	__GI__IO_iter_end
	.hidden	__GI__IO_iter_end
	.type	__GI__IO_iter_end, @function
__GI__IO_iter_end:
.LFB121:
	xorl	%eax, %eax
	ret
.LFE121:
	.size	__GI__IO_iter_end, .-__GI__IO_iter_end
	.globl	_IO_iter_end
	.set	_IO_iter_end,__GI__IO_iter_end
	.p2align 4,,15
	.globl	__GI__IO_iter_next
	.hidden	__GI__IO_iter_next
	.type	__GI__IO_iter_next, @function
__GI__IO_iter_next:
.LFB122:
	movq	104(%rdi), %rax
	ret
.LFE122:
	.size	__GI__IO_iter_next, .-__GI__IO_iter_next
	.globl	_IO_iter_next
	.set	_IO_iter_next,__GI__IO_iter_next
	.p2align 4,,15
	.globl	__GI__IO_iter_file
	.hidden	__GI__IO_iter_file
	.type	__GI__IO_iter_file, @function
__GI__IO_iter_file:
.LFB123:
	movq	%rdi, %rax
	ret
.LFE123:
	.size	__GI__IO_iter_file, .-__GI__IO_iter_file
	.globl	_IO_iter_file
	.set	_IO_iter_file,__GI__IO_iter_file
	.p2align 4,,15
	.globl	__GI__IO_list_lock
	.hidden	__GI__IO_list_lock
	.type	__GI__IO_list_lock, @function
__GI__IO_list_lock:
.LFB124:
	pushq	%rbx
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L604
#APP
# 1096 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L605
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L606:
	movq	%rbx, 8+list_all_lock(%rip)
.L604:
	addl	$1, 4+list_all_lock(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L606
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L606
.LFE124:
	.size	__GI__IO_list_lock, .-__GI__IO_list_lock
	.globl	_IO_list_lock
	.set	_IO_list_lock,__GI__IO_list_lock
	.p2align 4,,15
	.globl	__GI__IO_list_unlock
	.hidden	__GI__IO_list_unlock
	.type	__GI__IO_list_unlock, @function
__GI__IO_list_unlock:
.LFB125:
	movl	4+list_all_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L608
	movq	$0, 8+list_all_lock(%rip)
#APP
# 1105 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L611
	subl	$1, list_all_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L611:
#APP
# 1105 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L608
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1105 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L608:
	rep ret
.LFE125:
	.size	__GI__IO_list_unlock, .-__GI__IO_list_unlock
	.globl	_IO_list_unlock
	.set	_IO_list_unlock,__GI__IO_list_unlock
	.p2align 4,,15
	.globl	__GI__IO_list_resetlock
	.hidden	__GI__IO_list_resetlock
	.type	__GI__IO_list_resetlock, @function
__GI__IO_list_resetlock:
.LFB126:
	movq	$0, list_all_lock(%rip)
	movq	$0, 8+list_all_lock(%rip)
	ret
.LFE126:
	.size	__GI__IO_list_resetlock, .-__GI__IO_list_resetlock
	.globl	_IO_list_resetlock
	.set	_IO_list_resetlock,__GI__IO_list_resetlock
	.section	__libc_atexit,"aw",@progbits
	.align 8
	.type	__elf_set___libc_atexit_element__IO_cleanup__, @object
	.size	__elf_set___libc_atexit_element__IO_cleanup__, 8
__elf_set___libc_atexit_element__IO_cleanup__:
	.quad	_IO_cleanup
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_buffer_free__, @object
	.size	__elf_set___libc_subfreeres_element_buffer_free__, 8
__elf_set___libc_subfreeres_element_buffer_free__:
	.quad	buffer_free
	.local	freeres_list
	.comm	freeres_list,8,8
	.local	dealloc_buffers
	.comm	dealloc_buffers,1,1
	.local	stdio_needs_locking
	.comm	stdio_needs_locking,4,4
	.local	run_fp
	.comm	run_fp,8,8
	.local	list_all_lock
	.comm	list_all_lock,16,16
	.hidden	IO_accept_foreign_vtables
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
	.hidden	__lll_lock_wait_private
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
