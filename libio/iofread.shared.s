	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__IO_fread
	.hidden	__GI__IO_fread
	.type	__GI__IO_fread, @function
__GI__IO_fread:
.LFB68:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	imulq	%rdx, %rbx
	subq	$8, %rsp
	testq	%rbx, %rbx
	je	.L1
	movq	%rdx, %r13
	movl	(%rcx), %edx
	movq	%rcx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r14
	andl	$32768, %edx
	jne	.L3
	movq	136(%rcx), %rdi
	movq	%fs:16, %r15
	cmpq	%r15, 8(%rdi)
	je	.L4
#APP
# 37 "iofread.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L6:
	movq	136(%rbp), %rdi
	movq	%r15, 8(%rdi)
.L4:
	addl	$1, 4(%rdi)
.L3:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
.LEHB0:
	call	__GI__IO_sgetn
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L10
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L10
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L12
	subl	$1, (%rdi)
.L10:
	cmpq	%r8, %rbx
	je	.L16
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%r12
	movq	%rax, %rbx
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r13, %rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L6
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L10
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L10
.L17:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L14
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L14
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L15
	subl	$1, (%rdi)
.L14:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L15:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L14
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L14
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L17-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__GI__IO_fread, .-__GI__IO_fread
	.globl	_IO_fread
	.set	_IO_fread,__GI__IO_fread
	.weak	fread
	.set	fread,_IO_fread
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
