	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	putwchar_unlocked
	.type	putwchar_unlocked, @function
putwchar_unlocked:
	movq	stdout@GOTPCREL(%rip), %rdx
	movl	%edi, %eax
	movq	(%rdx), %rdi
	movq	160(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L2
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %esi
	jmp	__GI___woverflow
	.size	putwchar_unlocked, .-putwchar_unlocked
