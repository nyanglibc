	.text
	.p2align 4,,15
	.globl	_IO_str_underflow
	.hidden	_IO_str_underflow
	.type	_IO_str_underflow, @function
_IO_str_underflow:
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdx
	cmpq	%rdx, %rax
	jbe	.L2
	movq	%rax, 16(%rdi)
	movq	%rax, %rdx
.L2:
	movl	(%rdi), %ecx
	movl	%ecx, %esi
	andl	$3072, %esi
	cmpl	$3072, %esi
	je	.L3
	movq	8(%rdi), %rax
.L4:
	cmpq	%rdx, %rax
	jnb	.L6
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	andb	$-9, %ch
	movq	%rax, 8(%rdi)
	movl	%ecx, (%rdi)
	movq	48(%rdi), %rcx
	movq	%rcx, 40(%rdi)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, %eax
	ret
	.size	_IO_str_underflow, .-_IO_str_underflow
	.p2align 4,,15
	.globl	_IO_str_overflow
	.hidden	_IO_str_overflow
	.type	_IO_str_overflow, @function
_IO_str_overflow:
	movl	(%rdi), %edx
	testb	$8, %dl
	je	.L9
	xorl	%eax, %eax
	cmpl	$-1, %esi
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%r15
	movl	%edx, %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	andl	$3072, %eax
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$1024, %eax
	je	.L11
	movq	40(%rdi), %rdx
.L12:
	movq	56(%rdi), %r15
	movq	64(%rdi), %r12
	xorl	%eax, %eax
	movq	%rdx, %rcx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	%r15, %r12
	cmpl	$-1, %esi
	sete	%al
	subq	32(%rdi), %rcx
	addq	%r12, %rax
	cmpq	%rcx, %rax
	ja	.L13
	testb	$1, (%rdi)
	jne	.L15
	leaq	100(%r12,%r12), %r14
	cmpq	%r14, %r12
	ja	.L15
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L15
	testq	%r15, %r15
	je	.L16
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r15, %rdi
	call	free@PLT
	movq	$0, 56(%rbx)
.L16:
	leaq	0(%r13,%r12), %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	subq	%r12, %rdx
	call	memset@PLT
	leaq	0(%r13,%r14), %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_IO_setb
	movq	24(%rbx), %rax
	movq	40(%rbx), %rdx
	movq	%r13, 32(%rbx)
	subq	%r15, %rax
	subq	%r15, %rdx
	addq	%r13, %rax
	addq	%r13, %rdx
	movq	%rax, 24(%rbx)
	movq	8(%rbx), %rax
	movq	%rdx, 40(%rbx)
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 8(%rbx)
	movq	16(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 16(%rbx)
	movq	64(%rbx), %rax
	movq	%rax, 48(%rbx)
.L13:
	cmpl	$-1, %ebp
	je	.L17
	leaq	1(%rdx), %rax
	movq	%rax, 40(%rbx)
	movb	%bpl, (%rdx)
	movq	40(%rbx), %rdx
.L17:
	cmpq	%rdx, 16(%rbx)
	movl	%ebp, %eax
	jnb	.L8
	movq	%rdx, 16(%rbx)
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	orb	$8, %dh
	movq	16(%rdi), %rax
	movl	%edx, (%rdi)
	movq	8(%rdi), %rdx
	movq	%rax, 8(%rdi)
	movq	%rdx, 40(%rdi)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$-1, %eax
	jmp	.L8
	.size	_IO_str_overflow, .-_IO_str_overflow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"strops.c"
.LC1:
	.string	"offset >= oldend"
	.text
	.p2align 4,,15
	.type	enlarge_userbuf, @function
enlarge_userbuf:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	56(%rdi), %rbp
	movq	64(%rdi), %rbx
	subq	%rbp, %rbx
	cmpq	%rsi, %rbx
	jge	.L40
	movl	(%rdi), %r15d
	andl	$1, %r15d
	je	.L33
.L34:
	movl	$1, %r15d
.L31:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r15d, %r15d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L33:
	movq	32(%rdi), %rax
	movq	48(%rdi), %r13
	movq	%rdi, %r14
	movl	%edx, 28(%rsp)
	movq	%rsi, (%rsp)
	movq	%rax, 8(%rsp)
	leaq	100(%rsi), %rax
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L34
	testq	%rbp, %rbp
	je	.L35
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rbp, %rdi
	call	free@PLT
	movq	$0, 56(%r14)
.L35:
	movq	16(%rsp), %rdx
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	subq	8(%rsp), %r13
	addq	%r12, %rdx
	call	_IO_setb
	movl	28(%rsp), %eax
	testl	%eax, %eax
	je	.L36
	movq	32(%r14), %rax
	movq	%r12, 24(%r14)
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 32(%r14)
	movq	40(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 40(%r14)
	movq	48(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 48(%r14)
	movq	8(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	cmpq	%r13, (%rsp)
	movq	%rax, 8(%r14)
	movq	64(%r14), %rax
	movq	%rax, 16(%r14)
	jl	.L39
	movq	(%rsp), %rdx
	leaq	(%r12,%r13), %rdi
	xorl	%esi, %esi
	subq	%r13, %rdx
	call	memset@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movq	24(%r14), %rax
	movq	%r12, 32(%r14)
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 24(%r14)
	movq	8(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 8(%r14)
	movq	16(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	movq	%rax, 16(%r14)
	movq	40(%r14), %rax
	subq	%rbp, %rax
	addq	%r12, %rax
	cmpq	%r13, (%rsp)
	movq	%rax, 40(%r14)
	movq	64(%r14), %rax
	movq	%rax, 48(%r14)
	jl	.L39
	movq	(%rsp), %rdx
	leaq	(%r12,%r13), %rdi
	xorl	%esi, %esi
	xorl	%r15d, %r15d
	subq	%r13, %rdx
	call	memset@PLT
	jmp	.L31
.L39:
	leaq	__PRETTY_FUNCTION__.10863(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$222, %edx
	call	__assert_fail
	.size	enlarge_userbuf, .-enlarge_userbuf
	.p2align 4,,15
	.globl	_IO_str_seekoff
	.hidden	_IO_str_seekoff
	.type	_IO_str_seekoff, @function
_IO_str_seekoff:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	testl	%ecx, %ecx
	movl	(%rdi), %eax
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	jne	.L49
	movl	%eax, %ecx
	andl	$1024, %ecx
	je	.L50
	testb	$8, %ah
	jne	.L76
	cmpq	%rdx, %rsi
	jb	.L80
	cmpq	%rdx, 16(%rdi)
	movq	24(%rdi), %rsi
	movl	$1, %ecx
	cmovnb	16(%rdi), %rdx
	movq	%rdx, %r12
	subq	%rsi, %r12
.L75:
	testl	%r14d, %r14d
	je	.L59
	cmpl	$1, %r14d
	movq	%r12, %rbp
	jne	.L58
	movq	8(%rbx), %rbp
	subq	%rsi, %rbp
.L58:
	movq	%rbp, %rax
	negq	%rax
	cmpq	%r13, %rax
	jg	.L68
	movabsq	$9223372036854775807, %rax
	subq	%rbp, %rax
	cmpq	%r13, %rax
	setl	%al
	addq	%r13, %rbp
	testb	%al, %al
	jne	.L68
.L74:
	cmpq	%rbp, %r12
	jl	.L91
.L63:
	leaq	(%rsi,%rbp), %rax
	addq	%r12, %rsi
	movq	%rsi, 16(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	%rdx, %rsi
	jnb	.L92
.L51:
	testb	$1, %ah
	jne	.L72
.L93:
	movq	56(%rbx), %rsi
	movq	%rsi, 24(%rbx)
.L55:
	andb	$-9, %ah
	testl	%ecx, %ecx
	movq	%rdx, 16(%rbx)
	movl	%eax, (%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rsi, %rax
	jne	.L54
.L53:
	movq	8(%rbx), %rbp
	subq	%rax, %rbp
.L48:
	addq	$16, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	80(%rbx), %rsi
	movq	%rsi, 24(%rbx)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L92:
	testb	$8, %ah
	jne	.L51
	movq	24(%rdi), %rsi
.L54:
	cmpq	%rdx, 16(%rbx)
	movq	$-1, %rbp
	cmovnb	16(%rbx), %rdx
	movq	%rdx, %r12
	subq	%rsi, %r12
	testb	$1, %cl
	jne	.L75
.L57:
	andl	$2, %ecx
	je	.L48
	testl	%r14d, %r14d
	je	.L79
	cmpl	$1, %r14d
	jne	.L88
	movq	40(%rbx), %rbp
	subq	32(%rbx), %rbp
	movabsq	$9223372036854775807, %rdx
	subq	%rbp, %rdx
	movq	%rbp, %rax
	negq	%rax
	cmpq	%rdx, %r13
	setg	%dl
.L66:
	cmpq	%rax, %r13
	jl	.L68
	testb	%dl, %dl
	jne	.L68
	addq	%r13, %rbp
	cmpq	%rbp, %r12
	jl	.L69
.L71:
	movq	32(%rbx), %rax
	addq	%rbp, %rax
	movq	%rax, 40(%rbx)
	addq	$16, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	cmpq	%rdx, %rsi
	jb	.L51
	testb	$8, %ah
	jne	.L51
	movq	24(%rdi), %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L76:
	testb	$1, %ah
	movl	$2, %ecx
	jne	.L72
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%edx, %edx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L59:
	testq	%r13, %r13
	js	.L68
	movq	%r13, %rbp
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L88:
	movabsq	$9223372036854775807, %rdx
	movq	%r12, %rax
	movq	%r12, %rbp
	subq	%r12, %rdx
	negq	%rax
	cmpq	%rdx, %r13
	setg	%dl
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	enlarge_userbuf
	testl	%eax, %eax
	je	.L71
.L70:
	movq	$-1, %rbp
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	%ecx, 12(%rsp)
	call	enlarge_userbuf
	testl	%eax, %eax
	movl	12(%rsp), %ecx
	jne	.L70
	movq	24(%rbx), %rsi
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1, %ecx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L68:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbp
	movl	$22, %fs:(%rax)
	jmp	.L48
	.size	_IO_str_seekoff, .-_IO_str_seekoff
	.p2align 4,,15
	.globl	_IO_str_pbackfail
	.hidden	_IO_str_pbackfail
	.type	_IO_str_pbackfail, @function
_IO_str_pbackfail:
	testb	$8, (%rdi)
	je	.L97
	cmpl	$-1, %esi
	jne	.L95
.L97:
	jmp	_IO_default_pbackfail
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$-1, %eax
	ret
	.size	_IO_str_pbackfail, .-_IO_str_pbackfail
	.p2align 4,,15
	.globl	_IO_str_finish
	.type	_IO_str_finish, @function
_IO_str_finish:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L102
	testb	$1, (%rbx)
	je	.L107
.L102:
	movq	$0, 56(%rbx)
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	jmp	_IO_default_finish
	.p2align 4,,10
	.p2align 3
.L107:
	call	free@PLT
	jmp	.L102
	.size	_IO_str_finish, .-_IO_str_finish
	.p2align 4,,15
	.globl	_IO_str_init_static_internal
	.type	_IO_str_init_static_internal, @function
_IO_str_init_static_internal:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	jne	.L109
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__rawmemchr
	movq	%rax, %r12
.L110:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_setb
	testq	%r13, %r13
	movq	%rbp, 32(%rbx)
	movq	%rbp, 24(%rbx)
	movq	%rbp, 8(%rbx)
	je	.L111
	movq	%r13, 40(%rbx)
	movq	%r12, 48(%rbx)
	movq	%r13, 16(%rbx)
.L112:
	movq	$0, 224(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	addq	%rsi, %rdx
	movq	$-1, %r12
	cmpq	%rdx, %rsi
	cmovb	%rdx, %r12
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%rbp, 40(%rbx)
	movq	%rbp, 48(%rbx)
	movq	%r12, 16(%rbx)
	jmp	.L112
	.size	_IO_str_init_static_internal, .-_IO_str_init_static_internal
	.p2align 4,,15
	.globl	_IO_str_init_static
	.type	_IO_str_init_static, @function
_IO_str_init_static:
	testl	%edx, %edx
	movl	$-1, %eax
	cmovs	%eax, %edx
	movslq	%edx, %rdx
	jmp	_IO_str_init_static_internal
	.size	_IO_str_init_static, .-_IO_str_init_static
	.p2align 4,,15
	.globl	_IO_str_init_readonly
	.type	_IO_str_init_readonly, @function
_IO_str_init_readonly:
	testl	%edx, %edx
	movl	$-1, %eax
	pushq	%rbx
	cmovs	%eax, %edx
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movslq	%edx, %rdx
	call	_IO_str_init_static_internal
	orl	$8, (%rbx)
	popq	%rbx
	ret
	.size	_IO_str_init_readonly, .-_IO_str_init_readonly
	.p2align 4,,15
	.globl	_IO_str_count
	.type	_IO_str_count, @function
_IO_str_count:
	movq	40(%rdi), %rax
	cmpq	%rax, 16(%rdi)
	cmovnb	16(%rdi), %rax
	subq	24(%rdi), %rax
	ret
	.size	_IO_str_count, .-_IO_str_count
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10863, @object
	.size	__PRETTY_FUNCTION__.10863, 16
__PRETTY_FUNCTION__.10863:
	.string	"enlarge_userbuf"
	.hidden	_IO_str_jumps
	.globl	_IO_str_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_str_jumps, @object
	.size	_IO_str_jumps, 168
_IO_str_jumps:
	.quad	0
	.quad	0
	.quad	_IO_str_finish
	.quad	_IO_str_overflow
	.quad	_IO_str_underflow
	.quad	_IO_default_uflow
	.quad	_IO_str_pbackfail
	.quad	_IO_default_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_str_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_default_doallocate
	.hidden	_IO_default_xsgetn
	.hidden	_IO_default_xsputn
	.hidden	_IO_default_uflow
	.hidden	__rawmemchr
	.hidden	_IO_default_finish
	.hidden	_IO_default_pbackfail
	.hidden	__assert_fail
	.hidden	_IO_setb
