	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	putchar_unlocked
	.type	putchar_unlocked, @function
putchar_unlocked:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	(%rax), %rdx
	movzbl	%dil, %eax
	movq	40(%rdx), %rcx
	cmpq	48(%rdx), %rcx
	jnb	.L4
	leaq	1(%rcx), %rsi
	movq	%rsi, 40(%rdx)
	movb	%dil, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %esi
	movq	%rdx, %rdi
	jmp	__GI___overflow
	.size	putchar_unlocked, .-putchar_unlocked
