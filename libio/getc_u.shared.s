	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getc_unlocked
	.hidden	__getc_unlocked
	.type	__getc_unlocked, @function
__getc_unlocked:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L4
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	__GI___uflow
	.size	__getc_unlocked, .-__getc_unlocked
	.weak	__GI_getc_unlocked
	.hidden	__GI_getc_unlocked
	.set	__GI_getc_unlocked,__getc_unlocked
	.weak	getc_unlocked
	.set	getc_unlocked,__GI_getc_unlocked
	.weak	fgetc_unlocked
	.set	fgetc_unlocked,__getc_unlocked
