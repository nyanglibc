	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_vsscanf
	.type	_IO_vsscanf, @function
_IO_vsscanf:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	subq	$248, %rsp
	movl	$-1, %edx
	movl	$32768, %esi
	movq	%rsp, %rbx
	movq	$0, 136(%rsp)
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	leaq	_IO_str_jumps(%rip), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, 216(%rsp)
	call	_IO_str_init_static_internal@PLT
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	call	__vfscanf_internal
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	_IO_vsscanf, .-_IO_vsscanf
	.weak	vsscanf
	.set	vsscanf,_IO_vsscanf
	.weak	__vsscanf
	.set	__vsscanf,_IO_vsscanf
	.hidden	__vfscanf_internal
	.hidden	_IO_str_jumps
