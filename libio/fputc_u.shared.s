	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_fputc_unlocked
	.hidden	__GI_fputc_unlocked
	.type	__GI_fputc_unlocked, @function
__GI_fputc_unlocked:
	movq	40(%rsi), %rcx
	cmpq	48(%rsi), %rcx
	movq	%rsi, %rdx
	movzbl	%dil, %eax
	jnb	.L4
	leaq	1(%rcx), %rsi
	movq	%rsi, 40(%rdx)
	movb	%dil, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %esi
	movq	%rdx, %rdi
	jmp	__GI___overflow
	.size	__GI_fputc_unlocked, .-__GI_fputc_unlocked
	.globl	fputc_unlocked
	.set	fputc_unlocked,__GI_fputc_unlocked
