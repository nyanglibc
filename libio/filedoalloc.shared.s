	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__IO_file_doallocate
	.hidden	__GI__IO_file_doallocate
	.type	__GI__IO_file_doallocate, @function
__GI__IO_file_doallocate:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$144, %rsp
	movl	112(%rdi), %eax
	testl	%eax, %eax
	js	.L2
	movq	216(%rdi), %rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L15
.L3:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	call	*144(%rbx)
	testl	%eax, %eax
	js	.L2
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	je	.L16
.L5:
	movq	56(%rsp), %rbx
	leaq	-1(%rbx), %rax
	cmpq	$8190, %rax
	ja	.L2
.L9:
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	movl	$-1, %eax
	testq	%rsi, %rsi
	je	.L1
	leaq	(%rsi,%rbx), %rdx
	movl	$1, %ecx
	movq	%rbp, %rdi
	call	__GI__IO_setb
	movl	$1, %eax
.L1:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$8192, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L16:
	movq	40(%rsp), %rax
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$8, %rdx
	andl	$-4096, %eax
	andl	$4095, %edx
	orl	%edx, %eax
	subl	$136, %eax
	cmpl	$7, %eax
	ja	.L6
.L7:
	orl	$512, 0(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	112(%rbp), %edi
	movl	%fs:(%rbx), %r12d
	call	__isatty
	testl	%eax, %eax
	movl	%r12d, %fs:(%rbx)
	jne	.L7
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L15:
	call	_IO_vtable_check
	jmp	.L3
	.size	__GI__IO_file_doallocate, .-__GI__IO_file_doallocate
	.globl	_IO_file_doallocate
	.set	_IO_file_doallocate,__GI__IO_file_doallocate
	.hidden	_IO_vtable_check
	.hidden	__isatty
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
