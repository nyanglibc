	.text
	.p2align 4,,15
	.type	_IO_mem_sync, @function
_IO_mem_sync:
	movq	48(%rdi), %rax
	cmpq	%rax, 40(%rdi)
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L5
.L2:
	movq	32(%rbx), %rdx
	movq	240(%rbx), %rax
	movq	%rdx, (%rax)
	movq	40(%rbx), %rax
	subq	32(%rbx), %rax
	movq	248(%rbx), %rdx
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%esi, %esi
	call	_IO_str_overflow
	subq	$1, 40(%rbx)
	jmp	.L2
	.size	_IO_mem_sync, .-_IO_mem_sync
	.p2align 4,,15
	.type	_IO_mem_finish, @function
_IO_mem_finish:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	movq	40(%rbx), %rsi
	movq	240(%rbx), %rbp
	subq	%rdi, %rsi
	addq	$1, %rsi
	call	realloc@PLT
	movq	%rax, 0(%rbp)
	movq	240(%rbx), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L7
	movq	40(%rbx), %rax
	subq	32(%rbx), %rax
	movb	$0, (%rdx,%rax)
	movq	40(%rbx), %rax
	subq	32(%rbx), %rax
	movq	248(%rbx), %rdx
	movq	%rax, (%rdx)
	movq	$0, 56(%rbx)
.L7:
	addq	$8, %rsp
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	_IO_str_finish@PLT
	.size	_IO_mem_finish, .-_IO_mem_finish
	.p2align 4,,15
	.globl	__open_memstream
	.hidden	__open_memstream
	.type	__open_memstream, @function
__open_memstream:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	$504, %edi
	movq	%rsi, %r12
	subq	$8, %rsp
	call	malloc@PLT
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rbx
	leaq	256(%rax), %rax
	movl	$8192, %esi
	movl	$1, %edi
	movq	%rax, 136(%rbx)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L17
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_init_internal
	leaq	_IO_mem_jumps(%rip), %rax
	movq	%rbp, %rcx
	movl	$8192, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	call	_IO_str_init_static_internal@PLT
	movq	malloc@GOTPCREL(%rip), %rax
	andl	$-2, (%rbx)
	orl	$128, 116(%rbx)
	movq	%r13, 240(%rbx)
	movq	%r12, 248(%rbx)
	movq	%rax, 224(%rbx)
	movq	free@GOTPCREL(%rip), %rax
	movq	%rax, 232(%rbx)
.L12:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%ebx, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L12
	.size	__open_memstream, .-__open_memstream
	.weak	open_memstream
	.set	open_memstream,__open_memstream
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_mem_jumps, @object
	.size	_IO_mem_jumps, 168
_IO_mem_jumps:
	.quad	0
	.quad	0
	.quad	_IO_mem_finish
	.quad	_IO_str_overflow
	.quad	_IO_str_underflow
	.quad	_IO_default_uflow
	.quad	_IO_str_pbackfail
	.quad	_IO_default_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_str_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_mem_sync
	.quad	_IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_default_doallocate
	.hidden	_IO_str_seekoff
	.hidden	_IO_default_xsgetn
	.hidden	_IO_default_xsputn
	.hidden	_IO_str_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_str_underflow
	.hidden	_IO_init_internal
	.hidden	_IO_str_overflow
