	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _IO_new_fgetpos,_IO_fgetpos@@GLIBC_2.2.5
	.symver __new_fgetpos,fgetpos@@GLIBC_2.2.5
	.symver _IO_new_fgetpos64,_IO_fgetpos64@@GLIBC_2.2.5
	.symver __new_fgetpos64,fgetpos64@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_IO_new_fgetpos
	.type	_IO_new_fgetpos, @function
_IO_new_fgetpos:
.LFB68:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L3
#APP
# 50 "iofgetpos.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekoff_unlocked
	movl	(%rbx), %edx
	movl	%edx, %r8d
	andl	$256, %r8d
	je	.L8
	cmpq	$-1, %rax
	je	.L9
	movl	192(%rbx), %esi
	testl	%esi, %esi
	jle	.L34
	movq	%rax, 0(%rbp)
.L21:
	movq	152(%rbx), %rdi
	call	__libio_codecvt_encoding
	xorl	%r8d, %r8d
	testl	%eax, %eax
	movl	(%rbx), %edx
	jns	.L13
	movq	160(%rbx), %rax
	movq	88(%rax), %rax
	movq	%rax, 8(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L8:
	cmpq	$-1, %rax
	je	.L9
	movq	%rax, 0(%rbp)
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jg	.L21
.L13:
	andb	$-128, %dh
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	%fs:(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L13
	movl	$5, %fs:(%rax)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L34:
	movq	88(%rbx), %rcx
	subq	72(%rbx), %rcx
	subq	%rcx, %rax
	cmpq	$-1, %rax
	je	.L9
	movq	%rax, 0(%rbp)
	xorl	%r8d, %r8d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L24:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L19
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L19
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L20
	subl	$1, (%rdi)
.L19:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L20:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L24-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_new_fgetpos, .-_IO_new_fgetpos
	.globl	_IO_new_fgetpos64
	.set	_IO_new_fgetpos64,_IO_new_fgetpos
	.globl	__new_fgetpos64
	.set	__new_fgetpos64,_IO_new_fgetpos64
	.globl	__new_fgetpos
	.set	__new_fgetpos,_IO_new_fgetpos
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__libio_codecvt_encoding
	.hidden	_IO_seekoff_unlocked
