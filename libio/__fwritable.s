	.text
	.p2align 4,,15
	.globl	__fwritable
	.type	__fwritable, @function
__fwritable:
	xorl	%eax, %eax
	testb	$8, (%rdi)
	sete	%al
	ret
	.size	__fwritable, .-__fwritable
