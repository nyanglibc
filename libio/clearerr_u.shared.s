	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	clearerr_unlocked
	.type	clearerr_unlocked, @function
clearerr_unlocked:
	andl	$-49, (%rdi)
	ret
	.size	clearerr_unlocked, .-clearerr_unlocked
