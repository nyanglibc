	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	setbuf
	.type	setbuf, @function
setbuf:
	movl	$8192, %edx
	jmp	__GI__IO_setbuffer
	.size	setbuf, .-setbuf
