	.text
	.p2align 4,,15
	.globl	_IO_wpadn
	.type	_IO_wpadn, @function
_IO_wpadn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	blanks(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$88, %rsp
	cmpl	$32, %esi
	je	.L2
	cmpl	$48, %esi
	leaq	zeroes(%rip), %r13
	je	.L2
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %rcx
	leaq	60(%r13), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%esi, (%rax)
	subq	$4, %rax
	cmpq	%rcx, %rax
	jne	.L3
.L2:
	cmpl	$15, %edx
	movl	%edx, %r15d
	jle	.L13
	movl	%edx, %r12d
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r14
	xorl	%ebx, %ebx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %r14
	andl	$15, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*56(%rax)
	addq	%rax, %rbx
	cmpq	$16, %rax
	jne	.L1
	subl	$16, %r15d
	cmpl	%r12d, %r15d
	je	.L4
.L8:
	movq	216(%rbp), %rax
	movq	%rax, %rdx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	cmpq	%rdx, %r14
	ja	.L5
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r15d, %r15d
	jle	.L1
	movq	216(%rbp), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L20
.L10:
	movslq	%r15d, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*56(%r12)
	addq	%rax, %rbx
.L1:
	addq	$88, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L20:
	call	_IO_vtable_check
	jmp	.L10
	.size	_IO_wpadn, .-_IO_wpadn
	.section	.rodata
	.align 32
	.type	zeroes, @object
	.size	zeroes, 64
zeroes:
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.long	48
	.align 32
	.type	blanks, @object
	.size	blanks, 64
blanks:
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.long	32
	.weak	__start___libc_IO_vtables
	.weak	__stop___libc_IO_vtables
	.hidden	_IO_vtable_check
