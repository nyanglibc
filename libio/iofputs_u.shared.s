	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fputs_unlocked
	.hidden	__GI___fputs_unlocked
	.type	__GI___fputs_unlocked, @function
__GI___fputs_unlocked:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__GI_strlen
	movq	%rax, %r12
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L2
	movl	$-1, 192(%rbx)
.L3:
	movq	216(%rbx), %r13
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L12
.L5:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r12, %rax
	jne	.L6
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$-1, %eax
	je	.L3
.L6:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	call	_IO_vtable_check
	jmp	.L5
	.size	__GI___fputs_unlocked, .-__GI___fputs_unlocked
	.globl	__fputs_unlocked
	.set	__fputs_unlocked,__GI___fputs_unlocked
	.weak	__GI_fputs_unlocked
	.hidden	__GI_fputs_unlocked
	.set	__GI_fputs_unlocked,__fputs_unlocked
	.weak	fputs_unlocked
	.set	fputs_unlocked,__GI_fputs_unlocked
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
