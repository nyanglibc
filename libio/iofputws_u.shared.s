	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_fputws_unlocked
	.hidden	__GI_fputws_unlocked
	.type	__GI_fputws_unlocked, @function
__GI_fputws_unlocked:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbx
	call	__wcslen@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L5
	movq	216(%rbx), %r14
	leaq	__start___libc_IO_vtables(%rip), %rdx
	movl	%eax, %ebp
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%r14, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L8
.L4:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%r13, %rax
	jne	.L5
.L1:
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	call	_IO_vtable_check
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %ebp
	jmp	.L1
	.size	__GI_fputws_unlocked, .-__GI_fputws_unlocked
	.globl	fputws_unlocked
	.set	fputws_unlocked,__GI_fputws_unlocked
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
