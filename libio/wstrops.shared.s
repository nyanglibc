	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_wstr_underflow
	.type	_IO_wstr_underflow, @function
_IO_wstr_underflow:
	movq	160(%rdi), %rax
	movq	32(%rax), %rdx
	movq	8(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L2
	movq	%rdx, 8(%rax)
	movq	%rdx, %rcx
.L2:
	movl	(%rdi), %esi
	movl	%esi, %r8d
	andl	$3072, %r8d
	cmpl	$3072, %r8d
	je	.L3
	movq	(%rax), %rdx
.L4:
	cmpq	%rcx, %rdx
	movl	$-1, %eax
	jnb	.L1
	movl	(%rdx), %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L3:
	andl	$-2049, %esi
	movl	%esi, (%rdi)
	movq	40(%rax), %rsi
	movq	%rdx, (%rax)
	movq	%rsi, 32(%rax)
	jmp	.L4
	.size	_IO_wstr_underflow, .-_IO_wstr_underflow
	.p2align 4,,15
	.globl	_IO_wstr_overflow
	.type	_IO_wstr_overflow, @function
_IO_wstr_overflow:
	movl	(%rdi), %ecx
	testb	$8, %cl
	je	.L9
	xorl	%eax, %eax
	cmpl	$-1, %esi
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	%r15
	movl	%ecx, %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	andl	$3072, %eax
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$1024, %eax
	movq	160(%rdi), %rdx
	je	.L11
	movq	32(%rdx), %rcx
.L12:
	movq	48(%rdx), %r13
	movq	56(%rdx), %rbp
	xorl	%r8d, %r8d
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movl	%esi, %r12d
	subq	%r13, %rbp
	movq	%rbp, %r14
	sarq	$2, %r14
	cmpl	$-1, %esi
	sete	%r8b
	subq	24(%rdx), %rdi
	addq	%r14, %r8
	sarq	$2, %rdi
	cmpq	%rdi, %r8
	jbe	.L13
	movq	8(%rdx), %rsi
.L14:
	cmpl	$-1, %r12d
	je	.L18
	leaq	4(%rcx), %rax
	movq	%rax, 32(%rdx)
	movl	%r12d, (%rcx)
	movq	%rax, %rcx
.L18:
	cmpq	%rsi, %rcx
	movl	%r12d, %eax
	jbe	.L8
	movq	%rcx, 8(%rdx)
.L8:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testb	$8, 116(%rbx)
	jne	.L16
	leaq	50(%r14), %r15
	movabsq	$4611686018427387903, %rax
	leaq	(%r15,%r15), %rcx
	cmpq	%rax, %rcx
	ja	.L16
	cmpq	%rcx, %r14
	movq	%rcx, (%rsp)
	ja	.L16
	leaq	0(,%r15,8), %rax
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L16
	testq	%r13, %r13
	movq	(%rsp), %rcx
	je	.L17
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__wmemcpy
	movq	%r13, %rdi
	call	free@PLT
	movq	160(%rbx), %rax
	movq	(%rsp), %rcx
	movq	$0, 48(%rax)
.L17:
	movq	%rcx, %rdx
	leaq	(%r15,%rbp), %rdi
	xorl	%esi, %esi
	subq	%r14, %rdx
	call	__GI___wmemset
	movq	8(%rsp), %rdx
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	addq	%r15, %rdx
	call	__GI__IO_wsetb
	movq	160(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	8(%rdx), %rsi
	movq	32(%rdx), %rcx
	movq	%r15, 24(%rdx)
	subq	%r13, %rax
	subq	%r13, %rsi
	addq	%r15, %rax
	subq	%r13, %rcx
	addq	%r15, %rsi
	movq	%rax, 16(%rdx)
	movq	(%rdx), %rax
	addq	%r15, %rcx
	movq	%rsi, 8(%rdx)
	movq	%rcx, 32(%rdx)
	subq	%r13, %rax
	addq	%r15, %rax
	movq	%rax, (%rdx)
	movq	56(%rdx), %rax
	movq	%rax, 40(%rdx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	orb	$8, %ch
	movq	8(%rdx), %rax
	movl	%ecx, (%rdi)
	movq	(%rdx), %rcx
	movq	%rax, (%rdx)
	movq	%rcx, 32(%rdx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	_IO_wstr_overflow, .-_IO_wstr_overflow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wstrops.c"
.LC1:
	.string	"offset >= oldend"
	.text
	.p2align 4,,15
	.type	enlarge_userbuf, @function
enlarge_userbuf:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	160(%rdi), %rbx
	movq	48(%rbx), %r15
	movq	56(%rbx), %rcx
	subq	%r15, %rcx
	sarq	$2, %rcx
	cmpq	%rsi, %rcx
	jge	.L41
	movl	116(%rdi), %ebp
	andl	$8, %ebp
	jne	.L35
	leaq	100(%rsi), %r12
	movabsq	$4611686018427387903, %rax
	movq	%rcx, 40(%rsp)
	cmpq	%rax, %r12
	ja	.L35
	movq	24(%rbx), %rax
	salq	$2, %r12
	movq	%rdi, 24(%rsp)
	movq	%r12, %rdi
	movl	%edx, 36(%rsp)
	movq	%rsi, 8(%rsp)
	movq	40(%rbx), %r14
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L35
	testq	%r15, %r15
	je	.L36
	movq	40(%rsp), %rcx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rcx, %rdx
	call	__wmemcpy
	movq	%r15, %rdi
	call	free@PLT
	movq	$0, 48(%rbx)
.L36:
	subq	16(%rsp), %r14
	movq	24(%rsp), %rdi
	leaq	0(%r13,%r12), %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %r8
	sarq	$2, %r8
	movq	%r8, 16(%rsp)
	call	__GI__IO_wsetb
	movl	36(%rsp), %eax
	movq	16(%rsp), %r8
	testl	%eax, %eax
	je	.L37
	movq	24(%rbx), %rax
	movq	%r13, 16(%rbx)
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 24(%rbx)
	movq	32(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 40(%rbx)
	movq	(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	cmpq	%r8, 8(%rsp)
	movq	%rax, (%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 8(%rbx)
	jl	.L40
	movq	8(%rsp), %rdx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	subq	%r8, %rdx
	call	__GI___wmemset
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %ebp
.L32:
	addq	$56, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%ebp, %ebp
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%rbx), %rax
	movq	%r13, 24(%rbx)
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 16(%rbx)
	movq	(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, (%rbx)
	movq	8(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	movq	%rax, 8(%rbx)
	movq	32(%rbx), %rax
	subq	%r15, %rax
	addq	%r13, %rax
	cmpq	%r8, 8(%rsp)
	movq	%rax, 32(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 40(%rbx)
	jl	.L40
	movq	8(%rsp), %rdx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	xorl	%ebp, %ebp
	subq	%r8, %rdx
	call	__GI___wmemset
	jmp	.L32
.L40:
	leaq	__PRETTY_FUNCTION__.11554(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$225, %edx
	call	__GI___assert_fail
	.size	enlarge_userbuf, .-enlarge_userbuf
	.p2align 4,,15
	.globl	_IO_wstr_pbackfail
	.type	_IO_wstr_pbackfail, @function
_IO_wstr_pbackfail:
	testb	$8, (%rdi)
	je	.L52
	cmpl	$-1, %esi
	jne	.L50
.L52:
	jmp	__GI__IO_wdefault_pbackfail
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-1, %eax
	ret
	.size	_IO_wstr_pbackfail, .-_IO_wstr_pbackfail
	.p2align 4,,15
	.globl	_IO_wstr_finish
	.type	_IO_wstr_finish, @function
_IO_wstr_finish:
	pushq	%rbx
	movq	160(%rdi), %rax
	movq	%rdi, %rbx
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L57
	testb	$8, 116(%rbx)
	je	.L62
.L57:
	movq	$0, 48(%rax)
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	jmp	__GI__IO_wdefault_finish
	.p2align 4,,10
	.p2align 3
.L62:
	call	free@PLT
	movq	160(%rbx), %rax
	jmp	.L57
	.size	_IO_wstr_finish, .-_IO_wstr_finish
	.p2align 4,,15
	.globl	_IO_wstr_init_static
	.type	_IO_wstr_init_static, @function
_IO_wstr_init_static:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	jne	.L64
	movq	%rsi, %rdi
	call	__wcslen@PLT
	leaq	(%rbx,%rax,4), %rbp
.L65:
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__GI__IO_wsetb
	movq	160(%r12), %rax
	testq	%r13, %r13
	movq	%rbx, 24(%rax)
	movq	%rbx, 16(%rax)
	movq	%rbx, (%rax)
	je	.L67
	movq	%r13, 32(%rax)
	movq	%rbp, 40(%rax)
	movq	%r13, 8(%rax)
.L68:
	movq	$0, 224(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	(%rsi,%rdx,4), %rbp
	cmpq	%rbp, %rsi
	jb	.L65
	movq	%rsi, %rbp
	notq	%rbp
	andq	$-4, %rbp
	addq	%rsi, %rbp
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rbx, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	%rbp, 8(%rax)
	jmp	.L68
	.size	_IO_wstr_init_static, .-_IO_wstr_init_static
	.p2align 4,,15
	.globl	_IO_wstr_count
	.type	_IO_wstr_count, @function
_IO_wstr_count:
	movq	160(%rdi), %rdx
	movq	32(%rdx), %rax
	cmpq	%rax, 8(%rdx)
	cmovnb	8(%rdx), %rax
	subq	16(%rdx), %rax
	sarq	$2, %rax
	ret
	.size	_IO_wstr_count, .-_IO_wstr_count
	.p2align 4,,15
	.globl	_IO_wstr_seekoff
	.type	_IO_wstr_seekoff, @function
_IO_wstr_seekoff:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$16, %rsp
	movq	160(%rdi), %rsi
	testl	%ecx, %ecx
	movl	(%rdi), %edx
	movq	32(%rsi), %rax
	movq	24(%rsi), %rdi
	jne	.L72
	movl	%edx, %ecx
	andl	$1024, %ecx
	je	.L73
	testb	$8, %dh
	jne	.L98
	cmpq	%rax, %rdi
	jb	.L102
	movq	%rbp, %rdi
	call	_IO_wstr_count@PLT
	movl	$1, %ecx
	movq	%rax, %r14
.L97:
	testl	%r13d, %r13d
	je	.L100
	cmpl	$1, %r13d
	je	.L83
	movq	%r14, %rdx
	movabsq	$2305843009213693951, %rax
	movq	%r14, %rbx
	negq	%rdx
	subq	%r14, %rax
.L82:
	cmpq	%rdx, %r12
	jl	.L91
	cmpq	%rax, %r12
	jg	.L91
	addq	%r12, %rbx
	cmpq	%r14, %rbx
	jg	.L86
.L87:
	movq	160(%rbp), %rax
	andl	$2, %ecx
	movq	16(%rax), %rdx
	leaq	(%rdx,%rbx,4), %rsi
	leaq	(%rdx,%r14,4), %rdx
	movq	%r12, %rbx
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	je	.L71
.L116:
	testl	%r13d, %r13d
	je	.L101
	cmpl	$1, %r13d
	jne	.L111
	movq	160(%rbp), %rax
	movq	32(%rax), %rbx
	subq	24(%rax), %rbx
	movabsq	$2305843009213693951, %rax
	sarq	$2, %rbx
	movq	%rbx, %rdx
	subq	%rbx, %rax
	negq	%rdx
.L89:
	cmpq	%rdx, %r12
	jl	.L91
	cmpq	%rax, %r12
	jg	.L91
	addq	%r12, %rbx
	cmpq	%r14, %rbx
	jg	.L92
.L94:
	movq	160(%rbp), %rax
	movq	24(%rax), %rdx
	leaq	(%rdx,%rbx,4), %rdx
	movq	%rdx, 32(%rax)
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	cmpq	%rax, %rdi
	jnb	.L115
.L74:
	testb	$1, %dh
	jne	.L95
.L117:
	movq	48(%rsi), %r8
	movq	%r8, 16(%rsi)
.L78:
	andb	$-9, %dh
	testl	%ecx, %ecx
	movq	%rax, (%rsi)
	movq	%rax, 8(%rsi)
	movl	%edx, 0(%rbp)
	jne	.L77
.L76:
	subq	%rdi, %rax
	sarq	$2, %rax
	movq	%rax, %rbx
.L71:
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movq	72(%rsi), %r8
	movq	%r8, 16(%rsi)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L115:
	testb	$8, %dh
	jne	.L74
.L77:
	movq	%rbp, %rdi
	movl	%ecx, 12(%rsp)
	movq	$-1, %rbx
	call	_IO_wstr_count@PLT
	movl	12(%rsp), %ecx
	movq	%rax, %r14
	testb	$1, %cl
	jne	.L97
	andl	$2, %ecx
	jne	.L116
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	%rax, %rdi
	jb	.L74
	testb	$8, %dh
	je	.L76
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L98:
	testb	$1, %dh
	movl	$2, %ecx
	jne	.L95
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r14, %rdx
	movabsq	$2305843009213693951, %rax
	movq	%r14, %rbx
	negq	%rdx
	subq	%r14, %rax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L101:
	movabsq	$2305843009213693951, %rax
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$1, %ecx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	enlarge_userbuf
	testl	%eax, %eax
	je	.L94
.L93:
	movq	$-1, %rbx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L83:
	movq	160(%rbp), %rax
	movq	(%rax), %rbx
	subq	16(%rax), %rbx
	movabsq	$2305843009213693951, %rax
	sarq	$2, %rbx
	movq	%rbx, %rdx
	subq	%rbx, %rax
	negq	%rdx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L100:
	movabsq	$2305843009213693951, %rax
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	%ecx, 12(%rsp)
	call	enlarge_userbuf
	testl	%eax, %eax
	movl	12(%rsp), %ecx
	je	.L87
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L91:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbx
	movl	$22, %fs:(%rax)
	jmp	.L71
	.size	_IO_wstr_seekoff, .-_IO_wstr_seekoff
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11554, @object
	.size	__PRETTY_FUNCTION__.11554, 16
__PRETTY_FUNCTION__.11554:
	.string	"enlarge_userbuf"
	.hidden	_IO_wstr_jumps
	.globl	_IO_wstr_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_wstr_jumps, @object
	.size	_IO_wstr_jumps, 168
_IO_wstr_jumps:
	.quad	0
	.quad	0
	.quad	_IO_wstr_finish
	.quad	_IO_wstr_overflow
	.quad	_IO_wstr_underflow
	.quad	__GI__IO_wdefault_uflow
	.quad	_IO_wstr_pbackfail
	.quad	__GI__IO_wdefault_xsputn
	.quad	__GI__IO_wdefault_xsgetn
	.quad	_IO_wstr_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	__GI__IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	__wmemcpy
