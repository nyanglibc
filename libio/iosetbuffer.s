	.text
	.p2align 4,,15
	.globl	_IO_setbuffer
	.hidden	_IO_setbuffer
	.type	_IO_setbuffer, @function
_IO_setbuffer:
.LFB68:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	movl	%edx, %ecx
	andl	$32768, %ecx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L3
#APP
# 33 "iosetbuffer.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L4
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movl	(%rbx), %edx
	movq	%r13, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	andb	$-3, %dh
	movl	$0, %eax
	testq	%r12, %r12
	movq	216(%rbx), %r13
	cmove	%rax, %rbp
	movl	%edx, (%rbx)
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	%r13, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L9
.L11:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	*88(%r13)
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L15
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L15
	movq	224(%rax), %rax
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*88(%rax)
.L15:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	call	_IO_vtable_check
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L22:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L19
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L19
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L20
	subl	$1, (%rdi)
.L19:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L20:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L22-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_setbuffer, .-_IO_setbuffer
	.weak	setbuffer
	.set	setbuffer,_IO_setbuffer
	.weak	__start___libc_IO_vtables
	.weak	__stop___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
