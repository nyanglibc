	.text
	.p2align 4,,15
	.globl	__fopen_maybe_mmap
	.hidden	__fopen_maybe_mmap
	.type	__fopen_maybe_mmap, @function
__fopen_maybe_mmap:
.LFB68:
	testb	$1, 116(%rdi)
	movq	%rdi, %rax
	je	.L2
	testb	$8, (%rdi)
	je	.L2
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jle	.L12
	leaq	_IO_wfile_jumps_maybe_mmap(%rip), %rdx
	movq	%rdx, 216(%rdi)
.L4:
	movq	160(%rax), %rcx
	movq	%rdx, 224(%rcx)
.L2:
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	_IO_file_jumps_maybe_mmap(%rip), %rsi
	leaq	_IO_wfile_jumps_maybe_mmap(%rip), %rdx
	movq	%rsi, 216(%rdi)
	jmp	.L4
.LFE68:
	.size	__fopen_maybe_mmap, .-__fopen_maybe_mmap
	.p2align 4,,15
	.globl	__fopen_internal
	.hidden	__fopen_internal
	.type	__fopen_internal, @function
__fopen_internal:
.LFB69:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	$472, %edi
	movq	%rsi, %r13
	call	malloc@PLT
	testq	%rax, %rax
	je	.L18
	movq	%rax, %rbx
	leaq	224(%rax), %rax
	leaq	_IO_wfile_jumps(%rip), %r8
	leaq	240(%rbx), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, 136(%rbx)
	movq	%rbx, %rdi
	movq	%rbx, %rbp
	call	_IO_no_init@PLT
	leaq	_IO_file_jumps(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	call	_IO_new_file_init_internal
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_IO_file_fopen
	testq	%rax, %rax
	je	.L15
	testb	$1, 116(%rbx)
	je	.L13
	testb	$8, (%rbx)
	jne	.L26
.L13:
	popq	%rbx
	movq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L27
	leaq	_IO_wfile_jumps_maybe_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
.L17:
	movq	160(%rbx), %rdx
	movq	%rax, 224(%rdx)
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%ebp, %ebp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	_IO_file_jumps_maybe_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	_IO_wfile_jumps_maybe_mmap(%rip), %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	_IO_un_link
	movq	%rbx, %rdi
	call	free@PLT
	jmp	.L13
.LFE69:
	.size	__fopen_internal, .-__fopen_internal
	.p2align 4,,15
	.globl	_IO_new_fopen
	.type	_IO_new_fopen, @function
_IO_new_fopen:
.LFB70:
	movl	$1, %edx
	jmp	__fopen_internal
.LFE70:
	.size	_IO_new_fopen, .-_IO_new_fopen
	.weak	fopen64
	.set	fopen64,_IO_new_fopen
	.weak	_IO_fopen64
	.set	_IO_fopen64,_IO_new_fopen
	.weak	_IO_fopen
	.set	_IO_fopen,_IO_new_fopen
	.globl	__new_fopen
	.set	__new_fopen,_IO_new_fopen
	.weak	fopen
	.set	fopen,__new_fopen
	.hidden	_IO_un_link
	.hidden	_IO_file_fopen
	.hidden	_IO_new_file_init_internal
	.hidden	_IO_file_jumps
	.hidden	_IO_wfile_jumps
	.hidden	_IO_file_jumps_maybe_mmap
	.hidden	_IO_wfile_jumps_maybe_mmap
