	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vdprintf_internal
	.hidden	__vdprintf_internal
	.type	__vdprintf_internal, @function
__vdprintf_internal:
	pushq	%r14
	pushq	%r13
	leaq	__GI__IO_wfile_jumps(%rip), %r8
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r13d
	pushq	%rbx
	movl	%edi, %r14d
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movl	$32768, %esi
	xorl	%edx, %edx
	subq	$464, %rsp
	leaq	224(%rsp), %rcx
	movq	%rsp, %rbx
	movq	$0, 136(%rsp)
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	leaq	__GI__IO_file_jumps(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, 216(%rsp)
	call	_IO_new_file_init_internal
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	__GI__IO_file_attach
	testq	%rax, %rax
	je	.L17
	movl	(%rsp), %eax
	movq	%rbp, %rsi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	andl	$-4109, %eax
	orl	$68, %eax
	movl	%eax, (%rsp)
	call	__vfprintf_internal
	cmpl	$-1, %eax
	movl	%eax, %ebp
	je	.L4
	movl	192(%rsp), %eax
	testl	%eax, %eax
	jle	.L18
	movq	160(%rsp), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	__GI__IO_wdo_write
	cmpl	$-1, %eax
	sete	%al
.L6:
	testb	%al, %al
	je	.L7
.L4:
	movl	$-1, %ebp
.L7:
	movq	216(%rsp), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r12, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L19
.L8:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*16(%r12)
.L1:
	addq	$464, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	32(%rsp), %rsi
	movq	40(%rsp), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	__GI__IO_do_write
	cmpl	$-1, %eax
	sete	%al
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	call	_IO_vtable_check
	jmp	.L8
.L17:
	movq	%rbx, %rdi
	movl	$-1, %ebp
	call	__GI__IO_un_link
	jmp	.L1
	.size	__vdprintf_internal, .-__vdprintf_internal
	.p2align 4,,15
	.globl	__vdprintf
	.type	__vdprintf, @function
__vdprintf:
	xorl	%ecx, %ecx
	jmp	__vdprintf_internal
	.size	__vdprintf, .-__vdprintf
	.weak	vdprintf
	.set	vdprintf,__vdprintf
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
	.hidden	__vfprintf_internal
	.hidden	_IO_new_file_init_internal
