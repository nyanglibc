	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fread_unlocked
	.hidden	__GI___fread_unlocked
	.type	__GI___fread_unlocked, @function
__GI___fread_unlocked:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	imulq	%rdx, %rbx
	testq	%rbx, %rbx
	jne	.L9
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%rcx, %rdi
	call	__GI__IO_sgetn
	cmpq	%rax, %rbx
	je	.L3
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI___fread_unlocked, .-__GI___fread_unlocked
	.globl	__fread_unlocked
	.set	__fread_unlocked,__GI___fread_unlocked
	.weak	fread_unlocked
	.set	fread_unlocked,__fread_unlocked
