	.text
	.p2align 4,,15
	.globl	_IO_new_proc_close
	.type	_IO_new_proc_close, @function
_IO_new_proc_close:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %rbp
	testq	%rbp, %rbp
	je	.L2
	leaq	16(%rsp), %rdi
	leaq	unlock(%rip), %rsi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L3:
	movq	%fs:16, %r12
	cmpq	%r12, 8+proc_file_chain_lock(%rip)
	je	.L4
#APP
# 260 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
.L6:
	movq	%r12, 8+proc_file_chain_lock(%rip)
.L4:
	movl	4+proc_file_chain_lock(%rip), %ecx
	movq	proc_file_chain(%rip), %rdx
	movl	$-1, %r12d
	leal	1(%rcx), %eax
	testq	%rdx, %rdx
	movl	%eax, 4+proc_file_chain_lock(%rip)
	je	.L7
	cmpq	%rbx, %rdx
	jne	.L9
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%rbx, %rax
	je	.L36
	movq	%rax, %rdx
.L9:
	movq	232(%rdx), %rax
	testq	%rax, %rax
	jne	.L10
	movl	$-1, %r12d
.L7:
	testl	%ecx, %ecx
	movl	%ecx, 4+proc_file_chain_lock(%rip)
	jne	.L12
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 272 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L13
	subl	$1, proc_file_chain_lock(%rip)
.L12:
	testq	%rbp, %rbp
	je	.L15
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L15:
	cmpl	$-1, %r12d
	je	.L16
	movl	112(%rbx), %edi
	call	__close_nocancel
	testl	%eax, %eax
	js	.L16
	movq	__pthread_setcancelstate@GOTPCREL(%rip), %r12
	leaq	12(%rsp), %r13
	leaq	16(%rsp), %r14
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r14, %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
	movl	224(%rbx), %edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	__waitpid
	movl	16(%rsp), %edi
	movl	%eax, %ebp
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
	cmpl	$-1, %ebp
	jne	.L18
.L38:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L16
.L19:
	testq	%r12, %r12
	jne	.L37
	movl	224(%rbx), %edi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	__waitpid
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.L38
.L18:
	movl	12(%rsp), %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$48, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	unlock(%rip), %rax
	movq	$0, 24(%rsp)
	movq	%rax, 16(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$232, %rdx
.L8:
	movq	232(%rbx), %rax
	xorl	%r12d, %r12d
	movq	%rax, (%rdx)
	jmp	.L7
.L13:
#APP
# 272 "iopopen.c" 1
	xchgl %ecx, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %ecx
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 272 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, proc_file_chain_lock(%rip)
	je	.L6
	leaq	proc_file_chain_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	proc_file_chain(%rip), %rdx
	jmp	.L8
	.size	_IO_new_proc_close, .-_IO_new_proc_close
	.weak	_IO_proc_close
	.set	_IO_proc_close,_IO_new_proc_close
	.p2align 4,,15
	.type	unlock, @function
unlock:
	movl	4+proc_file_chain_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+proc_file_chain_lock(%rip)
	jne	.L39
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 59 "iopopen.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L42
	subl	$1, proc_file_chain_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L42:
#APP
# 59 "iopopen.c" 1
	xchgl %eax, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L39
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 59 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L39:
	rep ret
	.size	unlock, .-unlock
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"sh"
.LC1:
	.string	"-c"
.LC2:
	.string	"/bin/sh"
	.text
	.p2align 4,,15
	.globl	_IO_new_proc_open
	.type	_IO_new_proc_open, @function
_IO_new_proc_open:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	movq	%rdi, %rbx
	xorl	%esi, %esi
	subq	$200, %rsp
.L45:
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L92
.L51:
	addq	$1, %rdx
	cmpb	$114, %al
	je	.L47
	cmpb	$119, %al
	je	.L75
	cmpb	$101, %al
	je	.L93
.L46:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
.L44:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movzbl	(%rdx), %eax
	movl	$1, %r14d
	testb	%al, %al
	jne	.L51
.L92:
	cmpl	%esi, %r14d
	je	.L46
	cmpl	$-1, 112(%rbx)
	jne	.L91
	leaq	40(%rsp), %rax
	movl	$524288, %esi
	movq	%rax, %rdi
	movq	%rax, 24(%rsp)
	call	__pipe2
	testl	%eax, %eax
	js	.L91
	movl	%r14d, %eax
	leaq	112(%rsp), %r12
	xorl	$1, %eax
	cmpl	$1, %r14d
	movl	%eax, 20(%rsp)
	sbbl	%eax, %eax
	movq	%r12, %rdi
	andl	$-4, %eax
	addl	$8, %eax
	movl	%eax, 16(%rsp)
	call	__posix_spawn_file_actions_init
	movslq	%r14d, %rax
	movl	40(%rsp,%rax,4), %esi
	movq	%rax, 8(%rsp)
	cmpl	%r14d, %esi
	je	.L94
.L55:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_adddup2
	testl	%eax, %eax
	jne	.L57
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	je	.L58
	leaq	48(%rsp), %rdi
	leaq	unlock(%rip), %rsi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L59:
	movq	%fs:16, %r15
	cmpq	%r15, 8+proc_file_chain_lock(%rip)
	je	.L60
#APP
# 195 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L61
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
.L62:
	movq	%r15, 8+proc_file_chain_lock(%rip)
.L60:
	movq	proc_file_chain(%rip), %r15
	addl	$1, 4+proc_file_chain_lock(%rip)
	testq	%r15, %r15
	je	.L63
	.p2align 4,,10
	.p2align 3
.L65:
	movl	112(%r15), %esi
	cmpl	%esi, %r14d
	je	.L66
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_addclose
	testl	%eax, %eax
	jne	.L68
.L66:
	movq	232(%r15), %r15
	testq	%r15, %r15
	jne	.L65
.L63:
	leaq	.LC0(%rip), %rax
	movq	__environ(%rip), %r9
	leaq	224(%rbx), %rdi
	leaq	80(%rsp), %r8
	leaq	.LC2(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rax, 80(%rsp)
	leaq	.LC1(%rip), %rax
	movq	%r12, %rdx
	movq	%r13, 96(%rsp)
	movq	$0, 104(%rsp)
	movq	%rax, 88(%rsp)
	call	__posix_spawn
	testl	%eax, %eax
	je	.L95
.L68:
	xorl	%ebp, %ebp
.L67:
	movl	4+proc_file_chain_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+proc_file_chain_lock(%rip)
	jne	.L71
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 200 "iopopen.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L72
	subl	$1, proc_file_chain_lock(%rip)
.L71:
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	je	.L74
	leaq	48(%rsp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L74:
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_destroy
	testb	%bpl, %bpl
	je	.L57
	movl	(%rbx), %eax
	movl	16(%rsp), %r14d
	andl	$-13, %eax
	orl	%eax, %r14d
	movq	%rbx, %rax
	movl	%r14d, (%rbx)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$1, %ebp
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$1, %esi
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$1030, %esi
	movl	%r14d, %edi
	call	__fcntl
	testl	%eax, %eax
	movl	%eax, %r15d
	jns	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__close_nocancel
	movslq	20(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__close_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L91:
	xorl	%eax, %eax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	unlock(%rip), %rax
	movq	$0, 56(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L56:
	movq	8(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__close_nocancel
	movq	8(%rsp), %rax
	movl	%r15d, %esi
	movl	%r15d, 40(%rsp,%rax,4)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L95:
	movq	8(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__close_nocancel
	movslq	20(%rsp), %rax
	movq	24(%rsp), %rcx
	testl	%ebp, %ebp
	leaq	(%rcx,%rax,4), %r13
	jne	.L69
	movl	0(%r13), %edi
	xorl	%edx, %edx
	movl	$2, %esi
	xorl	%eax, %eax
	call	__fcntl
.L69:
	movl	0(%r13), %eax
	movl	$1, %ebp
	movl	%eax, 112(%rbx)
	movq	proc_file_chain(%rip), %rax
	movq	%rbx, proc_file_chain(%rip)
	movq	%rax, 232(%rbx)
	jmp	.L67
.L61:
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, proc_file_chain_lock(%rip)
	je	.L62
	leaq	proc_file_chain_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L62
.L72:
#APP
# 200 "iopopen.c" 1
	xchgl %eax, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L71
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 200 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L71
	.size	_IO_new_proc_open, .-_IO_new_proc_open
	.weak	_IO_proc_open
	.set	_IO_proc_open,_IO_new_proc_open
	.p2align 4,,15
	.globl	_IO_new_popen
	.type	_IO_new_popen, @function
_IO_new_popen:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	$256, %edi
	movq	%rsi, %r12
	call	malloc@PLT
	testq	%rax, %rax
	je	.L98
	leaq	240(%rax), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rdx, 136(%rax)
	call	_IO_init_internal
	leaq	_IO_proc_jumps(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	call	_IO_new_file_init_internal
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_new_proc_open
	testq	%rax, %rax
	je	.L100
.L96:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%ebx, %ebx
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rbx, %rdi
	call	_IO_un_link
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L96
	.size	_IO_new_popen, .-_IO_new_popen
	.weak	_IO_popen
	.set	_IO_popen,_IO_new_popen
	.globl	__new_popen
	.set	__new_popen,_IO_new_popen
	.weak	popen
	.set	popen,__new_popen
	.local	proc_file_chain_lock
	.comm	proc_file_chain_lock,16,16
	.local	proc_file_chain
	.comm	proc_file_chain,8,8
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_proc_jumps, @object
	.size	_IO_proc_jumps, 168
_IO_proc_jumps:
	.quad	0
	.quad	0
	.quad	_IO_new_file_finish
	.quad	_IO_new_file_overflow
	.quad	_IO_new_file_underflow
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_new_file_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_new_file_setbuf
	.quad	_IO_new_file_sync
	.quad	_IO_file_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_new_proc_close
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.weak	__pthread_setcancelstate
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.hidden	_IO_file_stat
	.hidden	_IO_file_seek
	.hidden	_IO_file_read
	.hidden	_IO_file_doallocate
	.hidden	_IO_default_xsgetn
	.hidden	_IO_default_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_un_link
	.hidden	_IO_new_file_init_internal
	.hidden	_IO_init_internal
	.hidden	__fcntl
	.hidden	__posix_spawn_file_actions_destroy
	.hidden	__posix_spawn
	.hidden	__posix_spawn_file_actions_addclose
	.hidden	__posix_spawn_file_actions_adddup2
	.hidden	__posix_spawn_file_actions_init
	.hidden	__pipe2
	.hidden	__lll_lock_wait_private
	.hidden	__waitpid
	.hidden	__close_nocancel
