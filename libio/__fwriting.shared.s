	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fwriting
	.type	__fwriting, @function
__fwriting:
	movl	(%rdi), %eax
	andl	$2052, %eax
	ret
	.size	__fwriting, .-__fwriting
