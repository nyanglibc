	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	_IO_strn_overflow, @function
_IO_strn_overflow:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	leaq	240(%rdi), %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rbp, 56(%rdi)
	je	.L2
	movq	40(%rdi), %rax
	leaq	304(%rdi), %r13
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movb	$0, (%rax)
	call	__GI__IO_setb
	movq	%rbp, 32(%rbx)
	movq	%rbp, 24(%rbx)
	movq	%rbp, 8(%rbx)
	movq	%r13, 16(%rbx)
.L2:
	movq	%rbp, 40(%rbx)
	movq	%rbp, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	_IO_strn_overflow, .-_IO_strn_overflow
	.p2align 4,,15
	.globl	__vsnprintf_internal
	.hidden	__vsnprintf_internal
	.type	__vsnprintf_internal, @function
__vsnprintf_internal:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	subq	$312, %rsp
	testq	%rsi, %rsi
	movq	$0, 136(%rsp)
	je	.L8
	leaq	-1(%rsi), %r15
	movq	%rdi, %rbp
	movq	%rsp, %rbx
.L6:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$-1, %edx
	movl	$32768, %esi
	call	_IO_no_init@PLT
	leaq	_IO_strn_jumps(%rip), %rax
	movq	%rbx, %rdi
	movq	%rbp, %rcx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%rax, 216(%rsp)
	movb	$0, 0(%rbp)
	call	_IO_str_init_static_internal@PLT
	movq	%rbx, %rdi
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	addq	$240, %rbx
	call	__vfprintf_internal
	cmpq	%rbx, 56(%rsp)
	je	.L5
	movq	40(%rsp), %rdx
	movb	$0, (%rdx)
.L5:
	addq	$312, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsp, %rbx
	movl	$63, %r15d
	leaq	240(%rbx), %rbp
	jmp	.L6
	.size	__vsnprintf_internal, .-__vsnprintf_internal
	.p2align 4,,15
	.globl	___vsnprintf
	.type	___vsnprintf, @function
___vsnprintf:
	xorl	%r8d, %r8d
	jmp	__vsnprintf_internal
	.size	___vsnprintf, .-___vsnprintf
	.weak	vsnprintf
	.set	vsnprintf,___vsnprintf
	.weak	__vsnprintf
	.set	__vsnprintf,___vsnprintf
	.hidden	_IO_strn_jumps
	.globl	_IO_strn_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_strn_jumps, @object
	.size	_IO_strn_jumps, 168
_IO_strn_jumps:
	.quad	0
	.quad	0
	.quad	_IO_str_finish
	.quad	_IO_strn_overflow
	.quad	__GI__IO_str_underflow
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_str_pbackfail
	.quad	__GI__IO_default_xsputn
	.quad	__GI__IO_default_xsgetn
	.quad	__GI__IO_str_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	__GI__IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	__vfprintf_internal
