	.text
	.p2align 4,,15
	.globl	__fread_unlocked
	.hidden	__fread_unlocked
	.type	__fread_unlocked, @function
__fread_unlocked:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	imulq	%rdx, %rbx
	testq	%rbx, %rbx
	jne	.L9
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rbx, %rdx
	movq	%rdi, %rsi
	movq	%rcx, %rdi
	call	_IO_sgetn
	cmpq	%rax, %rbx
	je	.L3
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__fread_unlocked, .-__fread_unlocked
	.weak	fread_unlocked
	.set	fread_unlocked,__fread_unlocked
	.hidden	_IO_sgetn
