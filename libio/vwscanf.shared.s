	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vwscanf
	.type	__vwscanf, @function
__vwscanf:
	movq	stdin@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfwscanf_internal
	.size	__vwscanf, .-__vwscanf
	.globl	vwscanf
	.set	vwscanf,__vwscanf
	.hidden	__vfwscanf_internal
