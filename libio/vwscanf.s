	.text
	.p2align 4,,15
	.globl	__vwscanf
	.type	__vwscanf, @function
__vwscanf:
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdin(%rip), %rdi
	xorl	%ecx, %ecx
	jmp	__vfwscanf_internal
	.size	__vwscanf, .-__vwscanf
	.globl	vwscanf
	.set	vwscanf,__vwscanf
	.hidden	__vfwscanf_internal
