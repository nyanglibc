	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	setlinebuf
	.type	setlinebuf, @function
setlinebuf:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	__GI__IO_setvbuf
	.size	setlinebuf, .-setlinebuf
