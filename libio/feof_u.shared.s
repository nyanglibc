	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__feof_unlocked
	.hidden	__feof_unlocked
	.type	__feof_unlocked, @function
__feof_unlocked:
	movl	(%rdi), %eax
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.size	__feof_unlocked, .-__feof_unlocked
	.weak	__GI_feof_unlocked
	.hidden	__GI_feof_unlocked
	.set	__GI_feof_unlocked,__feof_unlocked
	.weak	feof_unlocked
	.set	feof_unlocked,__GI_feof_unlocked
