	.text
	.p2align 4,,15
	.globl	putwchar
	.type	putwchar, @function
putwchar:
.LFB71:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	movq	stdout(%rip), %rbx
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L18
	movq	136(%rbx), %rcx
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rcx)
	je	.L19
#APP
# 25 "putwchar.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rcx
	movq	stdout(%rip), %rdi
	movq	%r12, 8(%rcx)
.L3:
	movq	160(%rdi), %rax
	addl	$1, 4(%rcx)
	movl	%ebp, %r8d
	testq	%rax, %rax
	jne	.L26
.L8:
	movl	%ebp, %esi
.LEHB0:
	call	__woverflow
	testl	$32768, (%rbx)
	movl	%eax, %r8d
	je	.L27
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rbx, %rdi
	movl	%ebp, %r8d
	movq	160(%rdi), %rax
	testq	%rax, %rax
	je	.L8
.L26:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L8
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	%ebp, (%rdx)
	testl	$32768, (%rbx)
	jne	.L1
.L27:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L14
	subl	$1, (%rdi)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rbx, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L5
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L20:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L16
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L16
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L16:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L16
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
.LFE71:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA71:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE71-.LLSDACSB71
.LLSDACSB71:
	.uleb128 .LEHB0-.LFB71
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L20-.LFB71
	.uleb128 0
	.uleb128 .LEHB1-.LFB71
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE71:
	.text
	.size	putwchar, .-putwchar
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__woverflow
