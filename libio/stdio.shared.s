	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.globl	stderr
	.section	.data.rel,"aw",@progbits
	.align 8
	.type	stderr, @object
	.size	stderr, 8
stderr:
	.quad	_IO_2_1_stderr_
	.globl	stdout
	.align 8
	.type	stdout, @object
	.size	stdout, 8
stdout:
	.quad	_IO_2_1_stdout_
	.globl	stdin
	.align 8
	.type	stdin, @object
	.size	stdin, 8
stdin:
	.quad	_IO_2_1_stdin_
