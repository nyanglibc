	.text
	.p2align 4,,15
	.type	_IO_obstack_xsputn, @function
_IO_obstack_xsputn:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	40(%rdi), %rdi
	movq	%rsi, %r14
	movq	48(%rbp), %rax
	addq	%rdi, %rdx
	cmpq	%rdx, %rax
	jnb	.L2
	movq	224(%rbp), %r12
	subq	%rax, %rdi
	movslq	%r13d, %rbx
	addq	24(%r12), %rdi
	leaq	(%rdi,%rbx), %rax
	cmpq	%rax, 32(%r12)
	movq	%rdi, 24(%r12)
	jb	.L7
.L3:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	16(%r12), %rax
	addq	24(%r12), %rbx
	movq	%rax, 32(%rbp)
	movq	32(%r12), %rax
	movq	%rbx, 40(%rbp)
	subq	%rbx, %rax
	cltq
	addq	%rax, %rbx
	movq	%r13, %rax
	movq	%rbx, 48(%rbp)
	movq	%rbx, 24(%r12)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_obstack_newchunk
	movq	24(%r12), %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r13, %rdx
	call	__mempcpy@PLT
	movq	%rax, 40(%rbp)
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	_IO_obstack_xsputn, .-_IO_obstack_xsputn
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"obprintf.c"
.LC1:
	.string	"c != EOF"
	.text
	.p2align 4,,15
	.type	_IO_obstack_overflow, @function
_IO_obstack_overflow:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$-1, %esi
	movq	224(%rdi), %rbx
	je	.L12
	movq	24(%rbx), %rdx
	movq	%rdi, %rbp
	movl	%esi, %eax
	leaq	1(%rdx), %rcx
	cmpq	32(%rbx), %rcx
	jbe	.L10
	movl	%esi, 12(%rsp)
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_obstack_newchunk
	movq	24(%rbx), %rdx
	movl	12(%rsp), %eax
	leaq	1(%rdx), %rcx
.L10:
	movq	%rcx, 24(%rbx)
	movb	%al, (%rdx)
	movq	16(%rbx), %rdx
	movq	32(%rbx), %rcx
	movq	%rdx, 32(%rbp)
	movq	24(%rbx), %rdx
	subq	%rdx, %rcx
	movq	%rdx, 40(%rbp)
	movslq	%ecx, %rcx
	addq	%rcx, %rdx
	movq	%rdx, 48(%rbp)
	movq	%rdx, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L12:
	leaq	__PRETTY_FUNCTION__.10884(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$47, %edx
	call	__assert_fail
	.size	_IO_obstack_overflow, .-_IO_obstack_overflow
	.section	.rodata.str1.1
.LC2:
	.string	"size != 0"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"size == (new_f.ofile.file.file._IO_write_end - new_f.ofile.file.file._IO_write_base)"
	.align 8
.LC4:
	.string	"new_f.ofile.file.file._IO_write_ptr == (new_f.ofile.file.file._IO_write_base + obstack_object_size (obstack))"
	.text
	.p2align 4,,15
	.globl	__obstack_vprintf_internal
	.hidden	__obstack_vprintf_internal
	.type	__obstack_vprintf_internal, @function
__obstack_vprintf_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r8d, %r8d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %r15d
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movl	$-1, %edx
	subq	$264, %rsp
	leaq	16(%rsp), %r12
	movq	%rsi, 8(%rsp)
	movl	$32768, %esi
	movq	$0, 152(%rsp)
	movq	%r12, %rdi
	call	_IO_no_init@PLT
	movq	24(%rbx), %rcx
	leaq	_IO_obstack_jumps(%rip), %rax
	movq	32(%rbx), %rbp
	movq	16(%rbx), %rsi
	movq	%rax, 232(%rsp)
	movq	%rcx, %rax
	subq	%rcx, %rbp
	subq	%rsi, %rax
	addl	%ebp, %eax
	jne	.L19
	cmpq	$63, %rbp
	movl	%ebp, %edx
	jle	.L21
.L15:
	testl	%edx, %edx
	je	.L16
	movq	16(%rbx), %rsi
	movl	%edx, %ebp
.L14:
	movslq	%edx, %r13
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_IO_str_init_static_internal@PLT
	movq	48(%rsp), %rdx
	movq	64(%rsp), %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %r13
	jne	.L22
	movq	24(%rbx), %rax
	movl	%eax, %ecx
	subl	16(%rbx), %ecx
	addq	%rcx, %rdx
	cmpq	%rdx, 56(%rsp)
	jne	.L23
	movslq	%ebp, %rbp
	movq	8(%rsp), %rsi
	movq	%r14, %rdx
	addq	%rbp, %rax
	movl	%r15d, %ecx
	movq	%r12, %rdi
	movq	%rax, 24(%rbx)
	movq	%rbx, 240(%rsp)
	call	__vfprintf_internal
	movq	56(%rsp), %rdx
	subq	64(%rsp), %rdx
	addq	%rdx, 24(%rbx)
	addq	$264, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$64, %esi
	movq	%rbx, %rdi
	call	_obstack_newchunk
	movq	24(%rbx), %rcx
	movl	32(%rbx), %edx
	subl	%ecx, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%eax, %edx
	jmp	.L14
.L23:
	leaq	__PRETTY_FUNCTION__.10920(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$163, %edx
	call	__assert_fail
.L22:
	leaq	__PRETTY_FUNCTION__.10920(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$160, %edx
	call	__assert_fail
.L16:
	leaq	__PRETTY_FUNCTION__.10920(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$152, %edx
	call	__assert_fail
	.size	__obstack_vprintf_internal, .-__obstack_vprintf_internal
	.p2align 4,,15
	.globl	__obstack_vprintf
	.type	__obstack_vprintf, @function
__obstack_vprintf:
	xorl	%ecx, %ecx
	jmp	__obstack_vprintf_internal
	.size	__obstack_vprintf, .-__obstack_vprintf
	.weak	obstack_vprintf
	.set	obstack_vprintf,__obstack_vprintf
	.p2align 4,,15
	.globl	__obstack_printf
	.type	__obstack_printf, @function
__obstack_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L27
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L27:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__obstack_vprintf_internal
	addq	$216, %rsp
	ret
	.size	__obstack_printf, .-__obstack_printf
	.weak	obstack_printf
	.set	obstack_printf,__obstack_printf
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10920, @object
	.size	__PRETTY_FUNCTION__.10920, 27
__PRETTY_FUNCTION__.10920:
	.string	"__obstack_vprintf_internal"
	.align 16
	.type	__PRETTY_FUNCTION__.10884, @object
	.size	__PRETTY_FUNCTION__.10884, 21
__PRETTY_FUNCTION__.10884:
	.string	"_IO_obstack_overflow"
	.hidden	_IO_obstack_jumps
	.globl	_IO_obstack_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_obstack_jumps, @object
	.size	_IO_obstack_jumps, 168
_IO_obstack_jumps:
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_obstack_overflow
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_obstack_xsputn
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.hidden	__vfprintf_internal
	.hidden	__assert_fail
	.hidden	_obstack_newchunk
