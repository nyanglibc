	.text
	.p2align 4,,15
	.globl	fputc_unlocked
	.hidden	fputc_unlocked
	.type	fputc_unlocked, @function
fputc_unlocked:
	movq	40(%rsi), %rcx
	cmpq	48(%rsi), %rcx
	movq	%rsi, %rdx
	movzbl	%dil, %eax
	jnb	.L4
	leaq	1(%rcx), %rsi
	movq	%rsi, 40(%rdx)
	movb	%dil, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %esi
	movq	%rdx, %rdi
	jmp	__overflow
	.size	fputc_unlocked, .-fputc_unlocked
	.hidden	__overflow
