	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"iofwide.c"
.LC1:
	.string	"fcts.towc_nsteps == 1"
.LC2:
	.string	"fcts.tomb_nsteps == 1"
	.text
	.p2align 4,,15
	.globl	_IO_fwide
	.type	_IO_fwide, @function
_IO_fwide:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	testl	%esi, %esi
	movl	192(%rdi), %eax
	js	.L15
	testl	%eax, %eax
	jne	.L1
	testl	%esi, %esi
	jne	.L16
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	testl	%eax, %eax
	jne	.L1
	movl	$-1, %eax
.L4:
	movl	%eax, 192(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	160(%rdi), %rbp
	leaq	104(%rbp), %rax
	movq	%rax, 152(%rdi)
	movq	8(%rbp), %rax
	movq	$0, 88(%rbp)
	movq	%rax, 0(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, 32(%rbp)
	movq	160(%rdi), %rax
	movq	%rsp, %rdi
	movq	$0, 96(%rax)
	call	__wcsmbs_clone_conv
	cmpq	$1, 8(%rsp)
	jne	.L17
	cmpq	$1, 24(%rsp)
	jne	.L18
	movq	(%rsp), %rax
	movq	16(%rsp), %rcx
	movq	$1, 128(%rbp)
	movl	$1, 136(%rbp)
	movq	%rax, 104(%rbp)
	movq	160(%rbx), %rax
	movq	%rcx, 160(%rbp)
	movq	$9, 184(%rbp)
	movl	$1, 192(%rbp)
	leaq	88(%rax), %rdx
	movq	224(%rax), %rax
	movq	%rdx, 144(%rbp)
	movq	%rdx, 200(%rbp)
	movq	%rax, 216(%rbx)
	movl	$1, %eax
	jmp	.L4
.L17:
	leaq	__PRETTY_FUNCTION__.12950(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$80, %edx
	call	__assert_fail
.L18:
	leaq	__PRETTY_FUNCTION__.12950(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$81, %edx
	call	__assert_fail
	.size	_IO_fwide, .-_IO_fwide
	.p2align 4,,15
	.globl	__libio_codecvt_out
	.hidden	__libio_codecvt_out
	.type	__libio_codecvt_out, @function
__libio_codecvt_out:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%r8, %r13
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$16, %rsp
	movq	56(%rdi), %r12
	movq	%r9, 64(%rdi)
	movq	64(%rsp), %rax
	movq	%rdx, 8(%rsp)
	movq	%rsi, 96(%rdi)
	cmpq	$0, (%r12)
	movq	40(%r12), %rbx
	movq	%rax, 72(%rdi)
	je	.L20
#APP
# 129 "iofwide.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L20:
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	leaq	64(%rbp), %rsi
	leaq	16(%rsp), %r9
	movq	%r12, %rdi
	call	*%rbx
	movq	24(%rsp), %rdx
	movq	64(%rbp), %rcx
	cmpl	$4, %eax
	movq	%rdx, 0(%r13)
	movq	88(%rsp), %rdx
	movq	%rcx, (%rdx)
	popq	%rdx
	popq	%rcx
	je	.L25
	jle	.L39
	cmpl	$5, %eax
	je	.L24
	cmpl	$7, %eax
	je	.L24
.L21:
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	testl	%eax, %eax
	jne	.L21
.L25:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__libio_codecvt_out, .-__libio_codecvt_out
	.p2align 4,,15
	.globl	__libio_codecvt_in
	.hidden	__libio_codecvt_in
	.type	__libio_codecvt_in, @function
__libio_codecvt_in:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%r8, %r13
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$16, %rsp
	movq	(%rdi), %r12
	movq	%r9, 8(%rdi)
	movq	64(%rsp), %rax
	movq	%rdx, 8(%rsp)
	movq	%rsi, 40(%rdi)
	cmpq	$0, (%r12)
	movq	40(%r12), %rbx
	movq	%rax, 16(%rdi)
	je	.L41
#APP
# 181 "iofwide.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L41:
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	leaq	8(%rbp), %rsi
	leaq	16(%rsp), %r9
	movq	%r12, %rdi
	call	*%rbx
	movq	24(%rsp), %rdx
	movq	8(%rbp), %rcx
	cmpl	$4, %eax
	movq	%rdx, 0(%r13)
	movq	88(%rsp), %rdx
	movq	%rcx, (%rdx)
	popq	%rdx
	popq	%rcx
	je	.L46
	jle	.L60
	cmpl	$5, %eax
	je	.L45
	cmpl	$7, %eax
	je	.L45
.L42:
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%eax, %eax
	jne	.L42
.L46:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__libio_codecvt_in, .-__libio_codecvt_in
	.p2align 4,,15
	.globl	__libio_codecvt_encoding
	.hidden	__libio_codecvt_encoding
	.type	__libio_codecvt_encoding, @function
__libio_codecvt_encoding:
	movq	(%rdi), %rdx
	movl	88(%rdx), %eax
	testl	%eax, %eax
	jne	.L63
	movl	72(%rdx), %ecx
	cmpl	76(%rdx), %ecx
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$-1, %eax
	ret
	.size	__libio_codecvt_encoding, .-__libio_codecvt_encoding
	.p2align 4,,15
	.globl	__libio_codecvt_length
	.hidden	__libio_codecvt_length
	.type	__libio_codecvt_length, @function
__libio_codecvt_length:
	pushq	%rbp
	salq	$2, %r8
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbx
	movq	%rcx, %r13
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rdx, -64(%rbp)
	leaq	18(%r8), %rdx
	movq	(%rdi), %r15
	movq	%rsi, 40(%rdi)
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rsp, %rax
	movq	40(%r15), %r12
	movq	%rax, 8(%rdi)
	addq	%r8, %rax
	cmpq	$0, (%r15)
	movq	%rax, 16(%rdi)
	je	.L65
#APP
# 248 "iofwide.c" 1
	ror $2*8+1, %r12
xor %fs:48, %r12
# 0 "" 2
#NO_APP
.L65:
	movq	%r12, %rdi
	call	_dl_mcount_wrapper_check
	pushq	$0
	leaq	-64(%rbp), %rdx
	leaq	8(%rbx), %rsi
	pushq	$0
	leaq	-56(%rbp), %r9
	movq	%r13, %rcx
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	call	*%r12
	movq	-64(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	subq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	__libio_codecvt_length, .-__libio_codecvt_length
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.12950, @object
	.size	__PRETTY_FUNCTION__.12950, 10
__PRETTY_FUNCTION__.12950:
	.string	"_IO_fwide"
	.hidden	_dl_mcount_wrapper_check
	.hidden	__assert_fail
	.hidden	__wcsmbs_clone_conv
