	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__freadable
	.type	__freadable, @function
__freadable:
	xorl	%eax, %eax
	testb	$4, (%rdi)
	sete	%al
	ret
	.size	__freadable, .-__freadable
