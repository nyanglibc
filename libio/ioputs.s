	.text
	.p2align 4,,15
	.globl	_IO_puts
	.hidden	_IO_puts
	.type	_IO_puts, @function
_IO_puts:
.LFB68:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	call	strlen
	movq	stdout(%rip), %rbp
	movq	%rax, %rbx
	movl	0(%rbp), %edx
	andl	$32768, %edx
	jne	.L22
	movq	136(%rbp), %rcx
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rcx)
	je	.L23
#APP
# 36 "ioputs.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbp), %rcx
	movq	stdout(%rip), %rdi
	movq	%r13, 8(%rcx)
.L3:
	movl	192(%rdi), %eax
	addl	$1, 4(%rcx)
	testl	%eax, %eax
	je	.L30
.L8:
	cmpl	$-1, %eax
	je	.L9
.L12:
	movl	$-1, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rbp, %rdi
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L8
.L30:
	movl	$-1, 192(%rdi)
.L9:
	movq	216(%rdi), %r13
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r13, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L31
.L11:
	movq	%rbx, %rdx
	movq	%r12, %rsi
.LEHB0:
	call	*56(%r13)
	cmpq	%rax, %rbx
	jne	.L12
	movq	stdout(%rip), %rdi
	movq	40(%rdi), %rax
	cmpq	48(%rdi), %rax
	jnb	.L32
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movb	$10, (%rax)
.L14:
	addq	$1, %rbx
	movl	$2147483647, %eax
	cmpq	$2147483647, %rbx
	cmova	%rax, %rbx
.L10:
	testl	$32768, 0(%rbp)
	jne	.L1
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L18
	subl	$1, (%rdi)
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rbp, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L31:
	call	_IO_vtable_check
	movq	stdout(%rip), %rdi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$10, %esi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L14
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L5
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L24:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L20
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L20
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L21
	subl	$1, (%rdi)
.L20:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L21:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L20
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L20
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L24-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_puts, .-_IO_puts
	.weak	puts
	.set	puts,_IO_puts
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__overflow
	.hidden	_IO_vtable_check
	.hidden	strlen
