	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal error: glibc detected an invalid stdio handle\n"
	.text
	.p2align 4,,15
	.globl	_IO_vtable_check
	.hidden	_IO_vtable_check
	.type	_IO_vtable_check, @function
_IO_vtable_check:
	cmpq	$0, __dlopen@GOTPCREL(%rip)
	je	.L7
	rep ret
.L7:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal
	.size	_IO_vtable_check, .-_IO_vtable_check
	.weak	__dlopen
	.hidden	__libc_fatal
	.hidden	__dlopen
