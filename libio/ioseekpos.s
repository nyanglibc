	.text
	.p2align 4,,15
	.globl	_IO_seekpos_unlocked
	.hidden	_IO_seekpos_unlocked
	.type	_IO_seekpos_unlocked, @function
_IO_seekpos_unlocked:
.LFB68:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L7
	movq	160(%rdi), %rax
	cmpq	$0, 64(%rax)
	je	.L3
	movq	%rsi, 8(%rsp)
	call	_IO_free_wbackup_area
	movq	8(%rsp), %rsi
.L3:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdi
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdx
	subq	%rdi, %rax
	subq	%rdi, %rdx
	cmpq	%rdx, %rax
	jbe	.L8
.L4:
	movq	72(%rbp), %rax
	addq	$16, %rsp
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	xorl	%edx, %edx
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	$0, 72(%rdi)
	je	.L3
	movq	%rsi, 8(%rsp)
	call	_IO_free_backup_area
	movq	8(%rsp), %rsi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rsi
	jmp	.L4
.LFE68:
	.size	_IO_seekpos_unlocked, .-_IO_seekpos_unlocked
	.p2align 4,,15
	.globl	_IO_seekpos
	.type	_IO_seekpos, @function
_IO_seekpos:
.LFB69:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	andl	$32768, %edx
	jne	.L10
	movq	136(%rdi), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L11
#APP
# 55 "ioseekpos.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L13:
	movq	136(%rbx), %rdi
	movq	%r13, 8(%rdi)
.L11:
	addl	$1, 4(%rdi)
.L10:
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekpos_unlocked
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L9
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L9
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L19
	subl	$1, (%rdi)
.L9:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L13
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L9
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L9
.L23:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L21
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L21
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L22
	subl	$1, (%rdi)
.L21:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L22:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L21
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L21
.LFE69:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA69:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE69-.LLSDACSB69
.LLSDACSB69:
	.uleb128 .LEHB0-.LFB69
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L23-.LFB69
	.uleb128 0
	.uleb128 .LEHB1-.LFB69
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE69:
	.text
	.size	_IO_seekpos, .-_IO_seekpos
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	_IO_free_backup_area
	.hidden	_IO_free_wbackup_area
