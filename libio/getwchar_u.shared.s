	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getwchar_unlocked
	.type	getwchar_unlocked, @function
getwchar_unlocked:
	movq	stdin@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	160(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L2
	leaq	4(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	jmp	__GI___wuflow
	.size	getwchar_unlocked, .-getwchar_unlocked
