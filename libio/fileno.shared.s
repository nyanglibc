	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fileno
	.hidden	__GI___fileno
	.type	__GI___fileno, @function
__GI___fileno:
	testl	$8192, (%rdi)
	je	.L2
	movl	112(%rdi), %eax
	testl	%eax, %eax
	js	.L2
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI___fileno, .-__GI___fileno
	.globl	__fileno
	.set	__fileno,__GI___fileno
	.weak	fileno_unlocked
	.set	fileno_unlocked,__fileno
	.weak	__GI_fileno
	.hidden	__GI_fileno
	.set	__GI_fileno,__fileno
	.weak	fileno
	.set	fileno,__GI_fileno
