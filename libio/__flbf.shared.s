	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__flbf
	.type	__flbf, @function
__flbf:
	movl	(%rdi), %eax
	andl	$512, %eax
	ret
	.size	__flbf, .-__flbf
