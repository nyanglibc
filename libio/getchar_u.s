	.text
	.p2align 4,,15
	.globl	getchar_unlocked
	.type	getchar_unlocked, @function
getchar_unlocked:
	movq	stdin(%rip), %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L4
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	__uflow
	.size	getchar_unlocked, .-getchar_unlocked
	.hidden	__uflow
