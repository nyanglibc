	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_putwc_unlocked
	.hidden	__GI_putwc_unlocked
	.type	__GI_putwc_unlocked, @function
__GI_putwc_unlocked:
	movq	160(%rsi), %rdx
	movq	%rsi, %rax
	testq	%rdx, %rdx
	je	.L2
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L2
	leaq	4(%rcx), %rax
	movq	%rax, 32(%rdx)
	movl	%edi, (%rcx)
	movl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edi, %esi
	movq	%rax, %rdi
	jmp	__GI___woverflow
	.size	__GI_putwc_unlocked, .-__GI_putwc_unlocked
	.globl	putwc_unlocked
	.set	putwc_unlocked,__GI_putwc_unlocked
