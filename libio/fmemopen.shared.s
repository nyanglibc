	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __fmemopen,fmemopen@@GLIBC_2.22
#NO_APP
	.p2align 4,,15
	.type	fmemopen_seek, @function
fmemopen_seek:
	cmpl	$1, %edx
	je	.L3
	cmpl	$2, %edx
	je	.L4
	testl	%edx, %edx
	je	.L12
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rsi), %rax
.L6:
	testq	%rax, %rax
	js	.L7
	cmpq	%rax, 16(%rdi)
	jb	.L7
	movq	%rax, 24(%rdi)
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	addq	32(%rdi), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rsi), %rax
	addq	24(%rdi), %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	fmemopen_seek, .-fmemopen_seek
	.p2align 4,,15
	.type	fmemopen_close, @function
fmemopen_close:
	pushq	%rbx
	movl	8(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L14
	movq	(%rdi), %rdi
	call	free@PLT
.L14:
	movq	%rbx, %rdi
	call	free@PLT
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	fmemopen_close, .-fmemopen_close
	.p2align 4,,15
	.type	fmemopen_write, @function
fmemopen_write:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	12(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L17
	movq	32(%rdi), %rdi
.L18:
	testq	%rbp, %rbp
	movl	$1, %r13d
	je	.L19
	xorl	%r13d, %r13d
	cmpb	$0, -1(%rsi,%rbp)
	setne	%r13b
.L19:
	movq	16(%rbx), %rax
	leaq	(%rdi,%rbp), %r12
	cmpq	%rax, %r12
	jbe	.L20
	movslq	%r13d, %rdx
	addq	24(%rbx), %rdx
	cmpq	%rdx, %rax
	jbe	.L39
	movq	%rax, %rbp
	movq	%rax, %r12
	subq	%rdi, %rbp
.L20:
	addq	(%rbx), %rdi
	movq	%rbp, %rdx
	call	__GI_memcpy@PLT
	cmpq	%r12, 32(%rbx)
	movq	%r12, 24(%rbx)
	jb	.L40
.L24:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	24(%rdi), %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L40:
	movq	16(%rbx), %rax
	movq	%r12, 32(%rbx)
	cmpq	%r12, %rax
	ja	.L41
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jne	.L24
	testl	%r13d, %r13d
	je	.L24
	movq	(%rbx), %rdx
	movb	$0, -1(%rdx,%rax)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L39:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	testl	%r13d, %r13d
	je	.L24
	movq	(%rbx), %rax
	movb	$0, (%rax,%r12)
	jmp	.L24
	.size	fmemopen_write, .-fmemopen_write
	.p2align 4,,15
	.type	fmemopen_read, @function
fmemopen_read:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	24(%rbx), %rsi
	movq	32(%rbx), %rax
	addq	%rsi, %rdx
	cmpq	%rax, %rdx
	jbe	.L47
	cmpq	%rax, %rsi
	jbe	.L48
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	subq	%rsi, %rax
	movq	%rax, %rbp
.L47:
	movq	%rbp, %r12
.L44:
	addq	(%rbx), %rsi
	movq	%rbp, %rdx
	call	__GI_memcpy@PLT
	addq	%rbp, 24(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	fmemopen_read, .-fmemopen_read
	.p2align 4,,15
	.globl	__GI___fmemopen
	.hidden	__GI___fmemopen
	.type	__GI___fmemopen, @function
__GI___fmemopen:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbp
	movl	$1, %esi
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$40, %edi
	movq	%rdx, %r14
	subq	$48, %rsp
	call	calloc@PLT
	testq	%rax, %rax
	je	.L63
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbp, %rbp
	sete	%al
	movl	%eax, 8(%rbx)
	je	.L72
	movq	%rbp, %rax
	negq	%rax
	cmpq	%r13, %rax
	jb	.L73
	movzbl	(%r14), %r12d
	movq	%rbp, (%rbx)
	cmpb	$119, %r12b
	je	.L74
.L56:
	cmpb	$97, %r12b
	je	.L75
	cmpb	$114, %r12b
	movq	%r13, 16(%rbx)
	jne	.L76
.L53:
	movq	%r13, 32(%rbx)
	movl	$0, 12(%rbx)
.L59:
	movq	$0, 24(%rbx)
.L60:
	leaq	fmemopen_read(%rip), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, 16(%rsp)
	leaq	fmemopen_write(%rip), %rax
	movq	%rax, 24(%rsp)
	leaq	fmemopen_seek(%rip), %rax
	movq	%rax, 32(%rsp)
	leaq	fmemopen_close(%rip), %rax
	movq	%rax, 40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	_IO_fopencookie@PLT
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L77
.L49:
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L78
	movzbl	(%r14), %r12d
	movb	$0, (%rax)
	movq	%r13, 16(%rbx)
	cmpb	$114, %r12b
	je	.L53
.L54:
	xorl	%eax, %eax
	cmpb	$97, %r12b
	sete	%al
	movl	%eax, 12(%rbx)
	jne	.L59
	movq	32(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	cmpb	$43, 1(%r14)
	je	.L79
.L57:
	movq	%r13, 16(%rbx)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$0, 12(%rbx)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__GI_strnlen
	movq	%rax, 32(%rbx)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L77:
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jne	.L80
.L61:
	movq	%rbx, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L79:
	movb	$0, 0(%rbp)
	movzbl	(%r14), %r12d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rbx, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%eax, %eax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rbx), %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	jmp	.L61
.L78:
	movq	%rbx, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L49
	.size	__GI___fmemopen, .-__GI___fmemopen
	.globl	__fmemopen
	.set	__fmemopen,__GI___fmemopen
