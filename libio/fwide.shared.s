	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	fwide
	.type	fwide, @function
fwide:
.LFB71:
	pushq	%r12
	pushq	%rbp
	testl	%esi, %esi
	pushq	%rbx
	movl	192(%rdi), %eax
	movq	%rdi, %rbx
	movl	%eax, %r8d
	js	.L33
	je	.L1
	testl	%eax, %eax
	jne	.L1
	testl	$32768, (%rdi)
	jne	.L31
	movq	136(%rdi), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	jne	.L34
	addl	$1, 4(%rdi)
.L31:
	movl	$1, %r12d
.L7:
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	movl	(%rbx), %edx
	movl	%eax, %r8d
	andl	$32768, %edx
	testl	%edx, %edx
	je	.L35
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	testl	%eax, %eax
	jne	.L1
	movl	(%rdi), %ecx
	movl	%ecx, %edx
	andl	$32768, %edx
	je	.L4
.L5:
	movl	$-1, %eax
	movl	%eax, 192(%rbx)
	movl	%eax, %r8d
.L13:
	testl	%edx, %edx
	jne	.L1
.L35:
	movq	136(%rbx), %rdi
	subl	$1, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	subl	$1, (%rdi)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	136(%rdi), %rdi
	movq	%fs:16, %rbp
	cmpq	8(%rdi), %rbp
	je	.L36
	movl	$-1, %r12d
.L19:
#APP
# 44 "fwide.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L8
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L9:
	movq	136(%rbx), %rax
	addl	$1, 4(%rax)
	cmpl	$-1, %r12d
	movq	%rbp, 8(%rax)
	jne	.L7
	movl	192(%rbx), %eax
	movl	(%rbx), %ecx
.L18:
	andl	$32768, %ecx
	testl	%eax, %eax
	movl	%eax, %r8d
	movl	%ecx, %edx
	jne	.L13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L9
.LEHB0:
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L36:
	addl	$1, 4(%rdi)
	jmp	.L18
.L24:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L16
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L16
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L16:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L17:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L16
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
.LFE71:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA71:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE71-.LLSDACSB71
.LLSDACSB71:
	.uleb128 .LEHB0-.LFB71
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L24-.LFB71
	.uleb128 0
	.uleb128 .LEHB1-.LFB71
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE71:
	.text
	.size	fwide, .-fwide
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
