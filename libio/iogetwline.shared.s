	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_getwline_info
	.type	_IO_getwline_info, @function
_IO_getwline_info:
.LFB72:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%ecx, %r12d
	subq	$40, %rsp
	testq	%r9, %r9
	movl	%r8d, 20(%rsp)
	movq	%r9, 24(%rsp)
	je	.L2
	movl	$0, (%r9)
.L2:
	movl	192(%r15), %eax
	testl	%eax, %eax
	je	.L31
.L3:
	testq	%r13, %r13
	movq	%rbp, 8(%rsp)
	jne	.L15
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	%r13, %rbx
	movl	%r12d, %esi
	movq	%r14, %rdi
	cmovnb	%r13, %rbx
	movq	%rbx, %rdx
	call	__GI_wmemchr
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L32
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	call	__wmemcpy
	movq	160(%r15), %rdx
	leaq	0(,%rbx,4), %rax
	subq	%rbx, %r13
	addq	%rax, %rbp
	addq	%rax, (%rdx)
	testq	%r13, %r13
	je	.L33
.L15:
	movq	160(%r15), %rax
	movq	(%rax), %r14
	movq	8(%rax), %rax
	subq	%r14, %rax
	movq	%rax, %rbx
	sarq	$2, %rbx
	testq	%rax, %rax
	jg	.L5
	movq	%r15, %rdi
	call	__GI___wuflow
	cmpl	$-1, %eax
	je	.L34
	cmpl	%eax, %r12d
	je	.L35
	subq	$1, %r13
	movl	%eax, 0(%rbp)
	addq	$4, %rbp
	testq	%r13, %r13
	jne	.L15
.L33:
	subq	8(%rsp), %rbp
	sarq	$2, %rbp
.L1:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rbp, %rbx
	subq	8(%rsp), %rbx
	movl	20(%rsp), %ecx
	movq	%rax, %r12
	subq	%r14, %r12
	sarq	$2, %r12
	sarq	$2, %rbx
	testl	%ecx, %ecx
	js	.L14
	leaq	1(%r12), %rax
	addq	$4, %r8
	testl	%ecx, %ecx
	cmovne	%rax, %r12
.L14:
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, 8(%rsp)
	leaq	(%r12,%rbx), %rbp
	call	__wmemcpy
	movq	160(%r15), %rax
	movq	8(%rsp), %r8
	movq	%r8, (%rax)
	jmp	.L1
.L31:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_IO_fwide@PLT
	jmp	.L3
.L34:
	subq	8(%rsp), %rbp
	movq	24(%rsp), %rcx
	sarq	$2, %rbp
	testq	%rcx, %rcx
	je	.L1
	movl	%eax, (%rcx)
	jmp	.L1
.L35:
	cmpl	$0, 20(%rsp)
	jle	.L8
	leaq	4(%rbp), %rbx
	movl	%r12d, 0(%rbp)
.L9:
	subq	8(%rsp), %rbx
	movq	%rbx, %rbp
	sarq	$2, %rbp
	jmp	.L1
.L16:
	xorl	%ebp, %ebp
	jmp	.L1
.L8:
	movq	%rbp, %rbx
	je	.L9
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	__GI__IO_sputbackc
	jmp	.L9
.LFE72:
	.size	_IO_getwline_info, .-_IO_getwline_info
	.p2align 4,,15
	.globl	_IO_getwline
	.type	_IO_getwline, @function
_IO_getwline:
.LFB71:
	xorl	%r9d, %r9d
	jmp	_IO_getwline_info@PLT
.LFE71:
	.size	_IO_getwline, .-_IO_getwline
	.hidden	__wmemcpy
