	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_fwrite_unlocked
	.hidden	__GI_fwrite_unlocked
	.type	__GI_fwrite_unlocked, @function
__GI_fwrite_unlocked:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	imulq	%rdx, %rbx
	subq	$24, %rsp
	testq	%rbx, %rbx
	je	.L1
	movl	192(%rcx), %eax
	testl	%eax, %eax
	jne	.L3
	movl	$-1, 192(%rcx)
.L4:
	movq	216(%rcx), %r13
	movq	%rdx, %r12
	leaq	__stop___libc_IO_vtables(%rip), %rax
	leaq	__start___libc_IO_vtables(%rip), %rdx
	movq	%rsi, %rbp
	movq	%rdi, %rsi
	movq	%r13, %rdi
	subq	%rdx, %rax
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L14
.L6:
	movq	%rbx, %rdx
	movq	%rcx, %rdi
	call	*56(%r13)
	cmpq	%rax, %rbx
	je	.L8
	cmpq	$-1, %rax
	je	.L8
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %rbx
.L1:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$-1, %eax
	je	.L4
	addq	$24, %rsp
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rcx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rcx
	movq	(%rsp), %rsi
	jmp	.L6
	.size	__GI_fwrite_unlocked, .-__GI_fwrite_unlocked
	.globl	fwrite_unlocked
	.set	fwrite_unlocked,__GI_fwrite_unlocked
	.hidden	_IO_vtable_check
	.hidden	__start___libc_IO_vtables
	.hidden	__stop___libc_IO_vtables
