	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fsetlocking
	.hidden	__GI___fsetlocking
	.type	__GI___fsetlocking, @function
__GI___fsetlocking:
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testb	$-128, %dh
	setne	%al
	addl	$1, %eax
	testl	%esi, %esi
	je	.L1
	andb	$127, %dh
	movl	%edx, %ecx
	orb	$-128, %ch
	cmpl	$2, %esi
	cmove	%ecx, %edx
	movl	%edx, (%rdi)
.L1:
	rep ret
	.size	__GI___fsetlocking, .-__GI___fsetlocking
	.globl	__fsetlocking
	.set	__fsetlocking,__GI___fsetlocking
