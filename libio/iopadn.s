	.text
	.p2align 4,,15
	.globl	_IO_padn
	.hidden	_IO_padn
	.type	_IO_padn, @function
_IO_padn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	blanks(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$40, %rsp
	cmpl	$32, %esi
	je	.L2
	cmpl	$48, %esi
	leaq	zeroes(%rip), %r13
	je	.L2
	leaq	16(%rsp), %r13
	leaq	15(%rsp), %rcx
	leaq	15(%r13), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movb	%sil, (%rax)
	subq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L3
.L2:
	cmpl	$15, %edx
	movl	%edx, %r15d
	jle	.L13
	movl	%edx, %r12d
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r14
	xorl	%ebx, %ebx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %r14
	andl	$15, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*56(%rax)
	addq	%rax, %rbx
	cmpq	$16, %rax
	jne	.L1
	subl	$16, %r15d
	cmpl	%r12d, %r15d
	je	.L4
.L8:
	movq	216(%rbp), %rax
	movq	%rax, %rdx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	cmpq	%rdx, %r14
	ja	.L5
	movq	%rax, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r15d, %r15d
	jle	.L1
	movq	216(%rbp), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L20
.L10:
	movslq	%r15d, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*56(%r12)
	addq	%rax, %rbx
.L1:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L20:
	call	_IO_vtable_check
	jmp	.L10
	.size	_IO_padn, .-_IO_padn
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	zeroes, @object
	.size	zeroes, 16
zeroes:
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.byte	48
	.align 16
	.type	blanks, @object
	.size	blanks, 16
blanks:
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.weak	__start___libc_IO_vtables
	.weak	__stop___libc_IO_vtables
	.hidden	_IO_vtable_check
