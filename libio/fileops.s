	.text
	.p2align 4,,15
	.type	_IO_file_seekoff_maybe_mmap, @function
_IO_file_seekoff_maybe_mmap:
.LFB107:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	subq	%rcx, %rax
	subq	%rcx, %rdi
	cmpq	%rdi, %rax
	jbe	.L7
.L2:
	movq	%rbx, %rdi
	call	*128(%rbp)
	testq	%rax, %rax
	js	.L4
	movq	%rax, 144(%rbx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%edx, 12(%rsp)
	movq	%rsi, (%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %edx
	movq	(%rsp), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movq	$-1, %rax
	jmp	.L1
.LFE107:
	.size	_IO_file_seekoff_maybe_mmap, .-_IO_file_seekoff_maybe_mmap
	.p2align 4,,15
	.globl	_IO_file_close
	.hidden	_IO_file_close
	.type	_IO_file_close, @function
_IO_file_close:
.LFB112:
	movl	112(%rdi), %edi
	jmp	__close_nocancel
.LFE112:
	.size	_IO_file_close, .-_IO_file_close
	.p2align 4,,15
	.globl	_IO_new_file_setbuf
	.type	_IO_new_file_setbuf, @function
_IO_new_file_setbuf:
.LFB92:
	pushq	%rbx
	movq	%rdi, %rbx
	call	_IO_default_setbuf@PLT
	testq	%rax, %rax
	je	.L9
	movq	56(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
.L9:
	popq	%rbx
	ret
.LFE92:
	.size	_IO_new_file_setbuf, .-_IO_new_file_setbuf
	.weak	_IO_file_setbuf
	.hidden	_IO_file_setbuf
	.set	_IO_file_setbuf,_IO_new_file_setbuf
	.p2align 4,,15
	.globl	_IO_file_setbuf_mmap
	.type	_IO_file_setbuf_mmap, @function
_IO_file_setbuf_mmap:
.LFB93:
	leaq	_IO_file_jumps(%rip), %rax
	pushq	%rbx
	leaq	_IO_wfile_jumps(%rip), %rcx
	movq	%rdi, %rbx
	movq	%rax, 216(%rdi)
	movq	160(%rdi), %rax
	movq	%rcx, 224(%rax)
	call	_IO_default_setbuf@PLT
	testq	%rax, %rax
	je	.L19
	movq	56(%rbx), %rax
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	160(%rbx), %rdx
	leaq	_IO_file_jumps_mmap(%rip), %rcx
	leaq	_IO_wfile_jumps_mmap(%rip), %rdi
	movq	%rcx, 216(%rbx)
	movq	%rdi, 224(%rdx)
	popq	%rbx
	ret
.LFE93:
	.size	_IO_file_setbuf_mmap, .-_IO_file_setbuf_mmap
	.p2align 4,,15
	.type	new_do_write, @function
new_do_write:
.LFB95:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	216(%rdi), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	movq	%r12, %rax
	subq	%r13, %rbp
	subq	%r13, %rax
	testl	$4096, (%rdi)
	je	.L21
	movq	$-1, 144(%rdi)
.L22:
	cmpq	%rax, %rbp
	jbe	.L38
.L25:
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	*120(%r12)
	testq	%rax, %rax
	movq	%rax, %r12
	movzwl	128(%rbx), %edi
	je	.L26
	testw	%di, %di
	jne	.L39
.L26:
	movl	192(%rbx), %edx
	movq	56(%rbx), %rax
	testl	%edx, %edx
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	jle	.L40
.L27:
	movq	64(%rbx), %rax
.L28:
	movq	%rax, 48(%rbx)
.L20:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	16(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L22
	cmpq	%rax, %rbp
	jbe	.L41
.L23:
	subq	%rdx, %rsi
	movq	%rbx, %rdi
	movl	$1, %edx
	call	*128(%r12)
	xorl	%r12d, %r12d
	cmpq	$-1, %rax
	je	.L20
	movq	216(%rbx), %r12
	movq	%rax, 144(%rbx)
	movq	%r12, %rax
	subq	%r13, %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L40:
	testl	$514, (%rbx)
	jne	.L28
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L39:
	subl	$1, %edi
	movl	%eax, %edx
	movq	%r14, %rsi
	call	_IO_adjust_column
	addl	$1, %eax
	movw	%ax, 128(%rbx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L38:
	call	_IO_vtable_check
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L41:
	call	_IO_vtable_check
	movq	32(%rbx), %rsi
	movq	16(%rbx), %rdx
	jmp	.L23
.LFE95:
	.size	new_do_write, .-new_do_write
	.p2align 4,,15
	.globl	_IO_new_file_underflow
	.type	_IO_new_file_underflow, @function
_IO_new_file_underflow:
.LFB96:
	movl	(%rdi), %eax
	testb	$16, %al
	jne	.L71
	testb	$4, %al
	jne	.L81
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	jb	.L82
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 56(%rdi)
	je	.L83
	testl	$514, %eax
	je	.L84
.L48:
	movq	stdout(%rip), %r12
	movl	(%r12), %edx
	movl	%edx, %esi
	andl	$32768, %esi
	je	.L85
	movq	%r12, %rdi
.L50:
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rbp
	andl	$648, %edx
	movq	%r14, %r13
	subq	%rbp, %r13
	cmpl	$640, %edx
	je	.L56
.L60:
	testl	$32768, (%r12)
	jne	.L58
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L58
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L61
	subl	$1, (%rdi)
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_switch_to_get_mode
	movq	216(%rbx), %r12
	movq	56(%rbx), %rsi
	movq	%r12, %rax
	subq	%rbp, %rax
	movq	%rsi, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	%r13, %rax
	movq	%rsi, 16(%rbx)
	movq	%rsi, 48(%rbx)
	movq	%rsi, 40(%rbx)
	movq	%rsi, 32(%rbx)
	jnb	.L86
.L62:
	movq	64(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	*112(%r12)
.LEHE0:
	testq	%rax, %rax
	jle	.L87
	movq	144(%rbx), %rdx
	addq	%rax, 16(%rbx)
	cmpq	$-1, %rdx
	je	.L67
	addq	%rdx, %rax
	movq	%rax, 144(%rbx)
.L67:
	movq	8(%rbx), %rax
	movzbl	(%rax), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movzbl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movq	136(%r12), %rcx
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rcx)
	je	.L73
#APP
# 497 "fileops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L52
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L53:
	movq	stdout(%rip), %rdi
	movq	136(%r12), %rcx
	movl	(%rdi), %edx
	movq	%rbp, 8(%rcx)
.L51:
	addl	$1, 4(%rcx)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L84:
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rbp
.L58:
	subq	%rbp, %r14
	movq	%r14, %r13
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L83:
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	free@PLT
	andl	$-257, (%rbx)
.L47:
	movq	%rbx, %rdi
	call	_IO_doallocbuf
	movl	(%rbx), %eax
	testl	$514, %eax
	jne	.L48
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L87:
	movl	(%rbx), %edx
	movl	%edx, %ecx
	orl	$16, %edx
	orl	$32, %ecx
	testq	%rax, %rax
	movq	$-1, %rax
	cmovne	%ecx, %edx
	movq	%rax, 144(%rbx)
	movl	%edx, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	216(%rdi), %r15
	movq	%r15, %rax
	subq	%rbp, %rax
	cmpq	%r13, %rax
	jnb	.L88
.L59:
	movl	$-1, %esi
.LEHB1:
	call	*24(%r15)
.LEHE1:
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r12, %rdi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L86:
.LEHB2:
	call	_IO_vtable_check
.LEHE2:
	movq	56(%rbx), %rsi
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	orl	$32, %eax
	movl	%eax, (%rdi)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movl	%esi, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L53
	movq	%rcx, %rdi
.LEHB3:
	call	__lll_lock_wait_private
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L88:
	call	_IO_vtable_check
.LEHE3:
	movq	stdout(%rip), %rdi
	jmp	.L59
.L61:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L58
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L49
.L74:
	testl	$32768, (%r12)
	movq	%rax, %r8
	jne	.L69
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L69
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L70
	subl	$1, (%rdi)
.L69:
	movq	%r8, %rdi
.LEHB4:
	call	_Unwind_Resume@PLT
.LEHE4:
.L70:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L69
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L69
.LFE96:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA96:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE96-.LLSDACSB96
.LLSDACSB96:
	.uleb128 .LEHB0-.LFB96
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB96
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L74-.LFB96
	.uleb128 0
	.uleb128 .LEHB2-.LFB96
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB96
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L74-.LFB96
	.uleb128 0
	.uleb128 .LEHB4-.LFB96
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE96:
	.text
	.size	_IO_new_file_underflow, .-_IO_new_file_underflow
	.weak	_IO_file_underflow
	.hidden	_IO_file_underflow
	.set	_IO_file_underflow,_IO_new_file_underflow
	.p2align 4,,15
	.globl	_IO_file_close_mmap
	.type	_IO_file_close_mmap, @function
_IO_file_close_mmap:
.LFB111:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	movq	64(%rbx), %rsi
	subq	%rdi, %rsi
	call	__munmap
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movl	112(%rbx), %edi
	popq	%rbx
	jmp	__close_nocancel
.LFE111:
	.size	_IO_file_close_mmap, .-_IO_file_close_mmap
	.p2align 4,,15
	.type	mmap_remap_check, @function
mmap_remap_check:
.LFB97:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L109
.L92:
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	call	*144(%rbp)
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	movl	%eax, %ebp
	subq	%rdi, %rsi
	testl	%eax, %eax
	jne	.L94
	movl	40(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L110
.L94:
	call	__munmap
.L99:
	movl	192(%rbx), %eax
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	testl	%eax, %eax
	jle	.L111
	leaq	_IO_wfile_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
.L104:
	movq	160(%rbx), %rdx
	movl	$1, %ebp
	movq	%rax, 224(%rdx)
.L91:
	addq	$160, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movq	64(%rsp), %r12
	testq	%r12, %r12
	je	.L94
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__getpagesize
	movq	8(%rsp), %rsi
	cltq
	movq	(%rsp), %rdi
	movq	%rax, %rcx
	leaq	-1(%r12,%rax), %rdx
	negq	%rcx
	leaq	-1(%rax,%rsi), %rsi
	andq	%rcx, %rdx
	andq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jb	.L112
	ja	.L113
	addq	%rdi, %r12
	movq	%r12, 64(%rbx)
.L96:
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	144(%rbx), %rcx
	subq	%rdi, %rsi
	movq	%rdi, 24(%rbx)
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	movq	%rcx, 144(%rbx)
	jge	.L100
	addq	%rcx, %rdi
	xorl	%edx, %edx
	movq	%r12, 16(%rbx)
	movq	%rdi, 8(%rbx)
	movl	112(%rbx), %edi
	call	__lseek64
	movq	64(%rbx), %rdx
	subq	56(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.L101
	orl	$32, (%rbx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	_IO_file_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	_IO_wfile_jumps(%rip), %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L109:
	call	_IO_vtable_check
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, 8(%rbx)
	movq	%r12, 16(%rbx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rdx, 144(%rbx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L112:
	addq	%rdx, %rdi
	subq	%rdx, %rsi
	call	__munmap
	movq	56(%rbx), %rdi
	movq	64(%rsp), %r12
	addq	%rdi, %r12
	movq	%r12, 64(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%eax, %eax
	movl	$1, %ecx
	call	__mremap
	cmpq	$-1, %rax
	movq	%rax, %rdi
	je	.L114
	movq	64(%rsp), %r12
	movq	%rax, 56(%rbx)
	addq	%rax, %r12
	movq	%r12, 64(%rbx)
	jmp	.L96
.L114:
	movq	56(%rbx), %rdi
	movq	64(%rbx), %rsi
	subq	%rdi, %rsi
	call	__munmap
	jmp	.L99
.LFE97:
	.size	mmap_remap_check, .-mmap_remap_check
	.p2align 4,,15
	.globl	_IO_file_seek
	.hidden	_IO_file_seek
	.type	_IO_file_seek, @function
_IO_file_seek:
.LFB109:
	movl	112(%rdi), %edi
	jmp	__lseek64
.LFE109:
	.size	_IO_file_seek, .-_IO_file_seek
	.p2align 4,,15
	.type	_IO_file_sync_mmap, @function
_IO_file_sync_mmap:
.LFB103:
	pushq	%rbx
	movq	8(%rdi), %rdx
	movq	%rdi, %rbx
	movq	%rdx, %rax
	subq	56(%rdi), %rax
	cmpq	16(%rdi), %rdx
	je	.L117
	movl	112(%rdi), %edi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	__lseek64
	movq	8(%rbx), %rdx
	subq	56(%rbx), %rdx
	cmpq	%rax, %rdx
	jne	.L120
.L117:
	movq	%rax, 144(%rbx)
	movq	24(%rbx), %rax
	movq	%rax, 8(%rbx)
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	orl	$32, (%rbx)
	movl	$-1, %eax
	popq	%rbx
	ret
.LFE103:
	.size	_IO_file_sync_mmap, .-_IO_file_sync_mmap
	.p2align 4,,15
	.type	decide_maybe_mmap, @function
decide_maybe_mmap:
.LFB99:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L145
.L122:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	call	*144(%rbp)
	testl	%eax, %eax
	jne	.L126
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L146
.L126:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L147
	leaq	_IO_wfile_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
.L132:
	movq	160(%rbx), %rdx
	movq	%rax, 224(%rdx)
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	_IO_file_jumps(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	_IO_wfile_jumps(%rip), %rax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L146:
	movq	48(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L126
	movq	144(%rbx), %rax
	cmpq	%rax, %rsi
	jge	.L134
	cmpq	$-1, %rax
	jne	.L126
.L134:
	movl	112(%rbx), %r8d
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$1, %ecx
	movl	$1, %edx
	call	__mmap64
	cmpq	$-1, %rax
	movq	%rax, %rbp
	je	.L126
	movq	48(%rsp), %rsi
	movl	112(%rbx), %edi
	xorl	%edx, %edx
	call	__lseek64
	movq	48(%rsp), %rsi
	cmpq	%rax, %rsi
	je	.L125
	movq	%rbp, %rdi
	call	__munmap
	movq	$-1, 144(%rbx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L145:
	call	_IO_vtable_check
	jmp	.L122
.L125:
	leaq	0(%rbp,%rsi), %rdx
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_setb
	movq	144(%rbx), %rdx
	movq	%rbp, 24(%rbx)
	leaq	0(%rbp,%rdx), %rax
	cmpq	$-1, %rdx
	movl	192(%rbx), %edx
	cmove	%rbp, %rax
	movq	%rax, 8(%rbx)
	movq	48(%rsp), %rax
	addq	%rax, %rbp
	testl	%edx, %edx
	movq	%rax, 144(%rbx)
	movq	%rbp, 16(%rbx)
	jle	.L148
	leaq	_IO_wfile_jumps_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
	jmp	.L132
.L148:
	leaq	_IO_file_jumps_mmap(%rip), %rax
	movq	%rax, 216(%rbx)
	leaq	_IO_wfile_jumps_mmap(%rip), %rax
	jmp	.L132
.LFE99:
	.size	decide_maybe_mmap, .-decide_maybe_mmap
	.p2align 4,,15
	.globl	_IO_file_underflow_maybe_mmap
	.type	_IO_file_underflow_maybe_mmap, @function
_IO_file_underflow_maybe_mmap:
.LFB100:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	decide_maybe_mmap
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L152
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L152:
	call	_IO_vtable_check
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
.LFE100:
	.size	_IO_file_underflow_maybe_mmap, .-_IO_file_underflow_maybe_mmap
	.p2align 4,,15
	.type	_IO_file_xsgetn_maybe_mmap, @function
_IO_file_xsgetn_maybe_mmap:
.LFB117:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	subq	$8, %rsp
	call	decide_maybe_mmap
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdx
	subq	%rcx, %rax
	subq	%rcx, %rdx
	cmpq	%rdx, %rax
	jbe	.L156
.L154:
	movq	64(%rbp), %rax
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L156:
	call	_IO_vtable_check
	jmp	.L154
.LFE117:
	.size	_IO_file_xsgetn_maybe_mmap, .-_IO_file_xsgetn_maybe_mmap
	.p2align 4,,15
	.globl	_IO_new_file_seekoff
	.type	_IO_new_file_seekoff, @function
_IO_new_file_seekoff:
.LFB105:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$184, %rsp
	testl	%ecx, %ecx
	je	.L226
	movq	16(%rdi), %rcx
	cmpq	%rcx, 24(%rdi)
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	32(%rdi), %rax
	movq	40(%rdi), %rdx
	je	.L227
.L172:
	cmpq	%rax, %rdx
	movl	$0, 8(%rsp)
	jbe	.L173
.L174:
	movq	%r15, %rdi
	call	_IO_switch_to_get_mode
	testl	%eax, %eax
	jne	.L224
	cmpq	$0, 56(%r15)
	je	.L228
.L175:
	cmpl	$1, %r12d
	je	.L178
.L232:
	cmpl	$2, %r12d
	jne	.L177
	movq	216(%r15), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	movq	%r14, %rax
	subq	%r13, %rbp
	subq	%r13, %rax
	cmpq	%rax, %rbp
	jbe	.L229
.L182:
	leaq	32(%rsp), %rsi
	movq	%r15, %rdi
	call	*144(%r14)
	testl	%eax, %eax
	je	.L230
.L184:
	movq	%r15, %rdi
	call	_IO_unsave_markers
	movq	216(%r15), %r14
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	%rbp, %rax
	jnb	.L231
.L194:
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*128(%r14)
	cmpq	$-1, %rax
	je	.L157
	movq	56(%r15), %rdx
	andl	$-17, (%r15)
	movq	%rax, 144(%r15)
	movq	%rdx, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	%rdx, 16(%r15)
	movq	%rdx, 40(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 48(%r15)
.L157:
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	cmpq	%rax, %rdx
	jne	.L172
	movl	$1, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L173:
	testl	$2048, (%r15)
	jne	.L174
	cmpq	$0, 56(%r15)
	jne	.L175
.L228:
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L176
	call	free@PLT
	andl	$-257, (%r15)
.L176:
	movq	%r15, %rdi
	call	_IO_doallocbuf
	movq	56(%r15), %rax
	cmpl	$1, %r12d
	movq	%rax, 40(%r15)
	movq	%rax, 32(%r15)
	movq	%rax, 48(%r15)
	movq	%rax, 24(%r15)
	movq	%rax, 8(%r15)
	movq	%rax, 16(%r15)
	jne	.L232
.L178:
	movq	16(%r15), %rax
	subq	8(%r15), %rax
	subq	%rax, %rbx
	movq	144(%r15), %rax
	cmpq	$-1, %rax
	je	.L180
	addq	%rax, %rbx
	js	.L181
.L223:
	xorl	%r12d, %r12d
.L177:
	movq	%r15, %rdi
	call	_IO_free_backup_area
	movq	144(%r15), %rsi
	movl	(%r15), %eax
	cmpq	$-1, %rsi
	je	.L186
	cmpq	$0, 24(%r15)
	je	.L186
	testb	$1, %ah
	jne	.L186
	movq	56(%r15), %rdx
	movq	%rdx, %rcx
	subq	16(%r15), %rcx
	addq	%rsi, %rcx
	cmpq	%rcx, %rbx
	jl	.L186
	cmpq	%rbx, %rsi
	jg	.L233
	.p2align 4,,10
	.p2align 3
.L186:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r14
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	subq	%r14, %rbp
	testb	$4, %al
	movq	%r14, %r13
	jne	.L184
	movq	56(%r15), %rdx
	movq	64(%r15), %rax
	movq	%rbx, %r12
	movq	%rdx, %rsi
	subq	%rax, %rsi
	subq	%rdx, %rax
	andq	%rbx, %rsi
	subq	%rsi, %r12
	cmpq	%r12, %rax
	jge	.L188
	movq	%rbx, %rsi
	xorl	%r12d, %r12d
.L188:
	movq	216(%r15), %rax
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	%rbp, %rdx
	jnb	.L234
.L189:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	*128(%rax)
	testq	%rax, %rax
	movq	%rax, %rcx
	js	.L224
	testq	%r12, %r12
	je	.L200
	movq	216(%r15), %rax
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	%rbp, %rdx
	jnb	.L235
.L191:
	movl	8(%rsp), %edi
	movq	112(%rax), %rax
	movq	%r12, %rdx
	movq	56(%r15), %rsi
	testl	%edi, %edi
	jne	.L192
	movq	64(%r15), %rdx
	subq	%rsi, %rdx
.L192:
	movq	%rcx, 8(%rsp)
	movq	%r15, %rdi
	call	*%rax
	cmpq	%rax, %r12
	jle	.L236
	cmpq	$-1, %rax
	movq	%r12, %rbx
	movl	$1, %r12d
	je	.L184
	subq	%rax, %rbx
	movl	$1, %r12d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L226:
	cmpq	$0, 56(%rdi)
	je	.L237
	movq	40(%rdi), %r12
	movq	32(%rdi), %rbx
	movl	(%rdi), %ebp
	andl	$4096, %ebp
	cmpq	%rbx, %r12
	jbe	.L203
	testl	%ebp, %ebp
	je	.L203
	movq	216(%rdi), %r13
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r13, %rdi
	subq	%rdx, %rax
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L238
.L164:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	call	*128(%r13)
	cmpq	$-1, %rax
	je	.L224
	cmpq	%rbx, %r12
	movq	%rax, 144(%r15)
	ja	.L167
.L240:
	movq	8(%r15), %rbx
	subq	16(%r15), %rbx
.L160:
	cmpq	$-1, %rax
	je	.L239
.L169:
	addq	%rbx, %rax
	jns	.L157
.L181:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L224:
	movq	$-1, %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L230:
	movl	56(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L184
	addq	80(%rsp), %rbx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L180:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	subq	%r13, %rbp
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%rbx, %r12
	movq	144(%r15), %rax
	jbe	.L240
.L167:
	testl	%ebp, %ebp
	movq	40(%r15), %rbx
	je	.L168
	subq	32(%r15), %rbx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L239:
	movq	216(%r15), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	subq	%rdx, %rax
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L241
.L170:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L224
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%eax, %eax
.L190:
	movq	56(%r15), %rdx
	andl	$-17, (%r15)
	movq	%rcx, 144(%r15)
	addq	%rdx, %r12
	addq	%rdx, %rax
	movq	%rdx, 24(%r15)
	movq	%r12, 8(%r15)
	movq	%rax, 16(%r15)
	movq	%rdx, 40(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 48(%r15)
.L225:
	movq	%rbx, %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L237:
	movq	144(%rdi), %rax
	xorl	%ebx, %ebx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L231:
	call	_IO_vtable_check
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L168:
	subq	16(%r15), %rbx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rbx, %rdi
	andl	$-17, %eax
	movq	%rdx, 24(%r15)
	subq	%rcx, %rdi
	movq	%rdx, 40(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdi, %rcx
	movq	%rdx, 48(%r15)
	movl	%eax, (%r15)
	addq	%rdx, %rcx
	testq	%rsi, %rsi
	movq	%rcx, 8(%r15)
	js	.L225
	movq	216(%r15), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	subq	%rdx, %rax
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L242
.L195:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	*128(%rbp)
	movq	%rbx, %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rsi, 24(%rsp)
	movq	%rax, 16(%rsp)
	call	_IO_vtable_check
	movq	24(%rsp), %rsi
	movq	16(%rsp), %rax
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L236:
	movq	8(%rsp), %rcx
	addq	%rax, %rcx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L229:
	call	_IO_vtable_check
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%rcx, 24(%rsp)
	movq	%rax, 16(%rsp)
	call	_IO_vtable_check
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L238:
	call	_IO_vtable_check
	jmp	.L164
.L241:
	call	_IO_vtable_check
	jmp	.L170
.L242:
	call	_IO_vtable_check
	movq	144(%r15), %rsi
	jmp	.L195
.LFE105:
	.size	_IO_new_file_seekoff, .-_IO_new_file_seekoff
	.weak	_IO_file_seekoff
	.hidden	_IO_file_seekoff
	.set	_IO_file_seekoff,_IO_new_file_seekoff
	.p2align 4,,15
	.globl	_IO_file_stat
	.hidden	_IO_file_stat
	.type	_IO_file_stat, @function
_IO_file_stat:
.LFB110:
	movl	112(%rdi), %edi
	jmp	__fstat64
.LFE110:
	.size	_IO_file_stat, .-_IO_file_stat
	.p2align 4,,15
	.globl	_IO_new_file_write
	.type	_IO_new_file_write, @function
_IO_new_file_write:
.LFB113:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	jle	.L251
	movq	%rsi, %rbp
	movq	%rdx, %r13
	movq	%rdx, %rbx
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L246:
	call	__write
	testq	%rax, %rax
	js	.L254
.L248:
	subq	%rax, %rbx
	addq	%rax, %rbp
	testq	%rbx, %rbx
	jle	.L255
.L249:
	testb	$2, 116(%r12)
	movl	112(%r12), %edi
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	je	.L246
	call	__write_nocancel
	testq	%rax, %rax
	jns	.L248
	.p2align 4,,10
	.p2align 3
.L254:
	orl	$32, (%r12)
	movq	%r13, %rax
	subq	%rbx, %rax
.L245:
	movq	144(%r12), %rdx
	testq	%rdx, %rdx
	js	.L244
	addq	%rax, %rdx
	movq	%rdx, 144(%r12)
.L244:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r13, %rax
	subq	%rbx, %rax
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L251:
	xorl	%eax, %eax
	jmp	.L245
.LFE113:
	.size	_IO_new_file_write, .-_IO_new_file_write
	.weak	_IO_file_write
	.set	_IO_file_write,_IO_new_file_write
	.p2align 4,,15
	.type	_IO_file_xsgetn_mmap, @function
_IO_file_xsgetn_mmap:
.LFB116:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbp
	subq	$24, %rsp
	movq	8(%rdi), %r13
	movq	16(%rdi), %rbx
	subq	%r13, %rbx
	cmpq	%rdx, %rbx
	jnb	.L257
	testl	$256, (%rdi)
	movq	%rsi, %rdx
	jne	.L272
.L258:
	movq	%r12, %rdi
	movq	%rdx, (%rsp)
	call	mmap_remap_check
	testl	%eax, %eax
	movq	(%rsp), %rdx
	jne	.L273
	movq	8(%r12), %r13
	movq	16(%r12), %rbx
	subq	%r13, %rbx
	cmpq	%rbx, %r14
	jbe	.L266
	orl	$16, (%r12)
	movq	%rdx, %rbp
.L257:
	testq	%rbx, %rbx
	jne	.L259
.L263:
	movq	%rbp, %rax
	subq	%r15, %rax
.L256:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__mempcpy@PLT
	movq	%r12, %rdi
	movq	%rax, 8(%rsp)
	movq	%rax, (%rsp)
	subq	%rbx, %r14
	call	_IO_switch_to_main_get_area@PLT
	movq	8(%r12), %r13
	movq	16(%r12), %rbx
	movq	(%rsp), %rdx
	movq	8(%rsp), %rax
	subq	%r13, %rbx
	cmpq	%rbx, %r14
	ja	.L258
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L259:
	cmpq	%r14, %rbx
	movq	%rbp, %rdi
	movq	%r13, %rsi
	cmova	%r14, %rbx
	movq	%rbx, %rdx
	addq	%r13, %rbx
	call	__mempcpy@PLT
	movq	%rbx, 8(%r12)
	movq	%rax, %rbp
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L273:
	subq	%rbp, %rdx
	movq	216(%r12), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rdx, %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	%r13, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L274
.L261:
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	*64(%r13)
	addq	%rbx, %rax
	jmp	.L256
.L274:
	call	_IO_vtable_check
	jmp	.L261
.L266:
	movq	%rdx, %rbp
	jmp	.L257
.LFE116:
	.size	_IO_file_xsgetn_mmap, .-_IO_file_xsgetn_mmap
	.p2align 4,,15
	.globl	_IO_file_xsgetn
	.hidden	_IO_file_xsgetn
	.type	_IO_file_xsgetn, @function
_IO_file_xsgetn:
.LFB115:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpq	$0, 56(%rdi)
	je	.L307
.L276:
	testq	%r14, %r14
	je	.L275
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rbp
	movq	%r14, %r12
	subq	%rsi, %rbp
	cmpq	%rbp, %r14
	ja	.L281
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L282:
	testl	$256, (%rbx)
	jne	.L308
.L283:
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L285
	movq	64(%rbx), %rcx
	subq	%rdx, %rcx
	cmpq	%r12, %rcx
	ja	.L309
	cmpq	$127, %rcx
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rdx, 48(%rbx)
	jbe	.L306
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%r12, %rax
	subq	%rdx, %rax
	movq	%rax, %rdx
.L293:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	subq	%rcx, %rax
	subq	%rcx, %rdi
	cmpq	%rdi, %rax
	jbe	.L310
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*112(%rbp)
	testq	%rax, %rax
	jle	.L311
.L288:
	movq	144(%rbx), %rdx
	addq	%rax, %r13
	subq	%rax, %r12
	cmpq	$-1, %rdx
	je	.L284
	addq	%rdx, %rax
	movq	%rax, 144(%rbx)
.L284:
	testq	%r12, %r12
	je	.L275
.L290:
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rbp
	subq	%rsi, %rbp
	cmpq	%rbp, %r12
	jbe	.L280
.L281:
	testq	%rbp, %rbp
	je	.L282
	movq	%r13, %rdi
	movq	%rbp, %rdx
	subq	%rbp, %r12
	call	__mempcpy@PLT
	addq	%rbp, 8(%rbx)
	testl	$256, (%rbx)
	movq	%rax, %r13
	je	.L283
.L308:
	movq	%rbx, %rdi
	call	_IO_switch_to_main_get_area@PLT
	testq	%r12, %r12
	jne	.L290
.L275:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
.L306:
	movq	%r12, %rdx
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rbx, %rdi
	call	__underflow
	cmpl	$-1, %eax
	jne	.L284
	addq	$16, %rsp
	subq	%r12, %r14
	popq	%rbx
	movq	%r14, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rdx, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*112(%rbp)
	testq	%rax, %rax
	jg	.L288
.L311:
	movl	(%rbx), %edx
	subq	%r12, %r14
	movl	%edx, %ecx
	orl	$16, %edx
	orl	$32, %ecx
	testq	%rax, %rax
	movq	%r14, %rax
	cmovne	%ecx, %edx
	movl	%edx, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L277
	call	free@PLT
	andl	$-257, (%rbx)
.L277:
	movq	%rbx, %rdi
	call	_IO_doallocbuf
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	addq	%r12, 8(%rbx)
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.LFE115:
	.size	_IO_file_xsgetn, .-_IO_file_xsgetn
	.p2align 4,,15
	.globl	_IO_file_seekoff_mmap
	.type	_IO_file_seekoff_mmap, @function
_IO_file_seekoff_mmap:
.LFB106:
	testl	%ecx, %ecx
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L326
	cmpl	$1, %edx
	je	.L316
	cmpl	$2, %edx
	je	.L317
	movq	%rsi, %rbp
.L315:
	testq	%rbp, %rbp
	js	.L327
	movq	216(%rdi), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	%rdi, %rbx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L328
.L319:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*128(%r12)
	testq	%rax, %rax
	js	.L323
	movq	64(%rbx), %rcx
	movq	56(%rbx), %rdx
	movq	%rcx, %rsi
	movq	%rdx, 24(%rbx)
	subq	%rdx, %rsi
	cmpq	%rsi, %rbp
	jle	.L320
	movq	%rcx, 8(%rbx)
	movq	%rcx, 16(%rbx)
.L321:
	andl	$-17, (%rbx)
	movq	%rax, 144(%rbx)
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	movq	16(%rdi), %rax
	subq	8(%rdi), %rax
	movq	144(%rdi), %rbp
	subq	%rax, %rbp
.L312:
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	movq	64(%rdi), %rbp
	subq	56(%rdi), %rbp
	addq	%rsi, %rbp
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L316:
	movq	8(%rdi), %rbp
	subq	24(%rdi), %rbp
	addq	%rsi, %rbp
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L320:
	addq	%rbp, %rdx
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L328:
	call	_IO_vtable_check
	jmp	.L319
.L327:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbp
	movl	$22, %fs:(%rax)
	jmp	.L312
.L323:
	movq	$-1, %rbp
	jmp	.L312
.LFE106:
	.size	_IO_file_seekoff_mmap, .-_IO_file_seekoff_mmap
	.p2align 4,,15
	.globl	_IO_file_underflow_mmap
	.type	_IO_file_underflow_mmap, @function
_IO_file_underflow_mmap:
.LFB98:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L330
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	mmap_remap_check
	testl	%eax, %eax
	jne	.L338
	movq	8(%rbx), %rax
	cmpq	16(%rbx), %rax
	jnb	.L334
	movzbl	(%rax), %eax
.L329:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L339
.L333:
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
.L334:
	orl	$16, (%rbx)
	movl	$-1, %eax
	jmp	.L329
.L339:
	call	_IO_vtable_check
	jmp	.L333
.LFE98:
	.size	_IO_file_underflow_mmap, .-_IO_file_underflow_mmap
	.p2align 4,,15
	.globl	_IO_file_read
	.hidden	_IO_file_read
	.type	_IO_file_read, @function
_IO_file_read:
.LFB108:
	movq	%rdi, %rax
	movl	112(%rdi), %edi
	testb	$2, 116(%rax)
	jne	.L342
	jmp	__read
	.p2align 4,,10
	.p2align 3
.L342:
	jmp	__read_nocancel
.LFE108:
	.size	_IO_file_read, .-_IO_file_read
	.p2align 4,,15
	.globl	_IO_new_file_xsputn
	.type	_IO_new_file_xsputn, @function
_IO_new_file_xsputn:
.LFB114:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L343
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	%rdx, %rbp
	movl	(%rbx), %eax
	movq	40(%rdi), %rdi
	andl	$2560, %eax
	cmpl	$2560, %eax
	je	.L374
	movq	48(%rbx), %rdx
	cmpq	%rdi, %rdx
	jbe	.L361
	subq	%rdi, %rdx
.L346:
	testq	%rdx, %rdx
	je	.L361
.L365:
	xorl	%r14d, %r14d
.L347:
	cmpq	%rdx, %rbp
	movq	%r13, %rsi
	movq	%rbp, %r15
	cmovbe	%rbp, %rdx
	movq	%rdx, %r12
	addq	%r12, %r13
	subq	%r12, %r15
	call	__mempcpy@PLT
	movq	%rax, 40(%rbx)
.L350:
	addq	%r15, %r14
	jne	.L352
.L373:
	subq	%r15, %rbp
	movq	%rbp, %r12
.L343:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%rbp, %r15
.L352:
	movq	216(%rbx), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L375
.L354:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%r12)
	cmpl	$-1, %eax
	je	.L376
	movq	64(%rbx), %rcx
	subq	56(%rbx), %rcx
	movq	%r15, %r14
	cmpq	$127, %rcx
	jbe	.L356
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rcx
	subq	%rdx, %r14
.L356:
	testq	%r14, %r14
	jne	.L377
.L357:
	testq	%r15, %r15
	movq	%rbp, %r12
	je	.L343
	leaq	0(%r13,%r14), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_IO_default_xsputn
	subq	%rax, %r15
	subq	%r15, %r12
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L374:
	movq	64(%rbx), %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, %rbp
	ja	.L346
	leaq	(%rsi,%rbp), %rcx
	cmpq	%rcx, %rsi
	jnb	.L365
	cmpb	$10, -1(%rcx)
	leaq	-1(%rcx), %rax
	jne	.L349
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L351:
	subq	$1, %rax
	cmpb	$10, (%rax)
	je	.L348
.L349:
	cmpq	%rax, %r13
	jne	.L351
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L376:
	subq	%r15, %rbp
	movq	$-1, %r12
	testq	%r15, %r15
	cmovne	%rbp, %r12
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	new_do_write
	subq	%rax, %r15
	cmpq	%r14, %rax
	jnb	.L357
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L348:
	subq	%r13, %rax
	addq	$1, %rax
	movq	%rax, %rdx
	jne	.L360
	movq	%rbp, %r15
	movl	$1, %r14d
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L375:
	call	_IO_vtable_check
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$1, %r14d
	jmp	.L347
.LFE114:
	.size	_IO_new_file_xsputn, .-_IO_new_file_xsputn
	.weak	_IO_file_xsputn
	.hidden	_IO_file_xsputn
	.set	_IO_file_xsputn,_IO_new_file_xsputn
	.p2align 4,,15
	.globl	_IO_new_file_init_internal
	.hidden	_IO_new_file_init_internal
	.type	_IO_new_file_init_internal, @function
_IO_new_file_init_internal:
.LFB85:
	pushq	%rbp
	pushq	%rbx
	movq	$-1, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	orl	$9228, (%rdi)
	movq	%rbp, 144(%rdi)
	call	_IO_link_in
	movl	%ebp, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE85:
	.size	_IO_new_file_init_internal, .-_IO_new_file_init_internal
	.p2align 4,,15
	.globl	_IO_new_file_init
	.type	_IO_new_file_init, @function
_IO_new_file_init:
.LFB86:
	pushq	%rbp
	pushq	%rbx
	movq	$-1, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	orl	$9228, (%rdi)
	movq	%rbp, 144(%rdi)
	call	_IO_link_in
	movl	%ebp, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE86:
	.size	_IO_new_file_init, .-_IO_new_file_init
	.weak	_IO_file_init
	.set	_IO_file_init,_IO_new_file_init
	.p2align 4,,15
	.globl	_IO_file_open
	.hidden	_IO_file_open
	.type	_IO_file_open, @function
_IO_file_open:
.LFB89:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%r8d, %ebp
	testb	$2, 116(%rbx)
	movq	%rsi, %rdi
	movl	%edx, %esi
	movl	%ecx, %edx
	jne	.L390
	call	__open
	movl	%eax, %r12d
.L384:
	testl	%r12d, %r12d
	js	.L388
	movl	(%rbx), %eax
	movl	%ebp, %edx
	andl	$4100, %ebp
	andl	$4108, %edx
	movl	%r12d, 112(%rbx)
	andl	$-4109, %eax
	orl	%edx, %eax
	cmpl	$4100, %ebp
	movl	%eax, (%rbx)
	jne	.L386
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L391
.L387:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L392
.L386:
	movq	%rbx, %rdi
	call	_IO_link_in
	movq	%rbx, %rax
.L382:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L386
	movl	%r12d, %edi
	call	__close_nocancel
	xorl	%eax, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L390:
	call	__open_nocancel
	movl	%eax, %r12d
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L388:
	xorl	%eax, %eax
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L391:
	call	_IO_vtable_check
	jmp	.L387
.LFE89:
	.size	_IO_file_open, .-_IO_file_open
	.p2align 4,,15
	.globl	_IO_new_file_attach
	.type	_IO_new_file_attach, @function
_IO_new_file_attach:
.LFB91:
	cmpl	$-1, 112(%rdi)
	jne	.L402
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	movq	216(%rdi), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%esi, 112(%rdi)
	movq	$-1, 144(%rdi)
	andl	$-13, %eax
	movq	%r12, %rcx
	orl	$64, %eax
	subq	%rdx, %rcx
	movl	%fs:0(%rbp), %r13d
	movl	%eax, (%rdi)
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	subq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L403
.L396:
	xorl	%esi, %esi
	movl	$3, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*72(%r12)
	cmpq	$-1, %rax
	je	.L404
.L397:
	movl	%r13d, %fs:0(%rbp)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	cmpl	$29, %fs:0(%rbp)
	je	.L397
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	call	_IO_vtable_check
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L402:
	xorl	%eax, %eax
	ret
.LFE91:
	.size	_IO_new_file_attach, .-_IO_new_file_attach
	.weak	_IO_file_attach
	.hidden	_IO_file_attach
	.set	_IO_file_attach,_IO_new_file_attach
	.p2align 4,,15
	.globl	_IO_new_do_write
	.type	_IO_new_do_write, @function
_IO_new_do_write:
.LFB94:
	testq	%rdx, %rdx
	jne	.L414
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	pushq	%rbx
	movq	%rdx, %rbx
	call	new_do_write
	cmpq	%rax, %rbx
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	popq	%rbx
	ret
.LFE94:
	.size	_IO_new_do_write, .-_IO_new_do_write
	.weak	_IO_do_write
	.hidden	_IO_do_write
	.set	_IO_do_write,_IO_new_do_write
	.p2align 4,,15
	.globl	_IO_new_file_close_it
	.type	_IO_new_file_close_it, @function
_IO_new_file_close_it:
.LFB87:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	112(%rdi), %ebp
	cmpl	$-1, %ebp
	je	.L415
	movl	(%rdi), %eax
	xorl	%r12d, %r12d
	movq	%rdi, %rbx
	andl	$2056, %eax
	cmpl	$2048, %eax
	je	.L429
.L417:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	_IO_unsave_markers
	testb	$32, 116(%rbx)
	jne	.L419
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L430
.L420:
	movq	%rbx, %rdi
	call	*136(%rbp)
	movl	%eax, %ebp
.L419:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L421
	movq	160(%rbx), %rax
	cmpq	$0, 64(%rax)
	je	.L422
	movq	%rbx, %rdi
	call	_IO_free_wbackup_area
.L422:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_wsetb
	movq	160(%rbx), %rax
	movq	$0, 16(%rax)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 32(%rax)
	movq	$0, 24(%rax)
	movq	$0, 40(%rax)
.L421:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_setb
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 48(%rbx)
	call	_IO_un_link
	testl	%ebp, %ebp
	movl	$-72539124, (%rbx)
	movl	$-1, 112(%rbx)
	movq	$-1, 144(%rbx)
	cmove	%r12d, %ebp
.L415:
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jle	.L431
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	movl	%eax, %r12d
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L431:
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	subq	%rsi, %rdx
	call	_IO_do_write
	movl	%eax, %r12d
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L430:
	call	_IO_vtable_check
	jmp	.L420
.LFE87:
	.size	_IO_new_file_close_it, .-_IO_new_file_close_it
	.weak	_IO_file_close_it
	.hidden	_IO_file_close_it
	.set	_IO_file_close_it,_IO_new_file_close_it
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	",ccs="
.LC1:
	.string	"fileops.c"
.LC2:
	.string	"fcts.towc_nsteps == 1"
.LC3:
	.string	"fcts.tomb_nsteps == 1"
	.text
	.p2align 4,,15
	.globl	_IO_new_file_fopen
	.type	_IO_new_file_fopen, @function
_IO_new_file_fopen:
.LFB90:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpl	$-1, 112(%rdi)
	jne	.L451
	movzbl	(%rdx), %eax
	movq	%rdi, %rbx
	cmpb	$114, %al
	je	.L436
	cmpb	$119, %al
	je	.L465
	cmpb	$97, %al
	je	.L485
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$22, %fs:(%rax)
.L432:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$4, %r8d
	movl	$1, %r9d
	movl	$576, %r10d
.L437:
	leaq	6(%rdx), %rdi
	movq	%rdx, %r12
.L448:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	cmpb	$99, %al
	je	.L440
	jg	.L441
	cmpb	$43, %al
	je	.L442
	cmpb	$98, %al
	je	.L484
	testb	%al, %al
	je	.L444
	.p2align 4,,10
	.p2align 3
.L439:
	cmpq	%rdi, %rdx
	jne	.L448
.L444:
	movl	%r10d, %edx
	movq	%rbx, %rdi
	orl	%r9d, %edx
	movl	%ecx, %r9d
	movl	$438, %ecx
	call	_IO_file_open
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L451
	leaq	1(%r12), %rdi
	leaq	.LC0(%rip), %rsi
	call	strstr
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L432
	leaq	5(%rax), %r15
	movl	$44, %esi
	movq	%r15, %rdi
	call	__strchrnul@PLT
	subq	%r15, %rax
	leaq	3(%rax), %rdi
	movq	%rax, %r14
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L486
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movb	$0, (%rax)
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L487
	movq	104+_nl_C_locobj(%rip), %r11
	movq	120+_nl_C_locobj(%rip), %r14
	movq	%r12, %rdi
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	movabsq	$2251799813701639, %r10
	.p2align 4,,10
	.p2align 3
.L458:
	movsbq	%dl, %r8
	leal	-44(%rdx), %ecx
	movzwl	(%r11,%r8,2), %eax
	shrw	$3, %ax
	andl	$1, %eax
	cmpb	$51, %cl
	ja	.L454
	movq	%r10, %r15
	shrq	%cl, %r15
	movq	%r15, %rcx
	andl	$1, %ecx
	orl	%ecx, %eax
.L454:
	testb	%al, %al
	leal	1(%r9), %ecx
	je	.L455
	movl	(%r14,%r8,4), %eax
	addq	$1, %rsi
	movb	%al, -1(%rsi)
.L456:
	addq	$1, %rdi
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	jne	.L458
	cmpl	$1, %r9d
	jg	.L457
	cmpl	$1, %ecx
	leaq	1(%rsi), %rax
	movb	$47, (%rsi)
	jne	.L466
.L453:
	leaq	1(%rax), %rsi
	movb	$47, (%rax)
.L457:
	movb	$0, (%rsi)
	cmpb	$0, 2(%r12)
	je	.L488
.L460:
	movq	%rsp, %rdi
	movq	%r12, %rsi
	call	__wcsmbs_named_conv
	testl	%eax, %eax
	jne	.L489
	movq	%r12, %rdi
	call	free@PLT
	cmpq	$1, 8(%rsp)
	jne	.L490
	cmpq	$1, 24(%rsp)
	jne	.L491
	movq	160(%rbx), %rax
	movq	16(%rsp), %rcx
	movq	8(%rax), %rdx
	movq	$0, 88(%rax)
	movq	%rdx, (%rax)
	movq	24(%rax), %rdx
	movq	%rdx, 32(%rax)
	movq	160(%rbx), %rax
	movq	$0, 96(%rax)
	movq	160(%rbx), %rax
	movq	160(%rbp), %rsi
	leaq	104(%rax), %rdx
	movq	%rdx, 152(%rbx)
	movq	(%rsp), %rdx
	movq	$1, 128(%rax)
	movl	$1, 136(%rax)
	movq	%rcx, 160(%rax)
	movq	$9, 184(%rax)
	movq	%rdx, 104(%rax)
	leaq	88(%rsi), %rdx
	movl	$1, 192(%rax)
	movq	%rdx, 144(%rax)
	movq	%rdx, 200(%rax)
	movq	224(%rax), %rax
	movq	%rax, 216(%rbx)
	movl	$1, 192(%rbp)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L441:
	cmpb	$109, %al
	je	.L445
	cmpb	$120, %al
	je	.L446
	cmpb	$101, %al
	jne	.L439
	orl	$524288, %r10d
	orl	$64, 116(%rbx)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L446:
	orb	$-128, %r10b
.L484:
	movq	%rdx, %r12
	jmp	.L439
.L486:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%rbx, %rdi
	movl	%fs:0(%rbp), %r12d
	call	_IO_file_close_it
	movl	%r12d, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L451:
	xorl	%ebp, %ebp
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L445:
	orl	$1, 116(%rbx)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L442:
	andl	$4096, %r8d
	movq	%rdx, %r12
	movl	$2, %r9d
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L440:
	orl	$2, 116(%rbx)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L436:
	movl	$8, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L485:
	movl	$4100, %r8d
	movl	$1, %r9d
	movl	$1088, %r10d
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L488:
	movq	120+_nl_C_locobj(%rip), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L461:
	movsbq	5(%r13,%rax), %rdx
	movl	(%rcx,%rdx,4), %edx
	movb	%dl, (%r12,%rax)
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L461
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L455:
	cmpb	$47, %dl
	jne	.L456
	cmpl	$3, %ecx
	je	.L457
	leal	2(%r9), %eax
	movb	$47, (%rsi)
	movl	%ecx, %r9d
	addq	$1, %rsi
	movl	%eax, %ecx
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L466:
	movq	%rax, %rsi
	jmp	.L457
.L489:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	_IO_file_close_it
	movq	%r12, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L432
.L487:
	leaq	1(%r12), %rax
	movb	$47, (%r12)
	jmp	.L453
.L490:
	leaq	__PRETTY_FUNCTION__.12619(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$322, %edx
	call	__assert_fail
.L491:
	leaq	__PRETTY_FUNCTION__.12619(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$323, %edx
	call	__assert_fail
.LFE90:
	.size	_IO_new_file_fopen, .-_IO_new_file_fopen
	.weak	_IO_file_fopen
	.hidden	_IO_file_fopen
	.set	_IO_file_fopen,_IO_new_file_fopen
	.p2align 4,,15
	.globl	_IO_new_file_finish
	.type	_IO_new_file_finish, @function
_IO_new_file_finish:
.LFB88:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-1, 112(%rdi)
	je	.L494
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L500
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
.L496:
	testb	$64, (%rbx)
	jne	.L494
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L501
.L498:
	movq	%rbx, %rdi
	call	*136(%rbp)
.L494:
	addq	$8, %rsp
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	_IO_default_finish
	.p2align 4,,10
	.p2align 3
.L500:
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	subq	%rsi, %rdx
	call	_IO_do_write
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L501:
	call	_IO_vtable_check
	jmp	.L498
.LFE88:
	.size	_IO_new_file_finish, .-_IO_new_file_finish
	.weak	_IO_file_finish
	.hidden	_IO_file_finish
	.set	_IO_file_finish,_IO_new_file_finish
	.p2align 4,,15
	.globl	_IO_new_file_overflow
	.type	_IO_new_file_overflow, @function
_IO_new_file_overflow:
.LFB101:
	movl	(%rdi), %ecx
	testb	$8, %cl
	jne	.L544
	testb	$8, %ch
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rax
	je	.L505
	testq	%rax, %rax
	je	.L506
	movq	40(%rdi), %rdx
.L507:
	cmpl	$-1, %ebp
	je	.L545
.L514:
	cmpq	%rdx, 64(%rbx)
	je	.L546
.L515:
	leaq	1(%rdx), %rax
	movq	%rax, 40(%rbx)
	movb	%bpl, (%rdx)
	movl	(%rbx), %eax
	testb	$2, %al
	jne	.L519
	testb	$2, %ah
	je	.L522
	cmpl	$10, %ebp
	je	.L519
.L522:
	movzbl	%bpl, %eax
.L502:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	testq	%rax, %rax
	je	.L506
	movq	8(%rdi), %rdx
.L508:
	testb	$1, %ch
	jne	.L547
.L509:
	movq	64(%rbx), %rax
	cmpq	%rdx, %rax
	je	.L512
	movq	16(%rbx), %rsi
.L513:
	movq	%rsi, 8(%rbx)
	movq	%rsi, 24(%rbx)
	movl	192(%rbx), %esi
	movq	%rax, 48(%rbx)
	movl	%ecx, %eax
	orb	$8, %ah
	movq	%rdx, 40(%rbx)
	movq	%rdx, 32(%rbx)
	movl	%eax, (%rbx)
	testl	%esi, %esi
	jg	.L507
	andl	$514, %ecx
	je	.L507
	cmpl	$-1, %ebp
	movq	%rdx, 48(%rbx)
	jne	.L514
.L545:
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	subq	%rsi, %rdx
	jmp	_IO_do_write
	.p2align 4,,10
	.p2align 3
.L519:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	_IO_do_write
	cmpl	$-1, %eax
	jne	.L522
.L521:
	movl	$-1, %eax
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L546:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L548
	movq	160(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	cmpl	$-1, %eax
	sete	%al
.L517:
	testb	%al, %al
	jne	.L521
	movq	40(%rbx), %rdx
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L548:
	movq	32(%rbx), %rsi
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	_IO_do_write
	cmpl	$-1, %eax
	sete	%al
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L512:
	movq	56(%rbx), %rdx
	movq	%rdx, 16(%rbx)
	movq	%rdx, %rsi
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L506:
	movq	%rbx, %rdi
	call	_IO_doallocbuf
	movq	56(%rbx), %rdx
	movl	(%rbx), %ecx
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L547:
	movq	16(%rbx), %r12
	movq	%rbx, %rdi
	subq	%rdx, %r12
	call	_IO_free_backup_area
	movq	24(%rbx), %rdx
	movq	%r12, %rsi
	negq	%rsi
	movq	%rdx, %rcx
	subq	56(%rbx), %rcx
	movq	%rcx, %rax
	negq	%rax
	cmpq	%r12, %rcx
	movl	(%rbx), %ecx
	cmova	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, 24(%rbx)
	movq	%rdx, 8(%rbx)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L544:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, %ecx
	movl	%ecx, (%rdi)
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
.LFE101:
	.size	_IO_new_file_overflow, .-_IO_new_file_overflow
	.weak	_IO_file_overflow
	.hidden	_IO_file_overflow
	.set	_IO_file_overflow,_IO_new_file_overflow
	.p2align 4,,15
	.globl	_IO_new_file_sync
	.type	_IO_new_file_sync, @function
_IO_new_file_sync:
.LFB102:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rsi, %rdx
	jbe	.L555
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L565
	movq	160(%rdi), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	testl	%eax, %eax
	setne	%al
.L554:
	testb	%al, %al
	jne	.L560
.L555:
	movq	8(%rbx), %rsi
	subq	16(%rbx), %rsi
	je	.L557
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L566
.L558:
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L559
	movq	8(%rbx), %rax
	movq	%rax, 16(%rbx)
.L557:
	movq	$-1, 144(%rbx)
	xorl	%eax, %eax
.L549:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	subq	%rsi, %rdx
	call	_IO_do_write
	testl	%eax, %eax
	setne	%al
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L559:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L557
.L560:
	movl	$-1, %eax
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%rsi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rsi
	jmp	.L558
.LFE102:
	.size	_IO_new_file_sync, .-_IO_new_file_sync
	.weak	_IO_file_sync
	.hidden	_IO_file_sync
	.set	_IO_file_sync,_IO_new_file_sync
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12619, @object
	.size	__PRETTY_FUNCTION__.12619, 19
__PRETTY_FUNCTION__.12619:
	.string	"_IO_new_file_fopen"
	.hidden	_IO_file_jumps_maybe_mmap
	.globl	_IO_file_jumps_maybe_mmap
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_file_jumps_maybe_mmap, @object
	.size	_IO_file_jumps_maybe_mmap, 168
_IO_file_jumps_maybe_mmap:
	.quad	0
	.quad	0
	.quad	_IO_file_finish
	.quad	_IO_file_overflow
	.quad	_IO_file_underflow_maybe_mmap
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	_IO_file_xsgetn_maybe_mmap
	.quad	_IO_file_seekoff_maybe_mmap
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_new_file_sync
	.quad	_IO_file_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_file_jumps_mmap
	.globl	_IO_file_jumps_mmap
	.align 32
	.type	_IO_file_jumps_mmap, @object
	.size	_IO_file_jumps_mmap, 168
_IO_file_jumps_mmap:
	.quad	0
	.quad	0
	.quad	_IO_file_finish
	.quad	_IO_file_overflow
	.quad	_IO_file_underflow_mmap
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	_IO_file_xsgetn_mmap
	.quad	_IO_file_seekoff_mmap
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_file_sync_mmap
	.quad	_IO_file_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close_mmap
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_file_jumps
	.globl	_IO_file_jumps
	.align 32
	.type	_IO_file_jumps, @object
	.size	_IO_file_jumps, 168
_IO_file_jumps:
	.quad	0
	.quad	0
	.quad	_IO_file_finish
	.quad	_IO_file_overflow
	.quad	_IO_file_underflow
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_file_xsputn
	.quad	_IO_file_xsgetn
	.quad	_IO_new_file_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_new_file_setbuf
	.quad	_IO_new_file_sync
	.quad	_IO_file_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	_IO_file_doallocate
	.hidden	_IO_default_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_default_finish
	.hidden	__assert_fail
	.hidden	__wcsmbs_named_conv
	.hidden	_nl_C_locobj
	.hidden	strstr
	.hidden	_IO_wdo_write
	.hidden	_IO_un_link
	.hidden	_IO_wsetb
	.hidden	_IO_free_wbackup_area
	.hidden	__open_nocancel
	.hidden	__open
	.hidden	_IO_link_in
	.hidden	_IO_default_xsputn
	.hidden	__read_nocancel
	.hidden	__read
	.hidden	__underflow
	.hidden	__write_nocancel
	.hidden	__write
	.hidden	__fstat64
	.hidden	_IO_free_backup_area
	.hidden	_IO_unsave_markers
	.hidden	_IO_setb
	.hidden	__mmap64
	.hidden	__mremap
	.hidden	__lseek64
	.hidden	__getpagesize
	.hidden	__munmap
	.hidden	__lll_lock_wait_private
	.hidden	_IO_doallocbuf
	.hidden	_IO_switch_to_get_mode
	.hidden	_IO_adjust_column
	.hidden	_IO_wfile_jumps_mmap
	.hidden	_IO_wfile_jumps
	.hidden	__close_nocancel
	.hidden	_IO_vtable_check
