	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__freading
	.type	__freading, @function
__freading:
	movl	(%rdi), %edx
	movl	%edx, %eax
	andl	$8, %eax
	jne	.L3
	andl	$2052, %edx
	jne	.L1
	xorl	%eax, %eax
	cmpq	$0, 24(%rdi)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %eax
.L1:
	rep ret
	.size	__freading, .-__freading
