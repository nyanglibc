	.text
#APP
	.section .gnu.warning.gets
	.previous
#NO_APP
	.p2align 4,,15
	.globl	_IO_gets
	.type	_IO_gets, @function
_IO_gets:
.LFB68:
	pushq	%r12
	pushq	%rbp
	movq	stdin(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	0(%rbp), %edx
	andl	$32768, %edx
	jne	.L21
	movq	136(%rbp), %rcx
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rcx)
	je	.L22
#APP
# 37 "iogets.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbp), %rcx
	movq	stdin(%rip), %rdi
	movq	%r12, 8(%rcx)
.L3:
	addl	$1, 4(%rcx)
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jb	.L8
.L28:
.LEHB0:
	call	__uflow
	cmpl	$-1, %eax
	jne	.L10
.L13:
	xorl	%ebx, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rbp, %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L28
.L8:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L10:
	cmpl	$10, %eax
	movq	%rbx, %rdx
	jne	.L29
.L12:
	movb	$0, (%rdx)
.L11:
	testl	$32768, 0(%rbp)
	jne	.L1
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	stdin(%rip), %rcx
	leaq	1(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	(%rcx), %edx
	movl	%edx, %r12d
	andl	$-33, %edx
	movl	%edx, (%rcx)
	movb	%al, (%rbx)
	movl	$10, %ecx
	movq	stdin(%rip), %rdi
	movl	$2147483647, %edx
	andl	$32, %r12d
	call	_IO_getline
	movq	stdin(%rip), %rcx
	addq	$1, %rax
	movl	(%rcx), %edx
	testb	$32, %dl
	jne	.L13
	orl	%edx, %r12d
	leaq	(%rbx,%rax), %rdx
	movl	%r12d, (%rcx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rbp, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L5
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L24:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L19
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L19
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L20
	subl	$1, (%rdi)
.L19:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L20:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L24-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_gets, .-_IO_gets
	.weak	gets
	.set	gets,_IO_gets
	.section	.gnu.warning.gets
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_gets, @object
	.size	__evoke_link_warning_gets, 57
__evoke_link_warning_gets:
	.string	"the `gets' function is dangerous and should not be used."
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_getline
	.hidden	__uflow
