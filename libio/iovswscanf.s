	.text
	.p2align 4,,15
	.globl	__vswscanf
	.type	__vswscanf, @function
__vswscanf:
	pushq	%r13
	pushq	%r12
	leaq	_IO_wstr_jumps(%rip), %r8
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movl	$32768, %esi
	subq	$488, %rsp
	xorl	%edx, %edx
	leaq	240(%rsp), %rbx
	movq	%rsp, %rcx
	movq	$0, 376(%rsp)
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_IO_fwide@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_IO_wstr_init_static@PLT
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	call	__vfwscanf_internal
	addq	$488, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__vswscanf, .-__vswscanf
	.weak	vswscanf
	.set	vswscanf,__vswscanf
	.hidden	__vfwscanf_internal
	.hidden	_IO_wstr_jumps
