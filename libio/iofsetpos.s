	.text
	.p2align 4,,15
	.globl	_IO_new_fsetpos
	.type	_IO_new_fsetpos, @function
_IO_new_fsetpos:
.LFB68:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L3
#APP
# 48 "iofsetpos.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	movq	0(%rbp), %rsi
	movl	$3, %edx
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekpos_unlocked
	cmpq	$-1, %rax
	je	.L24
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jg	.L10
.L22:
	xorl	%r8d, %r8d
.L9:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	leal	-1(%rax), %edx
	testl	%edx, %edx
	movl	%edx, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L15
	subl	$1, (%rdi)
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	152(%rbx), %rdi
	call	__libio_codecvt_encoding
	testl	%eax, %eax
	jns	.L22
	movq	160(%rbx), %rax
	movq	8(%rbp), %rdx
	movq	%rdx, 88(%rax)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%eax, %r8d
	movl	%fs:(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L9
	movl	$5, %fs:(%rdx)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L15:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L20:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L17
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L17
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L18
	subl	$1, (%rdi)
.L17:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L18:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L17
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L17
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L20-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_new_fsetpos, .-_IO_new_fsetpos
	.globl	_IO_new_fsetpos64
	.set	_IO_new_fsetpos64,_IO_new_fsetpos
	.weak	_IO_fsetpos64
	.set	_IO_fsetpos64,_IO_new_fsetpos64
	.globl	__new_fsetpos64
	.set	__new_fsetpos64,_IO_new_fsetpos64
	.weak	fsetpos64
	.set	fsetpos64,__new_fsetpos64
	.weak	_IO_fsetpos
	.set	_IO_fsetpos,_IO_new_fsetpos
	.globl	__new_fsetpos
	.set	__new_fsetpos,_IO_new_fsetpos
	.weak	fsetpos
	.set	fsetpos,__new_fsetpos
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__libio_codecvt_encoding
	.hidden	_IO_seekpos_unlocked
