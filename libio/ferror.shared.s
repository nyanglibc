	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_ferror
	.type	_IO_ferror, @function
_IO_ferror:
	testb	$-128, 116(%rdi)
	movl	(%rdi), %eax
	je	.L4
	movl	%eax, %edx
	andl	$32768, %edx
	jne	.L4
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	136(%rdi), %rcx
	movq	%fs:16, %rbp
	cmpq	8(%rcx), %rbp
	je	.L5
	movq	%rdi, %rbx
#APP
# 37 "ferror.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L7:
	movl	(%rbx), %esi
	movq	136(%rbx), %rcx
	movl	%esi, %r8d
	movq	%rbp, 8(%rcx)
	movl	4(%rcx), %edx
	shrl	$5, %r8d
	andl	$1, %r8d
	andl	$32768, %esi
	jne	.L18
.L8:
	testl	%edx, %edx
	jne	.L1
	movq	$0, 8(%rcx)
#APP
# 39 "ferror.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	subl	$1, (%rcx)
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	shrl	$5, %eax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	shrl	$5, %eax
	movl	4(%rcx), %edx
	andl	$1, %eax
	movl	%eax, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L7
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
#APP
# 39 "ferror.c" 1
	xchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	$202, %eax
#APP
# 39 "ferror.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L18:
	addl	$1, %edx
	movl	%edx, 4(%rcx)
	jmp	.L1
	.size	_IO_ferror, .-_IO_ferror
	.weak	ferror
	.set	ferror,_IO_ferror
	.hidden	__lll_lock_wait_private
