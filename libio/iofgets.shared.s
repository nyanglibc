	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_fgets
	.type	_IO_fgets, @function
_IO_fgets:
.LFB68:
	testl	%esi, %esi
	jle	.L16
	cmpl	$1, %esi
	je	.L28
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	movl	%esi, %ebp
	subq	$8, %rsp
	movl	(%rdx), %edx
	movl	%edx, %ecx
	andl	$32768, %ecx
	jne	.L4
	movq	136(%rbx), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L5
#APP
# 47 "iofgets.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L6
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L7:
	movq	136(%rbx), %rdi
	movl	(%rbx), %edx
	movq	%r13, 8(%rdi)
.L5:
	addl	$1, 4(%rdi)
.L4:
	movl	%edx, %r13d
	andl	$-33, %edx
	movl	$1, %r8d
	movl	%edx, (%rbx)
	leal	-1(%rbp), %edx
	movl	$10, %ecx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	andl	$32, %r13d
	movslq	%edx, %rdx
.LEHB0:
	call	__GI__IO_getline
	xorl	%r8d, %r8d
	testq	%rax, %rax
	movl	(%rbx), %edx
	je	.L10
	testb	$32, %dl
	je	.L11
	movq	__libc_errno@gottpoff(%rip), %rcx
	cmpl	$11, %fs:(%rcx)
	je	.L11
.L10:
	orl	%edx, %r13d
	movl	%r13d, (%rbx)
	andl	$32768, %r13d
	jne	.L1
.L29:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L12
	subl	$1, (%rdi)
.L1:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movb	$0, (%r12,%rax)
	movl	(%rbx), %edx
	movq	%r12, %r8
	orl	%edx, %r13d
	movl	%r13d, (%rbx)
	andl	$32768, %r13d
	je	.L29
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movb	$0, (%rdi)
	movq	%rdi, %r8
.L25:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r8d, %r8d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L7
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L19:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L14
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L14
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L15
	subl	$1, (%rdi)
.L14:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L15:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L14
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L14
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L19-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_fgets, .-_IO_fgets
	.weak	fgets
	.set	fgets,_IO_fgets
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
