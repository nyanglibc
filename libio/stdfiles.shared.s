	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	__GI__IO_list_all
	.globl	__GI__IO_list_all
	.section	.data.rel,"aw",@progbits
	.align 8
	.type	__GI__IO_list_all, @object
	.size	__GI__IO_list_all, 8
__GI__IO_list_all:
	.quad	_IO_2_1_stderr_
	.globl	_IO_list_all
	.set	_IO_list_all,__GI__IO_list_all
	.globl	_IO_2_1_stderr_
	.align 32
	.type	_IO_2_1_stderr_, @object
	.size	_IO_2_1_stderr_, 224
_IO_2_1_stderr_:
	.long	-72540026
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_2_1_stdout_
	.long	2
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_2_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_2
	.quad	0
	.zero	40
	.quad	__GI__IO_file_jumps
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	_IO_wide_data_2, @object
	.size	_IO_wide_data_2, 232
_IO_wide_data_2:
	.zero	224
	.quad	__GI__IO_wfile_jumps
	.local	_IO_stdfile_2_lock
	.comm	_IO_stdfile_2_lock,16,16
	.globl	_IO_2_1_stdout_
	.section	.data.rel
	.align 32
	.type	_IO_2_1_stdout_, @object
	.size	_IO_2_1_stdout_, 224
_IO_2_1_stdout_:
	.long	-72540028
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_2_1_stdin_
	.long	1
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_1_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_1
	.quad	0
	.zero	40
	.quad	__GI__IO_file_jumps
	.section	.data.rel.local
	.align 32
	.type	_IO_wide_data_1, @object
	.size	_IO_wide_data_1, 232
_IO_wide_data_1:
	.zero	224
	.quad	__GI__IO_wfile_jumps
	.local	_IO_stdfile_1_lock
	.comm	_IO_stdfile_1_lock,16,16
	.globl	_IO_2_1_stdin_
	.align 32
	.type	_IO_2_1_stdin_, @object
	.size	_IO_2_1_stdin_, 224
_IO_2_1_stdin_:
	.long	-72540024
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_0_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_0
	.quad	0
	.zero	40
	.quad	__GI__IO_file_jumps
	.align 32
	.type	_IO_wide_data_0, @object
	.size	_IO_wide_data_0, 232
_IO_wide_data_0:
	.zero	224
	.quad	__GI__IO_wfile_jumps
	.local	_IO_stdfile_0_lock
	.comm	_IO_stdfile_0_lock,16,16
