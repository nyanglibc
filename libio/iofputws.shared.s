	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	fputws
	.type	fputws, @function
fputws:
.LFB71:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
.LEHB0:
	call	__wcslen@PLT
.LEHE0:
	movl	(%rbx), %edx
	movq	%rax, %r12
	andl	$32768, %edx
	jne	.L2
	movq	136(%rbx), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L3
#APP
# 36 "iofputws.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%r13, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	movl	%eax, %r13d
	jne	.L13
	movq	216(%rbx), %r14
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L10
.L12:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
.LEHB1:
	call	*56(%r14)
	cmpq	%rax, %r12
	jne	.L13
.L9:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L1:
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	call	_IO_vtable_check
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$-1, %r13d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE1:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L21:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L19
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L19
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L20
	subl	$1, (%rdi)
.L19:
	movq	%r8, %rdi
.LEHB2:
	call	_Unwind_Resume@PLT
.LEHE2:
.L20:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
.LFE71:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA71:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE71-.LLSDACSB71
.LLSDACSB71:
	.uleb128 .LEHB0-.LFB71
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB71
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L21-.LFB71
	.uleb128 0
	.uleb128 .LEHB2-.LFB71
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE71:
	.text
	.size	fputws, .-fputws
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
