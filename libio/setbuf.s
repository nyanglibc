	.text
	.p2align 4,,15
	.globl	setbuf
	.type	setbuf, @function
setbuf:
	movl	$8192, %edx
	jmp	_IO_setbuffer
	.size	setbuf, .-setbuf
	.hidden	_IO_setbuffer
