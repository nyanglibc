	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ferror_unlocked
	.hidden	__ferror_unlocked
	.type	__ferror_unlocked, @function
__ferror_unlocked:
	movl	(%rdi), %eax
	shrl	$5, %eax
	andl	$1, %eax
	ret
	.size	__ferror_unlocked, .-__ferror_unlocked
	.weak	__GI_ferror_unlocked
	.hidden	__GI_ferror_unlocked
	.set	__GI_ferror_unlocked,__ferror_unlocked
	.weak	ferror_unlocked
	.set	ferror_unlocked,__GI_ferror_unlocked
