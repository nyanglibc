	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC0:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"nan"
	.section	.rodata.str4.4
	.align 4
.LC2:
	.string	"N"
	.string	""
	.string	""
	.string	"A"
	.string	""
	.string	""
	.string	"N"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC3:
	.string	"NAN"
	.section	.rodata.str4.4
	.align 4
.LC4:
	.string	"I"
	.string	""
	.string	""
	.string	"N"
	.string	""
	.string	""
	.string	"F"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC5:
	.string	"INF"
	.section	.rodata.str4.4
	.align 4
.LC6:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC7:
	.string	"inf"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../stdio-common/printf_fphex.c"
	.align 8
.LC9:
	.string	"*decimal != '\\0' && decimalwc != L'\\0'"
	.globl	__unordtf2
	.globl	__letf2
#NO_APP
	.text
	.p2align 4,,15
	.globl	__printf_fphex
	.hidden	__printf_fphex
	.type	__printf_fphex, @function
__printf_fphex:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r8
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbp
	pushq	%rbx
	subq	$344, %rsp
	movl	(%rsi), %eax
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	movl	4(%rsi), %ebp
	movl	%eax, 16(%rsp)
	movzbl	13(%rsi), %eax
	movq	%fs:(%rcx), %rcx
	movl	%eax, %r12d
	shrb	$2, %r12b
	andl	$1, %r12d
	testb	$1, %al
	jne	.L2
	movq	8(%rcx), %rcx
	movq	64(%rcx), %rdi
	movq	%rdi, 32(%rsp)
	movl	88(%rcx), %edi
	movl	%edi, 24(%rsp)
.L3:
	movq	32(%rsp), %rdi
	cmpb	$0, (%rdi)
	je	.L4
	movl	24(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L4
	testb	$16, %al
	movq	(%rdx), %rcx
	movslq	8(%r15), %r13
	je	.L6
	movdqa	(%rcx), %xmm3
	movq	%r8, 40(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movq	40(%rsp), %r8
	jne	.L468
	movdqa	(%rsp), %xmm2
	movq	%r8, 40(%rsp)
	pand	.LC10(%rip), %xmm2
	movaps	(%rsp), %xmm4
	movdqa	.LC11(%rip), %xmm1
	movmskps	%xmm4, %ebx
	movdqa	%xmm2, %xmm0
	andl	$8, %ebx
	movaps	%xmm2, 48(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movq	40(%rsp), %r8
	jne	.L275
	movdqa	48(%rsp), %xmm2
	movdqa	.LC11(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movq	40(%rsp), %r8
	jg	.L429
.L275:
	movq	(%rsp), %r14
	movq	8(%rsp), %rax
	leaq	176(%rsp), %r10
	xorl	%ecx, %ecx
	leaq	208(%rsp), %rsi
	cmpl	$65, %r13d
	sete	%cl
	movl	$16, %edx
	movq	%r8, 48(%rsp)
	movq	%r14, %rdi
	movq	%r10, 40(%rsp)
	movq	%rax, 72(%rsp)
	call	_itoa_word
	movl	8(%r15), %edi
	leaq	_itowa_lower_digits(%rip), %rdx
	leaq	_itowa_upper_digits(%rip), %rcx
	movq	48(%rsp), %r8
	movq	40(%rsp), %r10
	leaq	336(%rsp), %r13
	cmpl	$65, %edi
	cmovne	%rdx, %rcx
	movq	%r14, %rdx
.L65:
	movq	%rdx, %rsi
	shrq	$4, %rdx
	subq	$4, %r13
	andl	$15, %esi
	testq	%rdx, %rdx
	movl	(%rcx,%rsi,4), %esi
	movl	%esi, 0(%r13)
	jne	.L65
	leaq	16(%r10), %rsi
	cmpq	%rsi, %rax
	jbe	.L66
	movq	%rax, %rdx
	movq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L67:
	subq	$1, %rdx
	subq	$4, %rcx
	movb	$48, (%rdx)
	cmpq	%rsi, %rdx
	movl	$48, (%rcx)
	jne	.L67
	leaq	-1(%rax), %rcx
	movl	8(%r15), %edi
	subq	%rcx, %rax
	subq	%rdx, %rcx
	notq	%rcx
	leaq	15(%rax,%r10), %rax
	leaq	0(%r13,%rcx,4), %r13
.L66:
	movdqa	(%rsp), %xmm5
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	movq	8(%rsp), %rdx
	movq	%r10, 64(%rsp)
	pextrw	$6, %xmm5, %r11d
	movq	%r8, 48(%rsp)
	movl	%edx, %edx
	salq	$32, %r11
	orq	%rdx, %r11
	cmpl	$65, %edi
	movl	$16, %edx
	sete	%cl
	movq	%r11, %rdi
	movq	%r11, 40(%rsp)
	call	_itoa_word
	cmpl	$65, 8(%r15)
	movq	40(%rsp), %r11
	movq	%rax, %r9
	leaq	_itowa_lower_digits(%rip), %rcx
	leaq	_itowa_upper_digits(%rip), %rax
	movq	48(%rsp), %r8
	movq	64(%rsp), %r10
	cmove	%rax, %rcx
	movq	%r11, %rax
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rax, %rdx
	shrq	$4, %rax
	subq	$4, %r13
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, 0(%r13)
	jne	.L69
	leaq	4(%r10), %rcx
	cmpq	%rcx, %r9
	jbe	.L70
	movq	%r9, %rax
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L71:
	subq	$1, %rax
	subq	$4, %rdx
	movb	$48, (%rax)
	cmpq	%rcx, %rax
	movl	$48, (%rdx)
	jne	.L71
	subq	%r9, %rax
	leaq	4(%r10), %r9
	leaq	0(%r13,%rax,4), %r13
.L70:
	movdqa	(%rsp), %xmm6
	pextrw	$7, %xmm6, %eax
	testw	$32767, %ax
	setne	%al
	orq	%r14, %r11
	addl	$48, %eax
	movb	%al, 64(%rsp)
	movq	72(%rsp), %rax
	shrq	$48, %rax
	andl	$32767, %eax
	movl	%eax, (%rsp)
	je	.L469
	movl	(%rsp), %eax
	cmpl	$16382, %eax
	jle	.L75
	subl	$16383, %eax
	movl	$0, (%rsp)
.L74:
	xorl	%r14d, %r14d
	testq	%r11, %r11
	sete	%r14b
.L76:
	testl	%r14d, %r14d
	jne	.L82
.L83:
	cmpl	$48, 332(%rsp)
	jne	.L260
	leaq	336(%rsp), %rdx
	leaq	208(%rsp), %rcx
	.p2align 4,,10
	.p2align 3
.L93:
	subq	$4, %rdx
	subq	$1, %rcx
	cmpl	$48, -4(%rdx)
	je	.L93
	movq	%rcx, 88(%rsp)
.L92:
	movq	88(%rsp), %r10
	movl	16(%rsp), %edi
	subq	%r9, %r10
	cmpl	$-1, %edi
	je	.L94
	movslq	%edi, %rcx
	cmpq	%r10, %rcx
	jge	.L95
	testl	%edi, %edi
	movzbl	64(%rsp), %edx
	jle	.L96
	movzbl	-1(%r9,%rcx), %edx
.L96:
	leal	-65(%rdx), %edi
	movzbl	(%r9,%rcx), %ecx
	movsbl	%dl, %esi
	cmpb	$5, %dil
	ja	.L97
	leal	-55(%rsi), %edi
.L98:
	leal	-65(%rcx), %esi
	movsbl	%cl, %edx
	cmpb	$5, %sil
	ja	.L100
	subl	$55, %edx
.L101:
	testb	$7, %dl
	movl	$1, %esi
	jne	.L103
	movl	16(%rsp), %esi
	leal	1(%rsi), %ecx
	xorl	%esi, %esi
	movslq	%ecx, %rcx
	cmpq	%r10, %rcx
	setl	%sil
.L103:
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 144(%rsp)
# 0 "" 2
#NO_APP
	movzwl	144(%rsp), %ecx
	andw	$3072, %cx
	cmpw	$1024, %cx
	je	.L105
	jbe	.L470
	cmpw	$2048, %cx
	je	.L108
	cmpw	$3072, %cx
	jne	.L104
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	123(%rsp), %r10
	movslq	%eax, %r14
	leaq	128(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	$10, %edx
	movq	%r8, 80(%rsp)
	movq	%r9, 72(%rsp)
	movq	%r10, 48(%rsp)
	call	_itoa_word
	movq	72(%rsp), %r9
	movq	80(%rsp), %r8
	leaq	164(%rsp), %rcx
	movq	48(%rsp), %r10
	leaq	_itowa_lower_digits(%rip), %rdi
	movq	%rax, 40(%rsp)
	movabsq	$-3689348814741910323, %rsi
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%r14, %rax
	subq	$4, %rcx
	mulq	%rsi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r14
	testq	%rdx, %rdx
	movl	(%rdi,%r14,4), %eax
	movq	%rdx, %r14
	movl	%eax, (%rcx)
	jne	.L119
	testl	%ebx, %ebx
	movzbl	12(%r15), %r14d
	movl	$4, %eax
	jne	.L120
	xorl	%eax, %eax
	testb	$80, %r14b
	setne	%al
	addl	$3, %eax
.L120:
	leaq	5(%r10), %rdx
	subl	$2, %ebp
	movq	%rdx, %rdi
	subq	40(%rsp), %rdi
	subl	%edi, %ebp
	movq	%rdi, 72(%rsp)
	movl	16(%rsp), %edi
	addl	%edi, %eax
	subl	%eax, %ebp
	testl	%edi, %edi
	jle	.L471
.L121:
	testl	%r12d, %r12d
	movl	$1, %eax
	jne	.L123
	movq	32(%rsp), %rdi
	movq	%r8, 96(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%r9, 48(%rsp)
	call	__GI_strlen
	movq	96(%rsp), %r8
	movq	80(%rsp), %rcx
	movq	48(%rsp), %r9
.L123:
	subl	%eax, %ebp
.L451:
	andl	$32, %r14d
	movl	%ebp, 48(%rsp)
	jne	.L268
	cmpl	$48, 16(%r15)
	je	.L268
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jle	.L268
	movslq	%eax, %rbp
	testl	%r12d, %r12d
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%rbp, %rdx
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%r8, 80(%rsp)
	je	.L125
	call	_IO_wpadn@PLT
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
.L126:
	cmpq	%rbp, %rax
	jne	.L39
	testl	%ebx, %ebx
	movl	48(%rsp), %ebp
	je	.L127
.L488:
	testl	%r12d, %r12d
	je	.L128
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L129
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L129
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rdx)
.L146:
	addl	$1, %ebp
.L134:
	testl	%r12d, %r12d
	je	.L148
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L149
	movq	32(%rax), %rsi
	movq	40(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L149
	leaq	4(%rsi), %rdx
	movq	%rdx, 32(%rax)
	movl	$48, (%rsi)
	movl	8(%r15), %ebx
	leal	23(%rbx), %esi
.L152:
	cmpq	%rdi, %rdx
	jnb	.L234
	leaq	4(%rdx), %rdi
	cmpl	$-1, %esi
	movq	%rdi, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L157:
	testb	%al, %al
	jne	.L39
	testb	$32, 12(%r15)
	leal	2(%rbp), %ebx
	jne	.L161
	cmpl	$48, 16(%r15)
	jne	.L161
	movl	48(%rsp), %r10d
	testl	%r10d, %r10d
	jg	.L472
	.p2align 4,,10
	.p2align 3
.L161:
	testl	%r12d, %r12d
	je	.L160
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L164
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L164
	movsbl	64(%rsp), %esi
	leaq	4(%rdx), %rdi
	movq	%rdi, 32(%rax)
	cmpl	$-1, %esi
	movl	%esi, (%rdx)
	sete	%al
.L166:
	testb	%al, %al
	jne	.L39
	movl	16(%rsp), %edi
	leal	1(%rbx), %ebp
	testl	%edi, %edi
	jle	.L473
.L169:
	testl	%r12d, %r12d
	jne	.L171
	movq	32(%rsp), %rdi
	movq	%r8, 80(%rsp)
	xorl	%r13d, %r13d
	movq	%rcx, 64(%rsp)
	movq	%r9, 24(%rsp)
	addl	$2, %ebx
	call	__GI_strlen
	testq	%rax, %rax
	movq	24(%rsp), %r9
	movq	64(%rsp), %rcx
	movq	80(%rsp), %r8
	je	.L173
	movl	%r12d, 64(%rsp)
	movl	%ebx, %r14d
	movq	%r9, 24(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%r8, %rbp
	movq	%rax, %r12
	movq	32(%rsp), %rbx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L179:
	leaq	1(%rax), %rdi
	movq	%rdi, 40(%rbp)
	movb	%sil, (%rax)
.L180:
	leal	(%r14,%r13), %eax
	addq	$1, %r13
	cmpq	%r12, %r13
	je	.L474
.L172:
	movq	40(%rbp), %rax
	cmpq	48(%rbp), %rax
	movzbl	(%rbx,%r13), %esi
	jb	.L179
	movq	%rbp, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L180
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$-1, %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rcx), %rcx
	movq	80(%rcx), %rdi
	movq	%rdi, 32(%rsp)
	movl	408(%rcx), %edi
	movl	%edi, 24(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	12(%r15), %edx
	testb	$1, %dl
	je	.L15
	fldt	(%rcx)
	fld	%st(0)
	fstpt	(%rsp)
	fxam
	fnstsw	%ax
	movl	%eax, %ebx
	andl	$512, %ebx
	fucomip	%st(0), %st
	jp	.L458
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L475
.L430:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	leaq	.LC6(%rip), %rsi
	leaq	.LC4(%rip), %rcx
	leaq	.LC7(%rip), %r14
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%r13,2), %eax
	andw	$256, %ax
	leaq	.LC5(%rip), %rax
	cmove	%rsi, %rcx
	movq	%rcx, %r9
	cmovne	%rax, %r14
.L14:
	movl	%edx, %eax
	andl	$32, %eax
	testl	%ebx, %ebx
	je	.L476
	subl	$4, %ebp
	testb	%al, %al
	jne	.L450
	testl	%ebp, %ebp
	jle	.L450
.L229:
	movslq	%ebp, %r13
	testl	%r12d, %r12d
	movq	%r9, 16(%rsp)
	movq	%r13, %rdx
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%r8, (%rsp)
	jne	.L477
	call	__GI__IO_padn
	movq	16(%rsp), %r9
	movq	(%rsp), %r8
.L29:
	cmpq	%r13, %rax
	jne	.L39
	testl	%ebx, %ebx
	je	.L252
	testl	%r12d, %r12d
	movl	%ebp, %ebx
	je	.L33
.L481:
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L34
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L34
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$45, (%rdx)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L476:
	testb	$80, %dl
	je	.L478
	subl	$4, %ebp
	testb	%al, %al
	je	.L479
.L220:
	testb	$64, %dl
	je	.L41
	testl	%r12d, %r12d
	je	.L42
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L43
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L43
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$43, (%rdx)
	.p2align 4,,10
	.p2align 3
.L52:
	addl	$1, %ebx
.L40:
	testl	%r12d, %r12d
	je	.L54
	xorl	%r12d, %r12d
	addl	$1, %ebx
	movq	%r9, %r13
	movq	%r8, %r14
.L58:
	movq	160(%r14), %rax
	movl	0(%r13,%r12,4), %esi
	testq	%rax, %rax
	je	.L55
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L55
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L57:
	testb	%al, %al
	jne	.L39
	leal	(%rbx,%r12), %eax
	addq	$1, %r12
	cmpq	$3, %r12
	jne	.L58
	testb	$32, 12(%r15)
	movl	%eax, %r13d
	movq	%r14, %r8
	je	.L1
	testl	%ebp, %ebp
	jle	.L1
	movslq	%ebp, %rbx
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%rbx, %rdx
	call	_IO_wpadn@PLT
.L64:
	cmpq	%rbx, %rax
	jne	.L39
	addl	%ebp, %r13d
.L1:
	addq	$344, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	(%rcx), %xmm0
	movmskpd	%xmm0, %ebx
	movsd	%xmm0, 128(%rsp)
	andl	$1, %ebx
	ucomisd	%xmm0, %xmm0
	jp	.L458
	movapd	%xmm0, %xmm1
	andpd	.LC12(%rip), %xmm1
	ucomisd	.LC13(%rip), %xmm1
	ja	.L430
	movl	132(%rsp), %r14d
	movq	%xmm0, %rax
	leaq	_itowa_upper_digits(%rip), %rsi
	leaq	336(%rsp), %r11
	movl	%eax, %eax
	andl	$1048575, %r14d
	salq	$32, %r14
	movq	%r14, %rdi
	orq	%rax, %rdi
	leaq	_itowa_lower_digits(%rip), %rax
	sete	%r14b
	xorl	%ecx, %ecx
	cmpl	$65, %r13d
	cmovne	%rax, %rsi
	movq	%rdi, 40(%rsp)
	movzbl	%r14b, %r14d
	sete	%cl
	movq	%rdi, %rax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%r13, %r11
.L77:
	movq	%rax, %rdx
	shrq	$4, %rax
	leaq	-4(%r11), %r13
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rsi,%rdx,4), %edx
	movl	%edx, -4(%r11)
	jne	.L256
	movq	40(%rsp), %rdi
	leaq	176(%rsp), %r10
	leaq	208(%rsp), %rsi
	movl	$16, %edx
	movsd	%xmm0, 64(%rsp)
	movq	%r8, 48(%rsp)
	movq	%r10, (%rsp)
	movq	%r11, 72(%rsp)
	call	_itoa_word
	movq	%rax, %r9
	leaq	284(%rsp), %rax
	movq	(%rsp), %r10
	movq	48(%rsp), %r8
	movsd	64(%rsp), %xmm0
	cmpq	%rax, %r13
	jbe	.L78
	movq	72(%rsp), %r11
	subq	%rax, %r11
	xorl	%eax, %eax
	leaq	-5(%r11), %rcx
	shrq	$2, %rcx
	movq	%rcx, %rdx
	notq	%rdx
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$48, -4(%r13,%rax,4)
	movb	$48, -1(%r9,%rax)
	subq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L79
	notq	%rcx
	addq	%rax, %r9
	leaq	0(%r13,%rcx,4), %r13
.L78:
	movq	%xmm0, %rax
	shrq	$48, %rax
	testl	$32752, %eax
	setne	%al
	addl	$48, %eax
	movb	%al, 64(%rsp)
	movzwl	134(%rsp), %eax
	shrw	$4, %ax
	andl	$2047, %eax
	movl	%eax, (%rsp)
	jne	.L81
	cmpq	$0, 40(%rsp)
	jne	.L480
	xorl	%eax, %eax
.L82:
	cmpl	$-1, 16(%rsp)
	movq	%r9, 88(%rsp)
	jne	.L95
	movl	$0, 16(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L429:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	leaq	.LC6(%rip), %rcx
	leaq	.LC4(%rip), %rdx
	leaq	.LC7(%rip), %r14
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%r13,2), %eax
	andw	$256, %ax
	leaq	.LC5(%rip), %rax
	cmove	%rcx, %rdx
	movq	%rdx, %r9
	cmovne	%rax, %r14
.L9:
	movzbl	12(%r15), %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L478:
	subl	$3, %ebp
	testb	%al, %al
	jne	.L220
	testl	%ebp, %ebp
	jg	.L229
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L475:
	fldt	(%rsp)
	movq	(%rsp), %r14
	leaq	176(%rsp), %r10
	xorl	%ecx, %ecx
	leaq	208(%rsp), %rsi
	cmpl	$65, %r13d
	movl	$16, %edx
	sete	%cl
	movq	%r8, 40(%rsp)
	movq	%r10, (%rsp)
	movq	%r14, %rdi
	leaq	336(%rsp), %r13
	fstpt	144(%rsp)
	call	_itoa_word
	leaq	_itowa_upper_digits(%rip), %rdx
	leaq	_itowa_lower_digits(%rip), %rsi
	cmpl	$65, 8(%r15)
	movq	40(%rsp), %r8
	movq	(%rsp), %r10
	cmove	%rdx, %rsi
	movq	%r14, %rdx
.L85:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$4, %r13
	andl	$15, %ecx
	testq	%rdx, %rdx
	movl	(%rsi,%rcx,4), %ecx
	movl	%ecx, 0(%r13)
	jne	.L85
	leaq	16(%r10), %rsi
	cmpq	%rsi, %rax
	jbe	.L86
	movq	%rax, %rdx
	movq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$1, %rdx
	subq	$4, %rcx
	movb	$48, (%rdx)
	cmpq	%rsi, %rdx
	movl	$48, (%rcx)
	jne	.L87
	subq	%rax, %rdx
	leaq	16(%r10), %rax
	movb	$48, 64(%rsp)
	leaq	0(%r13,%rdx,4), %r13
.L88:
	leaq	1(%rax), %r9
	movzwl	152(%rsp), %eax
	addq	$4, %r13
	andl	$32767, %eax
	movl	%eax, (%rsp)
	jne	.L89
	cmpq	$1, %r14
	sbbl	%eax, %eax
	xorl	%edi, %edi
	notl	%eax
	andl	$16385, %eax
	testq	%r14, %r14
	setne	%dil
	movl	%edi, (%rsp)
.L90:
	testq	%r14, %r14
	sete	%r14b
	movzbl	%r14b, %r14d
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L477:
	call	_IO_wpadn@PLT
	movq	(%rsp), %r8
	movq	16(%rsp), %r9
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L450:
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	jne	.L481
.L33:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L482
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$45, (%rax)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	3(%r14), %rcx
	movq	%r14, %r12
.L62:
	addq	$1, %r12
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	movzbl	-1(%r12), %esi
	jnb	.L483
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	%sil, (%rax)
.L61:
	movl	%r12d, %r13d
	subl	%r14d, %r13d
	addl	%ebx, %r13d
	cmpq	%r12, %rcx
	jne	.L62
	testb	$32, 12(%r15)
	je	.L1
	testl	%ebp, %ebp
	jle	.L1
	movslq	%ebp, %rbx
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%rbx, %rdx
	call	__GI__IO_padn
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L41:
	andl	$16, %edx
	je	.L40
	testl	%r12d, %r12d
	je	.L48
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L49
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L49
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r14, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r8, %rdi
	movq	%rcx, 16(%rsp)
	movq	%r8, (%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	movq	16(%rsp), %rcx
	jne	.L61
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L42:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L484
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$43, (%rax)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L81:
	movl	(%rsp), %eax
	cmpl	$1022, %eax
	jle	.L84
	subl	$1023, %eax
	movl	$0, (%rsp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L252:
	movzbl	12(%r15), %edx
	movl	%ebp, %eax
.L32:
	movl	%ebp, %ebx
	movl	%eax, %ebp
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L458:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rcx
	leaq	.LC1(%rip), %r14
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%r13,2), %eax
	andw	$256, %ax
	leaq	.LC3(%rip), %rax
	cmove	%rsi, %rcx
	movq	%rcx, %r9
	cmovne	%rax, %r14
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L468:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	leaq	.LC0(%rip), %rcx
	leaq	.LC2(%rip), %rdx
	movaps	(%rsp), %xmm7
	leaq	.LC1(%rip), %r14
	movq	%fs:(%rax), %rax
	movmskps	%xmm7, %ebx
	andl	$8, %ebx
	movzwl	(%rax,%r13,2), %eax
	andw	$256, %ax
	leaq	.LC3(%rip), %rax
	cmove	%rcx, %rdx
	movq	%rdx, %r9
	cmovne	%rax, %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L469:
	cmpq	$1, %r11
	sbbl	%eax, %eax
	xorl	%edi, %edi
	notl	%eax
	andl	$16382, %eax
	testq	%r11, %r11
	setne	%dil
	movl	%edi, (%rsp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L48:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L485
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$32, (%rax)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$1022, %eax
	movl	$1, (%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$1023, %eax
	subl	(%rsp), %eax
	movl	$1, (%rsp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L89:
	movl	(%rsp), %eax
	cmpl	$16385, %eax
	jle	.L91
	subl	$16386, %eax
	movl	$0, (%rsp)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L171:
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L174
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L174
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	24(%rsp), %eax
	cmpl	$-1, %eax
	movl	%eax, (%rdx)
	sete	%al
.L176:
	testb	%al, %al
	jne	.L39
	movl	16(%rsp), %esi
	leal	2(%rbx), %ebp
	testl	%esi, %esi
	jle	.L178
	movslq	16(%rsp), %r10
	movq	88(%rsp), %rax
	subq	%r9, %rax
	cmpq	%rax, %r10
	movq	%rax, %rdx
	movq	%rax, %r9
	cmovle	%r10, %rdx
	xorl	%r14d, %r14d
	addl	$3, %ebx
	testq	%rdx, %rdx
	je	.L185
	movl	%r12d, 32(%rsp)
	movq	%r13, %rbp
	movq	%r10, 16(%rsp)
	movl	%ebx, %r13d
	movq	%r9, 24(%rsp)
	movq	%rcx, 64(%rsp)
	movq	%r8, %r12
	movq	%rdx, %rbx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L487:
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L181
	leaq	4(%rcx), %rdi
	cmpl	$-1, %esi
	movq	%rdi, 32(%rax)
	movl	%esi, (%rcx)
	sete	%al
.L183:
	testb	%al, %al
	jne	.L39
	leal	0(%r13,%r14), %eax
	addq	$1, %r14
	cmpq	%rbx, %r14
	je	.L486
.L184:
	movq	160(%r12), %rax
	movl	0(%rbp,%r14,4), %esi
	testq	%rax, %rax
	jne	.L487
.L181:
	movq	%r12, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L268:
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	jne	.L488
.L127:
	movzbl	12(%r15), %eax
	testb	$64, %al
	je	.L135
	testl	%r12d, %r12d
	je	.L136
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L137
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L137
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L148:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L489
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$48, (%rax)
	movl	8(%r15), %eax
	addl	$23, %eax
.L155:
	movq	40(%r8), %rdx
	cmpq	48(%r8), %rdx
	jnb	.L490
	leaq	1(%rdx), %rsi
	leal	2(%rbp), %ebx
	movq	%rsi, 40(%r8)
	movb	%al, (%rdx)
	testb	$32, 12(%r15)
	jne	.L160
	cmpl	$48, 16(%r15)
	jne	.L160
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jg	.L491
	.p2align 4,,10
	.p2align 3
.L160:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L492
	movzbl	64(%rsp), %edi
	leaq	1(%rax), %rdx
	leal	1(%rbx), %ebp
	movq	%rdx, 40(%r8)
	movb	%dil, (%rax)
	movl	16(%rsp), %edi
	testl	%edi, %edi
	jg	.L169
.L473:
	testb	$8, 12(%r15)
	jne	.L169
.L170:
	movl	8(%r15), %eax
	testl	%r12d, %r12d
	leal	15(%rax), %esi
	je	.L191
.L242:
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L192
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L192
	leaq	4(%rdx), %rdi
	cmpl	$-1, %esi
	movq	%rdi, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L194:
	testb	%al, %al
	jne	.L39
	cmpl	$1, (%rsp)
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$45, %esi
	testl	%r12d, %r12d
	je	.L197
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L199
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L199
	leaq	4(%rdx), %rdi
	addl	$2, %ebp
	movq	%rdi, 32(%rax)
	movl	%esi, (%rdx)
.L202:
	xorl	%ebx, %ebx
	cmpq	$0, 72(%rsp)
	leal	1(%rbp), %r14d
	je	.L457
	movl	%r12d, (%rsp)
	movq	%rcx, %r13
	movl	%r14d, %r12d
	movq	%r8, %rbp
	movq	72(%rsp), %r14
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L494:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L206
	leaq	4(%rdx), %rdi
	cmpl	$-1, %esi
	movq	%rdi, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L208:
	testb	%al, %al
	jne	.L39
	leal	(%r12,%rbx), %eax
	addq	$1, %rbx
	cmpq	%rbx, %r14
	je	.L493
.L209:
	movq	160(%rbp), %rax
	movl	0(%r13,%rbx,4), %esi
	testq	%rax, %rax
	jne	.L494
.L206:
	movq	%rbp, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L174:
	movl	24(%rsp), %esi
	movq	%r8, %rdi
	movq	%rcx, 64(%rsp)
	movq	%r9, 32(%rsp)
	movq	%r8, 24(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	24(%rsp), %r8
	movq	32(%rsp), %r9
	sete	%al
	movq	64(%rsp), %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L474:
	movq	24(%rsp), %r9
	movl	64(%rsp), %r12d
	movq	%rbp, %r8
	movq	80(%rsp), %rcx
	movl	%eax, %ebp
.L173:
	movl	16(%rsp), %edx
	testl	%edx, %edx
	jle	.L237
	movslq	16(%rsp), %rax
	movq	88(%rsp), %r10
	subq	%r9, %r10
	movq	%rax, %rbx
	subq	%r10, %rbx
	cmpq	%rax, %r10
	cmovle	%r10, %rax
	xorl	%r13d, %r13d
	leal	1(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L189
	movq	%rbx, 16(%rsp)
	movl	%r12d, 24(%rsp)
	movq	%r9, %rbx
	movq	%r14, %r12
	movq	%rcx, 32(%rsp)
	movq	%r8, %rbp
	movl	%r10d, %r14d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbp)
	movb	%sil, (%rax)
.L187:
	leal	(%r14,%r13), %eax
	addq	$1, %r13
	cmpq	%r12, %r13
	je	.L495
.L188:
	movq	40(%rbp), %rax
	cmpq	48(%rbp), %rax
	movzbl	(%rbx,%r13), %esi
	jb	.L186
	movq	%rbp, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L187
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L471:
	testb	$8, %r14b
	jne	.L121
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L237:
	movl	8(%r15), %eax
	leal	15(%rax), %esi
.L191:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L496
	cmpl	$1, (%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	%sil, (%rax)
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$45, %esi
.L197:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L497
	leaq	1(%rax), %rdx
	addl	$2, %ebp
	movq	%rdx, 40(%r8)
	movb	%sil, (%rax)
.L205:
	movq	40(%rsp), %rbx
	movq	72(%rsp), %rax
	movq	%rbx, %r14
	addq	%rax, %r14
	testq	%rax, %rax
	je	.L457
	movl	%r12d, (%rsp)
	movq	%r8, %r13
	movq	40(%rsp), %r12
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	%sil, (%rax)
.L212:
	movl	%ebx, %eax
	subl	%r12d, %eax
	addl	%ebp, %eax
	cmpq	%rbx, %r14
	je	.L498
.L213:
	addq	$1, %rbx
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	movzbl	-1(%rbx), %esi
	jb	.L211
	movq	%r13, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L212
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L135:
	testb	$16, %al
	je	.L134
	testl	%r12d, %r12d
	je	.L142
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L143
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L143
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L128:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L499
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$45, (%rax)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L498:
	movl	(%rsp), %r12d
	movq	%r13, %r8
	movl	%eax, %r13d
.L210:
	testb	$32, 12(%r15)
	je	.L1
	movl	16(%r15), %esi
	cmpl	$48, %esi
	je	.L1
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jle	.L1
	movslq	%eax, %rbx
	testl	%r12d, %r12d
	movq	%r8, %rdi
	movq	%rbx, %rdx
	je	.L215
	call	_IO_wpadn@PLT
.L216:
	cmpq	%rbx, %rax
	jne	.L39
	addl	48(%rsp), %r13d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L495:
	movq	16(%rsp), %rbx
	movl	24(%rsp), %r12d
	movq	%rbp, %r8
	movq	32(%rsp), %rcx
	movl	%eax, %ebp
.L189:
	testq	%rbx, %rbx
	jle	.L237
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movl	$48, %esi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	__GI__IO_padn
	movq	24(%rsp), %rcx
	movq	16(%rsp), %r8
.L190:
	cmpq	%rbx, %rax
	jne	.L39
	addl	%eax, %ebp
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L94:
	movl	%r10d, 16(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L493:
	movl	(%rsp), %r12d
	movl	%eax, %r13d
	movq	%rbp, %r8
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L136:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L500
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$43, (%rax)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%r12, %r8
	movq	16(%rsp), %r10
	movq	24(%rsp), %r9
	movq	64(%rsp), %rcx
	movl	32(%rsp), %r12d
	movl	%eax, %ebp
.L185:
	movq	%r10, %rbx
	subq	%r9, %rbx
	testq	%rbx, %rbx
	jle	.L178
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movl	$48, %esi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	_IO_wpadn@PLT
	movq	16(%rsp), %r8
	movq	24(%rsp), %rcx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r9, 16(%rsp)
	movl	$45, %esi
.L466:
	movq	%r8, %rdi
	movq	%r8, (%rsp)
	call	__GI___woverflow
	movq	(%rsp), %r8
	movq	16(%rsp), %r9
	cmpl	$-1, %eax
	sete	%al
.L51:
	testb	%al, %al
	je	.L52
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L125:
	call	__GI__IO_padn
	movq	104(%rsp), %rcx
	movq	96(%rsp), %r9
	movq	80(%rsp), %r8
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___woverflow
.L455:
	cmpl	$-1, %eax
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	sete	%al
	movq	104(%rsp), %rcx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r8, %rdi
	movl	$48, %esi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
	je	.L39
	movl	8(%r15), %eax
	leal	23(%rax), %esi
	movq	160(%r8), %rax
	testq	%rax, %rax
	je	.L234
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L164:
	movsbl	64(%rsp), %esi
	movq	%r8, %rdi
	movq	%rcx, 96(%rsp)
	movq	%r9, 80(%rsp)
	movq	%r8, 64(%rsp)
	call	__GI___woverflow
.L456:
	cmpl	$-1, %eax
	movq	64(%rsp), %r8
	movq	80(%rsp), %r9
	sete	%al
	movq	96(%rsp), %rcx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$16383, %r14d
	movl	%r14d, %eax
	subl	(%rsp), %eax
	movl	$1, (%rsp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L100:
	leal	-87(%rdx), %esi
	subl	$97, %ecx
	subl	$48, %edx
	cmpb	$5, %cl
	cmovbe	%esi, %edx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L97:
	leal	-87(%rsi), %edi
	subl	$97, %edx
	subl	$48, %esi
	cmpb	$5, %dl
	cmova	%esi, %edi
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	32(%r10), %rdi
	movq	%rdi, 88(%rsp)
	jmp	.L92
.L142:
	movq	40(%r8), %rax
	cmpq	48(%r8), %rax
	jnb	.L501
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r8)
	movb	$32, (%rax)
	jmp	.L146
.L472:
	testl	%r12d, %r12d
	movslq	48(%rsp), %rbp
	je	.L162
	movq	%r8, %rdi
	movq	%rbp, %rdx
	movl	$48, %esi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	_IO_wpadn@PLT
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
.L163:
	cmpq	%rax, %rbp
	jne	.L39
	addl	48(%rsp), %ebx
	jmp	.L161
.L482:
	movq	%r9, 16(%rsp)
	movl	$45, %esi
.L465:
	movq	%r8, %rdi
	movq	%r8, (%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	movq	16(%rsp), %r9
	sete	%al
	jmp	.L51
.L192:
	movq	%r8, %rdi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movq	24(%rsp), %rcx
	sete	%al
	jmp	.L194
.L199:
	movq	%r8, %rdi
	movq	%rcx, 16(%rsp)
	movq	%r8, (%rsp)
	addl	$2, %ebp
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	movq	16(%rsp), %rcx
	jne	.L202
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$16386, %eax
	subl	(%rsp), %eax
	movl	$1, (%rsp)
	jmp	.L90
.L43:
	movq	%r9, 16(%rsp)
	movl	$43, %esi
	jmp	.L466
.L215:
	call	__GI__IO_padn
	jmp	.L216
.L491:
	movslq	48(%rsp), %rbp
.L162:
	movq	%r8, %rdi
	movq	%rbp, %rdx
	movl	$48, %esi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI__IO_padn
	movq	104(%rsp), %rcx
	movq	96(%rsp), %r9
	movq	80(%rsp), %r8
	jmp	.L163
.L490:
	movzbl	%al, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___overflow
	jmp	.L455
.L489:
	movq	%r8, %rdi
	movl	$48, %esi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
	je	.L39
	movl	8(%r15), %eax
	addl	$23, %eax
	jmp	.L155
.L492:
	movzbl	64(%rsp), %esi
	movq	%r8, %rdi
	movq	%rcx, 96(%rsp)
	movq	%r9, 80(%rsp)
	movq	%r8, 64(%rsp)
	call	__GI___overflow
	jmp	.L456
.L129:
	movl	$45, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___woverflow
.L454:
	cmpl	$-1, %eax
	movq	80(%rsp), %r8
	movq	96(%rsp), %r9
	sete	%al
	movq	104(%rsp), %rcx
	testb	%al, %al
	je	.L146
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L457:
	movl	%ebp, %r13d
	jmp	.L210
.L108:
	testl	%ebx, %ebx
	jne	.L95
.L434:
	cmpl	$7, %edx
	jg	.L112
	testl	%esi, %esi
	je	.L95
.L112:
	movl	16(%rsp), %r11d
	subl	$1, %r11d
	js	.L116
	movslq	%r11d, %rdx
	leaq	(%r9,%rdx), %rdi
	movzbl	(%rdi), %esi
	cmpb	$57, %sil
	je	.L221
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %r10
	movsbq	%sil, %rcx
	movq	%fs:(%r10), %r14
	cmpl	$101, (%r14,%rcx,4)
	jle	.L222
	movl	%r11d, %r11d
	movq	%rdx, %rdi
	subq	%r11, %rdi
	movq	%rdi, %r11
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	subq	$1, %rdx
	movzbl	(%r9,%rdx), %esi
	leaq	(%r9,%rdx), %rdi
	cmpb	$57, %sil
	je	.L221
	movq	%fs:(%r10), %r14
	movsbq	%sil, %rcx
	cmpl	$101, (%r14,%rcx,4)
	jle	.L222
.L114:
	cmpq	%r11, %rdx
	movb	$48, (%r9,%rdx)
	movl	$48, 0(%r13,%rdx,4)
	jne	.L115
.L116:
	cmpb	$57, 64(%rsp)
	je	.L502
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rcx
	movsbq	64(%rsp), %rdx
	movq	%fs:(%rcx), %rcx
	movq	%rdx, %rdi
	cmpl	$101, (%rcx,%rdx,4)
	jg	.L117
	addl	$1, %edi
	movb	%dil, 64(%rsp)
	jmp	.L95
.L221:
	movl	8(%r15), %ecx
	movl	%ecx, 0(%r13,%rdx,4)
	movb	%cl, (%rdi)
	jmp	.L95
.L222:
	addl	$1, %esi
	addl	$1, 0(%r13,%rdx,4)
	movb	%sil, (%rdi)
	jmp	.L95
.L470:
	testw	%cx, %cx
	jne	.L104
	cmpl	$7, %edx
	jle	.L95
	orl	%edi, %esi
	andl	$1, %esi
	je	.L95
	jmp	.L112
.L105:
	testl	%ebx, %ebx
	je	.L95
	jmp	.L434
.L86:
	movzbl	(%rax), %edi
	movb	%dil, 64(%rsp)
	jmp	.L88
.L496:
	movq	%r8, %rdi
	movzbl	%sil, %esi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movq	24(%rsp), %rcx
	sete	%al
	jmp	.L194
.L497:
	movq	%r8, %rdi
	movq	%r8, (%rsp)
	addl	$2, %ebp
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	jne	.L205
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r9, 16(%rsp)
	movl	$43, %esi
	jmp	.L465
.L49:
	movq	%r9, 16(%rsp)
	movl	$32, %esi
	jmp	.L466
.L499:
	movl	$45, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___overflow
	jmp	.L454
.L137:
	movl	$43, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___woverflow
	jmp	.L454
.L502:
	movzbl	8(%r15), %edi
	movb	%dil, 64(%rsp)
	jmp	.L95
.L117:
	movl	(%rsp), %r11d
	testl	%r11d, %r11d
	je	.L118
	leal	-4(%rax), %edx
	testl	%edx, %edx
	jle	.L503
	movl	%edx, %eax
	movb	$49, 64(%rsp)
	jmp	.L95
.L485:
	movq	%r9, 16(%rsp)
	movl	$32, %esi
	jmp	.L465
.L178:
	movl	8(%r15), %eax
	leal	15(%rax), %esi
	jmp	.L242
.L500:
	movl	$43, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___overflow
	jmp	.L454
.L143:
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___woverflow
	jmp	.L454
.L118:
	addl	$4, %eax
	movb	$49, 64(%rsp)
	jmp	.L95
.L501:
	movl	$32, %esi
	movq	%r8, %rdi
	movq	%rcx, 104(%rsp)
	movq	%r9, 96(%rsp)
	movq	%r8, 80(%rsp)
	call	__GI___overflow
	jmp	.L454
.L503:
	movl	$4, %edx
	movb	$49, 64(%rsp)
	movl	$0, (%rsp)
	subl	%eax, %edx
	movl	%edx, %eax
	jmp	.L95
.L4:
	leaq	__PRETTY_FUNCTION__.14502(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$165, %edx
	call	__GI___assert_fail
.L104:
	call	__GI_abort
.L479:
	testl	%ebp, %ebp
	jg	.L229
	movl	%ebp, %eax
	xorl	%ebp, %ebp
	jmp	.L32
	.size	__printf_fphex, .-__printf_fphex
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.14502, @object
	.size	__PRETTY_FUNCTION__.14502, 15
__PRETTY_FUNCTION__.14502:
	.string	"__printf_fphex"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC11:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC12:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC13:
	.long	4294967295
	.long	2146435071
	.hidden	_itowa_upper_digits
	.hidden	_itowa_lower_digits
	.hidden	_itoa_word
