	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.tempnam
	.previous
#NO_APP
	.p2align 4,,15
	.globl	tempnam
	.type	tempnam, @function
tempnam:
	pushq	%rbx
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	movl	$1, %r8d
	movl	$4096, %esi
	subq	$4096, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__path_search
	testl	%eax, %eax
	jne	.L4
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rbx, %rdi
	call	__gen_tempname
	testl	%eax, %eax
	jne	.L4
	movq	%rbx, %rdi
	call	__GI___strdup
	addq	$4096, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$4096, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	tempnam, .-tempnam
	.section	.gnu.warning.tempnam
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_tempnam, @object
	.size	__evoke_link_warning_tempnam, 56
__evoke_link_warning_tempnam:
	.string	"the use of `tempnam' is dangerous, better use `mkstemp'"
	.hidden	__gen_tempname
	.hidden	__path_search
