	.text
	.p2align 4,,15
	.globl	__isoc99_vsscanf
	.hidden	__isoc99_vsscanf
	.type	__isoc99_vsscanf, @function
__isoc99_vsscanf:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	subq	$248, %rsp
	movl	$-1, %edx
	movl	$32768, %esi
	movq	%rsp, %rbx
	movq	$0, 136(%rsp)
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	leaq	_IO_str_jumps(%rip), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, 216(%rsp)
	call	_IO_str_init_static_internal@PLT
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$2, %ecx
	call	__vfscanf_internal
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__isoc99_vsscanf, .-__isoc99_vsscanf
	.hidden	__vfscanf_internal
	.hidden	_IO_str_jumps
