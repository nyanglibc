	.text
	.p2align 4,,15
	.globl	parse_printf_format
	.type	parse_printf_format, @function
parse_printf_format:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movl	$37, %esi
	subq	$104, %rsp
	movq	$0, 8(%rsp)
	call	__strchrnul@PLT
	cmpb	$0, (%rax)
	je	.L9
	leaq	16(%rsp), %rbp
	movq	%rax, %rdi
	xorl	%r15d, %r15d
	leaq	8(%rsp), %r13
	leaq	64(%rbp), %r14
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L24:
	movl	68(%rsp), %edx
	movl	%edx, (%r12,%rax,4)
.L5:
	movq	48(%rsp), %rdi
	cmpb	$0, (%rdi)
	je	.L23
.L8:
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r13, %rcx
	call	__parse_one_specmb
	movslq	60(%rsp), %rdx
	addq	%rax, %r15
	cmpl	$-1, %edx
	je	.L3
	cmpq	%rbx, %rdx
	jnb	.L3
	movl	$0, (%r12,%rdx,4)
.L3:
	movslq	56(%rsp), %rdx
	cmpl	$-1, %edx
	je	.L4
	cmpq	%rbx, %rdx
	jnb	.L4
	movl	$0, (%r12,%rdx,4)
.L4:
	movslq	64(%rsp), %rax
	cmpq	%rbx, %rax
	jnb	.L5
	movq	72(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L5
	cmpq	$1, %rdx
	je	.L24
	movq	%rbx, %rsi
	movslq	24(%rsp), %r8
	leaq	(%r12,%rax,4), %rdx
	subq	%rax, %rsi
	movq	__printf_arginfo_table(%rip), %rax
	movq	%rbp, %rdi
	movq	%r14, %rcx
	call	*(%rax,%r8,8)
	movq	48(%rsp), %rdi
	cmpb	$0, (%rdi)
	jne	.L8
.L23:
	cmpq	%r15, 8(%rsp)
	movq	%r15, %rax
	cmovnb	8(%rsp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	parse_printf_format, .-parse_printf_format
	.hidden	__printf_arginfo_table
	.hidden	__parse_one_specmb
