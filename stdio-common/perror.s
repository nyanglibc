	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	": "
.LC2:
	.string	"%s%s%s\n"
	.text
	.p2align 4,,15
	.type	perror_internal, @function
perror_internal:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%edx, %edi
	subq	$1024, %rsp
	testq	%rsi, %rsi
	je	.L4
	cmpb	$0, (%rsi)
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rbp
	je	.L4
.L2:
	movq	%rsp, %rsi
	movl	$1024, %edx
	call	__strerror_r
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$1024, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC0(%rip), %rbp
	movq	%rbp, %rbx
	jmp	.L2
	.size	perror_internal, .-perror_internal
	.section	.rodata.str1.1
.LC3:
	.string	"w+"
	.text
	.p2align 4,,15
	.globl	perror
	.hidden	perror
	.type	perror, @function
perror:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	stderr(%rip), %rdi
	movl	%fs:(%rax), %edx
	movl	192(%rdi), %eax
	testl	%eax, %eax
	je	.L20
.L8:
	addq	$16, %rsp
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	perror_internal
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%edx, 12(%rsp)
	call	__fileno
	cmpl	$-1, %eax
	movl	12(%rsp), %edx
	jne	.L9
.L19:
	movq	stderr(%rip), %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%eax, %edi
	movl	%edx, 12(%rsp)
	call	__dup
	cmpl	$-1, %eax
	movl	%eax, %ebp
	movl	12(%rsp), %edx
	je	.L19
	leaq	.LC3(%rip), %rsi
	movl	%eax, %edi
	movl	%edx, 12(%rsp)
	call	_IO_new_fdopen@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	movl	12(%rsp), %edx
	je	.L21
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	perror_internal
	testb	$32, (%r12)
	je	.L13
	movq	stderr(%rip), %rax
	orl	$32, (%rax)
.L13:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	_IO_new_fclose@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%ebp, %edi
	movl	%edx, 12(%rsp)
	call	__close
	movq	stderr(%rip), %rdi
	movl	12(%rsp), %edx
	jmp	.L8
	.size	perror, .-perror
	.hidden	__close
	.hidden	__dup
	.hidden	__fileno
	.hidden	__fxprintf
	.hidden	__strerror_r
