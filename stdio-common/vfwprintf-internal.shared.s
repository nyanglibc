	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rdx
	movl	$2147483647, %r10d
	movl	$-1, %r9d
	movl	4(%rdx), %ecx
	movl	(%rdx), %eax
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	subl	$48, %eax
	cmpl	$9, %ecx
	ja	.L5
.L7:
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jg	.L14
	leal	(%rax,%rax,4), %eax
	movl	%r10d, %r8d
	subl	%ecx, %r8d
	addl	%eax, %eax
	cmpl	%eax, %r8d
	jl	.L14
	addl	%ecx, %eax
.L3:
	movq	%rsi, %rdx
	movl	4(%rdx), %ecx
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L7
.L5:
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	12(%rdx), %eax
	leaq	12(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	%r9d, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	movq	%rsi, (%rdi)
	ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	_IO_helper_overflow, @function
_IO_helper_overflow:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	subq	$24, %rsp
	movq	160(%rdi), %rdx
	movq	24(%rdx), %rsi
	movq	32(%rdx), %rbx
	subq	%rsi, %rbx
	sarq	$2, %rbx
	testl	%ebx, %ebx
	je	.L17
	movq	456(%rdi), %rdi
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	216(%rdi), %r13
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L25
.L18:
	movslq	%ebx, %rbx
	movq	%rbx, %rdx
	call	*56(%r13)
	testq	%rax, %rax
	je	.L22
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	je	.L22
	movq	160(%r12), %rdx
	leaq	0(,%rax,4), %r13
	subq	%rax, %rbx
	movq	24(%rdx), %rdi
	movq	%rbx, %rdx
	leaq	(%rdi,%r13), %rsi
	call	__wmemmove
	movq	160(%r12), %rdx
	subq	%r13, 32(%rdx)
.L17:
	movq	32(%rdx), %rax
	cmpq	40(%rdx), %rax
	jnb	.L26
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	%ebp, (%rax)
	movl	%ebp, %eax
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	160(%r12), %rax
	movq	8(%rsp), %rdi
	movq	24(%rax), %rsi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$24, %rsp
	movl	%ebp, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI___woverflow
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-1, %eax
	jmp	.L16
	.size	_IO_helper_overflow, .-_IO_helper_overflow
	.p2align 4,,15
	.type	group_number, @function
group_number:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r9
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movsbl	(%rcx), %ebx
	leal	-1(%rbx), %eax
	cmpb	$125, %al
	jbe	.L39
.L28:
	addq	$24, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rdx, %rbp
	movq	%rdx, %r12
	movl	%r8d, 12(%rsp)
	subq	%rsi, %rbp
	leaq	1(%rcx), %r13
	movq	%rbp, %rdx
	call	__GI_memmove
	leaq	(%rax,%rbp), %rsi
	movq	%rax, %r10
	movq	%r12, %r9
	movl	12(%rsp), %r8d
	cmpq	%rsi, %rax
	jnb	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	subq	$4, %rsi
	movl	(%rsi), %eax
	subl	$1, %ebx
	leaq	-4(%r12), %r9
	movl	%eax, -4(%r12)
	jne	.L30
	cmpq	%rsi, %r10
	jnb	.L28
	cmpq	%r9, %rsi
	je	.L31
	movl	%r8d, -8(%r12)
	movsbl	0(%r13), %ebx
	leaq	-8(%r12), %r9
	cmpb	$127, %bl
	je	.L31
	testb	%bl, %bl
	js	.L31
	testb	%bl, %bl
	je	.L34
	addq	$1, %r13
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r9, %r12
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rsi, %r10
	jb	.L36
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	movsbl	-1(%r13), %ebx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r10, %rdx
	movq	%r9, %rdi
	subq	%rsi, %rdx
	call	__GI_memmove
	addq	$24, %rsp
	movq	%rax, %r9
	popq	%rbx
	movq	%r9, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	group_number, .-group_number
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_outpunct"
	.text
	.p2align 4,,15
	.type	_i18n_number_rewrite, @function
_i18n_number_rewrite:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %rbp
	subq	$1064, %rsp
	subq	%r13, %rbp
	call	__wctrans@PLT
	leaq	16(%rsp), %rbx
	movq	%rax, %r12
	movq	%rax, %rsi
	movl	$46, %edi
	call	__GI___towctrans
	movq	%r12, %rsi
	movl	$44, %edi
	movl	%eax, 8(%rsp)
	call	__GI___towctrans
	movq	%rbp, %rsi
	movl	%eax, 12(%rsp)
	leaq	16(%rbx), %rax
	sarq	$2, %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%rax, 16(%rsp)
	movq	$1024, 24(%rsp)
	call	__GI___libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L50
	movq	16(%rsp), %r15
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	__GI_mempcpy@PLT
	movq	%rax, %rcx
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
.L42:
	subq	$4, %rcx
	cmpq	%rcx, %r15
	ja	.L55
.L49:
	movl	(%rcx), %r8d
	subq	$4, %r14
	leal	-48(%r8), %eax
	cmpl	$9, %eax
	jbe	.L56
	testq	%r12, %r12
	jne	.L57
.L45:
	subq	$4, %rcx
	movl	%r8d, (%r14)
	cmpq	%rcx, %r15
	jbe	.L49
.L55:
	movq	16(%rsp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L40
	call	free@PLT
.L40:
	addq	$1064, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rdx), %rax
	addl	$3, %r8d
	movslq	%r8d, %r8
	movl	64(%rax,%r8,8), %eax
	movl	%eax, (%r14)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r8d, %eax
	andl	$-3, %eax
	cmpl	$44, %eax
	jne	.L45
	cmpl	$46, %r8d
	movl	8(%rsp), %eax
	cmovne	12(%rsp), %eax
	movl	%eax, (%r14)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r13, %r14
	jmp	.L40
	.size	_i18n_number_rewrite, .-_i18n_number_rewrite
	.section	.rodata.str1.1
.LC1:
	.string	"vfprintf-internal.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(size_t) done <= (size_t) INT_MAX"
	.section	.text.unlikely,"ax",@progbits
	.type	outstring_func.part.1, @function
outstring_func.part.1:
	leaq	__PRETTY_FUNCTION__.12664(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$238, %edx
	call	__GI___assert_fail
	.size	outstring_func.part.1, .-outstring_func.part.1
	.text
	.p2align 4,,15
	.type	outstring_converted_wide_string, @function
outstring_converted_wide_string:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%r9d, %ebx
	subq	$328, %rsp
	testl	%ecx, %ecx
	setg	23(%rsp)
	cmpb	$1, %r8b
	movq	%rsi, 40(%rsp)
	movl	%edx, 16(%rsp)
	movl	%ecx, 24(%rsp)
	movl	%r8d, 28(%rsp)
	movzbl	23(%rsp), %edi
	je	.L61
	testb	%dil, %dil
	je	.L61
	testl	%edx, %edx
	movq	$0, 56(%rsp)
	movq	%rsi, 48(%rsp)
	js	.L142
	movslq	16(%rsp), %r14
	movl	$0, %r12d
	je	.L64
	testq	%rsi, %rsi
	je	.L64
	leaq	64(%rsp), %rax
	movq	%r14, %rbx
	leaq	56(%rsp), %r15
	leaq	48(%rsp), %r13
	movl	%r9d, 8(%rsp)
	movq	%rax, %r14
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L143:
	testq	%rax, %rax
	je	.L136
	addq	%rax, %r12
	subq	%rax, %rbx
	je	.L136
	cmpq	$0, 48(%rsp)
	je	.L136
.L67:
	cmpq	$64, %rbx
	movl	$64, %edx
	movq	%r15, %rcx
	cmovb	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$-1, %eax
.L60:
	addq	$328, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movl	8(%rsp), %ebx
.L63:
	movslq	24(%rsp), %rax
	cmpq	%r12, %rax
	ja	.L64
.L68:
	movq	40(%rsp), %rsi
.L61:
	movslq	16(%rsp), %r8
	movq	$-1, %rax
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	$0, 56(%rsp)
	testl	%r8d, %r8d
	cmovns	%r8, %rax
	xorl	%r12d, %r12d
	movq	%rax, %r14
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%r14, %r15
	subq	%rcx, %rax
	movq	%rax, 8(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	*56(%rax)
	cmpq	%rax, %r13
	jne	.L140
	movslq	%ebx, %rbx
	movabsq	$-9223372036854775808, %rdi
	leaq	(%rbx,%r13), %rax
	leaq	(%rax,%rdi), %rdx
	movslq	%eax, %rcx
	movl	%eax, %ebx
	cmpq	%r13, %rdx
	setb	%dl
	cmpq	%rcx, %rax
	movl	$1, %ecx
	movzbl	%dl, %edx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L139
	testl	%eax, %eax
	js	.L60
	movl	16(%rsp), %edx
	movq	%r15, %rax
	movq	40(%rsp), %rsi
	subq	%r13, %rax
	addq	%r13, %r12
	testl	%edx, %edx
	cmovns	%rax, %r15
.L75:
	testq	%r15, %r15
	je	.L91
	testq	%rsi, %rsi
	je	.L91
	leaq	64(%rsp), %r14
	cmpq	$64, %r15
	movl	$64, %edx
	leaq	56(%rsp), %rcx
	leaq	40(%rsp), %rsi
	cmovb	%r15, %rdx
	movq	%r14, %rdi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L140
	testq	%rax, %rax
	je	.L91
	testl	%ebx, %ebx
	js	.L144
	movq	216(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, 8(%rsp)
	ja	.L82
	movq	%rax, 32(%rsp)
	call	_IO_vtable_check
	movq	32(%rsp), %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	cmpb	$0, 23(%rsp)
	je	.L106
	cmpb	$0, 28(%rsp)
	je	.L106
	movslq	24(%rsp), %rdx
	movl	%ebx, %eax
	cmpq	%r12, %rdx
	jbe	.L60
	movl	24(%rsp), %edx
	subl	%r12d, %edx
	testl	%edx, %edx
	jle	.L60
	movslq	%edx, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	movq	%rax, %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r12
	jne	.L60
	testl	%ebx, %ebx
	movl	%ebx, %eax
	js	.L60
	addl	%r12d, %eax
	jno	.L60
	.p2align 4,,10
	.p2align 3
.L139:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L64:
	movl	24(%rsp), %eax
	subl	%r12d, %eax
	testl	%eax, %eax
	jle	.L69
	movslq	%eax, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L140
	testl	%ebx, %ebx
	jns	.L145
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%ebx, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	__mbsrtowcs
	movq	%rax, %r12
	jmp	.L63
.L145:
	addl	%r12d, %ebx
	jo	.L139
.L69:
	testl	%ebx, %ebx
	jns	.L68
	movl	%ebx, %eax
	jmp	.L60
.L144:
	call	outstring_func.part.1
	.size	outstring_converted_wide_string, .-outstring_converted_wide_string
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC3:
	.string	"("
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"i"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	")"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC4:
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(mode_flags & PRINTF_FORTIFY) != 0"
	.align 8
.LC6:
	.string	"*** invalid %N$ use detected ***\n"
	.align 8
.LC7:
	.string	"*** %n in writable segment detected ***\n"
	.text
	.p2align 4,,15
	.type	printf_positional, @function
printf_positional:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-2128(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2312, %rsp
	movq	%rax, -2232(%rbp)
	addq	$16, %rax
	movq	%rax, -2128(%rbp)
	leaq	-1088(%rbp), %rax
	movq	%r8, -2240(%rbp)
	movq	%rdi, -2200(%rbp)
	movq	%rsi, -2344(%rbp)
	movq	%rax, -2312(%rbp)
	addq	$16, %rax
	cmpq	$-1, 48(%rbp)
	movl	%edx, -2332(%rbp)
	movl	%r9d, -2248(%rbp)
	movq	24(%rbp), %r8
	movq	$1024, -2120(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$1024, -1080(%rbp)
	movq	$0, -2184(%rbp)
	je	.L949
.L147:
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L950
	movq	-2232(%rbp), %rax
	movl	$14, %r12d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%r12, -2208(%rbp)
	movq	%r8, %r14
	leaq	16(%rax), %r13
	leaq	-2184(%rbp), %rax
	movq	%rax, -2216(%rbp)
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	(%r15,%r15,8), %rax
	movq	-2216(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %r15
	leaq	0(%r13,%rax,8), %r12
	movq	%r12, %rdx
	call	__parse_one_specwc
	movq	32(%r12), %r14
	addq	%rax, %rbx
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L951
	cmpq	%r15, -2208(%rbp)
	jne	.L148
	movq	-2232(%rbp), %rdi
	call	__GI___libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L151
	movabsq	$-2049638230412172401, %rax
	movq	-2128(%rbp), %r13
	mulq	-2120(%rbp)
	shrq	$6, %rdx
	movq	%rdx, -2208(%rbp)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L965:
	testl	%ebx, %ebx
	jle	.L915
	movq	-2200(%rbp), %rdi
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%rbx, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %rbx
	je	.L952
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$-1, %ebx
.L194:
	movq	-2312(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L454
	call	free@PLT
.L454:
	movq	-2232(%rbp), %rax
	movq	-2128(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L146
	call	free@PLT
.L146:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	cmpq	%rbx, -2184(%rbp)
	cmovnb	-2184(%rbp), %rbx
.L149:
	movq	-2312(%rbp), %rdi
	movl	$24, %edx
	movq	%rbx, %rsi
	call	__GI___libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L151
	movq	-1088(%rbp), %rax
	movq	%rbx, %r12
	leaq	0(,%rbx,4), %rdx
	salq	$4, %r12
	addq	%rax, %r12
	movq	%rax, -2224(%rbp)
	movl	64(%rbp), %eax
	leaq	(%r12,%rdx), %r10
	andl	$2, %eax
	movq	%r10, %rdi
	setne	%sil
	movl	%eax, -2328(%rbp)
	movzbl	%sil, %esi
	negl	%esi
	call	__GI_memset
	testq	%r15, %r15
	movq	%rax, %r10
	je	.L153
	leaq	(%r15,%r15,8), %rax
	movq	%r13, %r14
	movq	%r13, -2208(%rbp)
	movq	%rbx, -2216(%rbp)
	movq	%r12, %rbx
	movq	%r10, %r12
	leaq	0(%r13,%rax,8), %r8
	movq	%r8, %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L954:
	movslq	48(%r14), %rax
	movl	52(%r14), %edx
	movl	%edx, (%r12,%rax,4)
	movslq	48(%r14), %rax
	movl	64(%r14), %edx
	movl	%edx, (%rbx,%rax,4)
.L157:
	addq	$72, %r14
	cmpq	%r14, %r13
	je	.L953
.L159:
	movslq	44(%r14), %rax
	cmpl	$-1, %eax
	je	.L154
	movl	$0, (%r12,%rax,4)
.L154:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L155
	movl	$0, (%r12,%rax,4)
.L155:
	movq	56(%r14), %rsi
	testq	%rsi, %rsi
	je	.L157
	cmpq	$1, %rsi
	je	.L954
	movslq	48(%r14), %rdx
	movslq	8(%r14), %rax
	movq	%r14, %rdi
	movq	__printf_arginfo_table(%rip), %r10
	addq	$72, %r14
	salq	$2, %rdx
	leaq	(%rbx,%rdx), %rcx
	addq	%r12, %rdx
	call	*(%r10,%rax,8)
	cmpq	%r14, %r13
	jne	.L159
.L953:
	movq	%r12, %r10
	movq	%rbx, %r12
	movq	-2216(%rbp), %rbx
	movq	-2208(%rbp), %r13
	testq	%rbx, %rbx
	je	.L160
.L460:
	movl	64(%rbp), %edx
	movq	-2224(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	-2240(%rbp), %r8
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L193:
	movslq	(%r10,%r14,4), %rax
	cmpl	$5, %eax
	jg	.L162
	cmpl	$3, %eax
	jge	.L163
	cmpl	$1, %eax
	jg	.L164
	testl	%eax, %eax
	jns	.L165
	cmpl	$-1, %eax
	jne	.L161
	movl	-2328(%rbp), %eax
	testl	%eax, %eax
	je	.L955
	leaq	.LC6(%rip), %rdi
	call	__GI___libc_fatal
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	$256, %eax
	je	.L163
	jle	.L956
	cmpl	$512, %eax
	je	.L163
	cmpl	$1024, %eax
	je	.L165
	cmpl	$263, %eax
	jne	.L161
	testl	%edx, %edx
	jne	.L957
	testb	$8, 64(%rbp)
	je	.L183
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L184
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L185:
	movq	%r14, %rax
	movdqa	(%rsi), %xmm0
	addq	$1, %r14
	salq	$4, %rax
	cmpq	%rbx, %r14
	movaps	%xmm0, (%rcx,%rax)
	jb	.L193
	.p2align 4,,10
	.p2align 3
.L160:
	movslq	16(%rbp), %rax
	cmpq	%r15, %rax
	movq	%rax, -2216(%rbp)
	jnb	.L907
	movq	32(%rbp), %rcx
	leaq	(%rax,%rax,8), %rax
	movq	%r15, -2296(%rbp)
	leaq	0(%r13,%rax,8), %r14
	leaq	__stop___libc_IO_vtables(%rip), %rax
	leaq	1000(%rcx), %rdi
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	%rdi, -2320(%rbp)
	movl	-2248(%rbp), %edi
	subq	%rcx, %rax
	movq	%rax, -2256(%rbp)
	movl	%edi, %r15d
	.p2align 4,,10
	.p2align 3
.L453:
	movzbl	12(%r14), %eax
	movl	16(%r14), %esi
	movl	(%r14), %r10d
	movl	%esi, -2248(%rbp)
	movl	%eax, %edx
	movl	%eax, %edi
	movl	%eax, %r8d
	shrb	$5, %dl
	shrb	$3, %dil
	movl	%eax, %r9d
	movl	%edx, %ecx
	movzbl	13(%r14), %edx
	andl	$1, %edi
	andl	$1, %ecx
	movl	%eax, %r12d
	movl	%eax, %r13d
	movl	%ecx, -2240(%rbp)
	movl	%eax, %ecx
	shrb	$4, %r8b
	andl	$1, %ecx
	shrb	$6, %r9b
	shrb	%r12b
	movl	%edx, %ebx
	shrb	$3, %dl
	movb	%cl, -2276(%rbp)
	andl	$1, %edx
	movl	8(%r14), %ecx
	shrb	%bl
	movb	%dl, -2288(%rbp)
	movslq	44(%r14), %rdx
	shrb	$2, %r13b
	movb	%dil, -2264(%rbp)
	movl	%eax, %edi
	andl	$1, %r8d
	shrb	$7, %dil
	andl	$1, %r9d
	andl	$1, %r12d
	andl	$1, %ebx
	andl	$1, %r13d
	movb	%dil, -2272(%rbp)
	cmpl	$-1, %edx
	movl	%ecx, -2208(%rbp)
	je	.L958
	movq	-2224(%rbp), %rsi
	salq	$4, %rdx
	movl	(%rsi,%rdx), %r11d
	testl	%r11d, %r11d
	js	.L197
	movl	%r11d, 4(%r14)
.L196:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L199
	movq	-2224(%rbp), %rdi
	salq	$4, %rax
	movl	(%rdi,%rax), %r10d
	testl	%r10d, %r10d
	js	.L200
	movl	%r10d, (%r14)
.L199:
	movl	-2208(%rbp), %edi
	cmpl	$255, %edi
	jg	.L202
	movq	__printf_function_table(%rip), %rax
	testq	%rax, %rax
	je	.L202
	movslq	%edi, %rsi
	movq	(%rax,%rsi,8), %rax
	movq	%rsi, -2304(%rbp)
	testq	%rax, %rax
	je	.L202
	movq	56(%r14), %rdi
	leaq	30(,%rdi,8), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	testq	%rdi, %rdi
	je	.L203
	movl	48(%r14), %eax
	movb	%r9b, -2324(%rbp)
	xorl	%ecx, %ecx
	movq	-2224(%rbp), %r9
	movb	%r8b, -2280(%rbp)
	xorl	%esi, %esi
	movl	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L204:
	leal	(%r8,%rcx), %eax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, (%rdx,%rsi,8)
	leal	1(%rcx), %esi
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	ja	.L204
	movq	__printf_function_table(%rip), %rax
	movq	-2304(%rbp), %rcx
	movzbl	-2280(%rbp), %r8d
	movzbl	-2324(%rbp), %r9d
	movq	(%rax,%rcx,8), %rax
.L203:
	movl	%r10d, -2336(%rbp)
	movl	%r11d, -2324(%rbp)
	movq	%r14, %rsi
	movb	%r9b, -2280(%rbp)
	movb	%r8b, -2304(%rbp)
	movq	-2200(%rbp), %rdi
	call	*%rax
	cmpl	$-2, %eax
	movzbl	-2304(%rbp), %r8d
	movzbl	-2280(%rbp), %r9d
	movl	-2324(%rbp), %r11d
	movl	-2336(%rbp), %r10d
	je	.L202
.L933:
	testl	%eax, %eax
	js	.L151
.L931:
	testl	%r15d, %r15d
	js	.L901
	cltq
	addl	%r15d, %eax
	movl	%eax, %r15d
	jo	.L906
.L915:
	testl	%r15d, %r15d
	js	.L901
.L281:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	subq	%rsi, %rbx
	sarq	$2, %rbx
.L459:
	movq	-2200(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	216(%rax), %r12
	movq	%r12, %rax
	subq	%rcx, %rax
	cmpq	-2256(%rbp), %rax
	jnb	.L959
.L449:
	movq	%rbx, %rdx
	movq	-2200(%rbp), %rdi
	call	*56(%r12)
	cmpq	%rbx, %rax
	jne	.L151
	movslq	%r15d, %r9
	xorl	%eax, %eax
	addq	%rbx, %r9
	js	.L451
	cmpq	%rbx, %r9
	jb	.L451
.L450:
	movslq	%r9d, %rdx
	movl	%r9d, %r15d
	cmpq	%rdx, %r9
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	jne	.L906
	testl	%r9d, %r9d
	js	.L901
	addq	$1, -2216(%rbp)
	addq	$72, %r14
	movq	-2216(%rbp), %rax
	cmpq	-2296(%rbp), %rax
	jb	.L453
	.p2align 4,,10
	.p2align 3
.L901:
	movl	%r15d, %ebx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L163:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L186
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L187:
	movq	(%rsi), %rsi
	movq	%r14, %rax
	salq	$4, %rax
	movq	%rsi, (%rcx,%rax)
.L173:
	addq	$1, %r14
	cmpq	%rbx, %r14
	jb	.L193
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r14, %rsi
	salq	$4, %rsi
	testb	$8, %ah
	leaq	(%rcx,%rsi), %rdi
	je	.L188
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L189
	movl	%eax, %edi
	addq	16(%r8), %rdi
	addl	$8, %eax
	movl	%eax, (%r8)
.L190:
	movq	(%rdi), %rax
	movq	%rax, (%rcx,%rsi)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L165:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L174
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L175:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L202:
	movl	-2208(%rbp), %eax
	subl	$32, %eax
	cmpl	$90, %eax
	jbe	.L960
.L209:
	movq	56(%r14), %rsi
	leaq	30(,%rsi,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	testq	%rsi, %rsi
	je	.L387
	movl	48(%r14), %r8d
	movq	-2224(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L388:
	leal	(%r8,%rdx), %eax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, (%rdi,%rcx,8)
	leal	1(%rdx), %ecx
	cmpq	%rcx, %rsi
	movq	%rcx, %rdx
	ja	.L388
.L387:
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L389
	movq	32(%rdx), %rax
	movq	40(%rdx), %rsi
	cmpq	%rsi, %rax
	jnb	.L389
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	$37, (%rax)
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L961
.L392:
	cmpq	%rcx, %rsi
	jbe	.L398
	leaq	4(%rcx), %rax
	movl	$2, %ebx
	movq	%rax, 32(%rdx)
	movl	$35, (%rcx)
	movzbl	12(%r14), %eax
	testb	%al, %al
	js	.L462
.L401:
	testb	$64, %al
	je	.L405
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L406
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L406
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$43, (%rdx)
.L412:
	movzbl	12(%r14), %eax
	addl	$1, %ebx
.L409:
	testb	$32, %al
	je	.L413
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L414
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L414
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$45, (%rdx)
.L416:
	addl	$1, %ebx
.L413:
	cmpl	$48, 16(%r14)
	je	.L962
.L417:
	testb	$8, 13(%r14)
	je	.L421
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L422
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L422
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$73, (%rdx)
.L424:
	addl	$1, %ebx
.L421:
	movslq	4(%r14), %rcx
	testl	%ecx, %ecx
	je	.L425
	leaq	-2176(%rbp), %rdi
	movabsq	$-3689348814741910323, %r8
	leaq	48(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rcx, %rax
	subq	$4, %r12
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	leaq	_itowa_lower_digits(%rip), %rax
	testq	%rdx, %rdx
	movl	(%rax,%rcx,4), %esi
	movq	%rdx, %rcx
	movl	%esi, (%r12)
	jne	.L426
	leaq	48(%rdi), %r13
	cmpq	%r13, %r12
	jnb	.L425
	movq	%r14, -2208(%rbp)
	movq	%r13, %r14
	movq	-2200(%rbp), %r13
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L963:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L427
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L151
.L431:
	cmpl	$2147483647, %ebx
	je	.L151
	addl	$1, %ebx
	cmpq	%r14, %r12
	jnb	.L899
	movl	(%r12), %esi
.L432:
	movq	160(%r13), %rax
	addq	$4, %r12
	testq	%rax, %rax
	jne	.L963
.L427:
	movq	%r13, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L431
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L960:
	leaq	jump_table(%rip), %rdx
	movzbl	%r9b, %edi
	movzbl	%r8b, %ecx
	movl	%ecx, -2280(%rbp)
	movl	%edi, -2304(%rbp)
	movzbl	%r12b, %r12d
	movzbl	(%rdx,%rax), %edx
	leaq	step4_jumps.13002(%rip), %rax
	movzbl	%bl, %ebx
	movzbl	%r13b, %r13d
	movzbl	-2248(%rbp), %r9d
	movslq	(%rax,%rdx,4), %rax
	leaq	.L209(%rip), %rdx
	addq	%rdx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L341:
	movl	-2240(%rbp), %esi
	leal	-1(%r11), %ebx
	testl	%esi, %esi
	je	.L964
.L353:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rsi
	salq	$4, %rax
	movl	(%rsi,%rax), %esi
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L357
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L357
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L151
.L360:
	cmpl	$2147483647, %r15d
	je	.L151
	movl	-2240(%rbp), %ecx
	addl	$1, %r15d
	testl	%ecx, %ecx
	jne	.L965
.L214:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	subq	%rsi, %rbx
	sarq	$2, %rbx
	testl	%r15d, %r15d
	jns	.L459
.L371:
	call	outstring_func.part.1
	.p2align 4,,10
	.p2align 3
.L958:
	movl	4(%r14), %r11d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	negl	%r11d
	orl	$32, %eax
	movl	$1, -2240(%rbp)
	movl	%r11d, 4(%r14)
	movb	%al, 12(%r14)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$-1, (%r14)
	movl	$-1, %r10d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L956:
	cmpl	$7, %eax
	jg	.L161
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L178
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L179:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L949:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	8(%rax), %rax
	movl	96(%rax), %esi
	movq	80(%rax), %rax
	movq	%rax, 48(%rbp)
	movzbl	(%rax), %eax
	movl	%esi, 56(%rbp)
	testb	%al, %al
	je	.L469
	cmpb	$127, %al
	jne	.L147
.L469:
	movq	$0, 48(%rbp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L959:
	movq	%rsi, -2208(%rbp)
	call	_IO_vtable_check
	movq	-2208(%rbp), %rsi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L188:
	movq	__printf_va_arg_table(%rip), %r9
	testq	%r9, %r9
	jne	.L966
.L191:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L955:
	leaq	__PRETTY_FUNCTION__.12998(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$1888, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%r8), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 8(%r8)
	fldt	(%rax)
	movq	%r14, %rax
	salq	$4, %rax
	fstpt	(%rcx,%rax)
	jmp	.L173
.L364:
.L339:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rdi
	salq	$4, %rax
	movq	(%rdi,%rax), %rbx
	testq	%rbx, %rbx
	je	.L967
.L365:
	andl	$1, %r13d
	jne	.L368
	cmpl	$83, -2208(%rbp)
	je	.L368
	movl	-2240(%rbp), %r8d
	movq	-2200(%rbp), %rdi
	movl	%r15d, %r9d
	movl	%r11d, %ecx
	movl	%r10d, %edx
	movq	%rbx, %rsi
	call	outstring_converted_wide_string
	movl	%eax, %r15d
	jmp	.L915
.L340:
	testl	%r13d, %r13d
	jne	.L341
	movl	-2240(%rbp), %edi
	leal	-1(%r11), %ebx
	testl	%edi, %edi
	jne	.L342
	testl	%ebx, %ebx
	jle	.L343
	movq	-2200(%rbp), %rdi
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	addl	%r15d, %r12d
	movl	%r12d, %r15d
	jo	.L906
.L343:
	testl	%r15d, %r15d
	js	.L901
.L342:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rsi
	salq	$4, %rax
	movzbl	(%rsi,%rax), %edi
	call	__btowc
	movq	-2200(%rbp), %rdi
	movq	160(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L346
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L346
	leaq	4(%rcx), %rsi
	cmpl	$-1, %eax
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	jne	.L360
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L338:
	movq	32(%rbp), %rsi
	movl	40(%rbp), %edi
	movl	$1000, %edx
	movl	%r10d, -2264(%rbp)
	movl	%r11d, -2248(%rbp)
	xorl	%r13d, %r13d
	call	__GI___strerror_r
	movq	%rax, %rbx
	movl	-2248(%rbp), %r11d
	movl	-2264(%rbp), %r10d
	testq	%rbx, %rbx
	jne	.L365
.L967:
	cmpl	$-1, %r10d
	je	.L487
	xorl	%r12d, %r12d
	cmpl	$5, %r10d
	leaq	.LC4(%rip), %rbx
	jle	.L366
.L487:
	subl	$6, %r11d
	movl	$6, %r12d
	leaq	null(%rip), %rbx
	jmp	.L366
.L215:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rdi
	salq	$4, %rax
	testl	%r13d, %r13d
	je	.L216
	movq	(%rdi,%rax), %rax
.L217:
	testq	%rax, %rax
	js	.L968
	movq	%rax, -2248(%rbp)
	movl	$10, -2276(%rbp)
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	-2264(%rbp), %eax
	testl	%r10d, %r10d
	movzbl	-2272(%rbp), %esi
	movl	%eax, -2324(%rbp)
	js	.L472
	jne	.L464
	movq	-2248(%rbp), %rax
	testq	%rax, %rax
	jne	.L473
	cmpl	$8, -2276(%rbp)
	jne	.L474
	cmpb	$0, -2264(%rbp)
	je	.L474
	movq	32(%rbp), %rbx
	movq	-2320(%rbp), %rdx
	movl	$32, %r9d
	movq	32(%rbp), %rsi
	addq	$996, %rbx
	subq	%rbx, %rdx
	movl	$48, 996(%rsi)
	movq	%rdx, %rcx
	sarq	$2, %rcx
	negq	%rcx
	testq	%rcx, %rcx
	cmovs	%rax, %rcx
	movl	%ecx, %r10d
	.p2align 4,,10
	.p2align 3
.L231:
	movl	-2240(%rbp), %eax
	testl	%eax, %eax
	jne	.L247
	sarq	$2, %rdx
	movq	%rdx, %r12
	movl	%r11d, %edx
	subl	%r12d, %edx
	subl	%ecx, %edx
	cmpq	$0, -2248(%rbp)
	je	.L248
	cmpl	$16, -2276(%rbp)
	jne	.L248
	testb	$1, -2324(%rbp)
	leal	-2(%rdx), %eax
	cmovne	%eax, %edx
.L248:
	movl	-2280(%rbp), %eax
	orl	%r13d, %eax
	orl	-2304(%rbp), %eax
	cmpl	$1, %eax
	adcl	$-1, %edx
	cmpb	$32, %r9b
	je	.L969
.L250:
	testl	%r13d, %r13d
	je	.L255
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L256
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L256
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rcx)
.L266:
	cmpl	$2147483647, %r15d
	je	.L151
	addl	$1, %r15d
.L259:
	cmpq	$0, -2248(%rbp)
	je	.L267
	cmpl	$16, -2276(%rbp)
	jne	.L267
	testb	$1, -2324(%rbp)
	je	.L267
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L268
	movq	32(%rax), %rsi
	movq	40(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L268
	leaq	4(%rsi), %rcx
	cmpl	$2147483647, %r15d
	movq	%rcx, 32(%rax)
	movl	$48, (%rsi)
	je	.L151
.L273:
	cmpq	%rdi, %rcx
	jnb	.L272
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	-2208(%rbp), %eax
	movl	%eax, (%rcx)
.L465:
	cmpl	$2147483646, %r15d
	je	.L151
	addl	$2, %r15d
.L267:
	addl	%r10d, %edx
	testl	%edx, %edx
	jle	.L274
	movq	-2200(%rbp), %rdi
	movslq	%edx, %r13
	movl	$48, %esi
	movq	%r13, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	movl	%r15d, %r9d
	addl	%r13d, %r9d
	movl	%r9d, %r15d
	jo	.L906
.L274:
	testl	%r15d, %r15d
	js	.L901
	movq	-2200(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	216(%rax), %r13
	movq	%r13, %rax
	subq	%rcx, %rax
	cmpq	%rax, -2256(%rbp)
	jbe	.L936
.L372:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-2200(%rbp), %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	jne	.L151
	movslq	%r15d, %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%r12, %rax
	addq	%rax, %rdx
	movslq	%eax, %rcx
	movl	%eax, %r15d
	cmpq	%r12, %rdx
	setb	%dl
	cmpq	%rcx, %rax
	movl	$1, %ecx
	movzbl	%dl, %edx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L906
	testl	%eax, %eax
	jns	.L281
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L211
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L211
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$37, (%rdx)
.L213:
	cmpl	$2147483647, %r15d
	je	.L151
	addl	$1, %r15d
	jmp	.L214
.L312:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2176(%rbp)
	jne	.L970
.L313:
	testb	$8, 64(%rbp)
	je	.L314
	movzbl	-2276(%rbp), %edx
	movzbl	13(%r14), %eax
	sall	$4, %edx
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L315:
	movq	-2200(%rbp), %rdi
	leaq	-2176(%rbp), %rdx
	movq	%r14, %rsi
	call	__GI___printf_fp
	testl	%eax, %eax
	jns	.L931
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$16, -2276(%rbp)
.L223:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testl	%r13d, %r13d
	je	.L226
	movq	(%rax), %rax
	movl	$0, -2304(%rbp)
	xorl	%r13d, %r13d
	movl	$0, -2280(%rbp)
	movq	%rax, -2248(%rbp)
	jmp	.L221
.L328:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rsi
	salq	$4, %rax
	movq	(%rsi,%rax), %rdx
	testq	%rdx, %rdx
	je	.L329
	testl	%r10d, %r10d
	movq	%rdx, %rax
	js	.L971
	movq	%rdx, -2248(%rbp)
	jne	.L972
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	movl	$120, -2208(%rbp)
	movl	$16, -2276(%rbp)
	movl	$32, %r9d
	movl	$1, -2324(%rbp)
	jmp	.L229
.L224:
	movl	$8, -2276(%rbp)
	jmp	.L223
.L332:
	movl	-2328(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L333
	movl	-2332(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L973
.L334:
	movl	-2332(%rbp), %r8d
	testl	%r8d, %r8d
	js	.L974
.L333:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rcx
	salq	$4, %rax
	testl	%r13d, %r13d
	movq	(%rcx,%rax), %rax
	je	.L335
	movslq	%r15d, %rdx
	movq	%rdx, (%rax)
	jmp	.L214
.L320:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2176(%rbp)
	jne	.L975
.L321:
	testb	$8, 64(%rbp)
	je	.L322
	movzbl	-2276(%rbp), %eax
	movzbl	13(%r14), %edx
	sall	$4, %eax
	andl	$-17, %edx
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L323:
	movq	-2200(%rbp), %rdi
	leaq	-2176(%rbp), %rdx
	movq	%r14, %rsi
	call	__printf_fphex
	jmp	.L933
.L222:
	movl	$10, -2276(%rbp)
	jmp	.L223
.L964:
	testl	%ebx, %ebx
	jle	.L354
	movq	-2200(%rbp), %rdi
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	addl	%r15d, %r12d
	movl	%r12d, %r15d
	jo	.L906
.L354:
	testl	%r15d, %r15d
	jns	.L353
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-2200(%rbp), %rdi
	movl	$37, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	je	.L151
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L483
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L395
.L398:
	movq	-2200(%rbp), %rdi
	movl	$35, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	je	.L151
	movzbl	12(%r14), %eax
	movl	$2, %ebx
.L394:
	testb	%al, %al
	jns	.L401
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L402
.L462:
	movq	32(%rdx), %rax
	cmpq	40(%rdx), %rax
	jnb	.L402
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	$39, (%rax)
.L404:
	addl	$1, %ebx
	movzbl	12(%r14), %eax
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L247:
	testl	%r13d, %r13d
	je	.L282
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L283
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L283
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$45, (%rsi)
.L293:
	cmpl	$2147483647, %r15d
	je	.L151
	addl	$1, %r15d
	subl	$1, %r11d
.L286:
	cmpq	$0, -2248(%rbp)
	je	.L294
	cmpl	$16, -2276(%rbp)
	jne	.L294
	testb	$1, -2324(%rbp)
	je	.L294
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L295
	movq	32(%rax), %rdi
	movq	40(%rax), %r8
	cmpq	%r8, %rdi
	jnb	.L295
	leaq	4(%rdi), %rsi
	cmpl	$2147483647, %r15d
	movq	%rsi, 32(%rax)
	movl	$48, (%rdi)
	je	.L151
.L300:
	cmpq	%r8, %rsi
	jnb	.L299
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	-2208(%rbp), %eax
	movl	%eax, (%rsi)
.L467:
	cmpl	$2147483646, %r15d
	je	.L151
	addl	$2, %r15d
	subl	$2, %r11d
.L294:
	sarq	$2, %rdx
	leal	(%rdx,%rcx), %r12d
	movq	%rdx, %r13
	subl	%r12d, %r11d
	testl	%r10d, %r10d
	movslq	%r11d, %r12
	jle	.L301
	movq	-2200(%rbp), %rdi
	movslq	%r10d, %rcx
	movl	$48, %esi
	movq	%rcx, %rdx
	movq	%rcx, -2208(%rbp)
	call	_IO_wpadn@PLT
	movq	-2208(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	movl	%r15d, %eax
	addl	%ecx, %eax
	movl	%eax, %r15d
	jo	.L906
	testl	%eax, %eax
	js	.L901
.L305:
	movq	-2200(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	216(%rax), %rcx
	movq	%rcx, %rax
	subq	%rdi, %rax
	cmpq	%rax, -2256(%rbp)
	jbe	.L976
.L306:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	-2200(%rbp), %rdi
	call	*56(%rcx)
	cmpq	%r13, %rax
	jne	.L151
	movslq	%r15d, %rax
	xorl	%edx, %edx
	addq	%r13, %rax
	js	.L308
	cmpq	%r13, %rax
	jb	.L308
.L307:
	movslq	%eax, %rcx
	movl	%eax, %r15d
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L906
	testl	%eax, %eax
	js	.L901
	testl	%r12d, %r12d
	jle	.L281
	movq	-2200(%rbp), %rdi
	movq	%r12, %rdx
	movl	$32, %esi
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L151
	addl	%r15d, %r12d
	movl	%r12d, %r15d
	jno	.L915
	.p2align 4,,10
	.p2align 3
.L906:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$75, %fs:(%rax)
	jmp	.L194
.L972:
	movl	$120, -2208(%rbp)
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	movl	$1, -2324(%rbp)
	movl	$16, -2276(%rbp)
	.p2align 4,,10
	.p2align 3
.L464:
	movslq	%r10d, %r12
	movl	$32, %r9d
.L229:
	cmpq	$0, 48(%rbp)
	leaq	_itowa_upper_digits(%rip), %rcx
	setne	%al
	andl	%eax, %esi
	cmpl	$88, -2208(%rbp)
	leaq	_itowa_lower_digits(%rip), %rax
	cmovne	%rax, %rcx
	movl	-2276(%rbp), %eax
	cmpl	$10, %eax
	je	.L477
	cmpl	$16, %eax
	je	.L478
	cmpl	$8, %eax
	je	.L977
	movl	-2276(%rbp), %edi
	movq	-2320(%rbp), %rbx
	movq	-2248(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%edx, %edx
	subq	$4, %rbx
	divq	%rdi
	movl	(%rcx,%rdx,4), %edx
	testq	%rax, %rax
	movl	%edx, (%rbx)
	jne	.L240
.L237:
	testb	%sil, %sil
	jne	.L461
.L241:
	cmpl	$10, -2276(%rbp)
	jne	.L242
	cmpb	$0, -2288(%rbp)
	je	.L242
	movq	-2320(%rbp), %rsi
	movq	%rbx, %rdi
	movb	%r9b, -2272(%rbp)
	movl	%r11d, -2264(%rbp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movzbl	-2272(%rbp), %r9d
	movl	-2264(%rbp), %r11d
	movq	%rax, %rbx
.L242:
	movq	-2320(%rbp), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%r12, %rax
	jl	.L486
	cmpq	$0, -2248(%rbp)
	jne	.L243
.L486:
	movq	%rdx, %rax
	movq	%r12, %rcx
	sarq	$2, %rax
	subq	%rax, %rcx
	movl	$0, %eax
	cmovs	%rax, %rcx
	movl	%ecx, %r10d
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L226:
	testl	%ebx, %ebx
	movl	(%rax), %eax
	je	.L227
	movzbl	%al, %eax
	movl	$0, -2304(%rbp)
	movl	$0, -2280(%rbp)
	movq	%rax, -2248(%rbp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L472:
	movl	$1, %r12d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L478:
	movq	-2248(%rbp), %rax
.L235:
	movq	-2320(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%rax, %rdx
	shrq	$4, %rax
	subq	$4, %rbx
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rbx)
	jne	.L238
	testb	%sil, %sil
	movl	$16, -2276(%rbp)
	je	.L242
.L461:
	movl	56(%rbp), %r8d
	movq	48(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-2320(%rbp), %rdx
	movq	32(%rbp), %rdi
	movb	%r9b, -2272(%rbp)
	movl	%r11d, -2264(%rbp)
	call	group_number
	movzbl	-2272(%rbp), %r9d
	movq	%rax, %rbx
	movl	-2264(%rbp), %r11d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	$8, -2276(%rbp)
	jne	.L486
	testb	$1, -2324(%rbp)
	je	.L486
	movq	-2320(%rbp), %rdx
	leaq	-4(%rbx), %rax
	movl	$0, %esi
	movl	$48, -4(%rbx)
	movl	$8, -2276(%rbp)
	movq	%rax, %rbx
	subq	%rax, %rdx
	movq	%rdx, %rcx
	sarq	$2, %rcx
	subq	%rcx, %r12
	movq	%r12, %rcx
	cmovs	%rsi, %rcx
	movl	%ecx, %r10d
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L282:
	movl	-2304(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L287
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L288
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L288
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$43, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L255:
	movl	-2304(%rbp), %eax
	testl	%eax, %eax
	je	.L260
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L261
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L261
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rcx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L977:
	movq	-2320(%rbp), %rbx
	movq	-2248(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rax, %rdx
	shrq	$3, %rax
	subq	$4, %rbx
	andl	$7, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rbx)
	jne	.L236
	testb	%sil, %sil
	je	.L242
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L477:
	movq	-2320(%rbp), %rbx
	movq	-2248(%rbp), %rdi
	movabsq	$-3689348814741910323, %r8
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rdi, %rax
	subq	$4, %rbx
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	testq	%rdx, %rdx
	movl	(%rcx,%rdi,4), %eax
	movq	%rdx, %rdi
	movl	%eax, (%rbx)
	jne	.L234
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L301:
	testl	%r15d, %r15d
	jns	.L305
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L368:
	cmpl	$-1, %r10d
	je	.L369
.L331:
	movslq	%r10d, %rsi
	movq	%rbx, %rdi
	movl	%r11d, -2208(%rbp)
	call	__wcsnlen@PLT
	movl	-2208(%rbp), %r11d
	movq	%rax, %r12
	subl	%eax, %r11d
.L366:
	testl	%r11d, %r11d
	js	.L978
	movl	-2240(%rbp), %edx
	testl	%edx, %edx
	jne	.L376
	testl	%r11d, %r11d
	je	.L377
	movq	-2200(%rbp), %rdi
	movslq	%r11d, %r13
	movl	$32, %esi
	movq	%r13, %rdx
	movl	%r11d, -2208(%rbp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	addl	%r15d, %r13d
	movl	-2208(%rbp), %r11d
	movl	%r13d, %r15d
	jo	.L906
.L377:
	testl	%r15d, %r15d
	jns	.L380
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L227:
	testl	%r12d, %r12d
	je	.L903
	movzwl	%ax, %eax
.L903:
	movq	%rax, -2248(%rbp)
	movl	$0, -2304(%rbp)
	movl	$0, -2280(%rbp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L376:
	testl	%r15d, %r15d
	js	.L371
.L380:
	movq	-2200(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	216(%rax), %r13
	movq	%r13, %rax
	subq	%rcx, %rax
	cmpq	%rax, -2256(%rbp)
	jbe	.L979
.L381:
	movl	%r11d, -2208(%rbp)
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-2200(%rbp), %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	movl	-2208(%rbp), %r11d
	jne	.L151
	movslq	%r15d, %rax
	xorl	%edx, %edx
	addq	%r12, %rax
	js	.L383
	cmpq	%r12, %rax
	jb	.L383
.L382:
	movslq	%eax, %rcx
	movl	%eax, %r15d
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L906
	testl	%eax, %eax
	js	.L901
	testl	%r11d, %r11d
	je	.L281
	movl	-2240(%rbp), %eax
	testl	%eax, %eax
	je	.L281
	movq	-2200(%rbp), %rdi
	movslq	%r11d, %rbx
	movl	$32, %esi
	movq	%rbx, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %rbx
	je	.L912
	jmp	.L151
.L899:
	movq	-2208(%rbp), %r14
.L425:
	cmpl	$-1, (%r14)
	je	.L433
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L434
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L434
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$46, (%rdx)
.L436:
	cmpl	$2147483647, %ebx
	je	.L151
	leaq	-2176(%rbp), %rdi
	movslq	(%r14), %rcx
	addl	$1, %ebx
	movabsq	$-3689348814741910323, %r8
	leaq	48(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%rcx, %rax
	subq	$4, %r12
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	leaq	_itowa_lower_digits(%rip), %rax
	testq	%rdx, %rdx
	movl	(%rax,%rcx,4), %esi
	movq	%rdx, %rcx
	movl	%esi, (%r12)
	jne	.L437
	leaq	48(%rdi), %r13
	cmpq	%r13, %r12
	jnb	.L433
	movq	%r14, -2208(%rbp)
	movq	%r13, %r14
	movq	-2200(%rbp), %r13
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L980:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L438
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L151
.L441:
	cmpl	$2147483647, %ebx
	je	.L151
	addl	$1, %ebx
	cmpq	%r14, %r12
	jnb	.L900
	movl	(%r12), %esi
.L442:
	movq	160(%r13), %rax
	addq	$4, %r12
	testq	%rax, %rax
	jne	.L980
.L438:
	movq	%r13, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L441
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L287:
	movl	-2280(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L286
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L291
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L291
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$32, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L260:
	movl	-2280(%rbp), %eax
	testl	%eax, %eax
	je	.L259
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L264
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L264
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rcx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L153:
	testq	%rbx, %rbx
	jne	.L460
.L907:
	movl	-2248(%rbp), %eax
	movl	%eax, %ebx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L473:
	xorl	%r12d, %r12d
	movl	$32, %r9d
	jmp	.L229
.L405:
	testb	$16, %al
	je	.L409
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L410
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L410
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L412
.L335:
	testl	%ebx, %ebx
	jne	.L981
	testl	%r12d, %r12d
	jne	.L337
	movl	%r15d, (%rax)
	jmp	.L214
.L216:
	movl	(%rdi,%rax), %edx
	testl	%ebx, %ebx
	movsbq	%dl, %rax
	jne	.L217
	movslq	%edx, %rax
	testl	%r12d, %r12d
	movswq	%dx, %rdx
	cmovne	%rdx, %rax
	jmp	.L217
.L314:
	andb	$-17, 13(%r14)
	jmp	.L315
.L322:
	andb	$-17, 13(%r14)
	jmp	.L323
.L969:
	testl	%edx, %edx
	jle	.L251
	movq	-2200(%rbp), %rdi
	movslq	%edx, %rcx
	movl	$32, %esi
	movq	%rcx, %rdx
	movq	%rcx, -2240(%rbp)
	movl	%r10d, -2264(%rbp)
	call	_IO_wpadn@PLT
	movq	-2240(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L151
	testl	%r15d, %r15d
	js	.L901
	movl	%r15d, %edx
	movl	-2264(%rbp), %r10d
	addl	%ecx, %edx
	movl	%edx, %r15d
	jo	.L906
.L251:
	testl	%r15d, %r15d
	js	.L901
	xorl	%edx, %edx
	jmp	.L250
.L950:
	movq	-2232(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	leaq	16(%rax), %r13
	jmp	.L149
.L961:
	testb	%al, %al
	movl	$1, %ebx
	jns	.L401
	jmp	.L462
.L978:
	testl	%r15d, %r15d
	js	.L371
	movq	-2200(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rsi
	movq	216(%rax), %r13
	movq	%r13, %rax
	subq	%rsi, %rax
	cmpq	%rax, -2256(%rbp)
	ja	.L372
.L936:
	call	_IO_vtable_check
	jmp	.L372
.L474:
	movq	-2320(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$32, %r9d
	jmp	.L231
.L900:
	movq	-2208(%rbp), %r14
.L433:
	movl	8(%r14), %esi
	testl	%esi, %esi
	je	.L443
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L444
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L444
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L151
.L446:
	cmpl	$2147483647, %ebx
	je	.L151
	addl	$1, %ebx
.L443:
	testl	%r15d, %r15d
	js	.L901
	movslq	%ebx, %rbx
.L912:
	addl	%r15d, %ebx
	movl	%ebx, %r15d
	jo	.L906
	testl	%ebx, %ebx
	jns	.L281
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%rbx, %rdi
	movl	%r11d, -2208(%rbp)
	call	__wcslen@PLT
	movl	-2208(%rbp), %r11d
	movq	%rax, %r12
	subl	%eax, %r11d
	jmp	.L366
.L968:
	negq	%rax
	movl	$10, -2276(%rbp)
	movl	$1, %r13d
	movq	%rax, -2248(%rbp)
	jmp	.L221
.L962:
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L418
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L418
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$48, (%rdx)
.L420:
	addl	$1, %ebx
	jmp	.L417
.L329:
	cmpl	$5, %r10d
	movl	$5, %eax
	leaq	.LC3(%rip), %rbx
	cmovl	%eax, %r10d
	jmp	.L331
.L952:
	testl	%r15d, %r15d
	js	.L901
	addl	%r15d, %ebx
	movl	%ebx, %r15d
	jno	.L915
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L981:
	movb	%r15b, (%rax)
	jmp	.L214
.L402:
	movq	-2200(%rbp), %rdi
	movl	$39, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L404
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-2200(%rbp), %rdi
	movl	$48, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L420
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-2200(%rbp), %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L360
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%rax, -2248(%rbp)
	xorl	%esi, %esi
	movl	$1, %r12d
	xorl	%r13d, %r13d
	leaq	_itowa_lower_digits(%rip), %rcx
	movl	$120, -2208(%rbp)
	movl	$1, -2324(%rbp)
	jmp	.L235
.L957:
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L181
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L182:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	andl	$-257, (%r10,%r14,4)
	jmp	.L173
.L976:
	movq	%rcx, -2208(%rbp)
	call	_IO_vtable_check
	movq	-2208(%rbp), %rcx
	jmp	.L306
.L256:
	movl	%r10d, -2264(%rbp)
	movl	%edx, -2240(%rbp)
	movl	$45, %esi
.L943:
	movq	-2200(%rbp), %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2264(%rbp), %r10d
	jne	.L266
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rcx, -2288(%rbp)
	movl	%r10d, -2272(%rbp)
	movl	$45, %esi
	movq	%rdx, -2264(%rbp)
	movl	%r11d, -2240(%rbp)
.L947:
	movq	-2200(%rbp), %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2264(%rbp), %rdx
	movl	-2272(%rbp), %r10d
	movq	-2288(%rbp), %rcx
	jne	.L293
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-2200(%rbp), %rdi
	movl	$37, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L213
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L973:
	movq	-2344(%rbp), %rdi
	call	__wcslen@PLT
	movq	-2344(%rbp), %rdi
	leaq	4(,%rax,4), %rsi
	call	__readonly_area
	movl	%eax, -2332(%rbp)
	jmp	.L334
.L268:
	movq	-2200(%rbp), %rdi
	movl	$48, %esi
	movl	%r10d, -2248(%rbp)
	movl	%edx, -2240(%rbp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r10d
	je	.L151
	cmpl	$2147483647, %r15d
	je	.L151
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	jne	.L982
.L272:
	movl	-2208(%rbp), %esi
	movq	-2200(%rbp), %rdi
	movl	%r10d, -2248(%rbp)
	movl	%edx, -2240(%rbp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r10d
	jne	.L465
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L979:
	movl	%r11d, -2208(%rbp)
	call	_IO_vtable_check
	movl	-2208(%rbp), %r11d
	jmp	.L381
.L337:
	movw	%r15w, (%rax)
	jmp	.L214
.L966:
	cmpq	$0, -64(%r9,%rax,8)
	je	.L191
	movslq	(%r12,%r14,4), %rax
	movl	%edx, -2256(%rbp)
	movq	%rcx, -2240(%rbp)
	movq	%r10, -2216(%rbp)
	movq	%r8, -2208(%rbp)
	addq	$30, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movq	%rdi, (%rcx,%rsi)
	movslq	(%r10,%r14,4), %rax
	movq	%r8, %rsi
	call	*-64(%r9,%rax,8)
	movq	-2208(%rbp), %r8
	movq	-2216(%rbp), %r10
	movq	-2240(%rbp), %rcx
	movl	-2256(%rbp), %edx
	jmp	.L173
.L261:
	movl	%r10d, -2264(%rbp)
	movl	%edx, -2240(%rbp)
	movl	$43, %esi
	jmp	.L943
.L288:
	movq	%rcx, -2288(%rbp)
	movl	%r10d, -2272(%rbp)
	movl	$43, %esi
	movq	%rdx, -2264(%rbp)
	movl	%r11d, -2240(%rbp)
	jmp	.L947
.L299:
	movl	-2208(%rbp), %esi
	movq	-2200(%rbp), %rdi
	movq	%rcx, -2272(%rbp)
	movl	%r10d, -2264(%rbp)
	movq	%rdx, -2248(%rbp)
	movl	%r11d, -2240(%rbp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2248(%rbp), %rdx
	movl	-2264(%rbp), %r10d
	movq	-2272(%rbp), %rcx
	jne	.L467
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L295:
	movq	-2200(%rbp), %rdi
	movl	$48, %esi
	movq	%rcx, -2272(%rbp)
	movl	%r10d, -2264(%rbp)
	movq	%rdx, -2248(%rbp)
	movl	%r11d, -2240(%rbp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2248(%rbp), %rdx
	movl	-2264(%rbp), %r10d
	movq	-2272(%rbp), %rcx
	je	.L151
	cmpl	$2147483647, %r15d
	je	.L151
	movq	-2200(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L299
	movq	32(%rax), %rsi
	movq	40(%rax), %r8
	jmp	.L300
.L970:
	movl	$7, 52(%r14)
	andb	$-2, 12(%r14)
	jmp	.L313
.L975:
	andb	$-2, 12(%r14)
	jmp	.L321
.L406:
	movq	-2200(%rbp), %rdi
	movl	$43, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L412
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L483:
	movl	$1, %ebx
	jmp	.L394
.L346:
	movq	-2200(%rbp), %rdi
	movl	%eax, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L360
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L422:
	movq	-2200(%rbp), %rdi
	movl	$73, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L424
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-2200(%rbp), %rdi
	movl	$45, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L416
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-2200(%rbp), %rdi
	movl	$46, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L436
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L982:
	movq	32(%rax), %rcx
	movq	40(%rax), %rdi
	jmp	.L273
.L264:
	movl	%r10d, -2264(%rbp)
	movl	%edx, -2240(%rbp)
	movl	$32, %esi
	jmp	.L943
.L291:
	movq	%rcx, -2288(%rbp)
	movl	%r10d, -2272(%rbp)
	movl	$32, %esi
	movq	%rdx, -2264(%rbp)
	movl	%r11d, -2240(%rbp)
	jmp	.L947
.L410:
	movq	-2200(%rbp), %rdi
	movl	$32, %esi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L412
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L444:
	movq	-2200(%rbp), %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L446
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L395:
	movq	32(%rdx), %rcx
	movq	40(%rdx), %rsi
	jmp	.L392
.L451:
	movl	$1, %eax
	jmp	.L450
.L189:
	movq	8(%r8), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r8)
	jmp	.L190
.L383:
	movl	$1, %edx
	jmp	.L382
.L974:
	leaq	.LC7(%rip), %rdi
	call	__GI___libc_fatal
.L184:
	movq	8(%r8), %rax
	leaq	15(%rax), %rsi
	andq	$-16, %rsi
	leaq	16(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L185
.L308:
	movl	$1, %edx
	jmp	.L307
.L181:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L182
.L164:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L171
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L172:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L173
.L186:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L187
.L171:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L172
.L174:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L175
.L178:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L179
	.size	printf_positional, .-printf_positional
	.p2align 4,,15
	.globl	__vfwprintf_internal
	.hidden	__vfwprintf_internal
	.type	__vfwprintf_internal, @function
__vfwprintf_internal:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$1304, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rsi, 8(%rsp)
	movl	$1, %esi
	movl	%ecx, 64(%rsp)
	movl	%fs:(%rax), %eax
	movl	%eax, 96(%rsp)
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1631
	movl	(%rbx), %eax
	testb	$8, %al
	jne	.L1681
	cmpq	$0, 8(%rsp)
	je	.L1682
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1631
	movl	(%rbx), %ebp
	movl	%ebp, %eax
	andl	$2, %eax
	movl	%eax, 28(%rsp)
	jne	.L1683
	movq	16(%r15), %rax
	movq	8(%rsp), %rdi
	movl	$37, %esi
	movdqu	(%r15), %xmm0
	movq	%rax, 248(%rsp)
	movups	%xmm0, 232(%rsp)
	call	__wcschrnul@PLT
	andl	$32768, %ebp
	movq	%rax, 72(%rsp)
	movq	%rax, 176(%rsp)
	jne	.L991
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	movl	%eax, 28(%rsp)
	je	.L992
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	256(%rsp), %rdi
	movq	%rbx, %rdx
#APP
# 1400 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	call	*%rax
	testl	$32768, (%rbx)
	jne	.L991
.L993:
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L994
#APP
# 1401 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L995
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L996:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L994:
	addl	$1, 4(%rdi)
.L991:
	movq	216(%rbx), %rbp
	leaq	__stop___libc_IO_vtables(%rip), %rdx
	leaq	__start___libc_IO_vtables(%rip), %rax
	movq	72(%rsp), %r12
	subq	8(%rsp), %r12
	movq	%rdx, %rcx
	movq	%rax, %rsi
	movq	%rax, 40(%rsp)
	subq	%rax, %rcx
	movq	%rbp, %rax
	subq	%rsi, %rax
	movq	%rcx, 32(%rsp)
	sarq	$2, %r12
	cmpq	%rax, %rcx
	jbe	.L1684
.L997:
	movq	%r12, %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%rbp)
	cmpq	%rax, %r12
	jne	.L1303
	movslq	%r12d, %rdx
	movq	%r12, %rax
	movl	%r12d, %ebp
	shrq	$63, %rax
	cmpq	%rdx, %r12
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1290
	testl	%r12d, %r12d
	js	.L998
	movq	176(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L998
	cmpq	$0, __printf_function_table(%rip)
	jne	.L1306
	cmpq	$0, __printf_modifier_table(%rip)
	jne	.L1306
	cmpq	$0, __printf_va_arg_table(%rip)
	jne	.L1306
	leaq	288(%rsp), %rdi
	leaq	1288(%rsp), %rsi
	movl	$0, 128(%rsp)
	movl	$0, 100(%rsp)
	movl	$0, 132(%rsp)
	movq	%rdi, 104(%rsp)
	movq	%rsi, 120(%rsp)
	movq	$-1, 112(%rsp)
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdx
	leaq	.L984(%rip), %rax
	movq	%rdx, 176(%rsp)
	leal	-32(%r14), %edx
	cmpl	$90, %edx
	ja	.L1004
	leaq	jump_table(%rip), %rax
	leaq	.L984(%rip), %rsi
	movzbl	(%rax,%rdx), %edx
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdx,4), %rax
	addq	%rsi, %rax
.L1004:
	movl	$0, 92(%rsp)
	xorl	%r13d, %r13d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	movl	$0, 48(%rsp)
	movl	$0, 16(%rsp)
	movl	$0, 68(%rsp)
	movl	$0, 88(%rsp)
	movq	$-1, %r10
	movl	$32, 56(%rsp)
	leaq	step4_jumps.12797(%rip), %rcx
	leaq	step3b_jumps.12796(%rip), %rsi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	104(%rsp), %rsi
	movl	96(%rsp), %edi
	movl	$1000, %edx
	movl	%r10d, 48(%rsp)
	call	__GI___strerror_r
	movl	48(%rsp), %r10d
	movq	%rax, %r12
	xorl	%r9d, %r9d
.L1225:
	testq	%r12, %r12
	je	.L1685
	testl	%r9d, %r9d
	jne	.L1256
	cmpl	$83, %r14d
	jne	.L1686
.L1256:
	cmpl	$-1, %r10d
	je	.L1257
.L1210:
	movslq	%r10d, %rsi
	movq	%r12, %rdi
	call	__wcsnlen@PLT
	movq	%rax, %r14
.L1255:
	subl	%eax, %r13d
	js	.L1687
	movl	16(%rsp), %edi
	testl	%edi, %edi
	jne	.L1262
	testl	%r13d, %r13d
	je	.L1262
	movslq	%r13d, %rcx
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 48(%rsp)
	call	_IO_wpadn@PLT
	movq	48(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1303
	addl	%r13d, %ebp
	js	.L1290
	cmpl	%r13d, %ebp
	jb	.L1290
	testl	%ebp, %ebp
	js	.L998
.L1262:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	40(%rsp), %rdx
	cmpq	%rdx, 32(%rsp)
	jbe	.L1295
.L1265:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	cmpq	%rax, %r14
	jne	.L1303
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r14, %rax
	js	.L1267
	cmpq	%r14, %rax
	jb	.L1267
.L1266:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1290
	testl	%eax, %eax
	js	.L998
	testl	%r13d, %r13d
	je	.L1072
	movl	16(%rsp), %esi
	testl	%esi, %esi
	je	.L1072
	.p2align 4,,10
	.p2align 3
.L1648:
	movslq	%r13d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L1303
	addl	%ebp, %r13d
	movl	%r13d, %ebp
	jno	.L1626
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L998:
	testl	$32768, (%rbx)
	je	.L1688
.L1277:
	movl	28(%rsp), %edx
	testl	%edx, %edx
	je	.L983
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	256(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 1690 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L983:
	addq	$1304, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1277
	movq	$0, 8(%rdi)
#APP
# 1689 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1279
	subl	$1, (%rdi)
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1683:
	movl	64(%rsp), %ecx
	movq	8(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	buffered_vfprintf
	movl	%eax, %ebp
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1063:
.L1065:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1066
	leaq	jump_table(%rip), %rax
	movl	$1, %r9d
	xorl	%r11d, %r11d
	movzbl	(%rax,%rdi), %eax
	leaq	.L984(%rip), %rdi
	movslq	(%rcx,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1226:
	testl	%r9d, %r9d
	je	.L1689
.L1227:
	movl	16(%rsp), %r9d
	subl	$1, %r13d
	testl	%r9d, %r9d
	je	.L1690
.L1239:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1242
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1243:
	movq	160(%rbx), %rax
	movl	(%rdx), %esi
	testq	%rax, %rax
	je	.L1244
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1244
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L1303
.L1247:
	cmpl	$2147483647, %ebp
	je	.L1303
	addl	$1, %ebp
	testl	%r13d, %r13d
	jle	.L1072
	movl	16(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L1072
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1303
	addl	%r13d, %ebp
	js	.L1290
	cmpl	%r13d, %ebp
	jnb	.L1626
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1068
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1068
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$37, (%rdx)
.L1071:
	cmpl	$2147483647, %ebp
	je	.L1303
	addl	$1, %ebp
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	176(%rsp), %rax
	movl	$37, %esi
	addl	$1, 100(%rsp)
	leaq	4(%rax), %r13
	movq	%r13, %rdi
	movq	%r13, 176(%rsp)
	call	__wcschrnul@PLT
	movq	216(%rbx), %r14
	movq	%rax, 176(%rsp)
	subq	%r13, %rax
	sarq	$2, %rax
	movq	%rax, %r12
	movq	%r14, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1691
.L1271:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%rax, %r12
	jne	.L1303
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r12, %rax
	js	.L1273
	cmpq	%r12, %rax
	jb	.L1273
.L1272:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1290
	testl	%eax, %eax
	js	.L998
	movq	176(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1275
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1088:
	movl	$10, %r11d
.L1089:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	je	.L1092
	cmpl	$47, %eax
	ja	.L1093
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1094:
	movq	(%rdx), %rax
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 68(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1073:
	testl	%r9d, %r9d
	jne	.L1692
	testl	%edx, %edx
	movl	(%r15), %eax
	je	.L1078
	cmpl	$47, %eax
	ja	.L1079
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1080:
	movsbq	(%rdx), %rax
.L1077:
	testq	%rax, %rax
	js	.L1693
	movq	%rax, 80(%rsp)
	xorl	%r9d, %r9d
	movl	$10, %r11d
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1196:
	testb	$1, 64(%rsp)
	jne	.L1334
	movl	%r11d, %eax
	andl	$1, %eax
.L1197:
	movzbl	88(%rsp), %edx
	movzbl	68(%rsp), %ecx
	addl	%r12d, %r12d
	sall	$2, %r9d
	orl	%r12d, %eax
	movl	$0, 220(%rsp)
	orl	%r9d, %eax
	movl	%r10d, 208(%rsp)
	movl	%r13d, 212(%rsp)
	movl	%r14d, 216(%rsp)
	movb	$4, 221(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	orl	%edx, %eax
	movzbl	16(%rsp), %edx
	orl	%ecx, %eax
	movzbl	48(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	sall	$7, %edx
	orl	%ecx, %eax
	orl	%edx, %eax
	testl	%r11d, %r11d
	movb	%al, 220(%rsp)
	movl	56(%rsp), %eax
	movl	%eax, 224(%rsp)
	je	.L1198
	testb	$8, 64(%rsp)
	je	.L1199
	movl	$20, %eax
	movb	%al, 221(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1200
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1201:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 192(%rsp)
.L1202:
	leaq	192(%rsp), %rax
	leaq	184(%rsp), %rdx
	leaq	208(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 184(%rsp)
	call	__printf_fphex
	testl	%eax, %eax
	jns	.L1613
	.p2align 4,,10
	.p2align 3
.L1303:
	testl	$32768, (%rbx)
	movl	$-1, %ebp
	jne	.L1277
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1186:
	testb	$1, 64(%rsp)
	jne	.L1333
	movl	%r11d, %eax
	andl	$1, %eax
.L1187:
	movzbl	88(%rsp), %edx
	movzbl	68(%rsp), %ecx
	addl	%r12d, %r12d
	sall	$2, %r9d
	orl	%r12d, %eax
	movl	$0, 220(%rsp)
	orl	%r9d, %eax
	movl	56(%rsp), %esi
	movl	%r10d, 208(%rsp)
	movl	%r13d, 212(%rsp)
	movl	%r14d, 216(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	orl	%edx, %eax
	movzbl	16(%rsp), %edx
	movl	%esi, 224(%rsp)
	orl	%ecx, %eax
	movzbl	48(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	sall	$7, %edx
	orl	%ecx, %eax
	orl	%edx, %eax
	movb	%al, 220(%rsp)
	movzbl	92(%rsp), %eax
	sall	$3, %eax
	orl	$4, %eax
	testl	%r11d, %r11d
	movb	%al, 221(%rsp)
	je	.L1188
	testb	$8, 64(%rsp)
	je	.L1189
	orl	$16, %eax
	movb	%al, 221(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1190
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1191:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 192(%rsp)
.L1192:
	leaq	192(%rsp), %rax
	leaq	184(%rsp), %rdx
	leaq	208(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 184(%rsp)
	call	__GI___printf_fp
	testl	%eax, %eax
	js	.L1303
.L1613:
	xorl	%edx, %edx
	addl	%eax, %ebp
	js	.L1289
	cmpl	%eax, %ebp
	jb	.L1289
.L1288:
	testl	%edx, %edx
	jne	.L1290
.L1626:
	testl	%ebp, %ebp
	jns	.L1072
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1091:
	movl	$16, %r11d
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1090:
	movl	$8, %r11d
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	176(%rsp), %rdi
	leaq	4(%rdi), %rax
	movl	4(%rdi), %edi
	movq	%rax, 176(%rsp)
	movq	%rax, 208(%rsp)
	subl	$48, %edi
	cmpl	$9, %edi
	jbe	.L1694
.L1026:
	movl	(%r15), %edi
	cmpl	$47, %edi
	ja	.L1032
	movl	%edi, %r13d
	addq	16(%r15), %r13
	addl	$8, %edi
	movl	%edi, (%r15)
.L1033:
	movl	0(%r13), %r13d
	testl	%r13d, %r13d
	js	.L1695
.L1034:
	movl	(%rax), %r14d
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L984
.L1617:
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step1_jumps.12792(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1024
	leaq	jump_table(%rip), %rax
	movl	$1, 92(%rsp)
	movzbl	(%rax,%rdi), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1061:
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1060
	leaq	jump_table(%rip), %rax
	movl	$1, %r9d
	movl	$1, %r11d
	movzbl	(%rax,%rdi), %eax
	leaq	.L984(%rip), %rdi
	movslq	(%rcx,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1058
	leaq	jump_table(%rip), %rax
	movl	$1, %r9d
	movzbl	(%rax,%rdi), %eax
	leaq	.L984(%rip), %rdi
	movslq	(%rsi,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdx
	leaq	.L984(%rip), %rax
	movq	%rdx, 176(%rsp)
	leal	-32(%r14), %edx
	cmpl	$90, %edx
	ja	.L1056
	leaq	jump_table(%rip), %rax
	leaq	.L984(%rip), %rdi
	xorl	%r12d, %r12d
	movzbl	(%rax,%rdx), %eax
	movl	$1, %edx
	movslq	(%rcx,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1054
	leaq	jump_table(%rip), %rax
	movzbl	(%rax,%rdi), %edi
	leaq	step3a_jumps.12794(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
.L1054:
	movl	$1, %r12d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 176(%rsp)
	cmpl	$42, %r14d
	je	.L1696
	leal	-48(%r14), %eax
	xorl	%r10d, %r10d
	cmpl	$9, %eax
	jbe	.L1697
.L1050:
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L984
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step2_jumps.12793(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1038:
	leaq	176(%rsp), %rdi
	movq	%rsi, 168(%rsp)
	movq	%rcx, 160(%rsp)
	movl	%r10d, 152(%rsp)
	movl	%r9d, 144(%rsp)
	movl	%r8d, 136(%rsp)
	movl	%edx, 80(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	80(%rsp), %edx
	movl	136(%rsp), %r8d
	movl	144(%rsp), %r9d
	movslq	152(%rsp), %r10
	movq	160(%rsp), %rcx
	movq	168(%rsp), %rsi
	je	.L1290
	movq	176(%rsp), %rax
	movl	(%rax), %r14d
	cmpl	$36, %r14d
	je	.L1037
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	jbe	.L1617
	.p2align 4,,10
	.p2align 3
.L984:
	testl	%r14d, %r14d
	jne	.L1037
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1251
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1252:
	movq	(%rdx), %r12
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1211:
	testb	$2, 64(%rsp)
	je	.L1212
	movl	128(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L1212
	movq	8(%rsp), %r14
	movl	%r9d, 48(%rsp)
	movl	%edx, 16(%rsp)
	movq	%r14, %rdi
	call	__wcslen@PLT
	leaq	4(,%rax,4), %rsi
	movq	%r14, %rdi
	call	__readonly_area
	testl	%eax, %eax
	movl	%eax, 128(%rsp)
	movl	16(%rsp), %edx
	movl	48(%rsp), %r9d
	jns	.L1212
	leaq	.LC7(%rip), %rdi
	call	__GI___libc_fatal
	.p2align 4,,10
	.p2align 3
.L1212:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	jne	.L1698
	testl	%edx, %edx
	je	.L1216
	cmpl	$47, %eax
	ja	.L1217
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1218:
	movq	(%rdx), %rax
	movb	%bpl, (%rax)
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1207
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1208:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1209
	movq	%rax, 80(%rsp)
	xorl	%r8d, %r8d
	movl	$1, 88(%rsp)
	xorl	%r9d, %r9d
	movl	$120, %r14d
	movl	$16, %r11d
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1015
	leaq	jump_table(%rip), %rax
	movl	$1, 88(%rsp)
	movzbl	(%rax,%rdi), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1019:
	cmpq	$-1, 112(%rsp)
	je	.L1699
.L1020:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1022
	leaq	jump_table(%rip), %rax
	movl	$1, %r8d
	movzbl	(%rax,%rdi), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1016:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	movl	$48, %eax
	cmovne	56(%rsp), %eax
	movl	%eax, 56(%rsp)
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L984
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1011
	leaq	jump_table(%rip), %rax
	movl	$1, 48(%rsp)
	movzbl	(%rax,%rdi), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L1700
	leaq	jump_table(%rip), %rdi
	movl	$1, 68(%rsp)
	movzbl	(%rdi,%rax), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	176(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	leaq	.L984(%rip), %rax
	movq	%rdi, 176(%rsp)
	leal	-32(%r14), %edi
	cmpl	$90, %edi
	ja	.L1013
	leaq	jump_table(%rip), %rax
	movzbl	(%rax,%rdi), %edi
	leaq	step0_jumps.12761(%rip), %rax
	movslq	(%rax,%rdi,4), %rax
	leaq	.L984(%rip), %rdi
	addq	%rdi, %rax
.L1013:
	movl	$1, 16(%rsp)
	movl	$32, 56(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1306:
	leaq	288(%rsp), %rax
	movl	$0, 128(%rsp)
	movl	$0, 100(%rsp)
	movq	$-1, 112(%rsp)
	movq	%rax, 104(%rsp)
.L1003:
	subq	$8, %rsp
	movl	%ebp, %r9d
	movq	%r15, %rcx
	movl	72(%rsp), %eax
	movq	%rbx, %rdi
	pushq	%rax
	pushq	%rdx
	pushq	136(%rsp)
	movl	128(%rsp), %eax
	pushq	%rax
	pushq	144(%rsp)
	pushq	120(%rsp)
	movl	156(%rsp), %eax
	pushq	%rax
	movl	192(%rsp), %edx
	movq	72(%rsp), %rsi
	leaq	296(%rsp), %r8
	call	printf_positional
	addq	$64, %rsp
	movl	%eax, %ebp
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1066:
	movl	$1, %r9d
	xorl	%r11d, %r11d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1092:
	testl	%edx, %edx
	jne	.L1701
	testl	%r12d, %r12d
	jne	.L1098
	cmpl	$47, %eax
	ja	.L1099
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1100:
	movl	(%rdx), %eax
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 68(%rsp)
	movq	%rax, 80(%rsp)
	.p2align 4,,10
	.p2align 3
.L1087:
	testl	%r10d, %r10d
	js	.L1325
	jne	.L1619
	cmpq	$0, 80(%rsp)
	jne	.L1326
	cmpl	$8, %r11d
	jne	.L1327
	movl	88(%rsp), %eax
	testl	%eax, %eax
	je	.L1327
	movq	104(%rsp), %rax
	movl	$48, 1284(%rsp)
	movl	$4, %r8d
	leaq	996(%rax), %r12
.L1105:
	movl	16(%rsp), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.L1122
	sarq	$2, %r8
	xorl	%r10d, %r10d
	movl	$32, 56(%rsp)
	subl	%r8d, %r13d
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	68(%rsp), %eax
	orl	%r9d, %eax
	orl	48(%rsp), %eax
	cmpl	$1, %eax
	adcl	$-1, %r13d
	cmpl	$32, 56(%rsp)
	je	.L1702
.L1125:
	testl	%r9d, %r9d
	je	.L1128
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1129
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1129
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$45, (%rdx)
.L1139:
	cmpl	$2147483647, %ebp
	je	.L1303
	addl	$1, %ebp
.L1132:
	cmpq	$0, 80(%rsp)
	je	.L1140
	cmpl	$16, %r11d
	jne	.L1140
	movl	88(%rsp), %eax
	testl	%eax, %eax
	je	.L1140
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1141
	movq	32(%rax), %rcx
	movq	40(%rax), %rsi
	cmpq	%rsi, %rcx
	jnb	.L1141
	leaq	4(%rcx), %rdx
	cmpl	$2147483647, %ebp
	movq	%rdx, 32(%rax)
	movl	$48, (%rcx)
	je	.L1303
.L1299:
	cmpq	%rsi, %rdx
	jnb	.L1145
	leaq	4(%rdx), %rcx
	cmpl	$-1, %r14d
	movq	%rcx, 32(%rax)
	movl	%r14d, (%rdx)
	je	.L1303
.L1148:
	cmpl	$2147483646, %ebp
	je	.L1303
	addl	$2, %ebp
.L1140:
	addl	%r10d, %r13d
	testl	%r13d, %r13d
	jle	.L1149
	movslq	%r13d, %r14
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r8, 16(%rsp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %r14
	jne	.L1303
	addl	%r13d, %ebp
	js	.L1290
	cmpl	%r13d, %ebp
	jb	.L1290
	testl	%ebp, %ebp
	movq	16(%rsp), %r8
	js	.L998
.L1149:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1703
.L1152:
	movq	%r8, %rdx
	movq	%r8, 16(%rsp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	movq	16(%rsp), %r8
	cmpq	%rax, %r8
	jne	.L1303
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r8, %rax
	js	.L1260
	cmpq	%r8, %rax
	jb	.L1260
.L1259:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1290
	testl	%eax, %eax
	jns	.L1072
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1681:
	orl	$32, %eax
	movl	%eax, (%rbx)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
.L1631:
	movl	$-1, %ebp
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1691:
	call	_IO_vtable_check
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1684:
	call	_IO_vtable_check
	jmp	.L997
.L1701:
	cmpl	$47, %eax
	ja	.L1096
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1097:
	movzbl	(%rdx), %eax
	movl	$0, 48(%rsp)
	movl	$0, 68(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1686:
	movl	16(%rsp), %r8d
	movl	%ebp, %r9d
	movl	%r13d, %ecx
	movl	%r10d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, %ebp
	jns	.L1072
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1043:
	leaq	208(%rsp), %rdi
	movq	%rsi, 160(%rsp)
	movq	%rcx, 152(%rsp)
	movl	%r9d, 144(%rsp)
	movl	%r8d, 136(%rsp)
	movl	%edx, 80(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	80(%rsp), %edx
	movl	136(%rsp), %r8d
	movl	144(%rsp), %r9d
	movq	152(%rsp), %rcx
	movq	160(%rsp), %rsi
	je	.L1622
	testl	%eax, %eax
	je	.L1047
	movq	208(%rsp), %rax
	cmpl	$36, (%rax)
	jne	.L1047
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	132(%rsp), %edx
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	$1, %r9d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1060:
	movl	$1, %r9d
	movl	$1, %r11d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	$1, 92(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1011:
	movl	$1, 48(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	$1, %r8d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	$1, 88(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1700:
	leaq	.L984(%rip), %rax
	movl	$1, 68(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	$1, %edx
	xorl	%r12d, %r12d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1689:
	movl	16(%rsp), %r10d
	subl	$1, %r13d
	testl	%r10d, %r10d
	jne	.L1228
	testl	%r13d, %r13d
	jle	.L1228
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1303
	addl	%r13d, %ebp
	js	.L1290
	cmpl	%r13d, %ebp
	jb	.L1290
	testl	%ebp, %ebp
	js	.L998
.L1228:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1231
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1232:
	movzbl	(%rdx), %edi
	call	__btowc
	movq	160(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1233
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L1233
	leaq	4(%rcx), %rsi
	cmpl	$-1, %eax
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	jne	.L1247
	jmp	.L1303
.L1692:
	movl	(%r15), %edx
	cmpl	$47, %edx
	ja	.L1075
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$8, %edx
	movl	%edx, (%r15)
.L1076:
	movq	(%rax), %rax
	jmp	.L1077
.L1698:
	cmpl	$47, %eax
	ja	.L1214
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1215:
	movq	(%rdx), %rax
	movslq	%ebp, %rdx
	movq	%rdx, (%rax)
	jmp	.L1072
.L1188:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1193
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1194:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 192(%rsp)
	jmp	.L1192
.L1198:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1203
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1204:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 192(%rsp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1695:
	negl	%r13d
	movl	$1, 16(%rsp)
	movl	$32, 56(%rsp)
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L995:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L996
	call	__lll_lock_wait_private
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1696:
	leaq	8(%rax), %rdi
	movl	8(%rax), %eax
	movq	%rdi, 176(%rsp)
	movq	%rdi, 208(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1043
.L1047:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1704
	movl	%eax, %edi
	addq	16(%r15), %rdi
	addl	$8, %eax
	movl	%eax, (%r15)
.L1049:
	movslq	(%rdi), %r10
	movq	$-1, %rax
	testl	%r10d, %r10d
	cmovs	%rax, %r10
	movq	176(%rsp), %rax
	movl	(%rax), %r14d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1694:
	leaq	208(%rsp), %rdi
	movq	%rsi, 168(%rsp)
	movq	%rcx, 160(%rsp)
	movl	%r10d, 152(%rsp)
	movl	%r9d, 144(%rsp)
	movl	%r8d, 136(%rsp)
	movl	%edx, 80(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	80(%rsp), %edx
	movl	136(%rsp), %r8d
	movl	144(%rsp), %r9d
	movslq	152(%rsp), %r10
	movq	160(%rsp), %rcx
	movq	168(%rsp), %rsi
	je	.L1622
	testl	%eax, %eax
	je	.L1031
	movq	208(%rsp), %rax
	cmpl	$36, (%rax)
	je	.L1037
.L1031:
	movq	176(%rsp), %rax
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1209:
	cmpl	$5, %r10d
	movl	$5, %eax
	leaq	.LC3(%rip), %r12
	cmovl	%eax, %r10d
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	8(%rax), %rax
	movl	96(%rax), %r8d
	movq	80(%rax), %rax
	movq	%rax, 112(%rsp)
	movzbl	(%rax), %eax
	movl	%r8d, 132(%rsp)
	testb	%al, %al
	sete	%dil
	cmpb	$127, %al
	sete	%al
	orb	%al, %dil
	jne	.L1337
	testl	%r8d, %r8d
	jne	.L1020
.L1337:
	movq	$0, 112(%rsp)
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1122:
	testl	%r9d, %r9d
	je	.L1156
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1157
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1157
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rdx)
.L1167:
	cmpl	$2147483647, %ebp
	je	.L1303
	addl	$1, %ebp
	subl	$1, %r13d
.L1160:
	cmpq	$0, 80(%rsp)
	je	.L1168
	cmpl	$16, %r11d
	jne	.L1168
	movl	88(%rsp), %eax
	testl	%eax, %eax
	je	.L1168
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1169
	movq	32(%rax), %rsi
	movq	40(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L1169
	leaq	4(%rsi), %rdx
	cmpl	$2147483647, %ebp
	movq	%rdx, 32(%rax)
	movl	$48, (%rsi)
	je	.L1303
.L1300:
	cmpq	%rdx, %rdi
	jbe	.L1173
	leaq	4(%rdx), %rsi
	cmpl	$-1, %r14d
	movq	%rsi, 32(%rax)
	movl	%r14d, (%rdx)
	je	.L1303
.L1176:
	cmpl	$2147483646, %ebp
	je	.L1303
	addl	$2, %ebp
	subl	$2, %r13d
.L1168:
	sarq	$2, %r8
	addl	%r8d, %ecx
	movq	%r8, %r14
	subl	%ecx, %r13d
	testl	%r10d, %r10d
	jle	.L1177
	movslq	%r10d, %rcx
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movl	%r10d, 48(%rsp)
	call	_IO_wpadn@PLT
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1303
	movl	48(%rsp), %r10d
	xorl	%eax, %eax
	addl	%r10d, %ebp
	js	.L1179
	cmpl	%ecx, %ebp
	jb	.L1179
.L1178:
	testl	%eax, %eax
	jne	.L1290
	testl	%ebp, %ebp
	js	.L998
.L1177:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	40(%rsp), %rdx
	cmpq	%rdx, 32(%rsp)
	jbe	.L1281
.L1180:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	cmpq	%rax, %r14
	jne	.L1303
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r14, %rax
	js	.L1182
	cmpq	%r14, %rax
	jb	.L1182
.L1181:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1290
	testl	%eax, %eax
	js	.L998
	testl	%r13d, %r13d
	jg	.L1648
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1326:
	xorl	%r10d, %r10d
.L1619:
	movl	$32, 56(%rsp)
.L1103:
	leaq	_itowa_lower_digits(%rip), %rcx
	leaq	_itowa_upper_digits(%rip), %rax
	cmpl	$88, %r14d
	cmove	%rax, %rcx
	cmpl	$10, %r11d
	je	.L1330
	cmpl	$16, %r11d
	je	.L1331
	cmpl	$8, %r11d
	je	.L1705
	movq	104(%rsp), %rdi
	movq	80(%rsp), %rax
	movslq	%r11d, %rsi
	leaq	1000(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L1113:
	xorl	%edx, %edx
	subq	$4, %r12
	divq	%rsi
	movl	(%rcx,%rdx,4), %edx
	testq	%rax, %rax
	movl	%edx, (%r12)
	jne	.L1113
.L1111:
	cmpq	$0, 112(%rsp)
	je	.L1114
	testl	%r8d, %r8d
	je	.L1114
.L1297:
	movq	104(%rsp), %rdi
	movl	132(%rsp), %r8d
	movq	%r12, %rsi
	movq	112(%rsp), %rcx
	movl	%r11d, 152(%rsp)
	movq	%r10, 144(%rsp)
	movl	%r9d, 136(%rsp)
	leaq	1000(%rdi), %rdx
	call	group_number
	movl	152(%rsp), %r11d
	movq	144(%rsp), %r10
	movq	%rax, %r12
	movl	136(%rsp), %r9d
.L1114:
	cmpl	$10, %r11d
	jne	.L1115
	movl	92(%rsp), %eax
	testl	%eax, %eax
	je	.L1115
	movq	104(%rsp), %rax
	movq	%r12, %rdi
	movl	%r11d, 144(%rsp)
	movq	%r10, 136(%rsp)
	movl	%r9d, 92(%rsp)
	leaq	1000(%rax), %rsi
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movl	144(%rsp), %r11d
	movq	136(%rsp), %r10
	movq	%rax, %r12
	movl	92(%rsp), %r9d
.L1115:
	movq	120(%rsp), %r8
	subq	%r12, %r8
	movq	%r8, %rax
	sarq	$2, %rax
	cmpq	%r10, %rax
	jl	.L1338
	cmpq	$0, 80(%rsp)
	jne	.L1116
.L1338:
	movq	%r8, %rax
	movq	%r10, %rcx
	sarq	$2, %rax
	subq	%rax, %rcx
	movl	$0, %eax
	cmovs	%rax, %rcx
	movl	%ecx, %r10d
.L1118:
	movl	16(%rsp), %esi
	testl	%esi, %esi
	jne	.L1122
	sarq	$2, %r8
	subl	%r8d, %r13d
	subl	%ecx, %r13d
	cmpq	$0, 80(%rsp)
	je	.L1123
.L1302:
	cmpl	$16, %r11d
	jne	.L1123
	movl	88(%rsp), %edx
	leal	-2(%r13), %eax
	testl	%edx, %edx
	cmovne	%eax, %r13d
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1325:
	movl	$1, %r10d
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1116:
	cmpl	$8, %r11d
	jne	.L1339
	movl	88(%rsp), %eax
	testl	%eax, %eax
	je	.L1339
	movq	120(%rsp), %r8
	leaq	-4(%r12), %rax
	movq	%r10, %rcx
	movl	$48, -4(%r12)
	movq	%rax, %r12
	subq	%rax, %r8
	movq	%r8, %rdx
	sarq	$2, %rdx
	subq	%rdx, %rcx
	movl	$0, %edx
	cmovs	%rdx, %rcx
	movl	%ecx, %r10d
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1156:
	movl	48(%rsp), %eax
	testl	%eax, %eax
	je	.L1161
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1162
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1162
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1128:
	movl	48(%rsp), %eax
	testl	%eax, %eax
	je	.L1133
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1134
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1134
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1705:
	movq	104(%rsp), %rsi
	movq	80(%rsp), %rax
	leaq	1000(%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	%rax, %rdx
	shrq	$3, %rax
	subq	$4, %r12
	andl	$7, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%r12)
	jne	.L1110
.L1112:
	cmpq	$0, 112(%rsp)
	je	.L1115
	testl	%r8d, %r8d
	je	.L1115
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	104(%rsp), %rsi
	movq	80(%rsp), %rax
	leaq	1000(%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	%rax, %rdx
	shrq	$4, %rax
	subq	$4, %r12
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%r12)
	jne	.L1109
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	104(%rsp), %rax
	movq	80(%rsp), %rsi
	movabsq	$-3689348814741910323, %rdi
	leaq	1000(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	%rsi, %rax
	subq	$4, %r12
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movl	(%rcx,%rsi,4), %eax
	movq	%rdx, %rsi
	movl	%eax, (%r12)
	jne	.L1108
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1685:
	cmpl	$-1, %r10d
	je	.L1254
	cmpl	$5, %r10d
	jg	.L1254
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC4(%rip), %r12
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L992:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 264(%rsp)
	movq	%rax, 256(%rsp)
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	%r8, %rax
	movl	16(%rsp), %edi
	movq	%r10, %rcx
	sarq	$2, %rax
	movl	$0, %edx
	subq	%rax, %rcx
	cmovs	%rdx, %rcx
	testl	%edi, %edi
	movl	%ecx, %r10d
	jne	.L1122
	subl	%eax, %r13d
	movq	%rax, %r8
	subl	%ecx, %r13d
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1161:
	movl	68(%rsp), %eax
	testl	%eax, %eax
	je	.L1160
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1165
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1165
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1133:
	movl	68(%rsp), %eax
	testl	%eax, %eax
	je	.L1132
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1137
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1137
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1690:
	testl	%r13d, %r13d
	jle	.L1239
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1303
	addl	%r13d, %ebp
	js	.L1290
	cmpl	%r13d, %ebp
	jb	.L1290
	testl	%ebp, %ebp
	jns	.L1239
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1702:
	testl	%r13d, %r13d
	jle	.L1332
	movslq	%r13d, %rcx
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movl	%r10d, 144(%rsp)
	movl	%r11d, 136(%rsp)
	movl	%r9d, 92(%rsp)
	movq	%r8, 56(%rsp)
	call	_IO_wpadn@PLT
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1303
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movq	56(%rsp), %r8
	movl	92(%rsp), %r9d
	movl	136(%rsp), %r11d
	movl	144(%rsp), %r10d
	js	.L1127
	cmpl	%ecx, %ebp
	jb	.L1127
.L1126:
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1290
	testl	%ebp, %ebp
	jns	.L1125
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1706
.L1258:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r14, %rax
	jne	.L1303
	movslq	%ebp, %rax
	xorl	%edx, %edx
	addq	%r14, %rax
	js	.L1260
	cmpq	%r14, %rax
	jnb	.L1259
.L1260:
	movl	$1, %edx
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	104(%rsp), %rax
	xorl	%r8d, %r8d
	leaq	1000(%rax), %r12
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	192(%rsp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	192(%rsp)
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%r12, %rdi
	call	__wcslen@PLT
	movq	%rax, %r14
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1693:
	negq	%rax
	movl	$1, %r9d
	movl	$10, %r11d
	movq	%rax, 80(%rsp)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1254:
	movl	$6, %eax
	movl	$6, %r14d
	leaq	null(%rip), %r12
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	%rbx, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L1247
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1216:
	testl	%r12d, %r12d
	jne	.L1219
	cmpl	$47, %eax
	ja	.L1220
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1221:
	movq	(%rdx), %rax
	movl	%ebp, (%rax)
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1078:
	testl	%r12d, %r12d
	jne	.L1081
	cmpl	$47, %eax
	ja	.L1082
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1083:
	movslq	(%rdx), %rax
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1279:
#APP
# 1689 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1277
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1689 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1332:
	xorl	%r13d, %r13d
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%rax, 16(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %rax
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1157:
	movl	%r10d, 68(%rsp)
	movl	%r11d, 56(%rsp)
	movl	$45, %esi
	movq	%r8, 48(%rsp)
	movq	%rcx, 16(%rsp)
.L1647:
	movq	%rbx, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	48(%rsp), %r8
	movl	56(%rsp), %r11d
	movl	68(%rsp), %r10d
	jne	.L1167
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1129:
	movl	%r10d, 56(%rsp)
	movl	%r11d, 48(%rsp)
	movl	$45, %esi
	movq	%r8, 16(%rsp)
.L1643:
	movq	%rbx, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	48(%rsp), %r11d
	movl	56(%rsp), %r10d
	jne	.L1139
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	%r8, 16(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %r8
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1697:
	leaq	176(%rsp), %rdi
	movq	%rsi, 160(%rsp)
	movq	%rcx, 152(%rsp)
	movl	%r9d, 144(%rsp)
	movl	%r8d, 136(%rsp)
	movl	%edx, 80(%rsp)
	call	read_int
	movslq	%eax, %r10
	cmpl	$-1, %r10d
	je	.L1290
	movq	176(%rsp), %rax
	movq	160(%rsp), %rsi
	movq	152(%rsp), %rcx
	movl	144(%rsp), %r9d
	movl	136(%rsp), %r8d
	movl	80(%rsp), %edx
	movl	(%rax), %r14d
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	$37, %esi
	movq	%rbx, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L1071
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1141:
	movl	$48, %esi
	movq	%rbx, %rdi
	movl	%r10d, 48(%rsp)
	movq	%r8, 16(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	48(%rsp), %r10d
	je	.L1303
	cmpl	$2147483647, %ebp
	je	.L1303
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.L1707
.L1145:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%r10d, 48(%rsp)
	movq	%r8, 16(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	48(%rsp), %r10d
	jne	.L1148
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	%rax, 48(%rsp)
	call	_IO_vtable_check
	movq	48(%rsp), %rax
	jmp	.L1265
.L1162:
	movl	%r10d, 68(%rsp)
	movl	%r11d, 56(%rsp)
	movl	$43, %esi
	movq	%r8, 48(%rsp)
	movq	%rcx, 16(%rsp)
	jmp	.L1647
.L1134:
	movl	%r10d, 56(%rsp)
	movl	%r11d, 48(%rsp)
	movl	$43, %esi
	movq	%r8, 16(%rsp)
	jmp	.L1643
.L1169:
	movl	$48, %esi
	movq	%rbx, %rdi
	movl	%r10d, 56(%rsp)
	movq	%r8, 48(%rsp)
	movq	%rcx, 16(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	48(%rsp), %r8
	movl	56(%rsp), %r10d
	je	.L1303
	cmpl	$2147483647, %ebp
	je	.L1303
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.L1708
.L1173:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%r10d, 56(%rsp)
	movq	%r8, 48(%rsp)
	movq	%rcx, 16(%rsp)
	call	__GI___woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	48(%rsp), %r8
	movl	56(%rsp), %r10d
	jne	.L1176
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1334:
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	jmp	.L1197
.L1333:
	xorl	%r11d, %r11d
	xorl	%eax, %eax
	jmp	.L1187
.L1233:
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	__GI___woverflow
	cmpl	$-1, %eax
	jne	.L1247
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	32(%rax), %rdx
	movq	40(%rax), %rsi
	jmp	.L1299
.L1165:
	movl	%r10d, 68(%rsp)
	movl	%r11d, 56(%rsp)
	movl	$32, %esi
	movq	%r8, 48(%rsp)
	movq	%rcx, 16(%rsp)
	jmp	.L1647
.L1137:
	movl	%r10d, 56(%rsp)
	movl	%r11d, 48(%rsp)
	movl	$32, %esi
	movq	%r8, 16(%rsp)
	jmp	.L1643
.L1708:
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	jmp	.L1300
.L1706:
	call	_IO_vtable_check
	jmp	.L1258
.L1273:
	movl	$1, %edx
	jmp	.L1272
.L1622:
	movq	__libc_errno@gottpoff(%rip), %rsi
	movl	%eax, %ebp
	movl	$75, %fs:(%rsi)
	jmp	.L998
.L1079:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1080
.L1217:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1218
.L1093:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1094
.L1242:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1243
.L1231:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1232
.L1082:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1083
.L1081:
	cmpl	$47, %eax
	ja	.L1084
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1085:
	movswq	(%rdx), %rax
	jmp	.L1077
.L1704:
	movq	8(%r15), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r15)
	jmp	.L1049
.L1127:
	movl	$1, %eax
	jmp	.L1126
.L1182:
	movl	$1, %edx
	jmp	.L1181
.L1099:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1100
.L1084:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1085
.L1207:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1208
.L1190:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1191
.L1075:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1076
.L1032:
	movq	8(%r15), %r13
	leaq	8(%r13), %rdi
	movq	%rdi, 8(%r15)
	jmp	.L1033
.L1289:
	movl	$1, %edx
	jmp	.L1288
.L1193:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1194
.L1096:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1097
.L1214:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1215
.L1179:
	movl	$1, %eax
	jmp	.L1178
.L1098:
	cmpl	$47, %eax
	ja	.L1101
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1102:
	movzwl	(%rdx), %eax
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 68(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L1087
.L1267:
	movl	$1, %edx
	jmp	.L1266
.L1251:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1252
.L1101:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1102
.L1203:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1204
.L1200:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1201
.L1220:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1221
.L1219:
	cmpl	$47, %eax
	ja	.L1222
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1223:
	movq	(%rdx), %rax
	movw	%bp, (%rax)
	jmp	.L1072
.L1222:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1223
	.size	__vfwprintf_internal, .-__vfwprintf_internal
	.p2align 4,,15
	.type	buffered_vfprintf, @function
buffered_vfprintf:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	$1, %esi
	movq	%rdi, %rbx
	movq	%rdx, %r13
	movl	$-1, %r12d
	subq	$33280, %rsp
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1709
	leaq	32(%rsp), %rdi
	movq	%r13, %rdx
	movl	%r14d, %ecx
	movq	%rbp, %rsi
	movq	%rbx, 488(%rsp)
	movl	$1, 224(%rsp)
	leaq	224(%rdi), %rax
	movl	$-72515580, 32(%rsp)
	movq	$0, 168(%rsp)
	movq	%rax, 192(%rsp)
	leaq	512(%rsp), %rax
	movq	%rax, 288(%rsp)
	movq	%rax, 280(%rsp)
	leaq	33280(%rsp), %rax
	movq	%rax, 296(%rsp)
	movl	116(%rbx), %eax
	movl	%eax, 148(%rsp)
	leaq	_IO_helper_jumps(%rip), %rax
	movq	%rax, 248(%rsp)
	call	__vfwprintf_internal
	movl	__libc_pthread_functions_init(%rip), %r13d
	movl	%eax, %r12d
	testl	%r13d, %r13d
	je	.L1711
	movq	184+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	movq	%rbx, %rdx
#APP
# 2298 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	call	*%rax
.L1712:
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L1713
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L1714
#APP
# 2299 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1715
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L1716:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L1714:
	addl	$1, 4(%rdi)
.L1713:
	movq	192(%rsp), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rbp
	subq	%rsi, %rbp
	sarq	$2, %rbp
	testl	%ebp, %ebp
	jle	.L1717
	movq	216(%rbx), %r14
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L1730
.L1718:
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpl	%eax, %ebp
	movl	$-1, %eax
	cmovne	%eax, %r12d
.L1717:
	testl	$32768, (%rbx)
	jne	.L1720
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1720
	movq	$0, 8(%rdi)
#APP
# 2319 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1722
	subl	$1, (%rdi)
.L1720:
	testl	%r13d, %r13d
	je	.L1709
	movq	192+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%esi, %esi
#APP
# 2320 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L1709:
	addq	$33280, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L1712
	.p2align 4,,10
	.p2align 3
.L1730:
	call	_IO_vtable_check
	movq	192(%rsp), %rax
	movq	24(%rax), %rsi
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1715:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L1716
	call	__lll_lock_wait_private
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1722:
#APP
# 2319 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1720
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 2319 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1720
	.size	buffered_vfprintf, .-buffered_vfprintf
	.section	.rodata
	.align 32
	.type	step4_jumps.13002, @object
	.size	step4_jumps.13002, 120
step4_jumps.13002:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L210-.L209
	.long	.L215-.L209
	.long	.L222-.L209
	.long	.L224-.L209
	.long	.L225-.L209
	.long	.L312-.L209
	.long	.L340-.L209
	.long	.L364-.L209
	.long	.L328-.L209
	.long	.L332-.L209
	.long	.L338-.L209
	.long	.L341-.L209
	.long	.L320-.L209
	.long	0
	.long	0
	.long	0
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12998, @object
	.size	__PRETTY_FUNCTION__.12998, 18
__PRETTY_FUNCTION__.12998:
	.string	"printf_positional"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12664, @object
	.size	__PRETTY_FUNCTION__.12664, 15
__PRETTY_FUNCTION__.12664:
	.string	"outstring_func"
	.section	.rodata
	.align 32
	.type	step3b_jumps.12796, @object
	.size	step3b_jumps.12796, 120
step3b_jumps.12796:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1059-.L984
	.long	0
	.long	0
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	.L1186-.L984
	.long	.L1226-.L984
	.long	.L1250-.L984
	.long	.L1206-.L984
	.long	.L1211-.L984
	.long	.L1224-.L984
	.long	.L1227-.L984
	.long	.L1196-.L984
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step4_jumps.12797, @object
	.size	step4_jumps.12797, 120
step4_jumps.12797:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	.L1186-.L984
	.long	.L1226-.L984
	.long	.L1250-.L984
	.long	.L1206-.L984
	.long	.L1211-.L984
	.long	.L1224-.L984
	.long	.L1227-.L984
	.long	.L1196-.L984
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step3a_jumps.12794, @object
	.size	step3a_jumps.12794, 120
step3a_jumps.12794:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1055-.L984
	.long	0
	.long	0
	.long	0
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1211-.L984
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step2_jumps.12793, @object
	.size	step2_jumps.12793, 120
step2_jumps.12793:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1053-.L984
	.long	.L1057-.L984
	.long	.L1059-.L984
	.long	.L1061-.L984
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	.L1186-.L984
	.long	.L1226-.L984
	.long	.L1250-.L984
	.long	.L1206-.L984
	.long	.L1211-.L984
	.long	.L1224-.L984
	.long	.L1227-.L984
	.long	.L1196-.L984
	.long	.L1063-.L984
	.long	.L1065-.L984
	.long	0
	.align 32
	.type	step1_jumps.12792, @object
	.size	step1_jumps.12792, 120
step1_jumps.12792:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1041-.L984
	.long	.L1053-.L984
	.long	.L1057-.L984
	.long	.L1059-.L984
	.long	.L1061-.L984
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	.L1186-.L984
	.long	.L1226-.L984
	.long	.L1250-.L984
	.long	.L1206-.L984
	.long	.L1211-.L984
	.long	.L1224-.L984
	.long	.L1227-.L984
	.long	.L1196-.L984
	.long	.L1063-.L984
	.long	.L1065-.L984
	.long	0
	.align 32
	.type	step0_jumps.12761, @object
	.size	step0_jumps.12761, 120
step0_jumps.12761:
	.long	0
	.long	.L1006-.L984
	.long	.L1010-.L984
	.long	.L1012-.L984
	.long	.L1014-.L984
	.long	.L1016-.L984
	.long	.L1019-.L984
	.long	.L1025-.L984
	.long	.L1038-.L984
	.long	.L1041-.L984
	.long	.L1053-.L984
	.long	.L1057-.L984
	.long	.L1059-.L984
	.long	.L1061-.L984
	.long	.L1067-.L984
	.long	.L1073-.L984
	.long	.L1088-.L984
	.long	.L1090-.L984
	.long	.L1091-.L984
	.long	.L1186-.L984
	.long	.L1226-.L984
	.long	.L1250-.L984
	.long	.L1206-.L984
	.long	.L1211-.L984
	.long	.L1224-.L984
	.long	.L1227-.L984
	.long	.L1196-.L984
	.long	.L1063-.L984
	.long	.L1065-.L984
	.long	.L1023-.L984
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_helper_jumps, @object
	.size	_IO_helper_jumps, 168
_IO_helper_jumps:
	.quad	0
	.quad	0
	.quad	__GI__IO_wdefault_finish
	.quad	_IO_helper_overflow
	.quad	_IO_default_underflow
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_wdefault_pbackfail
	.quad	__GI__IO_wdefault_xsputn
	.quad	__GI__IO_wdefault_xsgetn
	.quad	_IO_default_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	__GI__IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.zero	16
	.section	.rodata
	.align 32
	.type	jump_table, @object
	.size	jump_table, 91
jump_table:
	.byte	1
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	14
	.byte	0
	.byte	6
	.byte	0
	.byte	0
	.byte	7
	.byte	2
	.byte	0
	.byte	3
	.byte	9
	.byte	0
	.byte	5
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	25
	.byte	0
	.byte	19
	.byte	19
	.byte	19
	.byte	0
	.byte	29
	.byte	0
	.byte	0
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	21
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	20
	.byte	15
	.byte	19
	.byte	19
	.byte	19
	.byte	10
	.byte	15
	.byte	28
	.byte	0
	.byte	11
	.byte	24
	.byte	23
	.byte	17
	.byte	22
	.byte	12
	.byte	0
	.byte	21
	.byte	27
	.byte	16
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.section	.rodata.str4.16,"aMS",@progbits,4
	.align 16
	.type	null, @object
	.size	null, 28
null:
	.string	"("
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"u"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	")"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.hidden	__lll_lock_wait_private
	.hidden	__printf_modifier_table
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__readonly_area
	.hidden	_itowa_upper_digits
	.hidden	__printf_fphex
	.hidden	__btowc
	.hidden	__printf_va_arg_table
	.hidden	_itowa_lower_digits
	.hidden	__printf_function_table
	.hidden	__printf_arginfo_table
	.hidden	__parse_one_specwc
	.hidden	__mbsrtowcs
	.hidden	_IO_vtable_check
	.hidden	__wmemmove
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
