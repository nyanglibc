	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_itoa_word
	.hidden	_itoa_word
	.type	_itoa_word, @function
_itoa_word:
	leaq	__GI__itoa_lower_digits(%rip), %r8
	leaq	__GI__itoa_upper_digits(%rip), %rax
	testl	%ecx, %ecx
	cmovne	%rax, %r8
	cmpl	$10, %edx
	je	.L11
	cmpl	$16, %edx
	je	.L5
	cmpl	$8, %edx
	movl	%edx, %ecx
	je	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdi, %rax
	xorl	%edx, %edx
	subq	$1, %rsi
	divq	%rcx
	movq	%rax, %rdi
	movzbl	(%r8,%rdx), %eax
	testq	%rdi, %rdi
	movb	%al, (%rsi)
	jne	.L8
.L1:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rdi, %rax
	shrq	$4, %rdi
	subq	$1, %rsi
	andl	$15, %eax
	testq	%rdi, %rdi
	movzbl	(%r8,%rax), %eax
	movb	%al, (%rsi)
	jne	.L5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rdi, %rax
	shrq	$3, %rdi
	subq	$1, %rsi
	andl	$7, %eax
	testq	%rdi, %rdi
	movzbl	(%r8,%rax), %eax
	movb	%al, (%rsi)
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movabsq	$-3689348814741910323, %rcx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rdi, %rax
	subq	$1, %rsi
	mulq	%rcx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	testq	%rdx, %rdx
	movzbl	(%r8,%rdi), %eax
	movq	%rdx, %rdi
	movb	%al, (%rsi)
	jne	.L4
	movq	%rsi, %rax
	ret
	.size	_itoa_word, .-_itoa_word
	.p2align 4,,15
	.globl	_fitoa_word
	.hidden	_fitoa_word
	.type	_fitoa_word, @function
_fitoa_word:
	subq	$40, %rsp
	movq	%rsi, %r9
	leaq	32(%rsp), %rsi
	call	_itoa_word
	leaq	32(%rsp), %rdi
	cmpq	%rdi, %rax
	jnb	.L22
	movq	%rdi, %rsi
	xorl	%edx, %edx
	subq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	(%rax,%rdx), %ecx
	movb	%cl, (%r9,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L21
	leaq	33(%rsp), %rdx
	addq	$1, %rax
	addq	$40, %rsp
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	%r9, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r9, %rax
	addq	$40, %rsp
	ret
	.size	_fitoa_word, .-_fitoa_word
