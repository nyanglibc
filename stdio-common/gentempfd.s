	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/tmp"
	.text
	.p2align 4,,15
	.globl	__gen_tempfd
	.hidden	__gen_tempfd
	.type	__gen_tempfd, @function
__gen_tempfd:
	movl	%edi, %esi
	leaq	.LC0(%rip), %rdi
	movl	$384, %edx
	orl	$4259970, %esi
	xorl	%eax, %eax
	jmp	__open
	.size	__gen_tempfd, .-__gen_tempfd
	.hidden	__open
