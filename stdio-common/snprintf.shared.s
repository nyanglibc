	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___snprintf
	.hidden	__GI___snprintf
	.type	__GI___snprintf, @function
__GI___snprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L3
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L3:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vsnprintf_internal
	addq	$216, %rsp
	ret
	.size	__GI___snprintf, .-__GI___snprintf
	.globl	__snprintf
	.set	__snprintf,__GI___snprintf
	.weak	snprintf
	.set	snprintf,__snprintf
	.hidden	__vsnprintf_internal
