	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rdx
	movl	$2147483647, %r10d
	movl	$-1, %r9d
	movl	4(%rdx), %ecx
	movl	(%rdx), %eax
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	subl	$48, %eax
	cmpl	$9, %ecx
	ja	.L5
.L7:
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jg	.L14
	leal	(%rax,%rax,4), %eax
	movl	%r10d, %r8d
	subl	%ecx, %r8d
	addl	%eax, %eax
	cmpl	%eax, %r8d
	jl	.L14
	addl	%ecx, %eax
.L3:
	movq	%rsi, %rdx
	movl	4(%rdx), %ecx
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L7
.L5:
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	12(%rdx), %eax
	leaq	12(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	%r9d, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	movq	%rsi, (%rdi)
	ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	char_buffer_add_slow, @function
char_buffer_add_slow:
	cmpq	$0, (%rdi)
	je	.L20
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	subq	16(%rdi), %rax
	leaq	16(%rdi), %rdi
	movq	%rax, %r12
	call	__GI___libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L23
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	andq	$-4, %rdx
	addq	%rax, %rdx
	addq	%r12, %rax
	movq	%rdx, 8(%rbx)
	leaq	4(%rax), %rdx
	movq	%rdx, (%rbx)
	movl	%ebp, (%rax)
.L16:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	rep ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	jmp	.L16
	.size	char_buffer_add_slow, .-char_buffer_add_slow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vfscanf-internal.c"
.LC1:
	.string	"n <= MB_LEN_MAX"
.LC2:
	.string	"to_inpunct"
	.text
	.p2align 4,,15
	.globl	__vfwscanf_internal
	.hidden	__vfwscanf_internal
	.type	__vfwscanf_internal, @function
__vfwscanf_internal:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-1104(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %r14
	subq	$1464, %rsp
	movq	%rax, -1384(%rbp)
	addq	$32, %rax
	movq	%rsi, -1344(%rbp)
	movq	%rax, -1088(%rbp)
	movl	$1, %esi
	movdqu	(%rdx), %xmm0
	movq	%rdx, -1440(%rbp)
	movl	%ecx, -1444(%rbp)
	movups	%xmm0, -1320(%rbp)
	movq	16(%rdx), %rax
	movq	$1024, -1080(%rbp)
	movq	%rax, -1304(%rbp)
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1012
	movl	(%r14), %eax
	testb	$4, %al
	jne	.L1785
	testq	%rbx, %rbx
	je	.L1786
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rdi
	movq	%fs:(%rdi), %rdx
	movq	8(%rdx), %rdx
	movl	96(%rdx), %edi
	movl	88(%rdx), %esi
	movl	%edi, -1360(%rbp)
	movl	__libc_pthread_functions_init(%rip), %edi
	movl	%esi, -1448(%rbp)
	testl	%edi, %edi
	movl	%edi, -1368(%rbp)
	je	.L28
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	-1296(%rbp), %rdi
	movq	%r14, %rdx
#APP
# 372 "vfscanf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	call	*%rax
	movl	(%r14), %eax
.L29:
	andl	$32768, %eax
	je	.L1787
.L30:
	movq	-1344(%rbp), %rax
	xorl	%r13d, %r13d
	xorl	%r8d, %r8d
	movl	%r13d, %r12d
	movl	$0, -1364(%rbp)
	movq	$0, -1400(%rbp)
	movq	$0, -1376(%rbp)
	movq	$0, -1432(%rbp)
	xorl	%r15d, %r15d
	movq	$0, -1416(%rbp)
	movq	$0, -1424(%rbp)
	movq	%r14, %r13
	movl	$0, -1356(%rbp)
.L34:
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	je	.L1788
	leaq	4(%rax), %rbx
	cmpl	$37, %r14d
	movq	%rbx, -1344(%rbp)
	je	.L35
	movl	%r14d, %edi
	movl	%r8d, -1352(%rbp)
	call	__GI_iswspace
	testl	%eax, %eax
	jne	.L1013
	cmpl	$-1, %r12d
	movl	-1352(%rbp), %r8d
	je	.L1789
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L39
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L39
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L41:
	cmpl	$-1, %r12d
	je	.L1693
	addq	$1, %r15
	testl	%r8d, %r8d
	jne	.L1790
.L43:
	cmpl	%r12d, %r14d
	jne	.L50
	movq	-1344(%rbp), %rax
	xorl	%r8d, %r8d
	movl	%r14d, %r12d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L1790:
	movl	%r12d, %edi
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L43
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L45
.L1791:
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L45
	movl	(%rdx), %r12d
	leaq	4(%rdx), %rcx
	movq	%rcx, (%rax)
	cmpl	$-1, %r12d
	je	.L48
.L1792:
	movl	%r12d, %edi
	addq	$1, %r15
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L43
	movq	160(%r13), %rax
	testq	%rax, %rax
	jne	.L1791
.L45:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	cmpl	$-1, %r12d
	jne	.L1792
	.p2align 4,,10
	.p2align 3
.L48:
	movl	-1356(%rbp), %eax
	movq	%r13, %r14
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	%r12d, %ebx
	movl	%r12d, -1356(%rbp)
	.p2align 4,,10
	.p2align 3
.L44:
	testl	$32768, (%r14)
	jne	.L955
	movq	136(%r14), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L955
	movq	$0, 8(%rdi)
#APP
# 3030 "vfscanf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L957
	subl	$1, (%rdi)
.L955:
	movl	-1368(%rbp), %edx
	testl	%edx, %edx
	je	.L958
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	-1296(%rbp), %rdi
	xorl	%esi, %esi
#APP
# 3030 "vfscanf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L958:
	movq	-1384(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L959
	call	free@PLT
.L959:
	cmpl	$-1, -1356(%rbp)
	je	.L1793
	cmpq	$0, -1376(%rbp)
	jne	.L1794
.L24:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-1080(%rbp), %rcx
	movq	-1088(%rbp), %rdx
	movl	4(%rax), %eax
	movl	$0, -1408(%rbp)
	andq	$-4, %rcx
	movq	%rdx, -1104(%rbp)
	addq	%rcx, %rdx
	movq	%rdx, -1096(%rbp)
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	jbe	.L1795
.L51:
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L57:
	leal	-39(%rax), %edx
	cmpl	$34, %edx
	jbe	.L1796
.L58:
	testb	%cl, %cl
	jne	.L1797
.L60:
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L61
	movq	-1344(%rbp), %rcx
	movl	$-1, %ebx
	movl	(%rcx), %eax
	movl	%eax, -1352(%rbp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L1796:
	movabsq	$17179869193, %rsi
	btq	%rdx, %rsi
	jnc	.L58
	addq	$4, %rbx
	cmpl	$42, %eax
	je	.L54
	cmpl	$73, %eax
	je	.L55
	cmpl	$39, %eax
	jne	.L53
	movl	-1360(%rbp), %r10d
	movl	%r14d, %eax
	orb	$-128, %al
	testl	%r10d, %r10d
	cmovne	%eax, %r14d
.L53:
	movl	(%rbx), %eax
	movl	$1, %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L55:
	orl	$1024, %r14d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	orl	$8, %r14d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	%rbx, %rax
	movl	$1, %r8d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	136(%r14), %rdi
	movq	%fs:16, %rbx
	cmpq	%rbx, 8(%rdi)
	je	.L31
#APP
# 372 "vfscanf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L32
	movl	%edx, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L33:
	movq	136(%r14), %rdi
	movq	%rbx, 8(%rdi)
.L31:
	addl	$1, 4(%rdi)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L1795:
	leaq	-1344(%rbp), %rdi
	movl	%r8d, -1392(%rbp)
	xorl	%r14d, %r14d
	call	read_int
	movq	-1344(%rbp), %rcx
	movl	%eax, %ebx
	movl	-1392(%rbp), %r8d
	movl	(%rcx), %eax
	cmpl	$36, %eax
	movl	%eax, -1352(%rbp)
	je	.L1798
.L52:
	testl	%ebx, %ebx
	movl	$-1, %eax
	cmove	%eax, %ebx
.L62:
	movl	-1352(%rbp), %eax
	leaq	4(%rcx), %rdx
	movq	%rdx, -1344(%rbp)
	subl	$76, %eax
	cmpl	$46, %eax
	ja	.L63
	leaq	.L65(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L65:
	.long	.L64-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L66-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L67-.L65
	.long	.L63-.L65
	.long	.L68-.L65
	.long	.L63-.L65
	.long	.L69-.L65
	.long	.L70-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L64-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L68-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L68-.L65
	.text
	.p2align 4,,10
	.p2align 3
.L68:
	movl	4(%rcx), %eax
	orl	$1, %r14d
	movl	%eax, -1352(%rbp)
.L72:
	movl	-1352(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1799
	addq	$4, %rdx
	testl	%r8d, %r8d
	movq	%rdx, -1344(%rbp)
	jne	.L78
	movl	-1352(%rbp), %eax
	subl	$67, %eax
	cmpl	$43, %eax
	jbe	.L1800
.L78:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %esi
	movq	%rax, -1392(%rbp)
	movl	$0, %fs:(%rax)
	movl	%r12d, %eax
	movl	%ebx, %r12d
	movl	%eax, %ebx
	movl	%esi, -1456(%rbp)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L1803:
	movl	%ebx, %edi
	addq	$1, %r15
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L1801
.L87:
	cmpl	$-1, %ebx
	je	.L1802
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L82
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L82
	leaq	4(%rdx), %rcx
	movl	(%rdx), %ebx
	movq	%rcx, (%rax)
.L84:
	cmpl	$-1, %ebx
	jne	.L1803
	movq	-1392(%rbp), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	movl	%eax, -1364(%rbp)
	je	.L1804
.L1018:
	movl	$-1, %ebx
	movl	%ebx, %edi
	call	__GI_iswspace
	testl	%eax, %eax
	jne	.L87
.L1801:
	movl	%ebx, %edi
	movl	%eax, -1464(%rbp)
	movl	%r12d, %ebx
	movq	-1392(%rbp), %rax
	movl	%edi, %r12d
	movl	-1456(%rbp), %edi
	cmpl	$-1, %r12d
	movl	%edi, %fs:(%rax)
	jne	.L1805
	movl	-1352(%rbp), %eax
	subl	$37, %eax
	cmpl	$83, %eax
	ja	.L966
	leaq	.L967(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L967:
	.long	.L109-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L1722-.L967
	.long	.L966-.L967
	.long	.L93-.L967
	.long	.L966-.L967
	.long	.L1722-.L967
	.long	.L1722-.L967
	.long	.L1722-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L968-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L1723-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L96-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L1722-.L967
	.long	.L966-.L967
	.long	.L97-.L967
	.long	.L1723-.L967
	.long	.L1722-.L967
	.long	.L1722-.L967
	.long	.L1722-.L967
	.long	.L966-.L967
	.long	.L99-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L100-.L967
	.long	.L1723-.L967
	.long	.L102-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L103-.L967
	.long	.L966-.L967
	.long	.L1723-.L967
	.long	.L966-.L967
	.long	.L966-.L967
	.long	.L1723-.L967
	.text
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	%r13, %r14
.L38:
	movl	-1356(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L1789:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %esi
	movq	%r13, %r14
	movl	%esi, %fs:(%rax)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %ebx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L1802:
	movq	-1392(%rbp), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	jne	.L1018
.L1804:
	movl	-1356(%rbp), %ebx
	movq	%r13, %r14
	testl	%ebx, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	-1344(%rbp), %rdi
	movl	%r8d, -1392(%rbp)
	call	read_int
	movq	-1344(%rbp), %rcx
	movl	%eax, %ebx
	movl	-1392(%rbp), %r8d
	movl	(%rcx), %eax
	movl	%eax, -1352(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L1805:
	leaq	-1(%r15), %rax
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%rax, -1456(%rbp)
	call	__GI__IO_sputbackwc
	movl	-1352(%rbp), %eax
	subl	$37, %eax
	cmpl	$83, %eax
	ja	.L89
	leaq	.L91(%rip), %rcx
	movl	-1464(%rbp), %r8d
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L91:
	.long	.L90-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1020-.L91
	.long	.L89-.L91
	.long	.L1021-.L91
	.long	.L89-.L91
	.long	.L1020-.L91
	.long	.L1020-.L91
	.long	.L1020-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L94-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1022-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1023-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1020-.L91
	.long	.L89-.L91
	.long	.L1024-.L91
	.long	.L1025-.L91
	.long	.L1020-.L91
	.long	.L1020-.L91
	.long	.L1020-.L91
	.long	.L89-.L91
	.long	.L1026-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1027-.L91
	.long	.L101-.L91
	.long	.L1028-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1029-.L91
	.long	.L89-.L91
	.long	.L1030-.L91
	.long	.L89-.L91
	.long	.L89-.L91
	.long	.L1022-.L91
	.text
	.p2align 4,,10
	.p2align 3
.L70:
	movl	4(%rcx), %eax
	cmpl	$108, %eax
	movl	%eax, -1352(%rbp)
	je	.L76
	orl	$8192, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L69:
	movl	4(%rcx), %eax
	cmpl	$108, %eax
	movl	%eax, -1352(%rbp)
	je	.L1806
	orl	$1, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L67:
	movl	4(%rcx), %eax
	cmpl	$104, %eax
	movl	%eax, -1352(%rbp)
	je	.L1807
	orl	$4, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L66:
	movl	4(%rcx), %eax
	movl	%eax, %ecx
	andl	$-9, %ecx
	cmpl	$83, %ecx
	je	.L1088
	cmpl	$115, %eax
	jne	.L74
.L1088:
	testb	$2, -1444(%rbp)
	je	.L1808
.L74:
	movq	%rdx, -1344(%rbp)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L64:
	movl	4(%rcx), %eax
	orl	$3, %r14d
	movl	%eax, -1352(%rbp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rcx, -1344(%rbp)
	movq	%rcx, %rdx
	jmp	.L72
.L1021:
	movq	-1456(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	$-1, %ebx
	movl	$1, %eax
	movl	%r14d, %r8d
	cmove	%eax, %ebx
	andl	$8, %r8d
	jne	.L218
	testl	$8448, %r14d
	je	.L219
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L220
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L221
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L222:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L223:
	cmpq	$0, -1376(%rbp)
	je	.L1809
	cmpl	$1024, %ebx
	movl	$1024, %eax
	movl	%r8d, -1392(%rbp)
	cmovle	%ebx, %eax
	movslq	%eax, %rcx
	leaq	0(,%rcx,4), %rdi
	movq	%rcx, -1352(%rbp)
	call	malloc@PLT
	movq	-1376(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, -1432(%rbp)
	movq	%rax, (%rsi)
	je	.L233
	movq	-1424(%rbp), %rax
	movq	-1352(%rbp), %rcx
	movl	-1392(%rbp), %r8d
	testq	%rax, %rax
	je	.L234
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L234
.L235:
	movq	%rcx, -1400(%rbp)
	movq	-1376(%rbp), %rdi
	movq	-1424(%rbp), %rcx
	movq	%rdx, (%rcx)
	movq	%rdi, 16(%rcx,%rax,8)
.L218:
	cmpl	$-1, %r12d
	je	.L1810
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L250
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L250
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L252:
	cmpl	$-1, %r12d
	je	.L1697
	leaq	1(%r15), %rax
	testl	%r8d, %r8d
	movq	%rax, -1352(%rbp)
	jne	.L253
	movl	%r14d, %edx
	movq	%rax, %r15
	movl	%ebx, %r14d
	andl	$8448, %edx
	movslq	%ebx, %rax
	movq	%r13, %rbx
	movl	%edx, %r13d
	movq	%rax, -1456(%rbp)
	movl	%r8d, -1392(%rbp)
	testl	%r13d, %r13d
	movq	-1432(%rbp), %rax
	je	.L255
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	-1376(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-1400(%rbp), %rsi
	leaq	0(,%rsi,4), %rcx
	leaq	(%rdi,%rcx), %rsi
	cmpq	%rsi, %rax
	je	.L1811
.L255:
	subl	$1, %r14d
	leaq	4(%rax), %rcx
	movl	%r12d, (%rax)
	testl	%r14d, %r14d
	jle	.L1812
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L258
	movq	(%rax), %rsi
	cmpq	8(%rax), %rsi
	jnb	.L258
	leaq	4(%rsi), %rdi
	movl	(%rsi), %r12d
	movq	%rdi, (%rax)
.L260:
	cmpl	$-1, %r12d
	je	.L261
	addq	$1, %r15
	testl	%r13d, %r13d
	movq	%rcx, %rax
	je	.L255
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1800:
	movabsq	$8800404766721, %rdx
	btq	%rax, %rdx
	jnc	.L78
	leaq	.L106(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L106:
	.long	.L93-.L106
	.long	.L105-.L106
	.long	.L92-.L106
	.long	.L92-.L106
	.long	.L92-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L107-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L1031-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L96-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L92-.L106
	.long	.L105-.L106
	.long	.L97-.L106
	.long	.L98-.L106
	.long	.L92-.L106
	.long	.L92-.L106
	.long	.L92-.L106
	.long	.L105-.L106
	.long	.L99-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L105-.L106
	.long	.L100-.L106
	.text
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L28:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rdx
	movq	%r14, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L1793:
	cmpq	$0, -1424(%rbp)
	movq	-1424(%rbp), %r13
	je	.L24
.L961:
	xorl	%r12d, %r12d
	cmpq	$0, 0(%r13)
	je	.L965
	.p2align 4,,10
	.p2align 3
.L963:
	movq	16(%r13,%r12,8), %rax
	movq	(%rax), %rdi
	call	free@PLT
	movq	16(%r13,%r12,8), %rax
	addq	$1, %r12
	cmpq	%r12, 0(%r13)
	movq	$0, (%rax)
	ja	.L963
.L965:
	movq	8(%r13), %r13
	testq	%r13, %r13
	jne	.L961
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L1798:
	movl	%ebx, -1408(%rbp)
	leaq	4(%rcx), %rbx
	movl	4(%rcx), %eax
	movq	%rbx, -1344(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L1799:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L1794:
	movq	-1376(%rbp), %r15
	movq	(%r15), %rdi
	call	free@PLT
	movq	$0, (%r15)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L50:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	call	__GI__IO_sputbackwc
	jmp	.L44
.L1788:
	testl	%r8d, %r8d
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	movl	%r12d, %r13d
	je	.L44
	movl	-1364(%rbp), %r12d
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L948:
	movq	160(%r14), %rax
	testq	%rax, %rax
	je	.L950
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L950
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r13d
	movq	%rcx, (%rax)
.L952:
	cmpl	$-1, %r13d
	jne	.L949
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r12d
.L949:
	movl	%r13d, %edi
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L1814
.L953:
	cmpl	$-1, %r13d
	jne	.L948
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r12d, %fs:(%rax)
	jmp	.L949
.L1031:
	movl	$16, %edx
.L95:
	cmpl	$-1, %r12d
	je	.L1815
.L377:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L380
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L380
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L382:
	cmpl	$-1, %r12d
	je	.L1700
	leaq	1(%r15), %rax
	movq	%rax, -1352(%rbp)
	leal	-43(%r12), %eax
	andl	$-3, %eax
	movl	$1, %eax
	jne	.L384
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	je	.L1816
	leaq	4(%rax), %rcx
	movq	%rcx, -1104(%rbp)
	movl	%r12d, (%rax)
.L386:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L388
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L388
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L390:
	testl	%ebx, %ebx
	setne	%al
	cmpl	$-1, %r12d
	je	.L391
	leaq	2(%r15), %rcx
	movq	%rcx, -1352(%rbp)
.L384:
	cmpl	$48, %r12d
	jne	.L392
	testb	%al, %al
	je	.L392
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	je	.L1817
	leaq	4(%rax), %rcx
	movq	%rcx, -1104(%rbp)
	movl	$48, (%rax)
.L395:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L976
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L976
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L397:
	cmpl	$-1, %r12d
	je	.L398
	addq	$1, -1352(%rbp)
.L399:
	testl	%ebx, %ebx
	je	.L400
	movl	%r12d, %edi
	movl	%edx, -1392(%rbp)
	call	__GI_towlower
	cmpl	$120, %eax
	movl	-1392(%rbp), %edx
	je	.L1818
.L400:
	testl	%edx, %edx
	je	.L1819
.L401:
	cmpl	$10, %edx
	jne	.L409
.L410:
	testl	$1024, %r14d
	movl	$10, %edx
	jne	.L1820
.L409:
	movl	%r14d, %eax
	movl	%r14d, -1392(%rbp)
	shrl	$7, %eax
	xorl	$1, %eax
	movl	%eax, %r15d
	andl	$1, %r15d
	movl	%r15d, %r14d
	movl	%edx, %r15d
.L443:
	cmpl	$-1, %r12d
	je	.L1706
.L1823:
	testl	%ebx, %ebx
	je	.L1706
	cmpl	$16, %r15d
	je	.L1821
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	ja	.L435
	cmpl	%r15d, %eax
	jl	.L434
.L435:
	cmpl	$10, %r15d
	jne	.L1706
	testb	%r14b, %r14b
	jne	.L1706
	cmpl	-1360(%rbp), %r12d
	jne	.L1706
.L434:
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	je	.L1822
.L436:
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L437:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L982
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L982
	leaq	4(%rcx), %rdx
	movl	(%rcx), %r12d
	movq	%rdx, (%rax)
.L440:
	cmpl	$-1, %r12d
	je	.L441
	addq	$1, -1352(%rbp)
	cmpl	$-1, %r12d
	jne	.L1823
	.p2align 4,,10
	.p2align 3
.L1706:
	movl	-1392(%rbp), %r14d
	movl	%r15d, %edx
.L432:
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	je	.L1824
	movq	-1088(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L447
	movq	%rax, %rsi
	subq	%rcx, %rsi
	cmpq	$4, %rsi
	je	.L1825
.L448:
	cmpl	$-1, %r12d
	jne	.L472
	movq	-1096(%rbp), %rcx
	movq	-1352(%rbp), %r15
.L471:
	cmpq	%rax, %rcx
	je	.L1826
	leaq	4(%rax), %rcx
	movq	%rcx, -1104(%rbp)
	movl	$0, (%rax)
.L474:
	testq	%rcx, %rcx
	je	.L1827
	movl	%r14d, %ecx
	movl	%r14d, %ebx
	movq	-1088(%rbp), %rdi
	andl	$128, %ecx
	andl	$64, %ebx
	leaq	-1336(%rbp), %rsi
	je	.L476
	call	__GI___wcstol_internal
.L477:
	movq	-1088(%rbp), %rcx
	cmpq	%rcx, -1336(%rbp)
	je	.L1828
	movl	%r14d, %r8d
	andl	$8, %r8d
	jne	.L1735
	movl	%r14d, %edx
	andl	$1, %edx
	testl	%ebx, %ebx
	je	.L480
	testl	%edx, %edx
	je	.L481
	movl	-1408(%rbp), %esi
	testl	%esi, %esi
	jne	.L482
.L1760:
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L535
	movl	%ecx, %edx
	addq	-1304(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L536:
	movq	(%rdx), %rdx
.L537:
	movq	%rax, (%rdx)
.L494:
	addl	$1, -1356(%rbp)
	movq	-1344(%rbp), %rax
	jmp	.L34
.L1027:
	movq	-1456(%rbp), %r15
.L100:
	movl	%r14d, %r8d
	andl	$8, %r8d
	jne	.L1735
	testb	$1, %r14b
	je	.L116
	movl	-1408(%rbp), %edi
	testl	%edi, %edi
	jne	.L117
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L118
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L119:
	movq	(%rax), %rax
.L120:
	movq	%r15, (%rax)
.L1734:
	movq	-1344(%rbp), %rax
	jmp	.L34
.L1026:
	movq	-1456(%rbp), %r15
.L99:
	orl	$64, %r14d
	xorl	%edx, %edx
	jmp	.L95
.L1024:
	movq	-1456(%rbp), %r15
.L97:
	testb	$1, %r14b
	jne	.L93
	cmpl	$-1, %ebx
	movl	$1, %eax
	movl	%r14d, %r8d
	cmove	%eax, %ebx
	andl	$8, %r8d
	jne	.L168
	testl	$8448, %r14d
	je	.L169
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L170
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L171
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L172:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L173:
	cmpq	$0, -1376(%rbp)
	je	.L1829
	movl	$100, %edi
	movl	%r8d, -1352(%rbp)
	call	malloc@PLT
	movq	-1376(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -1416(%rbp)
	movq	%rax, (%rcx)
	je	.L183
	movq	-1424(%rbp), %rax
	movl	-1352(%rbp), %r8d
	testq	%rax, %rax
	je	.L184
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L184
.L185:
	movq	-1424(%rbp), %rsi
	movq	-1376(%rbp), %rcx
	movq	$100, -1400(%rbp)
	movq	%rdx, (%rsi)
	movq	%rcx, 16(%rsi,%rax,8)
.L168:
	cmpl	$-1, %r12d
	je	.L1830
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L200
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L200
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L202:
	cmpl	$-1, %r12d
	je	.L1695
	leaq	-1328(%rbp), %rax
	addq	$1, %r15
	movl	%r14d, -1456(%rbp)
	movq	%r15, -1408(%rbp)
	movq	$0, -1328(%rbp)
	movl	%r12d, %r15d
	movq	%rax, -1352(%rbp)
	movl	%r14d, %eax
	movl	%r8d, %r12d
	andl	$8200, %eax
	movq	-1416(%rbp), %r14
	movl	%eax, -1392(%rbp)
	cmpl	$8192, -1392(%rbp)
	je	.L1831
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-1352(%rbp), %rdx
	testl	%r12d, %r12d
	movl	$0, %edi
	cmove	%r14, %rdi
	movl	%r15d, %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L1832
	subl	$1, %ebx
	addq	%rax, %r14
	testl	%ebx, %ebx
	jle	.L1696
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L209
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L209
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r15d
	movq	%rcx, (%rax)
.L211:
	cmpl	$-1, %r15d
	je	.L212
	addq	$1, -1408(%rbp)
	cmpl	$8192, -1392(%rbp)
	jne	.L204
.L1831:
	movq	-1376(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1400(%rbp), %rax
	addq	%rdi, %rax
	subq	%r14, %rax
	cmpq	$16, %rax
	jg	.L204
	salq	-1400(%rbp)
	subq	%rdi, %r14
	movq	-1400(%rbp), %rax
	movq	%rax, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1833
	movq	-1376(%rbp), %rcx
	addq	%rax, %r14
	movq	%rax, (%rcx)
	jmp	.L204
.L1023:
	movq	-1456(%rbp), %r15
.L96:
	movl	%r14d, %r8d
	movl	%r14d, %r9d
	andl	$8, %r8d
	andl	$1, %r9d
	je	.L819
	testl	%r8d, %r8d
	jne	.L820
	testl	$8448, %r14d
	je	.L821
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L822
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L823
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L824:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L825:
	cmpq	$0, -1376(%rbp)
	je	.L1834
	movl	$400, %edi
	movl	%r8d, -1392(%rbp)
	movl	%r9d, -1352(%rbp)
	call	malloc@PLT
	movq	-1376(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, -1432(%rbp)
	movq	%rax, (%rdi)
	je	.L835
.L1757:
	movq	-1424(%rbp), %rax
	movl	-1352(%rbp), %r9d
	movl	-1392(%rbp), %r8d
	testq	%rax, %rax
	je	.L865
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L865
.L866:
	movq	-1424(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%rdx, (%rsi)
	movq	%rdi, 16(%rsi,%rax,8)
	movq	$100, -1400(%rbp)
.L820:
	movq	-1344(%rbp), %rdx
	movl	(%rdx), %eax
	leaq	4(%rdx), %r11
	cmpl	$94, %eax
	jne	.L1071
	movl	4(%rdx), %eax
	leaq	8(%rdx), %rsi
	movq	%r11, -1344(%rbp)
	movl	$1, %ecx
.L879:
	testl	%ebx, %ebx
	movl	$2147483647, %edx
	movq	%r11, -1336(%rbp)
	cmovs	%edx, %ebx
	cmpl	$93, %eax
	jne	.L884
	movl	4(%r11), %eax
	movq	%rsi, -1344(%rbp)
	movq	%rsi, %r11
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L882:
	cmpl	$93, %eax
	je	.L883
	movl	(%rdx), %eax
	movq	%rdx, %r11
.L884:
	testl	%eax, %eax
	leaq	4(%r11), %rdx
	jne	.L882
	movq	%r13, %r14
	movq	%rdx, -1344(%rbp)
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1020:
	movq	-1456(%rbp), %r15
.L92:
	cmpl	$-1, %r12d
	je	.L1835
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L587
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L587
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L589:
	cmpl	$-1, %r12d
	je	.L1711
	leaq	1(%r15), %rax
	movq	-1104(%rbp), %rdx
	movq	%rax, -1352(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	leal	-43(%r12), %eax
	andl	$-3, %eax
	jne	.L1059
	cmpq	%rdx, -1096(%rbp)
	je	.L1836
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rdx)
.L593:
	testl	%ebx, %ebx
	je	.L1837
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L595
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L595
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L597:
	cmpl	$-1, %r12d
	je	.L1838
	leaq	2(%r15), %rax
	movq	-1104(%rbp), %rdx
	movb	$1, -1472(%rbp)
	movq	%rax, -1352(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
.L591:
	movl	%r12d, %edi
	movq	%rdx, -1392(%rbp)
	call	__GI_towlower
	cmpl	$110, %eax
	movq	-1392(%rbp), %rdx
	je	.L1839
	cmpl	$105, %eax
	jne	.L618
	cmpq	%rdx, -1096(%rbp)
	je	.L1840
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rdx)
.L620:
	testl	%ebx, %ebx
	je	.L1841
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L621
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L621
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L623:
	cmpl	$-1, %r12d
	je	.L1842
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$110, %eax
	jne	.L1843
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1844
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L628:
	testl	%ebx, %ebx
	je	.L1845
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L630
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L630
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L632:
	cmpl	$-1, %r12d
	je	.L1846
	movq	-1352(%rbp), %rax
	movl	%r12d, %edi
	leaq	2(%rax), %r15
	call	__GI_towlower
	cmpl	$102, %eax
	jne	.L1847
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1848
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L637:
	testl	%ebx, %ebx
	je	.L1731
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L639
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L639
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L641:
	cmpl	$-1, %r12d
	je	.L642
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$105, %eax
	je	.L1849
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	__GI__IO_sputbackwc
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rdx
.L617:
	cmpq	%rdx, %rax
	je	.L1850
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	$0, (%rax)
.L763:
	testq	%rdx, %rdx
	je	.L1851
	movl	%r14d, %edx
	movl	%r14d, %ebx
	movq	-1088(%rbp), %rdi
	andl	$128, %edx
	andl	$8, %ebx
	testb	$2, %r14b
	je	.L765
	testb	$4, -1444(%rbp)
	jne	.L1852
	testb	$1, -1444(%rbp)
	jne	.L765
	leaq	-1336(%rbp), %rsi
	call	__GI___wcstold_internal
	testl	%ebx, %ebx
	jne	.L1970
	movq	-1088(%rbp), %rax
	movq	-1336(%rbp), %rdx
	cmpq	%rax, %rdx
	movq	%rax, %rdi
	je	.L1971
	movl	-1408(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L782
	movl	-1320(%rbp), %esi
	cmpl	$47, %esi
	ja	.L783
	movl	%esi, %ecx
	addq	-1304(%rbp), %rcx
	addl	$8, %esi
	movl	%esi, -1320(%rbp)
.L784:
	movq	(%rcx), %rcx
.L785:
	fstpt	(%rcx)
.L781:
	cmpq	%rdx, %rax
	je	.L1853
	addl	$1, -1356(%rbp)
	.p2align 4,,10
	.p2align 3
.L1735:
	movq	-1344(%rbp), %rax
	xorl	%r8d, %r8d
	jmp	.L34
.L1025:
	movq	-1456(%rbp), %r15
.L98:
	orl	$64, %r14d
	movl	$10, %edx
	jmp	.L95
.L107:
	movl	%r14d, %r8d
	andl	$8, %r8d
.L108:
	testl	%r8d, %r8d
	jne	.L329
.L327:
	movl	%r14d, %r8d
	andl	$8448, %r8d
	je	.L330
	movl	-1408(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L331
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L332
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L333:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L334:
	cmpq	$0, -1376(%rbp)
	je	.L1854
	movl	$400, %edi
	call	malloc@PLT
	movq	-1376(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -1432(%rbp)
	movq	%rax, (%rcx)
	je	.L344
	movq	-1424(%rbp), %rax
	testq	%rax, %rax
	je	.L345
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L345
.L346:
	movq	-1424(%rbp), %rdi
	movq	-1376(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	$100, -1400(%rbp)
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi,%rax,8)
.L329:
	cmpl	$-1, %r12d
	je	.L1855
.L328:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L360
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L360
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L362:
	cmpl	$-1, %r12d
	je	.L1699
	movq	-1432(%rbp), %rax
	movl	%r14d, -1408(%rbp)
	addq	$1, %r15
	movq	%rax, -1352(%rbp)
	movl	%r14d, %eax
	movl	%r12d, %r14d
	andl	$8448, %eax
	movl	%r14d, %edi
	movl	%r8d, %r12d
	movl	%eax, -1392(%rbp)
	call	__GI_iswspace
	testl	%eax, %eax
	jne	.L1856
	.p2align 4,,10
	.p2align 3
.L364:
	testl	%r12d, %r12d
	jne	.L366
	movl	-1392(%rbp), %edi
	movq	-1352(%rbp), %rsi
	testl	%edi, %edi
	leaq	4(%rsi), %rax
	movl	%r14d, (%rsi)
	je	.L1052
	movq	-1376(%rbp), %rcx
	movq	-1400(%rbp), %rsi
	movq	%rax, -1352(%rbp)
	movq	(%rcx), %rdi
	leaq	0(,%rsi,4), %rcx
	leaq	(%rdi,%rcx), %rsi
	cmpq	%rsi, %rax
	je	.L1857
.L366:
	testl	%ebx, %ebx
	jle	.L369
	subl	$1, %ebx
	je	.L1858
.L369:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L974
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L974
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r14d
	movq	%rcx, (%rax)
.L371:
	cmpl	$-1, %r14d
	je	.L372
	movl	%r14d, %edi
	addq	$1, %r15
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L364
.L1856:
	movl	%r12d, -1392(%rbp)
	movl	%r14d, %r12d
	movq	%r13, %rdi
	movl	%r12d, %esi
	movl	-1408(%rbp), %r14d
	subq	$1, %r15
	call	__GI__IO_sputbackwc
	movl	-1392(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L365:
	testl	%r8d, %r8d
	je	.L373
	movq	-1352(%rbp), %rcx
	movq	-1344(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rcx, -1432(%rbp)
	jmp	.L34
.L1723:
	movq	%r13, %r14
.L378:
	movq	-1392(%rbp), %rax
	movl	-1364(%rbp), %edi
	movl	%edi, %fs:(%rax)
.L379:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L105:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1810:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movq	%r13, %r14
	movl	%edi, %fs:(%rax)
.L249:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1028:
	movq	-1456(%rbp), %r15
.L102:
	movl	%r14d, %eax
	movl	$16, %edx
	andl	$-7, %eax
	orl	$4097, %eax
	movl	%eax, %r14d
	jmp	.L95
.L1029:
	movq	-1456(%rbp), %r15
.L103:
	movl	%r14d, %r8d
	andl	$8, %r8d
	testb	$1, %r14b
	jne	.L108
	testl	%r8d, %r8d
	jne	.L272
	testl	$8448, %r14d
	je	.L273
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L274
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L275
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L276:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L277:
	cmpq	$0, -1376(%rbp)
	je	.L1859
	movl	$100, %edi
	movl	%r8d, -1352(%rbp)
	call	malloc@PLT
	movq	-1376(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, -1416(%rbp)
	movq	%rax, (%rsi)
	je	.L287
	movq	-1424(%rbp), %rax
	movl	-1352(%rbp), %r8d
	testq	%rax, %rax
	je	.L288
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L288
.L289:
	movq	-1424(%rbp), %rdi
	movq	-1376(%rbp), %rcx
	movq	$100, -1400(%rbp)
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi,%rax,8)
.L272:
	cmpl	$-1, %r12d
	je	.L1860
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L304
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L304
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L306:
	cmpl	$-1, %r12d
	je	.L1698
	leaq	-1328(%rbp), %rax
	addq	$1, %r15
	movl	%r12d, %edi
	movq	%r15, -1456(%rbp)
	movq	$0, -1328(%rbp)
	movq	%rax, -1352(%rbp)
	movl	%r14d, %eax
	movl	%r14d, -1464(%rbp)
	andl	$8448, %eax
	movl	%r8d, -1408(%rbp)
	movq	-1416(%rbp), %r15
	movl	%eax, -1472(%rbp)
	call	__GI_iswspace
	testl	%eax, %eax
	jne	.L1861
.L308:
	movl	-1408(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L310
	movl	-1472(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L311
	movq	-1376(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1400(%rbp), %rax
	addq	%rdi, %rax
	subq	%r15, %rax
	cmpq	$16, %rax
	jle	.L1862
.L311:
	movq	%r15, %r14
.L971:
	movq	-1352(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L1863
	cmpq	$16, %rax
	ja	.L1864
	testl	%ebx, %ebx
	leaq	(%r14,%rax), %r15
	jle	.L316
	subl	$1, %ebx
	je	.L1865
.L316:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L972
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L972
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L318:
	cmpl	$-1, %r12d
	je	.L319
	movl	%r12d, %edi
	addq	$1, -1456(%rbp)
	call	__GI_iswspace
	testl	%eax, %eax
	je	.L308
.L1861:
	movl	-1408(%rbp), %r8d
	movq	%r15, -1416(%rbp)
	movl	%r12d, %esi
	movq	-1456(%rbp), %r15
	movq	%r13, %rdi
	movl	-1464(%rbp), %r14d
	movl	%r8d, -1392(%rbp)
	call	__GI__IO_sputbackwc
	movl	-1392(%rbp), %r8d
	subq	$1, %r15
.L309:
	testl	%r8d, %r8d
	jne	.L1735
	leaq	-1184(%rbp), %rax
	movq	-1352(%rbp), %rdx
	xorl	%esi, %esi
	movl	%r8d, -1392(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1456(%rbp)
	call	__wcrtomb
	movl	%r14d, %edx
	movq	%rax, %rbx
	movl	-1392(%rbp), %r8d
	andl	$8448, %edx
	testq	%rax, %rax
	je	.L321
	testl	%edx, %edx
	je	.L322
	movq	-1376(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1416(%rbp), %rax
	leaq	(%rax,%rbx), %rdx
	movq	-1400(%rbp), %rax
	addq	%rdi, %rax
	cmpq	%rax, %rdx
	jnb	.L1866
.L323:
	movq	-1456(%rbp), %rsi
	movq	-1416(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r8d, -1352(%rbp)
	call	__GI_mempcpy@PLT
	movl	-1352(%rbp), %r8d
	leaq	1(%rax), %rcx
	movb	$0, (%rax)
	movq	%rcx, -1416(%rbp)
.L1001:
	movq	-1376(%rbp), %rbx
	movq	-1416(%rbp), %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	cmpq	-1400(%rbp), %rsi
	je	.L375
.L1745:
	movl	%r8d, -1352(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movl	-1352(%rbp), %r8d
	je	.L375
	movq	%rax, (%rbx)
.L375:
	addl	$1, -1356(%rbp)
	movq	-1344(%rbp), %rax
	movq	$0, -1376(%rbp)
	jmp	.L34
.L90:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L111
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L111
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L113:
	cmpl	$-1, %r12d
	je	.L1694
	cmpl	$37, %r12d
	je	.L1734
	movq	%r13, %r14
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	__GI__IO_sputbackwc
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L109:
	movq	-1392(%rbp), %rax
	movl	-1364(%rbp), %edi
	movq	%r13, %r14
	movl	%edi, %fs:(%rax)
.L110:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L94:
	movl	%r14d, %r8d
	movq	-1456(%rbp), %r15
	andl	$8, %r8d
	je	.L327
	jmp	.L328
.L101:
	movl	$8, %edx
.L104:
	movq	-1456(%rbp), %r15
	jmp	.L377
.L1022:
	movq	-1456(%rbp), %r15
	movl	$16, %edx
	jmp	.L95
.L1030:
	movl	$10, %edx
	jmp	.L104
.L968:
	testb	$8, %r14b
	je	.L327
	movq	%r13, %r14
.L969:
	movq	-1392(%rbp), %rax
	movl	-1364(%rbp), %esi
	movl	%esi, %fs:(%rax)
.L359:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1722:
	movq	%r13, %r14
.L585:
	movq	-1392(%rbp), %rax
	movl	-1364(%rbp), %ecx
	movl	%ecx, %fs:(%rax)
.L586:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	je	.L1087
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1808:
	orl	$256, %r14d
	movl	%eax, -1352(%rbp)
	jmp	.L72
.L966:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L89:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L1821:
	movl	%r12d, %edi
	call	__GI_iswxdigit
	testl	%eax, %eax
	je	.L1706
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	jne	.L436
.L1822:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L441:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L372:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r12d, %r8d
	movl	%r14d, %r12d
	movl	-1408(%rbp), %r14d
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%rax, -1352(%rbp)
	jmp	.L366
.L391:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
.L392:
	testl	%edx, %edx
	je	.L410
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L476:
	call	__GI___wcstoul_internal
	jmp	.L477
.L447:
	testl	$4096, %r14d
	je	.L1709
	cmpl	$4, %ebx
	jbe	.L1709
	cmpl	$40, %r12d
	jne	.L1709
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L450
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L450
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L452:
	cmpl	$-1, %r12d
	movl	%edx, -1392(%rbp)
	je	.L453
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$110, %eax
	movl	-1392(%rbp), %edx
	jne	.L1710
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L985
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L985
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L458:
	cmpl	$-1, %r12d
	movl	%edx, -1392(%rbp)
	je	.L459
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$105, %eax
	movl	-1392(%rbp), %edx
	jne	.L1710
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L987
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L987
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L462:
	cmpl	$-1, %r12d
	movl	%edx, -1392(%rbp)
	je	.L463
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$108, %eax
	movl	-1392(%rbp), %edx
	jne	.L1710
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L989
	movq	(%rax), %rcx
	cmpq	8(%rax), %rcx
	jnb	.L989
	leaq	4(%rcx), %rsi
	movl	(%rcx), %r12d
	movq	%rsi, (%rax)
.L467:
	cmpl	$-1, %r12d
	je	.L468
	movq	-1352(%rbp), %r15
	addq	$4, %r15
	cmpl	$41, %r12d
	jne	.L1710
	movq	-1104(%rbp), %rsi
	movq	-1096(%rbp), %rcx
	cmpq	%rsi, %rcx
	je	.L1867
	leaq	4(%rsi), %rax
	movq	%rax, -1104(%rbp)
	movl	$48, (%rsi)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L974:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r14d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L261:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1392(%rbp), %r8d
	movl	%r13d, %edx
	movq	%rbx, %r13
	movq	%rcx, %rbx
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
.L262:
	testl	%edx, %edx
	je	.L270
	movq	-1376(%rbp), %r14
	movq	%rbx, %rsi
	movq	(%r14), %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	-1400(%rbp), %rax
	je	.L270
	movl	%r8d, -1352(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movl	-1352(%rbp), %r8d
	je	.L270
	movq	%rax, (%r14)
.L270:
	addl	$1, -1356(%rbp)
	movq	-1344(%rbp), %rax
	movq	%rbx, -1432(%rbp)
	movq	$0, -1376(%rbp)
	jmp	.L34
.L1815:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r13, %r14
	movq	%rax, -1392(%rbp)
	jmp	.L378
.L1700:
	movq	%r13, %r14
	jmp	.L379
.L472:
	movq	-1352(%rbp), %r15
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI__IO_sputbackwc
	subq	$1, %r15
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rcx
	movl	-1392(%rbp), %edx
	jmp	.L471
.L310:
	movq	%r15, %r14
	xorl	%r15d, %r15d
	jmp	.L971
.L1825:
	movl	(%rcx), %ecx
	subl	$43, %ecx
	andl	$-3, %ecx
	jne	.L448
.L1709:
	cmpl	$-1, %r12d
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	movl	%r12d, %r13d
	je	.L44
.L469:
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	__GI__IO_sputbackwc
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L258:
	movq	%rbx, %rdi
	movq	%rcx, -1408(%rbp)
	call	__GI___wuflow
	movq	-1408(%rbp), %rcx
	movl	%eax, %r12d
	jmp	.L260
.L453:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L480:
	testl	%edx, %edx
	je	.L533
	movl	-1408(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L1760
	movq	-1440(%rbp), %rsi
	movl	-1408(%rbp), %r9d
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L538:
	subl	$1, %r9d
	je	.L1776
	cmpl	$47, %ecx
	ja	.L539
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L1012:
	movl	$-1, %ebx
	jmp	.L24
.L1811:
	movq	-1456(%rbp), %rax
	addq	-1352(%rbp), %rax
	movq	-1400(%rbp), %rsi
	subq	%r15, %rax
	movq	%rsi, %r9
	cmpq	%rax, %rsi
	jle	.L256
	leal	-1(%r14), %r9d
	movslq	%r9d, %r9
.L256:
	addq	-1400(%rbp), %r9
	movq	%rcx, -1432(%rbp)
	leaq	0(,%r9,4), %rsi
	movq	%r9, -1408(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1408(%rbp), %r9
	movq	-1432(%rbp), %rcx
	je	.L1868
	movq	-1376(%rbp), %rdi
	movq	%r9, -1400(%rbp)
	movq	%rax, (%rdi)
	addq	%rcx, %rax
	jmp	.L255
.L1806:
	movl	8(%rcx), %eax
	leaq	8(%rcx), %rdx
	orl	$3, %r14d
	movq	%rdx, -1344(%rbp)
	movl	%eax, -1352(%rbp)
	jmp	.L72
.L76:
	movl	8(%rcx), %eax
	leaq	8(%rcx), %rdx
	orl	$8193, %r14d
	movq	%rdx, -1344(%rbp)
	movl	%eax, -1352(%rbp)
	jmp	.L72
.L1807:
	movl	8(%rcx), %eax
	leaq	8(%rcx), %rdx
	orl	$512, %r14d
	movq	%rdx, -1344(%rbp)
	movl	%eax, -1352(%rbp)
	jmp	.L72
.L32:
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L33
	call	__lll_lock_wait_private
	jmp	.L33
.L380:
	movq	%r13, %rdi
	movl	%edx, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L382
.L319:
	movq	-1392(%rbp), %rax
	movq	%r15, -1416(%rbp)
	movl	-1464(%rbp), %r14d
	movq	-1456(%rbp), %r15
	movl	-1408(%rbp), %r8d
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L309
.L212:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, -1416(%rbp)
	movl	%r12d, %r8d
	movl	-1456(%rbp), %r14d
	movl	%r15d, %r12d
	movq	-1408(%rbp), %r15
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
.L208:
	testl	%r8d, %r8d
	jne	.L1735
	andl	$8448, %r14d
	je	.L375
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$1, %ebx
	jle	.L1736
	leal	-2(%rbx), %eax
	leaq	2(%r15,%rax), %rbx
	movq	-1352(%rbp), %r15
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L1870:
	addq	$1, %r15
	cmpq	%rbx, %r15
	je	.L1869
.L268:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L264
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L264
	leaq	4(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	(%rdx), %eax
.L266:
	cmpl	$-1, %eax
	jne	.L1870
	movl	%eax, %r12d
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	movq	-1344(%rbp), %rax
	jmp	.L34
.L264:
	movq	%r13, %rdi
	call	__GI___wuflow
	jmp	.L266
.L1814:
	cmpl	$-1, %r13d
	je	.L44
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	__GI__IO_sputbackwc
	jmp	.L44
.L398:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L399
.L373:
	movq	-1352(%rbp), %rax
	andl	$8448, %r14d
	leaq	4(%rax), %rcx
	movl	$0, (%rax)
	movq	%rcx, -1432(%rbp)
	je	.L375
	movq	-1376(%rbp), %rbx
	movq	(%rbx), %rdi
	subq	%rdi, %rcx
	movq	%rcx, %rax
	movq	%rcx, %rsi
	sarq	$2, %rax
	cmpq	-1400(%rbp), %rax
	je	.L375
	jmp	.L1745
.L1819:
	movl	$8, %edx
	jmp	.L409
.L957:
#APP
# 3030 "vfscanf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L955
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 3030 "vfscanf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L950:
	movq	%r14, %rdi
	call	__GI___wuflow
	movl	%eax, %r13d
	jmp	.L952
.L819:
	testl	%r8d, %r8d
	jne	.L820
	testl	$8448, %r14d
	je	.L850
	movl	-1408(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L851
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L852
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L853:
	movq	(%rax), %rax
	movq	%rax, -1376(%rbp)
.L854:
	cmpq	$0, -1376(%rbp)
	je	.L1871
	movl	$100, %edi
	movl	%r8d, -1392(%rbp)
	movl	%r9d, -1352(%rbp)
	call	malloc@PLT
	movq	-1376(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -1416(%rbp)
	movq	%rax, (%rcx)
	jne	.L1757
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1070
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L468:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1857:
	movq	-1400(%rbp), %rax
	movq	%rcx, -1352(%rbp)
	leaq	0(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1352(%rbp), %rcx
	je	.L1872
	movq	-1376(%rbp), %rdi
	salq	-1400(%rbp)
	movq	%rax, (%rdi)
	addq	%rcx, %rax
	movq	%rax, -1352(%rbp)
	jmp	.L366
.L533:
	testb	$4, %r14b
	je	.L546
	movl	-1408(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L547
.L1763:
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L548
	movl	%ecx, %edx
	addq	-1304(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L549:
	movq	(%rdx), %rdx
.L550:
	movw	%ax, (%rdx)
	jmp	.L494
.L481:
	testb	$4, %r14b
	je	.L495
	movl	-1408(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1763
	movq	-1440(%rbp), %rsi
	movl	-1408(%rbp), %r9d
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L500:
	subl	$1, %r9d
	je	.L1778
	cmpl	$47, %ecx
	ja	.L501
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1818:
	testb	$10, %dl
	jne	.L401
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	cmpl	$-1, %r12d
	je	.L1873
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L405
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L405
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L407:
	cmpl	$-1, %r12d
	je	.L408
	addq	$1, -1352(%rbp)
	movl	$16, %edx
	jmp	.L409
.L1785:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$9, %fs:(%rax)
	jmp	.L24
.L1827:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L1826:
	movq	-1384(%rbp), %rdi
	xorl	%esi, %esi
	movl	%edx, -1352(%rbp)
	call	char_buffer_add_slow
	movq	-1104(%rbp), %rcx
	movl	-1352(%rbp), %edx
	jmp	.L474
.L1059:
	movb	$0, -1472(%rbp)
	jmp	.L591
.L1828:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1812:
	movl	%r13d, %edx
	movl	-1392(%rbp), %r8d
	movq	%rbx, %r13
	movq	%rcx, %rbx
	jmp	.L262
.L1820:
	leaq	.LC2(%rip), %rdi
	call	__wctrans@PLT
	movq	%rax, %r10
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	testq	%r10, %r10
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	304(%rdx), %ecx
	leal	-1(%rcx), %r9d
	jne	.L1874
.L411:
	movl	%r14d, %r11d
	xorl	%r15d, %r15d
	shrl	$7, %r11d
	xorl	$1, %r11d
	andl	$1, %r11d
	.p2align 4,,10
	.p2align 3
.L413:
	cmpl	$-1, %r12d
	je	.L981
	testl	%ebx, %ebx
	je	.L981
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movslq	%r15d, %rdi
	xorl	%edx, %edx
	salq	$2, %rdi
	leaq	-1264(%rbp), %rcx
	movl	%r9d, -1392(%rbp)
	movq	%fs:(%rax), %r8
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L414:
	leal	31(%rdx), %esi
	movq	(%r8), %r9
	movslq	%esi, %rsi
	movq	64(%r9,%rsi,8), %rsi
.L415:
	addq	%rdi, %rsi
	cmpl	%r12d, (%rsi)
	movq	%rsi, (%rcx,%rdx,8)
	je	.L1058
	addq	$4, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	addq	$1, %rdx
	cmpq	$10, %rdx
	je	.L1875
.L417:
	testq	%r10, %r10
	movl	%edx, %eax
	je	.L414
	movq	-1184(%rbp,%rdx,8), %rsi
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L1875:
	movl	-1392(%rbp), %r9d
	leal	1(%r15), %edi
	cmpl	%edi, %r9d
	jl	.L979
.L978:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L419:
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, %eax
	cmpl	%r12d, (%rsi)
	je	.L416
	addq	$4, %rsi
	movq	%rsi, (%rcx,%rdx,8)
	addq	$1, %rdx
	cmpq	$10, %rdx
	jne	.L419
	addl	$1, %edi
	cmpl	%edi, %r9d
	jge	.L978
.L979:
	testb	%r11b, %r11b
	jne	.L981
	cmpl	-1360(%rbp), %r12d
	jne	.L981
.L980:
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	je	.L1876
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L422:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	cmpl	$-1, %r12d
	je	.L1877
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L426
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L426
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L428:
	cmpl	$-1, %r12d
	je	.L429
	addq	$1, -1352(%rbp)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	%r15d, %edi
.L416:
	leal	48(%rax), %r12d
	movl	%edi, %r9d
	movl	%edi, %r15d
	jmp	.L980
.L1877:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movl	%edi, %fs:(%rax)
	jmp	.L413
.L429:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L413
.L426:
	movq	%r13, %rdi
	movb	%r11b, -1464(%rbp)
	movq	%r10, -1456(%rbp)
	movl	%r9d, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %r9d
	movl	%eax, %r12d
	movq	-1456(%rbp), %r10
	movzbl	-1464(%rbp), %r11d
	jmp	.L428
.L981:
	movl	$10, %edx
	jmp	.L432
.L1876:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	movb	%r11b, -1464(%rbp)
	movq	%r10, -1456(%rbp)
	movl	%r9d, -1392(%rbp)
	call	char_buffer_add_slow
	movl	-1392(%rbp), %r9d
	movq	-1456(%rbp), %r10
	movzbl	-1464(%rbp), %r11d
	jmp	.L422
.L463:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1855:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r13, %r14
	movq	%rax, -1392(%rbp)
	jmp	.L969
.L219:
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L236
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L237
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L238:
	movq	(%rax), %rax
	movq	%rax, -1432(%rbp)
.L239:
	cmpq	$0, -1432(%rbp)
	jne	.L218
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L972:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L318
.L209:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r15d
	jmp	.L211
.L330:
	movl	-1408(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L347
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L348
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L349:
	movq	(%rax), %rax
	movq	%rax, -1432(%rbp)
.L350:
	cmpq	$0, -1432(%rbp)
	jne	.L329
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L388:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L390
.L976:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L397
.L1697:
	movq	%r13, %r14
	jmp	.L249
.L1699:
	movq	%r13, %r14
	jmp	.L359
.L1087:
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1839:
	cmpq	%rdx, -1096(%rbp)
	je	.L1878
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rdx)
.L601:
	testl	%ebx, %ebx
	je	.L1879
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L602
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L602
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L604:
	cmpl	$-1, %r12d
	je	.L1880
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$97, %eax
	jne	.L1881
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	subl	%eax, %ebx
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1882
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L609:
	testl	%ebx, %ebx
	je	.L1883
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L611
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L611
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L613:
	cmpl	$-1, %r12d
	je	.L1884
	movq	-1352(%rbp), %r15
	movl	%r12d, %edi
	call	__GI_towlower
	addq	$2, %r15
	cmpl	$110, %eax
	jne	.L1885
.L680:
	movq	-1104(%rbp), %rcx
	movq	-1096(%rbp), %rdx
	cmpq	%rdx, %rcx
	je	.L1886
	leaq	4(%rcx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rcx)
	jmp	.L617
.L883:
	testl	%r9d, %r9d
	movq	%rdx, -1344(%rbp)
	je	.L1887
	cmpl	$-1, %r12d
	je	.L1888
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L887
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L887
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L889:
	cmpl	$-1, %r12d
	je	.L1714
	leaq	1(%r15), %rsi
	leal	-1(%rbx), %eax
	leaq	(%rax,%rsi), %rbx
	movl	%ecx, %eax
	movq	%rsi, -1352(%rbp)
	xorl	$1, %eax
	movq	%rbx, -1392(%rbp)
	movl	%eax, -1408(%rbp)
	movq	%r11, %rbx
.L890:
	movq	-1336(%rbp), %r9
	cmpq	%rbx, %r9
	movq	%r9, %rax
	jb	.L899
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L892:
	cmpl	%r12d, %edx
	je	.L894
	addq	$4, %rax
.L997:
	cmpq	%rbx, %rax
	jnb	.L891
.L899:
	movl	(%rax), %edx
	cmpl	$45, %edx
	jne	.L892
	movl	4(%rax), %esi
	testl	%esi, %esi
	je	.L892
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L892
	cmpq	%rax, %r9
	je	.L892
	movl	-4(%rax), %edi
	cmpl	%esi, %edi
	ja	.L892
	leal	1(%rdi), %edx
	cmpl	%edx, %esi
	jl	.L893
	cmpl	%r12d, %edx
	jne	.L895
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L896:
	cmpl	%edx, %r12d
	je	.L894
.L895:
	addl	$1, %edx
	cmpl	%edx, %esi
	jge	.L896
.L893:
	addq	$8, %rax
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L894:
	testl	%ecx, %ecx
	je	.L891
.L1732:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%r8d, -1392(%rbp)
	subq	$1, -1352(%rbp)
	call	__GI__IO_sputbackwc
	movl	-1392(%rbp), %r8d
.L897:
	cmpq	%r15, -1352(%rbp)
	je	.L1889
	testl	%r8d, %r8d
	jne	.L1736
	movq	-1432(%rbp), %rax
	andl	$8448, %r14d
	leaq	4(%rax), %rbx
	movl	$0, (%rax)
	je	.L911
	movq	-1376(%rbp), %r15
	movq	%rbx, %rsi
	movq	(%r15), %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	-1400(%rbp), %rax
	je	.L911
	movl	%r8d, -1376(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movl	-1376(%rbp), %r8d
	je	.L911
	movq	%rax, (%r15)
.L911:
	addl	$1, -1356(%rbp)
	movq	-1344(%rbp), %rax
	movq	%rbx, -1432(%rbp)
	movq	-1352(%rbp), %r15
	movq	$0, -1376(%rbp)
	jmp	.L34
.L891:
	cmpq	%rax, %rbx
	jne	.L900
	movl	-1408(%rbp), %edi
	testl	%edi, %edi
	jne	.L1732
.L900:
	testl	%r8d, %r8d
	jne	.L901
	movq	-1432(%rbp), %rdi
	testl	$8448, %r14d
	leaq	4(%rdi), %rax
	movl	%r12d, (%rdi)
	je	.L1075
	movq	-1376(%rbp), %rsi
	movq	%rax, -1432(%rbp)
	movq	(%rsi), %rdi
	movq	-1400(%rbp), %rsi
	leaq	0(,%rsi,4), %rdx
	leaq	(%rdi,%rdx), %rsi
	cmpq	%rsi, %rax
	je	.L1890
.L901:
	movq	-1392(%rbp), %rsi
	cmpq	%rsi, -1352(%rbp)
	je	.L897
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L904
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L904
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L906:
	cmpl	$-1, %r12d
	je	.L907
	addq	$1, -1352(%rbp)
	jmp	.L890
.L989:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L467
.L618:
	testl	%ebx, %ebx
	je	.L1061
	cmpl	$48, %r12d
	jne	.L1061
	cmpq	%rdx, -1096(%rbp)
	je	.L1891
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	$48, (%rdx)
.L684:
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L991
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L991
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L686:
	cmpl	$-1, %r12d
	je	.L687
	addq	$1, -1352(%rbp)
.L688:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	movq	-1104(%rbp), %rdx
	setg	%al
	subl	%eax, %ebx
	je	.L1062
	movl	%r12d, %edi
	movq	%rdx, -1456(%rbp)
	call	__GI_towlower
	cmpl	$120, %eax
	movl	$101, -1392(%rbp)
	movl	$1, %ecx
	movq	-1456(%rbp), %rdx
	je	.L1892
.L682:
	movl	%r14d, %eax
	movl	%r14d, -1464(%rbp)
	movb	$0, -1456(%rbp)
	andl	$128, %eax
	movl	%ebx, %r14d
	xorl	%r15d, %r15d
	movl	%r12d, %ebx
	movl	%eax, -1480(%rbp)
	movl	%ecx, %r12d
	jmp	.L698
.L700:
	cmpb	$0, -1456(%rbp)
	je	.L1893
	movl	-1392(%rbp), %eax
	cmpl	%eax, -4(%rdx)
	je	.L1894
.L706:
	movl	%r15d, %eax
	xorl	$1, %eax
	andl	$1, %eax
	cmpl	-1448(%rbp), %ebx
	jne	.L709
	testb	%al, %al
	je	.L709
	cmpq	%rdx, -1096(%rbp)
	je	.L1895
	leaq	4(%rdx), %rax
	movl	$1, %r15d
	movq	%rax, -1104(%rbp)
	movl	%ebx, (%rdx)
.L702:
	testl	%r14d, %r14d
	je	.L1712
	cmpl	$-1, %ebx
	je	.L1896
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L716
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L716
	leaq	4(%rdx), %rcx
	movl	(%rdx), %ebx
	movq	%rcx, (%rax)
.L718:
	cmpl	$-1, %ebx
	je	.L719
	xorl	%eax, %eax
	addq	$1, -1352(%rbp)
	movq	-1104(%rbp), %rdx
	testl	%r14d, %r14d
	setg	%al
	subl	%eax, %r14d
.L698:
	testq	%rdx, %rdx
	je	.L1897
	leal	-48(%rbx), %eax
	cmpl	$9, %eax
	ja	.L700
.L1740:
	cmpq	%rdx, -1096(%rbp)
	je	.L1898
	leaq	4(%rdx), %rax
	movl	$1, %r12d
	movq	%rax, -1104(%rbp)
	movl	%ebx, (%rdx)
	jmp	.L702
.L719:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%ebx, %r12d
	movl	%r14d, %ebx
	movl	-1464(%rbp), %r14d
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
.L714:
	movq	-1104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1899
.L713:
	movsbq	-1472(%rbp), %rax
	testl	$1024, %r14d
	movq	-1088(%rbp), %rcx
	movq	%rax, -1464(%rbp)
	jne	.L1900
.L722:
	movq	-1104(%rbp), %rdx
.L1003:
	subq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	cmpq	-1464(%rbp), %rax
	je	.L1901
	testl	$2048, %r14d
	jne	.L760
.L761:
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rdx
	movq	-1352(%rbp), %r15
	jmp	.L617
.L1893:
	testl	$2048, -1464(%rbp)
	je	.L704
	movl	%ebx, %edi
	movq	%rdx, -1488(%rbp)
	call	__GI_iswxdigit
	testl	%eax, %eax
	movq	-1488(%rbp), %rdx
	jne	.L1740
.L704:
	testb	%r12b, %r12b
	je	.L706
	movl	%ebx, %edi
	movq	%rdx, -1488(%rbp)
	call	__GI_towlower
	cmpl	-1392(%rbp), %eax
	movq	-1488(%rbp), %rdx
	jne	.L706
	cmpq	%rdx, -1096(%rbp)
	je	.L1902
	leaq	4(%rdx), %rax
	movb	%r12b, -1456(%rbp)
	movl	%r12d, %r15d
	movq	%rax, -1104(%rbp)
	movl	-1392(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L702
.L1816:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	movl	%edx, -1392(%rbp)
	call	char_buffer_add_slow
	movl	-1392(%rbp), %edx
	jmp	.L386
.L1817:
	movq	-1384(%rbp), %rdi
	movl	$48, %esi
	movl	%edx, -1392(%rbp)
	call	char_buffer_add_slow
	movl	-1392(%rbp), %edx
	jmp	.L395
.L716:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %ebx
	jmp	.L718
.L459:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L116:
	testb	$4, %r14b
	je	.L129
	movl	-1408(%rbp), %esi
	testl	%esi, %esi
	jne	.L130
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L131
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L132:
	movq	(%rax), %rax
.L133:
	movw	%r15w, (%rax)
	xorl	%r8d, %r8d
	movq	-1344(%rbp), %rax
	jmp	.L34
.L709:
	movl	-1480(%rbp), %edi
	testl	%edi, %edi
	je	.L711
	cmpl	-1360(%rbp), %ebx
	jne	.L711
	testb	%al, %al
	je	.L711
	cmpq	%rdx, -1096(%rbp)
	je	.L1903
	leaq	4(%rdx), %rax
	xorl	%r15d, %r15d
	movq	%rax, -1104(%rbp)
	movl	%ebx, (%rdx)
	jmp	.L702
.L345:
	subq	$288, %rsp
	movq	-1424(%rbp), %rdi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rdi, 8(%rax)
	movq	%rax, -1424(%rbp)
	xorl	%eax, %eax
	jmp	.L346
.L546:
	andl	$512, %r14d
	jne	.L559
	movl	-1408(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L560
.L1766:
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L561
	movl	%ecx, %edx
	addq	-1304(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L562:
	movq	(%rdx), %rdx
	movl	%eax, (%rdx)
	jmp	.L494
.L1863:
	movq	-1392(%rbp), %rax
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	movl	$84, %fs:(%rax)
	jmp	.L44
.L495:
	andl	$512, %r14d
	jne	.L508
	movl	-1408(%rbp), %edx
	testl	%edx, %edx
	je	.L1766
	movq	-1440(%rbp), %rcx
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L513:
	subl	$1, -1408(%rbp)
	je	.L1780
	cmpl	$47, %ecx
	ja	.L514
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	%r13, %r14
	jmp	.L110
.L1832:
	movl	-1356(%rbp), %esi
	movq	%r13, %r14
	testl	%esi, %esi
	movl	%esi, %ebx
	jne	.L44
	movl	%eax, %ebx
	movl	%eax, -1356(%rbp)
	jmp	.L44
.L1711:
	movq	%r13, %r14
	jmp	.L586
.L1970:
	fstp	%st(0)
.L767:
	movq	-1088(%rbp), %rax
	cmpq	%rax, -1336(%rbp)
	jne	.L1735
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-1440(%rbp), %rsi
	movl	-1408(%rbp), %r8d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L335:
	subl	$1, %r8d
	je	.L1904
	cmpl	$47, %edx
	ja	.L336
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L1858:
	movl	%r12d, %r8d
	movl	%r14d, %r12d
	movl	-1408(%rbp), %r14d
	jmp	.L365
.L765:
	andl	$3, %r14d
	leaq	-1336(%rbp), %rsi
	je	.L794
	call	__GI___wcstod_internal
	testl	%ebx, %ebx
	jne	.L767
	movq	-1336(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L768
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L795
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L796
	movl	%ecx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L797:
	movq	(%rax), %rax
.L798:
	movsd	%xmm0, (%rax)
	movq	-1088(%rbp), %rax
	jmp	.L781
.L865:
	subq	$288, %rsp
	movq	-1424(%rbp), %rsi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, -1424(%rbp)
	xorl	%eax, %eax
	jmp	.L866
.L450:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L452
.L1867:
	movq	-1384(%rbp), %rdi
	movl	$48, %esi
	movl	%edx, -1352(%rbp)
	call	char_buffer_add_slow
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rcx
	movl	-1352(%rbp), %edx
	jmp	.L471
.L344:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	movl	-1356(%rbp), %ebx
	je	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1862:
	salq	-1400(%rbp)
	subq	%rdi, %r15
	movq	-1400(%rbp), %rax
	movq	%rax, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1905
	movq	-1376(%rbp), %rcx
	addq	%rax, %r15
	movq	%rax, (%rcx)
	jmp	.L311
.L1786:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L24
.L1869:
	movl	%eax, %r12d
	xorl	%r8d, %r8d
	movq	-1344(%rbp), %rax
	jmp	.L34
.L250:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L252
.L360:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L362
.L1860:
	movq	-1392(%rbp), %rax
	movl	-1364(%rbp), %esi
	movq	%r13, %r14
	movl	%esi, %fs:(%rax)
.L303:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1897:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L1835:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r13, %r14
	movq	%rax, -1392(%rbp)
	jmp	.L585
.L1868:
	movq	-1376(%rbp), %rax
	leaq	4(%rcx), %rsi
	addq	$1, -1400(%rbp)
	movq	%rcx, -1408(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1043
	movq	-1376(%rbp), %rdi
	movq	-1408(%rbp), %rcx
	movq	%rax, (%rdi)
	addq	%rcx, %rax
	jmp	.L255
.L1830:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %ecx
	movq	%r13, %r14
	movl	%ecx, %fs:(%rax)
.L199:
	movl	-1356(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L482:
	movq	-1440(%rbp), %rdi
	movl	-1408(%rbp), %r9d
	xorl	%r10d, %r10d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L486:
	subl	$1, %r9d
	je	.L1776
	cmpl	$47, %ecx
	ja	.L487
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L1061:
	movl	$101, -1392(%rbp)
	xorl	%ecx, %ecx
	jmp	.L682
.L236:
	movq	-1440(%rbp), %rsi
	movl	-1408(%rbp), %r9d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L240:
	subl	$1, %r9d
	je	.L1906
	cmpl	$47, %edx
	ja	.L241
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L220:
	movq	-1440(%rbp), %rdi
	movl	-1408(%rbp), %r9d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L224:
	subl	$1, %r9d
	je	.L1907
	cmpl	$47, %edx
	ja	.L225
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L821:
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L838
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L839
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L840:
	movq	(%rax), %rax
	movq	%rax, -1432(%rbp)
.L841:
	cmpq	$0, -1432(%rbp)
	jne	.L820
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L850:
	movl	-1408(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L867
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L868
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L869:
	movq	(%rax), %rax
	movq	%rax, -1416(%rbp)
.L870:
	cmpq	$0, -1416(%rbp)
	jne	.L820
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1887:
	cmpl	$-1, %r12d
	je	.L1908
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L915
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L915
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L917:
	cmpl	$-1, %r12d
	je	.L1717
	leaq	-1328(%rbp), %rax
	leaq	1(%r15), %rdi
	movq	$0, -1328(%rbp)
	movq	%rax, -1352(%rbp)
	leal	-1(%rbx), %eax
	movq	%rdi, -1392(%rbp)
	movq	%r11, %rbx
	addq	%rdi, %rax
	movq	%rax, -1408(%rbp)
	movl	%ecx, %eax
	xorl	$1, %eax
	movl	%eax, -1456(%rbp)
.L918:
	movq	-1336(%rbp), %r9
	cmpq	%rbx, %r9
	movq	%r9, %rax
	jb	.L927
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L920:
	cmpl	%r12d, %edx
	je	.L922
	addq	$4, %rax
.L999:
	cmpq	%rbx, %rax
	jnb	.L919
.L927:
	movl	(%rax), %edx
	cmpl	$45, %edx
	jne	.L920
	movl	4(%rax), %esi
	testl	%esi, %esi
	je	.L920
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbx
	je	.L920
	cmpq	%rax, %r9
	je	.L920
	movl	-4(%rax), %edi
	cmpl	%esi, %edi
	ja	.L920
	leal	1(%rdi), %edx
	cmpl	%edx, %esi
	jge	.L924
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L1909:
	addl	$1, %edx
	cmpl	%edx, %esi
	jl	.L921
.L924:
	cmpl	%edx, %r12d
	jne	.L1909
	.p2align 4,,10
	.p2align 3
.L922:
	testl	%ecx, %ecx
	je	.L919
.L1733:
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%r8d, -1408(%rbp)
	subq	$1, -1392(%rbp)
	call	__GI__IO_sputbackwc
	movl	-1408(%rbp), %r8d
.L925:
	cmpq	%r15, -1392(%rbp)
	je	.L1910
	testl	%r8d, %r8d
	je	.L940
	movq	-1344(%rbp), %rax
	xorl	%r8d, %r8d
	movq	-1392(%rbp), %r15
	jmp	.L34
.L919:
	cmpq	%rax, %rbx
	jne	.L928
	movl	-1456(%rbp), %esi
	testl	%esi, %esi
	jne	.L1733
.L928:
	testl	%r8d, %r8d
	jne	.L929
	testl	$8448, %r14d
	je	.L930
	movq	-1376(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1400(%rbp), %rax
	addq	%rdi, %rax
	subq	-1416(%rbp), %rax
	cmpq	$16, %rax
	jle	.L1911
.L930:
	movq	-1416(%rbp), %r9
.L1000:
	movq	-1352(%rbp), %rdx
	movq	-1416(%rbp), %rdi
	movl	%r12d, %esi
	movl	%r8d, -1480(%rbp)
	movl	%ecx, -1472(%rbp)
	movq	%r9, -1464(%rbp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	-1464(%rbp), %r9
	movl	-1472(%rbp), %ecx
	movl	-1480(%rbp), %r8d
	je	.L1912
	cmpq	$16, %rax
	ja	.L1913
	addq	%r9, %rax
	movq	-1392(%rbp), %rdi
	cmpq	%rdi, -1408(%rbp)
	movq	%rax, -1416(%rbp)
	je	.L925
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L935
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L935
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L937:
	cmpl	$-1, %r12d
	je	.L938
	addq	$1, -1392(%rbp)
	jmp	.L918
.L921:
	addq	$8, %rax
	jmp	.L999
.L929:
	movq	-1416(%rbp), %r9
	movq	$0, -1416(%rbp)
	jmp	.L1000
.L985:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L458
.L234:
	subq	$288, %rsp
	movq	-1424(%rbp), %rsi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, -1424(%rbp)
	xorl	%eax, %eax
	jmp	.L235
.L987:
	movq	%r13, %rdi
	movl	%edx, -1392(%rbp)
	call	__GI___wuflow
	movl	-1392(%rbp), %edx
	movl	%eax, %r12d
	jmp	.L462
.L111:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L113
.L508:
	movl	-1408(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L521
.L1769:
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L573
	movl	%ecx, %edx
	addq	-1304(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L574:
	movq	(%rdx), %rdx
	movb	%al, (%rdx)
	jmp	.L494
.L169:
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L186
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L187
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L188:
	movq	(%rax), %rax
	movq	%rax, -1416(%rbp)
.L189:
	cmpq	$0, -1416(%rbp)
	jne	.L168
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L233:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	movl	-1356(%rbp), %ebx
	je	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L642:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
.L1731:
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rdx
	jmp	.L617
.L587:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L589
.L1841:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L273:
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L290
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L291
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L292:
	movq	(%rax), %rax
	movq	%rax, -1416(%rbp)
.L293:
	cmpq	$0, -1416(%rbp)
	jne	.L272
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L347:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r9d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L351:
	subl	$1, %r9d
	je	.L1914
	cmpl	$47, %edx
	ja	.L352
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L907:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L897
.L559:
	movl	-1408(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1769
	movq	-1440(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L576:
	subl	$1, -1408(%rbp)
	je	.L1782
	cmpl	$47, %ecx
	ja	.L577
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r14d, %r8d
	andl	$512, %r8d
	jne	.L142
	movl	-1408(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L143
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L144
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L145:
	movq	(%rax), %rax
.L146:
	movl	%r15d, (%rax)
	movq	-1344(%rbp), %rax
	jmp	.L34
.L822:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r10d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L826:
	subl	$1, %r10d
	je	.L1915
	cmpl	$47, %edx
	ja	.L827
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L851:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r10d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L855:
	subl	$1, %r10d
	je	.L1916
	cmpl	$47, %edx
	ja	.L856
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1894:
	leal	-43(%rbx), %eax
	andl	$-3, %eax
	jne	.L706
	cmpq	%rdx, -1096(%rbp)
	je	.L1917
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%ebx, (%rdx)
	jmp	.L702
.L1736:
	movq	-1344(%rbp), %rax
	xorl	%r8d, %r8d
	movq	-1352(%rbp), %r15
	jmp	.L34
.L1695:
	movq	%r13, %r14
	jmp	.L199
.L1075:
	movq	%rax, -1432(%rbp)
	jmp	.L901
.L1698:
	movq	%r13, %r14
	jmp	.L303
.L1873:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movl	%edi, %fs:(%rax)
.L404:
	movl	$16, %edx
	movl	$-1, %r12d
	jmp	.L432
.L794:
	call	__GI___wcstof_internal
	testl	%ebx, %ebx
	jne	.L767
	movq	-1336(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L768
	movl	-1408(%rbp), %eax
	testl	%eax, %eax
	jne	.L807
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L808
	movl	%ecx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L809:
	movq	(%rax), %rax
.L810:
	movss	%xmm0, (%rax)
	movq	-1088(%rbp), %rax
	jmp	.L781
.L938:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L925
.L1872:
	movq	-1376(%rbp), %rax
	leaq	4(%rcx), %rsi
	addq	$1, -1400(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1352(%rbp), %rcx
	je	.L1918
	movq	-1376(%rbp), %rsi
	movq	%rax, (%rsi)
	addq	%rcx, %rax
	movq	%rax, -1352(%rbp)
	jmp	.L366
.L184:
	subq	$288, %rsp
	movq	-1424(%rbp), %rcx
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rax, -1424(%rbp)
	xorl	%eax, %eax
	jmp	.L185
.L835:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1069
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L288:
	subq	$288, %rsp
	movq	-1424(%rbp), %rdi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rdi, 8(%rax)
	movq	%rax, -1424(%rbp)
	xorl	%eax, %eax
	jmp	.L289
.L547:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r9d
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L551:
	subl	$1, %r9d
	je	.L1778
	cmpl	$47, %ecx
	ja	.L552
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r9d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L121:
	subl	$1, %r9d
	je	.L1919
	cmpl	$47, %edx
	ja	.L122
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1898:
	movq	-1384(%rbp), %rdi
	movl	%ebx, %esi
	movl	$1, %r12d
	call	char_buffer_add_slow
	jmp	.L702
.L1971:
	fstp	%st(0)
.L768:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L183:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	movl	-1356(%rbp), %ebx
	je	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L170:
	movq	-1440(%rbp), %rdi
	movl	-1408(%rbp), %r9d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L174:
	subl	$1, %r9d
	je	.L1920
	cmpl	$47, %edx
	ja	.L175
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	%r14, -1416(%rbp)
	movl	%r12d, %r8d
	movl	-1456(%rbp), %r14d
	movl	%r15d, %r12d
	movq	-1408(%rbp), %r15
	jmp	.L208
.L408:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L404
.L1852:
	leaq	-1336(%rbp), %rsi
	call	__GI___wcstof128_internal
	testl	%ebx, %ebx
	jne	.L767
	movq	-1336(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L768
	movl	-1408(%rbp), %esi
	testl	%esi, %esi
	jne	.L769
	movl	-1320(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L770
	movl	%ecx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1320(%rbp)
.L771:
	movq	(%rax), %rax
.L772:
	movaps	%xmm0, (%rax)
	movq	-1088(%rbp), %rax
	jmp	.L781
.L904:
	movq	%r13, %rdi
	movl	%r8d, -1464(%rbp)
	movl	%ecx, -1456(%rbp)
	call	__GI___wuflow
	movl	-1456(%rbp), %ecx
	movl	%eax, %r12d
	movl	-1464(%rbp), %r8d
	jmp	.L906
.L142:
	movl	-1408(%rbp), %edx
	testl	%edx, %edx
	jne	.L155
	movl	-1320(%rbp), %edx
	cmpl	$47, %edx
	ja	.L156
	movl	%edx, %eax
	addq	-1304(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1320(%rbp)
.L157:
	movq	(%rax), %rax
.L158:
	movb	%r15b, (%rax)
	xorl	%r8d, %r8d
	movq	-1344(%rbp), %rax
	jmp	.L34
.L1062:
	movl	$101, -1392(%rbp)
	movl	$1, %ecx
	jmp	.L682
.L274:
	movq	-1440(%rbp), %rcx
	movl	-1408(%rbp), %r9d
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L278:
	subl	$1, %r9d
	je	.L1921
	cmpl	$47, %edx
	ja	.L279
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L287:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	movl	-1356(%rbp), %ebx
	je	.L44
	movl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L935:
	movq	%r13, %rdi
	movl	%r8d, -1472(%rbp)
	movl	%ecx, -1464(%rbp)
	call	__GI___wuflow
	movl	-1464(%rbp), %ecx
	movl	%eax, %r12d
	movl	-1472(%rbp), %r8d
	jmp	.L937
.L838:
	movq	-1440(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L842:
	subl	$1, -1408(%rbp)
	je	.L1922
	cmpl	$47, %edx
	ja	.L843
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	-1376(%rbp), %rax
	leaq	16(%r14), %rdx
	movq	%rdx, %rsi
	movq	%rdx, -1400(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1036
	movq	-1376(%rbp), %rdi
	addq	%rax, %r14
	movq	%rax, (%rdi)
	jmp	.L204
.L1888:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %ecx
	movq	%r13, %r14
	movl	%ecx, %fs:(%rax)
.L886:
	movl	-1356(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L44
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L200:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L202
.L867:
	movq	-1440(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L871:
	subl	$1, -1408(%rbp)
	je	.L1923
	cmpl	$47, %edx
	ja	.L872
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-1440(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L134:
	subl	$1, -1408(%rbp)
	je	.L1924
	cmpl	$47, %edx
	ja	.L135
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L595:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L597
.L1908:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %ecx
	movq	%r13, %r14
	movl	%ecx, %fs:(%rax)
.L914:
	movl	-1356(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L44
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1884:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1842:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1837:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1836:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L593
.L1851:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L1850:
	movq	-1384(%rbp), %rdi
	xorl	%esi, %esi
	call	char_buffer_add_slow
	movq	-1104(%rbp), %rdx
	jmp	.L763
.L1846:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L304:
	movq	%r13, %rdi
	movl	%r8d, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %r8d
	movl	%eax, %r12d
	jmp	.L306
.L1071:
	movq	%r11, %rsi
	xorl	%ecx, %ecx
	movq	%rdx, %r11
	jmp	.L879
.L602:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L604
.L687:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L688
.L1840:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L620
.L1905:
	movq	-1376(%rbp), %rax
	leaq	16(%r15), %r14
	movq	%r14, %rsi
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1925
	movq	-1376(%rbp), %rdi
	addq	%rax, %r15
	movq	%r14, -1400(%rbp)
	movq	%rax, (%rdi)
	jmp	.L311
.L1912:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	movl	$84, %fs:(%rax)
	jmp	.L44
.L940:
	leaq	-1184(%rbp), %rax
	movq	-1352(%rbp), %rdx
	xorl	%esi, %esi
	movl	%r8d, -1408(%rbp)
	movq	%rax, %rdi
	movq	%rax, -1456(%rbp)
	call	__wcrtomb
	movq	%rax, %rbx
	movl	%r14d, %eax
	movl	-1408(%rbp), %r8d
	andl	$8448, %eax
	testq	%rbx, %rbx
	je	.L941
	testl	%eax, %eax
	je	.L942
	movq	-1376(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1416(%rbp), %rax
	leaq	(%rax,%rbx), %rdx
	movq	-1400(%rbp), %rax
	addq	%rdi, %rax
	cmpq	%rax, %rdx
	jnb	.L1926
.L943:
	movq	-1416(%rbp), %rdi
	movq	-1456(%rbp), %rsi
	movq	%rbx, %rdx
	movl	%r8d, -1352(%rbp)
	call	__GI_mempcpy@PLT
	movl	-1352(%rbp), %r8d
	leaq	1(%rax), %rdi
	movb	$0, (%rax)
	movq	%rdi, -1416(%rbp)
.L1006:
	movq	-1376(%rbp), %rbx
	movq	-1416(%rbp), %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	cmpq	%rsi, -1400(%rbp)
	je	.L946
	movl	%r8d, -1352(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movl	-1352(%rbp), %r8d
	je	.L946
	movq	%rax, (%rbx)
.L946:
	addl	$1, -1356(%rbp)
	movq	-1344(%rbp), %rax
	movq	-1392(%rbp), %r15
	movq	$0, -1376(%rbp)
	jmp	.L34
.L1717:
	movq	%r13, %r14
	jmp	.L914
.L521:
	movq	-1440(%rbp), %rdi
	xorl	%r10d, %r10d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L525:
	subl	$1, -1408(%rbp)
	je	.L1782
	cmpl	$47, %ecx
	ja	.L526
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L1874:
	leal	2(%rcx), %eax
	movslq	%ecx, %rsi
	xorl	%r8d, %r8d
	movq	%rsi, -1392(%rbp)
	movl	%ebx, -1492(%rbp)
	movq	%r10, %r15
	cltq
	movq	%r8, %rbx
	movl	%r12d, -1496(%rbp)
	salq	$2, %rax
	movl	%ecx, -1500(%rbp)
	leaq	-8(%rax), %rdi
	leaq	-4(%rax), %rsi
	addq	$30, %rax
	andq	$-16, %rax
	movq	%rdi, -1464(%rbp)
	movq	%rsi, -1472(%rbp)
	leaq	-1184(%rbp), %rdi
	leaq	-1264(%rbp), %rsi
	movq	%rax, -1480(%rbp)
	movq	%rdi, -1456(%rbp)
	movq	%rsi, -1488(%rbp)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L1927:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	addq	$1, %rbx
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
.L412:
	subq	-1480(%rbp), %rsp
	leal	31(%rbx), %eax
	cltq
	movq	64(%rdx,%rax,8), %rsi
	movq	-1488(%rbp), %rax
	leaq	15(%rsp), %rcx
	movq	-1392(%rbp), %rdx
	andq	$-16, %rcx
	movq	%rsi, (%rax,%rbx,8)
	movq	%rcx, %rdi
	movq	%rcx, %r12
	call	__wmemcpy
	leal	48(%rbx), %edi
	movq	%r15, %rsi
	call	__GI___towctrans
	movq	-1464(%rbp), %rcx
	cmpq	$9, %rbx
	movl	%eax, (%r12,%rcx)
	movq	-1472(%rbp), %rax
	movl	$0, (%r12,%rax)
	movq	-1456(%rbp), %rax
	movq	%r12, (%rax,%rbx,8)
	jne	.L1927
	movl	-1500(%rbp), %ecx
	movl	-1492(%rbp), %ebx
	movq	%r15, %r10
	movl	-1496(%rbp), %r12d
	movl	%ecx, %r9d
	jmp	.L411
.L630:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L632
.L405:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L407
.L621:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L623
.L1843:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1714:
	movq	%r13, %r14
	jmp	.L886
.L611:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L613
.L1883:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1881:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L290:
	movq	-1440(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L294:
	subl	$1, -1408(%rbp)
	je	.L1928
	cmpl	$47, %edx
	ja	.L295
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L186:
	movq	-1440(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L190:
	subl	$1, -1408(%rbp)
	je	.L1929
	cmpl	$47, %edx
	ja	.L191
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	%r15, -1416(%rbp)
	movl	-1464(%rbp), %r14d
	movq	-1456(%rbp), %r15
	movl	-1408(%rbp), %r8d
	jmp	.L309
.L1896:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %ecx
	movl	%ebx, %r12d
	movl	%r14d, %ebx
	movl	-1464(%rbp), %r14d
	movl	%ecx, %fs:(%rax)
	jmp	.L714
.L1712:
	movl	%ebx, %r12d
	movl	%r14d, %ebx
	movl	-1464(%rbp), %r14d
	jmp	.L714
.L560:
	movq	-1440(%rbp), %rdi
	xorl	%r10d, %r10d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rdi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L564:
	subl	$1, -1408(%rbp)
	je	.L1780
	cmpl	$47, %ecx
	ja	.L565
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L601
.L1886:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	movq	-1104(%rbp), %rax
	movq	-1096(%rbp), %rdx
	jmp	.L617
.L1848:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L637
.L1892:
	cmpq	%rdx, -1096(%rbp)
	je	.L1930
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rdx)
.L691:
	movl	%r14d, %eax
	andb	$127, %al
	orb	$8, %ah
	cmpl	$-1, %r12d
	movl	%eax, %r14d
	je	.L1931
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L694
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L694
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L696:
	cmpl	$-1, %r12d
	je	.L697
	addq	$1, -1352(%rbp)
.L693:
	testl	%ebx, %ebx
	movq	-1104(%rbp), %rdx
	jle	.L1064
	subl	$1, %ebx
	movl	$112, -1392(%rbp)
	xorl	%ecx, %ecx
	jmp	.L682
.L991:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L686
.L1854:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1845:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1849:
	testl	%ebx, %ebx
	jle	.L645
	subl	$1, %ebx
.L645:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1932
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L647:
	testl	%ebx, %ebx
	je	.L1933
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L649
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L649
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L651:
	cmpl	$-1, %r12d
	je	.L1934
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$110, %eax
	jne	.L1935
	testl	%ebx, %ebx
	jle	.L654
	subl	$1, %ebx
.L654:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1936
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L656:
	testl	%ebx, %ebx
	je	.L1937
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L658
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L658
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L660:
	cmpl	$-1, %r12d
	je	.L1938
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$105, %eax
	jne	.L1939
	testl	%ebx, %ebx
	jle	.L663
	subl	$1, %ebx
.L663:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1940
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L665:
	testl	%ebx, %ebx
	je	.L1941
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L667
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L667
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L669:
	cmpl	$-1, %r12d
	je	.L1942
	movl	%r12d, %edi
	call	__GI_towlower
	cmpl	$116, %eax
	jne	.L1943
	testl	%ebx, %ebx
	jle	.L672
	subl	$1, %ebx
.L672:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1944
	leaq	4(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movl	%r12d, (%rax)
.L674:
	testl	%ebx, %ebx
	je	.L1945
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L676
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L676
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L678:
	cmpl	$-1, %r12d
	je	.L1946
	movq	-1352(%rbp), %r15
	movl	%r12d, %edi
	call	__GI_towlower
	addq	$7, %r15
	cmpl	$121, %eax
	je	.L680
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1903:
	movq	-1384(%rbp), %rdi
	movl	%ebx, %esi
	xorl	%r15d, %r15d
	call	char_buffer_add_slow
	jmp	.L702
.L711:
	movl	%ebx, %r12d
	movl	%r14d, %ebx
	movl	-1464(%rbp), %r14d
	cmpl	$-1, %r12d
	je	.L713
	movl	%r12d, %esi
	movq	%r13, %rdi
	subq	$1, -1352(%rbp)
	call	__GI__IO_sputbackwc
	jmp	.L714
.L887:
	movq	%r13, %rdi
	movl	%r8d, -1408(%rbp)
	movq	%r11, -1392(%rbp)
	movl	%ecx, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %ecx
	movl	%eax, %r12d
	movq	-1392(%rbp), %r11
	movl	-1408(%rbp), %r8d
	jmp	.L889
.L769:
	movq	-1440(%rbp), %rdi
	xorl	%r8d, %r8d
	movdqu	(%rdi), %xmm1
	movups	%xmm1, -1184(%rbp)
	movq	16(%rdi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm1, %eax
	xorl	%edi, %edi
	movq	%rcx, -1168(%rbp)
	movd	%xmm1, %ecx
.L773:
	subl	$1, -1408(%rbp)
	je	.L1947
	cmpl	$47, %ecx
	ja	.L774
	addl	$8, %ecx
	movl	$1, %r8d
	jmp	.L773
.L795:
	movq	-1440(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	movdqu	(%rsi), %xmm1
	movups	%xmm1, -1184(%rbp)
	movq	16(%rsi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm1, %eax
	movq	%rcx, -1168(%rbp)
	movd	%xmm1, %ecx
.L799:
	subl	$1, -1408(%rbp)
	je	.L1948
	cmpl	$47, %ecx
	ja	.L800
	addl	$8, %ecx
	movl	$1, %r8d
	jmp	.L799
.L1885:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1882:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L609
.L1844:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L628
.L639:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L641
.L143:
	movq	-1440(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rsi), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L147:
	subl	$1, -1408(%rbp)
	je	.L1949
	cmpl	$47, %edx
	ja	.L148
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L147
.L807:
	movq	-1440(%rbp), %rdi
	xorl	%r8d, %r8d
	movdqu	(%rdi), %xmm1
	movups	%xmm1, -1184(%rbp)
	movq	16(%rdi), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm1, %eax
	xorl	%edi, %edi
	movq	%rcx, -1168(%rbp)
	movd	%xmm1, %ecx
.L811:
	subl	$1, -1408(%rbp)
	je	.L1950
	cmpl	$47, %ecx
	ja	.L812
	addl	$8, %ecx
	movl	$1, %r8d
	jmp	.L811
.L942:
	movq	-1456(%rbp), %rsi
	movq	-1416(%rbp), %rdi
	movq	%rbx, %rdx
	movl	%r8d, -1352(%rbp)
	call	__GI_mempcpy@PLT
	leaq	1(%rax), %rsi
	movb	$0, (%rax)
	movl	-1352(%rbp), %r8d
	movq	%rsi, -1416(%rbp)
	jmp	.L946
.L1891:
	movq	-1384(%rbp), %rdi
	movl	$48, %esi
	call	char_buffer_add_slow
	jmp	.L684
.L1847:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1890:
	movq	-1400(%rbp), %rax
	movq	%rdx, -1464(%rbp)
	movl	%r8d, -1456(%rbp)
	movl	%ecx, -1432(%rbp)
	leaq	0(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movl	-1432(%rbp), %ecx
	movl	-1456(%rbp), %r8d
	movq	-1464(%rbp), %rdx
	je	.L1951
	movq	-1376(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	-1400(%rbp), %rdi
	addq	%rdx, %rax
	movq	%rax, -1432(%rbp)
	movq	%rdi, %rax
	addq	%rdi, %rax
	movq	%rax, -1400(%rbp)
	jmp	.L901
.L155:
	movq	-1440(%rbp), %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rdx
	movq	-1176(%rbp), %rcx
	movd	%xmm0, %eax
	movq	%rdx, -1168(%rbp)
	movd	%xmm0, %edx
.L159:
	subl	$1, -1408(%rbp)
	je	.L1952
	cmpl	$47, %edx
	ja	.L160
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L159
.L1901:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1889:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L915:
	movq	%r13, %rdi
	movl	%r8d, -1408(%rbp)
	movq	%r11, -1392(%rbp)
	movl	%ecx, -1352(%rbp)
	call	__GI___wuflow
	movl	-1352(%rbp), %ecx
	movl	%eax, %r12d
	movq	-1392(%rbp), %r11
	movl	-1408(%rbp), %r8d
	jmp	.L917
.L1902:
	movl	-1392(%rbp), %esi
	movq	-1384(%rbp), %rdi
	movl	%r12d, %r15d
	call	char_buffer_add_slow
	movb	%r12b, -1456(%rbp)
	jmp	.L702
.L1911:
	movl	%ecx, -1464(%rbp)
	movq	-1400(%rbp), %rcx
	movq	-1416(%rbp), %rdx
	movl	%r8d, -1472(%rbp)
	movq	%rcx, %rax
	addq	%rcx, %rax
	subq	%rdi, %rdx
	movq	%rax, %rsi
	movq	%rdx, -1416(%rbp)
	movq	%rax, -1400(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1416(%rbp), %rdx
	movl	-1464(%rbp), %ecx
	movl	-1472(%rbp), %r8d
	je	.L1953
	movq	-1376(%rbp), %rsi
	movq	%rax, (%rsi)
	addq	%rdx, %rax
	movq	%rax, -1416(%rbp)
	jmp	.L930
.L1910:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L322:
	movq	-1416(%rbp), %rdi
	movq	-1456(%rbp), %rsi
	movq	%rax, %rdx
	movl	%r8d, -1352(%rbp)
	call	__GI_mempcpy@PLT
	leaq	1(%rax), %rdi
	movb	$0, (%rax)
	movl	-1352(%rbp), %r8d
	movq	%rdi, -1416(%rbp)
	jmp	.L375
.L321:
	movq	-1416(%rbp), %rdi
	testl	%edx, %edx
	leaq	1(%rdi), %rax
	movb	$0, (%rdi)
	movq	%rax, -1416(%rbp)
	jne	.L1001
	jmp	.L375
.L782:
	movq	-1440(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	movdqu	(%rcx), %xmm0
	movups	%xmm0, -1184(%rbp)
	movq	16(%rcx), %rcx
	movq	-1176(%rbp), %rsi
	movd	%xmm0, %eax
	movq	%rcx, -1168(%rbp)
	movd	%xmm0, %ecx
.L786:
	subl	$1, -1408(%rbp)
	je	.L1954
	cmpl	$47, %ecx
	ja	.L787
	addl	$8, %ecx
	movl	$1, %r10d
	jmp	.L786
.L1895:
	movq	-1384(%rbp), %rdi
	movl	%ebx, %esi
	movl	$1, %r15d
	call	char_buffer_add_slow
	jmp	.L702
.L1900:
	testl	$2048, %r14d
	jne	.L722
	movq	%rdx, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	cmpq	-1464(%rbp), %rax
	je	.L1089
	testb	$1, %r15b
	je	.L722
.L1089:
	leaq	.LC2(%rip), %rdi
	call	__wctrans@PLT
	testq	%rax, %rax
	movq	%rax, -1480(%rbp)
	je	.L1729
	cmpl	$-1, %r12d
	je	.L1955
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L727
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L727
	leaq	4(%rdx), %rcx
	movl	(%rdx), %r12d
	movq	%rcx, (%rax)
.L729:
	cmpl	$-1, %r12d
	je	.L730
	addq	$1, -1352(%rbp)
.L726:
	movq	-1480(%rbp), %rsi
	movl	$46, %edi
	call	__GI___towctrans
	movq	-1104(%rbp), %rdx
	movl	%eax, -1140(%rbp)
	movq	%rdx, %rsi
	subq	-1088(%rbp), %rsi
	sarq	$2, %rsi
	cmpq	-1464(%rbp), %rsi
	je	.L731
	movsbl	-1472(%rbp), %ecx
	addl	$1, %ecx
	movslq	%ecx, %rcx
	cmpq	%rsi, %rcx
	jne	.L724
	cmpl	-1448(%rbp), %eax
	je	.L731
.L724:
	testq	%rdx, %rdx
	jne	.L758
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L1899:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L731:
	movl	%r14d, %eax
	xorl	%edx, %edx
	movl	%ebx, -1492(%rbp)
	shrb	$7, %al
	movq	%rdx, %rbx
	movb	%al, -1488(%rbp)
.L735:
	cmpq	$10, %rbx
	je	.L733
	movq	-1480(%rbp), %rsi
	leal	48(%rbx), %edi
	call	__GI___towctrans
	movl	%eax, -1184(%rbp,%rbx,4)
.L734:
	addq	$1, %rbx
	cmpq	$11, %rbx
	jne	.L735
	movl	%r15d, %ebx
	movl	%r14d, %r15d
	movq	%r13, %r14
	movl	-1492(%rbp), %r13d
.L736:
	movq	-1104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1956
	cmpb	$0, -1456(%rbp)
	je	.L738
	xorl	%eax, %eax
	movl	-1392(%rbp), %ecx
	cmpl	%ecx, -4(%rdx)
	je	.L1957
.L739:
	leaq	-1184(%rbp), %r8
	jmp	.L750
.L743:
	addq	$1, %rax
	cmpq	$12, %rax
	je	.L1958
.L750:
	movl	(%r8,%rax,4), %esi
	movl	%eax, %edi
	cmpl	%r12d, %esi
	jne	.L743
	cmpl	$9, %eax
	jg	.L744
	cmpq	-1096(%rbp), %rdx
	leal	48(%rax), %esi
	je	.L1959
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%esi, (%rdx)
.L741:
	testl	%r13d, %r13d
	je	.L1960
	cmpl	$-1, %r12d
	je	.L1961
	movq	160(%r14), %rax
	testq	%rax, %rax
	je	.L752
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L752
	leaq	4(%rdx), %rsi
	movl	(%rdx), %r12d
	movq	%rsi, (%rax)
.L754:
	cmpl	$-1, %r12d
	je	.L755
	addq	$1, -1352(%rbp)
	testl	%r13d, %r13d
	jle	.L736
	subl	$1, %r13d
	jmp	.L736
.L1864:
	leaq	__PRETTY_FUNCTION__.12757(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$1094, %edx
	call	__GI___assert_fail
.L783:
	movq	-1312(%rbp), %rcx
	leaq	8(%rcx), %rsi
	movq	%rsi, -1312(%rbp)
	jmp	.L784
.L730:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L726
.L727:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L729
.L1955:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movl	%edi, %fs:(%rax)
	jmp	.L726
.L1961:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movq	%r14, %r13
	movl	%r15d, %r14d
	movl	%edi, %fs:(%rax)
.L1729:
	movq	-1104(%rbp), %rdx
	jmp	.L724
.L1797:
	movq	%rbx, -1344(%rbp)
	jmp	.L60
.L1953:
	movq	-1376(%rbp), %rax
	leaq	16(%rdx), %r9
	movq	%r9, %rsi
	movq	%r9, -1400(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1400(%rbp), %r9
	movq	-1416(%rbp), %rdx
	movl	-1464(%rbp), %ecx
	movl	-1472(%rbp), %r8d
	je	.L1962
	movq	-1376(%rbp), %rdi
	movq	%r9, -1400(%rbp)
	movq	%rax, (%rdi)
	addq	%rdx, %rax
	movq	%rax, -1416(%rbp)
	jmp	.L930
.L1069:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1776:
	testb	%r10b, %r10b
	je	.L542
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %edx
.L542:
	testb	%dil, %dil
	je	.L543
	movq	%rsi, -1176(%rbp)
.L543:
	cmpl	$47, %edx
	ja	.L544
	movl	%edx, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1184(%rbp)
.L545:
	movq	(%rcx), %rdx
	jmp	.L537
.L1962:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1080
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movb	$0, (%rax,%rdx)
	movl	-1356(%rbp), %eax
	movl	%eax, %ebx
	jmp	.L44
.L544:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1176(%rbp)
	jmp	.L545
.L1080:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1926:
	movq	-1416(%rbp), %rdx
	movl	%r8d, -1400(%rbp)
	subq	%rdi, %rdx
	leaq	1(%rbx,%rdx), %r15
	movq	%rdx, -1352(%rbp)
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1352(%rbp), %rdx
	movl	-1400(%rbp), %r8d
	je	.L1963
	movq	-1376(%rbp), %rcx
	movq	%r15, -1400(%rbp)
	movq	%rax, (%rcx)
	addq	%rdx, %rax
	movq	%rax, -1416(%rbp)
	jmp	.L943
.L1918:
	movl	-1408(%rbp), %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1054
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movl	$0, -4(%rax,%rcx)
	movl	-1356(%rbp), %eax
	movl	%eax, %ebx
	jmp	.L44
.L1963:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1082
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movb	$0, (%rax,%rdx)
.L945:
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1054:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L1082:
	movl	$-1, -1356(%rbp)
	jmp	.L945
.L856:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L855
.L1916:
	testb	%dil, %dil
	je	.L859
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L859:
	testb	%sil, %sil
	je	.L860
	movq	%rcx, -1176(%rbp)
.L860:
	cmpl	$47, %eax
	ja	.L861
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L862:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L854
.L279:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L278
.L812:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L811
.L1950:
	testb	%r8b, %r8b
	je	.L815
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %eax
.L815:
	testb	%dil, %dil
	je	.L816
	movq	%rsi, -1176(%rbp)
.L816:
	cmpl	$47, %eax
	ja	.L817
	movl	%eax, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L818:
	movq	(%rcx), %rax
	jmp	.L810
.L1921:
	testb	%dil, %dil
	je	.L282
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L282:
	testb	%sil, %sil
	je	.L283
	movq	%rcx, -1176(%rbp)
.L283:
	cmpl	$47, %eax
	ja	.L284
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L285:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L277
.L861:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L862
.L817:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L818
.L284:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L285
.L760:
	movsbl	-1472(%rbp), %edx
	addl	$2, %edx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	jne	.L761
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L526:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L525
.L348:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L349
.L565:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L564
.L1829:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L171:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L172
.L1917:
	movq	-1384(%rbp), %rdi
	movl	%ebx, %esi
	call	char_buffer_add_slow
	jmp	.L702
.L501:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L500
.L352:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L351
.L1914:
	testb	%dil, %dil
	je	.L355
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L355:
	testb	%sil, %sil
	je	.L356
	movq	%rcx, -1176(%rbp)
.L356:
	cmpl	$47, %eax
	ja	.L357
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L358:
	movq	(%rdx), %rax
	movq	%rax, -1432(%rbp)
	jmp	.L350
.L843:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L842
.L1922:
	testb	%dil, %dil
	je	.L846
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L846:
	testb	%sil, %sil
	je	.L847
	movq	%rcx, -1176(%rbp)
.L847:
	cmpl	$47, %eax
	ja	.L848
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L849:
	movq	(%rdx), %rax
	movq	%rax, -1432(%rbp)
	jmp	.L841
.L357:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L358
.L848:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L849
.L1871:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L852:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L853
.L191:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L190
.L1929:
	testb	%dil, %dil
	je	.L194
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L194:
	testb	%sil, %sil
	je	.L195
	movq	%rcx, -1176(%rbp)
.L195:
	cmpl	$47, %eax
	ja	.L196
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L197:
	movq	(%rdx), %rax
	movq	%rax, -1416(%rbp)
	jmp	.L189
.L1834:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1043:
	movq	%rbx, %r14
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L868:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L869
.L839:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L840
.L539:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L538
.L535:
	movq	-1312(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L536
.L487:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L486
.L295:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L294
.L1928:
	testb	%dil, %dil
	je	.L298
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L298:
	testb	%sil, %sil
	je	.L299
	movq	%rcx, -1176(%rbp)
.L299:
	cmpl	$47, %eax
	ja	.L300
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L301:
	movq	(%rdx), %rax
	movq	%rax, -1416(%rbp)
	jmp	.L293
.L577:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L576
.L160:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L159
.L1952:
	testb	%dil, %dil
	je	.L163
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L163:
	testb	%sil, %sil
	je	.L164
	movq	%rcx, -1176(%rbp)
.L164:
	cmpl	$47, %eax
	ja	.L165
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L166:
	movq	(%rdx), %rax
	jmp	.L158
.L131:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L132
.L758:
	movq	-1088(%rbp), %rcx
	jmp	.L1003
.L165:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L166
.L787:
	addq	$8, %rsi
	movl	$1, %r8d
	jmp	.L786
.L1954:
	testb	%r10b, %r10b
	je	.L790
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %eax
.L790:
	testb	%r8b, %r8b
	je	.L791
	movq	%rsi, -1176(%rbp)
.L791:
	cmpl	$47, %eax
	ja	.L792
	movl	%eax, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L793:
	movq	(%rcx), %rcx
	movq	%rdi, %rax
	jmp	.L785
.L1782:
	testb	%r10b, %r10b
	je	.L580
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %edx
.L580:
	testb	%dil, %dil
	je	.L581
	movq	%rsi, -1176(%rbp)
.L581:
	cmpl	$47, %edx
	ja	.L582
	movl	%edx, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1184(%rbp)
.L583:
	movq	(%rcx), %rdx
	movb	%al, (%rdx)
	jmp	.L494
.L300:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L301
.L792:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L793
.L582:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1176(%rbp)
	jmp	.L583
.L1036:
	orl	$-1, %ebx
	movq	%r13, %r14
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L770:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L771
.L118:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L119
.L552:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L551
.L187:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L188
.L1778:
	testb	%r10b, %r10b
	je	.L555
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %edx
.L555:
	testb	%dil, %dil
	je	.L556
	movq	%rsi, -1176(%rbp)
.L556:
	cmpl	$47, %edx
	ja	.L557
	movl	%edx, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1184(%rbp)
.L558:
	movq	(%rcx), %rdx
	jmp	.L550
.L774:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L773
.L1947:
	testb	%r8b, %r8b
	je	.L777
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %eax
.L777:
	testb	%dil, %dil
	je	.L778
	movq	%rsi, -1176(%rbp)
.L778:
	cmpl	$47, %eax
	ja	.L779
	movl	%eax, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L780:
	movq	(%rcx), %rax
	jmp	.L772
.L557:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1176(%rbp)
	jmp	.L558
.L779:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L780
.L1710:
	movq	%r13, %r14
	movl	%r12d, %r13d
	jmp	.L469
.L237:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L238
.L872:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L871
.L1923:
	testb	%dil, %dil
	je	.L875
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L875:
	testb	%sil, %sil
	je	.L876
	movq	%rcx, -1176(%rbp)
.L876:
	cmpl	$47, %eax
	ja	.L877
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L878:
	movq	(%rdx), %rax
	movq	%rax, -1416(%rbp)
	jmp	.L870
.L225:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L224
.L823:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L824
.L196:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L197
.L1907:
	testb	%dil, %dil
	je	.L228
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L228:
	testb	%sil, %sil
	je	.L229
	movq	%rcx, -1176(%rbp)
.L229:
	cmpl	$47, %eax
	ja	.L230
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L231:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L223
.L877:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L878
.L230:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L231
.L941:
	movq	-1416(%rbp), %rcx
	movb	$0, (%rcx)
	addq	$1, %rcx
	testl	%eax, %eax
	movq	%rcx, -1416(%rbp)
	je	.L946
	jmp	.L1006
.L1064:
	movl	$112, -1392(%rbp)
	xorl	%ecx, %ecx
	jmp	.L682
.L514:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L513
.L1780:
	testb	%r10b, %r10b
	je	.L568
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %edx
.L568:
	testb	%dil, %dil
	je	.L569
	movq	%rsi, -1176(%rbp)
.L569:
	cmpl	$47, %edx
	ja	.L570
	movl	%edx, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1184(%rbp)
.L571:
	movq	(%rcx), %rdx
	movl	%eax, (%rdx)
	jmp	.L494
.L697:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L693
.L1931:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1364(%rbp), %edi
	movl	%edi, %fs:(%rax)
	jmp	.L693
.L1930:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L691
.L694:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L696
.L570:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1176(%rbp)
	jmp	.L571
.L175:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L174
.L1920:
	testb	%dil, %dil
	je	.L178
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L178:
	testb	%sil, %sil
	je	.L179
	movq	%rcx, -1176(%rbp)
.L179:
	cmpl	$47, %eax
	ja	.L180
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L181:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L173
.L241:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L240
.L1906:
	testb	%dil, %dil
	je	.L244
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L244:
	testb	%sil, %sil
	je	.L245
	movq	%rcx, -1176(%rbp)
.L245:
	cmpl	$47, %eax
	ja	.L246
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L247:
	movq	(%rdx), %rax
	movq	%rax, -1432(%rbp)
	jmp	.L239
.L180:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L181
.L246:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L247
.L561:
	movq	-1312(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L562
.L1853:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L122:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L121
.L1919:
	testb	%dil, %dil
	je	.L125
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L125:
	testb	%sil, %sil
	je	.L126
	movq	%rcx, -1176(%rbp)
.L126:
	cmpl	$47, %eax
	ja	.L127
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L128:
	movq	(%rdx), %rax
	jmp	.L120
.L1938:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1932:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L647
.L1866:
	movq	-1416(%rbp), %rdx
	movl	%r8d, -1400(%rbp)
	subq	%rdi, %rdx
	leaq	1(%rbx,%rdx), %rcx
	movq	%rdx, -1392(%rbp)
	movq	%rcx, %rsi
	movq	%rcx, -1352(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1352(%rbp), %rcx
	movq	-1392(%rbp), %rdx
	movl	-1400(%rbp), %r8d
	je	.L1964
	movq	-1376(%rbp), %rsi
	movq	%rcx, -1400(%rbp)
	movq	%rax, (%rsi)
	addq	%rdx, %rax
	movq	%rax, -1416(%rbp)
	jmp	.L323
.L332:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L333
.L573:
	movq	-1312(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L574
.L1964:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1047
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movb	$0, (%rax,%rdx)
.L325:
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1946:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1945:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1944:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L674
.L676:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L678
.L1047:
	movl	$-1, -1356(%rbp)
	jmp	.L325
.L1935:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1934:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L649:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L651
.L1933:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1937:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1936:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L656
.L658:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L660
.L127:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L128
.L800:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L799
.L1948:
	testb	%r8b, %r8b
	je	.L803
	movl	%ecx, -1184(%rbp)
	movl	%ecx, %eax
.L803:
	testb	%dil, %dil
	je	.L804
	movq	%rsi, -1176(%rbp)
.L804:
	cmpl	$47, %eax
	ja	.L805
	movl	%eax, %ecx
	addq	-1168(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L806:
	movq	(%rcx), %rax
	jmp	.L798
.L1943:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1942:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L805:
	movq	-1176(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L806
.L667:
	movq	%r13, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L669
.L1941:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L1940:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L665
.L1939:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L135:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L134
.L1924:
	testb	%dil, %dil
	je	.L138
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L138:
	testb	%sil, %sil
	je	.L139
	movq	%rcx, -1176(%rbp)
.L139:
	cmpl	$47, %eax
	ja	.L140
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L141:
	movq	(%rdx), %rax
	jmp	.L133
.L291:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L292
.L1070:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L140:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L141
.L1913:
	leaq	__PRETTY_FUNCTION__.12757(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$2884, %edx
	call	__GI___assert_fail
.L1925:
	movl	-1464(%rbp), %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1046
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movb	$0, (%rax,%r15)
	movl	-1356(%rbp), %eax
	movl	%eax, %ebx
	jmp	.L44
.L148:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L147
.L1949:
	testb	%dil, %dil
	je	.L151
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L151:
	testb	%sil, %sil
	je	.L152
	movq	%rcx, -1176(%rbp)
.L152:
	cmpl	$47, %eax
	ja	.L153
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L154:
	movq	(%rdx), %rax
	jmp	.L146
.L1046:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
.L153:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L154
.L827:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L826
.L1915:
	testb	%dil, %dil
	je	.L830
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L830:
	testb	%sil, %sil
	je	.L831
	movq	%rcx, -1176(%rbp)
.L831:
	cmpl	$47, %eax
	ja	.L832
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L833:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L825
.L796:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L797
.L808:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L809
.L832:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L833
.L1859:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L275:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L276
.L156:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L157
.L1951:
	movq	-1376(%rbp), %rax
	leaq	4(%rdx), %rsi
	movl	%r8d, -1464(%rbp)
	movl	%ecx, -1456(%rbp)
	addq	$1, -1400(%rbp)
	movq	%rdx, -1432(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1432(%rbp), %rdx
	movl	-1456(%rbp), %ecx
	movl	-1464(%rbp), %r8d
	je	.L1965
	movq	-1376(%rbp), %rdi
	movq	%rax, (%rdi)
	addq	%rdx, %rax
	movq	%rax, -1432(%rbp)
	jmp	.L901
.L548:
	movq	-1312(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1312(%rbp)
	jmp	.L549
.L336:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L335
.L1904:
	testb	%dil, %dil
	je	.L339
	movl	%edx, -1184(%rbp)
	movl	%edx, %eax
.L339:
	testb	%sil, %sil
	je	.L340
	movq	%rcx, -1176(%rbp)
.L340:
	cmpl	$47, %eax
	ja	.L341
	movl	%eax, %edx
	addq	-1168(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1184(%rbp)
.L342:
	movq	(%rdx), %rax
	movq	%rax, -1376(%rbp)
	jmp	.L334
.L733:
	movq	-1480(%rbp), %rsi
	movl	$44, %edi
	call	__GI___towctrans
	testl	%eax, %eax
	movl	%eax, -1144(%rbp)
	setne	%al
	andb	%al, -1488(%rbp)
	jmp	.L734
.L341:
	movq	-1176(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1176(%rbp)
	jmp	.L342
.L1958:
	movq	%r14, %r13
	movl	%r15d, %r14d
.L748:
	cmpl	$-1, %r12d
	je	.L724
	movl	%r12d, %esi
	movq	%r13, %rdi
	subq	$1, -1352(%rbp)
	call	__GI__IO_sputbackwc
	movq	-1104(%rbp), %rdx
	jmp	.L724
.L1957:
	leal	-43(%r12), %esi
	andl	$-3, %esi
	jne	.L739
	cmpq	-1096(%rbp), %rdx
	je	.L1966
	leaq	4(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movl	%r12d, (%rdx)
	jmp	.L741
.L755:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movq	-1104(%rbp), %rdx
	movl	%r15d, %r14d
	movl	%fs:(%rax), %eax
	movl	%eax, -1364(%rbp)
	jmp	.L724
.L1960:
	movq	%r14, %r13
	movq	-1104(%rbp), %rdx
	movl	%r15d, %r14d
	jmp	.L724
.L752:
	movq	%r14, %rdi
	call	__GI___wuflow
	movl	%eax, %r12d
	jmp	.L754
.L1966:
	movq	-1384(%rbp), %rdi
	movl	%r12d, %esi
	call	char_buffer_add_slow
	jmp	.L741
.L1959:
	movq	-1384(%rbp), %rdi
	call	char_buffer_add_slow
	jmp	.L741
.L744:
	cmpl	$11, %eax
	jne	.L746
	testb	$1, %bl
	jne	.L746
	cmpq	-1096(%rbp), %rdx
	je	.L1967
	leaq	4(%rdx), %rax
	movl	$1, %ebx
	movq	%rax, -1104(%rbp)
	movl	-1448(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L741
.L738:
	movq	%rdx, %rax
	subq	-1088(%rbp), %rax
	sarq	$2, %rax
	cmpq	%rax, -1464(%rbp)
	jnb	.L1067
	movl	%r12d, %edi
	movq	%rdx, -1480(%rbp)
	call	__GI_towlower
	cmpl	-1392(%rbp), %eax
	movq	-1480(%rbp), %rdx
	jne	.L1067
	cmpq	-1096(%rbp), %rdx
	je	.L1968
	leaq	4(%rdx), %rax
	movb	$1, -1456(%rbp)
	movl	$1, %ebx
	movq	%rax, -1104(%rbp)
	movl	-1392(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L741
.L1956:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L44
.L1968:
	movl	-1392(%rbp), %esi
	movq	-1384(%rbp), %rdi
	movl	$1, %ebx
	call	char_buffer_add_slow
	movb	$1, -1456(%rbp)
	jmp	.L741
.L1067:
	xorl	%eax, %eax
	jmp	.L739
.L1967:
	movl	-1448(%rbp), %esi
	movq	-1384(%rbp), %rdi
	movl	$1, %ebx
	call	char_buffer_add_slow
	jmp	.L741
.L746:
	movl	%ebx, %ecx
	xorl	$1, %ecx
	testb	%cl, -1488(%rbp)
	je	.L1068
	cmpl	$10, %edi
	jne	.L1068
	cmpq	-1096(%rbp), %rdx
	je	.L1969
	leaq	4(%rdx), %rax
	xorl	%ebx, %ebx
	movq	%rax, -1104(%rbp)
	movl	-1360(%rbp), %eax
	movl	%eax, (%rdx)
	jmp	.L741
.L1809:
	movq	%r13, %r14
	movl	-1356(%rbp), %ebx
	jmp	.L44
.L221:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L222
.L144:
	movq	-1312(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1312(%rbp)
	jmp	.L145
.L1965:
	movl	%r14d, %ebx
	movq	%r13, %r14
	andb	$32, %bh
	jne	.L1077
	movq	-1376(%rbp), %rax
	addl	$1, -1356(%rbp)
	movq	$0, -1376(%rbp)
	movq	(%rax), %rax
	movl	$0, -4(%rax,%rdx)
	movl	-1356(%rbp), %eax
	movl	%eax, %ebx
	jmp	.L44
.L1969:
	movl	-1360(%rbp), %esi
	movq	-1384(%rbp), %rdi
	xorl	%ebx, %ebx
	call	char_buffer_add_slow
	jmp	.L741
.L1068:
	movq	%r14, %r13
	movl	%esi, %r12d
	movl	%r15d, %r14d
	jmp	.L748
.L1077:
	orl	$-1, %ebx
	movl	%ebx, -1356(%rbp)
	jmp	.L44
	.size	__vfwscanf_internal, .-__vfwscanf_internal
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12757, @object
	.size	__PRETTY_FUNCTION__.12757, 20
__PRETTY_FUNCTION__.12757:
	.string	"__vfwscanf_internal"
	.hidden	__wmemcpy
	.hidden	__lll_lock_wait_private
	.hidden	__wcrtomb
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
