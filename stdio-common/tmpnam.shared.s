	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.tmpnam
	.previous
#NO_APP
	.p2align 4,,15
	.globl	tmpnam
	.type	tmpnam, @function
tmpnam:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$20, %esi
	subq	$40, %rsp
	testq	%rdi, %rdi
	movq	%rsp, %rbx
	cmovne	%rdi, %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	__path_search
	testl	%eax, %eax
	jne	.L5
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rbx, %rdi
	call	__gen_tempname
	testl	%eax, %eax
	jne	.L5
	testq	%rbp, %rbp
	jne	.L4
	movdqu	(%rbx), %xmm0
	leaq	tmpnam_buffer(%rip), %rbp
	movaps	%xmm0, tmpnam_buffer(%rip)
	movl	16(%rbx), %eax
	movl	%eax, 16+tmpnam_buffer(%rip)
.L4:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%ebp, %ebp
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	tmpnam, .-tmpnam
	.section	.gnu.warning.tmpnam
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_tmpnam, @object
	.size	__evoke_link_warning_tmpnam, 55
__evoke_link_warning_tmpnam:
	.string	"the use of `tmpnam' is dangerous, better use `mkstemp'"
	.local	tmpnam_buffer
	.comm	tmpnam_buffer,20,16
	.hidden	__gen_tempname
	.hidden	__path_search
