	.text
	.p2align 4,,15
	.globl	__ftrylockfile
	.type	__ftrylockfile, @function
__ftrylockfile:
	movq	136(%rdi), %rdx
	movq	%fs:16, %rcx
	cmpq	%rcx, 8(%rdx)
	je	.L2
#APP
# 28 "../sysdeps/pthread/ftrylockfile.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, (%rdx)
# 0 "" 2
#NO_APP
.L4:
	testl	%eax, %eax
	jne	.L6
	movq	136(%rdi), %rdx
	movq	%rcx, 8(%rdx)
	movl	$1, 4(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$1, 4(%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, (%rdx)
	setne	%al
	movzbl	%al, %eax
	jmp	.L4
	.size	__ftrylockfile, .-__ftrylockfile
	.weak	ftrylockfile
	.set	ftrylockfile,__ftrylockfile
	.globl	_IO_ftrylockfile
	.set	_IO_ftrylockfile,__ftrylockfile
