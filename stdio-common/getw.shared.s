	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getw
	.type	getw, @function
getw:
	subq	$24, %rsp
	movq	%rdi, %rcx
	movl	$1, %edx
	leaq	12(%rsp), %rdi
	movl	$4, %esi
	call	__GI__IO_fread
	cmpq	$1, %rax
	jne	.L3
	movl	12(%rsp), %eax
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	getw, .-getw
