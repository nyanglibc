	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	": "
.LC2:
	.string	"%s%s%s\n"
#NO_APP
	.text
	.p2align 4,,15
	.type	perror_internal, @function
perror_internal:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%edx, %edi
	subq	$1024, %rsp
	testq	%rsi, %rsi
	je	.L4
	cmpb	$0, (%rsi)
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %rbp
	je	.L4
.L2:
	movq	%rsp, %rsi
	movl	$1024, %edx
	call	__GI___strerror_r
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$1024, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC0(%rip), %rbp
	movq	%rbp, %rbx
	jmp	.L2
	.size	perror_internal, .-perror_internal
	.section	.rodata.str1.1
.LC3:
	.string	"w+"
	.text
	.p2align 4,,15
	.globl	__GI_perror
	.hidden	__GI_perror
	.type	__GI_perror, @function
__GI_perror:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$24, %rsp
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	(%rbx), %rdi
	movl	%fs:(%rax), %edx
	movl	192(%rdi), %eax
	testl	%eax, %eax
	je	.L20
.L8:
	addq	$24, %rsp
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	perror_internal
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%edx, 12(%rsp)
	call	__GI___fileno
	cmpl	$-1, %eax
	movl	12(%rsp), %edx
	jne	.L9
.L19:
	movq	(%rbx), %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%eax, %edi
	movl	%edx, 12(%rsp)
	call	__GI___dup
	cmpl	$-1, %eax
	movl	%eax, %r12d
	movl	12(%rsp), %edx
	je	.L19
	leaq	.LC3(%rip), %rsi
	movl	%eax, %edi
	movl	%edx, 12(%rsp)
	call	_IO_new_fdopen@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movl	12(%rsp), %edx
	je	.L21
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	perror_internal
	testb	$32, 0(%r13)
	je	.L13
	movq	(%rbx), %rax
	orl	$32, (%rax)
.L13:
	addq	$24, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	_IO_new_fclose@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r12d, %edi
	movl	%edx, 12(%rsp)
	call	__GI___close
	movq	(%rbx), %rdi
	movl	12(%rsp), %edx
	jmp	.L8
	.size	__GI_perror, .-__GI_perror
	.globl	perror
	.set	perror,__GI_perror
	.hidden	__fxprintf
