	.text
	.p2align 4,,15
	.type	try_nocreate, @function
try_nocreate:
.LFB69:
	.cfi_startproc
	subq	$152, %rsp
	.cfi_def_cfa_offset 160
	movq	%rsp, %rsi
	call	__lstat64
	testl	%eax, %eax
	movq	__libc_errno@gottpoff(%rip), %rax
	je	.L2
	movl	%fs:(%rax), %edx
	cmpl	$75, %edx
	je	.L2
	xorl	%eax, %eax
	cmpl	$2, %edx
	setne	%al
	addq	$152, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movl	$17, %fs:(%rax)
	movl	$-1, %eax
	addq	$152, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE69:
	.size	try_nocreate, .-try_nocreate
	.p2align 4,,15
	.type	try_dir, @function
try_dir:
.LFB68:
	.cfi_startproc
	movl	$448, %esi
	jmp	__mkdir
	.cfi_endproc
.LFE68:
	.size	try_dir, .-try_dir
	.p2align 4,,15
	.type	try_file, @function
try_file:
.LFB67:
	.cfi_startproc
	movl	(%rsi), %esi
	movl	$384, %edx
	xorl	%eax, %eax
	andb	$60, %sil
	orb	$-62, %sil
	jmp	__open
	.cfi_endproc
.LFE67:
	.size	try_file, .-try_file
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"file"
.LC1:
	.string	"/tmp"
.LC2:
	.string	"TMPDIR"
.LC3:
	.string	"%.*s/%.*sXXXXXX"
	.text
	.p2align 4,,15
	.globl	__path_search
	.hidden	__path_search
	.type	__path_search, @function
__path_search:
.LFB66:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r13
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rsi, %r12
	movq	%rdx, %rbx
	subq	$168, %rsp
	.cfi_def_cfa_offset 224
	testq	%rcx, %rcx
	je	.L33
	cmpb	$0, (%rcx)
	movq	%rcx, %r14
	jne	.L46
.L33:
	testl	%r8d, %r8d
	movl	$4, %ebp
	leaq	.LC0(%rip), %r14
	jne	.L47
.L16:
	testq	%rbx, %rbx
	je	.L48
.L19:
	movq	%rbx, %rdi
	call	strlen
	cmpq	$1, %rax
	jbe	.L34
	cmpb	$47, -1(%rbx,%rax)
	leaq	-1(%rax), %rdx
	je	.L29
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	$47, -1(%rbx,%rdx)
	leaq	-1(%rdx), %rdi
	jne	.L28
	movq	%rdi, %rdx
.L29:
	cmpq	$1, %rdx
	jne	.L30
.L28:
	leaq	8(%rbp,%rdx), %rax
	cmpq	%r12, %rax
	ja	.L49
	leaq	.LC3(%rip), %rsi
	movq	%r14, %r9
	movl	%ebp, %r8d
	movq	%rbx, %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	sprintf
	xorl	%eax, %eax
.L14:
	addq	$168, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movq	%rcx, %rdi
	movl	%r8d, 8(%rsp)
	movl	$5, %ebp
	call	strlen
	movl	8(%rsp), %r8d
	cmpq	$5, %rax
	cmovbe	%rax, %rbp
	testl	%r8d, %r8d
	je	.L16
.L47:
	leaq	.LC2(%rip), %rdi
	leaq	16(%rsp), %r15
	call	__libc_secure_getenv
	testq	%rax, %rax
	je	.L17
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	__stat64
	testl	%eax, %eax
	jne	.L17
	movl	40(%rsp), %eax
	movq	8(%rsp), %rdx
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L50
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%rbx, %rbx
	je	.L21
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__stat64
	testl	%eax, %eax
	jne	.L21
	movl	40(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	16(%rsp), %r15
.L21:
	leaq	.LC1(%rip), %rdi
	movq	%r15, %rsi
	call	__stat64
	testl	%eax, %eax
	jne	.L25
	movl	40(%rsp), %eax
	leaq	.LC1(%rip), %rbx
	movl	$4, %edx
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L28
.L25:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, %rdx
	jmp	.L28
.L50:
	movq	%rdx, %rbx
	jmp	.L19
.L49:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L14
	.cfi_endproc
.LFE66:
	.size	__path_search, .-__path_search
	.section	.rodata.str1.1
.LC4:
	.string	"X"
	.text
	.p2align 4,,15
	.globl	__gen_tempname
	.hidden	__gen_tempname
	.type	__gen_tempname, @function
__gen_tempname:
.LFB72:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movslq	%ecx, %rax
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r15
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movslq	%esi, %rbx
	subq	$104, %rsp
	.cfi_def_cfa_offset 160
	movl	%edx, 60(%rsp)
	leaq	tryfunc.8752(%rip), %rdx
	leaq	64(%rsp), %rcx
	movq	%rdi, 24(%rsp)
	movq	(%rdx,%rax,8), %rsi
	movq	__libc_errno@gottpoff(%rip), %rax
	shrq	$4, %rcx
	movq	%rcx, %r14
	movq	%rcx, 64(%rsp)
	movl	%fs:(%rax), %eax
	movq	%rsi, 16(%rsp)
	movl	%eax, 44(%rsp)
	leaq	try_nocreate(%rip), %rax
	cmpq	%rax, %rsi
	sete	%r9b
	movb	%r9b, 8(%rsp)
	call	strlen
	leaq	6(%rbx), %rdx
	cmpq	%rdx, %rax
	jb	.L52
	subq	%rbx, %rax
	leaq	.LC4(%rip), %rsi
	leaq	-6(%r15,%rax), %rax
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	strspn
	cmpq	$5, %rax
	movzbl	8(%rsp), %r9d
	jbe	.L52
	leaq	60(%rsp), %rax
	leaq	letters(%rip), %r15
	movl	$238328, 40(%rsp)
	xorl	%r8d, %r8d
	movabsq	$-821457390474406913, %rbp
	movabsq	$2862933555777941757, %r13
	movq	%rax, 32(%rsp)
	movl	$3037000493, %r12d
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%ebx, %ebx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L71:
	subl	$1, %r8d
.L56:
	movq	%r14, %rdx
	movabsq	$-8925843906633654007, %rax
	movq	%r14, %rcx
	shrq	%rdx
	movq	(%rsp), %rdi
	mulq	%rdx
	shrq	$4, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r14
	movq	%rdx, 64(%rsp)
	salq	$5, %rax
	subq	%rdx, %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	movzbl	(%r15,%rcx), %eax
	movb	%al, (%rdi,%rbx)
	addq	$1, %rbx
	cmpq	$6, %rbx
	je	.L70
.L60:
	testl	%r8d, %r8d
	jne	.L71
	testb	%r9b, %r9b
	jne	.L59
.L57:
	leaq	80(%rsp), %rsi
	movl	$1, %edi
	call	__clock_gettime
	movq	88(%rsp), %rcx
	xorq	%r14, %rcx
	imulq	%r13, %rcx
	leaq	(%rcx,%r12), %r14
	cmpq	%rbp, %r14
	movq	%r14, 64(%rsp)
	jbe	.L72
.L59:
	leaq	72(%rsp), %rdi
	movl	$1, %edx
	movl	$8, %esi
	call	__getrandom
	cmpq	$8, %rax
	jne	.L57
	movq	72(%rsp), %r14
	cmpq	%rbp, %r14
	movq	%r14, 64(%rsp)
	ja	.L59
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$9, %r8d
	movl	$1, %r9d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L70:
	movb	%r9b, 15(%rsp)
	movl	%r8d, 8(%rsp)
	movq	32(%rsp), %rsi
	movq	24(%rsp), %rdi
	movq	16(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	movl	8(%rsp), %r8d
	movzbl	15(%rsp), %r9d
	jns	.L73
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$17, %fs:(%rax)
	jne	.L64
	subl	$1, 40(%rsp)
	jne	.L53
.L64:
	movl	$-1, %eax
.L51:
	addq	$104, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L73:
	.cfi_restore_state
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	44(%rsp), %esi
	movl	%esi, %fs:(%rbx)
	addq	$104, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L52:
	.cfi_restore_state
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L51
	.cfi_endproc
.LFE72:
	.size	__gen_tempname, .-__gen_tempname
	.section	.data.rel.ro.local,"aw",@progbits
	.align 16
	.type	tryfunc.8752, @object
	.size	tryfunc.8752, 24
tryfunc.8752:
	.quad	try_file
	.quad	try_dir
	.quad	try_nocreate
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	letters, @object
	.size	letters, 63
letters:
	.string	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	.hidden	__getrandom
	.hidden	__clock_gettime
	.hidden	strspn
	.hidden	__stat64
	.hidden	__libc_secure_getenv
	.hidden	sprintf
	.hidden	strlen
	.hidden	__open
	.hidden	__mkdir
	.hidden	__lstat64
