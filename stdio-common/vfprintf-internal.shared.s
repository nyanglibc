	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rcx
	movl	$-1, %esi
	movl	$2147483647, %r9d
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-48(%rdx), %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	leal	(%rax,%rax,4), %eax
	movl	%r9d, %r8d
	subl	%edx, %r8d
	addl	%eax, %eax
	addl	%eax, %edx
	cmpl	%eax, %r8d
	movl	%edx, %eax
	cmovl	%esi, %eax
.L3:
	addq	$1, %rcx
.L2:
	movq	%rcx, (%rdi)
	movzbl	(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L8
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jle	.L9
	movl	%esi, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	rep ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	group_number, @function
group_number:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rdi
	movq	%r8, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	subq	$24, %rsp
	call	__GI_strlen
	movsbl	(%r15), %ebp
	movq	%rax, %r8
	leal	-1(%rbp), %eax
	cmpb	$125, %al
	jbe	.L24
.L11:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	subq	%rbx, %rcx
	movq	%r8, 8(%rsp)
	addq	$1, %r15
	movq	%rcx, %rdx
	movq	%rcx, (%rsp)
	call	__GI_memmove
	movq	(%rsp), %rcx
	leaq	0(%r13,%rcx), %rsi
	cmpq	%rsi, %r13
	jnb	.L21
	movq	8(%rsp), %r8
	movslq	%r8d, %rax
	.p2align 4,,10
	.p2align 3
.L12:
	subq	$1, %rsi
	movzbl	(%rsi), %edx
	subl	$1, %ebp
	leaq	-1(%r12), %rbx
	movb	%dl, -1(%r12)
	jne	.L13
	cmpq	%rsi, %r13
	jnb	.L11
	movq	%rbx, %rdx
	subq	%rsi, %rdx
	cmpq	%rdx, %rax
	jge	.L14
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	-1(%r14,%rdi), %r9d
	subq	$1, %rdi
	subq	$1, %rbx
	testl	%edi, %edi
	movb	%r9b, (%rbx)
	jg	.L15
	movsbl	(%r15), %ebp
	cmpb	$127, %bpl
	je	.L14
	testb	%bpl, %bpl
	js	.L14
	testb	%bpl, %bpl
	je	.L18
	addq	$1, %r15
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbx, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	%rsi, %r13
	jb	.L20
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	movsbl	-1(%r15), %ebp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	__GI_memmove
	jmp	.L11
.L21:
	movq	%r12, %rbx
	jmp	.L11
	.size	group_number, .-group_number
	.p2align 4,,15
	.type	_IO_helper_overflow, @function
_IO_helper_overflow:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rcx
	movq	32(%rdi), %rsi
	movq	%rcx, %rbp
	subq	%rsi, %rbp
	testl	%ebp, %ebp
	je	.L26
	movq	224(%rdi), %rdi
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	216(%rdi), %r13
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L32
.L27:
	movslq	%ebp, %rbp
	movq	%rbp, %rdx
	call	*56(%r13)
	movq	%rax, %r13
	leaq	-1(%rax), %rax
	cmpq	$-3, %rax
	ja	.L30
	movq	32(%rbx), %rdi
	movq	%rbp, %rdx
	subq	%r13, %rdx
	leaq	(%rdi,%r13), %rsi
	call	__GI_memmove
	movq	40(%rbx), %rcx
	subq	%r13, %rcx
	movq	%rcx, 40(%rbx)
.L26:
	cmpq	%rcx, 48(%rbx)
	movzbl	%r12b, %eax
	jbe	.L33
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rcx)
.L25:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	32(%rbx), %rsi
	movq	8(%rsp), %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$24, %rsp
	movq	%rbx, %rdi
	movl	%eax, %esi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI___overflow
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %eax
	jmp	.L25
	.size	_IO_helper_overflow, .-_IO_helper_overflow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_outpunct"
	.text
	.p2align 4,,15
	.type	_i18n_number_rewrite, @function
_i18n_number_rewrite:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %rbx
	subq	$1128, %rsp
	call	__wctrans@PLT
	movl	$46, %edi
	movq	%rax, %rbp
	movq	%rax, %rsi
	call	__GI___towctrans
	movq	%rbp, %rsi
	movl	$44, %edi
	movl	%eax, %r15d
	call	__GI___towctrans
	testq	%rbp, %rbp
	leaq	80(%rsp), %r12
	jne	.L71
.L35:
	leaq	16(%r12), %rax
	subq	%r14, %r13
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$1024, 88(%rsp)
	movq	%rax, 80(%rsp)
	call	__GI___libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L52
	movq	80(%rsp), %r15
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__GI_mempcpy@PLT
	movq	%rax, %r13
	leaq	48(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 8(%rsp)
.L41:
	subq	$1, %r13
	cmpq	%r13, %r15
	ja	.L72
.L51:
	movsbl	0(%r13), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L73
	testq	%rbp, %rbp
	jne	.L74
.L45:
	subq	$1, %r13
	movb	%al, -1(%rbx)
	subq	$1, %rbx
	cmpq	%r13, %r15
	jbe	.L51
.L72:
	movq	80(%rsp), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L34
	call	free@PLT
.L34:
	addq	$1128, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	subl	$7, %eax
	cltq
	movq	%fs:(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx,%rax,8), %r14
	movq	%r14, %rdi
	call	__GI_strlen
	subq	%rax, %rbx
	testq	%rax, %rax
	leaq	-1(%rax), %rcx
	je	.L41
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	(%r14,%rcx), %eax
	movb	%al, (%rbx,%rcx)
	subq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L74:
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpb	$44, %cl
	jne	.L45
	cmpb	$46, %al
	movq	(%rsp), %rsi
	cmove	8(%rsp), %rsi
	movq	%rsi, %rcx
.L48:
	movl	(%rcx), %edi
	addq	$4, %rcx
	leal	-16843009(%rdi), %eax
	notl	%edi
	andl	%edi, %eax
	andl	$-2139062144, %eax
	je	.L48
	movl	%eax, %edi
	shrl	$16, %edi
	testl	$32896, %eax
	cmove	%edi, %eax
	leaq	2(%rcx), %rdi
	movl	%eax, %edx
	cmove	%rdi, %rcx
	addb	%al, %dl
	sbbq	$3, %rcx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rax
	je	.L41
	.p2align 4,,10
	.p2align 3
.L50:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L50
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	16(%rsp), %rdi
	movq	%r12, %rdx
	movl	%r15d, %esi
	movl	%eax, (%rsp)
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movl	(%rsp), %ecx
	je	.L75
	movb	$0, 16(%rsp,%rax)
.L37:
	leaq	48(%rsp), %rdi
	movq	%r12, %rdx
	movl	%ecx, %esi
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L76
	movb	$0, 48(%rsp,%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r14, %rbx
	jmp	.L34
.L75:
	movl	$46, %edx
	movw	%dx, 16(%rsp)
	jmp	.L37
.L76:
	movl	$44, %eax
	movw	%ax, 48(%rsp)
	jmp	.L35
	.size	_i18n_number_rewrite, .-_i18n_number_rewrite
	.section	.rodata.str1.1
.LC1:
	.string	"vfprintf-internal.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(size_t) done <= (size_t) INT_MAX"
	.section	.text.unlikely,"ax",@progbits
	.type	outstring_func.part.1, @function
outstring_func.part.1:
	leaq	__PRETTY_FUNCTION__.12638(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$238, %edx
	call	__GI___assert_fail
	.size	outstring_func.part.1, .-outstring_func.part.1
	.text
	.p2align 4,,15
	.type	outstring_converted_wide_string, @function
outstring_converted_wide_string:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%r9d, %ebx
	subq	$328, %rsp
	testl	%ecx, %ecx
	setg	23(%rsp)
	cmpb	$1, %r8b
	movq	%rsi, 40(%rsp)
	movl	%edx, 16(%rsp)
	movl	%ecx, 24(%rsp)
	movl	%r8d, 28(%rsp)
	movzbl	23(%rsp), %edi
	je	.L80
	testb	%dil, %dil
	je	.L80
	testl	%edx, %edx
	movq	$0, 56(%rsp)
	movq	%rsi, 48(%rsp)
	js	.L161
	movslq	16(%rsp), %r14
	movl	$0, %r12d
	je	.L83
	testq	%rsi, %rsi
	je	.L83
	leaq	64(%rsp), %rax
	movq	%r14, %rbx
	leaq	56(%rsp), %r15
	leaq	48(%rsp), %r13
	movl	%r9d, 8(%rsp)
	movq	%rax, %r14
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L162:
	testq	%rax, %rax
	je	.L155
	addq	%rax, %r12
	subq	%rax, %rbx
	je	.L155
	cmpq	$0, 48(%rsp)
	je	.L155
.L86:
	cmpq	$256, %rbx
	movl	$256, %edx
	movq	%r15, %rcx
	cmovb	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__wcsrtombs
	cmpq	$-1, %rax
	jne	.L162
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$-1, %eax
.L79:
	addq	$328, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	movl	8(%rsp), %ebx
.L82:
	movslq	24(%rsp), %rax
	cmpq	%r12, %rax
	ja	.L83
.L87:
	movq	40(%rsp), %rsi
.L80:
	movslq	16(%rsp), %r8
	movq	$-1, %rax
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	$0, 56(%rsp)
	testl	%r8d, %r8d
	cmovns	%r8, %rax
	xorl	%r12d, %r12d
	movq	%rax, %r14
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%r14, %r15
	subq	%rcx, %rax
	movq	%rax, 8(%rsp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	*56(%rax)
	cmpq	%rax, %r13
	jne	.L159
	movslq	%ebx, %rbx
	movabsq	$-9223372036854775808, %rdi
	leaq	(%rbx,%r13), %rax
	leaq	(%rax,%rdi), %rdx
	movslq	%eax, %rcx
	movl	%eax, %ebx
	cmpq	%r13, %rdx
	setb	%dl
	cmpq	%rcx, %rax
	movl	$1, %ecx
	movzbl	%dl, %edx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L158
	testl	%eax, %eax
	js	.L79
	movl	16(%rsp), %edx
	movq	%r15, %rax
	movq	40(%rsp), %rsi
	subq	%r13, %rax
	addq	%r13, %r12
	testl	%edx, %edx
	cmovns	%rax, %r15
.L94:
	testq	%r15, %r15
	je	.L110
	testq	%rsi, %rsi
	je	.L110
	leaq	64(%rsp), %r14
	cmpq	$256, %r15
	movl	$256, %edx
	leaq	56(%rsp), %rcx
	leaq	40(%rsp), %rsi
	cmovb	%r15, %rdx
	movq	%r14, %rdi
	call	__wcsrtombs
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L159
	testq	%rax, %rax
	je	.L110
	testl	%ebx, %ebx
	js	.L163
	movq	216(%rbp), %rax
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%rdx, 8(%rsp)
	ja	.L101
	movq	%rax, 32(%rsp)
	call	_IO_vtable_check
	movq	32(%rsp), %rax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L110:
	cmpb	$0, 23(%rsp)
	je	.L125
	cmpb	$0, 28(%rsp)
	je	.L125
	movslq	24(%rsp), %rdx
	movl	%ebx, %eax
	cmpq	%r12, %rdx
	jbe	.L79
	movl	24(%rsp), %edx
	subl	%r12d, %edx
	testl	%edx, %edx
	jle	.L79
	movslq	%edx, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	__GI__IO_padn
	movq	%rax, %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r12
	jne	.L79
	testl	%ebx, %ebx
	movl	%ebx, %eax
	js	.L79
	addl	%r12d, %eax
	jno	.L79
	.p2align 4,,10
	.p2align 3
.L158:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L83:
	movl	24(%rsp), %eax
	subl	%r12d, %eax
	testl	%eax, %eax
	jle	.L88
	movslq	%eax, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L159
	testl	%ebx, %ebx
	jns	.L164
	.p2align 4,,10
	.p2align 3
.L125:
	movl	%ebx, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	__wcsrtombs
	movq	%rax, %r12
	jmp	.L82
.L164:
	addl	%r12d, %ebx
	jo	.L158
.L88:
	testl	%ebx, %ebx
	jns	.L87
	movl	%ebx, %eax
	jmp	.L79
.L163:
	call	outstring_func.part.1
	.size	outstring_converted_wide_string, .-outstring_converted_wide_string
	.section	.rodata.str1.1
.LC3:
	.string	"(nil)"
.LC4:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(mode_flags & PRINTF_FORTIFY) != 0"
	.align 8
.LC6:
	.string	"*** invalid %N$ use detected ***\n"
	.align 8
.LC7:
	.string	"*** %n in writable segment detected ***\n"
	.text
	.p2align 4,,15
	.type	printf_positional, @function
printf_positional:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-2128(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2280, %rsp
	movq	%rax, -2192(%rbp)
	addq	$16, %rax
	movq	%rax, -2128(%rbp)
	leaq	-1088(%rbp), %rax
	movq	%r8, -2200(%rbp)
	movq	%rdi, -2224(%rbp)
	movq	%rsi, -2320(%rbp)
	movq	%rax, -2288(%rbp)
	addq	$16, %rax
	cmpq	$-1, 48(%rbp)
	movl	%edx, -2308(%rbp)
	movl	%r9d, -2208(%rbp)
	movq	24(%rbp), %r8
	movq	$1024, -2120(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$1024, -1080(%rbp)
	movq	$0, -2160(%rbp)
	je	.L785
.L166:
	cmpb	$0, (%r8)
	je	.L786
	movq	-2192(%rbp), %rax
	movl	$14, %r12d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%r12, -2168(%rbp)
	movq	%r8, %r14
	leaq	16(%rax), %r13
	leaq	-2160(%rbp), %rax
	movq	%rax, -2176(%rbp)
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	(%r15,%r15,8), %rax
	movq	-2176(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %r15
	leaq	0(%r13,%rax,8), %r12
	movq	%r12, %rdx
	call	__parse_one_specmb
	movq	32(%r12), %r14
	addq	%rax, %rbx
	cmpb	$0, (%r14)
	je	.L787
	cmpq	%r15, -2168(%rbp)
	jne	.L167
	movq	-2192(%rbp), %rdi
	call	__GI___libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L770
	movabsq	$-2049638230412172401, %rax
	movq	-2128(%rbp), %r13
	mulq	-2120(%rbp)
	shrq	$6, %rdx
	movq	%rdx, -2168(%rbp)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L822:
	movq	%r12, %rdx
	movl	$0, %eax
	subq	%rcx, %rdx
	cmovs	%rax, %rdx
	movl	-2208(%rbp), %eax
	movslq	%edx, %r12
	testl	%eax, %eax
	jne	.L256
	subl	%ecx, %ebx
	subl	%edx, %ebx
	testq	%r13, %r13
	je	.L262
.L447:
	cmpl	$16, %r9d
	jne	.L262
	movl	-2236(%rbp), %esi
	leal	-2(%rbx), %eax
	testl	%esi, %esi
	cmovne	%eax, %ebx
.L262:
	movl	-2224(%rbp), %eax
	orl	-2232(%rbp), %eax
	orl	%r10d, %eax
	je	.L263
	subl	$1, %ebx
	cmpb	$32, -2272(%rbp)
	je	.L449
.L264:
	testl	%r10d, %r10d
	je	.L269
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L788
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$45, (%rax)
.L277:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L770
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
.L272:
	testq	%r13, %r13
	je	.L278
	cmpl	$16, %r9d
	jne	.L278
	movl	-2236(%rbp), %eax
	testl	%eax, %eax
	je	.L278
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L789
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$48, (%rax)
.L280:
	cmpl	$2147483647, -2168(%rbp)
	je	.L770
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L790
	movzbl	-2200(%rbp), %edi
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	%dil, (%rax)
.L282:
	movl	-2168(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L770
	addl	$2, %eax
	movl	%eax, -2168(%rbp)
.L278:
	addl	%ebx, %r12d
	testl	%r12d, %r12d
	jle	.L283
	movslq	%r12d, %r12
	movl	$48, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rcx, -2208(%rbp)
	movq	%r11, -2200(%rbp)
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	movl	%eax, %edx
	movq	-2200(%rbp), %r11
	movq	-2208(%rbp), %rcx
	addl	%r12d, %edx
	movl	%edx, -2168(%rbp)
	jo	.L367
.L283:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	movq	216(%r15), %rbx
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%rbx, %rax
	subq	%rdi, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L791
.L286:
	movq	%rcx, %rdx
	movq	%rcx, -2200(%rbp)
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	*56(%rbx)
	movq	-2200(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L770
	movslq	-2168(%rbp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rcx, %rax
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
.L783:
	setb	%dl
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movzbl	%dl, %edx
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	je	.L767
	.p2align 4,,10
	.p2align 3
.L367:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L770:
	movl	$-1, -2168(%rbp)
.L213:
	movq	-2288(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L438
	call	free@PLT
.L438:
	movq	-2192(%rbp), %rax
	movq	-2128(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L165
	call	free@PLT
.L165:
	movl	-2168(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	cmpq	%rbx, -2160(%rbp)
	cmovnb	-2160(%rbp), %rbx
.L168:
	movq	-2288(%rbp), %rdi
	movl	$24, %edx
	movq	%rbx, %rsi
	call	__GI___libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L770
	movq	-1088(%rbp), %rax
	movq	%rbx, %r12
	leaq	0(,%rbx,4), %rdx
	salq	$4, %r12
	addq	%rax, %r12
	movq	%rax, -2184(%rbp)
	movl	64(%rbp), %eax
	movq	%r12, -2168(%rbp)
	addq	%rdx, %r12
	movq	%r12, %rdi
	andl	$2, %eax
	setne	%sil
	movl	%eax, -2296(%rbp)
	movzbl	%sil, %esi
	negl	%esi
	call	__GI_memset
	testq	%r15, %r15
	je	.L172
	leaq	(%r15,%r15,8), %rax
	movq	%r13, %r14
	movq	%r13, -2176(%rbp)
	movq	%rbx, -2216(%rbp)
	movq	-2168(%rbp), %rbx
	leaq	0(%r13,%rax,8), %r8
	movq	%r8, %r13
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L793:
	movslq	48(%r14), %rax
	movl	52(%r14), %edx
	movl	%edx, (%r12,%rax,4)
	movslq	48(%r14), %rax
	movl	64(%r14), %edx
	movl	%edx, (%rbx,%rax,4)
.L176:
	addq	$72, %r14
	cmpq	%r14, %r13
	je	.L792
.L178:
	movslq	44(%r14), %rax
	cmpl	$-1, %eax
	je	.L173
	movl	$0, (%r12,%rax,4)
.L173:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L174
	movl	$0, (%r12,%rax,4)
.L174:
	movq	56(%r14), %rsi
	testq	%rsi, %rsi
	je	.L176
	cmpq	$1, %rsi
	je	.L793
	movslq	48(%r14), %rdx
	movslq	8(%r14), %rax
	movq	%r14, %rdi
	movq	__printf_arginfo_table(%rip), %r10
	addq	$72, %r14
	salq	$2, %rdx
	leaq	(%rbx,%rdx), %rcx
	addq	%r12, %rdx
	call	*(%r10,%rax,8)
	cmpq	%r14, %r13
	jne	.L178
.L792:
	movq	-2216(%rbp), %rbx
	movq	-2176(%rbp), %r13
	testq	%rbx, %rbx
	je	.L179
.L446:
	movl	64(%rbp), %edx
	movq	-2184(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	-2200(%rbp), %r8
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L212:
	movslq	(%r12,%r14,4), %rax
	cmpl	$5, %eax
	jg	.L181
	cmpl	$3, %eax
	jge	.L182
	cmpl	$1, %eax
	jg	.L183
	testl	%eax, %eax
	jns	.L184
	cmpl	$-1, %eax
	jne	.L180
	movl	-2296(%rbp), %eax
	testl	%eax, %eax
	je	.L794
	leaq	.LC6(%rip), %rdi
	call	__GI___libc_fatal
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$256, %eax
	je	.L182
	jle	.L795
	cmpl	$512, %eax
	je	.L182
	cmpl	$1024, %eax
	je	.L184
	cmpl	$263, %eax
	jne	.L180
	testl	%edx, %edx
	jne	.L796
	testb	$8, 64(%rbp)
	je	.L202
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L203
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L204:
	movq	%r14, %rax
	movdqa	(%rsi), %xmm0
	addq	$1, %r14
	salq	$4, %rax
	cmpq	%r14, %rbx
	movaps	%xmm0, (%rcx,%rax)
	ja	.L212
	.p2align 4,,10
	.p2align 3
.L179:
	movslq	16(%rbp), %rax
	cmpq	%r15, %rax
	movq	%rax, -2176(%rbp)
	jnb	.L453
	leaq	(%rax,%rax,8), %rax
	movq	32(%rbp), %rcx
	movq	%r15, -2280(%rbp)
	movq	-2224(%rbp), %r15
	leaq	0(%r13,%rax,8), %r14
	movl	-2208(%rbp), %eax
	addq	$1000, %rcx
	movq	%rcx, -2304(%rbp)
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movl	%eax, -2168(%rbp)
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rcx, %rax
	movq	%rax, -2216(%rbp)
	.p2align 4,,10
	.p2align 3
.L437:
	movzbl	12(%r14), %eax
	movzbl	13(%r14), %ebx
	movl	8(%r14), %r9d
	movslq	(%r14), %r12
	movl	%eax, %edx
	movl	%eax, %ecx
	movl	%ebx, %r13d
	shrb	$3, %dl
	andl	$1, %ecx
	movl	%eax, %r11d
	movl	%edx, %edi
	movl	%eax, %edx
	movb	%cl, -2260(%rbp)
	andl	$1, %edi
	shrb	$4, %dl
	movl	%eax, %r8d
	movl	%edi, -2236(%rbp)
	movl	%edx, %edi
	movl	%eax, %edx
	andl	$1, %edi
	shrb	$5, %dl
	movl	16(%r14), %ecx
	movl	%edi, -2232(%rbp)
	movl	%edx, %edi
	movl	%eax, %edx
	andl	$1, %edi
	shrb	$6, %dl
	shrb	%r11b
	movl	%edi, -2208(%rbp)
	movl	%edx, %edi
	movl	%eax, %edx
	andl	$1, %edi
	shrb	$7, %dl
	shrb	%r13b
	movl	%edi, -2224(%rbp)
	movzbl	%dl, %edi
	movslq	44(%r14), %rdx
	shrb	$2, %r8b
	shrb	$3, %bl
	andl	$1, %r11d
	andl	$1, %ebx
	andl	$1, %r13d
	andl	$1, %r8d
	movl	%edi, -2248(%rbp)
	movb	%bl, -2240(%rbp)
	cmpl	$-1, %edx
	movl	%ecx, -2256(%rbp)
	movb	%r9b, -2200(%rbp)
	je	.L797
	movq	-2184(%rbp), %rcx
	salq	$4, %rdx
	movl	(%rcx,%rdx), %ebx
	testl	%ebx, %ebx
	js	.L216
	movl	%ebx, 4(%r14)
.L215:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L218
	movq	-2184(%rbp), %rsi
	salq	$4, %rax
	movslq	(%rsi,%rax), %r12
	testl	%r12d, %r12d
	js	.L219
	movl	%r12d, (%r14)
.L218:
	movq	__printf_function_table(%rip), %rax
	testq	%rax, %rax
	je	.L221
	movsbq	-2200(%rbp), %rsi
	movq	(%rax,%rsi,8), %rax
	movq	%rsi, -2272(%rbp)
	testq	%rax, %rax
	je	.L221
	movq	56(%r14), %rdi
	leaq	30(,%rdi,8), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	testq	%rdi, %rdi
	je	.L222
	movb	%r8b, -2261(%rbp)
	movl	48(%r14), %r10d
	xorl	%ecx, %ecx
	movq	-2184(%rbp), %r8
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L223:
	leal	(%r10,%rcx), %eax
	salq	$4, %rax
	addq	%r8, %rax
	movq	%rax, (%rdx,%rsi,8)
	leal	1(%rcx), %esi
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	ja	.L223
	movq	__printf_function_table(%rip), %rax
	movq	-2272(%rbp), %rcx
	movzbl	-2261(%rbp), %r8d
	movq	(%rax,%rcx,8), %rax
.L222:
	movl	%r9d, -2292(%rbp)
	movb	%r8b, -2261(%rbp)
	movq	%r14, %rsi
	movb	%r11b, -2272(%rbp)
	movq	%r15, %rdi
	call	*%rax
	cmpl	$-2, %eax
	movzbl	-2272(%rbp), %r11d
	movzbl	-2261(%rbp), %r8d
	movl	-2292(%rbp), %r9d
	je	.L221
	testl	%eax, %eax
	js	.L770
	movl	-2168(%rbp), %edi
	testl	%edi, %edi
	js	.L213
	cltq
	addl	%edi, %eax
	movl	%eax, -2168(%rbp)
	jo	.L367
.L352:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
.L290:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	subq	%rsi, %rbx
.L445:
	movq	216(%r15), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%r12, %rax
	subq	%rdi, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L798
.L433:
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	*56(%r12)
	cmpq	%rbx, %rax
	jne	.L770
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%rbx, %rax
	js	.L435
	cmpq	%rbx, %rax
	jb	.L435
.L434:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	addq	$1, -2176(%rbp)
	addq	$72, %r14
	movq	-2176(%rbp), %rax
	cmpq	-2280(%rbp), %rax
	jb	.L437
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L182:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L205
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L206:
	movq	(%rsi), %rsi
	movq	%r14, %rax
	salq	$4, %rax
	movq	%rsi, (%rcx,%rax)
.L192:
	addq	$1, %r14
	cmpq	%r14, %rbx
	ja	.L212
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r14, %rsi
	salq	$4, %rsi
	testb	$8, %ah
	leaq	(%rcx,%rsi), %rdi
	je	.L207
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L208
	movl	%eax, %edi
	addq	16(%r8), %rdi
	addl	$8, %eax
	movl	%eax, (%r8)
.L209:
	movq	(%rdi), %rax
	movq	%rax, (%rcx,%rsi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L184:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L193
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L194:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	leal	-32(%r9), %eax
	cmpb	$90, %al
	jbe	.L799
.L228:
	movq	56(%r14), %rsi
	leaq	30(,%rsi,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	testq	%rsi, %rsi
	je	.L392
	movl	48(%r14), %r8d
	movq	-2184(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L393:
	leal	(%r8,%rdx), %eax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, (%rdi,%rcx,8)
	leal	1(%rdx), %ecx
	cmpq	%rcx, %rsi
	movq	%rcx, %rdx
	ja	.L393
.L392:
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L800
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$37, (%rax)
.L395:
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L459
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L801
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$35, (%rax)
.L757:
	movzbl	12(%r14), %eax
	movl	$2, %ebx
.L396:
	testb	%al, %al
	js	.L802
.L399:
	testb	$64, %al
	je	.L402
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L803
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$43, (%rax)
.L407:
	movzbl	12(%r14), %eax
	addl	$1, %ebx
.L405:
	testb	$32, %al
	je	.L408
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L804
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$45, (%rax)
.L410:
	addl	$1, %ebx
.L408:
	cmpl	$48, 16(%r14)
	je	.L805
.L411:
	testb	$8, 13(%r14)
	je	.L414
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L806
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$73, (%rax)
.L416:
	addl	$1, %ebx
.L414:
	movl	4(%r14), %eax
	testl	%eax, %eax
	jne	.L807
.L417:
	cmpl	$-1, (%r14)
	je	.L422
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L808
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$46, (%rax)
.L424:
	cmpl	$2147483647, %ebx
	je	.L770
	movslq	(%r14), %rdi
	leaq	-2132(%rbp), %r12
	xorl	%ecx, %ecx
	movl	$10, %edx
	addl	$1, %ebx
	movq	%r12, %rsi
	call	_itoa_word
	cmpq	%r12, %rax
	movq	%rax, %r13
	jb	.L427
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r15)
	movb	%dl, (%rax)
.L426:
	cmpl	$2147483647, %ebx
	je	.L770
	addl	$1, %ebx
	cmpq	%r12, %r13
	je	.L422
.L427:
	addq	$1, %r13
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	movzbl	-1(%r13), %edx
	jb	.L425
	movzbl	%dl, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L426
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L799:
	movsbl	%r9b, %eax
	leaq	jump_table(%rip), %rdx
	leaq	step4_jumps.12976(%rip), %rdi
	subl	$32, %eax
	movzbl	%r11b, %ecx
	movzbl	%r13b, %r13d
	cltq
	movzbl	%r8b, %r10d
	movzbl	(%rdx,%rax), %eax
	leaq	.L228(%rip), %rdx
	movslq	(%rdi,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L345:
	movq	$0, -2152(%rbp)
	movslq	48(%r14), %rax
	leaq	-2144(%rbp), %r12
	movq	-2184(%rbp), %rcx
	leaq	-2152(%rbp), %rdx
	movq	%r12, %rdi
	salq	$4, %rax
	movl	(%rcx,%rax), %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L770
	subl	%eax, %ebx
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	je	.L809
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L362
.L361:
	movq	216(%r15), %rcx
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%rcx, %rax
	subq	%rdi, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L810
.L363:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*56(%rcx)
	cmpq	%rax, %r13
	jne	.L770
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%r13, %rax
	js	.L365
	cmpq	%r13, %rax
	jb	.L365
.L364:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	jle	.L290
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	je	.L290
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L770
	addl	-2168(%rbp), %r12d
	movl	%r12d, %eax
	movl	%r12d, -2168(%rbp)
	jno	.L767
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L797:
	movl	4(%r14), %ebx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	negl	%ebx
	orl	$32, %eax
	movl	$1, -2208(%rbp)
	movl	%ebx, 4(%r14)
	movb	%al, 12(%r14)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$-1, (%r14)
	movq	$-1, %r12
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L795:
	cmpl	$7, %eax
	jg	.L180
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L197
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L198:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L785:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	8(%rax), %rax
	movq	72(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rax, 48(%rbp)
	movzbl	(%rax), %eax
	movq	%rcx, 56(%rbp)
	testb	%al, %al
	je	.L451
	cmpb	$127, %al
	jne	.L166
.L451:
	movq	$0, 48(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L798:
	movq	%rsi, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %rsi
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L207:
	movq	__printf_va_arg_table(%rip), %r9
	testq	%r9, %r9
	jne	.L811
.L210:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L794:
	leaq	__PRETTY_FUNCTION__.12972(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$1888, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%r8), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 8(%r8)
	fldt	(%rax)
	movq	%r14, %rax
	salq	$4, %rax
	fstpt	(%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L809:
	testl	%ebx, %ebx
	jle	.L357
	movslq	%ebx, %rcx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rcx, %rdx
	movq	%rcx, -2200(%rbp)
	call	__GI__IO_padn
	movq	-2200(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%ecx, %eax
	movl	%eax, -2168(%rbp)
	jo	.L367
.L357:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	jns	.L361
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L233:
	movslq	48(%r14), %rax
	salq	$4, %rax
	testl	%r10d, %r10d
	je	.L234
	movq	-2184(%rbp), %rdi
	movq	(%rdi,%rax), %r13
.L235:
	xorl	%r10d, %r10d
	testq	%r13, %r13
	movl	$10, %r9d
	jns	.L239
	negq	%r13
	movl	$10, %r9d
	movl	$1, %r10d
	.p2align 4,,10
	.p2align 3
.L239:
	testl	%r12d, %r12d
	js	.L247
.L818:
	jne	.L754
	testq	%r13, %r13
	jne	.L454
	cmpl	$8, %r9d
	jne	.L455
	movl	-2236(%rbp), %eax
	testl	%eax, %eax
	je	.L455
	movq	32(%rbp), %rax
	movq	-2304(%rbp), %rcx
	leaq	999(%rax), %r11
	movb	$48, 999(%rax)
	subq	%r11, %rcx
	movq	%rcx, %rdx
	negq	%rdx
	testq	%rdx, %rdx
	cmovs	%r13, %rdx
	testq	%rcx, %rcx
	movslq	%edx, %r12
	jns	.L456
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	jne	.L256
	subl	%ecx, %ebx
	movb	$32, -2272(%rbp)
	subl	%edx, %ebx
	jmp	.L262
.L229:
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L812
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$37, (%rax)
.L231:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L770
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
.L232:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	movl	-2168(%rbp), %edi
	subq	%rsi, %rbx
	testl	%edi, %edi
	jns	.L445
.L362:
	call	outstring_func.part.1
	.p2align 4,,10
	.p2align 3
.L371:
.L343:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rcx
	salq	$4, %rax
	movq	(%rcx,%rax), %r11
	testq	%r11, %r11
	je	.L813
.L372:
	cmpb	$83, %r9b
	je	.L335
	andl	$1, %r10d
	jne	.L335
	cmpl	$-1, %r12d
	je	.L375
.L450:
	movslq	%r12d, %rsi
	movq	%r11, %rdi
	movq	%r11, -2200(%rbp)
	call	__GI___strnlen
	movq	-2200(%rbp), %r11
	movq	%rax, %r12
	subl	%eax, %ebx
.L374:
	testl	%ebx, %ebx
	js	.L814
	movl	-2208(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L381
	testl	%ebx, %ebx
	je	.L382
	movslq	%ebx, %r13
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r11, -2200(%rbp)
	call	__GI__IO_padn
	cmpq	%rax, %r13
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r13d
	movq	-2200(%rbp), %r11
	movl	%r13d, -2168(%rbp)
	jo	.L367
.L382:
	movl	-2168(%rbp), %r10d
	testl	%r10d, %r10d
	jns	.L385
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L344:
	testl	%r10d, %r10d
	jne	.L345
	movl	-2208(%rbp), %esi
	subl	$1, %ebx
	testl	%esi, %esi
	je	.L815
.L346:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rcx
	salq	$4, %rax
	movl	(%rcx,%rax), %edx
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L816
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r15)
	movb	%dl, (%rax)
.L351:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L770
	movl	-2208(%rbp), %edx
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
	testl	%edx, %edx
	je	.L232
	testl	%ebx, %ebx
	jle	.L352
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %rbx
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %ebx
	movl	%ebx, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L342:
	movq	32(%rbp), %rsi
	movl	40(%rbp), %edi
	movl	$1000, %edx
	movl	%r9d, -2200(%rbp)
	call	__GI___strerror_r
	movq	%rax, %r11
	xorl	%r10d, %r10d
	movl	-2200(%rbp), %r9d
	testq	%r11, %r11
	jne	.L372
.L813:
	cmpl	$-1, %r12d
	je	.L373
	cmpl	$5, %r12d
	jg	.L373
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %r11
	jmp	.L374
.L316:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2144(%rbp)
	jne	.L817
.L317:
	testb	$8, 64(%rbp)
	je	.L318
	movzbl	-2260(%rbp), %edx
	movzbl	13(%r14), %eax
	sall	$4, %edx
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L319:
	leaq	-2144(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__GI___printf_fp
	testl	%eax, %eax
	js	.L770
	movl	-2168(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L213
	cltq
	addl	%ecx, %eax
	movl	%eax, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$16, %r9d
.L241:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testl	%r10d, %r10d
	je	.L244
	xorl	%r10d, %r10d
	testl	%r12d, %r12d
	movq	(%rax), %r13
	movl	$0, -2224(%rbp)
	movl	$0, -2232(%rbp)
	jns	.L818
.L247:
	movzbl	-2256(%rbp), %eax
	movl	$1, %r12d
	movb	%al, -2272(%rbp)
	jmp	.L249
.L242:
	movl	$8, %r9d
	jmp	.L241
.L332:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rdi
	salq	$4, %rax
	movq	(%rdi,%rax), %r13
	testq	%r13, %r13
	je	.L333
	movb	$120, -2200(%rbp)
	movl	$0, -2248(%rbp)
	movl	$16, %r9d
	movl	$1, -2236(%rbp)
	xorl	%r10d, %r10d
	jmp	.L239
.L324:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2144(%rbp)
	jne	.L819
.L325:
	testb	$8, 64(%rbp)
	je	.L326
	movzbl	-2260(%rbp), %eax
	movzbl	13(%r14), %edx
	sall	$4, %eax
	andl	$-17, %edx
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L327:
	leaq	-2144(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__printf_fphex
	testl	%eax, %eax
	js	.L770
	movl	-2168(%rbp), %esi
	testl	%esi, %esi
	js	.L213
	cltq
	addl	%esi, %eax
	movl	%eax, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L336:
	movl	-2296(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L337
	movl	-2308(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L820
.L338:
	movl	-2308(%rbp), %edi
	testl	%edi, %edi
	js	.L821
.L337:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rdi
	salq	$4, %rax
	testl	%r10d, %r10d
	movq	(%rdi,%rax), %rax
	je	.L339
	movslq	-2168(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L232
.L240:
	movl	$10, %r9d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L454:
	xorl	%r12d, %r12d
.L754:
	movb	$32, -2272(%rbp)
.L249:
	xorl	%ecx, %ecx
	cmpb	$88, -2200(%rbp)
	movq	-2304(%rbp), %rsi
	movl	%r9d, %edx
	movq	%r13, %rdi
	movl	%r10d, -2260(%rbp)
	movl	%r9d, -2256(%rbp)
	sete	%cl
	call	_itoa_word
	cmpq	$0, 48(%rbp)
	movq	%rax, %r11
	movl	-2256(%rbp), %r9d
	movl	-2260(%rbp), %r10d
	je	.L252
	movl	-2248(%rbp), %eax
	testl	%eax, %eax
	je	.L252
	movq	56(%rbp), %r8
	movq	48(%rbp), %rcx
	movq	%r11, %rsi
	movq	-2304(%rbp), %rdx
	movq	32(%rbp), %rdi
	movl	%r10d, -2248(%rbp)
	call	group_number
	movl	-2256(%rbp), %r9d
	movl	-2248(%rbp), %r10d
	movq	%rax, %r11
.L252:
	cmpl	$10, %r9d
	jne	.L253
	cmpb	$0, -2240(%rbp)
	je	.L253
	movq	-2304(%rbp), %rsi
	movq	%r11, %rdi
	movl	%r9d, -2240(%rbp)
	movl	%r10d, -2248(%rbp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movl	-2240(%rbp), %r9d
	movl	-2248(%rbp), %r10d
	movq	%rax, %r11
.L253:
	movq	-2304(%rbp), %rcx
	subq	%r11, %rcx
	cmpq	%r12, %rcx
	jl	.L822
	testq	%r13, %r13
	je	.L823
	cmpl	$8, %r9d
	jne	.L460
	movl	-2236(%rbp), %eax
	testl	%eax, %eax
	je	.L460
	movq	-2304(%rbp), %rcx
	leaq	-1(%r11), %rax
	movl	-2208(%rbp), %edi
	movq	%r12, %rdx
	movl	$0, %esi
	movb	$48, -1(%r11)
	movq	%rax, %r11
	subq	%rax, %rcx
	subq	%rcx, %rdx
	cmovs	%rsi, %rdx
	testl	%edi, %edi
	movslq	%edx, %r12
	je	.L824
	.p2align 4,,10
	.p2align 3
.L256:
	testl	%r10d, %r10d
	je	.L291
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L825
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r15)
	movb	$45, (%rax)
.L299:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L770
	addl	$1, %eax
	subl	$1, %ebx
	movl	%eax, -2168(%rbp)
.L294:
	testq	%r13, %r13
	je	.L300
	cmpl	$16, %r9d
	jne	.L300
	movl	-2236(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L300
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L826
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r15)
	movb	$48, (%rax)
.L302:
	cmpl	$2147483647, -2168(%rbp)
	je	.L770
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L827
	movzbl	-2200(%rbp), %edi
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r15)
	movb	%dil, (%rax)
.L304:
	movl	-2168(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L770
	addl	$2, %eax
	subl	$2, %ebx
	movl	%eax, -2168(%rbp)
.L300:
	addl	%ecx, %edx
	subl	%edx, %ebx
	testl	%r12d, %r12d
	jle	.L305
	movq	%r12, %rdx
	movl	$48, %esi
	movq	%r15, %rdi
	movq	%rcx, -2208(%rbp)
	movq	%r11, -2200(%rbp)
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movl	%r12d, -2168(%rbp)
	jo	.L367
	testl	%r12d, %r12d
	movq	-2200(%rbp), %r11
	movq	-2208(%rbp), %rcx
	js	.L213
.L309:
	movq	216(%r15), %r12
	leaq	__start___libc_IO_vtables(%rip), %rdi
	movq	%r12, %rax
	subq	%rdi, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L828
.L310:
	movq	%rcx, %rdx
	movq	%rcx, -2200(%rbp)
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	*56(%r12)
	movq	-2200(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	.L770
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%rcx, %rax
	js	.L312
	cmpq	%rcx, %rax
	jb	.L312
.L311:
	movslq	%eax, %rcx
	movl	%eax, %r13d
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	jle	.L290
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %rbx
	jne	.L770
	addl	%r13d, %ebx
	movl	%ebx, %eax
	movl	%ebx, -2168(%rbp)
	jo	.L367
	.p2align 4,,10
	.p2align 3
.L767:
	testl	%eax, %eax
	jns	.L290
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L244:
	testl	%r13d, %r13d
	movl	(%rax), %eax
	je	.L245
	movzbl	%al, %r13d
	movl	$0, -2224(%rbp)
	movl	$0, -2232(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L263:
	cmpb	$32, -2272(%rbp)
	je	.L449
.L269:
	movl	-2224(%rbp), %eax
	testl	%eax, %eax
	je	.L273
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L829
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$43, (%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L291:
	movl	-2224(%rbp), %eax
	testl	%eax, %eax
	je	.L295
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L830
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r15)
	movb	$43, (%rax)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	movl	-2168(%rbp), %r10d
	testl	%r10d, %r10d
	jns	.L309
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L245:
	testl	%ecx, %ecx
	jne	.L246
	movl	%eax, %r13d
	movl	$0, -2224(%rbp)
	movl	$0, -2232(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L823:
	movq	%r12, %rdx
	subq	%rcx, %rdx
	cmovs	%r13, %rdx
	movslq	%edx, %r12
.L250:
	movl	-2208(%rbp), %esi
	testl	%esi, %esi
	jne	.L256
	subl	%ecx, %ebx
	subl	%edx, %ebx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L381:
	movl	-2168(%rbp), %r9d
	testl	%r9d, %r9d
	js	.L362
.L385:
	movq	216(%r15), %r13
	leaq	__start___libc_IO_vtables(%rip), %rcx
	movq	%r13, %rax
	subq	%rcx, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L831
.L386:
	movq	%r12, %rdx
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	jne	.L770
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%r12, %rax
	js	.L388
	cmpq	%r12, %rax
	jb	.L388
.L387:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	je	.L290
	movl	-2208(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L290
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %rbx
	je	.L781
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L172:
	testq	%rbx, %rbx
	jne	.L446
.L453:
	movl	-2208(%rbp), %eax
	movl	%eax, -2168(%rbp)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L273:
	movl	-2232(%rbp), %eax
	testl	%eax, %eax
	je	.L272
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L832
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$32, (%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L295:
	movl	-2232(%rbp), %eax
	testl	%eax, %eax
	je	.L294
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L833
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%r15)
	movb	$32, (%rax)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L460:
	movl	-2208(%rbp), %r8d
	movq	%r12, %rdx
	movl	$0, %eax
	subq	%rcx, %rdx
	cmovs	%rax, %rdx
	testl	%r8d, %r8d
	movslq	%edx, %r12
	jne	.L256
	subl	%ecx, %ebx
	subl	%edx, %ebx
	jmp	.L447
.L422:
	movl	8(%r14), %eax
	testl	%eax, %eax
	je	.L428
	movq	40(%r15), %rdx
	cmpq	48(%r15), %rdx
	jnb	.L834
	leaq	1(%rdx), %rcx
	movq	%rcx, 40(%r15)
	movb	%al, (%rdx)
.L430:
	cmpl	$2147483647, %ebx
	je	.L770
	addl	$1, %ebx
.L428:
	movl	-2168(%rbp), %edx
	testl	%edx, %edx
	js	.L213
	movslq	%ebx, %rbx
.L781:
	addl	-2168(%rbp), %ebx
	movl	%ebx, %eax
	movl	%ebx, -2168(%rbp)
	jno	.L767
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L333:
	cmpl	$5, %r12d
	movl	$5, %eax
	leaq	.LC3(%rip), %r11
	cmovl	%eax, %r12d
	cmpb	$83, %r9b
	jne	.L450
.L335:
	movl	-2168(%rbp), %r9d
	movl	-2208(%rbp), %r8d
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	outstring_converted_wide_string
	movl	%eax, -2168(%rbp)
	jmp	.L767
.L786:
	movq	-2192(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	leaq	16(%rax), %r13
	jmp	.L168
.L326:
	andb	$-17, 13(%r14)
	jmp	.L327
.L339:
	testl	%r13d, %r13d
	jne	.L835
	testl	%ecx, %ecx
	jne	.L341
	movl	-2168(%rbp), %ecx
	movl	%ecx, (%rax)
	jmp	.L232
.L318:
	andb	$-17, 13(%r14)
	jmp	.L319
.L234:
	movq	-2184(%rbp), %rsi
	testl	%r13d, %r13d
	movl	(%rsi,%rax), %eax
	je	.L236
	movsbq	%al, %r13
	jmp	.L235
.L459:
	movl	$1, %ebx
	jmp	.L396
.L402:
	testb	$16, %al
	je	.L405
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L836
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$32, (%rax)
	jmp	.L407
.L449:
	testl	%ebx, %ebx
	jle	.L265
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	movq	%rcx, -2256(%rbp)
	movl	%r9d, -2240(%rbp)
	movq	%r11, -2248(%rbp)
	movl	%r10d, -2208(%rbp)
	call	__GI__IO_padn
	cmpq	%rax, %rbx
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %ebx
	movl	-2208(%rbp), %r10d
	movq	-2248(%rbp), %r11
	movl	%ebx, -2168(%rbp)
	movl	-2240(%rbp), %r9d
	movq	-2256(%rbp), %rcx
	jo	.L367
.L265:
	movl	-2168(%rbp), %edx
	testl	%edx, %edx
	js	.L213
	xorl	%ebx, %ebx
	jmp	.L264
.L455:
	movq	-2304(%rbp), %r11
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movb	$32, -2272(%rbp)
	jmp	.L250
.L814:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L362
	movq	216(%r15), %r13
	leaq	__start___libc_IO_vtables(%rip), %rsi
	movq	%r13, %rax
	subq	%rsi, %rax
	cmpq	%rax, -2216(%rbp)
	jbe	.L837
.L377:
	movq	%r12, %rdx
	movq	%r11, %rsi
	movq	%r15, %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	jne	.L770
	movslq	-2168(%rbp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%r12, %rax
	addq	%rax, %rdx
	cmpq	%r12, %rdx
	jmp	.L783
.L246:
	movzwl	%ax, %r13d
	movl	$0, -2224(%rbp)
	movl	$0, -2232(%rbp)
	jmp	.L239
.L375:
	movq	%r11, %rdi
	movq	%r11, -2200(%rbp)
	call	__GI_strlen
	movq	-2200(%rbp), %r11
	movq	%rax, %r12
	subl	%eax, %ebx
	jmp	.L374
.L802:
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L838
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$39, (%rax)
.L401:
	addl	$1, %ebx
	movzbl	12(%r14), %eax
	jmp	.L399
.L373:
	subl	$6, %ebx
	movl	$6, %r12d
	leaq	null(%rip), %r11
	jmp	.L374
.L805:
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	jnb	.L839
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r15)
	movb	$48, (%rax)
.L413:
	addl	$1, %ebx
	jmp	.L411
.L824:
	subl	%ecx, %ebx
	subl	%edx, %ebx
	jmp	.L262
.L236:
	movslq	%eax, %rdx
	movswq	%ax, %r13
	testl	%ecx, %ecx
	cmove	%rdx, %r13
	jmp	.L235
.L835:
	movzbl	-2168(%rbp), %esi
	movb	%sil, (%rax)
	jmp	.L232
.L456:
	movb	$32, -2272(%rbp)
	jmp	.L250
.L815:
	testl	%ebx, %ebx
	jle	.L347
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L770
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movl	%r12d, -2168(%rbp)
	jo	.L367
.L347:
	movl	-2168(%rbp), %ecx
	testl	%ecx, %ecx
	jns	.L346
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L820:
	movq	-2320(%rbp), %rbx
	movl	%ecx, -2208(%rbp)
	movl	%r10d, -2200(%rbp)
	movq	%rbx, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rsi
	movq	%rbx, %rdi
	call	__readonly_area
	movl	-2208(%rbp), %ecx
	movl	%eax, -2308(%rbp)
	movl	-2200(%rbp), %r10d
	jmp	.L338
.L838:
	movl	$39, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L401
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L839:
	movl	$48, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L413
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	-2132(%rbp), %r12
	xorl	%ecx, %ecx
	movslq	%eax, %rdi
	movl	$10, %edx
	movq	%r12, %rsi
	call	_itoa_word
	cmpq	%r12, %rax
	movq	%rax, %r13
	jb	.L421
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r15)
	movb	%dl, (%rax)
.L420:
	cmpl	$2147483647, %ebx
	je	.L770
	addl	$1, %ebx
	cmpq	%r12, %r13
	je	.L417
.L421:
	addq	$1, %r13
	movq	40(%r15), %rax
	cmpq	48(%r15), %rax
	movzbl	-1(%r13), %edx
	jb	.L418
	movzbl	%dl, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L420
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L796:
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L200
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L201:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	andl	$-257, (%r12,%r14,4)
	jmp	.L192
.L828:
	movq	%rcx, -2208(%rbp)
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r11
	movq	-2208(%rbp), %rcx
	jmp	.L310
.L791:
	movq	%rcx, -2208(%rbp)
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2208(%rbp), %rcx
	movq	-2200(%rbp), %r11
	jmp	.L286
.L810:
	movq	%rcx, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %rcx
	jmp	.L363
.L831:
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r11
	jmp	.L386
.L811:
	cmpq	$0, -64(%r9,%rax,8)
	je	.L210
	movq	-2168(%rbp), %rax
	movl	%edx, -2216(%rbp)
	movq	%rcx, -2200(%rbp)
	movq	%r8, -2176(%rbp)
	movslq	(%rax,%r14,4), %rax
	addq	$30, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movq	%rdi, (%rcx,%rsi)
	movslq	(%r12,%r14,4), %rax
	movq	%r8, %rsi
	call	*-64(%r9,%rax,8)
	movq	-2176(%rbp), %r8
	movq	-2200(%rbp), %rcx
	movl	-2216(%rbp), %edx
	jmp	.L192
.L341:
	movzwl	-2168(%rbp), %ecx
	movw	%cx, (%rax)
	jmp	.L232
.L825:
	movq	%rdx, -2248(%rbp)
	movq	%rcx, -2232(%rbp)
	movl	$45, %esi
	movl	%r9d, -2224(%rbp)
	movq	%r11, -2208(%rbp)
.L779:
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movl	-2224(%rbp), %r9d
	movq	-2232(%rbp), %rcx
	movq	-2248(%rbp), %rdx
	jne	.L299
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%rcx, -2232(%rbp)
	movl	%r9d, -2224(%rbp)
	movl	$45, %esi
	movq	%r11, -2208(%rbp)
.L775:
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movl	-2224(%rbp), %r9d
	movq	-2232(%rbp), %rcx
	jne	.L277
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L812:
	movl	$37, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L231
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L819:
	andb	$-2, 12(%r14)
	jmp	.L325
.L817:
	movl	$7, 52(%r14)
	andb	$-2, 12(%r14)
	jmp	.L317
.L800:
	movl	$37, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L395
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L790:
	movzbl	-2200(%rbp), %esi
	movq	%r15, %rdi
	movq	%rcx, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movq	-2224(%rbp), %rcx
	jne	.L282
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$48, %esi
	movq	%r15, %rdi
	movq	%rcx, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movq	-2224(%rbp), %rcx
	jne	.L280
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L827:
	movzbl	-2200(%rbp), %esi
	movq	%r15, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rcx, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movq	-2224(%rbp), %rcx
	movq	-2232(%rbp), %rdx
	jne	.L304
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L826:
	movl	$48, %esi
	movq	%r15, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rcx, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r11
	movq	-2224(%rbp), %rcx
	movq	-2232(%rbp), %rdx
	jne	.L302
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L830:
	movq	%rdx, -2248(%rbp)
	movq	%rcx, -2232(%rbp)
	movl	$43, %esi
	movl	%r9d, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	jmp	.L779
.L829:
	movq	%rcx, -2232(%rbp)
	movl	%r9d, -2224(%rbp)
	movl	$43, %esi
	movq	%r11, -2208(%rbp)
	jmp	.L775
.L808:
	movl	$46, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L424
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L806:
	movl	$73, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L416
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$35, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L757
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$45, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L410
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L803:
	movl	$43, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L407
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L816:
	movzbl	%dl, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L351
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L837:
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r11
	jmp	.L377
.L833:
	movq	%rdx, -2248(%rbp)
	movq	%rcx, -2232(%rbp)
	movl	$32, %esi
	movl	%r9d, -2224(%rbp)
	movq	%r11, -2208(%rbp)
	jmp	.L779
.L832:
	movq	%rcx, -2232(%rbp)
	movl	%r9d, -2224(%rbp)
	movl	$32, %esi
	movq	%r11, -2208(%rbp)
	jmp	.L775
.L834:
	movzbl	%al, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L430
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L836:
	movl	$32, %esi
	movq	%r15, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L407
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$1, %edx
	jmp	.L434
.L821:
	leaq	.LC7(%rip), %rdi
	call	__GI___libc_fatal
.L312:
	movl	$1, %edx
	jmp	.L311
.L200:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L201
.L205:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L206
.L197:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L198
.L183:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L190
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L191:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L192
.L193:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L194
.L190:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L191
.L365:
	movl	$1, %edx
	jmp	.L364
.L388:
	movl	$1, %edx
	jmp	.L387
.L203:
	movq	8(%r8), %rax
	leaq	15(%rax), %rsi
	andq	$-16, %rsi
	leaq	16(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L204
.L208:
	movq	8(%r8), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r8)
	jmp	.L209
	.size	printf_positional, .-printf_positional
	.p2align 4,,15
	.globl	__vfprintf_internal
	.hidden	__vfprintf_internal
	.type	__vfprintf_internal, @function
__vfprintf_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1288, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 104(%rsp)
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L842
	movl	$-1, 192(%rdi)
.L843:
	movl	(%rdi), %ebp
	testb	$8, %bpl
	jne	.L1426
	testq	%rsi, %rsi
	je	.L1427
	movl	%ebp, %eax
	movl	%ecx, 76(%rsp)
	movq	%rdx, %r15
	andl	$2, %eax
	movq	%rsi, 8(%rsp)
	movq	%rdi, %rbx
	movl	%eax, 64(%rsp)
	jne	.L1428
	movdqu	(%rdx), %xmm0
	movl	$37, %esi
	movq	8(%rsp), %rdi
	movups	%xmm0, 216(%rsp)
	movq	16(%rdx), %rax
	movq	%rax, 232(%rsp)
	call	__strchrnul@PLT
	andl	$32768, %ebp
	movq	%rax, 80(%rsp)
	movq	%rax, 160(%rsp)
	jne	.L848
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	movl	%eax, 64(%rsp)
	je	.L849
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	240(%rsp), %rdi
	movq	%rbx, %rdx
#APP
# 1400 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	call	*%rax
	testl	$32768, (%rbx)
	jne	.L848
.L850:
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L851
#APP
# 1401 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L852
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L853:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L851:
	addl	$1, 4(%rdi)
.L848:
	movq	216(%rbx), %rbp
	leaq	__stop___libc_IO_vtables(%rip), %rdx
	leaq	__start___libc_IO_vtables(%rip), %rax
	movq	80(%rsp), %r12
	subq	8(%rsp), %r12
	movq	%rdx, %rsi
	movq	%rax, %rdi
	movq	%rax, 40(%rsp)
	subq	%rax, %rsi
	movq	%rbp, %rax
	subq	%rdi, %rax
	movq	%rsi, 32(%rsp)
	cmpq	%rax, %rsi
	jbe	.L1429
.L854:
	movq	%r12, %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%rbp)
	cmpq	%rax, %r12
	jne	.L1138
	movslq	%r12d, %rdx
	movq	%r12, %rax
	movl	%r12d, %ebp
	shrq	$63, %rax
	cmpq	%rdx, %r12
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1081
	testl	%r12d, %r12d
	js	.L855
	movq	160(%rsp), %rax
	cmpb	$0, (%rax)
	je	.L855
	cmpq	$0, __printf_function_table(%rip)
	jne	.L1140
	cmpq	$0, __printf_modifier_table(%rip)
	jne	.L1140
	cmpq	$0, __printf_va_arg_table(%rip)
	movl	$0, 148(%rsp)
	jne	.L1141
	movl	$0, 108(%rsp)
	movq	$-1, 112(%rsp)
	movq	$0, 136(%rsp)
	.p2align 4,,10
	.p2align 3
.L1108:
	leaq	1(%rax), %rdx
	movq	%rdx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %edx
	cmpb	$90, %dl
	ja	.L861
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %edx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rdx,4), %rax
	addq	%rsi, %rax
.L861:
	movl	$0, 88(%rsp)
	movl	$0, 24(%rsp)
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	movl	$0, 68(%rsp)
	movl	$0, 48(%rsp)
	movl	$0, 72(%rsp)
	movl	$0, 96(%rsp)
	leaq	step4_jumps.12771(%rip), %r11
	movb	$32, 56(%rsp)
	movl	$-1, 16(%rsp)
	leaq	step3b_jumps.12770(%rip), %rdx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1017:
	testb	$1, 76(%rsp)
	jne	.L1166
	movl	%r10d, %eax
	andl	$1, %eax
.L1018:
	movzbl	96(%rsp), %edx
	movzbl	72(%rsp), %ecx
	addl	%r13d, %r13d
	sall	$2, %r9d
	orl	%r13d, %eax
	movl	16(%rsp), %edi
	orl	%r9d, %eax
	movl	24(%rsp), %esi
	movsbl	%r12b, %r12d
	movl	$0, 204(%rsp)
	movl	%r12d, 200(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	movl	%edi, 192(%rsp)
	orl	%edx, %eax
	movzbl	48(%rsp), %edx
	movl	%esi, 196(%rsp)
	orl	%ecx, %eax
	movzbl	68(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	orl	%ecx, %eax
	sall	$7, %edx
	movl	%eax, %r8d
	movzbl	88(%rsp), %eax
	orl	%edx, %r8d
	movzbl	56(%rsp), %edx
	movb	%r8b, 204(%rsp)
	sall	$3, %eax
	testl	%r10d, %r10d
	movb	%al, 205(%rsp)
	movl	%edx, 208(%rsp)
	je	.L1019
	testb	$8, 76(%rsp)
	je	.L1020
	orl	$16, %eax
	movb	%al, 205(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1021
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1022:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 176(%rsp)
.L1023:
	leaq	176(%rsp), %rax
	leaq	168(%rsp), %rdx
	leaq	192(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 168(%rsp)
	call	__GI___printf_fp
	testl	%eax, %eax
	js	.L1138
.L1364:
	xorl	%edx, %edx
	addl	%eax, %ebp
	js	.L1123
	cmpl	%eax, %ebp
	jb	.L1123
.L1122:
	testl	%edx, %edx
	je	.L1376
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L855:
	testl	$32768, (%rbx)
	je	.L1430
.L1110:
	movl	64(%rsp), %ecx
	testl	%ecx, %ecx
	je	.L840
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	240(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 1690 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L840:
	addq	$1288, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	cmpl	$-1, %eax
	je	.L843
	movl	$-1, %ebp
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1110
	movq	$0, 8(%rdi)
#APP
# 1689 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1112
	subl	$1, (%rdi)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1428:
	call	buffered_vfprintf
	movl	%eax, %ebp
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L1058:
	testl	%r9d, %r9d
	je	.L1431
.L1059:
	movl	(%r15), %eax
	leaq	176(%rsp), %rdx
	movq	$0, 176(%rsp)
	cmpl	$47, %eax
	ja	.L1069
	movl	%eax, %ecx
	addq	16(%r15), %rcx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1070:
	leaq	192(%rsp), %rax
	movl	(%rcx), %esi
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L1138
	movl	24(%rsp), %r12d
	movl	48(%rsp), %r9d
	subl	%eax, %r12d
	testl	%r9d, %r9d
	je	.L1432
.L1072:
	movq	216(%rbx), %r14
	movq	%r14, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1127
.L1077:
	movq	%r13, %rdx
	movq	16(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%rax, %r13
	jne	.L1138
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r13, %rax
	js	.L1079
	cmpq	%r13, %rax
	jb	.L1079
.L1078:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1081
	testl	%eax, %eax
	js	.L855
	testl	%r12d, %r12d
	jle	.L927
	movl	48(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1384:
	movslq	%r12d, %r12
.L1396:
	movq	%r12, %rdx
	movl	$32, %esi
	movq	%rbx, %rdi
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L1138
	addl	%r12d, %ebp
	jo	.L1081
.L1376:
	testl	%ebp, %ebp
	jns	.L927
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1085:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1086
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1087:
	movq	(%rdx), %r14
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1027:
	testb	$1, 76(%rsp)
	jne	.L1167
	movl	%r10d, %eax
	andl	$1, %eax
.L1028:
	movzbl	96(%rsp), %edx
	movzbl	72(%rsp), %ecx
	addl	%r13d, %r13d
	sall	$2, %r9d
	orl	%r13d, %eax
	movl	16(%rsp), %esi
	orl	%r9d, %eax
	movl	24(%rsp), %edi
	movsbl	%r12b, %r12d
	movl	$0, 204(%rsp)
	movl	%r12d, 200(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	movl	%esi, 192(%rsp)
	orl	%edx, %eax
	movzbl	48(%rsp), %edx
	movl	%edi, 196(%rsp)
	orl	%ecx, %eax
	movzbl	68(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	orl	%ecx, %eax
	sall	$7, %edx
	movl	%eax, %r8d
	movzbl	56(%rsp), %eax
	orl	%edx, %r8d
	testl	%r10d, %r10d
	movb	%r8b, 204(%rsp)
	movl	%eax, 208(%rsp)
	je	.L1029
	testb	$8, 76(%rsp)
	je	.L1030
	orb	$16, 205(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1031
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1032:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 176(%rsp)
.L1033:
	leaq	176(%rsp), %rax
	leaq	168(%rsp), %rdx
	leaq	192(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 168(%rsp)
	call	__printf_fphex
	testl	%eax, %eax
	jns	.L1364
	.p2align 4,,10
	.p2align 3
.L1138:
	testl	$32768, (%rbx)
	movl	$-1, %ebp
	jne	.L1110
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L881:
	movq	160(%rsp), %rcx
	leaq	1(%rcx), %rax
	movq	%rax, 160(%rsp)
	movq	%rax, 192(%rsp)
	movzbl	1(%rcx), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L1433
.L882:
	movl	(%r15), %ecx
	cmpl	$47, %ecx
	ja	.L888
	movl	%ecx, %esi
	addq	16(%r15), %rsi
	addl	$8, %ecx
	movl	%ecx, (%r15)
.L889:
	movl	(%rsi), %esi
	testl	%esi, %esi
	movl	%esi, 24(%rsp)
	js	.L1434
.L890:
	movzbl	(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L841
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %ecx
	leaq	step1_jumps.12766(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L879:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L880
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	movl	$1, 88(%rsp)
	cltq
	movzbl	(%rsi,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L876:
	cmpq	$-1, 112(%rsp)
	je	.L1435
.L877:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L878
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	movl	$1, %r8d
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	leaq	.L841(%rip), %rcx
	addq	%rcx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L873:
	movl	48(%rsp), %eax
	movzbl	56(%rsp), %edi
	testl	%eax, %eax
	movl	$48, %eax
	cmove	%eax, %edi
	movq	160(%rsp), %rax
	movb	%dil, 56(%rsp)
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L841
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L894:
	leaq	160(%rsp), %rdi
	movq	%rdx, 128(%rsp)
	movl	%r8d, 120(%rsp)
	movl	%r9d, 100(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, 24(%rsp)
	movl	100(%rsp), %r9d
	movl	120(%rsp), %r8d
	movq	128(%rsp), %rdx
	je	.L1081
	movq	160(%rsp), %rax
	movzbl	(%rax), %r12d
	cmpb	$36, %r12b
	je	.L893
	leal	-32(%r12), %eax
	cmpb	$90, %al
	jbe	.L1436
	.p2align 4,,10
	.p2align 3
.L841:
	testb	%r12b, %r12b
	je	.L1437
.L893:
	movl	108(%rsp), %edx
.L860:
	subq	$8, %rsp
	movl	%ebp, %r9d
	movq	%r15, %rcx
	movl	84(%rsp), %eax
	movq	%rbx, %rdi
	pushq	%rax
	pushq	152(%rsp)
	pushq	136(%rsp)
	movl	136(%rsp), %eax
	pushq	%rax
	leaq	312(%rsp), %rax
	pushq	%rax
	pushq	128(%rsp)
	pushq	%rdx
	movl	212(%rsp), %edx
	movq	72(%rsp), %rsi
	leaq	280(%rsp), %r8
	call	printf_positional
	addq	$64, %rsp
	movl	%eax, %ebp
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L871:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L872
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	movl	$1, 96(%rsp)
	cltq
	movzbl	(%rsi,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L869:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L870
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	movb	$32, 56(%rsp)
	subl	$32, %eax
	movl	$1, 48(%rsp)
	cltq
	movzbl	(%rdi,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	leaq	.L841(%rip), %rcx
	addq	%rcx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L867:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L868
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	movl	$1, 68(%rsp)
	cltq
	movzbl	(%rcx,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L863:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L1438
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	movl	$1, 72(%rsp)
	cltq
	movzbl	(%rsi,%rax), %ecx
	leaq	step0_jumps.12735(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1056:
	movl	104(%rsp), %edi
	leaq	272(%rsp), %rsi
	movl	$1000, %edx
	call	__GI___strerror_r
	xorl	%r9d, %r9d
	movq	%rax, %r14
.L1057:
	testq	%r14, %r14
	je	.L1439
	cmpb	$83, %r12b
	je	.L1042
	testl	%r9d, %r9d
	jne	.L1042
	cmpl	$-1, 16(%rsp)
	je	.L1091
.L1041:
	movslq	16(%rsp), %rsi
	movq	%r14, %rdi
	call	__GI___strnlen
	movq	%rax, %r12
.L1090:
	movl	24(%rsp), %r8d
	subl	%eax, %r8d
	js	.L1440
	movl	48(%rsp), %edi
	testl	%edi, %edi
	jne	.L1096
	testl	%r8d, %r8d
	je	.L1096
	movslq	%r8d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movl	%r8d, 16(%rsp)
	call	__GI__IO_padn
	cmpq	%rax, %r13
	jne	.L1138
	movl	16(%rsp), %r8d
	addl	%r8d, %ebp
	js	.L1081
	cmpl	%r8d, %ebp
	jb	.L1081
	testl	%ebp, %ebp
	js	.L855
.L1096:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1131
.L1099:
	movl	%r8d, 16(%rsp)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	movl	16(%rsp), %r8d
	jne	.L1138
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r12, %rax
	js	.L1101
	cmpq	%r12, %rax
	jb	.L1101
.L1100:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1081
	testl	%eax, %eax
	js	.L855
	testl	%r8d, %r8d
	je	.L927
	movl	48(%rsp), %esi
	testl	%esi, %esi
	je	.L927
	movslq	%r8d, %r12
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1043:
	testb	$2, 76(%rsp)
	je	.L1044
	movl	148(%rsp), %r12d
	testl	%r12d, %r12d
	jne	.L1044
	movq	8(%rsp), %r12
	movl	%r9d, 16(%rsp)
	movq	%r12, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rsi
	movq	%r12, %rdi
	call	__readonly_area
	testl	%eax, %eax
	movl	%eax, 148(%rsp)
	movl	16(%rsp), %r9d
	jns	.L1044
	leaq	.LC7(%rip), %rdi
	call	__GI___libc_fatal
	.p2align 4,,10
	.p2align 3
.L1044:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	jne	.L1441
	testl	%r14d, %r14d
	je	.L1048
	cmpl	$47, %eax
	ja	.L1049
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1050:
	movq	(%rdx), %rax
	movb	%bpl, (%rax)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1038
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1039:
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L1040
	movq	%r10, %r14
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	$120, %r12d
	movl	$1, 96(%rsp)
	movl	$16, 100(%rsp)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L919:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L922
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	leaq	.L841(%rip), %rcx
	subl	$32, %eax
	xorl	%r10d, %r10d
	movl	$1, %r9d
	cltq
	movzbl	(%rdi,%rax), %eax
	movslq	(%r11,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L921:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	jbe	.L1442
.L922:
	xorl	%r10d, %r10d
	movl	$1, %r9d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L943:
	movl	$10, 100(%rsp)
.L944:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	je	.L947
	cmpl	$47, %eax
	ja	.L948
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L949:
	movq	(%rdx), %r14
	movl	$0, 68(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 72(%rsp)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L928:
	testl	%r9d, %r9d
	jne	.L1443
	testl	%r14d, %r14d
	movl	(%r15), %eax
	je	.L933
	cmpl	$47, %eax
	ja	.L934
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L935:
	movsbq	(%rdx), %r10
.L932:
	testq	%r10, %r10
	js	.L1444
	movq	%r10, %r14
	xorl	%r9d, %r9d
	movl	$10, 100(%rsp)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L923:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1445
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$37, (%rax)
.L926:
	cmpl	$2147483647, %ebp
	je	.L1138
	addl	$1, %ebp
	.p2align 4,,10
	.p2align 3
.L927:
	movq	160(%rsp), %rax
	movl	$37, %esi
	addl	$1, 108(%rsp)
	leaq	1(%rax), %r13
	movq	%r13, %rdi
	movq	%r13, 160(%rsp)
	call	__strchrnul@PLT
	movq	216(%rbx), %r14
	movq	%rax, 160(%rsp)
	subq	%r13, %rax
	movq	%rax, %r12
	movq	%r14, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1446
.L1134:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%r12, %rax
	jne	.L1138
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r12, %rax
	js	.L1106
	cmpq	%r12, %rax
	jb	.L1106
.L1105:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1081
	testl	%eax, %eax
	js	.L855
	movq	160(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L1108
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L911:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L912
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	xorl	%r13d, %r13d
	movl	$1, %r14d
	cltq
	movzbl	(%rcx,%rax), %eax
	movslq	(%r11,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L909:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L910
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rsi,%rax), %ecx
	leaq	step3a_jumps.12768(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
.L910:
	movl	$1, %r13d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L897:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	cmpb	$42, %r12b
	je	.L1447
	movzbl	%r12b, %eax
	movl	$0, 16(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1448
.L906:
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L841
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %ecx
	leaq	step2_jumps.12767(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	leaq	.L841(%rip), %rcx
	addq	%rcx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L915:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L916
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	movl	$1, %r9d
	movl	$1, %r10d
	cltq
	movzbl	(%rsi,%rax), %eax
	movslq	(%r11,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L913:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L1369
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	leaq	.L841(%rip), %rcx
	subl	$32, %eax
	movl	$1, %r9d
	cltq
	movzbl	(%rdi,%rax), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L917:
	movq	160(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L841(%rip), %rax
	leal	-32(%r12), %ecx
	cmpb	$90, %cl
	ja	.L922
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	leaq	.L841(%rip), %rsi
	subl	$32, %eax
	xorl	%r10d, %r10d
	movl	$1, %r9d
	cltq
	movzbl	(%rcx,%rax), %eax
	movslq	(%r11,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L946:
	movl	$16, 100(%rsp)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L945:
	movl	$8, 100(%rsp)
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L947:
	testl	%r14d, %r14d
	jne	.L1449
	testl	%r13d, %r13d
	jne	.L953
	cmpl	$47, %eax
	ja	.L954
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L955:
	movl	(%rdx), %r14d
	movl	$0, 68(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 72(%rsp)
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	272(%rsp), %r11
	leaq	1000(%r11), %rax
	movq	%rax, 120(%rsp)
	movl	16(%rsp), %eax
	testl	%eax, %eax
	js	.L1162
	je	.L959
	movslq	%eax, %r13
	movb	$32, 56(%rsp)
.L958:
	leaq	1000(%r11), %rax
	movl	100(%rsp), %edx
	xorl	%ecx, %ecx
	cmpb	$88, %r12b
	movq	%r14, %rdi
	movl	%r9d, 128(%rsp)
	sete	%cl
	movq	%rax, %rsi
	movq	%r11, 16(%rsp)
	movl	%r8d, 144(%rsp)
	movq	%rax, 152(%rsp)
	call	_itoa_word
	movq	112(%rsp), %rcx
	movq	%rax, %r10
	movq	16(%rsp), %r11
	movl	128(%rsp), %r9d
	testq	%rcx, %rcx
	je	.L961
	movl	144(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L961
	movq	136(%rsp), %r8
	movq	152(%rsp), %rdx
	movq	%r11, %rdi
	movq	%rax, %rsi
	call	group_number
	movl	128(%rsp), %r9d
	movq	16(%rsp), %r11
	movq	%rax, %r10
.L961:
	cmpl	$10, 100(%rsp)
	jne	.L962
	movl	88(%rsp), %eax
	testl	%eax, %eax
	je	.L962
	leaq	1000(%r11), %rsi
	movq	%r10, %rdi
	movl	%r9d, 16(%rsp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movl	16(%rsp), %r9d
	movq	%rax, %r10
.L962:
	movq	120(%rsp), %rdi
	movq	%rdi, %rcx
	subq	%r10, %rcx
	cmpq	%r13, %rcx
	jl	.L963
	testq	%r14, %r14
	je	.L964
	cmpl	$8, 100(%rsp)
	jne	.L965
	movl	96(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L965
	leaq	-1(%r10), %rax
	movl	$0, %edx
	movb	$48, -1(%r10)
	subq	%rax, %rdi
	movq	%rax, %r10
	subq	%rdi, %r13
	movq	%rdi, %rcx
	movl	48(%rsp), %edi
	cmovs	%rdx, %r13
	movl	%r13d, 16(%rsp)
	testl	%edi, %edi
	jne	.L967
	movl	24(%rsp), %edx
	subl	%ecx, %edx
	subl	%r13d, %edx
	movl	%edx, %r13d
	.p2align 4,,10
	.p2align 3
.L968:
	movl	72(%rsp), %eax
	orl	%r9d, %eax
	orl	68(%rsp), %eax
	cmpl	$1, %eax
	adcl	$-1, %r13d
	cmpb	$32, 56(%rsp)
	je	.L1450
.L970:
	testl	%r9d, %r9d
	je	.L973
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1451
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$45, (%rax)
.L981:
	cmpl	$2147483647, %ebp
	je	.L1138
	addl	$1, %ebp
.L976:
	testq	%r14, %r14
	je	.L982
	cmpl	$16, 100(%rsp)
	jne	.L982
	movl	96(%rsp), %eax
	testl	%eax, %eax
	je	.L982
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1452
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$48, (%rax)
.L984:
	cmpl	$2147483647, %ebp
	je	.L1138
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1453
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rax)
.L986:
	cmpl	$2147483646, %ebp
	je	.L1138
	addl	$2, %ebp
.L982:
	addl	16(%rsp), %r13d
	testl	%r13d, %r13d
	jle	.L987
	movslq	%r13d, %r12
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r10, 24(%rsp)
	movq	%rcx, 16(%rsp)
	call	__GI__IO_padn
	cmpq	%rax, %r12
	jne	.L1138
	addl	%r13d, %ebp
	js	.L1081
	cmpl	%r13d, %ebp
	jb	.L1081
	testl	%ebp, %ebp
	movq	16(%rsp), %rcx
	movq	24(%rsp), %r10
	js	.L855
.L987:
	movq	216(%rbx), %r12
	movq	%r12, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1454
.L990:
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movq	%r10, %rsi
	movq	%rbx, %rdi
	call	*56(%r12)
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1138
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%rcx, %rax
	js	.L1094
	cmpq	%rcx, %rax
	jb	.L1094
.L1093:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1081
	testl	%eax, %eax
	jns	.L927
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1446:
	call	_IO_vtable_check
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1429:
	call	_IO_vtable_check
	jmp	.L854
.L1449:
	cmpl	$47, %eax
	ja	.L951
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L952:
	movzbl	(%rdx), %r14d
	movl	$0, 68(%rsp)
	movl	$0, 72(%rsp)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1432:
	testl	%r12d, %r12d
	jle	.L1072
	movslq	%r12d, %r14
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r14
	jne	.L1138
	addl	%r12d, %ebp
	js	.L1081
	cmpl	%r12d, %ebp
	jb	.L1081
	testl	%ebp, %ebp
	jns	.L1072
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1438:
	leaq	.L841(%rip), %rax
	movl	$1, 72(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L868:
	movl	$1, 68(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L870:
	movb	$32, 56(%rsp)
	movl	$1, 48(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1369:
	movl	$1, %r9d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L916:
	movl	$1, %r9d
	movl	$1, %r10d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L912:
	xorl	%r13d, %r13d
	movl	$1, %r14d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L880:
	movl	$1, 88(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L878:
	movl	$1, %r8d
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L872:
	movl	$1, 96(%rsp)
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1442:
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	xorl	%r10d, %r10d
	movl	$1, %r9d
	cltq
	movzbl	(%rsi,%rax), %eax
	movslq	(%r11,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1431:
	movl	24(%rsp), %r12d
	movl	48(%rsp), %r11d
	subl	$1, %r12d
	testl	%r11d, %r11d
	je	.L1455
.L1060:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1063
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1064:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	movl	(%rdx), %edx
	jnb	.L1456
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	%dl, (%rax)
.L1066:
	cmpl	$2147483647, %ebp
	je	.L1138
	addl	$1, %ebp
	testl	%r12d, %r12d
	jle	.L927
	movl	48(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L927
	movslq	%r12d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r13
	jne	.L1138
	addl	%r12d, %ebp
	js	.L1081
	cmpl	%r12d, %ebp
	jnb	.L1376
	jmp	.L1081
.L1441:
	cmpl	$47, %eax
	ja	.L1046
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1047:
	movq	(%rdx), %rax
	movslq	%ebp, %rdx
	movq	%rdx, (%rax)
	jmp	.L927
.L1019:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1024
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1025:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 176(%rsp)
	jmp	.L1023
.L1029:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1034
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1035:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 176(%rsp)
	jmp	.L1033
.L1443:
	movl	(%r15), %edx
	cmpl	$47, %edx
	ja	.L930
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$8, %edx
	movl	%edx, (%r15)
.L931:
	movq	(%rax), %r10
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1436:
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rsi
	leaq	.L841(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rsi,%rax), %ecx
	leaq	step1_jumps.12766(%rip), %rax
	movslq	(%rax,%rcx,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%r14, %rdi
	call	__GI_strlen
	movq	%rax, %r12
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1434:
	negl	24(%rsp)
	movb	$32, 56(%rsp)
	movl	$1, 48(%rsp)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L1447:
	leaq	2(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movq	%rcx, 192(%rsp)
	movzbl	2(%rax), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L899
.L903:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1457
	movl	%eax, %ecx
	addq	16(%r15), %rcx
	addl	$8, %eax
	movl	%eax, (%r15)
.L905:
	movl	(%rcx), %ecx
	movl	$-1, %eax
	testl	%ecx, %ecx
	cmovns	%ecx, %eax
	movl	%eax, 16(%rsp)
	movq	160(%rsp), %rax
	movzbl	(%rax), %r12d
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L852:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L853
	call	__lll_lock_wait_private
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L1433:
	leaq	192(%rsp), %rdi
	movq	%rdx, 120(%rsp)
	movl	%r8d, 100(%rsp)
	movl	%r9d, 24(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	24(%rsp), %r9d
	movl	100(%rsp), %r8d
	movq	120(%rsp), %rdx
	je	.L1372
	testl	%eax, %eax
	je	.L887
	movq	192(%rsp), %rax
	cmpb	$36, (%rax)
	je	.L893
.L887:
	movq	160(%rsp), %rax
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	$0, 148(%rsp)
	movq	$-1, 112(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	8(%rax), %rax
	movq	80(%rax), %rdi
	movq	72(%rax), %rsi
	movzbl	(%rdi), %eax
	movq	%rsi, 136(%rsp)
	testb	%al, %al
	je	.L1148
	cmpb	$127, %al
	je	.L1148
	cmpb	$0, (%rsi)
	movl	$0, %eax
	cmovne	%rdi, %rax
	movq	%rax, 112(%rsp)
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	16(%rsp), %ecx
	movl	$5, %eax
	leaq	.LC3(%rip), %r14
	cmpl	$5, %ecx
	cmovge	%ecx, %eax
	cmpb	$83, %r12b
	movl	%eax, 16(%rsp)
	jne	.L1041
	.p2align 4,,10
	.p2align 3
.L1042:
	movl	48(%rsp), %r8d
	movl	24(%rsp), %ecx
	movl	%ebp, %r9d
	movl	16(%rsp), %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, %ebp
	jns	.L927
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L967:
	testl	%r9d, %r9d
	je	.L994
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1458
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$45, (%rax)
.L1002:
	cmpl	$2147483647, %ebp
	je	.L1138
	subl	$1, 24(%rsp)
	addl	$1, %ebp
.L997:
	testq	%r14, %r14
	je	.L1003
	cmpl	$16, 100(%rsp)
	jne	.L1003
	movl	96(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L1003
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1459
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$48, (%rax)
.L1005:
	cmpl	$2147483647, %ebp
	je	.L1138
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1460
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rax)
.L1007:
	cmpl	$2147483646, %ebp
	je	.L1138
	subl	$2, 24(%rsp)
	addl	$2, %ebp
.L1003:
	movl	24(%rsp), %r12d
	addl	%ecx, %r13d
	subl	%r13d, %r12d
	movl	16(%rsp), %r13d
	testl	%r13d, %r13d
	jle	.L1008
	movslq	%r13d, %r14
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r10, 24(%rsp)
	movq	%rcx, 16(%rsp)
	call	__GI__IO_padn
	cmpq	%rax, %r14
	jne	.L1138
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movq	16(%rsp), %rcx
	movq	24(%rsp), %r10
	js	.L1010
	cmpl	%r14d, %ebp
	jb	.L1010
.L1009:
	testl	%eax, %eax
	jne	.L1081
	testl	%ebp, %ebp
	js	.L855
.L1008:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1115
.L1011:
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movq	%r10, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1138
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%rcx, %rax
	js	.L1013
	cmpq	%rcx, %rax
	jb	.L1013
.L1012:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1081
	testl	%eax, %eax
	js	.L855
	testl	%r12d, %r12d
	jg	.L1384
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L963:
	movl	48(%rsp), %esi
	subq	%rcx, %r13
	movl	$0, %eax
	cmovs	%rax, %r13
	movl	%r13d, 16(%rsp)
	testl	%esi, %esi
	jne	.L967
	movl	24(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	testq	%r14, %r14
	movl	%eax, %r13d
	je	.L968
.L1135:
	cmpl	$16, 100(%rsp)
	jne	.L968
	movl	96(%rsp), %edx
	leal	-2(%r13), %eax
	testl	%edx, %edx
	cmovne	%eax, %r13d
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L959:
	testq	%r14, %r14
	jne	.L1163
	cmpl	$8, 100(%rsp)
	jne	.L1164
	movl	96(%rsp), %eax
	testl	%eax, %eax
	je	.L1164
	leaq	999(%r11), %r10
	movb	$48, 1271(%rsp)
	xorl	%r13d, %r13d
	movl	$1, %ecx
	movb	$32, 56(%rsp)
.L960:
	movl	48(%rsp), %edx
	testl	%edx, %edx
	jne	.L967
	movl	24(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L994:
	movl	68(%rsp), %eax
	testl	%eax, %eax
	je	.L998
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1461
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L973:
	movl	68(%rsp), %eax
	testl	%eax, %eax
	je	.L977
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1462
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1162:
	movl	$1, %r13d
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1163:
	xorl	%r13d, %r13d
	movb	$32, 56(%rsp)
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L964:
	subq	%rcx, %r13
	cmovs	%r14, %r13
	movl	%r13d, 16(%rsp)
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	16(%rsp), %eax
	cmpl	$-1, %eax
	je	.L1089
	cmpl	$5, %eax
	jg	.L1089
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %r14
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L849:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 248(%rsp)
	movq	%rax, 240(%rsp)
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L965:
	subq	%rcx, %r13
	movl	$0, %eax
	cmovs	%rax, %r13
	movl	48(%rsp), %eax
	movl	%r13d, 16(%rsp)
	testl	%eax, %eax
	jne	.L967
	movl	24(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L998:
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L997
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1463
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L977:
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L976
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1464
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1450:
	testl	%r13d, %r13d
	jle	.L1165
	movslq	%r13d, %r8
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	movq	%r8, 24(%rsp)
	movq	%r10, 88(%rsp)
	movq	%rcx, 56(%rsp)
	movl	%r9d, 48(%rsp)
	call	__GI__IO_padn
	movq	24(%rsp), %r8
	cmpq	%rax, %r8
	jne	.L1138
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movl	48(%rsp), %r9d
	movq	56(%rsp), %rcx
	movq	88(%rsp), %r10
	js	.L972
	cmpl	%r8d, %ebp
	jb	.L972
.L971:
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1081
	testl	%ebp, %ebp
	jns	.L970
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1164:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	movb	$32, 56(%rsp)
	leaq	1000(%r11), %r10
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	40(%rsp), %rax
	cmpq	%rax, 32(%rsp)
	jbe	.L1465
.L1092:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r12, %rax
	jne	.L1138
	movslq	%ebp, %rax
	xorl	%edx, %edx
	addq	%r12, %rax
	js	.L1094
	cmpq	%r12, %rax
	jnb	.L1093
.L1094:
	movl	$1, %edx
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	176(%rsp)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	176(%rsp)
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1444:
	negq	%r10
	movl	$10, 100(%rsp)
	movl	$1, %r9d
	movq	%r10, %r14
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1089:
	movl	$6, %eax
	movl	$6, %r12d
	leaq	null(%rip), %r14
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, %ebp
	movl	%ebp, (%rdi)
	movl	$-1, %ebp
	movl	$9, %fs:(%rax)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L1141:
	movq	$-1, 112(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L933:
	testl	%r13d, %r13d
	jne	.L936
	cmpl	$47, %eax
	ja	.L937
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L938:
	movslq	(%rdx), %r10
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1048:
	testl	%r13d, %r13d
	jne	.L1051
	cmpl	$47, %eax
	ja	.L1052
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1053:
	movq	(%rdx), %rax
	movl	%ebp, (%rax)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1112:
#APP
# 1689 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1110
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1689 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1165:
	xorl	%r13d, %r13d
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%r10, 24(%rsp)
	movq	%rcx, 16(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %rcx
	movq	24(%rsp), %r10
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1454:
	movq	%r10, 24(%rsp)
	movq	%rcx, 16(%rsp)
	call	_IO_vtable_check
	movq	24(%rsp), %r10
	movq	16(%rsp), %rcx
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1448:
	leaq	160(%rsp), %rdi
	movq	%rdx, 128(%rsp)
	movl	%r8d, 120(%rsp)
	movl	%r9d, 100(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, 16(%rsp)
	je	.L1081
	movq	160(%rsp), %rax
	movq	128(%rsp), %rdx
	movl	120(%rsp), %r8d
	movl	100(%rsp), %r9d
	movzbl	(%rax), %r12d
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1455:
	testl	%r12d, %r12d
	jle	.L1060
	movslq	%r12d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	__GI__IO_padn
	cmpq	%rax, %r13
	jne	.L1138
	addl	%r12d, %ebp
	js	.L1081
	cmpl	%r12d, %ebp
	jb	.L1081
	testl	%ebp, %ebp
	jns	.L1060
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	$0, 112(%rsp)
	jmp	.L877
.L1127:
	call	_IO_vtable_check
	jmp	.L1077
.L1131:
	movl	%r8d, 16(%rsp)
	call	_IO_vtable_check
	movl	16(%rsp), %r8d
	jmp	.L1099
.L1427:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L840
.L1451:
	movq	%r10, 48(%rsp)
	movq	%rcx, 24(%rsp)
	movl	$45, %esi
.L1391:
	movq	%rbx, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	24(%rsp), %rcx
	movq	48(%rsp), %r10
	jne	.L981
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	%r10, 56(%rsp)
	movq	%rcx, 48(%rsp)
	movl	$45, %esi
.L1395:
	movq	%rbx, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	48(%rsp), %rcx
	movq	56(%rsp), %r10
	jne	.L1002
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L899:
	leaq	192(%rsp), %rdi
	movq	%rdx, 120(%rsp)
	movl	%r8d, 100(%rsp)
	movl	%r9d, 16(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	16(%rsp), %r9d
	movl	100(%rsp), %r8d
	movq	120(%rsp), %rdx
	je	.L1372
	testl	%eax, %eax
	je	.L903
	movq	192(%rsp), %rax
	cmpb	$36, (%rax)
	jne	.L903
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L1167:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L1028
.L1445:
	movl	$37, %esi
	movq	%rbx, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L926
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1166:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L1018
.L1452:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r10, 48(%rsp)
	movq	%rcx, 24(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	24(%rsp), %rcx
	movq	48(%rsp), %r10
	jne	.L984
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1453:
	movzbl	%r12b, %esi
	movq	%rbx, %rdi
	movq	%r10, 48(%rsp)
	movq	%rcx, 24(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	24(%rsp), %rcx
	movq	48(%rsp), %r10
	jne	.L986
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	%r10, 48(%rsp)
	movq	%rcx, 24(%rsp)
	movl	$43, %esi
	jmp	.L1391
.L1461:
	movq	%r10, 56(%rsp)
	movq	%rcx, 48(%rsp)
	movl	$43, %esi
	jmp	.L1395
.L1459:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r10, 56(%rsp)
	movq	%rcx, 48(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	48(%rsp), %rcx
	movq	56(%rsp), %r10
	jne	.L1005
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1460:
	movzbl	%r12b, %esi
	movq	%rbx, %rdi
	movq	%r10, 56(%rsp)
	movq	%rcx, 48(%rsp)
	call	__GI___overflow
	cmpl	$-1, %eax
	movq	48(%rsp), %rcx
	movq	56(%rsp), %r10
	jne	.L1007
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1456:
	movzbl	%dl, %esi
	movq	%rbx, %rdi
	call	__GI___overflow
	cmpl	$-1, %eax
	jne	.L1066
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1465:
	call	_IO_vtable_check
	jmp	.L1092
.L1463:
	movq	%r10, 56(%rsp)
	movq	%rcx, 48(%rsp)
	movl	$32, %esi
	jmp	.L1395
.L1464:
	movq	%r10, 48(%rsp)
	movq	%rcx, 24(%rsp)
	movl	$32, %esi
	jmp	.L1391
.L1106:
	movl	$1, %edx
	jmp	.L1105
.L1372:
	movq	__libc_errno@gottpoff(%rip), %rcx
	movl	%eax, %ebp
	movl	$75, %fs:(%rcx)
	jmp	.L855
.L1024:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1025
.L1123:
	movl	$1, %edx
	jmp	.L1122
.L1021:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1022
.L1013:
	movl	$1, %edx
	jmp	.L1012
.L972:
	movl	$1, %eax
	jmp	.L971
.L934:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L935
.L1049:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1050
.L953:
	cmpl	$47, %eax
	ja	.L956
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L957:
	movzwl	(%rdx), %r14d
	movl	$0, 68(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 72(%rsp)
	jmp	.L942
.L1079:
	movl	$1, %edx
	jmp	.L1078
.L956:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L957
.L1063:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1064
.L930:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L931
.L1046:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1047
.L1031:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1032
.L1038:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1039
.L888:
	movq	8(%r15), %rsi
	leaq	8(%rsi), %rcx
	movq	%rcx, 8(%r15)
	jmp	.L889
.L1034:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1035
.L951:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L952
.L1101:
	movl	$1, %edx
	jmp	.L1100
.L1010:
	movl	$1, %eax
	jmp	.L1009
.L1457:
	movq	8(%r15), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%r15)
	jmp	.L905
.L948:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L949
.L954:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L955
.L937:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L938
.L936:
	cmpl	$47, %eax
	ja	.L939
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L940:
	movswq	(%rdx), %r10
	jmp	.L932
.L1086:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1087
.L1052:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1053
.L1051:
	cmpl	$47, %eax
	ja	.L1054
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1055:
	movq	(%rdx), %rax
	movw	%bp, (%rax)
	jmp	.L927
.L1069:
	movq	8(%r15), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1070
.L939:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L940
.L1054:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1055
	.size	__vfprintf_internal, .-__vfprintf_internal
	.p2align 4,,15
	.type	buffered_vfprintf, @function
buffered_vfprintf:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8480, %rsp
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L1467
	movl	$-1, 192(%rdi)
.L1468:
	leaq	288(%rsp), %rax
	movq	%rdi, 256(%rsp)
	movq	%rdi, %rbx
	movl	$-1, 224(%rsp)
	movl	$-72515580, 32(%rsp)
	movq	%rax, 72(%rsp)
	movq	%rax, 64(%rsp)
	leaq	8480(%rsp), %rax
	movq	$0, 168(%rsp)
	movq	%rax, 80(%rsp)
	movl	116(%rdi), %eax
	leaq	32(%rsp), %rdi
	movl	%eax, 148(%rsp)
	leaq	_IO_helper_jumps(%rip), %rax
	movq	%rax, 248(%rsp)
	call	__vfprintf_internal
	movl	__libc_pthread_functions_init(%rip), %r13d
	movl	%eax, %r12d
	testl	%r13d, %r13d
	je	.L1470
	movq	184+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	movq	%rbx, %rdx
#APP
# 2298 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	call	*%rax
.L1471:
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L1472
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L1473
#APP
# 2299 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1474
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L1475:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L1473:
	addl	$1, 4(%rdi)
.L1472:
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rbp
	subq	%rsi, %rbp
	testl	%ebp, %ebp
	jle	.L1476
	movq	216(%rbx), %r14
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L1489
.L1477:
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpl	%eax, %ebp
	movl	$-1, %eax
	cmovne	%eax, %r12d
.L1476:
	testl	$32768, (%rbx)
	jne	.L1479
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1479
	movq	$0, 8(%rdi)
#APP
# 2319 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1481
	subl	$1, (%rdi)
.L1479:
	testl	%r13d, %r13d
	je	.L1466
	movq	192+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rdi
	xorl	%esi, %esi
#APP
# 2320 "vfprintf-internal.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L1466:
	addq	$8480, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1467:
	movl	$-1, %r12d
	cmpl	%r12d, %eax
	je	.L1468
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1489:
	call	_IO_vtable_check
	movq	64(%rsp), %rsi
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1474:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L1475
	call	__lll_lock_wait_private
	jmp	.L1475
	.p2align 4,,10
	.p2align 3
.L1481:
#APP
# 2319 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1479
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 2319 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1479
	.size	buffered_vfprintf, .-buffered_vfprintf
	.section	.rodata
	.align 32
	.type	step4_jumps.12976, @object
	.size	step4_jumps.12976, 120
step4_jumps.12976:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L229-.L228
	.long	.L233-.L228
	.long	.L240-.L228
	.long	.L242-.L228
	.long	.L243-.L228
	.long	.L316-.L228
	.long	.L344-.L228
	.long	.L371-.L228
	.long	.L332-.L228
	.long	.L336-.L228
	.long	.L342-.L228
	.long	.L345-.L228
	.long	.L324-.L228
	.long	0
	.long	0
	.long	0
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12972, @object
	.size	__PRETTY_FUNCTION__.12972, 18
__PRETTY_FUNCTION__.12972:
	.string	"printf_positional"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12638, @object
	.size	__PRETTY_FUNCTION__.12638, 15
__PRETTY_FUNCTION__.12638:
	.string	"outstring_func"
	.section	.rodata
	.align 32
	.type	step3b_jumps.12770, @object
	.size	step3b_jumps.12770, 120
step3b_jumps.12770:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L915-.L841
	.long	0
	.long	0
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	.L1017-.L841
	.long	.L1058-.L841
	.long	.L1085-.L841
	.long	.L1037-.L841
	.long	.L1043-.L841
	.long	.L1056-.L841
	.long	.L1059-.L841
	.long	.L1027-.L841
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step4_jumps.12771, @object
	.size	step4_jumps.12771, 120
step4_jumps.12771:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	.L1017-.L841
	.long	.L1058-.L841
	.long	.L1085-.L841
	.long	.L1037-.L841
	.long	.L1043-.L841
	.long	.L1056-.L841
	.long	.L1059-.L841
	.long	.L1027-.L841
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step3a_jumps.12768, @object
	.size	step3a_jumps.12768, 120
step3a_jumps.12768:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L911-.L841
	.long	0
	.long	0
	.long	0
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L1043-.L841
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	step2_jumps.12767, @object
	.size	step2_jumps.12767, 120
step2_jumps.12767:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L909-.L841
	.long	.L913-.L841
	.long	.L915-.L841
	.long	.L917-.L841
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	.L1017-.L841
	.long	.L1058-.L841
	.long	.L1085-.L841
	.long	.L1037-.L841
	.long	.L1043-.L841
	.long	.L1056-.L841
	.long	.L1059-.L841
	.long	.L1027-.L841
	.long	.L919-.L841
	.long	.L921-.L841
	.long	0
	.align 32
	.type	step1_jumps.12766, @object
	.size	step1_jumps.12766, 120
step1_jumps.12766:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	.L897-.L841
	.long	.L909-.L841
	.long	.L913-.L841
	.long	.L915-.L841
	.long	.L917-.L841
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	.L1017-.L841
	.long	.L1058-.L841
	.long	.L1085-.L841
	.long	.L1037-.L841
	.long	.L1043-.L841
	.long	.L1056-.L841
	.long	.L1059-.L841
	.long	.L1027-.L841
	.long	.L919-.L841
	.long	.L921-.L841
	.long	0
	.align 32
	.type	step0_jumps.12735, @object
	.size	step0_jumps.12735, 120
step0_jumps.12735:
	.long	0
	.long	.L863-.L841
	.long	.L867-.L841
	.long	.L869-.L841
	.long	.L871-.L841
	.long	.L873-.L841
	.long	.L876-.L841
	.long	.L881-.L841
	.long	.L894-.L841
	.long	.L897-.L841
	.long	.L909-.L841
	.long	.L913-.L841
	.long	.L915-.L841
	.long	.L917-.L841
	.long	.L923-.L841
	.long	.L928-.L841
	.long	.L943-.L841
	.long	.L945-.L841
	.long	.L946-.L841
	.long	.L1017-.L841
	.long	.L1058-.L841
	.long	.L1085-.L841
	.long	.L1037-.L841
	.long	.L1043-.L841
	.long	.L1056-.L841
	.long	.L1059-.L841
	.long	.L1027-.L841
	.long	.L919-.L841
	.long	.L921-.L841
	.long	.L879-.L841
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_helper_jumps, @object
	.size	_IO_helper_jumps, 168
_IO_helper_jumps:
	.quad	0
	.quad	0
	.quad	__GI__IO_default_finish
	.quad	_IO_helper_overflow
	.quad	_IO_default_underflow
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_default_pbackfail
	.quad	__GI__IO_default_xsputn
	.quad	__GI__IO_default_xsgetn
	.quad	_IO_default_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	__GI__IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.zero	16
	.section	.rodata
	.align 32
	.type	jump_table, @object
	.size	jump_table, 91
jump_table:
	.byte	1
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	14
	.byte	0
	.byte	6
	.byte	0
	.byte	0
	.byte	7
	.byte	2
	.byte	0
	.byte	3
	.byte	9
	.byte	0
	.byte	5
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	25
	.byte	0
	.byte	19
	.byte	19
	.byte	19
	.byte	0
	.byte	29
	.byte	0
	.byte	0
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	21
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	20
	.byte	15
	.byte	19
	.byte	19
	.byte	19
	.byte	10
	.byte	15
	.byte	28
	.byte	0
	.byte	11
	.byte	24
	.byte	23
	.byte	17
	.byte	22
	.byte	12
	.byte	0
	.byte	21
	.byte	27
	.byte	16
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.section	.rodata.str1.1
	.type	null, @object
	.size	null, 7
null:
	.string	"(null)"
	.hidden	__lll_lock_wait_private
	.hidden	__printf_modifier_table
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__readonly_area
	.hidden	__printf_fphex
	.hidden	__printf_va_arg_table
	.hidden	_itoa_word
	.hidden	__printf_function_table
	.hidden	__printf_arginfo_table
	.hidden	__parse_one_specmb
	.hidden	__wcsrtombs
	.hidden	__wcrtomb
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
