	.text
	.hidden	_itoa_lower_digits
	.globl	_itoa_lower_digits
	.section	.rodata
	.align 32
	.type	_itoa_lower_digits, @object
	.size	_itoa_lower_digits, 36
_itoa_lower_digits:
	.ascii	"0123456789abcdefghijklmnopqrstuvwxyz"
