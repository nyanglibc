	.text
	.p2align 4,,15
	.globl	__isoc99_vscanf
	.type	__isoc99_vscanf, @function
__isoc99_vscanf:
.LFB68:
	.cfi_startproc
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdin(%rip), %rdi
	movl	$2, %ecx
	jmp	__vfscanf_internal
	.cfi_endproc
.LFE68:
	.size	__isoc99_vscanf, .-__isoc99_vscanf
	.hidden	__vfscanf_internal
