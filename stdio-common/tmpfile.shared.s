	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_tmpfile,tmpfile@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"tmpf"
.LC1:
	.string	"w+b"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__new_tmpfile
	.type	__new_tmpfile, @function
__new_tmpfile:
.LFB64:
	pushq	%rbp
	pushq	%rbx
	xorl	%edi, %edi
	subq	$4104, %rsp
	call	__GI___gen_tempfd
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L8
.L2:
	leaq	.LC1(%rip), %rsi
	movl	%ebx, %edi
	call	__GI__IO_fdopen
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L9
.L1:
	addq	$4104, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsp, %rbp
	leaq	.LC0(%rip), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$4096, %esi
	movq	%rbp, %rdi
	call	__path_search
	testl	%eax, %eax
	jne	.L3
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__gen_tempname
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L3
	movq	%rbp, %rdi
	call	__unlink
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%ebx, %edi
	call	__GI___close
	addq	$4104, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%ebp, %ebp
	jmp	.L1
.LFE64:
	.size	__new_tmpfile, .-__new_tmpfile
	.weak	tmpfile64
	.set	tmpfile64,__new_tmpfile
	.hidden	__unlink
	.hidden	__gen_tempname
	.hidden	__path_search
