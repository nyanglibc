	.text
	.p2align 4,,15
	.globl	remove
	.hidden	remove
	.type	remove, @function
remove:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__unlink
	testl	%eax, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$-1, %eax
	cmpl	$21, %fs:(%rdx)
	jne	.L1
	movq	%rbx, %rdi
	call	__rmdir
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	negl	%eax
.L1:
	popq	%rbx
	ret
	.size	remove, .-remove
	.hidden	__rmdir
	.hidden	__unlink
