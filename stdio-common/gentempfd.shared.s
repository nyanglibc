	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/tmp"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___gen_tempfd
	.hidden	__GI___gen_tempfd
	.type	__GI___gen_tempfd, @function
__GI___gen_tempfd:
	movl	%edi, %esi
	leaq	.LC0(%rip), %rdi
	movl	$384, %edx
	orl	$4259970, %esi
	xorl	%eax, %eax
	jmp	__GI___open
	.size	__GI___gen_tempfd, .-__GI___gen_tempfd
	.globl	__gen_tempfd
	.set	__gen_tempfd,__GI___gen_tempfd
