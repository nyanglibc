	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_sys_errlist_internal
	.globl	_sys_errlist_internal
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Success"
.LC1:
	.string	"Operation not permitted"
.LC2:
	.string	"No such file or directory"
.LC3:
	.string	"No such process"
.LC4:
	.string	"Interrupted system call"
.LC5:
	.string	"Input/output error"
.LC6:
	.string	"No such device or address"
.LC7:
	.string	"Argument list too long"
.LC8:
	.string	"Exec format error"
.LC9:
	.string	"Bad file descriptor"
.LC10:
	.string	"No child processes"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"Resource temporarily unavailable"
	.section	.rodata.str1.1
.LC12:
	.string	"Cannot allocate memory"
.LC13:
	.string	"Permission denied"
.LC14:
	.string	"Bad address"
.LC15:
	.string	"Block device required"
.LC16:
	.string	"Device or resource busy"
.LC17:
	.string	"File exists"
.LC18:
	.string	"Invalid cross-device link"
.LC19:
	.string	"No such device"
.LC20:
	.string	"Not a directory"
.LC21:
	.string	"Is a directory"
.LC22:
	.string	"Invalid argument"
.LC23:
	.string	"Too many open files in system"
.LC24:
	.string	"Too many open files"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"Inappropriate ioctl for device"
	.section	.rodata.str1.1
.LC26:
	.string	"Text file busy"
.LC27:
	.string	"File too large"
.LC28:
	.string	"No space left on device"
.LC29:
	.string	"Illegal seek"
.LC30:
	.string	"Read-only file system"
.LC31:
	.string	"Too many links"
.LC32:
	.string	"Broken pipe"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"Numerical argument out of domain"
	.section	.rodata.str1.1
.LC34:
	.string	"Numerical result out of range"
.LC35:
	.string	"Resource deadlock avoided"
.LC36:
	.string	"File name too long"
.LC37:
	.string	"No locks available"
.LC38:
	.string	"Function not implemented"
.LC39:
	.string	"Directory not empty"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"Too many levels of symbolic links"
	.section	.rodata.str1.1
.LC41:
	.string	"No message of desired type"
.LC42:
	.string	"Identifier removed"
.LC43:
	.string	"Channel number out of range"
.LC44:
	.string	"Level 2 not synchronized"
.LC45:
	.string	"Level 3 halted"
.LC46:
	.string	"Level 3 reset"
.LC47:
	.string	"Link number out of range"
.LC48:
	.string	"Protocol driver not attached"
.LC49:
	.string	"No CSI structure available"
.LC50:
	.string	"Level 2 halted"
.LC51:
	.string	"Invalid exchange"
.LC52:
	.string	"Invalid request descriptor"
.LC53:
	.string	"Exchange full"
.LC54:
	.string	"No anode"
.LC55:
	.string	"Invalid request code"
.LC56:
	.string	"Invalid slot"
.LC57:
	.string	"Bad font file format"
.LC58:
	.string	"Device not a stream"
.LC59:
	.string	"No data available"
.LC60:
	.string	"Timer expired"
.LC61:
	.string	"Out of streams resources"
.LC62:
	.string	"Machine is not on the network"
.LC63:
	.string	"Package not installed"
.LC64:
	.string	"Object is remote"
.LC65:
	.string	"Link has been severed"
.LC66:
	.string	"Advertise error"
.LC67:
	.string	"Srmount error"
.LC68:
	.string	"Communication error on send"
.LC69:
	.string	"Protocol error"
.LC70:
	.string	"Multihop attempted"
.LC71:
	.string	"RFS specific error"
.LC72:
	.string	"Bad message"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"Value too large for defined data type"
	.section	.rodata.str1.1
.LC74:
	.string	"Name not unique on network"
.LC75:
	.string	"File descriptor in bad state"
.LC76:
	.string	"Remote address changed"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"Can not access a needed shared library"
	.align 8
.LC78:
	.string	"Accessing a corrupted shared library"
	.align 8
.LC79:
	.string	".lib section in a.out corrupted"
	.align 8
.LC80:
	.string	"Attempting to link in too many shared libraries"
	.align 8
.LC81:
	.string	"Cannot exec a shared library directly"
	.align 8
.LC82:
	.string	"Invalid or incomplete multibyte or wide character"
	.align 8
.LC83:
	.string	"Interrupted system call should be restarted"
	.section	.rodata.str1.1
.LC84:
	.string	"Streams pipe error"
.LC85:
	.string	"Too many users"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"Socket operation on non-socket"
	.section	.rodata.str1.1
.LC87:
	.string	"Destination address required"
.LC88:
	.string	"Message too long"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"Protocol wrong type for socket"
	.section	.rodata.str1.1
.LC90:
	.string	"Protocol not available"
.LC91:
	.string	"Protocol not supported"
.LC92:
	.string	"Socket type not supported"
.LC93:
	.string	"Operation not supported"
.LC94:
	.string	"Protocol family not supported"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"Address family not supported by protocol"
	.section	.rodata.str1.1
.LC96:
	.string	"Address already in use"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"Cannot assign requested address"
	.section	.rodata.str1.1
.LC98:
	.string	"Network is down"
.LC99:
	.string	"Network is unreachable"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"Network dropped connection on reset"
	.align 8
.LC101:
	.string	"Software caused connection abort"
	.section	.rodata.str1.1
.LC102:
	.string	"Connection reset by peer"
.LC103:
	.string	"No buffer space available"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"Transport endpoint is already connected"
	.align 8
.LC105:
	.string	"Transport endpoint is not connected"
	.align 8
.LC106:
	.string	"Cannot send after transport endpoint shutdown"
	.align 8
.LC107:
	.string	"Too many references: cannot splice"
	.section	.rodata.str1.1
.LC108:
	.string	"Connection timed out"
.LC109:
	.string	"Connection refused"
.LC110:
	.string	"Host is down"
.LC111:
	.string	"No route to host"
.LC112:
	.string	"Operation already in progress"
.LC113:
	.string	"Operation now in progress"
.LC114:
	.string	"Stale file handle"
.LC115:
	.string	"Structure needs cleaning"
.LC116:
	.string	"Not a XENIX named type file"
.LC117:
	.string	"No XENIX semaphores available"
.LC118:
	.string	"Is a named type file"
.LC119:
	.string	"Remote I/O error"
.LC120:
	.string	"Disk quota exceeded"
.LC121:
	.string	"No medium found"
.LC122:
	.string	"Wrong medium type"
.LC123:
	.string	"Operation canceled"
.LC124:
	.string	"Required key not available"
.LC125:
	.string	"Key has expired"
.LC126:
	.string	"Key has been revoked"
.LC127:
	.string	"Key was rejected by service"
.LC128:
	.string	"Owner died"
.LC129:
	.string	"State not recoverable"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"Operation not possible due to RF-kill"
	.align 8
.LC131:
	.string	"Memory page has hardware error"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_sys_errlist_internal, @object
	.size	_sys_errlist_internal, 1072
_sys_errlist_internal:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.zero	8
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.zero	8
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.text
	.p2align 4,,15
	.globl	__get_errlist
	.hidden	__get_errlist
	.type	__get_errlist, @function
__get_errlist:
	cmpl	$133, %edi
	ja	.L3
	leaq	_sys_errlist_internal(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.size	__get_errlist, .-__get_errlist
	.section	.rodata
	.align 32
	.type	_sys_errname, @object
	.size	_sys_errname, 1109
_sys_errname:
	.string	"0"
	.string	"EPERM"
	.string	"ENOENT"
	.string	"ESRCH"
	.string	"EINTR"
	.string	"EIO"
	.string	"ENXIO"
	.string	"E2BIG"
	.string	"ENOEXEC"
	.string	"EBADF"
	.string	"ECHILD"
	.string	"EDEADLK"
	.string	"ENOMEM"
	.string	"EACCES"
	.string	"EFAULT"
	.string	"ENOTBLK"
	.string	"EBUSY"
	.string	"EEXIST"
	.string	"EXDEV"
	.string	"ENODEV"
	.string	"ENOTDIR"
	.string	"EISDIR"
	.string	"EINVAL"
	.string	"EMFILE"
	.string	"ENFILE"
	.string	"ENOTTY"
	.string	"ETXTBSY"
	.string	"EFBIG"
	.string	"ENOSPC"
	.string	"ESPIPE"
	.string	"EROFS"
	.string	"EMLINK"
	.string	"EPIPE"
	.string	"EDOM"
	.string	"ERANGE"
	.string	"EAGAIN"
	.string	"EINPROGRESS"
	.string	"EALREADY"
	.string	"ENOTSOCK"
	.string	"EMSGSIZE"
	.string	"EPROTOTYPE"
	.string	"ENOPROTOOPT"
	.string	"EPROTONOSUPPORT"
	.string	"ESOCKTNOSUPPORT"
	.string	"EOPNOTSUPP"
	.string	"EPFNOSUPPORT"
	.string	"EAFNOSUPPORT"
	.string	"EADDRINUSE"
	.string	"EADDRNOTAVAIL"
	.string	"ENETDOWN"
	.string	"ENETUNREACH"
	.string	"ENETRESET"
	.string	"ECONNABORTED"
	.string	"ECONNRESET"
	.string	"ENOBUFS"
	.string	"EISCONN"
	.string	"ENOTCONN"
	.string	"EDESTADDRREQ"
	.string	"ESHUTDOWN"
	.string	"ETOOMANYREFS"
	.string	"ETIMEDOUT"
	.string	"ECONNREFUSED"
	.string	"ELOOP"
	.string	"ENAMETOOLONG"
	.string	"EHOSTDOWN"
	.string	"EHOSTUNREACH"
	.string	"ENOTEMPTY"
	.string	"EUSERS"
	.string	"EDQUOT"
	.string	"ESTALE"
	.string	"EREMOTE"
	.string	"ENOLCK"
	.string	"ENOSYS"
	.string	"EILSEQ"
	.string	"EBADMSG"
	.string	"EIDRM"
	.string	"EMULTIHOP"
	.string	"ENODATA"
	.string	"ENOLINK"
	.string	"ENOMSG"
	.string	"ENOSR"
	.string	"ENOSTR"
	.string	"EOVERFLOW"
	.string	"EPROTO"
	.string	"ETIME"
	.string	"ECANCELED"
	.string	"EOWNERDEAD"
	.string	"ENOTRECOVERABLE"
	.string	"ERESTART"
	.string	"ECHRNG"
	.string	"EL2NSYNC"
	.string	"EL3HLT"
	.string	"EL3RST"
	.string	"ELNRNG"
	.string	"EUNATCH"
	.string	"ENOCSI"
	.string	"EL2HLT"
	.string	"EBADE"
	.string	"EBADR"
	.string	"EXFULL"
	.string	"ENOANO"
	.string	"EBADRQC"
	.string	"EBADSLT"
	.string	"EBFONT"
	.string	"ENONET"
	.string	"ENOPKG"
	.string	"EADV"
	.string	"ESRMNT"
	.string	"ECOMM"
	.string	"EDOTDOT"
	.string	"ENOTUNIQ"
	.string	"EBADFD"
	.string	"EREMCHG"
	.string	"ELIBACC"
	.string	"ELIBBAD"
	.string	"ELIBSCN"
	.string	"ELIBMAX"
	.string	"ELIBEXEC"
	.string	"ESTRPIPE"
	.string	"EUCLEAN"
	.string	"ENOTNAM"
	.string	"ENAVAIL"
	.string	"EISNAM"
	.string	"EREMOTEIO"
	.string	"ENOMEDIUM"
	.string	"EMEDIUMTYPE"
	.string	"ENOKEY"
	.string	"EKEYEXPIRED"
	.string	"EKEYREVOKED"
	.string	"EKEYREJECTED"
	.string	"ERFKILL"
	.string	"EHWPOISON"
	.align 32
	.type	_sys_errnameidx, @object
	.size	_sys_errnameidx, 268
_sys_errnameidx:
	.value	0
	.value	2
	.value	8
	.value	15
	.value	21
	.value	27
	.value	31
	.value	37
	.value	43
	.value	51
	.value	57
	.value	229
	.value	72
	.value	79
	.value	86
	.value	93
	.value	101
	.value	107
	.value	114
	.value	120
	.value	127
	.value	135
	.value	142
	.value	156
	.value	149
	.value	163
	.value	170
	.value	178
	.value	184
	.value	191
	.value	198
	.value	204
	.value	211
	.value	217
	.value	222
	.value	64
	.value	537
	.value	612
	.value	619
	.value	573
	.value	531
	.zero	2
	.value	673
	.value	641
	.value	762
	.value	769
	.value	778
	.value	785
	.value	792
	.value	799
	.value	807
	.value	814
	.value	821
	.value	827
	.value	833
	.value	840
	.value	847
	.value	855
	.zero	2
	.value	863
	.value	686
	.value	657
	.value	710
	.value	680
	.value	870
	.value	877
	.value	604
	.value	665
	.value	884
	.value	889
	.value	896
	.value	703
	.value	647
	.value	902
	.value	633
	.value	693
	.value	910
	.value	919
	.value	926
	.value	934
	.value	942
	.value	950
	.value	958
	.value	966
	.value	626
	.value	753
	.value	975
	.value	583
	.value	257
	.value	472
	.value	266
	.value	275
	.value	286
	.value	298
	.value	314
	.value	330
	.value	341
	.value	354
	.value	367
	.value	378
	.value	392
	.value	401
	.value	413
	.value	423
	.value	436
	.value	447
	.value	455
	.value	463
	.value	485
	.value	495
	.value	508
	.value	518
	.value	550
	.value	560
	.value	248
	.value	236
	.value	597
	.value	984
	.value	992
	.value	1000
	.value	1008
	.value	1015
	.value	590
	.value	1025
	.value	1035
	.value	716
	.value	1047
	.value	1054
	.value	1066
	.value	1078
	.value	726
	.value	737
	.value	1091
	.value	1099
	.text
	.p2align 4,,15
	.globl	__get_errname
	.hidden	__get_errname
	.type	__get_errname, @function
__get_errname:
	cmpl	$133, %edi
	ja	.L9
	leaq	_sys_errnameidx(%rip), %rax
	movslq	%edi, %rdx
	testl	%edi, %edi
	movzwl	(%rax,%rdx,2), %eax
	je	.L7
	testw	%ax, %ax
	je	.L9
.L7:
	leaq	_sys_errname(%rip), %rdx
	addq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	ret
	.size	__get_errname, .-__get_errname
	.globl	__GLIBC_2_1_sys_nerr
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
	.type	__GLIBC_2_1_sys_nerr, @object
	.size	__GLIBC_2_1_sys_nerr, 4
__GLIBC_2_1_sys_nerr:
	.long	125
	.globl	__GLIBC_2_1__sys_nerr
	.set	__GLIBC_2_1__sys_nerr,__GLIBC_2_1_sys_nerr
#APP
	.globl __GLIBC_2_1_sys_errlist
	.set __GLIBC_2_1_sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_1_sys_errlist, %object
	.size __GLIBC_2_1_sys_errlist, 125 * (64 / 8)
	.globl __GLIBC_2_1__sys_errlist
	.set __GLIBC_2_1__sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_1__sys_errlist, %object
	.size __GLIBC_2_1__sys_errlist, 125 * (64 / 8)
	.symver __GLIBC_2_1_sys_nerr,sys_nerr@GLIBC_2.2.5
	.symver __GLIBC_2_1__sys_nerr,_sys_nerr@GLIBC_2.2.5
	.symver __GLIBC_2_1_sys_errlist,sys_errlist@GLIBC_2.2.5
	.symver __GLIBC_2_1__sys_errlist,_sys_errlist@GLIBC_2.2.5
#NO_APP
	.globl	__GLIBC_2_3_sys_nerr
	.align 4
	.type	__GLIBC_2_3_sys_nerr, @object
	.size	__GLIBC_2_3_sys_nerr, 4
__GLIBC_2_3_sys_nerr:
	.long	126
	.globl	__GLIBC_2_3__sys_nerr
	.set	__GLIBC_2_3__sys_nerr,__GLIBC_2_3_sys_nerr
#APP
	.globl __GLIBC_2_3_sys_errlist
	.set __GLIBC_2_3_sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_3_sys_errlist, %object
	.size __GLIBC_2_3_sys_errlist, 126 * (64 / 8)
	.globl __GLIBC_2_3__sys_errlist
	.set __GLIBC_2_3__sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_3__sys_errlist, %object
	.size __GLIBC_2_3__sys_errlist, 126 * (64 / 8)
	.symver __GLIBC_2_3_sys_nerr,sys_nerr@GLIBC_2.3
	.symver __GLIBC_2_3__sys_nerr,_sys_nerr@GLIBC_2.3
	.symver __GLIBC_2_3_sys_errlist,sys_errlist@GLIBC_2.3
	.symver __GLIBC_2_3__sys_errlist,_sys_errlist@GLIBC_2.3
#NO_APP
	.globl	__GLIBC_2_4_sys_nerr
	.align 4
	.type	__GLIBC_2_4_sys_nerr, @object
	.size	__GLIBC_2_4_sys_nerr, 4
__GLIBC_2_4_sys_nerr:
	.long	132
	.globl	__GLIBC_2_4__sys_nerr
	.set	__GLIBC_2_4__sys_nerr,__GLIBC_2_4_sys_nerr
#APP
	.globl __GLIBC_2_4_sys_errlist
	.set __GLIBC_2_4_sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_4_sys_errlist, %object
	.size __GLIBC_2_4_sys_errlist, 132 * (64 / 8)
	.globl __GLIBC_2_4__sys_errlist
	.set __GLIBC_2_4__sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_4__sys_errlist, %object
	.size __GLIBC_2_4__sys_errlist, 132 * (64 / 8)
	.symver __GLIBC_2_4_sys_nerr,sys_nerr@GLIBC_2.4
	.symver __GLIBC_2_4__sys_nerr,_sys_nerr@GLIBC_2.4
	.symver __GLIBC_2_4_sys_errlist,sys_errlist@GLIBC_2.4
	.symver __GLIBC_2_4__sys_errlist,_sys_errlist@GLIBC_2.4
#NO_APP
	.globl	__GLIBC_2_12_sys_nerr
	.align 4
	.type	__GLIBC_2_12_sys_nerr, @object
	.size	__GLIBC_2_12_sys_nerr, 4
__GLIBC_2_12_sys_nerr:
	.long	135
	.globl	__GLIBC_2_12__sys_nerr
	.set	__GLIBC_2_12__sys_nerr,__GLIBC_2_12_sys_nerr
#APP
	.globl __GLIBC_2_12_sys_errlist
	.set __GLIBC_2_12_sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_12_sys_errlist, %object
	.size __GLIBC_2_12_sys_errlist, 135 * (64 / 8)
	.globl __GLIBC_2_12__sys_errlist
	.set __GLIBC_2_12__sys_errlist, _sys_errlist_internal
	.type __GLIBC_2_12__sys_errlist, %object
	.size __GLIBC_2_12__sys_errlist, 135 * (64 / 8)
	.symver __GLIBC_2_12_sys_nerr,sys_nerr@GLIBC_2.12
	.symver __GLIBC_2_12__sys_nerr,_sys_nerr@GLIBC_2.12
	.symver __GLIBC_2_12_sys_errlist,sys_errlist@GLIBC_2.12
	.symver __GLIBC_2_12__sys_errlist,_sys_errlist@GLIBC_2.12
