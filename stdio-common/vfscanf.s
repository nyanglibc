	.text
	.p2align 4,,15
	.globl	___vfscanf
	.type	___vfscanf, @function
___vfscanf:
.LFB68:
	xorl	%ecx, %ecx
	jmp	__vfscanf_internal
.LFE68:
	.size	___vfscanf, .-___vfscanf
	.weak	vfscanf
	.set	vfscanf,___vfscanf
	.globl	__vfscanf
	.hidden	__vfscanf
	.set	__vfscanf,___vfscanf
	.hidden	__vfscanf_internal
