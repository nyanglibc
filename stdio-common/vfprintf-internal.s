	.text
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rcx
	movl	$-1, %esi
	movl	$2147483647, %r9d
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-48(%rdx), %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	leal	(%rax,%rax,4), %eax
	movl	%r9d, %r8d
	subl	%edx, %r8d
	addl	%eax, %eax
	addl	%eax, %edx
	cmpl	%eax, %r8d
	movl	%edx, %eax
	cmovl	%esi, %eax
.L3:
	addq	$1, %rcx
.L2:
	movq	%rcx, (%rdi)
	movzbl	(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L8
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jle	.L9
	movl	%esi, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	rep ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	group_number, @function
group_number:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rdi
	movq	%r8, %r14
	movq	%rsi, %rbx
	movq	%rdx, %r12
	subq	$24, %rsp
	call	strlen
	movsbl	(%r15), %ebp
	movq	%rax, %r8
	leal	-1(%rbp), %eax
	cmpb	$125, %al
	jbe	.L24
.L11:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	subq	%rbx, %rcx
	movq	%r8, 8(%rsp)
	addq	$1, %r15
	movq	%rcx, %rdx
	movq	%rcx, (%rsp)
	call	memmove
	movq	(%rsp), %rcx
	leaq	0(%r13,%rcx), %rsi
	cmpq	%rsi, %r13
	jnb	.L21
	movq	8(%rsp), %r8
	movslq	%r8d, %rax
	.p2align 4,,10
	.p2align 3
.L12:
	subq	$1, %rsi
	movzbl	(%rsi), %edx
	subl	$1, %ebp
	leaq	-1(%r12), %rbx
	movb	%dl, -1(%r12)
	jne	.L13
	cmpq	%rsi, %r13
	jnb	.L11
	movq	%rbx, %rdx
	subq	%rsi, %rdx
	cmpq	%rdx, %rax
	jge	.L14
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	-1(%r14,%rdi), %r9d
	subq	$1, %rdi
	subq	$1, %rbx
	testl	%edi, %edi
	movb	%r9b, (%rbx)
	jg	.L15
	movsbl	(%r15), %ebp
	cmpb	$127, %bpl
	je	.L14
	testb	%bpl, %bpl
	js	.L14
	testb	%bpl, %bpl
	je	.L18
	addq	$1, %r15
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbx, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	%rsi, %r13
	jb	.L20
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	movsbl	-1(%r15), %ebp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	memmove
	jmp	.L11
.L21:
	movq	%r12, %rbx
	jmp	.L11
	.size	group_number, .-group_number
	.p2align 4,,15
	.type	_IO_helper_overflow, @function
_IO_helper_overflow:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rcx
	movq	32(%rdi), %rsi
	movq	%rcx, %rbp
	subq	%rsi, %rbp
	testl	%ebp, %ebp
	je	.L26
	movq	224(%rdi), %rdi
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	216(%rdi), %r13
	subq	%rdx, %rax
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L32
.L27:
	movslq	%ebp, %rbp
	movq	%rbp, %rdx
	call	*56(%r13)
	movq	%rax, %r13
	leaq	-1(%rax), %rax
	cmpq	$-3, %rax
	ja	.L30
	movq	32(%rbx), %rdi
	movq	%rbp, %rdx
	subq	%r13, %rdx
	leaq	(%rdi,%r13), %rsi
	call	memmove
	movq	40(%rbx), %rcx
	subq	%r13, %rcx
	movq	%rcx, 40(%rbx)
.L26:
	cmpq	%rcx, 48(%rbx)
	movzbl	%r12b, %eax
	jbe	.L33
	leaq	1(%rcx), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rcx)
.L25:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	32(%rbx), %rsi
	movq	8(%rsp), %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$24, %rsp
	movq	%rbx, %rdi
	movl	%eax, %esi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__overflow
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %eax
	jmp	.L25
	.size	_IO_helper_overflow, .-_IO_helper_overflow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_outpunct"
	.text
	.p2align 4,,15
	.type	_i18n_number_rewrite, @function
_i18n_number_rewrite:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %rbx
	subq	$1128, %rsp
	call	__wctrans@PLT
	movl	$46, %edi
	movq	%rax, %rbp
	movq	%rax, %rsi
	call	__towctrans
	movq	%rbp, %rsi
	movl	$44, %edi
	movl	%eax, %r15d
	call	__towctrans
	testq	%rbp, %rbp
	leaq	80(%rsp), %r12
	jne	.L71
.L35:
	leaq	16(%r12), %rax
	subq	%r14, %r13
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$1024, 88(%rsp)
	movq	%rax, 80(%rsp)
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L52
	movq	80(%rsp), %r15
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__mempcpy@PLT
	movq	%rax, %r13
	leaq	48(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 8(%rsp)
.L41:
	subq	$1, %r13
	cmpq	%r13, %r15
	ja	.L72
.L51:
	movsbl	0(%r13), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L73
	testq	%rbp, %rbp
	jne	.L74
.L45:
	subq	$1, %r13
	movb	%al, -1(%rbx)
	subq	$1, %rbx
	cmpq	%r13, %r15
	jbe	.L51
.L72:
	movq	80(%rsp), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L34
	call	free@PLT
.L34:
	addq	$1128, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rcx
	subl	$7, %eax
	cltq
	movq	%fs:(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx,%rax,8), %r14
	movq	%r14, %rdi
	call	strlen
	subq	%rax, %rbx
	testq	%rax, %rax
	leaq	-1(%rax), %rcx
	je	.L41
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	(%r14,%rcx), %eax
	movb	%al, (%rbx,%rcx)
	subq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L74:
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpb	$44, %cl
	jne	.L45
	cmpb	$46, %al
	movq	(%rsp), %rsi
	cmove	8(%rsp), %rsi
	movq	%rsi, %rcx
.L48:
	movl	(%rcx), %edi
	addq	$4, %rcx
	leal	-16843009(%rdi), %eax
	notl	%edi
	andl	%edi, %eax
	andl	$-2139062144, %eax
	je	.L48
	movl	%eax, %edi
	shrl	$16, %edi
	testl	$32896, %eax
	cmove	%edi, %eax
	leaq	2(%rcx), %rdi
	movl	%eax, %edx
	cmove	%rdi, %rcx
	addb	%al, %dl
	sbbq	$3, %rcx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rax
	je	.L41
	.p2align 4,,10
	.p2align 3
.L50:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L50
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	16(%rsp), %rdi
	movq	%r12, %rdx
	movl	%r15d, %esi
	movl	%eax, (%rsp)
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movl	(%rsp), %ecx
	je	.L75
	movb	$0, 16(%rsp,%rax)
.L37:
	leaq	48(%rsp), %rdi
	movq	%r12, %rdx
	movl	%ecx, %esi
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L76
	movb	$0, 48(%rsp,%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r14, %rbx
	jmp	.L34
.L75:
	movl	$46, %edx
	movw	%dx, 16(%rsp)
	jmp	.L37
.L76:
	movl	$44, %eax
	movw	%ax, 48(%rsp)
	jmp	.L35
	.size	_i18n_number_rewrite, .-_i18n_number_rewrite
	.section	.rodata.str1.1
.LC1:
	.string	"vfprintf-internal.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(size_t) done <= (size_t) INT_MAX"
	.section	.text.unlikely,"ax",@progbits
	.type	outstring_func.part.1, @function
outstring_func.part.1:
	leaq	__PRETTY_FUNCTION__.12650(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$238, %edx
	call	__assert_fail
	.size	outstring_func.part.1, .-outstring_func.part.1
	.text
	.p2align 4,,15
	.type	outstring_converted_wide_string, @function
outstring_converted_wide_string:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%r9d, %ebx
	subq	$328, %rsp
	testl	%ecx, %ecx
	setg	23(%rsp)
	cmpb	$1, %r8b
	movl	%ecx, 24(%rsp)
	movq	%rsi, 40(%rsp)
	movl	%edx, 16(%rsp)
	movl	%r8d, 28(%rsp)
	movzbl	23(%rsp), %ecx
	je	.L80
	testb	%cl, %cl
	je	.L80
	testl	%edx, %edx
	movq	$0, 56(%rsp)
	movq	%rsi, 48(%rsp)
	js	.L161
	movslq	16(%rsp), %r13
	movl	$0, %r12d
	je	.L83
	testq	%rsi, %rsi
	je	.L83
	leaq	48(%rsp), %rax
	movq	%r13, %rbx
	leaq	56(%rsp), %r15
	leaq	64(%rsp), %r14
	movl	%r9d, (%rsp)
	movq	%rax, %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L162:
	testq	%rax, %rax
	je	.L155
	addq	%rax, %r12
	subq	%rax, %rbx
	je	.L155
	cmpq	$0, 48(%rsp)
	je	.L155
.L86:
	cmpq	$256, %rbx
	movl	$256, %edx
	movq	%r15, %rcx
	cmovb	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__wcsrtombs
	cmpq	$-1, %rax
	jne	.L162
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$-1, %eax
.L79:
	addq	$328, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	movl	(%rsp), %ebx
.L82:
	movslq	24(%rsp), %rax
	cmpq	%r12, %rax
	ja	.L83
.L87:
	movq	40(%rsp), %rsi
.L80:
	movslq	16(%rsp), %r8
	movq	$-1, %rax
	movq	$0, 56(%rsp)
	leaq	64(%rsp), %r12
	testl	%r8d, %r8d
	cmovns	%r8, %rax
	xorl	%r13d, %r13d
	movq	%rax, %r15
	leaq	56(%rsp), %rax
	movq	%r15, %r14
	movq	%rax, (%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 8(%rsp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	*56(%r9)
	cmpq	%rax, %r15
	jne	.L159
	movslq	%ebx, %rbx
	movabsq	$-9223372036854775808, %rdi
	leaq	(%rbx,%r15), %rax
	leaq	(%rax,%rdi), %rdx
	movslq	%eax, %rsi
	movl	%eax, %ebx
	cmpq	%r15, %rdx
	setb	%dl
	cmpq	%rsi, %rax
	movl	$1, %esi
	movzbl	%dl, %edx
	cmovne	%esi, %edx
	testl	%edx, %edx
	jne	.L158
	testl	%eax, %eax
	js	.L79
	movl	16(%rsp), %edx
	movq	%r14, %rax
	movq	40(%rsp), %rsi
	subq	%r15, %rax
	addq	%r15, %r13
	testl	%edx, %edx
	cmovns	%rax, %r14
.L94:
	testq	%r14, %r14
	je	.L110
	testq	%rsi, %rsi
	je	.L110
	movq	(%rsp), %rcx
	movq	8(%rsp), %rsi
	cmpq	$256, %r14
	movl	$256, %edx
	movq	%r12, %rdi
	cmovb	%r14, %rdx
	call	__wcsrtombs
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L159
	testq	%rax, %rax
	je	.L110
	testl	%ebx, %ebx
	js	.L163
	movq	216(%rbp), %r9
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r9, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	ja	.L101
	movq	%r9, 32(%rsp)
	call	_IO_vtable_check
	movq	32(%rsp), %r9
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L110:
	cmpb	$0, 23(%rsp)
	je	.L125
	cmpb	$0, 28(%rsp)
	je	.L125
	movslq	24(%rsp), %rdx
	movl	%ebx, %eax
	cmpq	%r13, %rdx
	jbe	.L79
	movl	24(%rsp), %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L79
	movslq	%edx, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_padn
	movq	%rax, %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r12
	jne	.L79
	testl	%ebx, %ebx
	movl	%ebx, %eax
	js	.L79
	addl	%r12d, %eax
	jno	.L79
	.p2align 4,,10
	.p2align 3
.L158:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L83:
	movl	24(%rsp), %eax
	subl	%r12d, %eax
	testl	%eax, %eax
	jle	.L88
	movslq	%eax, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L159
	testl	%ebx, %ebx
	jns	.L164
	.p2align 4,,10
	.p2align 3
.L125:
	movl	%ebx, %eax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	__wcsrtombs
	movq	%rax, %r12
	jmp	.L82
.L164:
	addl	%r12d, %ebx
	jo	.L158
.L88:
	testl	%ebx, %ebx
	jns	.L87
	movl	%ebx, %eax
	jmp	.L79
.L163:
	call	outstring_func.part.1
	.size	outstring_converted_wide_string, .-outstring_converted_wide_string
	.section	.rodata.str1.1
.LC3:
	.string	"(nil)"
.LC4:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(mode_flags & PRINTF_FORTIFY) != 0"
	.align 8
.LC6:
	.string	"*** invalid %N$ use detected ***\n"
	.align 8
.LC7:
	.string	"*** %n in writable segment detected ***\n"
	.text
	.p2align 4,,15
	.type	printf_positional, @function
printf_positional:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-2128(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2296, %rsp
	movq	%rax, -2192(%rbp)
	addq	$16, %rax
	movq	%rax, -2128(%rbp)
	leaq	-1088(%rbp), %rax
	movq	%r8, -2200(%rbp)
	movq	%rdi, -2216(%rbp)
	movq	%rsi, -2312(%rbp)
	movq	%rax, -2280(%rbp)
	addq	$16, %rax
	cmpq	$-1, 48(%rbp)
	movl	%edx, -2304(%rbp)
	movl	%r9d, -2208(%rbp)
	movq	24(%rbp), %r8
	movq	$1024, -2120(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$1024, -1080(%rbp)
	movq	$0, -2160(%rbp)
	je	.L783
.L166:
	cmpb	$0, (%r8)
	je	.L784
	movq	-2192(%rbp), %rax
	movl	$14, %r12d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%r12, -2168(%rbp)
	movq	%r8, %r14
	leaq	16(%rax), %r13
	leaq	-2160(%rbp), %rax
	movq	%rax, -2176(%rbp)
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	(%r15,%r15,8), %rax
	movq	-2176(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %r15
	leaq	0(%r13,%rax,8), %r12
	movq	%r12, %rdx
	call	__parse_one_specmb
	movq	32(%r12), %r14
	addq	%rax, %rbx
	cmpb	$0, (%r14)
	je	.L785
	cmpq	%r15, -2168(%rbp)
	jne	.L167
	movq	-2192(%rbp), %rdi
	call	__libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L762
	movabsq	$-2049638230412172401, %rax
	movq	-2128(%rbp), %r13
	mulq	-2120(%rbp)
	shrq	$6, %rdx
	movq	%rdx, -2168(%rbp)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L454:
	xorl	%r12d, %r12d
.L755:
	movb	$32, -2281(%rbp)
.L249:
	xorl	%ecx, %ecx
	cmpb	$88, -2200(%rbp)
	movq	-2296(%rbp), %r15
	movl	-2232(%rbp), %edx
	movq	%r11, %rdi
	movl	%r10d, -2264(%rbp)
	movq	%r11, -2256(%rbp)
	movq	%r15, %rsi
	sete	%cl
	call	_itoa_word
	cmpq	$0, 48(%rbp)
	movq	%rax, %r9
	movq	-2256(%rbp), %r11
	movl	-2264(%rbp), %r10d
	je	.L252
	movl	-2240(%rbp), %eax
	testl	%eax, %eax
	je	.L252
	movq	56(%rbp), %r8
	movq	48(%rbp), %rcx
	movq	%r9, %rsi
	movq	32(%rbp), %rdi
	movq	%r15, %rdx
	movl	%r10d, -2240(%rbp)
	call	group_number
	movq	-2256(%rbp), %r11
	movl	-2240(%rbp), %r10d
	movq	%rax, %r9
.L252:
	cmpl	$10, -2232(%rbp)
	jne	.L253
	cmpb	$0, -2248(%rbp)
	je	.L253
	movq	-2296(%rbp), %rsi
	movq	%r9, %rdi
	movq	%r11, -2248(%rbp)
	movl	%r10d, -2240(%rbp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movq	-2248(%rbp), %r11
	movl	-2240(%rbp), %r10d
	movq	%rax, %r9
.L253:
	movq	-2296(%rbp), %r15
	subq	%r9, %r15
	cmpq	%r12, %r15
	jl	.L786
	testq	%r11, %r11
	je	.L787
	cmpl	$8, -2232(%rbp)
	jne	.L460
	movl	-2228(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L460
	movq	-2296(%rbp), %r15
	leaq	-1(%r9), %rax
	movl	-2208(%rbp), %esi
	movq	%r12, %rdx
	movl	$0, %ecx
	movb	$48, -1(%r9)
	movq	%rax, %r9
	subq	%rax, %r15
	subq	%r15, %rdx
	cmovs	%rcx, %rdx
	testl	%esi, %esi
	movslq	%edx, %r12
	je	.L788
	.p2align 4,,10
	.p2align 3
.L256:
	testl	%r10d, %r10d
	je	.L291
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L789
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	$45, (%rax)
.L299:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L762
	addl	$1, %eax
	subl	$1, %ebx
	movl	%eax, -2168(%rbp)
.L294:
	testq	%r11, %r11
	je	.L300
	cmpl	$16, -2232(%rbp)
	jne	.L300
	movl	-2228(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L300
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L790
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	$48, (%rax)
.L302:
	cmpl	$2147483647, -2168(%rbp)
	je	.L762
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L791
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movzbl	-2200(%rbp), %ecx
	movb	%cl, (%rax)
.L304:
	movl	-2168(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L762
	addl	$2, %eax
	subl	$2, %ebx
	movl	%eax, -2168(%rbp)
.L300:
	addl	%r15d, %edx
	subl	%edx, %ebx
	testl	%r12d, %r12d
	jle	.L305
	movq	%r12, %rdx
	movl	$48, %esi
	movq	%r13, %rdi
	movq	%r9, -2200(%rbp)
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movl	%r12d, -2168(%rbp)
	jo	.L367
	testl	%r12d, %r12d
	movq	-2200(%rbp), %r9
	js	.L213
.L309:
	movq	216(%r13), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	%rax, %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	cmpq	%rdx, %r8
	jbe	.L792
.L310:
	movq	%r8, -2200(%rbp)
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	cmpq	%r15, %rax
	movq	-2200(%rbp), %r8
	jne	.L762
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%r15, %rax
	js	.L312
	cmpq	%r15, %rax
	jb	.L312
.L311:
	movslq	%eax, %rcx
	movl	%eax, %r15d
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	jle	.L290
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r8, -2200(%rbp)
	call	_IO_padn
	cmpq	%rax, %rbx
	jne	.L762
	addl	%r15d, %ebx
	movl	%ebx, %eax
	movl	%ebx, -2168(%rbp)
	jno	.L761
	.p2align 4,,10
	.p2align 3
.L367:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L762:
	movl	$-1, -2168(%rbp)
.L213:
	movq	-2280(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L438
	call	free@PLT
.L438:
	movq	-2192(%rbp), %rax
	movq	-2128(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L165
	call	free@PLT
.L165:
	movl	-2168(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	cmpq	%rbx, -2160(%rbp)
	cmovnb	-2160(%rbp), %rbx
.L168:
	movq	-2280(%rbp), %rdi
	movl	$24, %edx
	movq	%rbx, %rsi
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L762
	movq	-1088(%rbp), %rax
	movq	%rbx, %r12
	leaq	0(,%rbx,4), %rdx
	salq	$4, %r12
	addq	%rax, %r12
	movq	%rax, -2184(%rbp)
	movl	64(%rbp), %eax
	movq	%r12, -2168(%rbp)
	addq	%rdx, %r12
	movq	%r12, %rdi
	andl	$2, %eax
	setne	%sil
	movl	%eax, -2300(%rbp)
	movzbl	%sil, %esi
	negl	%esi
	call	memset
	testq	%r15, %r15
	je	.L172
	leaq	(%r15,%r15,8), %rax
	movq	%r13, %r14
	movq	%r13, -2176(%rbp)
	movq	%rbx, -2224(%rbp)
	movq	-2168(%rbp), %rbx
	leaq	0(%r13,%rax,8), %r8
	movq	%r8, %r13
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L794:
	movslq	48(%r14), %rax
	movl	52(%r14), %edx
	movl	%edx, (%r12,%rax,4)
	movslq	48(%r14), %rax
	movl	64(%r14), %edx
	movl	%edx, (%rbx,%rax,4)
.L176:
	addq	$72, %r14
	cmpq	%r14, %r13
	je	.L793
.L178:
	movslq	44(%r14), %rax
	cmpl	$-1, %eax
	je	.L173
	movl	$0, (%r12,%rax,4)
.L173:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L174
	movl	$0, (%r12,%rax,4)
.L174:
	movq	56(%r14), %rsi
	testq	%rsi, %rsi
	je	.L176
	cmpq	$1, %rsi
	je	.L794
	movslq	48(%r14), %rdx
	movslq	8(%r14), %rax
	movq	%r14, %rdi
	movq	__printf_arginfo_table(%rip), %r10
	addq	$72, %r14
	salq	$2, %rdx
	leaq	(%rbx,%rdx), %rcx
	addq	%r12, %rdx
	call	*(%r10,%rax,8)
	cmpq	%r14, %r13
	jne	.L178
.L793:
	movq	-2224(%rbp), %rbx
	movq	-2176(%rbp), %r13
	testq	%rbx, %rbx
	je	.L179
.L446:
	movl	64(%rbp), %edx
	movq	-2184(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	-2200(%rbp), %r8
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L212:
	movslq	(%r12,%r14,4), %rax
	cmpl	$5, %eax
	jg	.L181
	cmpl	$3, %eax
	jge	.L182
	cmpl	$1, %eax
	jg	.L183
	testl	%eax, %eax
	jns	.L184
	cmpl	$-1, %eax
	jne	.L180
	movl	-2300(%rbp), %eax
	testl	%eax, %eax
	je	.L795
	leaq	.LC6(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$256, %eax
	je	.L182
	jle	.L796
	cmpl	$512, %eax
	je	.L182
	cmpl	$1024, %eax
	je	.L184
	cmpl	$263, %eax
	jne	.L180
	testl	%edx, %edx
	jne	.L797
	testb	$8, 64(%rbp)
	je	.L202
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L203
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L204:
	movq	%r14, %rax
	movdqa	(%rsi), %xmm0
	addq	$1, %r14
	salq	$4, %rax
	cmpq	%rbx, %r14
	movaps	%xmm0, (%rcx,%rax)
	jb	.L212
	.p2align 4,,10
	.p2align 3
.L179:
	movslq	16(%rbp), %rax
	cmpq	%r15, %rax
	movq	%rax, -2176(%rbp)
	jnb	.L453
	leaq	(%rax,%rax,8), %rax
	movq	32(%rbp), %rcx
	movq	%r15, -2272(%rbp)
	leaq	0(%r13,%rax,8), %r14
	movl	-2208(%rbp), %eax
	movq	-2216(%rbp), %r13
	leaq	1000(%rcx), %rdi
	movl	%eax, -2168(%rbp)
	leaq	-2144(%rbp), %rax
	movq	%rdi, -2296(%rbp)
	movq	%rax, -2320(%rbp)
	addq	$12, %rax
	movq	%rax, -2328(%rbp)
	.p2align 4,,10
	.p2align 3
.L437:
	movzbl	12(%r14), %eax
	movzbl	13(%r14), %ebx
	movl	8(%r14), %r9d
	movslq	(%r14), %r12
	movl	%eax, %edx
	movl	%ebx, %r11d
	movl	%eax, %r15d
	shrb	$3, %dl
	movl	%eax, %r8d
	shrb	%r15b
	movl	%edx, %edi
	movl	%eax, %edx
	shrb	%r11b
	andl	$1, %edi
	shrb	$4, %dl
	shrb	$2, %r8b
	movl	%edi, -2228(%rbp)
	movl	%edx, %edi
	movl	%eax, %edx
	andl	$1, %edi
	shrb	$5, %dl
	shrb	$3, %bl
	movl	%edi, -2224(%rbp)
	movl	%edx, %edi
	movl	%eax, %edx
	shrb	$6, %dl
	andl	$1, %edi
	andl	$1, %ebx
	movl	%edx, %ecx
	movl	%eax, %edx
	movl	%edi, -2208(%rbp)
	shrb	$7, %dl
	movl	%eax, %edi
	andl	$1, %ecx
	movzbl	%dl, %esi
	movslq	44(%r14), %rdx
	andl	$1, %edi
	movb	%dil, -2232(%rbp)
	movl	16(%r14), %edi
	andl	$1, %r15d
	andl	$1, %r11d
	andl	$1, %r8d
	movl	%ecx, -2216(%rbp)
	movl	%esi, -2240(%rbp)
	movb	%bl, -2248(%rbp)
	cmpl	$-1, %edx
	movl	%edi, -2256(%rbp)
	movb	%r9b, -2200(%rbp)
	je	.L798
	movq	-2184(%rbp), %rcx
	salq	$4, %rdx
	movl	(%rcx,%rdx), %ebx
	testl	%ebx, %ebx
	js	.L216
	movl	%ebx, 4(%r14)
.L215:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L218
	movq	-2184(%rbp), %rsi
	salq	$4, %rax
	movslq	(%rsi,%rax), %r12
	testl	%r12d, %r12d
	js	.L219
	movl	%r12d, (%r14)
.L218:
	movq	__printf_function_table(%rip), %rax
	testq	%rax, %rax
	je	.L221
	movsbq	-2200(%rbp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	%rcx, -2264(%rbp)
	testq	%rax, %rax
	je	.L221
	movq	56(%r14), %rdi
	leaq	30(,%rdi,8), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	testq	%rdi, %rdi
	je	.L222
	movb	%r8b, -2281(%rbp)
	movl	48(%r14), %r10d
	xorl	%ecx, %ecx
	movq	-2184(%rbp), %r8
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L223:
	leal	(%r10,%rcx), %eax
	salq	$4, %rax
	addq	%r8, %rax
	movq	%rax, (%rdx,%rsi,8)
	leal	1(%rcx), %esi
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	ja	.L223
	movq	__printf_function_table(%rip), %rax
	movq	-2264(%rbp), %rdi
	movzbl	-2281(%rbp), %r8d
	movq	(%rax,%rdi,8), %rax
.L222:
	movl	%r9d, -2288(%rbp)
	movb	%r8b, -2281(%rbp)
	movq	%r14, %rsi
	movb	%r11b, -2264(%rbp)
	movq	%r13, %rdi
	call	*%rax
	cmpl	$-2, %eax
	movzbl	-2264(%rbp), %r11d
	movzbl	-2281(%rbp), %r8d
	movl	-2288(%rbp), %r9d
	je	.L221
	testl	%eax, %eax
	js	.L762
	movl	-2168(%rbp), %esi
	testl	%esi, %esi
	js	.L213
	cltq
	addl	%esi, %eax
	movl	%eax, -2168(%rbp)
	jo	.L367
.L352:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
.L748:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	subq	%r12, %r8
.L290:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	subq	%rsi, %rbx
.L445:
	movq	216(%r13), %r15
	movq	%r15, %rax
	subq	%r12, %rax
	cmpq	%rax, %r8
	jbe	.L799
.L433:
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	*56(%r15)
	cmpq	%rbx, %rax
	jne	.L762
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%rbx, %rax
	js	.L435
	cmpq	%rbx, %rax
	jb	.L435
.L434:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	addq	$1, -2176(%rbp)
	addq	$72, %r14
	movq	-2176(%rbp), %rax
	cmpq	-2272(%rbp), %rax
	jb	.L437
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L182:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L205
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L206:
	movq	(%rsi), %rsi
	movq	%r14, %rax
	salq	$4, %rax
	movq	%rsi, (%rcx,%rax)
.L192:
	addq	$1, %r14
	cmpq	%rbx, %r14
	jb	.L212
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r14, %rsi
	salq	$4, %rsi
	testb	$8, %ah
	leaq	(%rcx,%rsi), %rdi
	je	.L207
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L208
	movl	%eax, %edi
	addq	16(%r8), %rdi
	addl	$8, %eax
	movl	%eax, (%r8)
.L209:
	movq	(%rdi), %rax
	movq	%rax, (%rcx,%rsi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L184:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L193
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L194:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L221:
	leal	-32(%r9), %eax
	cmpb	$90, %al
	jbe	.L800
.L228:
	movq	56(%r14), %rsi
	leaq	30(,%rsi,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	testq	%rsi, %rsi
	je	.L392
	movl	48(%r14), %r8d
	movq	-2184(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L393:
	leal	(%r8,%rdx), %eax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, (%rdi,%rcx,8)
	leal	1(%rdx), %ecx
	cmpq	%rcx, %rsi
	movq	%rcx, %rdx
	ja	.L393
.L392:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L801
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$37, (%rax)
.L395:
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L459
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L802
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$35, (%rax)
.L758:
	movzbl	12(%r14), %eax
	movl	$2, %ebx
.L396:
	testb	%al, %al
	js	.L803
.L399:
	testb	$64, %al
	je	.L402
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L804
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$43, (%rax)
.L407:
	movzbl	12(%r14), %eax
	addl	$1, %ebx
.L405:
	testb	$32, %al
	je	.L408
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L805
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$45, (%rax)
.L410:
	addl	$1, %ebx
.L408:
	cmpl	$48, 16(%r14)
	je	.L806
.L411:
	testb	$8, 13(%r14)
	je	.L414
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L807
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$73, (%rax)
.L416:
	addl	$1, %ebx
.L414:
	movl	4(%r14), %eax
	testl	%eax, %eax
	jne	.L808
.L417:
	cmpl	$-1, (%r14)
	je	.L422
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L809
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$46, (%rax)
.L424:
	cmpl	$2147483647, %ebx
	je	.L762
	movq	-2328(%rbp), %r15
	movslq	(%r14), %rdi
	xorl	%ecx, %ecx
	movl	$10, %edx
	addl	$1, %ebx
	movq	%r15, %rsi
	call	_itoa_word
	cmpq	%r15, %rax
	movq	%rax, %r12
	jb	.L427
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	%dl, (%rax)
.L426:
	cmpl	$2147483647, %ebx
	je	.L762
	addl	$1, %ebx
	cmpq	%r15, %r12
	je	.L422
.L427:
	addq	$1, %r12
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	movzbl	-1(%r12), %edx
	jb	.L425
	movzbl	%dl, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L426
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L800:
	movsbl	%r9b, %eax
	leaq	jump_table(%rip), %rdi
	leaq	step4_jumps.12968(%rip), %rcx
	subl	$32, %eax
	movzbl	%r15b, %r15d
	movzbl	%r11b, %edx
	cltq
	movzbl	%r8b, %r10d
	movzbl	(%rdi,%rax), %eax
	jmp	*(%rcx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L345:
	movq	$0, -2152(%rbp)
	movslq	48(%r14), %rax
	leaq	-2152(%rbp), %rdx
	movq	-2184(%rbp), %rdi
	salq	$4, %rax
	movl	(%rdi,%rax), %esi
	movq	-2320(%rbp), %rdi
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L762
	subl	%eax, %ebx
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	je	.L810
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L362
.L361:
	movq	216(%r13), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	%rax, %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	cmpq	%rdx, %r8
	jbe	.L811
.L363:
	movq	%r8, -2200(%rbp)
	movq	%r15, %rdx
	movq	-2320(%rbp), %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	cmpq	%rax, %r15
	movq	-2200(%rbp), %r8
	jne	.L762
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%r15, %rax
	js	.L365
	cmpq	%r15, %rax
	jb	.L365
.L364:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	jle	.L290
.L781:
	movl	-2208(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L290
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r8, -2200(%rbp)
	call	_IO_padn
	cmpq	%rax, %rbx
	jne	.L762
	addl	-2168(%rbp), %ebx
	movl	%ebx, %eax
	movl	%ebx, -2168(%rbp)
	jo	.L367
.L761:
	testl	%eax, %eax
	movq	-2200(%rbp), %r8
	jns	.L290
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L798:
	movl	4(%r14), %ebx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L216:
	negl	%ebx
	orl	$32, %eax
	movl	$1, -2208(%rbp)
	movl	%ebx, 4(%r14)
	movb	%al, 12(%r14)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$-1, (%r14)
	movq	$-1, %r12
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L796:
	cmpl	$7, %eax
	jg	.L180
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L197
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L198:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L783:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	72(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rax, 48(%rbp)
	movzbl	(%rax), %eax
	movq	%rcx, 56(%rbp)
	testb	%al, %al
	je	.L451
	cmpb	$127, %al
	jne	.L166
.L451:
	movq	$0, 48(%rbp)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L799:
	movq	%rsi, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %rsi
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L207:
	movq	__printf_va_arg_table(%rip), %r9
	testq	%r9, %r9
	jne	.L812
.L210:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	__PRETTY_FUNCTION__.12964(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$1888, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%r8), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 8(%r8)
	fldt	(%rax)
	movq	%r14, %rax
	salq	$4, %rax
	fstpt	(%rcx,%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L810:
	testl	%ebx, %ebx
	jle	.L357
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movl	%r12d, -2168(%rbp)
	jo	.L367
.L357:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	jns	.L361
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L233:
	movslq	48(%r14), %rax
	salq	$4, %rax
	testl	%r10d, %r10d
	je	.L234
	movq	-2184(%rbp), %rcx
	movq	(%rcx,%rax), %rax
.L235:
	testq	%rax, %rax
	js	.L813
	movq	%rax, %r11
	movl	$10, -2232(%rbp)
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L239:
	testl	%r12d, %r12d
	js	.L247
.L820:
	jne	.L755
	testq	%r11, %r11
	jne	.L454
	cmpl	$8, -2232(%rbp)
	jne	.L455
	movl	-2228(%rbp), %eax
	testl	%eax, %eax
	je	.L455
	movq	32(%rbp), %rax
	movq	-2296(%rbp), %r15
	leaq	999(%rax), %r9
	movb	$48, 999(%rax)
	subq	%r9, %r15
	movq	%r15, %rdx
	negq	%rdx
	testq	%rdx, %rdx
	cmovs	%r11, %rdx
	testq	%r15, %r15
	movslq	%edx, %r12
	jns	.L456
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	jne	.L256
	subl	%r15d, %ebx
	movb	$32, -2281(%rbp)
	subl	%edx, %ebx
	jmp	.L262
.L229:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L814
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$37, (%rax)
.L231:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L762
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
.L232:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	movl	-2168(%rbp), %edi
	subq	%rsi, %rbx
	testl	%edi, %edi
	js	.L362
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	subq	%r12, %r8
	jmp	.L445
.L371:
.L343:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rcx
	salq	$4, %rax
	movq	(%rcx,%rax), %r11
	testq	%r11, %r11
	je	.L815
.L372:
	cmpb	$83, %r9b
	je	.L335
	andl	$1, %r10d
	jne	.L335
	cmpl	$-1, %r12d
	je	.L375
.L450:
	movq	%r11, %rdi
	movslq	%r12d, %rsi
	movq	%r11, -2200(%rbp)
	call	__strnlen
	movq	-2200(%rbp), %r11
	movq	%rax, %r15
	subl	%eax, %ebx
.L374:
	testl	%ebx, %ebx
	js	.L816
	movl	-2208(%rbp), %eax
	testl	%eax, %eax
	jne	.L381
	testl	%ebx, %ebx
	je	.L382
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r11, -2200(%rbp)
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movq	-2200(%rbp), %r11
	movl	%r12d, -2168(%rbp)
	jo	.L367
.L382:
	movl	-2168(%rbp), %r12d
	testl	%r12d, %r12d
	jns	.L385
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L344:
	testl	%r10d, %r10d
	jne	.L345
	movl	-2208(%rbp), %esi
	subl	$1, %ebx
	testl	%esi, %esi
	je	.L817
.L346:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rcx
	salq	$4, %rax
	movl	(%rcx,%rax), %edx
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L818
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	%dl, (%rax)
.L351:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L762
	movl	-2208(%rbp), %edx
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
	testl	%edx, %edx
	je	.L232
	testl	%ebx, %ebx
	jle	.L352
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	_IO_padn
	cmpq	%rax, %rbx
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %ebx
	movl	%ebx, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L342:
	movq	32(%rbp), %rsi
	movl	40(%rbp), %edi
	movl	$1000, %edx
	movl	%r9d, -2200(%rbp)
	call	__strerror_r
	movq	%rax, %r11
	xorl	%r10d, %r10d
	movl	-2200(%rbp), %r9d
	testq	%r11, %r11
	jne	.L372
.L815:
	cmpl	$-1, %r12d
	je	.L373
	cmpl	$5, %r12d
	jg	.L373
	xorl	%r15d, %r15d
	leaq	.LC4(%rip), %r11
	jmp	.L374
.L316:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2144(%rbp)
	jne	.L819
.L317:
	testb	$8, 64(%rbp)
	je	.L318
	movzbl	-2232(%rbp), %edx
	movzbl	13(%r14), %eax
	sall	$4, %edx
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L319:
	movq	-2320(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__printf_fp
	testl	%eax, %eax
	js	.L762
	movl	-2168(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L213
	cltq
	addl	%ecx, %eax
	movl	%eax, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L243:
	movl	$16, -2232(%rbp)
.L241:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testl	%r10d, %r10d
	je	.L244
	xorl	%r10d, %r10d
	testl	%r12d, %r12d
	movq	(%rax), %r11
	movl	$0, -2216(%rbp)
	movl	$0, -2224(%rbp)
	jns	.L820
.L247:
	movzbl	-2256(%rbp), %eax
	movl	$1, %r12d
	movb	%al, -2281(%rbp)
	jmp	.L249
.L332:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rcx
	salq	$4, %rax
	movq	(%rcx,%rax), %rax
	testq	%rax, %rax
	je	.L333
	movq	%rax, %r11
	movb	$120, -2200(%rbp)
	movl	$0, -2240(%rbp)
	movl	$1, -2228(%rbp)
	movl	$16, -2232(%rbp)
	xorl	%r10d, %r10d
	jmp	.L239
.L242:
	movl	$8, -2232(%rbp)
	jmp	.L241
.L240:
	movl	$10, -2232(%rbp)
	jmp	.L241
.L336:
	movl	-2300(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L337
	movl	-2304(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L821
.L338:
	movl	-2304(%rbp), %edi
	testl	%edi, %edi
	js	.L822
.L337:
	movslq	48(%r14), %rax
	movq	-2184(%rbp), %rdi
	salq	$4, %rax
	testl	%r10d, %r10d
	movq	(%rdi,%rax), %rax
	je	.L339
	movslq	-2168(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L232
.L324:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2184(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2144(%rbp)
	jne	.L823
.L325:
	testb	$8, 64(%rbp)
	je	.L326
	movzbl	-2232(%rbp), %eax
	movzbl	13(%r14), %edx
	sall	$4, %eax
	andl	$-17, %edx
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L327:
	movq	-2320(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__printf_fphex
	testl	%eax, %eax
	js	.L762
	movl	-2168(%rbp), %edi
	testl	%edi, %edi
	js	.L213
	cltq
	addl	%edi, %eax
	movl	%eax, -2168(%rbp)
	jno	.L352
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L786:
	movq	%r12, %rdx
	movl	$0, %eax
	subq	%r15, %rdx
	cmovs	%rax, %rdx
	movl	-2208(%rbp), %eax
	movslq	%edx, %r12
	testl	%eax, %eax
	jne	.L256
	subl	%r15d, %ebx
	subl	%edx, %ebx
	testq	%r11, %r11
	je	.L262
.L447:
	cmpl	$16, -2232(%rbp)
	jne	.L262
	movl	-2228(%rbp), %ecx
	leal	-2(%rbx), %eax
	testl	%ecx, %ecx
	cmovne	%eax, %ebx
.L262:
	movl	-2216(%rbp), %eax
	orl	-2224(%rbp), %eax
	orl	%r10d, %eax
	je	.L263
	subl	$1, %ebx
	cmpb	$32, -2281(%rbp)
	je	.L449
.L264:
	testl	%r10d, %r10d
	je	.L269
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L824
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$45, (%rax)
.L277:
	movl	-2168(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L762
	addl	$1, %eax
	movl	%eax, -2168(%rbp)
.L272:
	testq	%r11, %r11
	je	.L278
	cmpl	$16, -2232(%rbp)
	jne	.L278
	movl	-2228(%rbp), %eax
	testl	%eax, %eax
	je	.L278
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L825
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$48, (%rax)
.L280:
	cmpl	$2147483647, -2168(%rbp)
	je	.L762
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L826
	movzbl	-2200(%rbp), %edi
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	%dil, (%rax)
.L282:
	movl	-2168(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L762
	addl	$2, %eax
	movl	%eax, -2168(%rbp)
.L278:
	addl	%ebx, %r12d
	testl	%r12d, %r12d
	jle	.L283
	movslq	%r12d, %r12
	movl	$48, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r9, -2200(%rbp)
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	movl	%eax, %edx
	movq	-2200(%rbp), %r9
	addl	%r12d, %edx
	movl	%edx, -2168(%rbp)
	jo	.L367
.L283:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	movq	216(%r13), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	%rbx, %rax
	subq	%r12, %r8
	subq	%r12, %rax
	cmpq	%rax, %r8
	jbe	.L827
.L286:
	movq	%r8, -2200(%rbp)
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	*56(%rbx)
	cmpq	%rax, %r15
	jne	.L762
	movslq	-2168(%rbp), %rdx
	movabsq	$-9223372036854775808, %rax
	addq	%r15, %rdx
	addq	%rdx, %rax
	movslq	%edx, %rcx
	movl	%edx, -2168(%rbp)
	cmpq	%r15, %rax
	setb	%al
	cmpq	%rcx, %rdx
	movl	$1, %ecx
	movzbl	%al, %eax
	cmovne	%ecx, %eax
	testl	%eax, %eax
	jne	.L367
	testl	%edx, %edx
	movq	-2200(%rbp), %r8
	jns	.L290
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L244:
	testl	%edx, %edx
	movl	(%rax), %eax
	je	.L245
	movzbl	%al, %r11d
	movl	$0, -2216(%rbp)
	movl	$0, -2224(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L263:
	cmpb	$32, -2281(%rbp)
	je	.L449
.L269:
	movl	-2216(%rbp), %eax
	testl	%eax, %eax
	je	.L273
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L828
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$43, (%rax)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L291:
	movl	-2216(%rbp), %eax
	testl	%eax, %eax
	je	.L295
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L829
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	$43, (%rax)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	movl	-2168(%rbp), %r10d
	testl	%r10d, %r10d
	jns	.L309
.L362:
	call	outstring_func.part.1
	.p2align 4,,10
	.p2align 3
.L245:
	testl	%r15d, %r15d
	jne	.L246
	movl	%eax, %r11d
	movl	$0, -2216(%rbp)
	movl	$0, -2224(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%r12, %rdx
	subq	%r15, %rdx
	cmovs	%r11, %rdx
	movslq	%edx, %r12
.L250:
	movl	-2208(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L256
	subl	%r15d, %ebx
	subl	%edx, %ebx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L381:
	movl	-2168(%rbp), %r10d
	testl	%r10d, %r10d
	js	.L362
.L385:
	movq	216(%r13), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	%rax, %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	cmpq	%rdx, %r8
	jbe	.L830
.L386:
	movq	%r8, -2200(%rbp)
	movq	%r15, %rdx
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	*56(%rax)
	cmpq	%rax, %r15
	movq	-2200(%rbp), %r8
	jne	.L762
	movslq	-2168(%rbp), %rax
	xorl	%edx, %edx
	addq	%r15, %rax
	js	.L388
	cmpq	%r15, %rax
	jb	.L388
.L387:
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L367
	testl	%eax, %eax
	js	.L213
	testl	%ebx, %ebx
	jne	.L781
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L172:
	testq	%rbx, %rbx
	jne	.L446
.L453:
	movl	-2208(%rbp), %eax
	movl	%eax, -2168(%rbp)
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L295:
	movl	-2224(%rbp), %eax
	testl	%eax, %eax
	je	.L294
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L831
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	$32, (%rax)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L460:
	movl	-2208(%rbp), %edi
	movq	%r12, %rdx
	movl	$0, %eax
	subq	%r15, %rdx
	cmovs	%rax, %rdx
	testl	%edi, %edi
	movslq	%edx, %r12
	jne	.L256
	subl	%r15d, %ebx
	subl	%edx, %ebx
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L273:
	movl	-2224(%rbp), %eax
	testl	%eax, %eax
	je	.L272
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L832
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$32, (%rax)
	jmp	.L277
.L422:
	movl	8(%r14), %eax
	testl	%eax, %eax
	je	.L428
	movq	40(%r13), %rdx
	cmpq	48(%r13), %rdx
	jnb	.L833
	leaq	1(%rdx), %rcx
	movq	%rcx, 40(%r13)
	movb	%al, (%rdx)
.L430:
	cmpl	$2147483647, %ebx
	je	.L762
	addl	$1, %ebx
.L428:
	movl	-2168(%rbp), %edx
	testl	%edx, %edx
	js	.L213
	movslq	%ebx, %rbx
	addl	-2168(%rbp), %ebx
	movl	%ebx, -2168(%rbp)
	jo	.L367
	testl	%ebx, %ebx
	jns	.L748
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L333:
	cmpl	$5, %r12d
	movl	$5, %eax
	leaq	.LC3(%rip), %r11
	cmovl	%eax, %r12d
	cmpb	$83, %r9b
	jne	.L450
.L335:
	movl	-2168(%rbp), %r9d
	movl	-2208(%rbp), %r8d
	movl	%ebx, %ecx
	movl	%r12d, %edx
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, -2168(%rbp)
	jns	.L748
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L784:
	movq	-2192(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	leaq	16(%rax), %r13
	jmp	.L168
.L234:
	movq	-2184(%rbp), %rdi
	testl	%edx, %edx
	movl	(%rdi,%rax), %eax
	je	.L236
	movsbq	%al, %rax
	jmp	.L235
.L326:
	andb	$-17, 13(%r14)
	jmp	.L327
.L339:
	testl	%edx, %edx
	jne	.L834
	testl	%r15d, %r15d
	jne	.L341
	movl	-2168(%rbp), %edi
	movl	%edi, (%rax)
	jmp	.L232
.L318:
	andb	$-17, 13(%r14)
	jmp	.L319
.L459:
	movl	$1, %ebx
	jmp	.L396
.L402:
	testb	$16, %al
	je	.L405
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L835
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$32, (%rax)
	jmp	.L407
.L449:
	testl	%ebx, %ebx
	jle	.L265
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r11, -2248(%rbp)
	movq	%r9, -2240(%rbp)
	movl	%r10d, -2208(%rbp)
	call	_IO_padn
	cmpq	%rax, %rbx
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %ebx
	movl	-2208(%rbp), %r10d
	movq	-2240(%rbp), %r9
	movl	%ebx, -2168(%rbp)
	movq	-2248(%rbp), %r11
	jo	.L367
.L265:
	movl	-2168(%rbp), %edx
	testl	%edx, %edx
	js	.L213
	xorl	%ebx, %ebx
	jmp	.L264
.L455:
	movq	-2296(%rbp), %r9
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	movb	$32, -2281(%rbp)
	jmp	.L250
.L816:
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L362
	movq	216(%r13), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	%rbx, %rdx
	subq	%r12, %r8
	subq	%r12, %rdx
	cmpq	%rdx, %r8
	jbe	.L836
.L377:
	movq	%r8, -2200(%rbp)
	movq	%r15, %rdx
	movq	%r11, %rsi
	movq	%r13, %rdi
	call	*56(%rbx)
	cmpq	%rax, %r15
	jne	.L762
	movslq	-2168(%rbp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%r15, %rax
	addq	%rax, %rdx
	movslq	%eax, %rcx
	movl	%eax, -2168(%rbp)
	cmpq	%r15, %rdx
	setb	%dl
	cmpq	%rcx, %rax
	movl	$1, %ecx
	movzbl	%dl, %edx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	je	.L761
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L246:
	movzwl	%ax, %r11d
	movl	$0, -2216(%rbp)
	movl	$0, -2224(%rbp)
	jmp	.L239
.L375:
	movq	%r11, %rdi
	movq	%r11, -2200(%rbp)
	call	strlen
	movq	-2200(%rbp), %r11
	movq	%rax, %r15
	subl	%eax, %ebx
	jmp	.L374
.L803:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L837
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$39, (%rax)
.L401:
	addl	$1, %ebx
	movzbl	12(%r14), %eax
	jmp	.L399
.L813:
	negq	%rax
	movl	$10, -2232(%rbp)
	movl	$1, %r10d
	movq	%rax, %r11
	jmp	.L239
.L373:
	subl	$6, %ebx
	movl	$6, %r15d
	leaq	null(%rip), %r11
	jmp	.L374
.L806:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L838
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$48, (%rax)
.L413:
	addl	$1, %ebx
	jmp	.L411
.L788:
	subl	%r15d, %ebx
	subl	%edx, %ebx
	jmp	.L262
.L834:
	movzbl	-2168(%rbp), %edi
	movb	%dil, (%rax)
	jmp	.L232
.L236:
	movslq	%eax, %rdx
	testl	%r15d, %r15d
	movswq	%ax, %rax
	cmove	%rdx, %rax
	jmp	.L235
.L456:
	movb	$32, -2281(%rbp)
	jmp	.L250
.L821:
	movq	-2312(%rbp), %rbx
	movl	%edx, -2208(%rbp)
	movl	%r10d, -2200(%rbp)
	movq	%rbx, %rdi
	call	strlen
	leaq	1(%rax), %rsi
	movq	%rbx, %rdi
	call	__readonly_area
	movl	-2208(%rbp), %edx
	movl	%eax, -2304(%rbp)
	movl	-2200(%rbp), %r10d
	jmp	.L338
.L817:
	testl	%ebx, %ebx
	jle	.L347
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%r12, %rdx
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L762
	movl	-2168(%rbp), %eax
	testl	%eax, %eax
	js	.L213
	addl	%eax, %r12d
	movl	%r12d, -2168(%rbp)
	jo	.L367
.L347:
	movl	-2168(%rbp), %ecx
	testl	%ecx, %ecx
	jns	.L346
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L837:
	movl	$39, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L401
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L838:
	movl	$48, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L413
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L808:
	movq	-2328(%rbp), %r15
	xorl	%ecx, %ecx
	movslq	%eax, %rdi
	movl	$10, %edx
	movq	%r15, %rsi
	call	_itoa_word
	cmpq	%r15, %rax
	movq	%rax, %r12
	jb	.L421
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%r13)
	movb	%dl, (%rax)
.L420:
	cmpl	$2147483647, %ebx
	je	.L762
	addl	$1, %ebx
	cmpq	%r15, %r12
	je	.L417
.L421:
	addq	$1, %r12
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	movzbl	-1(%r12), %edx
	jb	.L418
	movzbl	%dl, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L420
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L797:
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L200
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L201:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	andl	$-257, (%r12,%r14,4)
	jmp	.L192
.L792:
	movq	%rax, -2216(%rbp)
	movq	%r8, -2208(%rbp)
	movq	%r9, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r9
	movq	-2208(%rbp), %r8
	movq	-2216(%rbp), %rax
	jmp	.L310
.L827:
	movq	%r8, -2208(%rbp)
	movq	%r9, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2208(%rbp), %r8
	movq	-2200(%rbp), %r9
	jmp	.L286
.L811:
	movq	%rax, -2216(%rbp)
	movq	%r8, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r8
	movq	-2216(%rbp), %rax
	jmp	.L363
.L830:
	movq	%rax, -2224(%rbp)
	movq	%r8, -2216(%rbp)
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2200(%rbp), %r11
	movq	-2216(%rbp), %r8
	movq	-2224(%rbp), %rax
	jmp	.L386
.L812:
	cmpq	$0, -64(%r9,%rax,8)
	je	.L210
	movq	-2168(%rbp), %rax
	movl	%edx, -2224(%rbp)
	movq	%rcx, -2200(%rbp)
	movq	%r8, -2176(%rbp)
	movslq	(%rax,%r14,4), %rax
	addq	$30, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movq	%rdi, (%rcx,%rsi)
	movslq	(%r12,%r14,4), %rax
	movq	%r8, %rsi
	call	*-64(%r9,%rax,8)
	movq	-2176(%rbp), %r8
	movq	-2200(%rbp), %rcx
	movl	-2224(%rbp), %edx
	jmp	.L192
.L341:
	movzwl	-2168(%rbp), %edi
	movw	%di, (%rax)
	jmp	.L232
.L789:
	movq	%rdx, -2224(%rbp)
	movq	%r11, -2216(%rbp)
	movl	$45, %esi
	movq	%r9, -2208(%rbp)
.L773:
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	movq	-2216(%rbp), %r11
	movq	-2224(%rbp), %rdx
	jne	.L299
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L824:
	movq	%r11, -2216(%rbp)
	movq	%r9, -2208(%rbp)
	movl	$45, %esi
.L769:
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	movq	-2216(%rbp), %r11
	jne	.L277
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L823:
	andb	$-2, 12(%r14)
	jmp	.L325
.L819:
	movl	$7, 52(%r14)
	andb	$-2, 12(%r14)
	jmp	.L317
.L814:
	movl	$37, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L231
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L801:
	movl	$37, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L395
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L825:
	movl	$48, %esi
	movq	%r13, %rdi
	movq	%r9, -2208(%rbp)
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	jne	.L280
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L826:
	movzbl	-2200(%rbp), %esi
	movq	%r13, %rdi
	movq	%r9, -2208(%rbp)
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	jne	.L282
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L829:
	movq	%rdx, -2224(%rbp)
	movq	%r11, -2216(%rbp)
	movl	$43, %esi
	movq	%r9, -2208(%rbp)
	jmp	.L773
.L828:
	movq	%r11, -2216(%rbp)
	movq	%r9, -2208(%rbp)
	movl	$43, %esi
	jmp	.L769
.L791:
	movzbl	-2200(%rbp), %esi
	movq	%r13, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%r9, -2208(%rbp)
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	movq	-2216(%rbp), %rdx
	jne	.L304
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L790:
	movl	$48, %esi
	movq	%r13, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%r9, -2208(%rbp)
	call	__overflow
	cmpl	$-1, %eax
	movq	-2208(%rbp), %r9
	movq	-2216(%rbp), %rdx
	jne	.L302
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L809:
	movl	$46, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L424
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L818:
	movzbl	%dl, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L351
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L807:
	movl	$73, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L416
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L805:
	movl	$45, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L410
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L804:
	movl	$43, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L407
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L802:
	movl	$35, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L758
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r8, -2208(%rbp)
	movq	%r11, -2200(%rbp)
	call	_IO_vtable_check
	movq	-2208(%rbp), %r8
	movq	-2200(%rbp), %r11
	jmp	.L377
.L832:
	movq	%r11, -2216(%rbp)
	movq	%r9, -2208(%rbp)
	movl	$32, %esi
	jmp	.L769
.L833:
	movzbl	%al, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L430
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L831:
	movq	%rdx, -2224(%rbp)
	movq	%r11, -2216(%rbp)
	movl	$32, %esi
	movq	%r9, -2208(%rbp)
	jmp	.L773
.L835:
	movl	$32, %esi
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L407
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$1, %edx
	jmp	.L434
.L388:
	movl	$1, %edx
	jmp	.L387
.L822:
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
.L365:
	movl	$1, %edx
	jmp	.L364
.L208:
	movq	8(%r8), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r8)
	jmp	.L209
.L203:
	movq	8(%r8), %rax
	leaq	15(%rax), %rsi
	andq	$-16, %rsi
	leaq	16(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L204
.L312:
	movl	$1, %edx
	jmp	.L311
.L200:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L201
.L205:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L206
.L183:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L190
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L191:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L192
.L193:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L194
.L197:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L198
.L190:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L191
	.size	printf_positional, .-printf_positional
	.p2align 4,,15
	.globl	__vfprintf_internal
	.hidden	__vfprintf_internal
	.type	__vfprintf_internal, @function
__vfprintf_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1272, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 88(%rsp)
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L841
	movl	$-1, 192(%rdi)
.L842:
	movl	(%rdi), %ebp
	testb	$8, %bpl
	jne	.L1395
	testq	%rsi, %rsi
	je	.L1396
	movl	%ebp, %eax
	movl	%ecx, 60(%rsp)
	movq	%rdx, %r15
	andl	$2, %eax
	movq	%rsi, (%rsp)
	movq	%rdi, %rbx
	movl	%eax, 48(%rsp)
	jne	.L1397
	movdqu	(%rdx), %xmm0
	movl	$37, %esi
	movq	(%rsp), %rdi
	movups	%xmm0, 200(%rsp)
	movq	16(%rdx), %rax
	movq	%rax, 216(%rsp)
	call	__strchrnul@PLT
	andl	$32768, %ebp
	movq	%rax, 64(%rsp)
	movq	%rax, 144(%rsp)
	je	.L1398
.L847:
	movq	216(%rbx), %rbp
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rsi
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rsi
	movq	64(%rsp), %r12
	subq	(%rsp), %r12
	movq	%rbp, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rsi, 24(%rsp)
	cmpq	%rax, %rsi
	jbe	.L1399
.L853:
	movq	%r12, %rdx
	movq	(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%rbp)
	cmpq	%rax, %r12
	jne	.L1121
	movslq	%r12d, %rdx
	movq	%r12, %rax
	movl	%r12d, %ebp
	shrq	$63, %rax
	cmpq	%rdx, %r12
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1063
	testl	%r12d, %r12d
	js	.L854
	movq	144(%rsp), %rax
	cmpb	$0, (%rax)
	je	.L854
	cmpq	$0, __printf_function_table(%rip)
	jne	.L1123
	cmpq	$0, __printf_modifier_table(%rip)
	jne	.L1123
	cmpq	$0, __printf_va_arg_table(%rip)
	movl	$0, 132(%rsp)
	jne	.L1124
	movl	$0, 92(%rsp)
	movq	$-1, 96(%rsp)
	movq	$0, 120(%rsp)
	.p2align 4,,10
	.p2align 3
.L1090:
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leaq	.L840(%rip), %rax
	leal	-32(%r12), %edx
	cmpb	$90, %dl
	ja	.L860
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step0_jumps.12746(%rip), %rax
	movq	(%rax,%rdx,8), %rax
.L860:
	movl	$0, 72(%rsp)
	movl	$0, 16(%rsp)
	xorl	%r14d, %r14d
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	movl	$0, 52(%rsp)
	movl	$0, 32(%rsp)
	movl	$0, 56(%rsp)
	movl	$0, 80(%rsp)
	leaq	step4_jumps.12782(%rip), %r11
	movb	$32, 40(%rsp)
	movl	$-1, 8(%rsp)
	leaq	step3b_jumps.12781(%rip), %rsi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1040:
	testl	%r9d, %r9d
	jne	.L1041
	movl	16(%rsp), %r12d
	movl	32(%rsp), %r11d
	subl	$1, %r12d
	testl	%r11d, %r11d
	je	.L1400
.L1042:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1045
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1046:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	movl	(%rdx), %edx
	jnb	.L1401
	leaq	1(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movb	%dl, (%rax)
.L1048:
	cmpl	$2147483647, %ebp
	je	.L1121
	addl	$1, %ebp
	testl	%r12d, %r12d
	jle	.L909
	movl	32(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L909
	movslq	%r12d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	_IO_padn
	cmpq	%rax, %r13
	jne	.L1121
	addl	%r12d, %ebp
	js	.L1063
	cmpl	%r12d, %ebp
	jnb	.L1359
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L854:
	testl	$32768, (%rbx)
	jne	.L1092
.L1410:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1092
	movq	$0, 8(%rdi)
#APP
# 1689 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1094
	subl	$1, (%rdi)
.L1092:
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L1402
.L839:
	addq	$1272, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	cmpl	$-1, %eax
	je	.L842
	movl	$-1, %ebp
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L1398:
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	je	.L848
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	leaq	224(%rsp), %rdi
	movq	%rbx, %rdx
	call	_pthread_cleanup_push_defer@PLT
	testl	$32768, (%rbx)
	movl	$1, 48(%rsp)
	jne	.L847
.L849:
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L850
#APP
# 1401 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L851
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L852:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L850:
	addl	$1, 4(%rdi)
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L1402:
	leaq	224(%rsp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L1397:
	call	buffered_vfprintf
	movl	%eax, %ebp
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L848:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 232(%rsp)
	movq	%rax, 224(%rsp)
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L902:
.L904:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	xorl	%r10d, %r10d
	subl	$32, %eax
	movl	$1, %r9d
	cltq
	movzbl	(%rcx,%rax), %eax
	movq	(%r11,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1041:
	movl	(%r15), %eax
	leaq	160(%rsp), %rdx
	movq	$0, 160(%rsp)
	cmpl	$47, %eax
	ja	.L1051
	movl	%eax, %ecx
	addq	16(%r15), %rcx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1052:
	leaq	176(%rsp), %rax
	movl	(%rcx), %esi
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L1121
	movl	16(%rsp), %r12d
	movl	32(%rsp), %r9d
	subl	%eax, %r12d
	testl	%r9d, %r9d
	je	.L1403
.L1054:
	movq	216(%rbx), %r14
	movq	%r14, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1109
.L1059:
	movq	%r13, %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%rax, %r13
	jne	.L1121
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r13, %rax
	js	.L1061
	cmpq	%r13, %rax
	jb	.L1061
.L1060:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1063
	testl	%eax, %eax
	js	.L854
	testl	%r12d, %r12d
	jle	.L909
	movl	32(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L909
	.p2align 4,,10
	.p2align 3
.L1367:
	movslq	%r12d, %r12
.L1379:
	movq	%r12, %rdx
	movl	$32, %esi
	movq	%rbx, %rdi
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L1121
	addl	%r12d, %ebp
	jo	.L1063
.L1359:
	testl	%ebp, %ebp
	jns	.L909
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1067:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1068
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1069:
	movq	(%rdx), %r14
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L903:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	xorl	%r10d, %r10d
	subl	$32, %eax
	movl	$1, %r9d
	cltq
	movzbl	(%rdi,%rax), %eax
	movq	(%r11,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L885:
	leaq	144(%rsp), %rdi
	movq	%rsi, 112(%rsp)
	movl	%r8d, 104(%rsp)
	movl	%r9d, 84(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, 16(%rsp)
	movl	84(%rsp), %r9d
	movl	104(%rsp), %r8d
	movq	112(%rsp), %rsi
	je	.L1063
	movq	144(%rsp), %rax
	movzbl	(%rax), %r12d
	cmpb	$36, %r12b
	je	.L884
	leal	-32(%r12), %eax
	cmpb	$90, %al
	jbe	.L1404
	.p2align 4,,10
	.p2align 3
.L840:
	testb	%r12b, %r12b
	je	.L1405
.L884:
	movl	92(%rsp), %edx
.L859:
	subq	$8, %rsp
	movl	%ebp, %r9d
	movq	%r15, %rcx
	movl	68(%rsp), %eax
	movq	%rbx, %rdi
	pushq	%rax
	pushq	136(%rsp)
	pushq	120(%rsp)
	movl	120(%rsp), %eax
	pushq	%rax
	leaq	296(%rsp), %rax
	pushq	%rax
	pushq	112(%rsp)
	pushq	%rdx
	movl	196(%rsp), %edx
	movq	64(%rsp), %rsi
	leaq	264(%rsp), %r8
	call	printf_positional
	addq	$64, %rsp
	movl	%eax, %ebp
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L905:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1406
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$37, (%rax)
.L908:
	cmpl	$2147483647, %ebp
	je	.L1121
	addl	$1, %ebp
	.p2align 4,,10
	.p2align 3
.L909:
	movq	144(%rsp), %rax
	movl	$37, %esi
	addl	$1, 92(%rsp)
	leaq	1(%rax), %r13
	movq	%r13, %rdi
	movq	%r13, 144(%rsp)
	call	__strchrnul@PLT
	movq	216(%rbx), %r14
	movq	%rax, 144(%rsp)
	subq	%r13, %rax
	movq	%rax, %r12
	movq	%r14, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1407
.L1116:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%r12, %rax
	jne	.L1121
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r12, %rax
	js	.L1088
	cmpq	%r12, %rax
	jb	.L1088
.L1087:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1063
	testl	%eax, %eax
	js	.L854
	movq	144(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L1090
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L925:
	movl	$10, 84(%rsp)
.L926:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	je	.L929
	cmpl	$47, %eax
	ja	.L930
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L931:
	movq	(%rdx), %r14
	movl	$0, 52(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 56(%rsp)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L910:
	testl	%r9d, %r9d
	jne	.L1408
	testl	%r14d, %r14d
	movl	(%r15), %eax
	je	.L915
	cmpl	$47, %eax
	ja	.L916
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L917:
	movsbq	(%rdx), %r10
.L914:
	testq	%r10, %r10
	js	.L1409
	movq	%r10, %r14
	xorl	%r9d, %r9d
	movl	$10, 84(%rsp)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1009:
	testb	$1, 60(%rsp)
	jne	.L1150
	movl	%r10d, %eax
	andl	$1, %eax
.L1010:
	movzbl	80(%rsp), %edx
	movzbl	56(%rsp), %ecx
	addl	%r13d, %r13d
	sall	$2, %r9d
	orl	%r13d, %eax
	movl	8(%rsp), %esi
	orl	%r9d, %eax
	movl	16(%rsp), %edi
	movsbl	%r12b, %r12d
	movl	$0, 188(%rsp)
	movl	%r12d, 184(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	movl	%esi, 176(%rsp)
	orl	%edx, %eax
	movzbl	32(%rsp), %edx
	movl	%edi, 180(%rsp)
	orl	%ecx, %eax
	movzbl	52(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	orl	%ecx, %eax
	sall	$7, %edx
	movl	%eax, %r8d
	movzbl	40(%rsp), %eax
	orl	%edx, %r8d
	testl	%r10d, %r10d
	movb	%r8b, 188(%rsp)
	movl	%eax, 192(%rsp)
	je	.L1011
	testb	$8, 60(%rsp)
	je	.L1012
	orb	$16, 189(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1013
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1014:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 160(%rsp)
.L1015:
	leaq	160(%rsp), %rax
	leaq	152(%rsp), %rdx
	leaq	176(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 152(%rsp)
	call	__printf_fphex
	testl	%eax, %eax
	jns	.L1344
	.p2align 4,,10
	.p2align 3
.L1121:
	testl	$32768, (%rbx)
	movl	$-1, %ebp
	jne	.L1092
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L999:
	testb	$1, 60(%rsp)
	jne	.L1149
	movl	%r10d, %eax
	andl	$1, %eax
.L1000:
	movzbl	80(%rsp), %edx
	movzbl	56(%rsp), %ecx
	addl	%r13d, %r13d
	sall	$2, %r9d
	orl	%r13d, %eax
	movl	8(%rsp), %edi
	orl	%r9d, %eax
	movl	16(%rsp), %esi
	movsbl	%r12b, %r12d
	movl	$0, 188(%rsp)
	movl	%r12d, 184(%rsp)
	sall	$3, %edx
	sall	$4, %ecx
	movl	%edi, 176(%rsp)
	orl	%edx, %eax
	movzbl	32(%rsp), %edx
	movl	%esi, 180(%rsp)
	orl	%ecx, %eax
	movzbl	52(%rsp), %ecx
	sall	$5, %edx
	orl	%edx, %eax
	sall	$6, %ecx
	movl	%r8d, %edx
	orl	%ecx, %eax
	sall	$7, %edx
	movl	%eax, %r8d
	movzbl	72(%rsp), %eax
	orl	%edx, %r8d
	movzbl	40(%rsp), %edx
	movb	%r8b, 188(%rsp)
	sall	$3, %eax
	testl	%r10d, %r10d
	movb	%al, 189(%rsp)
	movl	%edx, 192(%rsp)
	je	.L1001
	testb	$8, 60(%rsp)
	je	.L1002
	orl	$16, %eax
	movb	%al, 189(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1003
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1004:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 160(%rsp)
.L1005:
	leaq	160(%rsp), %rax
	leaq	152(%rsp), %rdx
	leaq	176(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 152(%rsp)
	call	__printf_fp
	testl	%eax, %eax
	js	.L1121
.L1344:
	xorl	%edx, %edx
	addl	%eax, %ebp
	js	.L1105
	cmpl	%eax, %ebp
	jb	.L1105
.L1104:
	testl	%edx, %edx
	je	.L1359
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L928:
	movl	$16, 84(%rsp)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L927:
	movl	$8, 84(%rsp)
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L1025:
	testb	$2, 60(%rsp)
	je	.L1026
	movl	132(%rsp), %r12d
	testl	%r12d, %r12d
	jne	.L1026
	movq	(%rsp), %r12
	movl	%r9d, 8(%rsp)
	movq	%r12, %rdi
	call	strlen
	leaq	1(%rax), %rsi
	movq	%r12, %rdi
	call	__readonly_area
	testl	%eax, %eax
	movl	%eax, 132(%rsp)
	movl	8(%rsp), %r9d
	jns	.L1026
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L1026:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	jne	.L1411
	testl	%r14d, %r14d
	je	.L1030
	cmpl	$47, %eax
	ja	.L1031
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1032:
	movq	(%rdx), %rax
	movb	%bpl, (%rax)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L1019:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1020
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1021:
	movq	(%rdx), %r10
	testq	%r10, %r10
	je	.L1022
	movq	%r10, %r14
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	$120, %r12d
	movl	$1, 80(%rsp)
	movl	$16, 84(%rsp)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1038:
	movl	88(%rsp), %edi
	leaq	256(%rsp), %rsi
	movl	$1000, %edx
	call	__strerror_r
	xorl	%r9d, %r9d
	movq	%rax, %r14
.L1039:
	testq	%r14, %r14
	je	.L1412
	cmpb	$83, %r12b
	je	.L1024
	testl	%r9d, %r9d
	jne	.L1024
	cmpl	$-1, 8(%rsp)
	je	.L1073
.L1023:
	movslq	8(%rsp), %rsi
	movq	%r14, %rdi
	call	__strnlen
	movq	%rax, %r12
.L1072:
	movl	16(%rsp), %r8d
	subl	%eax, %r8d
	js	.L1413
	movl	32(%rsp), %edi
	testl	%edi, %edi
	jne	.L1078
	testl	%r8d, %r8d
	je	.L1078
	movslq	%r8d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movl	%r8d, 8(%rsp)
	call	_IO_padn
	cmpq	%rax, %r13
	jne	.L1121
	movl	8(%rsp), %r8d
	addl	%r8d, %ebp
	js	.L1063
	cmpl	%r8d, %ebp
	jb	.L1063
	testl	%ebp, %ebp
	js	.L854
.L1078:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1113
.L1081:
	movl	%r8d, 8(%rsp)
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%rax, %r12
	movl	8(%rsp), %r8d
	jne	.L1121
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r12, %rax
	js	.L1083
	cmpq	%r12, %rax
	jb	.L1083
.L1082:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1063
	testl	%eax, %eax
	js	.L854
	testl	%r8d, %r8d
	je	.L909
	movl	32(%rsp), %esi
	testl	%esi, %esi
	je	.L909
	movslq	%r8d, %r12
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L873:
	movq	144(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 144(%rsp)
	movq	%rax, 176(%rsp)
	movzbl	1(%rdx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L1414
.L874:
	movl	(%r15), %edx
	cmpl	$47, %edx
	ja	.L880
	movl	%edx, %ecx
	addq	16(%r15), %rcx
	addl	$8, %edx
	movl	%edx, (%r15)
.L881:
	movl	(%rcx), %edi
	testl	%edi, %edi
	movl	%edi, 16(%rsp)
	js	.L1415
.L882:
	movzbl	(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %edx
	leaq	step1_jumps.12777(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L872:
	movq	144(%rsp), %rax
	movl	$1, 72(%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
.L1350:
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step0_jumps.12746(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L899:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	xorl	%r13d, %r13d
	subl	$32, %eax
	movl	$1, %r14d
	cltq
	movzbl	(%rdi,%rax), %eax
	movq	(%r11,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L898:
	movq	144(%rsp), %rax
	movl	$1, %r13d
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %edx
	leaq	step3a_jumps.12779(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L887:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	cmpb	$42, %r12b
	je	.L1416
	movzbl	%r12b, %eax
	movl	$0, 8(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1417
.L896:
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step2_jumps.12778(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L901:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	movl	$1, %r9d
	subl	$32, %eax
	movl	$1, %r10d
	cltq
	movzbl	(%rdi,%rax), %eax
	movq	(%r11,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L900:
	movq	144(%rsp), %rax
	movl	$1, %r9d
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %eax
	jmp	*(%rsi,%rax,8)
	.p2align 4,,10
	.p2align 3
.L867:
	movq	144(%rsp), %rax
	movl	$1, 80(%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
.L1351:
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rcx
	subl	$32, %eax
	cltq
	movzbl	(%rcx,%rax), %edx
	leaq	step0_jumps.12746(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L866:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	movb	$32, 40(%rsp)
	subl	$32, %eax
	movl	$1, 32(%rsp)
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step0_jumps.12746(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L865:
	movq	144(%rsp), %rax
	movl	$1, 52(%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L861:
	movq	144(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	movl	$1, 56(%rsp)
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step0_jumps.12746(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L870:
	cmpq	$-1, 96(%rsp)
	je	.L1418
.L871:
	movq	144(%rsp), %rax
	movl	$1, %r8d
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L868:
	movl	32(%rsp), %eax
	movzbl	40(%rsp), %edi
	testl	%eax, %eax
	movl	$48, %eax
	cmove	%eax, %edi
	movq	144(%rsp), %rax
	movb	%dil, 40(%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movzbl	1(%rax), %r12d
	leal	-32(%r12), %eax
	cmpb	$90, %al
	ja	.L840
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L929:
	testl	%r14d, %r14d
	jne	.L1419
	testl	%r13d, %r13d
	jne	.L935
	cmpl	$47, %eax
	ja	.L936
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L937:
	movl	(%rdx), %r14d
	movl	$0, 52(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 56(%rsp)
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	256(%rsp), %r11
	leaq	1000(%r11), %rax
	movq	%rax, 104(%rsp)
	movl	8(%rsp), %eax
	testl	%eax, %eax
	js	.L1145
	je	.L941
	movslq	%eax, %r13
	movb	$32, 40(%rsp)
.L940:
	leaq	1000(%r11), %rax
	movl	84(%rsp), %edx
	xorl	%ecx, %ecx
	cmpb	$88, %r12b
	movq	%r14, %rdi
	movl	%r9d, 112(%rsp)
	sete	%cl
	movq	%rax, %rsi
	movq	%r11, 8(%rsp)
	movl	%r8d, 128(%rsp)
	movq	%rax, 136(%rsp)
	call	_itoa_word
	movq	96(%rsp), %rcx
	movq	%rax, %r10
	movq	8(%rsp), %r11
	movl	112(%rsp), %r9d
	testq	%rcx, %rcx
	je	.L943
	movl	128(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L943
	movq	120(%rsp), %r8
	movq	136(%rsp), %rdx
	movq	%r11, %rdi
	movq	%rax, %rsi
	call	group_number
	movl	112(%rsp), %r9d
	movq	8(%rsp), %r11
	movq	%rax, %r10
.L943:
	cmpl	$10, 84(%rsp)
	jne	.L944
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L944
	leaq	1000(%r11), %rsi
	movq	%r10, %rdi
	movl	%r9d, 8(%rsp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movl	8(%rsp), %r9d
	movq	%rax, %r10
.L944:
	movq	104(%rsp), %rdi
	movq	%rdi, %rcx
	subq	%r10, %rcx
	cmpq	%r13, %rcx
	jl	.L945
	testq	%r14, %r14
	je	.L946
	cmpl	$8, 84(%rsp)
	jne	.L947
	movl	80(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L947
	leaq	-1(%r10), %rax
	movq	%rdi, %rcx
	movl	32(%rsp), %edi
	movl	$0, %edx
	movb	$48, -1(%r10)
	subq	%rax, %rcx
	movq	%rax, %r10
	subq	%rcx, %r13
	cmovs	%rdx, %r13
	testl	%edi, %edi
	movl	%r13d, 8(%rsp)
	jne	.L949
	movl	16(%rsp), %edx
	subl	%ecx, %edx
	subl	%r13d, %edx
	movl	%edx, %r13d
	.p2align 4,,10
	.p2align 3
.L950:
	movl	56(%rsp), %eax
	orl	%r9d, %eax
	orl	52(%rsp), %eax
	cmpl	$1, %eax
	adcl	$-1, %r13d
	cmpb	$32, 40(%rsp)
	je	.L1420
.L952:
	testl	%r9d, %r9d
	je	.L955
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1421
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$45, (%rax)
.L963:
	cmpl	$2147483647, %ebp
	je	.L1121
	addl	$1, %ebp
.L958:
	testq	%r14, %r14
	je	.L964
	cmpl	$16, 84(%rsp)
	jne	.L964
	movl	80(%rsp), %eax
	testl	%eax, %eax
	je	.L964
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1422
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$48, (%rax)
.L966:
	cmpl	$2147483647, %ebp
	je	.L1121
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1423
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rax)
.L968:
	cmpl	$2147483646, %ebp
	je	.L1121
	addl	$2, %ebp
.L964:
	addl	8(%rsp), %r13d
	testl	%r13d, %r13d
	jle	.L969
	movslq	%r13d, %r12
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r10, 16(%rsp)
	movq	%rcx, 8(%rsp)
	call	_IO_padn
	cmpq	%rax, %r12
	jne	.L1121
	addl	%r13d, %ebp
	js	.L1063
	cmpl	%r13d, %ebp
	jb	.L1063
	testl	%ebp, %ebp
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r10
	js	.L854
.L969:
	movq	216(%rbx), %r12
	movq	%r12, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1424
.L972:
	movq	%rcx, %rdx
	movq	%rcx, 8(%rsp)
	movq	%r10, %rsi
	movq	%rbx, %rdi
	call	*56(%r12)
	movq	8(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1121
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%rcx, %rax
	js	.L1076
	cmpq	%rcx, %rax
	jb	.L1076
.L1075:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1063
	testl	%eax, %eax
	jns	.L909
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1407:
	call	_IO_vtable_check
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1399:
	call	_IO_vtable_check
	jmp	.L853
.L1419:
	cmpl	$47, %eax
	ja	.L933
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L934:
	movzbl	(%rdx), %r14d
	movl	$0, 52(%rsp)
	movl	$0, 56(%rsp)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1403:
	testl	%r12d, %r12d
	jle	.L1054
	movslq	%r12d, %r14
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	call	_IO_padn
	cmpq	%rax, %r14
	jne	.L1121
	addl	%r12d, %ebp
	js	.L1063
	cmpl	%r12d, %ebp
	jb	.L1063
	testl	%ebp, %ebp
	jns	.L1054
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L854
.L1001:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1006
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1007:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 160(%rsp)
	jmp	.L1005
.L1408:
	movl	(%r15), %edx
	cmpl	$47, %edx
	ja	.L912
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$8, %edx
	movl	%edx, (%r15)
.L913:
	movq	(%rax), %r10
	jmp	.L914
.L1011:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1016
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1017:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 160(%rsp)
	jmp	.L1015
.L1411:
	cmpl	$47, %eax
	ja	.L1028
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1029:
	movq	(%rdx), %rax
	movslq	%ebp, %rdx
	movq	%rdx, (%rax)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L1404:
	movsbl	%r12b, %eax
	leaq	jump_table(%rip), %rdi
	subl	$32, %eax
	cltq
	movzbl	(%rdi,%rax), %edx
	leaq	step1_jumps.12777(%rip), %rax
	jmp	*(%rax,%rdx,8)
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%r14, %rdi
	call	strlen
	movq	%rax, %r12
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1415:
	negl	16(%rsp)
	movb	$32, 40(%rsp)
	movl	$1, 32(%rsp)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L1416:
	leaq	2(%rax), %rdx
	movq	%rdx, 144(%rsp)
	movq	%rdx, 176(%rsp)
	movzbl	2(%rax), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L889
.L893:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1425
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L895:
	movl	(%rdx), %edx
	movl	$-1, %eax
	testl	%edx, %edx
	cmovns	%edx, %eax
	movl	%eax, 8(%rsp)
	movq	144(%rsp), %rax
	movzbl	(%rax), %r12d
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L1414:
	leaq	176(%rsp), %rdi
	movq	%rsi, 104(%rsp)
	movl	%r8d, 84(%rsp)
	movl	%r9d, 16(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	16(%rsp), %r9d
	movl	84(%rsp), %r8d
	movq	104(%rsp), %rsi
	je	.L1355
	testl	%eax, %eax
	je	.L879
	movq	176(%rsp), %rax
	cmpb	$36, (%rax)
	je	.L884
.L879:
	movq	144(%rsp), %rax
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L1123:
	movl	$0, 132(%rsp)
	movq	$-1, 96(%rsp)
	movq	$0, 120(%rsp)
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	80(%rax), %rdi
	movq	72(%rax), %rcx
	movzbl	(%rdi), %eax
	movq	%rcx, 120(%rsp)
	testb	%al, %al
	je	.L1131
	cmpb	$127, %al
	je	.L1131
	cmpb	$0, (%rcx)
	movl	$0, %eax
	cmovne	%rdi, %rax
	movq	%rax, 96(%rsp)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	8(%rsp), %esi
	movl	$5, %eax
	leaq	.LC3(%rip), %r14
	cmpl	$5, %esi
	cmovge	%esi, %eax
	cmpb	$83, %r12b
	movl	%eax, 8(%rsp)
	jne	.L1023
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	32(%rsp), %r8d
	movl	16(%rsp), %ecx
	movl	%ebp, %r9d
	movl	8(%rsp), %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, %ebp
	jns	.L909
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L949:
	testl	%r9d, %r9d
	je	.L976
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1426
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$45, (%rax)
.L984:
	cmpl	$2147483647, %ebp
	je	.L1121
	subl	$1, 16(%rsp)
	addl	$1, %ebp
.L979:
	testq	%r14, %r14
	je	.L985
	cmpl	$16, 84(%rsp)
	jne	.L985
	movl	80(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L985
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1427
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$48, (%rax)
.L987:
	cmpl	$2147483647, %ebp
	je	.L1121
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1428
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%r12b, (%rax)
.L989:
	cmpl	$2147483646, %ebp
	je	.L1121
	subl	$2, 16(%rsp)
	addl	$2, %ebp
.L985:
	movl	16(%rsp), %r12d
	addl	%ecx, %r13d
	subl	%r13d, %r12d
	movl	8(%rsp), %r13d
	testl	%r13d, %r13d
	jle	.L990
	movslq	%r13d, %r14
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r10, 16(%rsp)
	movq	%rcx, 8(%rsp)
	call	_IO_padn
	cmpq	%rax, %r14
	jne	.L1121
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r10
	js	.L992
	cmpl	%r14d, %ebp
	jb	.L992
.L991:
	testl	%eax, %eax
	jne	.L1063
	testl	%ebp, %ebp
	js	.L854
.L990:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1097
.L993:
	movq	%rcx, %rdx
	movq	%rcx, 8(%rsp)
	movq	%r10, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	movq	8(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1121
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%rcx, %rax
	js	.L995
	cmpq	%rcx, %rax
	jb	.L995
.L994:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1063
	testl	%eax, %eax
	js	.L854
	testl	%r12d, %r12d
	jg	.L1367
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L945:
	movl	32(%rsp), %esi
	subq	%rcx, %r13
	movl	$0, %eax
	cmovs	%rax, %r13
	movl	%r13d, 8(%rsp)
	testl	%esi, %esi
	jne	.L949
	movl	16(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	testq	%r14, %r14
	movl	%eax, %r13d
	je	.L950
.L1117:
	cmpl	$16, 84(%rsp)
	jne	.L950
	movl	80(%rsp), %edx
	leal	-2(%r13), %eax
	testl	%edx, %edx
	cmovne	%eax, %r13d
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L941:
	testq	%r14, %r14
	jne	.L1146
	cmpl	$8, 84(%rsp)
	jne	.L1147
	movl	80(%rsp), %eax
	testl	%eax, %eax
	je	.L1147
	leaq	999(%r11), %r10
	movb	$48, 1255(%rsp)
	xorl	%r13d, %r13d
	movl	$1, %ecx
	movb	$32, 40(%rsp)
.L942:
	movl	32(%rsp), %edx
	testl	%edx, %edx
	jne	.L949
	movl	16(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L976:
	movl	52(%rsp), %eax
	testl	%eax, %eax
	je	.L980
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1429
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L955:
	movl	52(%rsp), %eax
	testl	%eax, %eax
	je	.L959
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1430
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	$1, %r13d
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1146:
	xorl	%r13d, %r13d
	movb	$32, 40(%rsp)
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L946:
	subq	%rcx, %r13
	cmovs	%r14, %r13
	movl	%r13d, 8(%rsp)
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1412:
	movl	8(%rsp), %eax
	cmpl	$-1, %eax
	je	.L1071
	cmpl	$5, %eax
	jg	.L1071
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC4(%rip), %r14
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L947:
	subq	%rcx, %r13
	movl	$0, %eax
	cmovs	%rax, %r13
	movl	32(%rsp), %eax
	movl	%r13d, 8(%rsp)
	testl	%eax, %eax
	jne	.L949
	movl	16(%rsp), %eax
	subl	%ecx, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L980:
	movl	56(%rsp), %eax
	testl	%eax, %eax
	je	.L979
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1431
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L959:
	movl	56(%rsp), %eax
	testl	%eax, %eax
	je	.L958
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L1432
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1420:
	testl	%r13d, %r13d
	jle	.L1148
	movslq	%r13d, %r8
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	movq	%r8, 16(%rsp)
	movq	%r10, 72(%rsp)
	movq	%rcx, 40(%rsp)
	movl	%r9d, 32(%rsp)
	call	_IO_padn
	movq	16(%rsp), %r8
	cmpq	%rax, %r8
	jne	.L1121
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movl	32(%rsp), %r9d
	movq	40(%rsp), %rcx
	movq	72(%rsp), %r10
	js	.L954
	cmpl	%r8d, %ebp
	jb	.L954
.L953:
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1063
	testl	%ebp, %ebp
	jns	.L952
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1147:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	movb	$32, 40(%rsp)
	leaq	1000(%r11), %r10
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1433
.L1074:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r12, %rax
	jne	.L1121
	movslq	%ebp, %rax
	xorl	%edx, %edx
	addq	%r12, %rax
	js	.L1076
	cmpq	%r12, %rax
	jnb	.L1075
.L1076:
	movl	$1, %edx
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	160(%rsp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	160(%rsp)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1409:
	negq	%r10
	movl	$10, 84(%rsp)
	movl	$1, %r9d
	movq	%r10, %r14
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1071:
	movl	$6, %eax
	movl	$6, %r12d
	leaq	null(%rip), %r14
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L851:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L852
	call	__lll_lock_wait_private
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, %ebp
	movl	%ebp, (%rdi)
	movl	$-1, %ebp
	movl	$9, %fs:(%rax)
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L915:
	testl	%r13d, %r13d
	jne	.L918
	cmpl	$47, %eax
	ja	.L919
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L920:
	movslq	(%rdx), %r10
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1030:
	testl	%r13d, %r13d
	jne	.L1033
	cmpl	$47, %eax
	ja	.L1034
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1035:
	movq	(%rdx), %rax
	movl	%ebp, (%rax)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	$-1, 96(%rsp)
	movq	$0, 120(%rsp)
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1094:
#APP
# 1689 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1092
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1689 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1148:
	xorl	%r13d, %r13d
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	%r10, 16(%rsp)
	movq	%rcx, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r10
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%r10, 16(%rsp)
	movq	%rcx, 8(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %r10
	movq	8(%rsp), %rcx
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	144(%rsp), %rdi
	movq	%rsi, 112(%rsp)
	movl	%r8d, 104(%rsp)
	movl	%r9d, 84(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, 8(%rsp)
	je	.L1063
	movq	144(%rsp), %rax
	movq	112(%rsp), %rsi
	movl	104(%rsp), %r8d
	movl	84(%rsp), %r9d
	movzbl	(%rax), %r12d
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L1400:
	testl	%r12d, %r12d
	jle	.L1042
	movslq	%r12d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	_IO_padn
	cmpq	%rax, %r13
	jne	.L1121
	addl	%r12d, %ebp
	js	.L1063
	cmpl	%r12d, %ebp
	jb	.L1063
	testl	%ebp, %ebp
	jns	.L1042
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	$0, 96(%rsp)
	jmp	.L871
.L1109:
	call	_IO_vtable_check
	jmp	.L1059
.L1113:
	movl	%r8d, 8(%rsp)
	call	_IO_vtable_check
	movl	8(%rsp), %r8d
	jmp	.L1081
.L1396:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L839
.L1421:
	movq	%r10, 32(%rsp)
	movq	%rcx, 16(%rsp)
	movl	$45, %esi
.L1374:
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	32(%rsp), %r10
	jne	.L963
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%r10, 40(%rsp)
	movq	%rcx, 32(%rsp)
	movl	$45, %esi
.L1378:
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movq	32(%rsp), %rcx
	movq	40(%rsp), %r10
	jne	.L984
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L889:
	leaq	176(%rsp), %rdi
	movq	%rsi, 104(%rsp)
	movl	%r8d, 84(%rsp)
	movl	%r9d, 8(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	8(%rsp), %r9d
	movl	84(%rsp), %r8d
	movq	104(%rsp), %rsi
	je	.L1355
	testl	%eax, %eax
	je	.L893
	movq	176(%rsp), %rax
	cmpb	$36, (%rax)
	jne	.L893
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L1406:
	movl	$37, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L908
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1150:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L1010
.L1149:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L1000
.L1422:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r10, 32(%rsp)
	movq	%rcx, 16(%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	32(%rsp), %r10
	jne	.L966
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1423:
	movzbl	%r12b, %esi
	movq	%rbx, %rdi
	movq	%r10, 32(%rsp)
	movq	%rcx, 16(%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movq	16(%rsp), %rcx
	movq	32(%rsp), %r10
	jne	.L968
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%r10, 40(%rsp)
	movq	%rcx, 32(%rsp)
	movl	$43, %esi
	jmp	.L1378
.L1430:
	movq	%r10, 32(%rsp)
	movq	%rcx, 16(%rsp)
	movl	$43, %esi
	jmp	.L1374
.L1427:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r10, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movq	32(%rsp), %rcx
	movq	40(%rsp), %r10
	jne	.L987
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1428:
	movzbl	%r12b, %esi
	movq	%rbx, %rdi
	movq	%r10, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movq	32(%rsp), %rcx
	movq	40(%rsp), %r10
	jne	.L989
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1401:
	movzbl	%dl, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L1048
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1433:
	call	_IO_vtable_check
	jmp	.L1074
.L1431:
	movq	%r10, 40(%rsp)
	movq	%rcx, 32(%rsp)
	movl	$32, %esi
	jmp	.L1378
.L1432:
	movq	%r10, 32(%rsp)
	movq	%rcx, 16(%rsp)
	movl	$32, %esi
	jmp	.L1374
.L1088:
	movl	$1, %edx
	jmp	.L1087
.L1355:
	movq	__libc_errno@gottpoff(%rip), %rsi
	movl	%eax, %ebp
	movl	$75, %fs:(%rsi)
	jmp	.L854
.L916:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L917
.L992:
	movl	$1, %eax
	jmp	.L991
.L954:
	movl	$1, %eax
	jmp	.L953
.L995:
	movl	$1, %edx
	jmp	.L994
.L1051:
	movq	8(%r15), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1052
.L919:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L920
.L918:
	cmpl	$47, %eax
	ja	.L921
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L922:
	movswq	(%rdx), %r10
	jmp	.L914
.L1003:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1004
.L1016:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1017
.L921:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L922
.L1028:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1029
.L1068:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1069
.L1034:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1035
.L1033:
	cmpl	$47, %eax
	ja	.L1036
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1037:
	movq	(%rdx), %rax
	movw	%bp, (%rax)
	jmp	.L909
.L1425:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L895
.L1013:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1014
.L1045:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1046
.L936:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L937
.L935:
	cmpl	$47, %eax
	ja	.L938
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L939:
	movzwl	(%rdx), %r14d
	movl	$0, 52(%rsp)
	xorl	%r9d, %r9d
	movl	$0, 56(%rsp)
	jmp	.L924
.L880:
	movq	8(%r15), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L881
.L1031:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1032
.L1061:
	movl	$1, %edx
	jmp	.L1060
.L1020:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1021
.L938:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L939
.L1105:
	movl	$1, %edx
	jmp	.L1104
.L912:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L913
.L1006:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1007
.L933:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L934
.L1083:
	movl	$1, %edx
	jmp	.L1082
.L930:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L931
.L1036:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1037
	.size	__vfprintf_internal, .-__vfprintf_internal
	.p2align 4,,15
	.type	buffered_vfprintf, @function
buffered_vfprintf:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8480, %rsp
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L1435
	movl	$-1, 192(%rdi)
.L1436:
	leaq	288(%rsp), %rax
	movq	%rdi, 256(%rsp)
	movq	%rdi, %rbx
	movl	$-1, 224(%rsp)
	movl	$-72515580, 32(%rsp)
	movq	%rax, 72(%rsp)
	movq	%rax, 64(%rsp)
	leaq	8480(%rsp), %rax
	movq	$0, 168(%rsp)
	movq	%rax, 80(%rsp)
	movl	116(%rdi), %eax
	leaq	32(%rsp), %rdi
	movl	%eax, 148(%rsp)
	leaq	_IO_helper_jumps(%rip), %rax
	movq	%rax, 248(%rsp)
	call	__vfprintf_internal
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %r13
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L1438
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	movq	%rsp, %rdi
	movq	%rbx, %rdx
	call	_pthread_cleanup_push_defer@PLT
.L1439:
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L1440
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L1441
#APP
# 2299 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1442
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L1443:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L1441:
	addl	$1, 4(%rdi)
.L1440:
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rbp
	subq	%rsi, %rbp
	testl	%ebp, %ebp
	jle	.L1444
	movq	216(%rbx), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r14, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L1457
.L1445:
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpl	%eax, %ebp
	movl	$-1, %eax
	cmovne	%eax, %r12d
.L1444:
	testl	$32768, (%rbx)
	jne	.L1447
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1447
	movq	$0, 8(%rdi)
#APP
# 2319 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1449
	subl	$1, (%rdi)
.L1447:
	testq	%r13, %r13
	je	.L1434
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L1434:
	addq	$8480, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1435:
	movl	$-1, %r12d
	cmpl	%r12d, %eax
	je	.L1436
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1457:
	call	_IO_vtable_check
	movq	64(%rsp), %rsi
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1442:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L1443
	call	__lll_lock_wait_private
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1449:
#APP
# 2319 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1447
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 2319 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1447
	.size	buffered_vfprintf, .-buffered_vfprintf
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	step4_jumps.12968, @object
	.size	step4_jumps.12968, 240
step4_jumps.12968:
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.quad	.L229
	.quad	.L233
	.quad	.L240
	.quad	.L242
	.quad	.L243
	.quad	.L316
	.quad	.L344
	.quad	.L371
	.quad	.L332
	.quad	.L336
	.quad	.L342
	.quad	.L345
	.quad	.L324
	.quad	.L228
	.quad	.L228
	.quad	.L228
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12964, @object
	.size	__PRETTY_FUNCTION__.12964, 18
__PRETTY_FUNCTION__.12964:
	.string	"printf_positional"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12650, @object
	.size	__PRETTY_FUNCTION__.12650, 15
__PRETTY_FUNCTION__.12650:
	.string	"outstring_func"
	.section	.data.rel.ro.local
	.align 32
	.type	step3b_jumps.12781, @object
	.size	step3b_jumps.12781, 240
step3b_jumps.12781:
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L901
	.quad	.L840
	.quad	.L840
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L999
	.quad	.L1040
	.quad	.L1067
	.quad	.L1019
	.quad	.L1025
	.quad	.L1038
	.quad	.L1041
	.quad	.L1009
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.align 32
	.type	step4_jumps.12782, @object
	.size	step4_jumps.12782, 240
step4_jumps.12782:
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L999
	.quad	.L1040
	.quad	.L1067
	.quad	.L1019
	.quad	.L1025
	.quad	.L1038
	.quad	.L1041
	.quad	.L1009
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.align 32
	.type	step3a_jumps.12779, @object
	.size	step3a_jumps.12779, 240
step3a_jumps.12779:
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L899
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L1025
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.align 32
	.type	step2_jumps.12778, @object
	.size	step2_jumps.12778, 240
step2_jumps.12778:
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L898
	.quad	.L900
	.quad	.L901
	.quad	.L902
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L999
	.quad	.L1040
	.quad	.L1067
	.quad	.L1019
	.quad	.L1025
	.quad	.L1038
	.quad	.L1041
	.quad	.L1009
	.quad	.L903
	.quad	.L904
	.quad	.L840
	.align 32
	.type	step1_jumps.12777, @object
	.size	step1_jumps.12777, 240
step1_jumps.12777:
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L840
	.quad	.L887
	.quad	.L898
	.quad	.L900
	.quad	.L901
	.quad	.L902
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L999
	.quad	.L1040
	.quad	.L1067
	.quad	.L1019
	.quad	.L1025
	.quad	.L1038
	.quad	.L1041
	.quad	.L1009
	.quad	.L903
	.quad	.L904
	.quad	.L840
	.align 32
	.type	step0_jumps.12746, @object
	.size	step0_jumps.12746, 240
step0_jumps.12746:
	.quad	.L840
	.quad	.L861
	.quad	.L865
	.quad	.L866
	.quad	.L867
	.quad	.L868
	.quad	.L870
	.quad	.L873
	.quad	.L885
	.quad	.L887
	.quad	.L898
	.quad	.L900
	.quad	.L901
	.quad	.L902
	.quad	.L905
	.quad	.L910
	.quad	.L925
	.quad	.L927
	.quad	.L928
	.quad	.L999
	.quad	.L1040
	.quad	.L1067
	.quad	.L1019
	.quad	.L1025
	.quad	.L1038
	.quad	.L1041
	.quad	.L1009
	.quad	.L903
	.quad	.L904
	.quad	.L872
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_helper_jumps, @object
	.size	_IO_helper_jumps, 168
_IO_helper_jumps:
	.quad	0
	.quad	0
	.quad	_IO_default_finish
	.quad	_IO_helper_overflow
	.quad	_IO_default_underflow
	.quad	_IO_default_uflow
	.quad	_IO_default_pbackfail
	.quad	_IO_default_xsputn
	.quad	_IO_default_xsgetn
	.quad	_IO_default_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_default_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.zero	16
	.section	.rodata
	.align 32
	.type	jump_table, @object
	.size	jump_table, 91
jump_table:
	.byte	1
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	14
	.byte	0
	.byte	6
	.byte	0
	.byte	0
	.byte	7
	.byte	2
	.byte	0
	.byte	3
	.byte	9
	.byte	0
	.byte	5
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	25
	.byte	0
	.byte	19
	.byte	19
	.byte	19
	.byte	0
	.byte	29
	.byte	0
	.byte	0
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	21
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	20
	.byte	15
	.byte	19
	.byte	19
	.byte	19
	.byte	10
	.byte	15
	.byte	28
	.byte	0
	.byte	11
	.byte	24
	.byte	23
	.byte	17
	.byte	22
	.byte	12
	.byte	0
	.byte	21
	.byte	27
	.byte	16
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.section	.rodata.str1.1
	.type	null, @object
	.size	null, 7
null:
	.string	"(null)"
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	_IO_default_doallocate
	.hidden	_IO_default_xsgetn
	.hidden	_IO_default_xsputn
	.hidden	_IO_default_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_default_finish
	.hidden	__lll_lock_wait_private
	.hidden	__printf_modifier_table
	.hidden	__readonly_area
	.hidden	__printf_fphex
	.hidden	__printf_fp
	.hidden	__strerror_r
	.hidden	__strnlen
	.hidden	__printf_va_arg_table
	.hidden	_nl_current_LC_NUMERIC
	.hidden	__printf_function_table
	.hidden	__libc_fatal
	.hidden	__printf_arginfo_table
	.hidden	memset
	.hidden	_itoa_word
	.hidden	__libc_scratch_buffer_grow_preserve
	.hidden	__parse_one_specmb
	.hidden	_IO_padn
	.hidden	__wcsrtombs
	.hidden	__assert_fail
	.hidden	__wcrtomb
	.hidden	_nl_current_LC_CTYPE
	.hidden	__libc_scratch_buffer_set_array_size
	.hidden	__towctrans
	.hidden	__overflow
	.hidden	_IO_vtable_check
	.hidden	memmove
	.hidden	strlen
