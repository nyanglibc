	.text
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rdx
	movl	$2147483647, %r10d
	movl	$-1, %r9d
	movl	4(%rdx), %ecx
	movl	(%rdx), %eax
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	subl	$48, %eax
	cmpl	$9, %ecx
	ja	.L5
.L7:
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jg	.L14
	leal	(%rax,%rax,4), %eax
	movl	%r10d, %r8d
	subl	%ecx, %r8d
	addl	%eax, %eax
	cmpl	%eax, %r8d
	jl	.L14
	addl	%ecx, %eax
.L3:
	movq	%rsi, %rdx
	movl	4(%rdx), %ecx
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L7
.L5:
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	12(%rdx), %eax
	leaq	12(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	%r9d, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	movq	%rsi, (%rdi)
	ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	_IO_helper_overflow, @function
_IO_helper_overflow:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	subq	$24, %rsp
	movq	160(%rdi), %rdx
	movq	24(%rdx), %rsi
	movq	32(%rdx), %rbx
	subq	%rsi, %rbx
	sarq	$2, %rbx
	testl	%ebx, %ebx
	je	.L17
	movq	456(%rdi), %rdi
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	216(%rdi), %r13
	subq	%rdx, %rax
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L25
.L18:
	movslq	%ebx, %rbx
	movq	%rbx, %rdx
	call	*56(%r13)
	testq	%rax, %rax
	je	.L22
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	je	.L22
	movq	160(%r12), %rdx
	leaq	0(,%rax,4), %r13
	subq	%rax, %rbx
	movq	24(%rdx), %rdi
	movq	%rbx, %rdx
	leaq	(%rdi,%r13), %rsi
	call	__wmemmove
	movq	160(%r12), %rdx
	subq	%r13, 32(%rdx)
.L17:
	movq	32(%rdx), %rax
	cmpq	40(%rdx), %rax
	jnb	.L26
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	%ebp, (%rax)
	movl	%ebp, %eax
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	160(%r12), %rax
	movq	8(%rsp), %rdi
	movq	24(%rax), %rsi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$24, %rsp
	movl	%ebp, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__woverflow
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-1, %eax
	jmp	.L16
	.size	_IO_helper_overflow, .-_IO_helper_overflow
	.p2align 4,,15
	.type	group_number, @function
group_number:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r9
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movsbl	(%rcx), %ebx
	leal	-1(%rbx), %eax
	cmpb	$125, %al
	jbe	.L39
.L28:
	addq	$24, %rsp
	movq	%r9, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rdx, %rbp
	movq	%rdx, %r12
	movl	%r8d, 12(%rsp)
	subq	%rsi, %rbp
	leaq	1(%rcx), %r13
	movq	%rbp, %rdx
	call	memmove
	leaq	(%rax,%rbp), %rsi
	movq	%rax, %r10
	movq	%r12, %r9
	movl	12(%rsp), %r8d
	cmpq	%rsi, %rax
	jnb	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	subq	$4, %rsi
	movl	(%rsi), %eax
	subl	$1, %ebx
	leaq	-4(%r12), %r9
	movl	%eax, -4(%r12)
	jne	.L30
	cmpq	%rsi, %r10
	jnb	.L28
	cmpq	%r9, %rsi
	je	.L31
	movl	%r8d, -8(%r12)
	movsbl	0(%r13), %ebx
	leaq	-8(%r12), %r9
	cmpb	$127, %bl
	je	.L31
	testb	%bl, %bl
	js	.L31
	testb	%bl, %bl
	je	.L34
	addq	$1, %r13
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r9, %r12
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rsi, %r10
	jb	.L36
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	movsbl	-1(%r13), %ebx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r10, %rdx
	movq	%r9, %rdi
	subq	%rsi, %rdx
	call	memmove
	addq	$24, %rsp
	movq	%rax, %r9
	popq	%rbx
	movq	%r9, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	group_number, .-group_number
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_outpunct"
	.text
	.p2align 4,,15
	.type	_i18n_number_rewrite, @function
_i18n_number_rewrite:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %rbp
	subq	$1064, %rsp
	subq	%r13, %rbp
	call	__wctrans@PLT
	leaq	16(%rsp), %rbx
	movq	%rax, %r12
	movq	%rax, %rsi
	movl	$46, %edi
	call	__towctrans
	movq	%r12, %rsi
	movl	$44, %edi
	movl	%eax, 8(%rsp)
	call	__towctrans
	movq	%rbp, %rsi
	movl	%eax, 12(%rsp)
	leaq	16(%rbx), %rax
	sarq	$2, %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	movq	%rax, 16(%rsp)
	movq	$1024, 24(%rsp)
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L50
	movq	16(%rsp), %r15
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	__mempcpy@PLT
	movq	%rax, %rcx
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
.L42:
	subq	$4, %rcx
	cmpq	%rcx, %r15
	ja	.L55
.L49:
	movl	(%rcx), %r8d
	subq	$4, %r14
	leal	-48(%r8), %eax
	cmpl	$9, %eax
	jbe	.L56
	testq	%r12, %r12
	jne	.L57
.L45:
	subq	$4, %rcx
	movl	%r8d, (%r14)
	cmpq	%rcx, %r15
	jbe	.L49
.L55:
	movq	16(%rsp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L40
	call	free@PLT
.L40:
	addq	$1064, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rdx), %rax
	addl	$3, %r8d
	movslq	%r8d, %r8
	movl	64(%rax,%r8,8), %eax
	movl	%eax, (%r14)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r8d, %eax
	andl	$-3, %eax
	cmpl	$44, %eax
	jne	.L45
	cmpl	$46, %r8d
	movl	8(%rsp), %eax
	cmovne	12(%rsp), %eax
	movl	%eax, (%r14)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r13, %r14
	jmp	.L40
	.size	_i18n_number_rewrite, .-_i18n_number_rewrite
	.section	.rodata.str1.1
.LC1:
	.string	"vfprintf-internal.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(size_t) done <= (size_t) INT_MAX"
	.section	.text.unlikely,"ax",@progbits
	.type	outstring_func.part.1, @function
outstring_func.part.1:
	leaq	__PRETTY_FUNCTION__.12676(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$238, %edx
	call	__assert_fail
	.size	outstring_func.part.1, .-outstring_func.part.1
	.text
	.p2align 4,,15
	.type	outstring_converted_wide_string, @function
outstring_converted_wide_string:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%r9d, %ebx
	subq	$328, %rsp
	testl	%ecx, %ecx
	setg	23(%rsp)
	cmpb	$1, %r8b
	movl	%ecx, 24(%rsp)
	movq	%rsi, 40(%rsp)
	movl	%edx, 16(%rsp)
	movl	%r8d, 28(%rsp)
	movzbl	23(%rsp), %ecx
	je	.L61
	testb	%cl, %cl
	je	.L61
	testl	%edx, %edx
	movq	$0, 56(%rsp)
	movq	%rsi, 48(%rsp)
	js	.L142
	movslq	16(%rsp), %r13
	movl	$0, %r12d
	je	.L64
	testq	%rsi, %rsi
	je	.L64
	leaq	48(%rsp), %rax
	movq	%r13, %rbx
	leaq	56(%rsp), %r15
	leaq	64(%rsp), %r14
	movl	%r9d, (%rsp)
	movq	%rax, %r13
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L143:
	testq	%rax, %rax
	je	.L136
	addq	%rax, %r12
	subq	%rax, %rbx
	je	.L136
	cmpq	$0, 48(%rsp)
	je	.L136
.L67:
	cmpq	$64, %rbx
	movl	$64, %edx
	movq	%r15, %rcx
	cmovb	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$-1, %eax
.L60:
	addq	$328, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movl	(%rsp), %ebx
.L63:
	movslq	24(%rsp), %rax
	cmpq	%r12, %rax
	ja	.L64
.L68:
	movq	40(%rsp), %rsi
.L61:
	movslq	16(%rsp), %r8
	movq	$-1, %rax
	movq	$0, 56(%rsp)
	leaq	64(%rsp), %r12
	testl	%r8d, %r8d
	cmovns	%r8, %rax
	xorl	%r13d, %r13d
	movq	%rax, %r15
	leaq	56(%rsp), %rax
	movq	%r15, %r14
	movq	%rax, (%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 8(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	*56(%r9)
	cmpq	%rax, %r15
	jne	.L140
	movslq	%ebx, %rbx
	movabsq	$-9223372036854775808, %rdi
	leaq	(%rbx,%r15), %rax
	leaq	(%rax,%rdi), %rdx
	movslq	%eax, %rsi
	movl	%eax, %ebx
	cmpq	%r15, %rdx
	setb	%dl
	cmpq	%rsi, %rax
	movl	$1, %esi
	movzbl	%dl, %edx
	cmovne	%esi, %edx
	testl	%edx, %edx
	jne	.L139
	testl	%eax, %eax
	js	.L60
	movl	16(%rsp), %edx
	movq	%r14, %rax
	movq	40(%rsp), %rsi
	subq	%r15, %rax
	addq	%r15, %r13
	testl	%edx, %edx
	cmovns	%rax, %r14
.L75:
	testq	%r14, %r14
	je	.L91
	testq	%rsi, %rsi
	je	.L91
	movq	(%rsp), %rcx
	movq	8(%rsp), %rsi
	cmpq	$64, %r14
	movl	$64, %edx
	movq	%r12, %rdi
	cmovb	%r14, %rdx
	call	__mbsrtowcs
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L140
	testq	%rax, %rax
	je	.L91
	testl	%ebx, %ebx
	js	.L144
	movq	216(%rbp), %r9
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r9, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	ja	.L82
	movq	%r9, 32(%rsp)
	call	_IO_vtable_check
	movq	32(%rsp), %r9
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L91:
	cmpb	$0, 23(%rsp)
	je	.L106
	cmpb	$0, 28(%rsp)
	je	.L106
	movslq	24(%rsp), %rdx
	movl	%ebx, %eax
	cmpq	%r13, %rdx
	jbe	.L60
	movl	24(%rsp), %edx
	subl	%r13d, %edx
	testl	%edx, %edx
	jle	.L60
	movslq	%edx, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	movq	%rax, %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r12
	jne	.L60
	testl	%ebx, %ebx
	movl	%ebx, %eax
	js	.L60
	addl	%r12d, %eax
	jno	.L60
	.p2align 4,,10
	.p2align 3
.L139:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L64:
	movl	24(%rsp), %eax
	subl	%r12d, %eax
	testl	%eax, %eax
	jle	.L69
	movslq	%eax, %r12
	movl	$32, %esi
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L140
	testl	%ebx, %ebx
	jns	.L145
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%ebx, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	56(%rsp), %rcx
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	__mbsrtowcs
	movq	%rax, %r12
	jmp	.L63
.L145:
	addl	%r12d, %ebx
	jo	.L139
.L69:
	testl	%ebx, %ebx
	jns	.L68
	movl	%ebx, %eax
	jmp	.L60
.L144:
	call	outstring_func.part.1
	.size	outstring_converted_wide_string, .-outstring_converted_wide_string
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC3:
	.string	"("
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"i"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	")"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC4:
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(mode_flags & PRINTF_FORTIFY) != 0"
	.align 8
.LC6:
	.string	"*** invalid %N$ use detected ***\n"
	.align 8
.LC7:
	.string	"*** %n in writable segment detected ***\n"
	.text
	.p2align 4,,15
	.type	printf_positional, @function
printf_positional:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-2128(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2312, %rsp
	movq	%rax, -2232(%rbp)
	addq	$16, %rax
	movq	%rax, -2128(%rbp)
	leaq	-1088(%rbp), %rax
	movq	%r8, -2240(%rbp)
	movq	%rdi, -2208(%rbp)
	movq	%rsi, -2336(%rbp)
	movq	%rax, -2304(%rbp)
	addq	$16, %rax
	cmpq	$-1, 48(%rbp)
	movl	%edx, -2324(%rbp)
	movl	%r9d, -2248(%rbp)
	movq	24(%rbp), %r8
	movq	$1024, -2120(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$1024, -1080(%rbp)
	movq	$0, -2184(%rbp)
	je	.L922
.L147:
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.L923
	movq	-2232(%rbp), %rax
	movl	$14, %r12d
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movq	%r12, -2200(%rbp)
	movq	%r8, %r14
	leaq	16(%rax), %r13
	leaq	-2184(%rbp), %rax
	movq	%rax, -2216(%rbp)
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	(%r15,%r15,8), %rax
	movq	-2216(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	addq	$1, %r15
	leaq	0(%r13,%rax,8), %r12
	movq	%r12, %rdx
	call	__parse_one_specwc
	movq	32(%r12), %r14
	addq	%rax, %rbx
	movl	(%r14), %eax
	testl	%eax, %eax
	je	.L924
	cmpq	%r15, -2200(%rbp)
	jne	.L148
	movq	-2232(%rbp), %rdi
	call	__libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L893
	movabsq	$-2049638230412172401, %rax
	movq	-2128(%rbp), %r13
	mulq	-2120(%rbp)
	shrq	$6, %rdx
	movq	%rdx, -2200(%rbp)
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L247:
	testl	%r13d, %r13d
	je	.L282
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L283
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L283
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$45, (%rsi)
.L293:
	movl	-2200(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L893
	addl	$1, %eax
	subl	$1, %r11d
	movl	%eax, -2200(%rbp)
.L286:
	testq	%r15, %r15
	je	.L294
	cmpl	$16, -2248(%rbp)
	jne	.L294
	testb	$1, -2276(%rbp)
	je	.L294
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L295
	movq	32(%rax), %rdi
	movq	40(%rax), %r8
	cmpq	%r8, %rdi
	jnb	.L295
	cmpl	$2147483647, -2200(%rbp)
	leaq	4(%rdi), %rsi
	movq	%rsi, 32(%rax)
	movl	$48, (%rdi)
	je	.L893
.L300:
	cmpq	%r8, %rsi
	jnb	.L299
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	%r9d, (%rsi)
.L467:
	movl	-2200(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L893
	addl	$2, %eax
	subl	$2, %r11d
	movl	%eax, -2200(%rbp)
.L294:
	sarq	$2, %rdx
	leal	(%rdx,%rcx), %r12d
	movq	%rdx, %r15
	subl	%r12d, %r11d
	testl	%r10d, %r10d
	movslq	%r11d, %r12
	jle	.L301
	movq	-2208(%rbp), %rdi
	movslq	%r10d, %r13
	movl	$48, %esi
	movq	%r13, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	addl	%eax, %r13d
	movl	%r13d, -2200(%rbp)
	jo	.L892
	testl	%r13d, %r13d
	js	.L194
.L305:
	movq	-2208(%rbp), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	216(%rax), %rax
	subq	%r13, %r8
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpq	%rdx, %r8
	jbe	.L925
.L306:
	movq	%r8, -2240(%rbp)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-2208(%rbp), %rdi
	call	*56(%rax)
	cmpq	%r15, %rax
	movq	-2240(%rbp), %r8
	jne	.L893
	movslq	-2200(%rbp), %rax
	xorl	%edx, %edx
	addq	%r15, %rax
	js	.L308
	cmpq	%r15, %rax
	jb	.L308
.L307:
	movslq	%eax, %rcx
	movl	%eax, %ebx
	movl	%eax, -2200(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L892
	testl	%eax, %eax
	js	.L194
	testl	%r12d, %r12d
	jle	.L281
	movq	-2208(%rbp), %rdi
	movq	%r12, %rdx
	movl	$32, %esi
	movq	%r8, -2240(%rbp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L893
	addl	%ebx, %r12d
	movl	%r12d, %eax
	movl	%r12d, -2200(%rbp)
	jno	.L890
	.p2align 4,,10
	.p2align 3
.L892:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L893:
	movl	$-1, -2200(%rbp)
.L194:
	movq	-2304(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L454
	call	free@PLT
.L454:
	movq	-2232(%rbp), %rax
	movq	-2128(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L146
	call	free@PLT
.L146:
	movl	-2200(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	cmpq	%rbx, -2184(%rbp)
	cmovnb	-2184(%rbp), %rbx
.L149:
	movq	-2304(%rbp), %rdi
	movl	$24, %edx
	movq	%rbx, %rsi
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L893
	movq	-1088(%rbp), %rax
	movq	%rbx, %r12
	leaq	0(,%rbx,4), %rdx
	salq	$4, %r12
	addq	%rax, %r12
	movq	%rax, -2224(%rbp)
	movl	64(%rbp), %eax
	movq	%r12, -2200(%rbp)
	addq	%rdx, %r12
	movq	%r12, %rdi
	andl	$2, %eax
	setne	%sil
	movl	%eax, -2320(%rbp)
	movzbl	%sil, %esi
	negl	%esi
	call	memset
	testq	%r15, %r15
	je	.L153
	leaq	(%r15,%r15,8), %rax
	movq	%r13, %r14
	movq	%r13, -2216(%rbp)
	movq	%rbx, -2256(%rbp)
	movq	-2200(%rbp), %rbx
	leaq	0(%r13,%rax,8), %r8
	movq	%r8, %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L927:
	movslq	48(%r14), %rax
	movl	52(%r14), %edx
	movl	%edx, (%r12,%rax,4)
	movslq	48(%r14), %rax
	movl	64(%r14), %edx
	movl	%edx, (%rbx,%rax,4)
.L157:
	addq	$72, %r14
	cmpq	%r14, %r13
	je	.L926
.L159:
	movslq	44(%r14), %rax
	cmpl	$-1, %eax
	je	.L154
	movl	$0, (%r12,%rax,4)
.L154:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L155
	movl	$0, (%r12,%rax,4)
.L155:
	movq	56(%r14), %rsi
	testq	%rsi, %rsi
	je	.L157
	cmpq	$1, %rsi
	je	.L927
	movslq	48(%r14), %rdx
	movslq	8(%r14), %rax
	movq	%r14, %rdi
	movq	__printf_arginfo_table(%rip), %r10
	addq	$72, %r14
	salq	$2, %rdx
	leaq	(%rbx,%rdx), %rcx
	addq	%r12, %rdx
	call	*(%r10,%rax,8)
	cmpq	%r14, %r13
	jne	.L159
.L926:
	movq	-2256(%rbp), %rbx
	movq	-2216(%rbp), %r13
	testq	%rbx, %rbx
	je	.L160
.L460:
	movl	64(%rbp), %edx
	movq	-2224(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	-2240(%rbp), %r8
	andl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L193:
	movslq	(%r12,%r14,4), %rax
	cmpl	$5, %eax
	jg	.L162
	cmpl	$3, %eax
	jge	.L163
	cmpl	$1, %eax
	jg	.L164
	testl	%eax, %eax
	jns	.L165
	cmpl	$-1, %eax
	jne	.L161
	movl	-2320(%rbp), %eax
	testl	%eax, %eax
	je	.L928
	leaq	.LC6(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	$256, %eax
	je	.L163
	jle	.L929
	cmpl	$512, %eax
	je	.L163
	cmpl	$1024, %eax
	je	.L165
	cmpl	$263, %eax
	jne	.L161
	testl	%edx, %edx
	jne	.L930
	testb	$8, 64(%rbp)
	je	.L183
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L184
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L185:
	movq	%r14, %rax
	movdqa	(%rsi), %xmm0
	addq	$1, %r14
	salq	$4, %rax
	cmpq	%rbx, %r14
	movaps	%xmm0, (%rcx,%rax)
	jb	.L193
	.p2align 4,,10
	.p2align 3
.L160:
	movslq	16(%rbp), %rax
	cmpq	%r15, %rax
	movq	%rax, -2216(%rbp)
	jnb	.L471
	leaq	(%rax,%rax,8), %rax
	movq	32(%rbp), %rcx
	movq	%r15, -2288(%rbp)
	leaq	0(%r13,%rax,8), %r14
	movl	-2248(%rbp), %eax
	leaq	1000(%rcx), %rsi
	movl	%eax, -2200(%rbp)
	leaq	-2176(%rbp), %rax
	movq	%rsi, -2312(%rbp)
	movq	%rax, -2344(%rbp)
	addq	$48, %rax
	movq	%rax, -2352(%rbp)
	.p2align 4,,10
	.p2align 3
.L453:
	movzbl	12(%r14), %eax
	movl	(%r14), %r10d
	movl	8(%r14), %r9d
	movl	%eax, %edx
	movl	%eax, %edi
	movl	%eax, %r15d
	shrb	$5, %dl
	shrb	$3, %dil
	movl	%eax, %r8d
	movl	%edx, %ecx
	movzbl	13(%r14), %edx
	andl	$1, %edi
	movb	%dil, -2256(%rbp)
	movl	%eax, %edi
	andl	$1, %ecx
	shrb	$7, %dil
	movl	%eax, %r12d
	movl	%eax, %r13d
	movb	%dil, -2264(%rbp)
	movl	16(%r14), %edi
	shrb	$4, %r15b
	movl	%edx, %ebx
	shrb	$3, %dl
	shrb	$6, %r8b
	andl	$1, %edx
	shrb	%r12b
	shrb	%bl
	movb	%dl, -2272(%rbp)
	movslq	44(%r14), %rdx
	shrb	$2, %r13b
	movl	%ecx, -2240(%rbp)
	movl	%eax, %ecx
	andl	$1, %r15d
	andl	$1, %ecx
	andl	$1, %r8d
	andl	$1, %r12d
	andl	$1, %ebx
	andl	$1, %r13d
	movb	%cl, -2276(%rbp)
	cmpl	$-1, %edx
	movl	%edi, -2248(%rbp)
	je	.L931
	movq	-2224(%rbp), %rcx
	salq	$4, %rdx
	movl	(%rcx,%rdx), %r11d
	testl	%r11d, %r11d
	js	.L197
	movl	%r11d, 4(%r14)
.L196:
	movslq	40(%r14), %rax
	cmpl	$-1, %eax
	je	.L199
	movq	-2224(%rbp), %rcx
	salq	$4, %rax
	movl	(%rcx,%rax), %r10d
	testl	%r10d, %r10d
	js	.L200
	movl	%r10d, (%r14)
.L199:
	cmpl	$255, %r9d
	jg	.L202
	movq	__printf_function_table(%rip), %rax
	testq	%rax, %rax
	je	.L202
	movslq	%r9d, %rsi
	movq	(%rax,%rsi,8), %rax
	movq	%rsi, -2296(%rbp)
	testq	%rax, %rax
	je	.L202
	movq	56(%r14), %rdi
	leaq	30(,%rdi,8), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	testq	%rdi, %rdi
	je	.L203
	movl	48(%r14), %eax
	movb	%r8b, -2316(%rbp)
	xorl	%ecx, %ecx
	movq	-2224(%rbp), %r8
	movb	%r15b, -2280(%rbp)
	xorl	%esi, %esi
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L204:
	leal	(%r15,%rcx), %eax
	salq	$4, %rax
	addq	%r8, %rax
	movq	%rax, (%rdx,%rsi,8)
	leal	1(%rcx), %esi
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	ja	.L204
	movq	__printf_function_table(%rip), %rax
	movq	-2296(%rbp), %rdi
	movzbl	-2280(%rbp), %r15d
	movzbl	-2316(%rbp), %r8d
	movq	(%rax,%rdi,8), %rax
.L203:
	movl	%r9d, -2328(%rbp)
	movl	%r10d, -2316(%rbp)
	movq	%r14, %rsi
	movl	%r11d, -2280(%rbp)
	movb	%r8b, -2296(%rbp)
	movq	-2208(%rbp), %rdi
	call	*%rax
	cmpl	$-2, %eax
	movzbl	-2296(%rbp), %r8d
	movl	-2280(%rbp), %r11d
	movl	-2316(%rbp), %r10d
	movl	-2328(%rbp), %r9d
	je	.L202
	testl	%eax, %eax
	js	.L893
	movl	-2200(%rbp), %edi
	testl	%edi, %edi
	js	.L194
	cltq
	addl	%edi, %eax
	movl	%eax, -2200(%rbp)
	jo	.L892
.L361:
	movl	-2200(%rbp), %r10d
	testl	%r10d, %r10d
	js	.L194
.L866:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	subq	%r13, %r8
.L281:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	subq	%rsi, %rbx
	sarq	$2, %rbx
.L459:
	movq	-2208(%rbp), %rax
	movq	216(%rax), %r12
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	%r8, %rax
	jnb	.L932
.L449:
	movq	%rbx, %rdx
	movq	-2208(%rbp), %rdi
	call	*56(%r12)
	cmpq	%rbx, %rax
	jne	.L893
	movslq	-2200(%rbp), %r9
	xorl	%eax, %eax
	addq	%rbx, %r9
	js	.L451
	cmpq	%rbx, %r9
	jb	.L451
.L450:
	movslq	%r9d, %rdx
	movl	%r9d, -2200(%rbp)
	cmpq	%rdx, %r9
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	jne	.L892
	testl	%r9d, %r9d
	js	.L194
	addq	$1, -2216(%rbp)
	addq	$72, %r14
	movq	-2216(%rbp), %rax
	cmpq	-2288(%rbp), %rax
	jb	.L453
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L163:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L186
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L187:
	movq	(%rsi), %rsi
	movq	%r14, %rax
	salq	$4, %rax
	movq	%rsi, (%rcx,%rax)
.L173:
	addq	$1, %r14
	cmpq	%rbx, %r14
	jb	.L193
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r14, %rsi
	salq	$4, %rsi
	testb	$8, %ah
	leaq	(%rcx,%rsi), %rdi
	je	.L188
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L189
	movl	%eax, %edi
	addq	16(%r8), %rdi
	addl	$8, %eax
	movl	%eax, (%r8)
.L190:
	movq	(%rdi), %rax
	movq	%rax, (%rcx,%rsi)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L165:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L174
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L175:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L202:
	leal	-32(%r9), %eax
	cmpl	$90, %eax
	jbe	.L933
.L209:
	movq	56(%r14), %rsi
	leaq	30(,%rsi,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	testq	%rsi, %rsi
	je	.L387
	movl	48(%r14), %r8d
	movq	-2224(%rbp), %r9
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L388:
	leal	(%r8,%rdx), %eax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rax, (%rdi,%rcx,8)
	leal	1(%rdx), %ecx
	cmpq	%rcx, %rsi
	movq	%rcx, %rdx
	ja	.L388
.L387:
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L389
	movq	32(%rdx), %rax
	movq	40(%rdx), %rsi
	cmpq	%rsi, %rax
	jnb	.L389
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	$37, (%rax)
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L934
.L392:
	cmpq	%rcx, %rsi
	jbe	.L398
	leaq	4(%rcx), %rax
	movl	$2, %ebx
	movq	%rax, 32(%rdx)
	movl	$35, (%rcx)
	movzbl	12(%r14), %eax
	testb	%al, %al
	js	.L462
.L401:
	testb	$64, %al
	je	.L405
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L406
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L406
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$43, (%rdx)
.L412:
	movzbl	12(%r14), %eax
	addl	$1, %ebx
.L409:
	testb	$32, %al
	je	.L413
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L414
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L414
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$45, (%rdx)
.L416:
	addl	$1, %ebx
.L413:
	cmpl	$48, 16(%r14)
	je	.L935
.L417:
	testb	$8, 13(%r14)
	je	.L421
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L422
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L422
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$73, (%rdx)
.L424:
	addl	$1, %ebx
.L421:
	movslq	4(%r14), %rcx
	testl	%ecx, %ecx
	je	.L425
	movq	-2344(%rbp), %rax
	movabsq	$-3689348814741910323, %rdi
	leaq	48(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rcx, %rax
	subq	$4, %r12
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	leaq	_itowa_lower_digits(%rip), %rax
	testq	%rdx, %rdx
	movl	(%rax,%rcx,4), %esi
	movq	%rdx, %rcx
	movl	%esi, (%r12)
	jne	.L426
	movq	-2352(%rbp), %r15
	cmpq	%r15, %r12
	jnb	.L425
	movq	-2208(%rbp), %r13
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L936:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L427
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L893
.L431:
	cmpl	$2147483647, %ebx
	je	.L893
	addl	$1, %ebx
	cmpq	%r15, %r12
	jnb	.L425
	movl	(%r12), %esi
.L432:
	movq	160(%r13), %rax
	addq	$4, %r12
	testq	%rax, %rax
	jne	.L936
.L427:
	movq	%r13, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L431
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L933:
	movzbl	-2248(%rbp), %ecx
	movzbl	%r15b, %edi
	leaq	step4_jumps.12994(%rip), %rdx
	movl	%edi, -2280(%rbp)
	movzbl	%r8b, %edi
	movzbl	%r12b, %r12d
	movl	%edi, -2296(%rbp)
	movzbl	%bl, %ebx
	movzbl	%r13b, %r13d
	movb	%cl, -2316(%rbp)
	leaq	jump_table(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	jmp	*(%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L341:
	movl	-2240(%rbp), %r13d
	leal	-1(%r11), %ebx
	testl	%r13d, %r13d
	je	.L937
.L353:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rdi
	salq	$4, %rax
	movl	(%rdi,%rax), %esi
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L357
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L357
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L893
.L360:
	cmpl	$2147483647, -2200(%rbp)
	je	.L893
	movl	-2240(%rbp), %r11d
	addl	$1, -2200(%rbp)
	testl	%r11d, %r11d
	jne	.L938
.L214:
	movq	24(%r14), %rsi
	movq	32(%r14), %rbx
	movl	-2200(%rbp), %edx
	subq	%rsi, %rbx
	sarq	$2, %rbx
	testl	%edx, %edx
	js	.L371
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	subq	%r13, %r8
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L931:
	movl	4(%r14), %r11d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	negl	%r11d
	orl	$32, %eax
	movl	$1, -2240(%rbp)
	movl	%r11d, 4(%r14)
	movb	%al, 12(%r14)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$-1, (%r14)
	movl	$-1, %r10d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L929:
	cmpl	$7, %eax
	jg	.L161
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L178
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L179:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L922:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	96(%rax), %ecx
	movq	80(%rax), %rax
	movq	%rax, 48(%rbp)
	movzbl	(%rax), %eax
	movl	%ecx, 56(%rbp)
	testb	%al, %al
	je	.L469
	cmpb	$127, %al
	jne	.L147
.L469:
	movq	$0, 48(%rbp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%rsi, -2240(%rbp)
	call	_IO_vtable_check
	movq	-2240(%rbp), %rsi
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L188:
	movq	__printf_va_arg_table(%rip), %r9
	testq	%r9, %r9
	jne	.L939
.L191:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L928:
	leaq	__PRETTY_FUNCTION__.12990(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$1888, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L183:
	movq	8(%r8), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rsi
	movq	%rsi, 8(%r8)
	fldt	(%rax)
	movq	%r14, %rax
	salq	$4, %rax
	fstpt	(%rcx,%rax)
	jmp	.L173
.L364:
.L339:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rcx
	salq	$4, %rax
	movq	(%rcx,%rax), %rbx
	testq	%rbx, %rbx
	je	.L940
.L365:
	andl	$1, %r13d
	jne	.L368
	cmpl	$83, %r9d
	je	.L368
	movl	-2200(%rbp), %r9d
	movl	-2240(%rbp), %r8d
	movl	%r11d, %ecx
	movq	-2208(%rbp), %rdi
	movl	%r10d, %edx
	movq	%rbx, %rsi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, -2200(%rbp)
	jns	.L866
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L340:
	testl	%r13d, %r13d
	jne	.L341
	movl	-2240(%rbp), %eax
	leal	-1(%r11), %ebx
	testl	%eax, %eax
	jne	.L342
	testl	%ebx, %ebx
	jle	.L343
	movq	-2208(%rbp), %rdi
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	addl	%eax, %r12d
	movl	%r12d, -2200(%rbp)
	jo	.L892
.L343:
	movl	-2200(%rbp), %r15d
	testl	%r15d, %r15d
	js	.L194
.L342:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rsi
	salq	$4, %rax
	movzbl	(%rsi,%rax), %edi
	call	__btowc
	movq	-2208(%rbp), %rdi
	movq	160(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L346
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L346
	leaq	4(%rcx), %rsi
	cmpl	$-1, %eax
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	jne	.L360
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L338:
	movq	32(%rbp), %rsi
	movl	40(%rbp), %edi
	movl	$1000, %edx
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	xorl	%r13d, %r13d
	movl	%r11d, -2248(%rbp)
	call	__strerror_r
	movq	%rax, %rbx
	movl	-2248(%rbp), %r11d
	movl	-2256(%rbp), %r10d
	testq	%rbx, %rbx
	movl	-2264(%rbp), %r9d
	jne	.L365
.L940:
	cmpl	$-1, %r10d
	je	.L487
	xorl	%r12d, %r12d
	cmpl	$5, %r10d
	leaq	.LC4(%rip), %rbx
	jle	.L366
.L487:
	subl	$6, %r11d
	movl	$6, %r12d
	leaq	null(%rip), %rbx
	jmp	.L366
.L215:
	movslq	48(%r14), %rax
	salq	$4, %rax
	testl	%r13d, %r13d
	je	.L216
	movq	-2224(%rbp), %rsi
	movq	(%rsi,%rax), %r15
.L217:
	testq	%r15, %r15
	js	.L941
	movl	$10, -2248(%rbp)
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	-2256(%rbp), %eax
	testl	%r10d, %r10d
	movzbl	-2264(%rbp), %esi
	movl	%eax, -2276(%rbp)
	js	.L472
	jne	.L464
	testq	%r15, %r15
	jne	.L473
	cmpl	$8, -2248(%rbp)
	jne	.L474
	cmpb	$0, -2256(%rbp)
	je	.L474
	movq	32(%rbp), %rax
	movq	-2312(%rbp), %rdx
	movb	$32, -2316(%rbp)
	leaq	996(%rax), %rbx
	movl	$48, 996(%rax)
	subq	%rbx, %rdx
	movq	%rdx, %rcx
	sarq	$2, %rcx
	negq	%rcx
	testq	%rcx, %rcx
	cmovs	%r15, %rcx
	movl	%ecx, %r10d
	.p2align 4,,10
	.p2align 3
.L231:
	movl	-2240(%rbp), %eax
	testl	%eax, %eax
	jne	.L247
	sarq	$2, %rdx
	movq	%rdx, %r12
	movl	%r11d, %edx
	subl	%r12d, %edx
	subl	%ecx, %edx
	testq	%r15, %r15
	je	.L248
	cmpl	$16, -2248(%rbp)
	jne	.L248
	testb	$1, -2276(%rbp)
	leal	-2(%rdx), %eax
	cmovne	%eax, %edx
.L248:
	movl	-2280(%rbp), %eax
	orl	%r13d, %eax
	orl	-2296(%rbp), %eax
	cmpl	$1, %eax
	adcl	$-1, %edx
	cmpb	$32, -2316(%rbp)
	je	.L942
.L250:
	testl	%r13d, %r13d
	je	.L255
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L256
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L256
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rcx)
.L266:
	movl	-2200(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L893
	addl	$1, %eax
	movl	%eax, -2200(%rbp)
.L259:
	testq	%r15, %r15
	je	.L267
	cmpl	$16, -2248(%rbp)
	jne	.L267
	testb	$1, -2276(%rbp)
	je	.L267
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L268
	movq	32(%rax), %rsi
	movq	40(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L268
	cmpl	$2147483647, -2200(%rbp)
	leaq	4(%rsi), %rcx
	movq	%rcx, 32(%rax)
	movl	$48, (%rsi)
	je	.L893
.L273:
	cmpq	%rdi, %rcx
	jnb	.L272
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	%r9d, (%rcx)
.L465:
	movl	-2200(%rbp), %eax
	cmpl	$2147483646, %eax
	je	.L893
	addl	$2, %eax
	movl	%eax, -2200(%rbp)
.L267:
	addl	%r10d, %edx
	testl	%edx, %edx
	jle	.L274
	movq	-2208(%rbp), %rdi
	movslq	%edx, %r13
	movl	$48, %esi
	movq	%r13, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	movl	%eax, %r9d
	addl	%r13d, %r9d
	movl	%r9d, -2200(%rbp)
	jo	.L892
.L274:
	movl	-2200(%rbp), %edi
	testl	%edi, %edi
	js	.L194
.L920:
	movq	-2208(%rbp), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	216(%rax), %r15
	subq	%r13, %r8
	movq	%r15, %rax
	subq	%r13, %rax
	cmpq	%rax, %r8
	jbe	.L943
.L372:
	movq	%r8, -2240(%rbp)
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-2208(%rbp), %rdi
	call	*56(%r15)
	cmpq	%rax, %r12
	jne	.L893
	movslq	-2200(%rbp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%r12, %rax
	addq	%rax, %rdx
	movslq	%eax, %rcx
	movl	%eax, -2200(%rbp)
	cmpq	%r12, %rdx
	setb	%dl
	cmpq	%rcx, %rax
	movl	$1, %ecx
	movzbl	%dl, %edx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L892
.L890:
	testl	%eax, %eax
	movq	-2240(%rbp), %r8
	jns	.L281
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L210:
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L211
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L211
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$37, (%rdx)
.L213:
	movl	-2200(%rbp), %eax
	cmpl	$2147483647, %eax
	je	.L893
	addl	$1, %eax
	movl	%eax, -2200(%rbp)
	jmp	.L214
.L312:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2176(%rbp)
	jne	.L944
.L313:
	testb	$8, 64(%rbp)
	je	.L314
	movzbl	-2276(%rbp), %edx
	movzbl	13(%r14), %eax
	sall	$4, %edx
	andl	$-17, %eax
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L315:
	movq	-2344(%rbp), %rdx
	movq	-2208(%rbp), %rdi
	movq	%r14, %rsi
	call	__printf_fp
	testl	%eax, %eax
	js	.L893
	movl	-2200(%rbp), %esi
	testl	%esi, %esi
	js	.L194
	cltq
	addl	%esi, %eax
	movl	%eax, -2200(%rbp)
	jno	.L361
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$16, -2248(%rbp)
.L223:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testl	%r13d, %r13d
	je	.L226
	movq	(%rax), %r15
	movl	$0, -2296(%rbp)
	xorl	%r13d, %r13d
	movl	$0, -2280(%rbp)
	jmp	.L221
.L328:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rdi
	salq	$4, %rax
	movq	(%rdi,%rax), %r15
	testq	%r15, %r15
	je	.L329
	testl	%r10d, %r10d
	movq	%r15, %rax
	js	.L945
	jne	.L946
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	movl	$120, %r9d
	movl	$16, -2248(%rbp)
	movl	$1, -2276(%rbp)
	movb	$32, -2316(%rbp)
	jmp	.L229
.L224:
	movl	$8, -2248(%rbp)
	jmp	.L223
.L332:
	movl	-2320(%rbp), %eax
	testl	%eax, %eax
	je	.L333
	movl	-2324(%rbp), %eax
	testl	%eax, %eax
	je	.L947
.L334:
	movl	-2324(%rbp), %eax
	testl	%eax, %eax
	js	.L948
.L333:
	movslq	48(%r14), %rax
	movq	-2224(%rbp), %rsi
	salq	$4, %rax
	testl	%r13d, %r13d
	movq	(%rsi,%rax), %rax
	je	.L335
	movslq	-2200(%rbp), %rdx
	movq	%rdx, (%rax)
	jmp	.L214
.L320:
	movslq	48(%r14), %rax
	salq	$4, %rax
	addq	-2224(%rbp), %rax
	testb	$1, 64(%rbp)
	movq	%rax, -2176(%rbp)
	jne	.L949
.L321:
	testb	$8, 64(%rbp)
	je	.L322
	movzbl	-2276(%rbp), %eax
	movzbl	13(%r14), %edx
	sall	$4, %eax
	andl	$-17, %edx
	orl	%edx, %eax
	movb	%al, 13(%r14)
.L323:
	movq	-2344(%rbp), %rdx
	movq	-2208(%rbp), %rdi
	movq	%r14, %rsi
	call	__printf_fphex
	testl	%eax, %eax
	js	.L893
	movl	-2200(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L194
	cltq
	addl	%ecx, %eax
	movl	%eax, -2200(%rbp)
	jno	.L361
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$10, -2248(%rbp)
	jmp	.L223
.L937:
	testl	%ebx, %ebx
	jle	.L354
	movq	-2208(%rbp), %rdi
	movslq	%ebx, %r12
	movl	$32, %esi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	addl	%eax, %r12d
	movl	%r12d, -2200(%rbp)
	jo	.L892
.L354:
	movl	-2200(%rbp), %r12d
	testl	%r12d, %r12d
	jns	.L353
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-2208(%rbp), %rdi
	movl	$37, %esi
	call	__woverflow
	cmpl	$-1, %eax
	je	.L893
	movzbl	12(%r14), %eax
	testb	$8, %al
	je	.L483
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L395
.L398:
	movq	-2208(%rbp), %rdi
	movl	$35, %esi
	call	__woverflow
	cmpl	$-1, %eax
	je	.L893
	movzbl	12(%r14), %eax
	movl	$2, %ebx
.L394:
	testb	%al, %al
	jns	.L401
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L402
.L462:
	movq	32(%rdx), %rax
	cmpq	40(%rdx), %rax
	jnb	.L402
	leaq	4(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movl	$39, (%rax)
.L404:
	addl	$1, %ebx
	movzbl	12(%r14), %eax
	jmp	.L401
.L946:
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	movl	$120, %r9d
	movl	$1, -2276(%rbp)
	movl	$16, -2248(%rbp)
	.p2align 4,,10
	.p2align 3
.L464:
	movslq	%r10d, %r12
	movb	$32, -2316(%rbp)
.L229:
	cmpq	$0, 48(%rbp)
	leaq	_itowa_upper_digits(%rip), %rcx
	setne	%al
	andl	%eax, %esi
	leaq	_itowa_lower_digits(%rip), %rax
	cmpl	$88, %r9d
	cmovne	%rax, %rcx
	movl	-2248(%rbp), %eax
	cmpl	$10, %eax
	je	.L477
	cmpl	$16, %eax
	je	.L478
	cmpl	$8, %eax
	je	.L950
	movl	-2248(%rbp), %edi
	movq	-2312(%rbp), %rbx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%edx, %edx
	subq	$4, %rbx
	divq	%rdi
	movl	(%rcx,%rdx,4), %edx
	testq	%rax, %rax
	movl	%edx, (%rbx)
	jne	.L240
.L237:
	testb	%sil, %sil
	jne	.L461
.L241:
	cmpl	$10, -2248(%rbp)
	jne	.L242
	cmpb	$0, -2272(%rbp)
	je	.L242
	movq	-2312(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%r9d, -2264(%rbp)
	movl	%r11d, -2256(%rbp)
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movl	-2264(%rbp), %r9d
	movl	-2256(%rbp), %r11d
	movq	%rax, %rbx
.L242:
	movq	-2312(%rbp), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rax
	sarq	$2, %rax
	cmpq	%r12, %rax
	jl	.L486
	testq	%r15, %r15
	jne	.L243
.L486:
	movq	%rdx, %rax
	movq	%r12, %rcx
	sarq	$2, %rax
	subq	%rax, %rcx
	movl	$0, %eax
	cmovs	%rax, %rcx
	movl	%ecx, %r10d
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L226:
	testl	%ebx, %ebx
	movl	(%rax), %r15d
	je	.L227
	movzbl	%r15b, %r15d
	movl	$0, -2296(%rbp)
	movl	$0, -2280(%rbp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L472:
	movl	$1, %r12d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%r15, %rax
.L235:
	movq	-2312(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%rax, %rdx
	shrq	$4, %rax
	subq	$4, %rbx
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rbx)
	jne	.L238
	testb	%sil, %sil
	movl	$16, -2248(%rbp)
	je	.L242
.L461:
	movl	56(%rbp), %r8d
	movq	48(%rbp), %rcx
	movq	%rbx, %rsi
	movq	-2312(%rbp), %rdx
	movq	32(%rbp), %rdi
	movl	%r9d, -2264(%rbp)
	movl	%r11d, -2256(%rbp)
	call	group_number
	movl	-2264(%rbp), %r9d
	movq	%rax, %rbx
	movl	-2256(%rbp), %r11d
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L282:
	movl	-2296(%rbp), %esi
	testl	%esi, %esi
	je	.L287
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L288
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L288
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$43, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	$8, -2248(%rbp)
	jne	.L486
	testb	$1, -2276(%rbp)
	je	.L486
	movq	-2312(%rbp), %rdx
	leaq	-4(%rbx), %rax
	movl	$0, %esi
	movl	$48, -4(%rbx)
	movl	$8, -2248(%rbp)
	movq	%rax, %rbx
	subq	%rax, %rdx
	movq	%rdx, %rcx
	sarq	$2, %rcx
	subq	%rcx, %r12
	movq	%r12, %rcx
	cmovs	%rsi, %rcx
	movl	%ecx, %r10d
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L255:
	movl	-2296(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L260
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L261
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L261
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rcx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L950:
	movq	-2312(%rbp), %rbx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rax, %rdx
	shrq	$3, %rax
	subq	$4, %rbx
	andl	$7, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rbx)
	jne	.L236
	testb	%sil, %sil
	je	.L242
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L477:
	movq	-2312(%rbp), %rbx
	movq	%r15, %rdi
	movabsq	$-3689348814741910323, %r8
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rdi, %rax
	subq	$4, %rbx
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	testq	%rdx, %rdx
	movl	(%rcx,%rdi,4), %eax
	movq	%rdx, %rdi
	movl	%eax, (%rbx)
	jne	.L234
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L301:
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	jns	.L305
.L371:
	call	outstring_func.part.1
	.p2align 4,,10
	.p2align 3
.L368:
	cmpl	$-1, %r10d
	je	.L369
.L331:
	movslq	%r10d, %rsi
	movq	%rbx, %rdi
	movl	%r11d, -2248(%rbp)
	call	__wcsnlen@PLT
	movl	-2248(%rbp), %r11d
	movq	%rax, %r12
	subl	%eax, %r11d
.L366:
	testl	%r11d, %r11d
	js	.L951
	movl	-2240(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L376
	testl	%r11d, %r11d
	je	.L377
	movq	-2208(%rbp), %rdi
	movslq	%r11d, %r13
	movl	$32, %esi
	movq	%r13, %rdx
	movl	%r11d, -2248(%rbp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	addl	%eax, %r13d
	movl	-2248(%rbp), %r11d
	movl	%r13d, -2200(%rbp)
	jo	.L892
.L377:
	movl	-2200(%rbp), %edi
	testl	%edi, %edi
	jns	.L380
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L227:
	testl	%r12d, %r12d
	je	.L886
	movzwl	%r15w, %r15d
.L886:
	movl	$0, -2296(%rbp)
	movl	$0, -2280(%rbp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L376:
	movl	-2200(%rbp), %esi
	testl	%esi, %esi
	js	.L371
.L380:
	movq	-2208(%rbp), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	216(%rax), %r15
	subq	%r13, %r8
	movq	%r15, %rax
	subq	%r13, %rax
	cmpq	%rax, %r8
	jbe	.L952
.L381:
	movq	%r8, -2256(%rbp)
	movl	%r11d, -2248(%rbp)
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-2208(%rbp), %rdi
	call	*56(%r15)
	cmpq	%rax, %r12
	movl	-2248(%rbp), %r11d
	movq	-2256(%rbp), %r8
	jne	.L893
	movslq	-2200(%rbp), %rax
	xorl	%edx, %edx
	addq	%r12, %rax
	js	.L383
	cmpq	%r12, %rax
	jb	.L383
.L382:
	movslq	%eax, %rcx
	movl	%eax, -2200(%rbp)
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L892
	testl	%eax, %eax
	js	.L194
	testl	%r11d, %r11d
	je	.L281
	movl	-2240(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L281
	movq	-2208(%rbp), %rdi
	movslq	%r11d, %rbx
	movl	$32, %esi
	movq	%rbx, %rdx
	movq	%r8, -2240(%rbp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %rbx
	jne	.L893
	addl	-2200(%rbp), %ebx
	movl	%ebx, %eax
	movl	%ebx, -2200(%rbp)
	jno	.L890
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L425:
	cmpl	$-1, (%r14)
	je	.L433
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L434
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L434
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$46, (%rdx)
.L436:
	cmpl	$2147483647, %ebx
	je	.L893
	movq	-2344(%rbp), %rax
	movslq	(%r14), %rcx
	addl	$1, %ebx
	movabsq	$-3689348814741910323, %rdi
	leaq	48(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%rcx, %rax
	subq	$4, %r12
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	leaq	_itowa_lower_digits(%rip), %rax
	testq	%rdx, %rdx
	movl	(%rax,%rcx,4), %esi
	movq	%rdx, %rcx
	movl	%esi, (%r12)
	jne	.L437
	movq	-2352(%rbp), %r15
	cmpq	%r15, %r12
	jnb	.L433
	movq	-2208(%rbp), %r13
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L953:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L438
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L893
.L441:
	cmpl	$2147483647, %ebx
	je	.L893
	addl	$1, %ebx
	cmpq	%r15, %r12
	jnb	.L433
	movl	(%r12), %esi
.L442:
	movq	160(%r13), %rax
	addq	$4, %r12
	testq	%rax, %rax
	jne	.L953
.L438:
	movq	%r13, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L441
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L938:
	testl	%ebx, %ebx
	jle	.L361
	movq	-2208(%rbp), %rdi
	movslq	%ebx, %rbx
	movl	$32, %esi
	movq	%rbx, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %rbx
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	addl	%eax, %ebx
	movl	%ebx, -2200(%rbp)
	jno	.L361
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L287:
	movl	-2280(%rbp), %eax
	testl	%eax, %eax
	je	.L286
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L291
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L291
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$32, (%rsi)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L260:
	movl	-2280(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L259
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L264
	movq	32(%rax), %rcx
	cmpq	40(%rax), %rcx
	jnb	.L264
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rcx)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L153:
	testq	%rbx, %rbx
	jne	.L460
.L471:
	movl	-2248(%rbp), %eax
	movl	%eax, -2200(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L473:
	xorl	%r12d, %r12d
	movb	$32, -2316(%rbp)
	jmp	.L229
.L216:
	movq	-2224(%rbp), %rdi
	testl	%ebx, %ebx
	movl	(%rdi,%rax), %r15d
	je	.L218
	movsbq	%r15b, %r15
	jmp	.L217
.L314:
	andb	$-17, 13(%r14)
	jmp	.L315
.L322:
	andb	$-17, 13(%r14)
	jmp	.L323
.L335:
	testl	%ebx, %ebx
	jne	.L954
	testl	%r12d, %r12d
	jne	.L337
	movl	-2200(%rbp), %edi
	movl	%edi, (%rax)
	jmp	.L214
.L405:
	testb	$16, %al
	je	.L409
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L410
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L410
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L412
.L942:
	testl	%edx, %edx
	jle	.L251
	movq	-2208(%rbp), %rdi
	movslq	%edx, %rcx
	movl	$32, %esi
	movq	%rcx, %rdx
	movq	%rcx, -2240(%rbp)
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	call	_IO_wpadn@PLT
	movq	-2240(%rbp), %rcx
	cmpq	%rax, %rcx
	jne	.L893
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	movl	%eax, %edx
	movl	-2256(%rbp), %r10d
	movl	-2264(%rbp), %r9d
	addl	%ecx, %edx
	movl	%edx, -2200(%rbp)
	jo	.L892
.L251:
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	xorl	%edx, %edx
	jmp	.L250
.L923:
	movq	-2232(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	leaq	16(%rax), %r13
	jmp	.L149
.L934:
	testb	%al, %al
	movl	$1, %ebx
	jns	.L401
	jmp	.L462
.L951:
	movl	-2200(%rbp), %r9d
	testl	%r9d, %r9d
	jns	.L920
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L474:
	movq	-2312(%rbp), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$32, -2316(%rbp)
	jmp	.L231
.L433:
	movl	8(%r14), %esi
	testl	%esi, %esi
	je	.L443
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L444
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L444
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L893
.L446:
	cmpl	$2147483647, %ebx
	je	.L893
	addl	$1, %ebx
.L443:
	movl	-2200(%rbp), %eax
	testl	%eax, %eax
	js	.L194
	movslq	%ebx, %rbx
	addl	-2200(%rbp), %ebx
	movl	%ebx, -2200(%rbp)
	jo	.L892
	testl	%ebx, %ebx
	jns	.L866
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%rbx, %rdi
	movl	%r11d, -2248(%rbp)
	call	__wcslen@PLT
	movl	-2248(%rbp), %r11d
	movq	%rax, %r12
	subl	%eax, %r11d
	jmp	.L366
.L941:
	negq	%r15
	movl	$10, -2248(%rbp)
	movl	$1, %r13d
	jmp	.L221
.L935:
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L418
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L418
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$48, (%rdx)
.L420:
	addl	$1, %ebx
	jmp	.L417
.L329:
	cmpl	$5, %r10d
	movl	$5, %eax
	leaq	.LC3(%rip), %rbx
	cmovl	%eax, %r10d
	jmp	.L331
.L954:
	movzbl	-2200(%rbp), %esi
	movb	%sil, (%rax)
	jmp	.L214
.L218:
	movslq	%r15d, %rax
	testl	%r12d, %r12d
	movswq	%r15w, %r15
	cmove	%rax, %r15
	jmp	.L217
.L402:
	movq	-2208(%rbp), %rdi
	movl	$39, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L404
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-2208(%rbp), %rdi
	movl	$48, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L420
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-2208(%rbp), %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L360
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%r8, -2240(%rbp)
	call	_IO_vtable_check
	movq	-2240(%rbp), %r8
	jmp	.L372
.L945:
	movq	%rax, %r15
	xorl	%esi, %esi
	movl	$1, %r12d
	movl	$120, %r9d
	leaq	_itowa_lower_digits(%rip), %rcx
	xorl	%r13d, %r13d
	movl	$1, -2276(%rbp)
	jmp	.L235
.L930:
	movl	4(%r8), %eax
	cmpl	$175, %eax
	ja	.L181
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$16, %eax
	movl	%eax, 4(%r8)
.L182:
	movsd	(%rsi), %xmm0
	movq	%r14, %rax
	salq	$4, %rax
	movsd	%xmm0, (%rcx,%rax)
	andl	$-257, (%r12,%r14,4)
	jmp	.L173
.L925:
	movq	%r8, -2248(%rbp)
	movq	%rax, -2240(%rbp)
	call	_IO_vtable_check
	movq	-2240(%rbp), %rax
	movq	-2248(%rbp), %r8
	jmp	.L306
.L256:
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	movl	$45, %esi
	movl	%edx, -2240(%rbp)
.L899:
	movq	-2208(%rbp), %rdi
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2256(%rbp), %r10d
	movl	-2264(%rbp), %r9d
	jne	.L266
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rcx, -2296(%rbp)
	movl	%r9d, -2272(%rbp)
	movl	$45, %esi
	movl	%r10d, -2264(%rbp)
	movq	%rdx, -2256(%rbp)
	movl	%r11d, -2240(%rbp)
.L903:
	movq	-2208(%rbp), %rdi
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2256(%rbp), %rdx
	movl	-2264(%rbp), %r10d
	movl	-2272(%rbp), %r9d
	movq	-2296(%rbp), %rcx
	jne	.L293
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-2208(%rbp), %rdi
	movl	$37, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L213
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L947:
	movq	-2336(%rbp), %r15
	movq	%r15, %rdi
	call	__wcslen@PLT
	leaq	4(,%rax,4), %rsi
	movq	%r15, %rdi
	call	__readonly_area
	movl	%eax, -2324(%rbp)
	jmp	.L334
.L268:
	movq	-2208(%rbp), %rdi
	movl	$48, %esi
	movl	%r9d, -2256(%rbp)
	movl	%r10d, -2248(%rbp)
	movl	%edx, -2240(%rbp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r10d
	movl	-2256(%rbp), %r9d
	je	.L893
	cmpl	$2147483647, -2200(%rbp)
	je	.L893
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	jne	.L955
.L272:
	movq	-2208(%rbp), %rdi
	movl	%r9d, %esi
	movl	%r10d, -2248(%rbp)
	movl	%edx, -2240(%rbp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %edx
	movl	-2248(%rbp), %r10d
	jne	.L465
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L952:
	movq	%r8, -2256(%rbp)
	movl	%r11d, -2248(%rbp)
	call	_IO_vtable_check
	movl	-2248(%rbp), %r11d
	movq	-2256(%rbp), %r8
	jmp	.L381
.L337:
	movzwl	-2200(%rbp), %esi
	movw	%si, (%rax)
	jmp	.L214
.L261:
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	movl	$43, %esi
	movl	%edx, -2240(%rbp)
	jmp	.L899
.L288:
	movq	%rcx, -2296(%rbp)
	movl	%r9d, -2272(%rbp)
	movl	$43, %esi
	movl	%r10d, -2264(%rbp)
	movq	%rdx, -2256(%rbp)
	movl	%r11d, -2240(%rbp)
	jmp	.L903
.L939:
	cmpq	$0, -64(%r9,%rax,8)
	je	.L191
	movq	-2200(%rbp), %rax
	movl	%edx, -2256(%rbp)
	movq	%rcx, -2240(%rbp)
	movq	%r8, -2216(%rbp)
	movslq	(%rax,%r14,4), %rax
	addq	$30, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movq	%rdi, (%rcx,%rsi)
	movslq	(%r12,%r14,4), %rax
	movq	%r8, %rsi
	call	*-64(%r9,%rax,8)
	movq	-2216(%rbp), %r8
	movq	-2240(%rbp), %rcx
	movl	-2256(%rbp), %edx
	jmp	.L173
.L299:
	movq	-2208(%rbp), %rdi
	movl	%r9d, %esi
	movq	%rcx, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	movq	%rdx, -2248(%rbp)
	movl	%r11d, -2240(%rbp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2248(%rbp), %rdx
	movl	-2256(%rbp), %r10d
	movq	-2264(%rbp), %rcx
	jne	.L467
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L295:
	movq	-2208(%rbp), %rdi
	movl	$48, %esi
	movq	%rcx, -2272(%rbp)
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	movq	%rdx, -2248(%rbp)
	movl	%r11d, -2240(%rbp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	-2240(%rbp), %r11d
	movq	-2248(%rbp), %rdx
	movl	-2256(%rbp), %r10d
	movl	-2264(%rbp), %r9d
	movq	-2272(%rbp), %rcx
	je	.L893
	cmpl	$2147483647, -2200(%rbp)
	je	.L893
	movq	-2208(%rbp), %rax
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L299
	movq	32(%rax), %rsi
	movq	40(%rax), %r8
	jmp	.L300
.L949:
	andb	$-2, 12(%r14)
	jmp	.L321
.L944:
	movl	$7, 52(%r14)
	andb	$-2, 12(%r14)
	jmp	.L313
.L346:
	movq	-2208(%rbp), %rdi
	movl	%eax, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L360
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L422:
	movq	-2208(%rbp), %rdi
	movl	$73, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L424
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L414:
	movq	-2208(%rbp), %rdi
	movl	$45, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L416
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-2208(%rbp), %rdi
	movl	$43, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L412
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L483:
	movl	$1, %ebx
	jmp	.L394
.L434:
	movq	-2208(%rbp), %rdi
	movl	$46, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L436
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L955:
	movq	32(%rax), %rcx
	movq	40(%rax), %rdi
	jmp	.L273
.L291:
	movq	%rcx, -2296(%rbp)
	movl	%r9d, -2272(%rbp)
	movl	$32, %esi
	movl	%r10d, -2264(%rbp)
	movq	%rdx, -2256(%rbp)
	movl	%r11d, -2240(%rbp)
	jmp	.L903
.L264:
	movl	%r9d, -2264(%rbp)
	movl	%r10d, -2256(%rbp)
	movl	$32, %esi
	movl	%edx, -2240(%rbp)
	jmp	.L899
.L410:
	movq	-2208(%rbp), %rdi
	movl	$32, %esi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L412
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L395:
	movq	32(%rdx), %rcx
	movq	40(%rdx), %rsi
	jmp	.L392
.L444:
	movq	-2208(%rbp), %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L446
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L451:
	movl	$1, %eax
	jmp	.L450
.L178:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L179
.L174:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L175
.L383:
	movl	$1, %edx
	jmp	.L382
.L189:
	movq	8(%r8), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r8)
	jmp	.L190
.L308:
	movl	$1, %edx
	jmp	.L307
.L181:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L182
.L948:
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
.L184:
	movq	8(%r8), %rax
	leaq	15(%rax), %rsi
	andq	$-16, %rsi
	leaq	16(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L185
.L186:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L187
.L164:
	movl	(%r8), %eax
	cmpl	$47, %eax
	ja	.L171
	movl	%eax, %esi
	addq	16(%r8), %rsi
	addl	$8, %eax
	movl	%eax, (%r8)
.L172:
	movl	(%rsi), %esi
	movq	%r14, %rax
	salq	$4, %rax
	movl	%esi, (%rcx,%rax)
	jmp	.L173
.L171:
	movq	8(%r8), %rsi
	leaq	8(%rsi), %rax
	movq	%rax, 8(%r8)
	jmp	.L172
	.size	printf_positional, .-printf_positional
	.p2align 4,,15
	.globl	__vfwprintf_internal
	.hidden	__vfwprintf_internal
	.type	__vfwprintf_internal, @function
__vfwprintf_internal:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$1288, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rsi, 8(%rsp)
	movl	$1, %esi
	movl	%ecx, 36(%rsp)
	movl	%fs:(%rax), %eax
	movl	%eax, 80(%rsp)
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1587
	movl	(%rbx), %eax
	testb	$8, %al
	jne	.L1621
	cmpq	$0, 8(%rsp)
	je	.L1622
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1587
	movl	(%rbx), %ebp
	movl	%ebp, %eax
	andl	$2, %eax
	movl	%eax, 32(%rsp)
	jne	.L1623
	movq	16(%r15), %rax
	movq	8(%rsp), %rdi
	movl	$37, %esi
	movdqu	(%r15), %xmm0
	movq	%rax, 232(%rsp)
	movups	%xmm0, 216(%rsp)
	call	__wcschrnul@PLT
	andl	$32768, %ebp
	movq	%rax, 56(%rsp)
	movq	%rax, 160(%rsp)
	je	.L1624
.L964:
	movq	216(%rbx), %rbp
	movq	56(%rsp), %r12
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rcx
	subq	8(%rsp), %r12
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	%rbp, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	sarq	$2, %r12
	movq	%rcx, 24(%rsp)
	cmpq	%rax, %rcx
	jbe	.L1625
.L970:
	movq	%r12, %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*56(%rbp)
	cmpq	%rax, %r12
	jne	.L1260
	movslq	%r12d, %rdx
	movq	%r12, %rax
	movl	%r12d, %ebp
	shrq	$63, %rax
	cmpq	%rdx, %r12
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1246
	testl	%r12d, %r12d
	js	.L971
	movq	160(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L971
	cmpq	$0, __printf_function_table(%rip)
	jne	.L1263
	cmpq	$0, __printf_modifier_table(%rip)
	jne	.L1263
	cmpq	$0, __printf_va_arg_table(%rip)
	jne	.L1263
	leaq	272(%rsp), %rdi
	leaq	1272(%rsp), %rcx
	movl	$0, 116(%rsp)
	movl	$0, 112(%rsp)
	movl	$0, 120(%rsp)
	movq	%rdi, 88(%rsp)
	movq	%rcx, 104(%rsp)
	movq	$-1, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L1231:
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdx
	leaq	.L957(%rip), %rax
	movq	%rdx, 160(%rsp)
	leal	-32(%r14), %edx
	cmpl	$90, %edx
	ja	.L977
	leaq	jump_table(%rip), %rax
	movzbl	(%rax,%rdx), %edx
	leaq	step0_jumps.12772(%rip), %rax
	movq	(%rax,%rdx,8), %rax
.L977:
	movl	$0, 76(%rsp)
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	movl	$0, 16(%rsp)
	movl	$0, 48(%rsp)
	movl	$0, 72(%rsp)
	movq	$-1, %r10
	movl	$32, 40(%rsp)
	leaq	step4_jumps.12808(%rip), %rsi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	88(%rsp), %rsi
	movl	80(%rsp), %edi
	movl	$1000, %edx
	movl	%r10d, 40(%rsp)
	call	__strerror_r
	movl	40(%rsp), %r10d
	movq	%rax, %r12
	xorl	%r9d, %r9d
.L1181:
	testq	%r12, %r12
	je	.L1626
	testl	%r9d, %r9d
	jne	.L1212
	cmpl	$83, %r14d
	jne	.L1627
.L1212:
	cmpl	$-1, %r10d
	je	.L1213
.L1166:
	movslq	%r10d, %rsi
	movq	%r12, %rdi
	call	__wcsnlen@PLT
	movq	%rax, %r14
.L1211:
	subl	%eax, %r13d
	js	.L1628
	movl	16(%rsp), %edi
	testl	%edi, %edi
	jne	.L1218
	testl	%r13d, %r13d
	je	.L1218
	movslq	%r13d, %rcx
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 40(%rsp)
	call	_IO_wpadn@PLT
	movq	40(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1260
	addl	%r13d, %ebp
	js	.L1246
	cmpl	%r13d, %ebp
	jb	.L1246
	testl	%ebp, %ebp
	js	.L971
.L1218:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	cmpq	%rdx, 24(%rsp)
	jbe	.L1251
.L1221:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	cmpq	%rax, %r14
	jne	.L1260
	movslq	%ebp, %rbp
	xorl	%edx, %edx
	movq	%rbp, %rax
	addq	%r14, %rax
	js	.L1223
	cmpq	%r14, %rax
	jb	.L1223
.L1222:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1246
	testl	%eax, %eax
	js	.L971
	testl	%r13d, %r13d
	je	.L1028
	movl	16(%rsp), %esi
	testl	%esi, %esi
	je	.L1028
	.p2align 4,,10
	.p2align 3
.L1601:
	movslq	%r13d, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r13
	jne	.L1260
	addl	%ebp, %r13d
	movl	%r13d, %ebp
	jno	.L1582
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$75, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L971:
	testl	$32768, (%rbx)
	jne	.L1233
.L1635:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1233
	movq	$0, 8(%rdi)
#APP
# 1689 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1235
	subl	$1, (%rdi)
.L1233:
	movl	32(%rsp), %edx
	testl	%edx, %edx
	jne	.L1629
.L956:
	addq	$1288, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1624:
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	je	.L965
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	leaq	240(%rsp), %rdi
	movq	%rbx, %rdx
	call	_pthread_cleanup_push_defer@PLT
	testl	$32768, (%rbx)
	movl	$1, 32(%rsp)
	jne	.L964
.L966:
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L967
#APP
# 1401 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L968
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L969:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L967:
	addl	$1, 4(%rdi)
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1629:
	leaq	240(%rsp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1623:
	movl	36(%rsp), %ecx
	movq	8(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	buffered_vfprintf
	movl	%eax, %ebp
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L965:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 248(%rsp)
	movq	%rax, 240(%rsp)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1021:
.L1022:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdx
	movq	%rdx, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	jbe	.L1630
	.p2align 4,,10
	.p2align 3
.L957:
	testl	%r14d, %r14d
	jne	.L1002
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1182:
	testl	%r9d, %r9d
	je	.L1631
.L1183:
	movl	16(%rsp), %r9d
	subl	$1, %r13d
	testl	%r9d, %r9d
	je	.L1632
.L1195:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1198
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1199:
	movq	160(%rbx), %rax
	movl	(%rdx), %esi
	testq	%rax, %rax
	je	.L1200
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1200
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	je	.L1260
.L1203:
	cmpl	$2147483647, %ebp
	je	.L1260
	addl	$1, %ebp
	testl	%r13d, %r13d
	jle	.L1028
	movl	16(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L1028
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1260
	addl	%r13d, %ebp
	js	.L1246
	cmpl	%r13d, %ebp
	jnb	.L1582
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1167:
	testb	$2, 36(%rsp)
	je	.L1168
	movl	116(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L1168
	movq	8(%rsp), %r14
	movl	%r9d, 40(%rsp)
	movl	%ecx, 16(%rsp)
	movq	%r14, %rdi
	call	__wcslen@PLT
	leaq	4(,%rax,4), %rsi
	movq	%r14, %rdi
	call	__readonly_area
	testl	%eax, %eax
	movl	%eax, 116(%rsp)
	movl	16(%rsp), %ecx
	movl	40(%rsp), %r9d
	jns	.L1168
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L1168:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	jne	.L1633
	testl	%ecx, %ecx
	je	.L1172
	cmpl	$47, %eax
	ja	.L1173
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1174:
	movq	(%rdx), %rax
	movb	%bpl, (%rax)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1162:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1163
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1164:
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1165
	movq	%rax, 64(%rsp)
	xorl	%r8d, %r8d
	movl	$1, 72(%rsp)
	xorl	%r9d, %r9d
	movl	$120, %r14d
	movl	$16, 84(%rsp)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1024
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1024
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$37, (%rdx)
.L1027:
	cmpl	$2147483647, %ebp
	je	.L1260
	addl	$1, %ebp
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	160(%rsp), %rax
	movl	$37, %esi
	addl	$1, 112(%rsp)
	leaq	4(%rax), %r13
	movq	%r13, %rdi
	movq	%r13, 160(%rsp)
	call	__wcschrnul@PLT
	movq	216(%rbx), %r14
	movq	%rax, 160(%rsp)
	subq	%r13, %rax
	sarq	$2, %rax
	movq	%rax, %r12
	movq	%r14, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1634
.L1227:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%rax, %r12
	jne	.L1260
	movslq	%ebp, %r9
	xorl	%eax, %eax
	addq	%r12, %r9
	js	.L1229
	cmpq	%r12, %r9
	jb	.L1229
.L1228:
	movslq	%r9d, %rdx
	movl	%r9d, %ebp
	cmpq	%rdx, %r9
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	jne	.L1246
	testl	%r9d, %r9d
	js	.L971
	movq	160(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L1231
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1206:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1207
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1208:
	movq	(%rdx), %r12
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1152:
	testb	$1, 36(%rsp)
	jne	.L1291
	movl	%edx, %eax
	andl	$1, %eax
.L1153:
	addl	%r12d, %r12d
	leal	0(,%r9,4), %ecx
	movl	$0, 204(%rsp)
	orl	%r12d, %eax
	movl	%r10d, 192(%rsp)
	movl	%r13d, 196(%rsp)
	movl	%eax, %r9d
	movzbl	72(%rsp), %eax
	movl	%r14d, 200(%rsp)
	orl	%ecx, %r9d
	movzbl	48(%rsp), %ecx
	movb	$4, 205(%rsp)
	sall	$3, %eax
	orl	%eax, %r9d
	movzbl	16(%rsp), %eax
	sall	$4, %ecx
	orl	%ecx, %r9d
	movl	%r11d, %ecx
	movl	%r9d, %r11d
	sall	$6, %ecx
	sall	$5, %eax
	orl	%eax, %r11d
	movl	%r8d, %eax
	sall	$7, %eax
	orl	%ecx, %r11d
	orl	%eax, %r11d
	movl	40(%rsp), %eax
	testl	%edx, %edx
	movb	%r11b, 204(%rsp)
	movl	%eax, 208(%rsp)
	je	.L1154
	testb	$8, 36(%rsp)
	je	.L1155
	movl	$20, %eax
	movb	%al, 205(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1156
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1157:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 176(%rsp)
.L1158:
	leaq	176(%rsp), %rax
	leaq	168(%rsp), %rdx
	leaq	192(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 168(%rsp)
	call	__printf_fphex
	testl	%eax, %eax
	jns	.L1567
	.p2align 4,,10
	.p2align 3
.L1260:
	testl	$32768, (%rbx)
	movl	$-1, %ebp
	jne	.L1233
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1142:
	testb	$1, 36(%rsp)
	jne	.L1290
	movl	%edx, %eax
	andl	$1, %eax
.L1143:
	addl	%r12d, %r12d
	leal	0(,%r9,4), %ecx
	movl	40(%rsp), %esi
	orl	%r12d, %eax
	movl	$0, 204(%rsp)
	movl	%r10d, 192(%rsp)
	movl	%eax, %r9d
	movzbl	72(%rsp), %eax
	movl	%r13d, 196(%rsp)
	orl	%ecx, %r9d
	movzbl	48(%rsp), %ecx
	movl	%r14d, 200(%rsp)
	movl	%esi, 208(%rsp)
	sall	$3, %eax
	orl	%eax, %r9d
	movzbl	16(%rsp), %eax
	sall	$4, %ecx
	orl	%ecx, %r9d
	movl	%r11d, %ecx
	movl	%r9d, %r11d
	sall	$6, %ecx
	sall	$5, %eax
	orl	%eax, %r11d
	movl	%r8d, %eax
	sall	$7, %eax
	orl	%ecx, %r11d
	orl	%eax, %r11d
	movzbl	76(%rsp), %eax
	movb	%r11b, 204(%rsp)
	sall	$3, %eax
	orl	$4, %eax
	testl	%edx, %edx
	movb	%al, 205(%rsp)
	je	.L1144
	testb	$8, 36(%rsp)
	je	.L1145
	orl	$16, %eax
	movb	%al, 205(%rsp)
	movl	4(%r15), %eax
	cmpl	$175, %eax
	ja	.L1146
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$16, %eax
	movl	%eax, 4(%r15)
.L1147:
	movdqa	(%rdx), %xmm0
	movaps	%xmm0, 176(%rsp)
.L1148:
	leaq	176(%rsp), %rax
	leaq	168(%rsp), %rdx
	leaq	192(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rax, 168(%rsp)
	call	__printf_fp
	testl	%eax, %eax
	js	.L1260
.L1567:
	xorl	%edx, %edx
	addl	%eax, %ebp
	js	.L1245
	cmpl	%eax, %ebp
	jb	.L1245
.L1244:
	testl	%edx, %edx
	jne	.L1246
.L1582:
	testl	%ebp, %ebp
	jns	.L1028
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1047:
	movl	$16, 84(%rsp)
.L1045:
	testl	%r9d, %r9d
	movl	(%r15), %eax
	je	.L1048
	cmpl	$47, %eax
	ja	.L1049
	movl	%eax, %edx
	addl	$8, %eax
	addq	16(%r15), %rdx
	movl	%eax, (%r15)
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1046:
	movl	$8, 84(%rsp)
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1044:
	movl	$10, 84(%rsp)
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1029:
	testl	%r9d, %r9d
	jne	.L1636
	testl	%ecx, %ecx
	movl	(%r15), %eax
	je	.L1034
	cmpl	$47, %eax
	ja	.L1035
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1036:
	movsbq	(%rdx), %rax
.L1033:
	testq	%rax, %rax
	js	.L1637
	movq	%rax, 64(%rsp)
	xorl	%r9d, %r9d
	movl	$10, 84(%rsp)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L991:
	movq	160(%rsp), %rdi
	leaq	4(%rdi), %rax
	movl	4(%rdi), %edi
	movq	%rax, 160(%rsp)
	movq	%rax, 192(%rsp)
	subl	$48, %edi
	cmpl	$9, %edi
	jbe	.L1638
.L992:
	movl	(%r15), %edi
	cmpl	$47, %edi
	ja	.L998
	movl	%edi, %r13d
	addq	16(%r15), %r13
	addl	$8, %edi
	movl	%edi, (%r15)
.L999:
	movl	0(%r13), %r13d
	testl	%r13d, %r13d
	js	.L1639
.L1000:
	movl	(%rax), %r14d
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
.L1574:
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step1_jumps.12803(%rip), %rax
	jmp	*(%rax,%rdi,8)
	.p2align 4,,10
	.p2align 3
.L990:
	movq	160(%rsp), %rax
	movl	$1, 76(%rsp)
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
.L1573:
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step0_jumps.12772(%rip), %rax
	jmp	*(%rax,%rdi,8)
	.p2align 4,,10
	.p2align 3
.L987:
	cmpq	$-1, 96(%rsp)
	je	.L1640
.L988:
	movq	160(%rsp), %rax
	movl	$1, %r8d
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L985:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	movl	$48, %eax
	cmovne	40(%rsp), %eax
	movl	%eax, 40(%rsp)
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L984:
	movq	160(%rsp), %rax
	movl	$1, 72(%rsp)
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L983:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movl	$1, 16(%rsp)
	movl	$32, 40(%rsp)
	movzbl	(%rdi,%rax), %edi
	leaq	step0_jumps.12772(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L982:
	movq	160(%rsp), %rax
	movl	$1, %r11d
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L978:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movl	$1, 48(%rsp)
	movzbl	(%rdi,%rax), %edi
	leaq	step0_jumps.12772(%rip), %rax
	movq	(%rax,%rdi,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rcx
	movq	%rcx, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rcx
	xorl	%r12d, %r12d
	movzbl	(%rcx,%rax), %eax
	movl	$1, %ecx
	movq	(%rsi,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	160(%rsp), %rax
	movl	$1, %r12d
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step3a_jumps.12805(%rip), %rax
	jmp	*(%rax,%rdi,8)
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	cmpl	$42, %r14d
	je	.L1641
	leal	-48(%r14), %eax
	xorl	%r10d, %r10d
	cmpl	$9, %eax
	jbe	.L1642
.L1014:
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %edi
	leaq	step2_jumps.12804(%rip), %rax
	jmp	*(%rax,%rdi,8)
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	160(%rsp), %rdi
	movq	%rsi, 152(%rsp)
	movl	%r10d, 144(%rsp)
	movl	%r9d, 136(%rsp)
	movl	%edx, 128(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r11d, 84(%rsp)
	movl	%ecx, 64(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	64(%rsp), %ecx
	movl	84(%rsp), %r11d
	movl	124(%rsp), %r8d
	movl	128(%rsp), %edx
	movl	136(%rsp), %r9d
	movslq	144(%rsp), %r10
	movq	152(%rsp), %rsi
	je	.L1246
	movq	160(%rsp), %rax
	movl	(%rax), %r14d
	cmpl	$36, %r14d
	je	.L1002
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	160(%rsp), %rax
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdx
	movq	%rdx, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movl	$1, %r9d
	movl	$1, %edx
	movzbl	(%rdi,%rax), %eax
	movq	(%rsi,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1020:
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	160(%rsp), %rax
	movl	$1, %r9d
	movl	4(%rax), %r14d
	leaq	4(%rax), %rdi
	movq	%rdi, 160(%rsp)
	leal	-32(%r14), %eax
	cmpl	$90, %eax
	ja	.L957
	leaq	jump_table(%rip), %rdi
	movzbl	(%rdi,%rax), %eax
	leaq	step3b_jumps.12807(%rip), %rdi
	jmp	*(%rdi,%rax,8)
	.p2align 4,,10
	.p2align 3
.L1263:
	leaq	272(%rsp), %rax
	movl	$0, 116(%rsp)
	movl	$0, 112(%rsp)
	movq	$-1, 96(%rsp)
	movq	%rax, 88(%rsp)
.L976:
	subq	$8, %rsp
	movl	%ebp, %r9d
	movq	%r15, %rcx
	movl	44(%rsp), %eax
	movq	%rbx, %rdi
	pushq	%rax
	pushq	%rdx
	pushq	120(%rsp)
	movl	112(%rsp), %eax
	pushq	%rax
	pushq	128(%rsp)
	pushq	104(%rsp)
	movl	168(%rsp), %eax
	pushq	%rax
	movl	180(%rsp), %edx
	movq	72(%rsp), %rsi
	leaq	280(%rsp), %r8
	call	printf_positional
	addq	$64, %rsp
	movl	%eax, %ebp
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1630:
	leaq	jump_table(%rip), %rdi
	movl	$1, %r9d
	xorl	%edx, %edx
	movzbl	(%rdi,%rax), %eax
	movq	(%rsi,%rax,8), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1048:
	testl	%ecx, %ecx
	jne	.L1643
	testl	%r12d, %r12d
	jne	.L1054
	cmpl	$47, %eax
	ja	.L1055
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1056:
	movl	(%rdx), %eax
	xorl	%r11d, %r11d
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movq	%rax, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L1043:
	testl	%r10d, %r10d
	js	.L1282
	jne	.L1577
	cmpq	$0, 64(%rsp)
	jne	.L1283
	cmpl	$8, 84(%rsp)
	jne	.L1284
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L1284
	movq	88(%rsp), %rax
	movl	$48, 1268(%rsp)
	movl	$4, %r8d
	leaq	996(%rax), %r12
.L1061:
	movl	16(%rsp), %eax
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jne	.L1078
	sarq	$2, %r8
	xorl	%r10d, %r10d
	movl	$32, 40(%rsp)
	subl	%r8d, %r13d
	.p2align 4,,10
	.p2align 3
.L1079:
	movl	48(%rsp), %eax
	orl	%r9d, %eax
	orl	%r11d, %eax
	cmpl	$1, %eax
	adcl	$-1, %r13d
	cmpl	$32, 40(%rsp)
	je	.L1644
.L1081:
	testl	%r9d, %r9d
	je	.L1084
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1085
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1085
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$45, (%rdx)
.L1095:
	cmpl	$2147483647, %ebp
	je	.L1260
	addl	$1, %ebp
.L1088:
	cmpq	$0, 64(%rsp)
	je	.L1096
	cmpl	$16, 84(%rsp)
	jne	.L1096
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L1096
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1097
	movq	32(%rax), %rcx
	movq	40(%rax), %rsi
	cmpq	%rsi, %rcx
	jnb	.L1097
	leaq	4(%rcx), %rdx
	cmpl	$2147483647, %ebp
	movq	%rdx, 32(%rax)
	movl	$48, (%rcx)
	je	.L1260
.L1255:
	cmpq	%rsi, %rdx
	jnb	.L1101
	leaq	4(%rdx), %rcx
	cmpl	$-1, %r14d
	movq	%rcx, 32(%rax)
	movl	%r14d, (%rdx)
	je	.L1260
.L1104:
	cmpl	$2147483646, %ebp
	je	.L1260
	addl	$2, %ebp
.L1096:
	addl	%r10d, %r13d
	testl	%r13d, %r13d
	jle	.L1105
	movslq	%r13d, %r14
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r8, 16(%rsp)
	call	_IO_wpadn@PLT
	cmpq	%rax, %r14
	jne	.L1260
	addl	%r13d, %ebp
	js	.L1246
	cmpl	%r13d, %ebp
	jb	.L1246
	testl	%ebp, %ebp
	movq	16(%rsp), %r8
	js	.L971
.L1105:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1645
.L1108:
	movq	%r8, %rdx
	movq	%r8, 16(%rsp)
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	movq	16(%rsp), %r8
	cmpq	%rax, %r8
	jne	.L1260
	movslq	%ebp, %r9
	xorl	%eax, %eax
	addq	%r8, %r9
	js	.L1110
	cmpq	%r8, %r9
	jb	.L1110
.L1109:
	movslq	%r9d, %rdx
	movl	%r9d, %ebp
	cmpq	%rdx, %r9
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	jne	.L1246
	testl	%r9d, %r9d
	jns	.L1028
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1621:
	orl	$32, %eax
	movl	%eax, (%rbx)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
.L1587:
	movl	$-1, %ebp
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1634:
	call	_IO_vtable_check
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1625:
	call	_IO_vtable_check
	jmp	.L970
.L1643:
	cmpl	$47, %eax
	ja	.L1052
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1053:
	movzbl	(%rdx), %eax
	xorl	%r11d, %r11d
	movl	$0, 48(%rsp)
	movq	%rax, 64(%rsp)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1627:
	movl	16(%rsp), %r8d
	movl	%ebp, %r9d
	movl	%r13d, %ecx
	movl	%r10d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	outstring_converted_wide_string
	testl	%eax, %eax
	movl	%eax, %ebp
	jns	.L1028
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	192(%rsp), %rdi
	movq	%rsi, 144(%rsp)
	movl	%r9d, 136(%rsp)
	movl	%edx, 128(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r11d, 84(%rsp)
	movl	%ecx, 64(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	64(%rsp), %ecx
	movl	84(%rsp), %r11d
	movl	124(%rsp), %r8d
	movl	128(%rsp), %edx
	movl	136(%rsp), %r9d
	movq	144(%rsp), %rsi
	je	.L1580
	testl	%eax, %eax
	je	.L1011
	movq	192(%rsp), %rax
	cmpl	$36, (%rax)
	jne	.L1011
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	120(%rsp), %edx
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1631:
	movl	16(%rsp), %r10d
	subl	$1, %r13d
	testl	%r10d, %r10d
	jne	.L1184
	testl	%r13d, %r13d
	jle	.L1184
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1260
	addl	%r13d, %ebp
	js	.L1246
	cmpl	%r13d, %ebp
	jb	.L1246
	testl	%ebp, %ebp
	js	.L971
.L1184:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1187
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1188:
	movzbl	(%rdx), %edi
	call	__btowc
	movq	160(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L1189
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L1189
	leaq	4(%rcx), %rsi
	cmpl	$-1, %eax
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	jne	.L1203
	jmp	.L1260
.L1633:
	cmpl	$47, %eax
	ja	.L1170
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1171:
	movq	(%rdx), %rax
	movslq	%ebp, %rdx
	movq	%rdx, (%rax)
	jmp	.L1028
.L1636:
	movl	(%r15), %edx
	cmpl	$47, %edx
	ja	.L1031
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$8, %edx
	movl	%edx, (%r15)
.L1032:
	movq	(%rax), %rax
	jmp	.L1033
.L1144:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1149
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1150:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 176(%rsp)
	jmp	.L1148
.L1154:
	movl	4(%r15), %edx
	cmpl	$175, %edx
	ja	.L1159
	movl	%edx, %eax
	addq	16(%r15), %rax
	addl	$16, %edx
	movl	%edx, 4(%r15)
.L1160:
	movsd	(%rax), %xmm0
	movsd	%xmm0, 176(%rsp)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1639:
	negl	%r13d
	movl	$1, 16(%rsp)
	movl	$32, 40(%rsp)
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1641:
	leaq	8(%rax), %rdi
	movl	8(%rax), %eax
	movq	%rdi, 160(%rsp)
	movq	%rdi, 192(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1007
.L1011:
	movl	(%r15), %eax
	cmpl	$47, %eax
	ja	.L1646
	movl	%eax, %edi
	addq	16(%r15), %rdi
	addl	$8, %eax
	movl	%eax, (%r15)
.L1013:
	movslq	(%rdi), %r10
	movq	$-1, %rax
	testl	%r10d, %r10d
	cmovs	%rax, %r10
	movq	160(%rsp), %rax
	movl	(%rax), %r14d
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1638:
	leaq	192(%rsp), %rdi
	movq	%rsi, 152(%rsp)
	movl	%r10d, 144(%rsp)
	movl	%r9d, 136(%rsp)
	movl	%edx, 128(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r11d, 84(%rsp)
	movl	%ecx, 64(%rsp)
	call	read_int
	cmpl	$-1, %eax
	movl	64(%rsp), %ecx
	movl	84(%rsp), %r11d
	movl	124(%rsp), %r8d
	movl	128(%rsp), %edx
	movl	136(%rsp), %r9d
	movslq	144(%rsp), %r10
	movq	152(%rsp), %rsi
	je	.L1580
	testl	%eax, %eax
	je	.L997
	movq	192(%rsp), %rax
	cmpl	$36, (%rax)
	je	.L1002
.L997:
	movq	160(%rsp), %rax
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1165:
	cmpl	$5, %r10d
	movl	$5, %eax
	leaq	.LC3(%rip), %r12
	cmovl	%eax, %r10d
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	96(%rax), %r8d
	movq	80(%rax), %rax
	movq	%rax, 96(%rsp)
	movzbl	(%rax), %eax
	movl	%r8d, 120(%rsp)
	testb	%al, %al
	sete	%dil
	cmpb	$127, %al
	sete	%al
	orb	%al, %dil
	jne	.L1294
	testl	%r8d, %r8d
	jne	.L988
.L1294:
	movq	$0, 96(%rsp)
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1078:
	testl	%r9d, %r9d
	je	.L1112
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1113
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1113
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rdx)
.L1123:
	cmpl	$2147483647, %ebp
	je	.L1260
	addl	$1, %ebp
	subl	$1, %r13d
.L1116:
	cmpq	$0, 64(%rsp)
	je	.L1124
	cmpl	$16, 84(%rsp)
	jne	.L1124
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L1124
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1125
	movq	32(%rax), %rsi
	movq	40(%rax), %rdi
	cmpq	%rdi, %rsi
	jnb	.L1125
	leaq	4(%rsi), %rdx
	cmpl	$2147483647, %ebp
	movq	%rdx, 32(%rax)
	movl	$48, (%rsi)
	je	.L1260
.L1256:
	cmpq	%rdx, %rdi
	jbe	.L1129
	leaq	4(%rdx), %rsi
	cmpl	$-1, %r14d
	movq	%rsi, 32(%rax)
	movl	%r14d, (%rdx)
	je	.L1260
.L1132:
	cmpl	$2147483646, %ebp
	je	.L1260
	addl	$2, %ebp
	subl	$2, %r13d
.L1124:
	sarq	$2, %r8
	addl	%r8d, %ecx
	movq	%r8, %r14
	subl	%ecx, %r13d
	testl	%r10d, %r10d
	jle	.L1133
	movslq	%r10d, %rcx
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movl	%r10d, 40(%rsp)
	call	_IO_wpadn@PLT
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1260
	movl	40(%rsp), %r10d
	xorl	%eax, %eax
	addl	%r10d, %ebp
	js	.L1135
	cmpl	%ecx, %ebp
	jb	.L1135
.L1134:
	testl	%eax, %eax
	jne	.L1246
	testl	%ebp, %ebp
	js	.L971
.L1133:
	movq	216(%rbx), %rax
	movq	%rax, %rdx
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	cmpq	%rdx, 24(%rsp)
	jbe	.L1237
.L1136:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%rax)
	cmpq	%rax, %r14
	jne	.L1260
	movslq	%ebp, %r9
	xorl	%eax, %eax
	addq	%r14, %r9
	js	.L1138
	cmpq	%r14, %r9
	jb	.L1138
.L1137:
	movslq	%r9d, %rdx
	movl	%r9d, %ebp
	cmpq	%rdx, %r9
	movl	$1, %edx
	cmovne	%edx, %eax
	testl	%eax, %eax
	jne	.L1246
	testl	%r9d, %r9d
	js	.L971
	testl	%r13d, %r13d
	jg	.L1601
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1283:
	xorl	%r10d, %r10d
.L1577:
	movl	$32, 40(%rsp)
.L1059:
	leaq	_itowa_upper_digits(%rip), %rax
	leaq	_itowa_lower_digits(%rip), %rcx
	cmpl	$88, %r14d
	cmove	%rax, %rcx
	movl	84(%rsp), %eax
	cmpl	$10, %eax
	je	.L1287
	cmpl	$16, %eax
	je	.L1288
	cmpl	$8, %eax
	je	.L1647
	movq	88(%rsp), %rdi
	movslq	84(%rsp), %rsi
	movq	64(%rsp), %rax
	leaq	1000(%rdi), %r12
	.p2align 4,,10
	.p2align 3
.L1069:
	xorl	%edx, %edx
	subq	$4, %r12
	divq	%rsi
	movl	(%rcx,%rdx,4), %edx
	testq	%rax, %rax
	movl	%edx, (%r12)
	jne	.L1069
.L1067:
	cmpq	$0, 96(%rsp)
	je	.L1070
	testl	%r8d, %r8d
	je	.L1070
.L1253:
	movq	88(%rsp), %rdi
	movl	120(%rsp), %r8d
	movq	%r12, %rsi
	movq	96(%rsp), %rcx
	movq	%r10, 136(%rsp)
	movl	%r9d, 128(%rsp)
	movl	%r11d, 124(%rsp)
	leaq	1000(%rdi), %rdx
	call	group_number
	movq	136(%rsp), %r10
	movl	128(%rsp), %r9d
	movq	%rax, %r12
	movl	124(%rsp), %r11d
.L1070:
	cmpl	$10, 84(%rsp)
	jne	.L1071
	movl	76(%rsp), %eax
	testl	%eax, %eax
	je	.L1071
	movq	88(%rsp), %rax
	movq	%r12, %rdi
	movq	%r10, 128(%rsp)
	movl	%r9d, 124(%rsp)
	movl	%r11d, 76(%rsp)
	leaq	1000(%rax), %rsi
	movq	%rsi, %rdx
	call	_i18n_number_rewrite
	movq	128(%rsp), %r10
	movl	124(%rsp), %r9d
	movq	%rax, %r12
	movl	76(%rsp), %r11d
.L1071:
	movq	104(%rsp), %r8
	subq	%r12, %r8
	movq	%r8, %rax
	sarq	$2, %rax
	cmpq	%r10, %rax
	jl	.L1295
	cmpq	$0, 64(%rsp)
	jne	.L1072
.L1295:
	movq	%r8, %rax
	movq	%r10, %rcx
	sarq	$2, %rax
	subq	%rax, %rcx
	movl	$0, %eax
	cmovs	%rax, %rcx
	movl	%ecx, %r10d
.L1074:
	movl	16(%rsp), %esi
	testl	%esi, %esi
	jne	.L1078
	sarq	$2, %r8
	subl	%r8d, %r13d
	subl	%ecx, %r13d
	cmpq	$0, 64(%rsp)
	je	.L1079
.L1258:
	cmpl	$16, 84(%rsp)
	jne	.L1079
	movl	72(%rsp), %edx
	leal	-2(%r13), %eax
	testl	%edx, %edx
	cmovne	%eax, %r13d
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1282:
	movl	$1, %r10d
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1072:
	cmpl	$8, 84(%rsp)
	jne	.L1296
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L1296
	movq	104(%rsp), %r8
	leaq	-4(%r12), %rax
	movq	%r10, %rcx
	movl	$48, -4(%r12)
	movq	%rax, %r12
	subq	%rax, %r8
	movq	%r8, %rdx
	sarq	$2, %rdx
	subq	%rdx, %rcx
	movl	$0, %edx
	cmovs	%rdx, %rcx
	movl	%ecx, %r10d
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1112:
	testl	%r11d, %r11d
	je	.L1117
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1118
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1118
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1084:
	testl	%r11d, %r11d
	je	.L1089
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1090
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1090
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	88(%rsp), %rsi
	movq	64(%rsp), %rax
	leaq	1000(%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	%rax, %rdx
	shrq	$3, %rax
	subq	$4, %r12
	andl	$7, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%r12)
	jne	.L1066
.L1068:
	cmpq	$0, 96(%rsp)
	je	.L1071
	testl	%r8d, %r8d
	je	.L1071
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	88(%rsp), %rsi
	movq	64(%rsp), %rax
	leaq	1000(%rsi), %r12
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	%rax, %rdx
	shrq	$4, %rax
	subq	$4, %r12
	andl	$15, %edx
	testq	%rax, %rax
	movl	(%rcx,%rdx,4), %edx
	movl	%edx, (%r12)
	jne	.L1065
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	88(%rsp), %rax
	movq	64(%rsp), %rsi
	movabsq	$-3689348814741910323, %rdi
	leaq	1000(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	%rsi, %rax
	subq	$4, %r12
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movl	(%rcx,%rsi,4), %eax
	movq	%rdx, %rsi
	movl	%eax, (%r12)
	jne	.L1064
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1626:
	cmpl	$-1, %r10d
	je	.L1210
	cmpl	$5, %r10d
	jg	.L1210
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC4(%rip), %r12
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%r8, %rax
	movl	16(%rsp), %edi
	movq	%r10, %rcx
	sarq	$2, %rax
	movl	$0, %edx
	subq	%rax, %rcx
	cmovs	%rdx, %rcx
	testl	%edi, %edi
	movl	%ecx, %r10d
	jne	.L1078
	subl	%eax, %r13d
	movq	%rax, %r8
	subl	%ecx, %r13d
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	48(%rsp), %eax
	testl	%eax, %eax
	je	.L1116
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1121
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1121
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1089:
	movl	48(%rsp), %eax
	testl	%eax, %eax
	je	.L1088
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L1093
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L1093
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1632:
	testl	%r13d, %r13d
	jle	.L1195
	movslq	%r13d, %r12
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	call	_IO_wpadn@PLT
	cmpq	%rax, %r12
	jne	.L1260
	addl	%r13d, %ebp
	js	.L1246
	cmpl	%r13d, %ebp
	jb	.L1246
	testl	%ebp, %ebp
	jns	.L1195
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1644:
	testl	%r13d, %r13d
	jle	.L1289
	movslq	%r13d, %rcx
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, 16(%rsp)
	movl	%r10d, 128(%rsp)
	movl	%r9d, 124(%rsp)
	movl	%r11d, 76(%rsp)
	movq	%r8, 40(%rsp)
	call	_IO_wpadn@PLT
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jne	.L1260
	xorl	%eax, %eax
	addl	%r13d, %ebp
	movq	40(%rsp), %r8
	movl	76(%rsp), %r11d
	movl	124(%rsp), %r9d
	movl	128(%rsp), %r10d
	js	.L1083
	cmpl	%ecx, %ebp
	jb	.L1083
.L1082:
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1246
	testl	%ebp, %ebp
	jns	.L1081
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	216(%rbx), %r13
	movq	%r13, %rax
	subq	__start___libc_IO_vtables@GOTPCREL(%rip), %rax
	cmpq	%rax, 24(%rsp)
	jbe	.L1648
.L1214:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r13)
	cmpq	%r14, %rax
	jne	.L1260
	movslq	%ebp, %rax
	xorl	%edx, %edx
	addq	%r14, %rax
	js	.L1216
	cmpq	%r14, %rax
	jb	.L1216
.L1215:
	movslq	%eax, %rcx
	movl	%eax, %ebp
	cmpq	%rcx, %rax
	movl	$1, %ecx
	cmovne	%ecx, %edx
	testl	%edx, %edx
	jne	.L1246
	testl	%eax, %eax
	jns	.L1028
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	88(%rsp), %rax
	xorl	%r8d, %r8d
	leaq	1000(%rax), %r12
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	176(%rsp)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	8(%r15), %rax
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rdx
	movq	%rdx, 8(%r15)
	fldt	(%rax)
	fstpt	176(%rsp)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	%r12, %rdi
	call	__wcslen@PLT
	movq	%rax, %r14
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1637:
	negq	%rax
	movl	$1, %r9d
	movl	$10, 84(%rsp)
	movq	%rax, 64(%rsp)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1210:
	movl	$6, %eax
	movl	$6, %r14d
	leaq	null(%rip), %r12
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L968:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L969
	call	__lll_lock_wait_private
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L1203
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1172:
	testl	%r12d, %r12d
	jne	.L1175
	cmpl	$47, %eax
	ja	.L1176
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1177:
	movq	(%rdx), %rax
	movl	%ebp, (%rax)
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1034:
	testl	%r12d, %r12d
	jne	.L1037
	cmpl	$47, %eax
	ja	.L1038
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1039:
	movslq	(%rdx), %rax
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1235:
#APP
# 1689 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1233
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1689 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1289:
	xorl	%r13d, %r13d
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	%rax, 16(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %rax
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%rcx, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	$45, %esi
	movl	%r10d, 16(%rsp)
.L1600:
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	movl	16(%rsp), %r10d
	movq	40(%rsp), %r8
	movq	48(%rsp), %rcx
	jne	.L1123
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1085:
	movl	%r10d, 40(%rsp)
	movq	%r8, 16(%rsp)
	movl	$45, %esi
.L1596:
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	40(%rsp), %r10d
	jne	.L1095
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	%r8, 16(%rsp)
	call	_IO_vtable_check
	movq	16(%rsp), %r8
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1642:
	leaq	160(%rsp), %rdi
	movq	%rsi, 144(%rsp)
	movl	%r9d, 136(%rsp)
	movl	%edx, 128(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r11d, 84(%rsp)
	movl	%ecx, 64(%rsp)
	call	read_int
	movslq	%eax, %r10
	cmpl	$-1, %r10d
	je	.L1246
	movq	160(%rsp), %rax
	movq	144(%rsp), %rsi
	movl	136(%rsp), %r9d
	movl	128(%rsp), %edx
	movl	124(%rsp), %r8d
	movl	84(%rsp), %r11d
	movl	(%rax), %r14d
	movl	64(%rsp), %ecx
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1024:
	movl	$37, %esi
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L1027
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1097:
	movl	$48, %esi
	movq	%rbx, %rdi
	movl	%r10d, 40(%rsp)
	movq	%r8, 16(%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	40(%rsp), %r10d
	je	.L1260
	cmpl	$2147483647, %ebp
	je	.L1260
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.L1649
.L1101:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%r10d, 40(%rsp)
	movq	%r8, 16(%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movq	16(%rsp), %r8
	movl	40(%rsp), %r10d
	jne	.L1104
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	%rax, 40(%rsp)
	call	_IO_vtable_check
	movq	40(%rsp), %rax
	jmp	.L1221
.L1090:
	movl	%r10d, 40(%rsp)
	movq	%r8, 16(%rsp)
	movl	$43, %esi
	jmp	.L1596
.L1118:
	movq	%rcx, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	$43, %esi
	movl	%r10d, 16(%rsp)
	jmp	.L1600
.L1125:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%rcx, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	%r10d, 16(%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	16(%rsp), %r10d
	movq	40(%rsp), %r8
	movq	48(%rsp), %rcx
	je	.L1260
	cmpl	$2147483647, %ebp
	je	.L1260
	movq	160(%rbx), %rax
	testq	%rax, %rax
	jne	.L1650
.L1129:
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movq	%rcx, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	%r10d, 16(%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	16(%rsp), %r10d
	movq	40(%rsp), %r8
	movq	48(%rsp), %rcx
	jne	.L1132
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1290:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L1143
.L1291:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L1153
.L1189:
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	jne	.L1203
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	32(%rax), %rdx
	movq	40(%rax), %rsi
	jmp	.L1255
.L1121:
	movq	%rcx, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	$32, %esi
	movl	%r10d, 16(%rsp)
	jmp	.L1600
.L1093:
	movl	%r10d, 40(%rsp)
	movq	%r8, 16(%rsp)
	movl	$32, %esi
	jmp	.L1596
.L1650:
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	jmp	.L1256
.L1648:
	call	_IO_vtable_check
	jmp	.L1214
.L1229:
	movl	$1, %eax
	jmp	.L1228
.L1580:
	movq	__libc_errno@gottpoff(%rip), %rsi
	movl	%eax, %ebp
	movl	$75, %fs:(%rsi)
	jmp	.L971
.L1052:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1053
.L998:
	movq	8(%r15), %r13
	leaq	8(%r13), %rdi
	movq	%rdi, 8(%r15)
	jmp	.L999
.L1049:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
.L1050:
	movq	(%rdx), %rax
	xorl	%r11d, %r11d
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movq	%rax, 64(%rsp)
	jmp	.L1043
.L1031:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1032
.L1646:
	movq	8(%r15), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 8(%r15)
	jmp	.L1013
.L1198:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1199
.L1163:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1164
.L1216:
	movl	$1, %edx
	jmp	.L1215
.L1138:
	movl	$1, %eax
	jmp	.L1137
.L1135:
	movl	$1, %eax
	jmp	.L1134
.L1156:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1157
.L1207:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1208
.L1173:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1174
.L1035:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1036
.L1159:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1160
.L1083:
	movl	$1, %eax
	jmp	.L1082
.L1187:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1188
.L1223:
	movl	$1, %edx
	jmp	.L1222
.L1170:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1171
.L1245:
	movl	$1, %edx
	jmp	.L1244
.L1149:
	movq	8(%r15), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%r15)
	jmp	.L1150
.L1176:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1177
.L1175:
	cmpl	$47, %eax
	ja	.L1178
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1179:
	movq	(%rdx), %rax
	movw	%bp, (%rax)
	jmp	.L1028
.L1038:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1039
.L1037:
	cmpl	$47, %eax
	ja	.L1040
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1041:
	movswq	(%rdx), %rax
	jmp	.L1033
.L1178:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1179
.L1040:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1041
.L1054:
	cmpl	$47, %eax
	ja	.L1057
	movl	%eax, %edx
	addq	16(%r15), %rdx
	addl	$8, %eax
	movl	%eax, (%r15)
.L1058:
	movzwl	(%rdx), %eax
	xorl	%r11d, %r11d
	movl	$0, 48(%rsp)
	xorl	%r9d, %r9d
	movq	%rax, 64(%rsp)
	jmp	.L1043
.L1110:
	movl	$1, %eax
	jmp	.L1109
.L1057:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1058
.L1146:
	movq	8(%r15), %rax
	leaq	15(%rax), %rdx
	andq	$-16, %rdx
	leaq	16(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1147
.L1055:
	movq	8(%r15), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, 8(%r15)
	jmp	.L1056
	.size	__vfwprintf_internal, .-__vfwprintf_internal
	.p2align 4,,15
	.type	buffered_vfprintf, @function
buffered_vfprintf:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	$1, %esi
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movl	$-1, %r13d
	subq	$33280, %rsp
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L1651
	leaq	32(%rsp), %rdi
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movq	%rbp, %rsi
	movq	%rbx, 488(%rsp)
	movl	$1, 224(%rsp)
	leaq	224(%rdi), %rax
	movl	$-72515580, 32(%rsp)
	movq	$0, 168(%rsp)
	movq	%rax, 192(%rsp)
	leaq	512(%rsp), %rax
	movq	%rax, 288(%rsp)
	movq	%rax, 280(%rsp)
	leaq	33280(%rsp), %rax
	movq	%rax, 296(%rsp)
	movl	116(%rbx), %eax
	movl	%eax, 148(%rsp)
	leaq	_IO_helper_jumps(%rip), %rax
	movq	%rax, 248(%rsp)
	call	__vfwprintf_internal
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %r12
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L1653
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	movq	%rsp, %rdi
	movq	%rbx, %rdx
	call	_pthread_cleanup_push_defer@PLT
.L1654:
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L1655
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L1656
#APP
# 2299 "vfprintf-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1657
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L1658:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L1656:
	addl	$1, 4(%rdi)
.L1655:
	movq	192(%rsp), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rbp
	subq	%rsi, %rbp
	sarq	$2, %rbp
	testl	%ebp, %ebp
	jle	.L1659
	movq	216(%rbx), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r14, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L1672
.L1660:
	movslq	%ebp, %rdx
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpl	%eax, %ebp
	movl	$-1, %eax
	cmovne	%eax, %r13d
.L1659:
	testl	$32768, (%rbx)
	jne	.L1662
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1662
	movq	$0, 8(%rdi)
#APP
# 2319 "vfprintf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1664
	subl	$1, (%rdi)
.L1662:
	testq	%r12, %r12
	je	.L1651
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L1651:
	addq	$33280, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rax
	movq	%rbx, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1672:
	call	_IO_vtable_check
	movq	192(%rsp), %rax
	movq	24(%rax), %rsi
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1657:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L1658
	call	__lll_lock_wait_private
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1664:
#APP
# 2319 "vfprintf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1662
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 2319 "vfprintf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1662
	.size	buffered_vfprintf, .-buffered_vfprintf
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	step4_jumps.12994, @object
	.size	step4_jumps.12994, 240
step4_jumps.12994:
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.quad	.L210
	.quad	.L215
	.quad	.L222
	.quad	.L224
	.quad	.L225
	.quad	.L312
	.quad	.L340
	.quad	.L364
	.quad	.L328
	.quad	.L332
	.quad	.L338
	.quad	.L341
	.quad	.L320
	.quad	.L209
	.quad	.L209
	.quad	.L209
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12990, @object
	.size	__PRETTY_FUNCTION__.12990, 18
__PRETTY_FUNCTION__.12990:
	.string	"printf_positional"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12676, @object
	.size	__PRETTY_FUNCTION__.12676, 15
__PRETTY_FUNCTION__.12676:
	.string	"outstring_func"
	.section	.data.rel.ro.local
	.align 32
	.type	step3b_jumps.12807, @object
	.size	step3b_jumps.12807, 240
step3b_jumps.12807:
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1019
	.quad	.L957
	.quad	.L957
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L1142
	.quad	.L1182
	.quad	.L1206
	.quad	.L1162
	.quad	.L1167
	.quad	.L1180
	.quad	.L1183
	.quad	.L1152
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.align 32
	.type	step4_jumps.12808, @object
	.size	step4_jumps.12808, 240
step4_jumps.12808:
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L1142
	.quad	.L1182
	.quad	.L1206
	.quad	.L1162
	.quad	.L1167
	.quad	.L1180
	.quad	.L1183
	.quad	.L1152
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.align 32
	.type	step3a_jumps.12805, @object
	.size	step3a_jumps.12805, 240
step3a_jumps.12805:
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1017
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1167
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.align 32
	.type	step2_jumps.12804, @object
	.size	step2_jumps.12804, 240
step2_jumps.12804:
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1016
	.quad	.L1018
	.quad	.L1019
	.quad	.L1020
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L1142
	.quad	.L1182
	.quad	.L1206
	.quad	.L1162
	.quad	.L1167
	.quad	.L1180
	.quad	.L1183
	.quad	.L1152
	.quad	.L1021
	.quad	.L1022
	.quad	.L957
	.align 32
	.type	step1_jumps.12803, @object
	.size	step1_jumps.12803, 240
step1_jumps.12803:
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L957
	.quad	.L1005
	.quad	.L1016
	.quad	.L1018
	.quad	.L1019
	.quad	.L1020
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L1142
	.quad	.L1182
	.quad	.L1206
	.quad	.L1162
	.quad	.L1167
	.quad	.L1180
	.quad	.L1183
	.quad	.L1152
	.quad	.L1021
	.quad	.L1022
	.quad	.L957
	.align 32
	.type	step0_jumps.12772, @object
	.size	step0_jumps.12772, 240
step0_jumps.12772:
	.quad	.L957
	.quad	.L978
	.quad	.L982
	.quad	.L983
	.quad	.L984
	.quad	.L985
	.quad	.L987
	.quad	.L991
	.quad	.L1003
	.quad	.L1005
	.quad	.L1016
	.quad	.L1018
	.quad	.L1019
	.quad	.L1020
	.quad	.L1023
	.quad	.L1029
	.quad	.L1044
	.quad	.L1046
	.quad	.L1047
	.quad	.L1142
	.quad	.L1182
	.quad	.L1206
	.quad	.L1162
	.quad	.L1167
	.quad	.L1180
	.quad	.L1183
	.quad	.L1152
	.quad	.L1021
	.quad	.L1022
	.quad	.L990
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_helper_jumps, @object
	.size	_IO_helper_jumps, 168
_IO_helper_jumps:
	.quad	0
	.quad	0
	.quad	_IO_wdefault_finish
	.quad	_IO_helper_overflow
	.quad	_IO_default_underflow
	.quad	_IO_default_uflow
	.quad	_IO_wdefault_pbackfail
	.quad	_IO_wdefault_xsputn
	.quad	_IO_wdefault_xsgetn
	.quad	_IO_default_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.zero	16
	.section	.rodata
	.align 32
	.type	jump_table, @object
	.size	jump_table, 91
jump_table:
	.byte	1
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	14
	.byte	0
	.byte	6
	.byte	0
	.byte	0
	.byte	7
	.byte	2
	.byte	0
	.byte	3
	.byte	9
	.byte	0
	.byte	5
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	25
	.byte	0
	.byte	19
	.byte	19
	.byte	19
	.byte	0
	.byte	29
	.byte	0
	.byte	0
	.byte	12
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	21
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	26
	.byte	0
	.byte	20
	.byte	15
	.byte	19
	.byte	19
	.byte	19
	.byte	10
	.byte	15
	.byte	28
	.byte	0
	.byte	11
	.byte	24
	.byte	23
	.byte	17
	.byte	22
	.byte	12
	.byte	0
	.byte	21
	.byte	27
	.byte	16
	.byte	0
	.byte	0
	.byte	18
	.byte	0
	.byte	13
	.section	.rodata.str4.16,"aMS",@progbits,4
	.align 16
	.type	null, @object
	.size	null, 28
null:
	.string	"("
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"u"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	"l"
	.string	""
	.string	""
	.string	")"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	_IO_wdefault_doallocate
	.hidden	_IO_wdefault_xsgetn
	.hidden	_IO_wdefault_xsputn
	.hidden	_IO_wdefault_pbackfail
	.hidden	_IO_default_uflow
	.hidden	_IO_wdefault_finish
	.hidden	__lll_lock_wait_private
	.hidden	__printf_modifier_table
	.hidden	__readonly_area
	.hidden	_itowa_upper_digits
	.hidden	__printf_fphex
	.hidden	__printf_fp
	.hidden	__strerror_r
	.hidden	__btowc
	.hidden	__printf_va_arg_table
	.hidden	_nl_current_LC_NUMERIC
	.hidden	_itowa_lower_digits
	.hidden	__printf_function_table
	.hidden	__libc_fatal
	.hidden	__printf_arginfo_table
	.hidden	memset
	.hidden	__libc_scratch_buffer_grow_preserve
	.hidden	__parse_one_specwc
	.hidden	__mbsrtowcs
	.hidden	__assert_fail
	.hidden	_nl_current_LC_CTYPE
	.hidden	__libc_scratch_buffer_set_array_size
	.hidden	__towctrans
	.hidden	memmove
	.hidden	__woverflow
	.hidden	_IO_vtable_check
	.hidden	__wmemmove
