	.text
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rdx
	movl	$2147483647, %r10d
	movl	$-1, %r9d
	movl	4(%rdx), %ecx
	movl	(%rdx), %eax
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	subl	$48, %eax
	cmpl	$9, %ecx
	ja	.L5
.L7:
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jg	.L14
	leal	(%rax,%rax,4), %eax
	movl	%r10d, %r8d
	subl	%ecx, %r8d
	addl	%eax, %eax
	cmpl	%eax, %r8d
	jl	.L14
	addl	%ecx, %eax
.L3:
	movq	%rsi, %rdx
	movl	4(%rdx), %ecx
	leaq	4(%rdx), %rsi
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L7
.L5:
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	8(%rdx), %eax
	leaq	8(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	12(%rdx), %eax
	leaq	12(%rdx), %rsi
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L10
	movl	%r9d, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	movq	%rsi, (%rdi)
	ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.globl	__parse_one_specwc
	.hidden	__parse_one_specwc
	.type	__parse_one_specwc, @function
__parse_one_specwc:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r11
	pushq	%r12
	pushq	%rbp
	leaq	4(%rdi), %rbp
	pushq	%rbx
	movq	%rdx, %rbx
	movq	%rsi, %r12
	subq	$16, %rsp
	andb	$7, 12(%rdx)
	movl	$-1, 48(%rdx)
	movq	%rbp, 8(%rsp)
	movzbl	13(%rdx), %eax
	movl	$32, 16(%rdx)
	andl	$-30, %eax
	orl	$4, %eax
	movb	%al, 13(%rdx)
	movl	4(%rdi), %eax
	leal	-48(%rax), %edx
	cmpl	$9, %edx
	jbe	.L115
.L17:
	leaq	.L22(%rip), %rcx
	movq	%rbp, %rdx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L31:
	subl	$32, %eax
	cmpl	$41, %eax
	ja	.L20
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L21-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L23-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L24-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L25-.L22
	.long	.L20-.L22
	.long	.L26-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L27-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L20-.L22
	.long	.L28-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L28:
	orb	$8, 13(%rbx)
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$4, %rdx
	movl	(%rdx), %eax
	movl	$1, %esi
	testl	%eax, %eax
	jne	.L31
.L110:
	movq	%rdx, 8(%rsp)
	movq	%rdx, %rbp
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$48, 16(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L26:
	orb	$32, 12(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L25:
	orb	$64, 12(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L24:
	orb	$-128, 12(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L23:
	orb	$8, 12(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L21:
	orb	$16, 12(%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%sil, %sil
	jne	.L110
.L29:
	testb	$32, 12(%rbx)
	je	.L32
	movl	$32, 16(%rbx)
.L32:
	movl	$-1, 44(%rbx)
	movl	$0, 4(%rbx)
	movl	0(%rbp), %eax
	cmpl	$42, %eax
	je	.L116
	subl	$48, %eax
	xorl	%r13d, %r13d
	cmpl	$9, %eax
	jbe	.L117
.L35:
	movl	$-1, 40(%rbx)
	movl	$-1, (%rbx)
	cmpl	$46, 0(%rbp)
	je	.L118
.L43:
	xorl	%eax, %eax
	andb	$-3, 13(%rbx)
	andb	$-8, 12(%rbx)
	movw	%ax, 14(%rbx)
	movq	__printf_modifier_table(%rip), %rax
	testq	%rax, %rax
	jne	.L54
.L114:
	movl	0(%rbp), %ecx
.L55:
	leal	-76(%rcx), %eax
	leaq	4(%rbp), %rdx
	cmpl	$46, %eax
	movq	%rdx, 8(%rsp)
	ja	.L57
	leaq	.L59(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L59:
	.long	.L58-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L60-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L61-.L59
	.long	.L57-.L59
	.long	.L60-.L59
	.long	.L57-.L59
	.long	.L62-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L58-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L60-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L57-.L59
	.long	.L60-.L59
	.text
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$104, 4(%rbp)
	je	.L63
	orb	$2, 12(%rbx)
	leaq	8(%rbp), %rdx
	movl	4(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L57:
	cmpq	$0, __printf_function_table(%rip)
	movq	%rdx, 8(%rsp)
	movl	%ecx, 8(%rbx)
	movl	$-1, 64(%rbx)
	je	.L65
	cmpl	$255, %ecx
	jle	.L119
.L65:
	leal	-65(%rcx), %eax
	movq	$1, 56(%rbx)
	movl	48(%rbx), %esi
	cmpl	$55, %eax
	ja	.L67
	leaq	.L69(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L69:
	.long	.L68-.L69
	.long	.L67-.L69
	.long	.L70-.L69
	.long	.L67-.L69
	.long	.L68-.L69
	.long	.L68-.L69
	.long	.L68-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L71-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L72-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L68-.L69
	.long	.L67-.L69
	.long	.L73-.L69
	.long	.L72-.L69
	.long	.L68-.L69
	.long	.L68-.L69
	.long	.L68-.L69
	.long	.L67-.L69
	.long	.L72-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L74-.L69
	.long	.L72-.L69
	.long	.L75-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L76-.L69
	.long	.L67-.L69
	.long	.L72-.L69
	.long	.L67-.L69
	.long	.L67-.L69
	.long	.L72-.L69
	.text
	.p2align 4,,10
	.p2align 3
.L60:
	orb	$4, 12(%rbx)
	leaq	8(%rbp), %rdx
	movl	4(%rbp), %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	8(%rsp), %rdi
	call	read_int
	cmpl	$-1, %eax
	je	.L111
	movl	%eax, 4(%rbx)
.L111:
	movq	8(%rsp), %rbp
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L118:
	movl	4(%rbp), %eax
	leaq	4(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	cmpl	$42, %eax
	je	.L120
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L121
	movl	$0, (%rbx)
	movq	%rdx, %rbp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L116:
	movl	4(%rbp), %eax
	leaq	4(%rbp), %r13
	movq	%r13, 8(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L34
.L41:
	movl	%r12d, 44(%rbx)
	movq	%r13, 8(%rsp)
	movq	%r13, %rbp
	addq	$1, %r12
	movl	$1, %r13d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rdi, %r13
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L18
	movq	8(%rsp), %rdx
	cmpl	$36, (%rdx)
	jne	.L18
	leaq	4(%rdx), %rbp
	cmpl	$-1, %eax
	movq	%rbp, 8(%rsp)
	je	.L109
	leal	-1(%rax), %ecx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%ecx, 48(%rbx)
	movq	%rax, (%r11)
.L109:
	movl	4(%rdx), %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rbp, 8(%rsp)
	movl	4(%r13), %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L62:
	orb	$4, 12(%rbx)
	leaq	8(%rbp), %rdx
	movl	4(%rbp), %ecx
	cmpl	$108, %ecx
	jne	.L57
.L58:
	orb	$1, 12(%rbx)
	addq	$4, %rdx
	movl	-4(%rdx), %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, 52(%rbx)
.L82:
	cmpl	$-1, %esi
	movq	8(%rsp), %rdi
	je	.L90
.L83:
	movq	%rdi, 24(%rbx)
	movl	$37, %esi
	call	__wcschrnul@PLT
	movq	%rax, 32(%rbx)
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$-1, %esi
	movl	$2048, 52(%rbx)
	movq	8(%rsp), %rdi
	jne	.L83
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$1, %edx
.L86:
	movl	%r12d, 48(%rbx)
	addq	%rdx, %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$5, 52(%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$3, 52(%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L68:
	movzbl	12(%rbx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	xorb	%al, %al
	addl	$263, %eax
	movl	%eax, 52(%rbx)
.L78:
	cmpl	$-1, %esi
	je	.L90
.L84:
	testl	%ecx, %ecx
	movq	8(%rsp), %rdi
	jne	.L83
	subq	$4, %rdi
	movq	%r13, %rax
	movq	%rdi, 32(%rbx)
	movq	%rdi, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$-1, %esi
	movl	$2, 52(%rbx)
	movq	8(%rsp), %rdi
	jne	.L83
	movl	%r12d, 48(%rbx)
	addq	$1, %r13
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$4, 52(%rbx)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	12(%rbx), %eax
	testb	$4, %al
	je	.L77
	movl	$512, 52(%rbx)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	8(%rsp), %rdi
	call	read_int
	cmpl	$-1, %eax
	je	.L113
	movl	%eax, (%rbx)
.L113:
	movq	8(%rsp), %rbp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L67:
	movq	$0, 56(%rbx)
	jmp	.L84
.L119:
	movq	__printf_arginfo_table(%rip), %rdx
	movslq	%ecx, %rax
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L65
	leaq	64(%rbx), %rcx
	leaq	52(%rbx), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	*%rax
	movslq	%eax, %rdx
	testl	%eax, %eax
	movl	8(%rbx), %ecx
	movq	%rdx, 56(%rbx)
	js	.L65
	cmpl	$-1, 48(%rbx)
	jne	.L84
	testq	%rdx, %rdx
	je	.L84
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L120:
	movl	8(%rbp), %eax
	leaq	8(%rbp), %r14
	movq	%r14, 8(%rsp)
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L45
.L51:
	movl	%r12d, 40(%rbx)
	addq	$1, %r13
	movq	%r14, 8(%rsp)
	movq	%r14, %rbp
	addq	$1, %r12
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L38
	movq	8(%rsp), %rcx
	cmpl	$36, (%rcx)
	je	.L122
.L38:
	movl	44(%rbx), %edx
.L37:
	testl	%edx, %edx
	js	.L41
	movq	8(%rsp), %rbp
	xorl	%r13d, %r13d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L54:
	movl	0(%rbp), %edx
	cmpq	$0, (%rax,%rdx,8)
	movq	%rdx, %rcx
	je	.L55
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	call	__handle_registered_modifier_wc
	testl	%eax, %eax
	je	.L123
	movq	8(%rsp), %rbp
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L77:
	testb	$2, %al
	je	.L79
	movl	$1024, 52(%rbx)
	jmp	.L78
.L79:
	xorl	%eax, %eax
	testb	$2, 13(%rbx)
	setne	%al
	movl	%eax, 52(%rbx)
	jmp	.L78
.L123:
	movq	8(%rsp), %rax
	leaq	4(%rax), %rdx
	movl	(%rax), %ecx
	jmp	.L57
.L122:
	cmpl	$-1, %eax
	je	.L124
	leal	-1(%rax), %edx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%edx, 44(%rbx)
	movq	%rax, (%r11)
.L40:
	addq	$4, %rcx
	movq	%rcx, 8(%rsp)
	jmp	.L37
.L63:
	orb	$2, 13(%rbx)
	leaq	12(%rbp), %rdx
	movl	8(%rbp), %ecx
	jmp	.L57
.L45:
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L48
	movq	8(%rsp), %rcx
	cmpl	$36, (%rcx)
	je	.L125
.L48:
	movl	40(%rbx), %edx
.L47:
	testl	%edx, %edx
	jns	.L113
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L125:
	cmpl	$-1, %eax
	je	.L126
	leal	-1(%rax), %edx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%edx, 40(%rbx)
	movq	%rax, (%r11)
.L50:
	addq	$4, %rcx
	movq	%rcx, 8(%rsp)
	jmp	.L47
.L124:
	movl	44(%rbx), %edx
	jmp	.L40
.L126:
	movl	40(%rbx), %edx
	jmp	.L50
	.size	__parse_one_specwc, .-__parse_one_specwc
	.hidden	__handle_registered_modifier_wc
	.hidden	__printf_arginfo_table
	.hidden	__printf_function_table
	.hidden	__printf_modifier_table
