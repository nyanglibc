	.text
	.p2align 4,,15
	.globl	__vfprintf
	.type	__vfprintf, @function
__vfprintf:
.LFB68:
	xorl	%ecx, %ecx
	jmp	__vfprintf_internal
.LFE68:
	.size	__vfprintf, .-__vfprintf
	.globl	vfprintf
	.hidden	vfprintf
	.set	vfprintf,__vfprintf
	.globl	_IO_vfprintf
	.set	_IO_vfprintf,__vfprintf
	.hidden	__vfprintf_internal
