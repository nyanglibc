	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___renameat
	.hidden	__GI___renameat
	.type	__GI___renameat, @function
__GI___renameat:
	movq	%rcx, %r10
	movl	$264, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/renameat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___renameat, .-__GI___renameat
	.globl	__renameat
	.set	__renameat,__GI___renameat
	.weak	renameat
	.set	renameat,__renameat
