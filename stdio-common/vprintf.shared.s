	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vprintf
	.type	__vprintf, @function
__vprintf:
.LFB45:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfprintf_internal
.LFE45:
	.size	__vprintf, .-__vprintf
	.globl	vprintf
	.set	vprintf,__vprintf
	.hidden	__vfprintf_internal
