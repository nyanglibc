	.text
	.hidden	__sys_siglist
	.globl	__sys_siglist
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Hangup"
.LC1:
	.string	"Interrupt"
.LC2:
	.string	"Quit"
.LC3:
	.string	"Illegal instruction"
.LC4:
	.string	"Trace/breakpoint trap"
.LC5:
	.string	"Aborted"
.LC6:
	.string	"Bus error"
.LC7:
	.string	"Floating point exception"
.LC8:
	.string	"Killed"
.LC9:
	.string	"User defined signal 1"
.LC10:
	.string	"Segmentation fault"
.LC11:
	.string	"User defined signal 2"
.LC12:
	.string	"Broken pipe"
.LC13:
	.string	"Alarm clock"
.LC14:
	.string	"Terminated"
.LC15:
	.string	"Stack fault"
.LC16:
	.string	"Child exited"
.LC17:
	.string	"Continued"
.LC18:
	.string	"Stopped (signal)"
.LC19:
	.string	"Stopped"
.LC20:
	.string	"Stopped (tty input)"
.LC21:
	.string	"Stopped (tty output)"
.LC22:
	.string	"Urgent I/O condition"
.LC23:
	.string	"CPU time limit exceeded"
.LC24:
	.string	"File size limit exceeded"
.LC25:
	.string	"Virtual timer expired"
.LC26:
	.string	"Profiling timer expired"
.LC27:
	.string	"Window changed"
.LC28:
	.string	"I/O possible"
.LC29:
	.string	"Power failure"
.LC30:
	.string	"Bad system call"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	__sys_siglist, @object
	.size	__sys_siglist, 520
__sys_siglist:
	.zero	8
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.zero	264
	.hidden	__sys_sigabbrev
	.globl	__sys_sigabbrev
	.section	.rodata.str1.1
.LC31:
	.string	"HUP"
.LC32:
	.string	"INT"
.LC33:
	.string	"QUIT"
.LC34:
	.string	"ILL"
.LC35:
	.string	"TRAP"
.LC36:
	.string	"ABRT"
.LC37:
	.string	"BUS"
.LC38:
	.string	"FPE"
.LC39:
	.string	"KILL"
.LC40:
	.string	"USR1"
.LC41:
	.string	"SEGV"
.LC42:
	.string	"USR2"
.LC43:
	.string	"PIPE"
.LC44:
	.string	"ALRM"
.LC45:
	.string	"TERM"
.LC46:
	.string	"STKFLT"
.LC47:
	.string	"CHLD"
.LC48:
	.string	"CONT"
.LC49:
	.string	"STOP"
.LC50:
	.string	"TSTP"
.LC51:
	.string	"TTIN"
.LC52:
	.string	"TTOU"
.LC53:
	.string	"URG"
.LC54:
	.string	"XCPU"
.LC55:
	.string	"XFSZ"
.LC56:
	.string	"VTALRM"
.LC57:
	.string	"PROF"
.LC58:
	.string	"WINCH"
.LC59:
	.string	"POLL"
.LC60:
	.string	"PWR"
.LC61:
	.string	"SYS"
	.section	.data.rel.ro.local
	.align 32
	.type	__sys_sigabbrev, @object
	.size	__sys_sigabbrev, 520
__sys_sigabbrev:
	.zero	8
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.zero	264
