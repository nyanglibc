	.text
	.hidden	_itoa_upper_digits
	.globl	_itoa_upper_digits
	.section	.rodata
	.align 32
	.type	_itoa_upper_digits, @object
	.size	_itoa_upper_digits, 36
_itoa_upper_digits:
	.ascii	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
