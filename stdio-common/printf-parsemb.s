	.text
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rcx
	movl	$-1, %esi
	movl	$2147483647, %r9d
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-48(%rdx), %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	leal	(%rax,%rax,4), %eax
	movl	%r9d, %r8d
	subl	%edx, %r8d
	addl	%eax, %eax
	addl	%eax, %edx
	cmpl	%eax, %r8d
	movl	%edx, %eax
	cmovl	%esi, %eax
.L3:
	addq	$1, %rcx
.L2:
	movq	%rcx, (%rdi)
	movzbl	(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L8
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jle	.L9
	movl	%esi, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	rep ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.globl	__parse_one_specmb
	.hidden	__parse_one_specmb
	.type	__parse_one_specmb, @function
__parse_one_specmb:
	pushq	%r13
	pushq	%r12
	leaq	1(%rdi), %r10
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rcx, %r11
	subq	$24, %rsp
	andb	$7, 12(%rdx)
	movl	$-1, 48(%rdx)
	movq	%r10, 8(%rsp)
	movzbl	13(%rdx), %eax
	movl	$32, 16(%rdx)
	andl	$-30, %eax
	movb	%al, 13(%rdx)
	movzbl	1(%rdi), %edx
	movl	%edx, %eax
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L107
.L11:
	leaq	.L17(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L14:
	subl	$32, %eax
	cmpb	$41, %al
	ja	.L15
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L16-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L18-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L19-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L20-.L17
	.long	.L15-.L17
	.long	.L21-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L22-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L15-.L17
	.long	.L23-.L17
	.text
	.p2align 4,,10
	.p2align 3
.L23:
	orb	$8, 13(%rbx)
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %r10
	movq	%r10, 8(%rsp)
	movzbl	(%r10), %eax
	testb	%al, %al
	jne	.L14
.L15:
	testb	$32, 12(%rbx)
	je	.L25
	movl	$32, 16(%rbx)
.L25:
	movl	$-1, 44(%rbx)
	movl	$0, 4(%rbx)
	movzbl	(%r10), %eax
	cmpb	$42, %al
	je	.L108
	subl	$48, %eax
	xorl	%r12d, %r12d
	cmpl	$9, %eax
	jbe	.L109
.L28:
	movl	$-1, 40(%rbx)
	movl	$-1, (%rbx)
	cmpb	$46, (%r10)
	je	.L110
.L36:
	xorl	%eax, %eax
	andb	$-3, 13(%rbx)
	andb	$-8, 12(%rbx)
	movw	%ax, 14(%rbx)
	movq	__printf_modifier_table(%rip), %rax
	testq	%rax, %rax
	jne	.L111
.L47:
	leaq	1(%r10), %rdx
	movq	%rdx, 8(%rsp)
	movzbl	(%r10), %eax
	subl	$76, %eax
	cmpb	$46, %al
	ja	.L81
	leaq	.L51(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L51:
	.long	.L50-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L52-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L53-.L51
	.long	.L81-.L51
	.long	.L52-.L51
	.long	.L81-.L51
	.long	.L54-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L50-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L52-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L81-.L51
	.long	.L52-.L51
	.text
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$48, 16(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	orb	$32, 12(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L20:
	orb	$64, 12(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L19:
	orb	$-128, 12(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L18:
	orb	$8, 12(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L16:
	orb	$16, 12(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L52:
	orb	$4, 12(%rbx)
.L106:
	leaq	2(%r10), %rax
.L49:
	movq	%rax, 8(%rsp)
	cmpq	$0, __printf_function_table(%rip)
	movzbl	(%rdx), %edx
	movl	$-1, 64(%rbx)
	movl	%edx, 8(%rbx)
	jne	.L112
.L57:
	leal	-65(%rdx), %eax
	movq	$1, 56(%rbx)
	movl	48(%rbx), %esi
	cmpl	$55, %eax
	ja	.L59
	leaq	.L61(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L61:
	.long	.L60-.L61
	.long	.L59-.L61
	.long	.L62-.L61
	.long	.L59-.L61
	.long	.L60-.L61
	.long	.L60-.L61
	.long	.L60-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L63-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L64-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L60-.L61
	.long	.L59-.L61
	.long	.L65-.L61
	.long	.L64-.L61
	.long	.L60-.L61
	.long	.L60-.L61
	.long	.L60-.L61
	.long	.L59-.L61
	.long	.L64-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L66-.L61
	.long	.L64-.L61
	.long	.L67-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L68-.L61
	.long	.L59-.L61
	.long	.L64-.L61
	.long	.L59-.L61
	.long	.L59-.L61
	.long	.L64-.L61
	.text
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	8(%rsp), %rdi
	call	read_int
	cmpl	$-1, %eax
	je	.L103
	movl	%eax, 4(%rbx)
.L103:
	movq	8(%rsp), %r10
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	1(%r10), %rdx
	movq	%rdx, 8(%rsp)
	movzbl	1(%r10), %eax
	cmpb	$42, %al
	je	.L113
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L114
	movl	$0, (%rbx)
	movq	%rdx, %r10
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	1(%r10), %r12
	movq	%r12, 8(%rsp)
	movzbl	1(%r10), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L27
.L34:
	movl	%ebp, 44(%rbx)
	movq	%r12, 8(%rsp)
	movq	%r12, %r10
	addq	$1, %rbp
	movl	$1, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rdi, %r12
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L12
	movq	8(%rsp), %rdx
	cmpb	$36, (%rdx)
	jne	.L12
	leaq	1(%rdx), %r10
	cmpl	$-1, %eax
	movq	%r10, 8(%rsp)
	je	.L102
	leal	-1(%rax), %ecx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%ecx, 48(%rbx)
	movq	%rax, (%r11)
.L102:
	movzbl	1(%rdx), %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r10, 8(%rsp)
	movzbl	1(%r12), %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L54:
	orb	$4, 12(%rbx)
	cmpb	$108, 1(%r10)
	jne	.L106
	leaq	2(%r10), %rdx
.L50:
	orb	$1, 12(%rbx)
	leaq	1(%rdx), %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	cmpb	$104, 1(%r10)
	je	.L55
	orb	$2, 12(%rbx)
	leaq	2(%r10), %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$3, 52(%rbx)
.L74:
	cmpl	$-1, %esi
	movq	8(%rsp), %rdi
	je	.L83
.L75:
	movq	%rdi, 24(%rbx)
	movl	$37, %esi
	call	__strchrnul@PLT
	movq	%rax, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$-1, %esi
	movl	$5, 52(%rbx)
	movq	8(%rsp), %rdi
	jne	.L75
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %ecx
.L78:
	movl	%ebp, 48(%rbx)
	addq	%rcx, %r12
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$2048, 52(%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$1, 52(%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L64:
	movzbl	12(%rbx), %eax
	testb	$4, %al
	je	.L69
	movl	$512, 52(%rbx)
.L70:
	cmpl	$-1, %esi
	je	.L83
.L76:
	testl	%edx, %edx
	movq	8(%rsp), %rdi
	jne	.L75
	subq	$1, %rdi
	movq	%r12, %rax
	movq	%rdi, 32(%rbx)
	movq	%rdi, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$4, 52(%rbx)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L60:
	movzbl	12(%rbx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	xorb	%al, %al
	addl	$263, %eax
	movl	%eax, 52(%rbx)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	$-1, %esi
	movl	$2, 52(%rbx)
	movq	8(%rsp), %rdi
	jne	.L75
	movl	%ebp, 48(%rbx)
	addq	$1, %r12
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	8(%rsp), %rdi
	call	read_int
	cmpl	$-1, %eax
	je	.L105
	movl	%eax, (%rbx)
.L105:
	movq	8(%rsp), %r10
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L81:
	movq	%rdx, %rax
	movq	%r10, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	movq	$0, 56(%rbx)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	2(%r10), %r13
	movq	%r13, 8(%rsp)
	movzbl	2(%r10), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L38
.L44:
	movl	%ebp, 40(%rbx)
	addq	$1, %r12
	movq	%r13, 8(%rsp)
	movq	%r13, %r10
	addq	$1, %rbp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L31
	movq	8(%rsp), %rcx
	cmpb	$36, (%rcx)
	je	.L115
.L31:
	movl	44(%rbx), %edx
.L30:
	testl	%edx, %edx
	js	.L34
	movq	8(%rsp), %r10
	xorl	%r12d, %r12d
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L112:
	movq	__printf_arginfo_table(%rip), %rcx
	movq	(%rcx,%rdx,8), %rax
	testq	%rax, %rax
	je	.L57
	leaq	64(%rbx), %rcx
	leaq	52(%rbx), %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	*%rax
	movslq	%eax, %rcx
	testl	%eax, %eax
	movl	8(%rbx), %edx
	movq	%rcx, 56(%rbx)
	js	.L57
	cmpl	$-1, 48(%rbx)
	jne	.L76
	testq	%rcx, %rcx
	je	.L76
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L111:
	movzbl	(%r10), %edx
	cmpq	$0, (%rax,%rdx,8)
	je	.L47
	leaq	8(%rsp), %rdi
	movq	%rbx, %rsi
	call	__handle_registered_modifier_mb
	testl	%eax, %eax
	je	.L116
	movq	8(%rsp), %r10
	jmp	.L47
.L69:
	testb	$2, %al
	je	.L71
	movl	$1024, 52(%rbx)
	jmp	.L70
.L116:
	movq	8(%rsp), %rdx
	leaq	1(%rdx), %rax
	jmp	.L49
.L71:
	xorl	%eax, %eax
	testb	$2, 13(%rbx)
	setne	%al
	movl	%eax, 52(%rbx)
	jmp	.L70
.L115:
	cmpl	$-1, %eax
	je	.L117
	leal	-1(%rax), %edx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%edx, 44(%rbx)
	movq	%rax, (%r11)
.L33:
	addq	$1, %rcx
	movq	%rcx, 8(%rsp)
	jmp	.L30
.L55:
	leaq	2(%r10), %rdx
	orb	$2, 13(%rbx)
	leaq	3(%r10), %rax
	jmp	.L49
.L38:
	leaq	8(%rsp), %rdi
	call	read_int
	testl	%eax, %eax
	je	.L41
	movq	8(%rsp), %rcx
	cmpb	$36, (%rcx)
	je	.L118
.L41:
	movl	40(%rbx), %edx
.L40:
	testl	%edx, %edx
	jns	.L105
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L118:
	cmpl	$-1, %eax
	je	.L119
	leal	-1(%rax), %edx
	movl	%eax, %eax
	cmpq	%rax, (%r11)
	cmovnb	(%r11), %rax
	movl	%edx, 40(%rbx)
	movq	%rax, (%r11)
.L43:
	addq	$1, %rcx
	movq	%rcx, 8(%rsp)
	jmp	.L40
.L117:
	movl	44(%rbx), %edx
	jmp	.L33
.L119:
	movl	40(%rbx), %edx
	jmp	.L43
	.size	__parse_one_specmb, .-__parse_one_specmb
	.hidden	__handle_registered_modifier_mb
	.hidden	__printf_arginfo_table
	.hidden	__printf_function_table
	.hidden	__printf_modifier_table
