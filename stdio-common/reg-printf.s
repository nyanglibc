	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__register_printf_specifier
	.hidden	__register_printf_specifier
	.type	__register_printf_specifier, @function
__register_printf_specifier:
	cmpl	$255, %edi
	ja	.L16
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
#APP
# 52 "reg-printf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, lock(%rip)
# 0 "" 2
#NO_APP
.L5:
	movq	__printf_function_table(%rip), %rcx
	testq	%rcx, %rcx
	je	.L6
	movq	__printf_arginfo_table(%rip), %rax
.L7:
	movslq	%ebx, %rdi
	xorl	%r8d, %r8d
	movq	%rbp, (%rcx,%rdi,8)
	movq	%rdx, (%rax,%rdi,8)
.L8:
#APP
# 72 "reg-printf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	subl	$1, lock(%rip)
.L1:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$16, %esi
	movl	$256, %edi
	movq	%rdx, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, __printf_arginfo_table(%rip)
	movl	$-1, %r8d
	je	.L8
	leaq	2048(%rax), %rcx
	movq	8(%rsp), %rdx
	movq	%rcx, __printf_function_table(%rip)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
#APP
# 72 "reg-printf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 72 "reg-printf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, lock(%rip)
	je	.L5
	leaq	lock(%rip), %rdi
	movq	%rdx, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	movl	%r8d, %eax
	ret
	.size	__register_printf_specifier, .-__register_printf_specifier
	.weak	register_printf_specifier
	.set	register_printf_specifier,__register_printf_specifier
	.p2align 4,,15
	.globl	__register_printf_function
	.type	__register_printf_function, @function
__register_printf_function:
	jmp	__register_printf_specifier
	.size	__register_printf_function, .-__register_printf_function
	.weak	register_printf_function
	.set	register_printf_function,__register_printf_function
	.local	lock
	.comm	lock,4,4
	.hidden	__printf_function_table
	.comm	__printf_function_table,8,8
	.hidden	__printf_arginfo_table
	.globl	__printf_arginfo_table
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	__printf_arginfo_table, @object
	.size	__printf_arginfo_table, 8
__printf_arginfo_table:
	.zero	8
	.hidden	__lll_lock_wait_private
