	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___vfscanf
	.type	___vfscanf, @function
___vfscanf:
.LFB68:
	xorl	%ecx, %ecx
	jmp	__vfscanf_internal
.LFE68:
	.size	___vfscanf, .-___vfscanf
	.weak	vfscanf
	.set	vfscanf,___vfscanf
	.globl	__GI___vfscanf
	.hidden	__GI___vfscanf
	.set	__GI___vfscanf,___vfscanf
	.globl	__vfscanf
	.set	__vfscanf,__GI___vfscanf
	.hidden	__vfscanf_internal
