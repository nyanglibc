	.text
	.p2align 4,,15
	.type	read_int, @function
read_int:
	movq	(%rdi), %rcx
	movl	$-1, %esi
	movl	$2147483647, %r9d
	movzbl	(%rcx), %edx
	addq	$1, %rcx
	leal	-48(%rdx), %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	leal	(%rax,%rax,4), %eax
	movl	%r9d, %r8d
	subl	%edx, %r8d
	addl	%eax, %eax
	addl	%eax, %edx
	cmpl	%eax, %r8d
	movl	%edx, %eax
	cmovl	%esi, %eax
.L3:
	addq	$1, %rcx
.L2:
	movq	%rcx, (%rdi)
	movzbl	(%rcx), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L8
	testl	%eax, %eax
	js	.L3
	cmpl	$214748364, %eax
	jle	.L9
	movl	%esi, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	rep ret
	.size	read_int, .-read_int
	.p2align 4,,15
	.type	char_buffer_add_slow, @function
char_buffer_add_slow:
	cmpq	$0, (%rdi)
	je	.L14
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	subq	16(%rdi), %rax
	leaq	16(%rdi), %rdi
	movq	%rax, %r12
	call	__libc_scratch_buffer_grow_preserve
	testb	%al, %al
	je	.L17
	movq	16(%rbx), %rax
	movq	24(%rbx), %rdx
	addq	%rax, %rdx
	addq	%r12, %rax
	movq	%rdx, 8(%rbx)
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movb	%bpl, (%rax)
.L10:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	jmp	.L10
	.size	char_buffer_add_slow, .-char_buffer_add_slow
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_inpunct"
.LC1:
	.string	"vfscanf-internal.c"
.LC2:
	.string	"cnt < MB_LEN_MAX"
	.text
	.p2align 4,,15
	.globl	__vfscanf_internal
	.hidden	__vfscanf_internal
	.type	__vfscanf_internal, @function
__vfscanf_internal:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1672, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%rsi, -1504(%rbp)
	movq	%fs:(%rax), %rax
	movq	%rax, -1520(%rbp)
	movq	(%rax), %rax
	movq	$1024, -1080(%rbp)
	movq	%rax, -1608(%rbp)
	leaq	-1104(%rbp), %rax
	movq	%rax, -1568(%rbp)
	addq	$32, %rax
	movq	%rax, -1088(%rbp)
	movdqu	(%rdx), %xmm0
	movups	%xmm0, -1448(%rbp)
	movq	16(%rdx), %rax
	movq	%rax, -1432(%rbp)
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L19
	movl	$-1, 192(%rdi)
.L20:
	movl	(%rdi), %eax
	testb	$4, %al
	jne	.L1909
	testq	%rsi, %rsi
	je	.L1910
	movq	-1520(%rbp), %rsi
	movq	%rdx, -1664(%rbp)
	movq	%rdi, %r13
	movl	%ecx, -1672(%rbp)
	movq	8(%rsi), %rdx
	movq	72(%rdx), %rsi
	movq	64(%rdx), %rdi
	movl	$0, %edx
	cmpb	$0, (%rsi)
	movq	%rdi, -1680(%rbp)
	cmovne	%rsi, %rdx
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	movq	%rdx, -1560(%rbp)
	je	.L25
	movq	_IO_funlockfile@GOTPCREL(%rip), %rsi
	leaq	-1424(%rbp), %rdi
	movq	%r13, %rdx
	call	_pthread_cleanup_push_defer@PLT
	movl	0(%r13), %eax
.L26:
	andl	$32768, %eax
	je	.L1911
.L27:
	movq	-1504(%rbp), %rbx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	movq	%r13, %rax
	xorl	%r9d, %r9d
	movl	%r15d, %r13d
	movq	$0, -1488(%rbp)
	movq	%r14, %r15
	movl	$0, -1512(%rbp)
	movl	$0, -1524(%rbp)
	movq	$0, -1640(%rbp)
	movl	%r9d, %r12d
	movq	$0, -1584(%rbp)
	movq	$0, -1624(%rbp)
	movq	%rax, %r14
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
.L31:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L1912
	testb	$-128, %al
	jne	.L1913
.L32:
	leaq	1(%rbx), %rax
	movq	%rax, -1504(%rbp)
	movzbl	(%rbx), %esi
	cmpb	$37, %sil
	je	.L43
	movq	-1520(%rbp), %rdi
	movzbl	%sil, %edx
	movq	104(%rdi), %rcx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L1078
	cmpl	$-1, %r13d
	je	.L1914
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1915
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L47:
	addq	$1, %r15
	testl	%r12d, %r12d
	jne	.L1916
.L49:
	movzbl	%sil, %r13d
	cmpl	%eax, %r13d
	jne	.L53
.L1873:
	movq	-1504(%rbp), %rbx
	xorl	%r12d, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L1913:
	movq	%rbx, %rdi
	call	strlen
	leaq	-1488(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	__mbrlen
	testl	%eax, %eax
	jg	.L33
	movq	-1504(%rbp), %rbx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L43:
	movq	-1088(%rbp), %rdx
	movl	$0, -1668(%rbp)
	movq	%rdx, -1104(%rbp)
	addq	-1080(%rbp), %rdx
	movq	%rdx, -1096(%rbp)
	movzbl	1(%rbx), %edx
	movl	%edx, %ecx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L1917
.L54:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L60:
	leal	-39(%rcx), %edx
	cmpb	$34, %dl
	jbe	.L1918
.L61:
	subl	$48, %ecx
	movl	%esi, -1528(%rbp)
	movl	$-1, -1544(%rbp)
	cmpl	$9, %ecx
	jbe	.L1919
.L63:
	leaq	1(%rax), %rcx
	movq	%rcx, -1504(%rbp)
	movzbl	(%rax), %esi
	leal	-76(%rsi), %edx
	cmpb	$46, %dl
	ja	.L64
	leaq	.L66(%rip), %rsi
	movzbl	%dl, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L66:
	.long	.L65-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L67-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L68-.L66
	.long	.L64-.L66
	.long	.L69-.L66
	.long	.L64-.L66
	.long	.L70-.L66
	.long	.L71-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L65-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L69-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L64-.L66
	.long	.L69-.L66
	.text
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$-1, %eax
	je	.L20
.L1875:
	movl	$-1, -1524(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L1911:
	movq	136(%r13), %rdi
	movq	%fs:16, %rbx
	cmpq	%rbx, 8(%rdi)
	je	.L28
#APP
# 372 "vfscanf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L29
	movl	%edx, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L30:
	movq	136(%r13), %rdi
	movq	%rbx, 8(%rdi)
.L28:
	addl	$1, 4(%rdi)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	$-1, %r13d
	je	.L34
	subl	$1, %eax
	leaq	1(%r15,%rax), %rcx
.L35:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1920
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L38:
	movq	-1504(%rbp), %rdx
	addq	$1, %r15
	leaq	1(%rdx), %rbx
	movq	%rbx, -1504(%rbp)
	movzbl	(%rdx), %edx
	cmpl	%eax, %edx
	jne	.L1921
	cmpq	%rcx, %r15
	jne	.L35
	movl	%eax, %r13d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L1917:
	leaq	-1504(%rbp), %rdi
	call	read_int
	movl	%eax, -1544(%rbp)
	movq	-1504(%rbp), %rax
	movl	$0, -1528(%rbp)
	cmpb	$36, (%rax)
	je	.L1922
.L55:
	movl	-1544(%rbp), %edi
	movl	$-1, %edx
	testl	%edi, %edi
	cmovne	%edi, %edx
	movl	%edx, -1544(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L69:
	orl	$1, -1528(%rbp)
	movzbl	1(%rax), %edx
.L73:
	testb	%dl, %dl
	je	.L1763
	leaq	1(%rcx), %rax
	testl	%r12d, %r12d
	movq	%rax, -1504(%rbp)
	movzbl	(%rcx), %ecx
	jne	.L78
	leal	-67(%rcx), %eax
	cmpb	$43, %al
	jbe	.L1923
.L78:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-2, %r8
	movl	$-1, %r12d
	movl	-1512(%rbp), %r9d
	movq	-1520(%rbp), %rbx
	movl	%fs:(%rax), %esi
	movq	%rax, -1536(%rbp)
	movl	$0, %fs:(%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L80:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1924
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L83:
	movq	104(%rbx), %rax
	movslq	%r13d, %rdx
	addq	$1, %r15
	addq	%rdx, %rdx
	movzwl	(%rax,%rdx), %eax
	testb	$32, %ah
	je	.L1925
.L86:
	cmpl	$-1, %r13d
	jne	.L80
	movq	-1536(%rbp), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L1926
.L1084:
	movq	104(%rbx), %rax
	movq	%r8, %rdx
	movl	%r12d, %r13d
	movzwl	(%rax,%rdx), %eax
	testb	$32, %ah
	jne	.L86
.L1925:
	movq	-1536(%rbp), %rax
	cmpl	$-1, %r13d
	movl	%r9d, -1512(%rbp)
	movl	%esi, %fs:(%rax)
	je	.L87
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	movb	%cl, -1552(%rbp)
	call	_IO_sputbackc
	movzbl	-1552(%rbp), %ecx
	leaq	-1(%r15), %r12
	leal	-37(%rcx), %eax
	cmpb	$83, %al
	ja	.L1764
	leaq	.L89(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L89:
	.long	.L88-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1086-.L89
	.long	.L1765-.L89
	.long	.L1087-.L89
	.long	.L1765-.L89
	.long	.L1086-.L89
	.long	.L1086-.L89
	.long	.L1086-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L92-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1861-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1089-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1086-.L89
	.long	.L1765-.L89
	.long	.L1090-.L89
	.long	.L1091-.L89
	.long	.L1086-.L89
	.long	.L1086-.L89
	.long	.L1086-.L89
	.long	.L1765-.L89
	.long	.L1092-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1093-.L89
	.long	.L99-.L89
	.long	.L100-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1094-.L89
	.long	.L1765-.L89
	.long	.L1095-.L89
	.long	.L1765-.L89
	.long	.L1765-.L89
	.long	.L1861-.L89
	.text
	.p2align 4,,10
	.p2align 3
.L1918:
	movabsq	$17179869193, %rdi
	btq	%rdx, %rdi
	jnc	.L61
	addq	$1, %rax
	movq	%rax, -1504(%rbp)
	movzbl	-1(%rax), %edx
	cmpb	$42, %dl
	je	.L57
	cmpb	$73, %dl
	je	.L58
	cmpb	$39, %dl
	jne	.L56
	movl	%esi, %edx
	orb	$-128, %dl
	cmpq	$0, -1560(%rbp)
	cmovne	%edx, %esi
.L56:
	movzbl	(%rax), %ecx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L58:
	orl	$1024, %esi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	orl	$8, %esi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L25:
	movq	_IO_funlockfile@GOTPCREL(%rip), %rdx
	movq	%r13, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%rax, %rbx
	movl	$1, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movq	%r14, %r13
	movl	%edi, %fs:(%rax)
.L36:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	.p2align 4,,10
	.p2align 3
.L41:
	testl	$32768, 0(%r13)
	jne	.L1006
	movq	136(%r13), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1006
	movq	$0, 8(%rdi)
#APP
# 3030 "vfscanf-internal.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L1008
	subl	$1, (%rdi)
.L1006:
	cmpq	$0, _pthread_cleanup_push_defer@GOTPCREL(%rip)
	je	.L1009
	leaq	-1424(%rbp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L1009:
	movq	-1568(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$32, %rax
	cmpq	%rax, %rdi
	je	.L1010
	call	free@PLT
.L1010:
	cmpl	$-1, -1524(%rbp)
	je	.L1927
	cmpq	$0, -1584(%rbp)
	jne	.L1928
.L18:
	movl	-1524(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1914:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movq	%r14, %r13
	movl	%esi, %fs:(%rax)
.L45:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1916:
	movq	-1520(%rbp), %rdi
	movl	%esi, %ebx
	movq	104(%rdi), %rcx
.L48:
	movslq	%eax, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L1929
.L52:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1930
	leaq	1(%rax), %rdx
	addq	$1, %r15
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
	movslq	%eax, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L52
.L1929:
	movl	%ebx, %esi
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L1923:
	movabsq	$8800404766721, %rdx
	btq	%rax, %rdx
	jnc	.L78
	leaq	.L103(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L103:
	.long	.L91-.L103
	.long	.L1766-.L103
	.long	.L90-.L103
	.long	.L90-.L103
	.long	.L90-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L104-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1096-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L94-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L90-.L103
	.long	.L1766-.L103
	.long	.L95-.L103
	.long	.L96-.L103
	.long	.L90-.L103
	.long	.L90-.L103
	.long	.L90-.L103
	.long	.L1766-.L103
	.long	.L97-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L1766-.L103
	.long	.L98-.L103
	.text
	.p2align 4,,10
	.p2align 3
.L71:
	movzbl	1(%rax), %edx
	cmpb	$108, %dl
	je	.L77
	orl	$8192, -1528(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L70:
	movzbl	1(%rax), %edx
	cmpb	$108, %dl
	je	.L1931
	orl	$1, -1528(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L68:
	movzbl	1(%rax), %edx
	cmpb	$104, %dl
	je	.L1932
	orl	$4, -1528(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L67:
	movzbl	1(%rax), %edx
	movl	%edx, %esi
	andl	$-9, %esi
	cmpb	$83, %sil
	je	.L75
	cmpb	$115, %dl
	jne	.L64
.L75:
	testb	$2, -1672(%rbp)
	jne	.L64
	orl	$256, -1528(%rbp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L65:
	orl	$3, -1528(%rbp)
	movzbl	1(%rax), %edx
	jmp	.L73
.L1915:
	movq	%r14, %rdi
	movb	%sil, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movzbl	-1536(%rbp), %esi
	jne	.L47
	movq	%r14, %r13
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rax, -1504(%rbp)
	movq	%rax, %rcx
	movzbl	(%rax), %edx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L1919:
	leaq	-1504(%rbp), %rdi
	call	read_int
	movl	%eax, -1544(%rbp)
	movq	-1504(%rbp), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L1924:
	movq	%r14, %rdi
	movl	%esi, -1592(%rbp)
	movb	%cl, -1576(%rbp)
	movl	%r9d, -1552(%rbp)
	movq	%r8, -1512(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movq	-1512(%rbp), %r8
	movl	-1552(%rbp), %r9d
	movzbl	-1576(%rbp), %ecx
	movl	-1592(%rbp), %esi
	jne	.L83
	movq	-1536(%rbp), %rax
	movl	%fs:(%rax), %r9d
	movl	%r9d, %eax
	cmpl	$4, %eax
	jne	.L1084
.L1926:
	movl	-1524(%rbp), %esi
	movl	$-1, %eax
	movq	%r14, %r13
	testl	%esi, %esi
	cmovne	%esi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1927:
	cmpq	$0, -1648(%rbp)
	movq	-1648(%rbp), %r12
	je	.L1875
.L1012:
	xorl	%ebx, %ebx
	cmpq	$0, (%r12)
	je	.L1015
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	16(%r12,%rbx,8), %rax
	movq	(%rax), %rdi
	call	free@PLT
	movq	16(%r12,%rbx,8), %rax
	addq	$1, %rbx
	cmpq	%rbx, (%r12)
	movq	$0, (%rax)
	ja	.L1014
.L1015:
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.L1012
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L51
	movl	-1524(%rbp), %esi
	movq	%r14, %r13
	testl	%esi, %esi
	cmovne	%esi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1928:
	movq	-1584(%rbp), %rbx
	movq	(%rbx), %rdi
	call	free@PLT
	movq	$0, (%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L87:
	leal	-37(%rcx), %eax
	cmpb	$83, %al
	ja	.L1847
	leaq	.L1016(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1016:
	.long	.L106-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1849-.L1016
	.long	.L1848-.L1016
	.long	.L91-.L1016
	.long	.L1848-.L1016
	.long	.L1849-.L1016
	.long	.L1849-.L1016
	.long	.L1849-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1017-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1850-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L94-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1849-.L1016
	.long	.L1848-.L1016
	.long	.L95-.L1016
	.long	.L1850-.L1016
	.long	.L1849-.L1016
	.long	.L1849-.L1016
	.long	.L1849-.L1016
	.long	.L1848-.L1016
	.long	.L97-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L98-.L1016
	.long	.L1850-.L1016
	.long	.L1162-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L101-.L1016
	.long	.L1848-.L1016
	.long	.L1850-.L1016
	.long	.L1848-.L1016
	.long	.L1848-.L1016
	.long	.L1850-.L1016
	.text
	.p2align 4,,10
	.p2align 3
.L1920:
	movq	%r14, %rdi
	movq	%rcx, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movq	-1536(%rbp), %rcx
	jne	.L38
	movq	%r14, %r13
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L1922:
	movl	-1544(%rbp), %edi
	leaq	1(%rax), %rdx
	movq	%rdx, -1504(%rbp)
	movzbl	1(%rax), %ecx
	movq	%rdx, %rax
	movl	%edi, -1668(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	%r14, %r13
	jmp	.L41
.L53:
	movzbl	%al, %esi
	movq	%r14, %rdi
	movq	%r14, %r13
	call	_IO_sputbackc
	jmp	.L41
.L1087:
	movq	%r12, %r15
.L91:
	movl	-1544(%rbp), %esi
	movl	$1, %eax
	cmpl	$-1, %esi
	cmovne	%esi, %eax
	movl	%eax, -1544(%rbp)
	movl	-1528(%rbp), %eax
	movl	%eax, %r12d
	andl	$8, %r12d
	jne	.L216
	testb	$33, %ah
	je	.L217
	movl	-1668(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L218
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L219
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L220:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L221:
	cmpq	$0, -1584(%rbp)
	je	.L1771
	movslq	-1544(%rbp), %rax
	movl	$1024, %ebx
	cmpl	$1024, %eax
	cmovle	%rax, %rbx
	leaq	0(,%rbx,4), %rdi
	call	malloc@PLT
	movq	-1584(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, -1624(%rbp)
	movq	%rax, (%rdi)
	je	.L230
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L231
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L231
.L232:
	movq	-1648(%rbp), %rsi
	movq	-1584(%rbp), %rdi
	movq	%rbx, -1640(%rbp)
	movq	%rdx, (%rsi)
	movq	%rdi, 16(%rsi,%rax,8)
.L216:
	cmpl	$-1, %r13d
	je	.L1933
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1934
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L248:
	movslq	-1544(%rbp), %rdi
	movl	-1528(%rbp), %esi
	leaq	-1312(%rbp), %rbx
	movl	%r12d, -1544(%rbp)
	addq	$1, %r15
	movq	$0, -1312(%rbp)
	movq	%rbx, %r12
	andl	$8448, %esi
	movq	%rdi, %rax
	movq	%rdi, -1552(%rbp)
	movl	%esi, -1576(%rbp)
	subl	$1, %eax
	subl	%edi, %eax
	movl	%eax, -1592(%rbp)
	leaq	-1392(%rbp), %rax
	movq	%rax, -1536(%rbp)
.L249:
	movl	-1544(%rbp), %r9d
	movb	%r13b, -1392(%rbp)
	testl	%r9d, %r9d
	jne	.L250
	movl	-1576(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L250
	movq	-1584(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1640(%rbp), %rax
	leaq	0(,%rax,4), %rdx
	leaq	(%rdi,%rdx), %rax
	cmpq	%rax, -1624(%rbp)
	je	.L1935
.L250:
	movl	-1544(%rbp), %edi
	movl	$0, %ebx
	testl	%edi, %edi
	cmove	-1624(%rbp), %rbx
.L259:
	movq	-1536(%rbp), %rsi
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	__mbrtowc
	cmpq	$-2, %rax
	jne	.L1936
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1937
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L1859:
	addq	$1, %r15
	movb	%r13b, -1392(%rbp)
	jmp	.L259
.L1096:
	movq	%r15, %r12
.L1861:
	movl	$16, %r8d
.L93:
	cmpl	$-1, %r13d
	je	.L1938
.L372:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1939
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L376:
	leal	-43(%r13), %eax
	leaq	1(%r12), %r15
	movl	$1, %ebx
	andl	$-3, %eax
	jne	.L378
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1940
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	%r13b, (%rax)
.L380:
	movl	-1544(%rbp), %esi
	xorl	%edx, %edx
	movq	8(%r14), %rax
	testl	%esi, %esi
	setg	%dl
	subl	%edx, %esi
	setne	%bl
	cmpq	16(%r14), %rax
	movl	%esi, -1544(%rbp)
	jnb	.L1941
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L383:
	leaq	2(%r12), %r15
.L378:
	cmpl	$48, %r13d
	jne	.L385
	testb	%bl, %bl
	je	.L385
	movl	-1544(%rbp), %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	setg	%al
	subl	%eax, %edi
	movq	-1104(%rbp), %rax
	cmpq	%rax, -1096(%rbp)
	movl	%edi, -1544(%rbp)
	je	.L1942
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	$48, (%rax)
.L388:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1943
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L389:
	addq	$1, %r15
.L391:
	movl	-1544(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L392
	movq	-1520(%rbp), %rdi
	movzbl	%r13b, %eax
	movq	112(%rdi), %rdx
	cmpl	$120, (%rdx,%rax,4)
	je	.L1944
.L392:
	testl	%r8d, %r8d
	je	.L1945
.L393:
	cmpl	$10, %r8d
	jne	.L400
.L401:
	testl	$1024, -1528(%rbp)
	movl	$10, %r8d
	jne	.L1946
.L400:
	movl	-1528(%rbp), %eax
	movl	-1544(%rbp), %ebx
	movq	%r15, %r9
	movq	%r14, %r15
	andl	$128, %eax
	movl	%eax, -1592(%rbp)
	.p2align 4,,10
	.p2align 3
.L504:
	movq	-1104(%rbp), %rcx
	cmpl	$-1, %r13d
	movq	%rcx, %rax
	je	.L1790
.L1951:
	testl	%ebx, %ebx
	je	.L1790
	cmpl	$16, %r8d
	je	.L1947
	leal	-48(%r13), %esi
	cmpl	$9, %esi
	ja	.L478
	leal	-47(%r13), %eax
	cmpl	%r8d, %eax
	jg	.L1790
	movl	%r13d, %r10d
	movl	%r13d, %r11d
.L477:
	cmpq	%rcx, -1096(%rbp)
	je	.L1948
.L496:
	leaq	1(%rcx), %rax
	movq	%rax, -1104(%rbp)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	setg	%al
	movb	%r10b, (%rcx)
	subl	%eax, %ebx
	cmpl	$-1, %r13d
	je	.L1949
.L499:
	movq	8(%r15), %rax
	cmpq	16(%r15), %rax
	jnb	.L1950
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r15)
	movzbl	(%rax), %r13d
.L502:
	movq	-1104(%rbp), %rcx
	addq	$1, %r9
	cmpl	$-1, %r13d
	movq	%rcx, %rax
	jne	.L1951
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	%r15, %r14
	movl	%ebx, -1544(%rbp)
	movq	%r9, %r15
.L475:
	testq	%rcx, %rcx
	je	.L1952
.L507:
	movq	-1088(%rbp), %rax
	cmpq	%rax, %rcx
	je	.L508
	subq	%rax, %rcx
	cmpq	$1, %rcx
	je	.L1953
.L509:
	cmpl	$-1, %r13d
	je	.L530
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	_IO_sputbackc
	movl	-1536(%rbp), %r8d
	subq	$1, %r15
.L530:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1954
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	$0, (%rax)
.L532:
	cmpq	$0, -1104(%rbp)
	je	.L1955
	movl	-1528(%rbp), %eax
	movq	-1088(%rbp), %rdi
	leaq	-1496(%rbp), %rsi
	movl	%r8d, %edx
	movl	%eax, %ecx
	andl	$128, %ecx
	andl	$64, %eax
	movl	%eax, %ebx
	je	.L534
	call	__strtol_internal
.L535:
	movq	-1088(%rbp), %rdi
	cmpq	%rdi, -1496(%rbp)
	je	.L1798
	movl	-1528(%rbp), %r12d
	andl	$8, %r12d
	jne	.L1873
	movl	-1528(%rbp), %edx
	andl	$1, %edx
	testl	%ebx, %ebx
	je	.L537
	testl	%edx, %edx
	je	.L538
	movl	-1668(%rbp), %esi
	testl	%esi, %esi
	jne	.L539
.L1886:
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L592
	movl	%ecx, %edx
	addq	-1432(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L593:
	movq	(%rdx), %rdx
.L594:
	movq	%rax, (%rdx)
.L551:
	addl	$1, -1524(%rbp)
.L1874:
	movq	-1504(%rbp), %rbx
	jmp	.L31
.L1093:
	movq	%r12, %r15
.L98:
	movl	-1528(%rbp), %r12d
	andl	$8, %r12d
	jne	.L1873
	testb	$1, -1528(%rbp)
	je	.L112
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L113
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L114
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L115:
	movq	(%rax), %rax
.L116:
	movq	%r15, (%rax)
	movq	-1504(%rbp), %rbx
	jmp	.L31
.L1089:
	movq	%r12, %r15
.L94:
	movl	-1528(%rbp), %eax
	movl	%eax, %edi
	movl	%eax, %r12d
	andl	$8, %edi
	andl	$1, %r12d
	movl	%edi, -1552(%rbp)
	je	.L888
	testl	%edi, %edi
	jne	.L889
	testb	$33, %ah
	je	.L890
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L891
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L892
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L893:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L894:
	movq	-1584(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1837
	movl	$400, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1624(%rbp)
	movq	%rax, (%rbx)
	je	.L903
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L904
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L904
.L905:
	movq	-1648(%rbp), %rsi
	movq	-1584(%rbp), %rdi
	movq	$100, -1640(%rbp)
	movq	%rdx, (%rsi)
	movq	%rdi, 16(%rsi,%rax,8)
.L889:
	movq	-1504(%rbp), %rax
	xorl	%ebx, %ebx
	cmpb	$94, (%rax)
	jne	.L946
	addq	$1, %rax
	movl	$1, %ebx
	movq	%rax, -1504(%rbp)
.L946:
	movl	-1544(%rbp), %esi
	movl	$2147483647, %eax
	movl	$1, %edx
	testl	%esi, %esi
	cmovns	%esi, %eax
	movl	$256, %esi
	movl	%eax, -1544(%rbp)
	movq	-1568(%rbp), %rax
	leaq	16(%rax), %rdi
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L1151
	movq	-1088(%rbp), %rax
	leaq	8(%rax), %rdi
	movq	$0, (%rax)
	movq	$0, 248(%rax)
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	256(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	movq	-1504(%rbp), %rdx
	movzbl	(%rdx), %eax
	cmpb	$93, %al
	je	.L1168
	cmpb	$45, %al
	jne	.L950
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L951:
	movq	-1088(%rbp), %rdx
	movb	$1, (%rdx,%rax)
	movq	-1504(%rbp), %rdx
.L950:
	leaq	1(%rdx), %rcx
	movq	%rcx, -1504(%rbp)
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L1841
	cmpb	$93, %al
	je	.L1956
	cmpb	$45, %al
	jne	.L951
	movzbl	1(%rdx), %edi
	testb	%dil, %dil
	je	.L951
	cmpb	$93, %dil
	je	.L951
	movzbl	-1(%rdx), %esi
	cmpb	%sil, %dil
	jb	.L951
	jbe	.L1152
.L953:
	movq	-1088(%rbp), %rdx
	movzbl	%sil, %eax
	addl	$1, %esi
	movb	$1, (%rdx,%rax)
	movq	-1504(%rbp), %rdx
	cmpb	%sil, (%rdx)
	ja	.L953
	jmp	.L950
.L1092:
	movq	%r12, %r15
.L97:
	orl	$64, -1528(%rbp)
	movq	%r15, %r12
	xorl	%r8d, %r8d
	jmp	.L93
.L1090:
	movq	%r12, %r15
.L95:
	movl	-1528(%rbp), %edi
	movl	%edi, %r12d
	andl	$1, %r12d
	jne	.L91
	movl	-1544(%rbp), %esi
	movl	$1, %eax
	movl	%edi, %ebx
	cmpl	$-1, %esi
	cmovne	%esi, %eax
	andl	$8, %ebx
	movl	%eax, -1544(%rbp)
	jne	.L164
	andl	$8448, %edi
	je	.L165
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L166
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L167
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L168:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L169:
	cmpq	$0, -1584(%rbp)
	je	.L1768
	movslq	-1544(%rbp), %rsi
	movl	$1024, %eax
	cmpl	$1024, %esi
	cmovle	%rsi, %rax
	movq	%rax, %rdi
	movq	%rax, -1640(%rbp)
	call	malloc@PLT
	movq	-1584(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, -1656(%rbp)
	movq	%rax, (%rdi)
	je	.L178
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L179
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L179
.L180:
	movq	-1648(%rbp), %rdi
	movq	-1584(%rbp), %rsi
	movq	%rdx, (%rdi)
	movq	%rsi, 16(%rdi,%rax,8)
.L164:
	cmpl	$-1, %r13d
	je	.L1957
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1958
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L196:
	leaq	1(%r15), %rax
	testl	%ebx, %ebx
	movq	%rax, -1536(%rbp)
	jne	.L1959
	movl	-1528(%rbp), %r12d
	movslq	-1544(%rbp), %rax
	movq	-1536(%rbp), %r15
	andl	$8448, %r12d
	movl	%r12d, %r8d
	movq	%rax, %rbx
	movq	%rax, -1544(%rbp)
	testl	%r8d, %r8d
	movl	%ebx, %r12d
	movq	-1656(%rbp), %rax
	movq	%r15, %rbx
	movl	%r13d, %r15d
	movq	-1640(%rbp), %r13
	je	.L199
.L1963:
	movq	-1584(%rbp), %rdi
	movq	%r13, %rcx
	movq	(%rdi), %rdi
	leaq	(%rdi,%r13), %rsi
	cmpq	%rsi, %rax
	je	.L1960
.L199:
	subl	$1, %r12d
	leaq	1(%rax), %rcx
	movb	%r15b, (%rax)
	testl	%r12d, %r12d
	jle	.L1961
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1962
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r14)
	movzbl	(%rax), %r15d
.L203:
	addq	$1, %rbx
	testl	%r8d, %r8d
	movq	%rcx, %rax
	je	.L199
	jmp	.L1963
.L1091:
	movq	%r12, %r15
.L96:
	orl	$64, -1528(%rbp)
	movq	%r15, %r12
	movl	$10, %r8d
	jmp	.L93
.L1086:
	movq	%r12, %r15
.L90:
	cmpl	$-1, %r13d
	je	.L1964
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1965
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L1866:
	movl	-1544(%rbp), %edi
	leaq	1(%r15), %r12
	testl	%edi, %edi
	jg	.L1966
.L1061:
	leal	-43(%r13), %eax
	movq	-1104(%rbp), %rdx
	andl	$-3, %eax
	jne	.L1126
	cmpq	%rdx, -1096(%rbp)
	je	.L1967
	leaq	1(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movb	%r13b, (%rdx)
.L649:
	movl	-1544(%rbp), %esi
	testl	%esi, %esi
	je	.L1800
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1968
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L651:
	movl	-1544(%rbp), %edi
	xorl	%eax, %eax
	addq	$1, %r12
	movq	-1104(%rbp), %rdx
	movb	$1, -1552(%rbp)
	testl	%edi, %edi
	setg	%al
	subl	%eax, %edi
	movl	%edi, -1544(%rbp)
.L647:
	movq	-1520(%rbp), %rdi
	movzbl	%r13b, %eax
	movq	112(%rdi), %rcx
	movl	(%rcx,%rax,4), %eax
	cmpl	$110, %eax
	je	.L1969
	cmpl	$105, %eax
	jne	.L664
	cmpq	%rdx, -1096(%rbp)
	je	.L1970
	leaq	1(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movb	%r13b, (%rdx)
.L666:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	je	.L1971
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1972
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L668:
	movq	-1520(%rbp), %rsi
	movzbl	%al, %edx
	movq	112(%rsi), %rcx
	cmpl	$110, (%rcx,%rdx,4)
	jne	.L1808
	movl	-1544(%rbp), %edi
	xorl	%edx, %edx
	testl	%edi, %edi
	setg	%dl
	subl	%edx, %edi
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	movl	%edi, -1544(%rbp)
	je	.L1973
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L671:
	movl	-1544(%rbp), %edx
	testl	%edx, %edx
	je	.L1809
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1974
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L1868:
	movq	-1520(%rbp), %rdi
	movzbl	%r13b, %eax
	leaq	2(%r12), %r15
	movq	112(%rdi), %rdx
	cmpl	$102, (%rdx,%rax,4)
	jne	.L1811
	movl	-1544(%rbp), %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	setg	%al
	subl	%eax, %esi
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	movl	%esi, -1544(%rbp)
	je	.L1975
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	%r13b, (%rax)
.L676:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	je	.L663
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1976
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L678:
	movq	-1520(%rbp), %rsi
	movzbl	%r13b, %eax
	movq	112(%rsi), %rdx
	cmpl	$105, (%rdx,%rax,4)
	je	.L1977
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	call	_IO_sputbackc
.L663:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L1978
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	$0, (%rax)
.L832:
	cmpq	$0, -1104(%rbp)
	je	.L1979
	movl	-1528(%rbp), %eax
	movq	-1088(%rbp), %rdi
	movl	%eax, %edx
	movl	%eax, %ebx
	andl	$128, %edx
	andl	$8, %ebx
	testb	$2, %al
	je	.L834
	testb	$4, -1672(%rbp)
	jne	.L1980
	testb	$1, -1672(%rbp)
	jne	.L834
	leaq	-1496(%rbp), %rsi
	call	__strtold_internal
	testl	%ebx, %ebx
	jne	.L2139
	movq	-1088(%rbp), %rsi
	movq	-1496(%rbp), %rdx
	cmpq	%rsi, %rdx
	movq	%rsi, %rdi
	je	.L1834
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L851
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L852
	movl	%ecx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L853:
	movq	(%rax), %rax
.L854:
	fstpt	(%rax)
.L850:
	cmpq	%rsi, %rdx
	je	.L1981
	addl	$1, -1524(%rbp)
	jmp	.L1873
.L104:
	movl	-1528(%rbp), %eax
	andl	$8, %eax
	movl	%eax, -1576(%rbp)
.L105:
	movl	-1576(%rbp), %eax
	testl	%eax, %eax
	jne	.L317
.L315:
	movl	-1528(%rbp), %eax
	andl	$8448, %eax
	movl	%eax, -1576(%rbp)
	je	.L318
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L319
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L320
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L321:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L322:
	movq	-1584(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1778
	movl	$400, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1624(%rbp)
	movq	%rax, (%rbx)
	je	.L332
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L333
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L333
.L334:
	movq	-1648(%rbp), %rdi
	movq	-1584(%rbp), %rsi
	movl	$0, -1576(%rbp)
	movq	$100, -1640(%rbp)
	movq	%rdx, (%rdi)
	movq	%rsi, 16(%rdi,%rax,8)
.L317:
	cmpl	$-1, %r13d
	je	.L1982
.L316:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1983
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L349:
	movq	-1520(%rbp), %rsi
	leaq	-1312(%rbp), %rax
	movq	$0, -1312(%rbp)
	addq	$1, %r15
	movq	%rax, -1552(%rbp)
	movslq	%r13d, %rax
	movq	104(%rsi), %rdx
	testb	$32, 1(%rdx,%rax,2)
	jne	.L350
	leaq	-1392(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -1536(%rbp)
.L351:
	movl	-1576(%rbp), %eax
	movq	-1624(%rbp), %rbx
	movb	%r13b, -1392(%rbp)
	testl	%eax, %eax
	cmovne	%r12, %rbx
.L359:
	movq	-1552(%rbp), %rcx
	movq	-1536(%rbp), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	__mbrtowc
	cmpq	$-2, %rax
	jne	.L1984
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1985
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L1860:
	addq	$1, %r15
	movb	%r13b, -1392(%rbp)
	jmp	.L359
.L1932:
	leaq	2(%rax), %rcx
	orl	$512, -1528(%rbp)
	movq	%rcx, -1504(%rbp)
	movzbl	2(%rax), %edx
	jmp	.L73
.L77:
	leaq	2(%rax), %rcx
	orl	$8193, -1528(%rbp)
	movq	%rcx, -1504(%rbp)
	movzbl	2(%rax), %edx
	jmp	.L73
.L1931:
	leaq	2(%rax), %rcx
	orl	$3, -1528(%rbp)
	movq	%rcx, -1504(%rbp)
	movzbl	2(%rax), %edx
	jmp	.L73
.L1850:
	movq	-1536(%rbp), %rdx
	movq	%r14, %r13
.L373:
	movl	-1512(%rbp), %eax
	movl	%eax, %fs:(%rdx)
.L374:
	movl	-1524(%rbp), %esi
	movl	$-1, %eax
	testl	%esi, %esi
	cmovne	%esi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1766:
	movq	%r14, %r13
	jmp	.L41
.L1094:
	movq	%r12, %r15
.L101:
	movl	-1528(%rbp), %eax
	movl	%eax, %edi
	movl	%eax, %r12d
	andl	$8, %edi
	andl	$1, %r12d
	movl	%edi, -1576(%rbp)
	jne	.L105
	testl	%edi, %edi
	jne	.L269
	testb	$33, %ah
	je	.L270
	movl	-1668(%rbp), %esi
	testl	%esi, %esi
	jne	.L271
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L272
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L273:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L274:
	movq	-1584(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1775
	movl	$100, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1656(%rbp)
	movq	%rax, (%rbx)
	je	.L283
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L284
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L284
.L285:
	movq	-1648(%rbp), %rdi
	movq	-1584(%rbp), %rsi
	movq	$100, -1640(%rbp)
	movq	%rdx, (%rdi)
	movq	%rsi, 16(%rdi,%rax,8)
.L269:
	cmpl	$-1, %r13d
	je	.L1986
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1987
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L301:
	movq	-1520(%rbp), %r10
	movslq	%r13d, %rax
	addq	$1, %r15
	movq	104(%r10), %rdx
	testb	$32, 1(%rdx,%rax,2)
	jne	.L302
	movl	-1528(%rbp), %r8d
	movl	-1544(%rbp), %ebx
	movq	%r15, %rcx
	movl	%r12d, -1544(%rbp)
	movq	-1640(%rbp), %r11
	movq	-1656(%rbp), %r12
	movl	-1576(%rbp), %r15d
	andl	$8448, %r8d
	jmp	.L303
.L1992:
	movq	-1584(%rbp), %rdi
	movq	%rax, %r12
	movq	(%rdi), %rdi
	leaq	(%rdi,%r11), %rsi
	cmpq	%rsi, %rax
	je	.L1988
.L305:
	testl	%ebx, %ebx
	jle	.L308
	subl	$1, %ebx
	je	.L1989
.L308:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1990
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L309:
	movq	104(%r10), %rdx
	movslq	%r13d, %rax
	addq	$1, %rcx
	testb	$32, 1(%rdx,%rax,2)
	jne	.L1991
.L303:
	testl	%r15d, %r15d
	jne	.L305
	testl	%r8d, %r8d
	leaq	1(%r12), %rax
	movb	%r13b, (%r12)
	jne	.L1992
	movq	%rax, %r12
	jmp	.L305
.L1162:
	movq	%r15, %r12
.L100:
	movl	-1528(%rbp), %eax
	movl	$16, %r8d
	andl	$-7, %eax
	orl	$4097, %eax
	movl	%eax, -1528(%rbp)
	jmp	.L93
.L88:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L1993
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L109:
	cmpl	$37, %r13d
	je	.L1873
	movl	%r13d, %r15d
	movq	%r14, %rdi
	movq	%r14, %r13
	movzbl	%r15b, %esi
	call	_IO_sputbackc
	jmp	.L41
.L106:
	movq	-1536(%rbp), %rdx
	movl	-1512(%rbp), %eax
	movq	%r14, %r13
	movl	%eax, %fs:(%rdx)
.L107:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L99:
	movl	$8, %r8d
	jmp	.L372
.L1095:
	movl	$10, %r8d
	jmp	.L372
.L92:
	movl	-1528(%rbp), %eax
	movq	%r12, %r15
	andl	$8, %eax
	movl	%eax, -1576(%rbp)
	je	.L315
	jmp	.L316
.L1017:
	testb	$8, -1528(%rbp)
	je	.L315
	movq	-1536(%rbp), %rdx
	movq	%r14, %r13
.L1018:
	movl	-1512(%rbp), %eax
	movl	%eax, %fs:(%rdx)
.L347:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1849:
	movq	-1536(%rbp), %rdx
	movq	%r14, %r13
.L642:
	movl	-1512(%rbp), %eax
	movl	%eax, %fs:(%rdx)
.L643:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1848:
	movq	%r14, %r13
	jmp	.L41
.L1765:
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	-1520(%rbp), %rsi
	movslq	%r13d, %rax
	movq	104(%rsi), %rsi
	testb	$16, 1(%rsi,%rax,2)
	je	.L1790
	cmpq	%rcx, -1096(%rbp)
	movl	%r13d, %r10d
	movsbl	%r13b, %r11d
	jne	.L496
.L1948:
	movq	-1568(%rbp), %rdi
	movl	%r11d, %esi
	movq	%r9, -1544(%rbp)
	movl	%r8d, -1536(%rbp)
	call	char_buffer_add_slow
	xorl	%eax, %eax
	testl	%ebx, %ebx
	movl	-1536(%rbp), %r8d
	setg	%al
	movq	-1544(%rbp), %r9
	subl	%eax, %ebx
	cmpl	$-1, %r13d
	jne	.L499
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movl	%esi, %fs:(%rax)
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L478:
	cmpl	$10, %r8d
	jne	.L1790
	movl	-1592(%rbp), %edi
	testl	%edi, %edi
	je	.L1790
	movq	-1560(%rbp), %r12
	testl	%ebx, %ebx
	movl	$2147483647, %r14d
	cmovg	%ebx, %r14d
	movzbl	(%r12), %esi
	cmpl	%esi, %r13d
	jne	.L1125
	movl	%ebx, -1536(%rbp)
	movl	%r8d, -1552(%rbp)
	movq	%r9, %rbx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L481:
	leaq	1(%rax), %rcx
	addq	$1, %r12
	movq	%rcx, -1104(%rbp)
	movb	%r13b, (%rax)
	cmpb	$0, (%r12)
	je	.L483
.L1996:
	testl	%r14d, %r14d
	je	.L484
	movq	8(%r15), %rax
	cmpq	16(%r15), %rax
	jnb	.L1994
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r15)
	movzbl	(%rax), %r13d
.L486:
	movzbl	(%r12), %esi
	movq	-1104(%rbp), %rax
	addq	$1, %rbx
	subl	$1, %r14d
	movq	%rax, %rcx
	cmpl	%r13d, %esi
	jne	.L1995
.L488:
	cmpq	%rax, -1096(%rbp)
	movl	%r13d, %r10d
	movsbl	%r13b, %r11d
	jne	.L481
	movq	-1568(%rbp), %rdi
	movl	%r11d, %esi
	addq	$1, %r12
	movl	%r11d, -1544(%rbp)
	movb	%r13b, -1576(%rbp)
	call	char_buffer_add_slow
	cmpb	$0, (%r12)
	movl	-1544(%rbp), %r11d
	movzbl	-1576(%rbp), %r10d
	jne	.L1996
	.p2align 4,,10
	.p2align 3
.L483:
	movq	-1104(%rbp), %rcx
	movq	%rbx, %r9
	movl	-1552(%rbp), %r8d
	movl	-1536(%rbp), %ebx
	testq	%rcx, %rcx
	jne	.L1045
.L1864:
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	%r15, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L486
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rbx, %r9
	movl	-1552(%rbp), %r8d
	movl	-1536(%rbp), %ebx
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	movq	-1104(%rbp), %rax
	movq	%rax, %rcx
.L480:
	testq	%rax, %rax
	je	.L1864
	cmpb	$0, (%r12)
	jne	.L1997
	movl	%r13d, %r10d
	movsbl	%r13b, %r11d
.L1045:
	testl	%ebx, %ebx
	cmovle	%ebx, %r14d
	subq	$1, %rcx
	movq	%rcx, -1104(%rbp)
	movl	%r14d, %ebx
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L484:
	movl	-1536(%rbp), %eax
	movq	%r15, %r14
	movl	-1552(%rbp), %r8d
	movq	%rbx, %r15
	movl	%eax, -1544(%rbp)
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	jne	.L1046
	movq	%r14, %r13
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1995:
	movq	%rbx, %r9
	movl	-1552(%rbp), %r8d
	movl	-1536(%rbp), %ebx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L1950:
	movq	%r15, %rdi
	movq	%r9, -1544(%rbp)
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	movq	-1544(%rbp), %r9
	jne	.L502
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L504
.L1941:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L383
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L385:
	testl	%r8d, %r8d
	je	.L401
	jmp	.L393
.L1921:
	movzbl	%al, %esi
	movq	%r14, %rdi
	movq	%r14, %r13
	call	_IO_sputbackc
	jmp	.L41
.L1912:
	testl	%r12d, %r12d
	movl	%r13d, %r15d
	movq	%r14, %r13
	je	.L41
	movq	$-2, %rbx
	movl	-1512(%rbp), %r14d
	movq	-1520(%rbp), %r12
	movl	%r15d, %eax
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L1998
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %eax
	movq	%rax, %rdx
	addq	%rdx, %rdx
.L1001:
	movq	104(%r12), %rcx
	movzwl	(%rcx,%rdx), %edx
	andb	$32, %dh
	je	.L1999
.L1004:
	cmpl	$-1, %eax
	jne	.L1000
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%r14d, %fs:(%rdx)
	movq	%rbx, %rdx
	jmp	.L1001
.L1938:
	movq	%r14, %r13
	movq	__libc_errno@gottpoff(%rip), %rdx
	jmp	.L373
.L534:
	call	__strtoul_internal
	jmp	.L535
.L508:
	testl	$4096, -1528(%rbp)
	je	.L1793
	cmpl	$4, -1544(%rbp)
	jbe	.L1793
	cmpl	$40, %r13d
	jne	.L1793
	movq	-1520(%rbp), %rax
	movq	112(%rax), %rbx
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2000
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
	cmpl	$110, (%rbx,%r13,4)
	jne	.L1797
.L1056:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2001
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
	cmpl	$105, (%rbx,%r13,4)
	jne	.L1797
.L1058:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2002
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
	cmpl	$108, (%rbx,%r13,4)
	jne	.L1797
.L526:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2003
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L528:
	addq	$4, %r15
	cmpl	$41, %r13d
	jne	.L1797
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L2004
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	$48, (%rax)
	jmp	.L530
.L29:
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L30
	call	__lll_lock_wait_private
	jmp	.L30
.L1936:
	cmpq	$1, %rax
	jne	.L2005
	movl	-1592(%rbp), %eax
	addl	-1552(%rbp), %eax
	addq	$4, -1624(%rbp)
	testl	%eax, %eax
	jle	.L1774
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2006
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L263:
	addq	$1, %r15
	subq	$1, -1552(%rbp)
	jmp	.L249
.L1984:
	cmpq	$1, %rax
	jne	.L2007
	movl	-1576(%rbp), %eax
	addq	$4, -1624(%rbp)
	movq	-1624(%rbp), %rsi
	testl	%eax, %eax
	jne	.L361
	testl	$8448, -1528(%rbp)
	je	.L361
	movq	-1584(%rbp), %rax
	movq	(%rax), %rdi
	movq	-1640(%rbp), %rax
	leaq	0(,%rax,4), %rbx
	leaq	(%rdi,%rbx), %rax
	cmpq	%rax, %rsi
	je	.L2008
.L361:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	jle	.L364
	subl	$1, -1544(%rbp)
	je	.L352
.L364:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2009
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L366:
	movq	-1520(%rbp), %rdi
	movslq	%r13d, %rax
	addq	$1, %r15
	movq	104(%rdi), %rdx
	testb	$32, 1(%rdx,%rax,2)
	je	.L351
.L350:
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	subq	$1, %r15
	call	_IO_sputbackc
.L352:
	movl	-1576(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L1873
	testl	$8448, -1528(%rbp)
	movq	-1624(%rbp), %rax
	leaq	4(%rax), %rbx
	movl	$0, (%rax)
	je	.L370
	movq	-1584(%rbp), %r12
	movq	%rbx, %rsi
	movq	(%r12), %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	-1640(%rbp), %rax
	je	.L370
	call	realloc@PLT
	testq	%rax, %rax
	je	.L370
	movq	%rax, (%r12)
.L370:
	addl	$1, -1524(%rbp)
	movq	%rbx, -1624(%rbp)
	movq	$0, -1584(%rbp)
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1008:
#APP
# 3030 "vfscanf-internal.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1006
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 3030 "vfscanf-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1999:
	cmpl	$-1, %eax
	je	.L41
	movzbl	%al, %esi
	movq	%r13, %rdi
	call	_IO_sputbackc
	jmp	.L41
.L1956:
	testl	%r12d, %r12d
	je	.L2010
	cmpl	$-1, %r13d
	je	.L2011
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2012
	leaq	1(%rax), %rdx
	leaq	1(%r15), %r9
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L960:
	leaq	-1392(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%r15, -1576(%rbp)
	movq	$0, -1392(%rbp)
	movq	%r12, %r15
	movq	%r9, %r12
	movq	%rax, -1536(%rbp)
	leaq	-1312(%rbp), %rax
	movq	%rax, -1592(%rbp)
.L963:
	movq	-1088(%rbp), %rdx
	movslq	%r13d, %rax
	cmpb	%bl, (%rdx,%rax)
	je	.L2013
.L964:
	movl	-1552(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L2014
.L966:
	subl	$1, -1544(%rbp)
	je	.L2015
.L968:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2016
	leaq	1(%rax), %rdx
	addq	$1, %r12
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
	movq	-1088(%rbp), %rdx
	movslq	%r13d, %rax
	cmpb	%bl, (%rdx,%rax)
	jne	.L964
.L2013:
	movq	%r12, %r9
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	subq	$1, %r9
	movq	%r15, %r12
	movq	-1576(%rbp), %r15
	movq	%r9, -1536(%rbp)
	call	_IO_sputbackc
	movq	-1536(%rbp), %r9
.L965:
	testq	%r12, %r12
	jne	.L2017
	cmpq	%r15, %r9
	je	.L1843
	movl	-1552(%rbp), %edi
	testl	%edi, %edi
	jne	.L975
	testl	$8448, -1528(%rbp)
	movq	-1624(%rbp), %rax
	leaq	4(%rax), %rbx
	movl	$0, (%rax)
	je	.L977
	movq	-1584(%rbp), %r15
	movq	%rbx, %rsi
	movq	(%r15), %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	-1640(%rbp), %rax
	je	.L977
	movq	%r9, -1536(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1536(%rbp), %r9
	je	.L977
	movq	%rax, (%r15)
.L977:
	addl	$1, -1524(%rbp)
	movq	%rbx, -1624(%rbp)
	movq	$0, -1584(%rbp)
.L975:
	movq	-1504(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%r9, %r15
	jmp	.L31
.L2014:
	movq	-1536(%rbp), %rcx
	movq	-1592(%rbp), %rsi
	movl	$1, %edx
	movq	-1624(%rbp), %rdi
	movb	%r13b, -1312(%rbp)
	call	__mbrtowc
	cmpq	$-2, %rax
	je	.L2018
	addq	$4, -1624(%rbp)
	testl	$8448, -1528(%rbp)
	movq	-1624(%rbp), %rsi
	je	.L1154
	movq	-1584(%rbp), %rax
	xorl	%r15d, %r15d
	movq	(%rax), %rdi
	movq	-1640(%rbp), %rax
	leaq	0(,%rax,4), %rdx
	leaq	(%rdi,%rdx), %rax
	cmpq	%rax, %rsi
	jne	.L966
	movq	-1640(%rbp), %rax
	movq	%rdx, -1600(%rbp)
	leaq	0(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1600(%rbp), %rdx
	je	.L2019
	movq	-1584(%rbp), %rdi
	salq	-1640(%rbp)
	movq	%rax, (%rdi)
	addq	%rdx, %rax
	movq	%rax, -1624(%rbp)
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L1859
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$84, %fs:(%rax)
	jmp	.L41
.L1953:
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L509
.L1793:
	movl	%r13d, %r15d
	movq	%r14, %r13
	cmpl	$-1, %r15d
	je	.L41
.L1069:
	movzbl	%r15b, %esi
	movq	%r13, %rdi
	call	_IO_sputbackc
	jmp	.L41
.L1985:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L1860
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$84, %fs:(%rax)
	jmp	.L41
.L1909:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, -1524(%rbp)
	movl	$9, %fs:(%rax)
	jmp	.L18
.L537:
	testl	%edx, %edx
	je	.L590
	movl	-1668(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L1886
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L595:
	subl	$1, %r8d
	je	.L1902
	cmpl	$47, %ecx
	ja	.L596
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L1945:
	movl	$8, %r8d
	jmp	.L400
.L1125:
	movq	-1560(%rbp), %r12
	jmp	.L480
.L888:
	movl	-1552(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L889
	testl	$8448, -1528(%rbp)
	je	.L918
	movl	-1668(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L919
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L920
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L921:
	movq	(%rax), %rax
	movq	%rax, -1584(%rbp)
.L922:
	movq	-1584(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1838
	movl	$100, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1656(%rbp)
	movq	%rax, (%rbx)
	je	.L931
	movq	-1648(%rbp), %rax
	testq	%rax, %rax
	je	.L932
	movq	(%rax), %rax
	cmpq	$32, %rax
	leaq	1(%rax), %rdx
	je	.L932
.L933:
	movq	-1648(%rbp), %rdi
	movq	-1584(%rbp), %rsi
	movq	$100, -1640(%rbp)
	movq	%rdx, (%rdi)
	movq	%rsi, 16(%rdi,%rax,8)
	jmp	.L889
.L1126:
	movb	$0, -1552(%rbp)
	jmp	.L647
.L1944:
	testb	$10, %r8b
	jne	.L393
	movl	-1544(%rbp), %edi
	xorl	%eax, %eax
	testl	%edi, %edi
	setg	%al
	subl	%eax, %edi
	cmpl	$-1, %r13d
	movl	%edi, -1544(%rbp)
	je	.L2020
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2021
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L398:
	addq	$1, %r15
	movl	$16, %r8d
	jmp	.L400
.L1933:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movq	%r14, %r13
	movl	%edi, %fs:(%rax)
.L246:
	movl	-1524(%rbp), %esi
	movl	$-1, %eax
	testl	%esi, %esi
	cmovne	%esi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1841:
	movq	%r14, %r13
	jmp	.L41
.L1946:
	leaq	.LC0(%rip), %rdi
	call	__wctrans@PLT
	movq	-1608(%rbp), %rsi
	movq	%rax, -1688(%rbp)
	movl	216(%rsi), %edi
	movl	%edi, -1552(%rbp)
	subl	$1, %edi
	testq	%rax, %rax
	movl	%edi, -1696(%rbp)
	jne	.L2022
.L402:
	movq	%r14, %rax
	movl	$0, -1616(%rbp)
	movl	%r13d, %r14d
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L472:
	cmpl	$-1, %r14d
	je	.L1863
	movl	-1544(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1863
	movl	-1544(%rbp), %eax
	movl	$2147483647, %edi
	movq	%r15, -1552(%rbp)
	testl	%eax, %eax
	cmovg	%eax, %edi
	leaq	-1392(%rbp), %rax
	xorl	%ebx, %ebx
	movl	%edi, -1600(%rbp)
	movq	%rax, -1536(%rbp)
	.p2align 4,,10
	.p2align 3
.L428:
	cmpq	$0, -1688(%rbp)
	movl	%ebx, -1592(%rbp)
	jne	.L2023
	movq	-1608(%rbp), %rax
	movq	224(%rax,%rbx,8), %r12
	movq	-1536(%rbp), %rax
	movq	%r12, (%rax,%rbx,8)
.L411:
	movl	-1616(%rbp), %r9d
	xorl	%r15d, %r15d
	testl	%r9d, %r9d
	je	.L410
	movl	%r14d, -1576(%rbp)
	movq	%r13, -1632(%rbp)
	movl	%r15d, %r14d
	movq	%r12, %r13
	movl	-1616(%rbp), %r15d
	movq	-1536(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r13, %rdi
	addl	$1, %r14d
	call	strlen@PLT
	leaq	1(%r13,%rax), %r13
	cmpl	%r14d, %r15d
	movq	%r13, (%r12,%rbx,8)
	jne	.L409
	movq	%r13, %r12
	movl	-1576(%rbp), %r14d
	movq	-1632(%rbp), %r13
.L410:
	movzbl	(%r12), %ecx
	cmpl	%r14d, %ecx
	jne	.L2024
	cmpb	$0, 1(%r12)
	leaq	1(%r12), %r8
	je	.L1862
	movq	%rbx, -1576(%rbp)
	movl	-1600(%rbp), %ecx
	movq	%r8, %r15
	movq	-1552(%rbp), %rbx
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %r14d
.L417:
	movzbl	(%r15), %edx
	addq	$1, %rbx
	subl	$1, %ecx
	cmpl	%r14d, %edx
	movl	%edx, %eax
	jne	.L2025
	addq	$1, %r15
	cmpb	$0, (%r15)
	je	.L1784
	testl	%ecx, %ecx
	je	.L415
.L421:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jb	.L416
	movq	%r13, %rdi
	movl	%ecx, -1552(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r14d
	movl	-1552(%rbp), %ecx
	jne	.L417
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rbx, -1552(%rbp)
	movq	%r15, %r8
	movq	-1576(%rbp), %rbx
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	movzbl	(%r15), %eax
.L420:
	testb	%al, %al
	je	.L2026
	cmpq	%r12, %r8
	ja	.L2027
.L424:
	movq	%r12, %rdi
	call	strlen@PLT
	movq	-1536(%rbp), %rdi
	leaq	1(%r12,%rax), %rax
	movq	%rax, (%rdi,%rbx,8)
	addq	$1, %rbx
	cmpq	$10, %rbx
	jne	.L428
	movl	-1616(%rbp), %eax
	movq	-1552(%rbp), %r15
	addl	$1, %eax
	cmpl	%eax, -1696(%rbp)
	movl	%eax, -1632(%rbp)
	jl	.L1025
	movq	%r13, %rax
	movl	%r14d, %r13d
	movq	%rax, %r14
.L1024:
	movq	%r14, %rax
	movq	$0, -1552(%rbp)
	movl	%r13d, %r14d
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L445:
	movq	-1552(%rbp), %rax
	movq	-1536(%rbp), %rdi
	movq	(%rdi,%rax,8), %rbx
	movl	%eax, -1592(%rbp)
	movzbl	(%rbx), %edx
	cmpl	%r14d, %edx
	jne	.L2028
	cmpb	$0, 1(%rbx)
	leaq	1(%rbx), %r12
	movl	-1600(%rbp), %ecx
	jne	.L439
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L434:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %r14d
.L435:
	movzbl	(%r12), %edx
	addq	$1, %r15
	subl	$1, %ecx
	cmpl	%r14d, %edx
	movl	%edx, %eax
	jne	.L438
	addq	$1, %r12
	cmpb	$0, (%r12)
	je	.L1040
	testl	%ecx, %ecx
	je	.L433
.L439:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jb	.L434
	movq	%r13, %rdi
	movl	%ecx, -1576(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r14d
	movl	-1576(%rbp), %ecx
	jne	.L435
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	movzbl	(%r12), %eax
.L438:
	testb	%al, %al
	je	.L1040
	cmpq	%rbx, %r12
	ja	.L2029
.L441:
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	-1552(%rbp), %rdi
	movq	-1536(%rbp), %rsi
	leaq	1(%rbx,%rax), %rax
	movq	%rax, (%rsi,%rdi,8)
	addq	$1, %rdi
	cmpq	$10, %rdi
	movq	%rdi, -1552(%rbp)
	jne	.L445
	movq	%r13, %rax
	addl	$1, -1632(%rbp)
	movl	%r14d, %r13d
	movq	%rax, %r14
	movl	-1632(%rbp), %eax
	cmpl	%eax, -1696(%rbp)
	jge	.L1024
	movq	%r14, %rax
	movl	%r13d, %r14d
	movq	%rax, %r13
.L1025:
	testb	$-128, -1528(%rbp)
	je	.L1863
	movq	-1560(%rbp), %r12
	movl	-1544(%rbp), %eax
	movl	$2147483647, %ecx
	movzbl	(%r12), %ebx
	testl	%eax, %eax
	cmovg	%eax, %ecx
	cmpl	%r14d, %ebx
	jne	.L1120
	movl	%ecx, %r14d
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	1(%rsi), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%bl, (%rsi)
.L450:
	addq	$1, %r12
	cmpb	$0, (%r12)
	je	.L451
	testl	%r14d, %r14d
	je	.L452
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L2030
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %ebx
.L454:
	movzbl	(%r12), %eax
	addq	$1, %r15
	subl	$1, %r14d
	cmpl	%ebx, %eax
	jne	.L2031
.L456:
	movq	-1104(%rbp), %rsi
	cmpq	%rsi, -1096(%rbp)
	movl	%ebx, %edx
	movsbl	%bl, %eax
	jne	.L449
	movq	-1568(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -1536(%rbp)
	movb	%bl, -1552(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %eax
	movzbl	-1552(%rbp), %edx
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L2029:
	cmpl	$-1, %r14d
	jne	.L1068
.L442:
	leaq	-1(%r12), %rax
	cmpq	%rax, %rbx
	movq	%rax, -1576(%rbp)
	jnb	.L443
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L444:
	movzbl	(%r14), %esi
	movq	%r13, %rdi
	subq	$1, %r14
	call	_IO_sputbackc
	cmpq	%r14, %rbx
	jne	.L444
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	$1, %rax
	addq	%rax, -1576(%rbp)
	addq	%rax, %r15
.L443:
	movq	-1576(%rbp), %rax
	movzbl	(%rax), %r14d
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L433:
	cmpq	%r12, %rbx
	jnb	.L441
.L1068:
	movzbl	%r14b, %esi
	movq	%r13, %rdi
	subq	$1, %r15
	call	_IO_sputbackc
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L2027:
	cmpl	$-1, %r14d
	jne	.L1067
.L425:
	leaq	-1(%r8), %r15
	cmpq	%r12, %r15
	jbe	.L426
	movq	%rbx, -1576(%rbp)
	movq	%r8, %r14
	movq	%r15, %rbx
	.p2align 4,,10
	.p2align 3
.L427:
	movzbl	(%rbx), %esi
	movq	%r13, %rdi
	subq	$1, %rbx
	call	_IO_sputbackc
	cmpq	%r12, %rbx
	jne	.L427
	movq	%r12, %rax
	movq	-1576(%rbp), %rbx
	subq	%r14, %rax
	addq	$1, %rax
	addq	%rax, -1552(%rbp)
	addq	%rax, %r15
.L426:
	movzbl	(%r15), %r14d
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L415:
	cmpq	%r12, %r15
	movq	%rbx, -1552(%rbp)
	movq	%r15, %r8
	movq	-1576(%rbp), %rbx
	jbe	.L424
.L1067:
	movzbl	%r14b, %esi
	movq	%r13, %rdi
	movq	%r8, -1576(%rbp)
	subq	$1, -1552(%rbp)
	call	_IO_sputbackc
	movq	-1576(%rbp), %r8
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	%rbx, -1552(%rbp)
	movq	%r15, %r8
	movq	-1576(%rbp), %rbx
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	%rbx, %r15
.L1039:
	movl	-1616(%rbp), %eax
	movl	%eax, -1632(%rbp)
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	cmovle	%eax, %ecx
.L423:
	movl	-1592(%rbp), %r14d
	movl	-1632(%rbp), %edi
	movq	-1104(%rbp), %rsi
	addl	$48, %r14d
	movl	%edi, -1696(%rbp)
	movl	%edi, -1616(%rbp)
	movl	%r14d, %edx
	movl	%r14d, %eax
.L1026:
	cmpq	%rsi, -1096(%rbp)
	je	.L2032
	leaq	1(%rsi), %rax
	movq	%rax, -1104(%rbp)
	movb	%dl, (%rsi)
.L465:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setg	%al
	subl	%eax, %ecx
	cmpl	$-1, %r14d
	movl	%ecx, -1544(%rbp)
	je	.L2033
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L2034
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%r13)
	movzbl	(%rax), %r14d
.L470:
	addq	$1, %r15
	jmp	.L472
.L2028:
	testb	%dl, %dl
	jne	.L441
	movl	-1600(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	cmovle	%eax, %ecx
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	-1312(%rbp,%rbx,8), %r12
	movq	-1536(%rbp), %rax
	movq	%r12, (%rax,%rbx,8)
	jmp	.L411
.L2033:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movl	%edi, %fs:(%rax)
	jmp	.L472
.L2030:
	movq	%r13, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L2035
	movl	%r14d, %ecx
	movl	%eax, %r14d
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L448:
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L1041
	cmpb	$0, (%r12)
	jne	.L2036
	movl	%r14d, %edx
	movsbl	%r14b, %eax
.L1042:
	movl	-1544(%rbp), %edi
	testl	%edi, %edi
	cmovle	%edi, %ecx
	subq	$1, %rsi
	movq	%rsi, -1104(%rbp)
	jmp	.L1026
.L2024:
	testb	%cl, %cl
	jne	.L424
.L1862:
	movq	-1552(%rbp), %r15
	movl	-1600(%rbp), %ecx
	jmp	.L1039
.L1154:
	xorl	%r15d, %r15d
	jmp	.L966
.L2018:
	addq	$1, %r15
	cmpq	$15, %r15
	jbe	.L968
	leaq	__PRETTY_FUNCTION__.13126(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$2684, %edx
	call	__assert_fail
.L1997:
	movq	%r15, %r14
	movl	%ebx, -1544(%rbp)
	movq	%r9, %r15
.L1046:
	cmpq	%r12, -1560(%rbp)
	movq	%rax, %rcx
	jnb	.L507
	movq	%r12, %rdx
	subq	-1560(%rbp), %rdx
	subq	%rdx, %rax
	cmpl	$-1, %r13d
	movq	%rax, -1104(%rbp)
	je	.L492
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	_IO_sputbackc
	movl	-1536(%rbp), %r8d
	subq	$1, %r15
.L492:
	leaq	-1(%r12), %rbx
	cmpq	%rbx, -1560(%rbp)
	jnb	.L493
	movq	%rbx, -1536(%rbp)
	movl	%r8d, %r13d
.L494:
	movzbl	(%rbx), %esi
	movq	%r14, %rdi
	subq	$1, %rbx
	call	_IO_sputbackc
	cmpq	%rbx, -1560(%rbp)
	jne	.L494
	movq	-1560(%rbp), %rax
	movq	-1536(%rbp), %rbx
	movl	%r13d, %r8d
	subq	%r12, %rax
	addq	$1, %rax
	addq	%rax, %r15
	addq	%rax, %rbx
.L493:
	movzbl	(%rbx), %r13d
	movq	-1104(%rbp), %rcx
	jmp	.L475
.L2032:
	movq	-1568(%rbp), %rdi
	movl	%eax, %esi
	movl	%ecx, -1536(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %ecx
	jmp	.L465
.L1982:
	movq	%r14, %r13
	movq	__libc_errno@gottpoff(%rip), %rdx
	jmp	.L1018
.L1939:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L376
	movq	%r14, %r13
	jmp	.L374
.L1168:
	movq	-1088(%rbp), %rdx
	movb	$1, (%rdx,%rax)
	movq	-1504(%rbp), %rax
	leaq	1(%rax), %rdx
	jmp	.L950
.L1952:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L1990:
	movq	%r14, %rdi
	movl	%r8d, -1616(%rbp)
	movq	%rcx, -1600(%rbp)
	movq	%r10, -1592(%rbp)
	movq	%r11, -1552(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movq	-1552(%rbp), %r11
	movq	-1592(%rbp), %r10
	movq	-1600(%rbp), %rcx
	movl	-1616(%rbp), %r8d
	jne	.L309
	movq	-1536(%rbp), %rax
	movq	%r12, -1656(%rbp)
	movq	%rcx, %r15
	movl	-1544(%rbp), %r12d
	movq	%r11, -1640(%rbp)
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L304:
	movl	-1576(%rbp), %edx
	testl	%edx, %edx
	jne	.L1874
	testl	$8448, -1528(%rbp)
	movq	-1656(%rbp), %rax
	leaq	1(%rax), %rdx
	movb	$0, (%rax)
	je	.L313
	movq	-1584(%rbp), %rbx
	movq	%rdx, %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	cmpq	-1640(%rbp), %rsi
	je	.L313
	movq	%rdx, -1536(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1536(%rbp), %rdx
	je	.L313
	movq	%rax, (%rbx)
.L313:
	addl	$1, -1524(%rbp)
	movq	-1504(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%rdx, -1656(%rbp)
	movq	$0, -1584(%rbp)
	jmp	.L31
.L2010:
	cmpl	$-1, %r13d
	je	.L2037
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2038
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
	leaq	1(%r15), %rdx
.L983:
	movl	-1544(%rbp), %edi
	movslq	%r13d, %rax
	leal	-1(%rdi), %ecx
	movl	-1528(%rbp), %edi
	leaq	(%rcx,%rdx), %rsi
	movq	-1088(%rbp), %rcx
	andl	$8448, %edi
	movq	%rsi, -1536(%rbp)
	movl	%edi, -1544(%rbp)
	cmpb	(%rcx,%rax), %bl
	je	.L993
	movl	%r12d, -1576(%rbp)
	movq	%r15, -1592(%rbp)
	movq	%r14, %rcx
	movq	%rdx, %r15
	movl	%ebx, %r14d
	movl	-1552(%rbp), %r12d
	movl	%r13d, %ebx
	movq	-1656(%rbp), %r13
	jmp	.L994
.L2043:
	movq	-1584(%rbp), %rsi
	movq	%rax, %r13
	movq	(%rsi), %rdi
	movq	-1640(%rbp), %rsi
	addq	%rdi, %rsi
	cmpq	%rsi, %rax
	je	.L2039
.L986:
	cmpq	%r15, -1536(%rbp)
	je	.L2040
	movq	8(%rcx), %rax
	cmpq	16(%rcx), %rax
	jnb	.L2041
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%rcx)
	movzbl	(%rax), %ebx
.L991:
	movq	-1088(%rbp), %rsi
	movslq	%ebx, %rax
	addq	$1, %r15
	cmpb	%r14b, (%rsi,%rax)
	je	.L2042
.L994:
	testl	%r12d, %r12d
	jne	.L986
	movl	-1544(%rbp), %esi
	leaq	1(%r13), %rax
	movb	%bl, 0(%r13)
	testl	%esi, %esi
	jne	.L2043
	movq	%rax, %r13
	jmp	.L986
.L217:
	movl	-1668(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L233
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L234
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L235:
	movq	(%rax), %rax
	movq	%rax, -1624(%rbp)
.L236:
	cmpq	$0, -1624(%rbp)
	jne	.L216
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L318:
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L335
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L336
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L337:
	movq	(%rax), %rax
	movq	%rax, -1624(%rbp)
.L338:
	cmpq	$0, -1624(%rbp)
	jne	.L317
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	%r13, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L2044
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%fs:(%rdx), %r14d
	movq	%rbx, %rdx
	jmp	.L1001
.L2016:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L2045
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r12, %r9
	movq	%r15, %r12
	movq	-1576(%rbp), %r15
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L965
.L590:
	testb	$4, -1528(%rbp)
	je	.L603
	movl	-1668(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L604
.L1889:
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L605
	movl	%ecx, %edx
	addq	-1432(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L606:
	movq	(%rdx), %rdx
.L607:
	movw	%ax, (%rdx)
	jmp	.L551
.L1955:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L538:
	testb	$4, -1528(%rbp)
	je	.L552
	movl	-1668(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1889
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L557:
	subl	$1, -1668(%rbp)
	je	.L1904
	cmpl	$47, %ecx
	ja	.L558
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	-1568(%rbp), %rdi
	xorl	%esi, %esi
	movl	%r8d, -1536(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %r8d
	jmp	.L532
.L2034:
	movq	%r13, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r14d
	jne	.L470
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L472
.L452:
	movq	-1104(%rbp), %rax
	movq	%r13, %r14
	testq	%rax, %rax
	jne	.L1043
	movq	%r14, %r13
.L1041:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L451:
	movq	-1104(%rbp), %rsi
	movl	%r14d, %ecx
	testq	%rsi, %rsi
	je	.L1041
	movl	%ebx, %r14d
	jmp	.L1042
.L1960:
	movq	-1544(%rbp), %rax
	addq	-1536(%rbp), %rax
	movq	%r13, %r9
	subq	%rbx, %rax
	cmpq	%rax, %r13
	jl	.L200
	leal	-1(%r12), %r9d
	movslq	%r9d, %r9
.L200:
	addq	%r13, %r9
	movl	%r8d, -1528(%rbp)
	movq	%rcx, -1576(%rbp)
	movq	%r9, %rsi
	movq	%r9, -1552(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1552(%rbp), %r9
	movq	-1576(%rbp), %rcx
	movl	-1528(%rbp), %r8d
	je	.L2046
	movq	-1584(%rbp), %rdi
	movq	%rax, (%rdi)
	addq	%r13, %rax
	movq	%r9, %r13
	jmp	.L199
.L1940:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	movl	%r8d, -1536(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %r8d
	jmp	.L380
.L1942:
	movq	-1568(%rbp), %rdi
	movl	$48, %esi
	movl	%r8d, -1536(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %r8d
	jmp	.L388
.L664:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	je	.L1128
	cmpl	$48, %r13d
	jne	.L1128
	cmpq	%rdx, -1096(%rbp)
	je	.L2047
	leaq	1(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movb	$48, (%rdx)
.L705:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2048
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L706:
	addq	$1, %r12
.L708:
	movl	-1544(%rbp), %esi
	xorl	%eax, %eax
	movq	-1104(%rbp), %rdx
	testl	%esi, %esi
	setg	%al
	subl	%eax, %esi
	movl	%esi, -1544(%rbp)
	je	.L1129
	movq	-1520(%rbp), %rdi
	movzbl	%r13b, %eax
	movb	$101, -1576(%rbp)
	movl	$1, %ebx
	movq	112(%rdi), %rcx
	cmpl	$120, (%rcx,%rax,4)
	je	.L2049
.L703:
	movsbl	-1576(%rbp), %eax
	movb	%bl, -1536(%rbp)
	xorl	%r15d, %r15d
	movl	%r13d, %ebx
	movb	$0, -1592(%rbp)
	movq	%r14, %r13
	movl	-1544(%rbp), %r14d
	movl	%eax, -1600(%rbp)
	jmp	.L717
.L2054:
	cmpq	%rdx, -1096(%rbp)
	je	.L2050
.L726:
	leaq	1(%rdx), %rsi
	movb	$1, -1536(%rbp)
	movq	%rsi, -1104(%rbp)
	movb	%bl, (%rdx)
.L723:
	testl	%r14d, %r14d
	je	.L1825
	cmpl	$-1, %ebx
	je	.L2051
.L722:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L2052
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %ebx
.L765:
	xorl	%edx, %edx
	addq	$1, %r12
	testl	%r14d, %r14d
	setg	%dl
	subl	%edx, %r14d
	movq	-1104(%rbp), %rdx
.L717:
	testq	%rdx, %rdx
	je	.L2053
	leal	-48(%rbx), %esi
	cmpl	$9, %esi
	jbe	.L2054
	cmpb	$0, -1592(%rbp)
	je	.L2055
	movzbl	-1576(%rbp), %eax
	cmpb	%al, -1(%rdx)
	je	.L2056
.L727:
	movq	-1680(%rbp), %rax
	testl	%r14d, %r14d
	movl	$2147483647, %r9d
	cmovg	%r14d, %r9d
	testb	%r15b, %r15b
	movzbl	(%rax), %edx
	je	.L2057
	testb	%dl, %dl
	je	.L745
.L1136:
	movq	-1560(%rbp), %rsi
.L746:
	testq	%rsi, %rsi
	je	.L1823
.L757:
	cmpb	$0, (%rsi)
	jne	.L1823
.L1051:
	movq	-1560(%rbp), %r10
	movzbl	(%r10), %edx
	testb	%dl, %dl
	je	.L763
.L760:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L2058
	leaq	1(%rax), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%dl, (%rax)
.L762:
	addq	$1, %r10
	movzbl	(%r10), %edx
	testb	%dl, %dl
	jne	.L760
.L763:
	testl	%r14d, %r14d
	cmovg	%r9d, %r14d
	jmp	.L723
.L2055:
	testl	$2048, -1528(%rbp)
	je	.L725
	movq	-1520(%rbp), %rax
	movslq	%ebx, %rsi
	movq	104(%rax), %rdi
	testb	$16, 1(%rdi,%rsi,2)
	je	.L725
	cmpq	%rdx, -1096(%rbp)
	jne	.L726
	movq	-1568(%rbp), %rdi
	movsbl	%bl, %esi
	call	char_buffer_add_slow
	movb	$1, -1536(%rbp)
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L1969:
	cmpq	%rdx, -1096(%rbp)
	je	.L2059
	leaq	1(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movb	%r13b, (%rdx)
.L654:
	movl	-1544(%rbp), %edx
	testl	%edx, %edx
	je	.L2060
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2061
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L656:
	movq	-1520(%rbp), %rsi
	movzbl	%al, %edx
	movq	112(%rsi), %rcx
	cmpl	$97, (%rcx,%rdx,4)
	jne	.L1803
	movl	-1544(%rbp), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	setg	%dl
	subl	%edx, %esi
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	movl	%esi, -1544(%rbp)
	je	.L2062
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L659:
	movl	-1544(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1804
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2063
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L1867:
	movq	-1520(%rbp), %rdi
	movzbl	%r13b, %eax
	leaq	2(%r12), %r15
	movq	112(%rdi), %rdx
	cmpl	$110, (%rdx,%rax,4)
	jne	.L1806
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L2064
.L702:
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	%r13b, (%rax)
	jmp	.L663
.L725:
	cmpb	$0, -1536(%rbp)
	je	.L727
	movq	-1520(%rbp), %rax
	movzbl	%bl, %esi
	movq	112(%rax), %rdi
	movzbl	-1576(%rbp), %eax
	cmpb	(%rdi,%rsi,4), %al
	jne	.L727
	cmpq	%rdx, -1096(%rbp)
	je	.L2065
	movzbl	-1576(%rbp), %eax
	movzbl	-1536(%rbp), %r15d
	leaq	1(%rdx), %rsi
	movq	%rsi, -1104(%rbp)
	movb	%r15b, -1592(%rbp)
	movb	%al, (%rdx)
	jmp	.L723
.L2057:
	movzbl	%dl, %esi
	cmpl	%esi, %ebx
	jne	.L1133
	movq	-1680(%rbp), %rax
	cmpb	$0, 1(%rax)
	leaq	1(%rax), %rsi
	jne	.L740
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L735:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %ebx
.L736:
	movzbl	(%rsi), %eax
	addq	$1, %r12
	subl	$1, %r9d
	cmpl	%ebx, %eax
	jne	.L739
	addq	$1, %rsi
	cmpb	$0, (%rsi)
	je	.L1869
	testl	%r9d, %r9d
	je	.L734
.L740:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jb	.L735
	movq	%r13, %rdi
	movq	%rsi, -1616(%rbp)
	movl	%r9d, -1544(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movl	-1544(%rbp), %r9d
	movq	-1616(%rbp), %rsi
	jne	.L736
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%fs:(%rdx), %eax
	movl	%eax, -1512(%rbp)
.L739:
	cmpb	$0, (%rsi)
	jne	.L734
.L1869:
	movq	-1680(%rbp), %rax
	movzbl	(%rax), %edx
.L1049:
	testb	%dl, %dl
	je	.L745
	movq	-1680(%rbp), %r15
.L741:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L2066
	leaq	1(%rax), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%dl, (%rax)
.L744:
	addq	$1, %r15
	movzbl	(%r15), %edx
	testb	%dl, %dl
	jne	.L741
.L745:
	testl	%r14d, %r14d
	jle	.L1870
	movl	%r9d, %r14d
.L1870:
	movl	$1, %r15d
	jmp	.L723
.L112:
	testb	$4, -1528(%rbp)
	je	.L125
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L126
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L127
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L128:
	movq	(%rax), %rax
.L129:
	movw	%r15w, (%rax)
	movq	-1504(%rbp), %rbx
	xorl	%r12d, %r12d
	jmp	.L31
.L1910:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L1875
.L1798:
	movq	%r14, %r13
	jmp	.L41
.L2031:
	movl	%r14d, %ecx
	movl	%ebx, %r14d
	jmp	.L448
.L218:
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L222:
	subl	$1, %r8d
	je	.L2067
	cmpl	$47, %edx
	ja	.L223
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-1664(%rbp), %rsi
	movl	-1668(%rbp), %r8d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L323:
	subl	$1, %r8d
	je	.L2068
	cmpl	$47, %edx
	ja	.L324
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	%r13, %rax
	movl	%r14d, %r13d
	movq	%rax, %r14
	movq	-1104(%rbp), %rax
.L459:
	movq	%rax, %rcx
	movl	$10, %r8d
	jmp	.L475
.L231:
	subq	$288, %rsp
	movq	-1648(%rbp), %rsi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L232
.L333:
	subq	$288, %rsp
	movq	-1648(%rbp), %rsi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L334
.L2139:
	fstp	%st(0)
.L836:
	movq	-1496(%rbp), %rax
	cmpq	%rax, -1088(%rbp)
	jne	.L1873
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1959:
	cmpl	$1, -1544(%rbp)
	jle	.L2069
	movl	-1544(%rbp), %eax
	subl	$2, %eax
	leaq	2(%r15,%rax), %rbx
	movq	-1536(%rbp), %r15
	jmp	.L211
.L208:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L209:
	addq	$1, %r15
	cmpq	%rbx, %r15
	je	.L2070
.L211:
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jb	.L208
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L209
	movl	%eax, %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	-1504(%rbp), %rbx
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L31
.L834:
	testb	$3, -1528(%rbp)
	leaq	-1496(%rbp), %rsi
	je	.L863
	call	__strtod_internal
	testl	%ebx, %ebx
	jne	.L836
	movq	-1496(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L1835
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L864
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L865
	movl	%ecx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L866:
	movq	(%rax), %rax
.L867:
	movsd	%xmm0, (%rax)
	movq	-1088(%rbp), %rsi
	jmp	.L850
.L2041:
	movq	%rcx, %rdi
	movq	%rcx, -1600(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movq	-1600(%rbp), %rcx
	jne	.L991
	movq	%r13, -1656(%rbp)
	movl	%eax, %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r15, %rdx
	movl	-1576(%rbp), %r12d
	movq	-1592(%rbp), %r15
	movq	%rcx, %r14
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L985:
	cmpq	%r15, %rdx
	je	.L1846
	movl	-1552(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L995
	movq	-1504(%rbp), %rbx
	movq	%rdx, %r15
	jmp	.L31
.L1943:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L389
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L391
.L332:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1962:
	movq	%r14, %rdi
	movq	%rcx, -1576(%rbp)
	movl	%r8d, -1552(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r15d
	movl	-1552(%rbp), %r8d
	movq	-1576(%rbp), %rcx
	jne	.L203
	movq	%r13, -1640(%rbp)
	movl	%eax, %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r8d, %r12d
	movq	%rbx, %r15
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L206:
	testl	%r12d, %r12d
	je	.L213
	movq	-1584(%rbp), %rbx
	movq	%rcx, %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	cmpq	-1640(%rbp), %rsi
	je	.L213
	movq	%rcx, -1536(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1536(%rbp), %rcx
	je	.L213
	movq	%rax, (%rbx)
.L213:
	addl	$1, -1524(%rbp)
	movq	-1504(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%rcx, -1656(%rbp)
	movq	$0, -1584(%rbp)
	jmp	.L31
.L230:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L2053:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L1988:
	leaq	(%r11,%r11), %r9
	movq	%r11, -1640(%rbp)
	movl	%r8d, -1632(%rbp)
	movq	%rcx, -1616(%rbp)
	movq	%r10, -1600(%rbp)
	movq	%r9, %rsi
	movq	%r11, -1592(%rbp)
	movq	%r9, -1552(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1552(%rbp), %r9
	movq	-1592(%rbp), %r11
	movq	-1600(%rbp), %r10
	movq	-1616(%rbp), %rcx
	movl	-1632(%rbp), %r8d
	movq	-1640(%rbp), %rdx
	je	.L2071
	movq	-1584(%rbp), %rdi
	leaq	(%rax,%r11), %r12
	movq	%r9, %r11
	movq	%rax, (%rdi)
	jmp	.L305
.L1957:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movq	%r14, %r13
	movl	%edi, %fs:(%rax)
.L194:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1966:
	subl	$1, -1544(%rbp)
	jmp	.L1061
.L1964:
	movq	%r14, %r13
	movq	__libc_errno@gottpoff(%rip), %rdx
	jmp	.L642
.L2052:
	movq	%r13, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %ebx
	jne	.L765
	movl	%r14d, -1544(%rbp)
	movq	%r13, %r14
	movl	%eax, %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L721:
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	je	.L2072
	movsbq	-1552(%rbp), %rdi
	subq	-1088(%rbp), %rax
	testl	$1024, -1528(%rbp)
	movq	%rdi, -1600(%rbp)
	jne	.L2073
	cmpq	-1600(%rbp), %rax
	je	.L1831
	testl	$2048, -1528(%rbp)
	movq	%r12, %r15
	je	.L663
.L1064:
	movsbl	-1552(%rbp), %edx
	addl	$2, %edx
	movslq	%edx, %rdx
	cmpq	%rax, %rdx
	je	.L1832
.L1872:
	movq	%r12, %r15
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L1128:
	movb	$101, -1576(%rbp)
	xorl	%ebx, %ebx
	jmp	.L703
.L1986:
	movq	-1536(%rbp), %rdx
	movl	-1512(%rbp), %eax
	movq	%r14, %r13
	movl	%eax, %fs:(%rdx)
.L299:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L603:
	testl	$512, -1528(%rbp)
	jne	.L616
	movl	-1668(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L617
.L1892:
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L618
	movl	%ecx, %edx
	addq	-1432(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L619:
	movq	(%rdx), %rdx
	movl	%eax, (%rdx)
	jmp	.L551
.L1961:
	movq	%r13, -1640(%rbp)
	movl	%r8d, %r12d
	movl	%r15d, %r13d
	movq	%rbx, %r15
	jmp	.L206
.L2066:
	movq	-1568(%rbp), %rdi
	movsbl	%dl, %esi
	movl	%r9d, -1544(%rbp)
	call	char_buffer_add_slow
	movl	-1544(%rbp), %r9d
	jmp	.L744
.L1935:
	movq	-1552(%rbp), %rcx
	movl	-1592(%rbp), %ebx
	movq	-1640(%rbp), %rsi
	movq	%rdx, -1600(%rbp)
	addl	%ecx, %ebx
	movslq	%ebx, %rbx
	cmpq	%rsi, %rcx
	cmovge	%rsi, %rbx
	addq	%rsi, %rbx
	leaq	0(,%rbx,4), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1600(%rbp), %rdx
	je	.L2074
	movq	-1584(%rbp), %rsi
	movq	%rbx, -1640(%rbp)
	movq	%rax, (%rsi)
	addq	%rdx, %rax
	movq	%rax, -1624(%rbp)
	jmp	.L250
.L552:
	testl	$512, -1528(%rbp)
	jne	.L565
	movl	-1668(%rbp), %edx
	testl	%edx, %edx
	je	.L1892
	movq	-1664(%rbp), %rdi
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L570:
	subl	$1, -1668(%rbp)
	je	.L1906
	cmpl	$47, %ecx
	ja	.L571
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L2056:
	leal	-43(%rbx), %esi
	andl	$-3, %esi
	jne	.L727
	cmpq	%rdx, -1096(%rbp)
	je	.L2075
	leaq	1(%rdx), %rsi
	movq	%rsi, -1104(%rbp)
	movb	%bl, (%rdx)
	jmp	.L723
.L2060:
	movq	%r14, %r13
	jmp	.L41
.L2040:
	movq	%r13, -1656(%rbp)
	movq	%r15, %rdx
	movl	-1576(%rbp), %r12d
	movq	-1592(%rbp), %r15
	movl	%ebx, %r13d
	movq	%rcx, %r14
	jmp	.L985
.L2004:
	movq	-1568(%rbp), %rdi
	movl	$48, %esi
	movl	%r8d, -1536(%rbp)
	call	char_buffer_add_slow
	movl	-1536(%rbp), %r8d
	jmp	.L530
.L1991:
	movq	%r12, -1656(%rbp)
	movl	-1544(%rbp), %r12d
	movq	%rcx, %r15
	movq	%r11, -1640(%rbp)
.L302:
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	subq	$1, %r15
	call	_IO_sputbackc
	jmp	.L304
.L165:
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L181
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L182
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L183:
	movq	(%rax), %rax
	movq	%rax, -1656(%rbp)
.L184:
	cmpq	$0, -1656(%rbp)
	jne	.L164
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L270:
	movl	-1668(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L286
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L287
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L288:
	movq	(%rax), %rax
	movq	%rax, -1656(%rbp)
.L289:
	cmpq	$0, -1656(%rbp)
	jne	.L269
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L890:
	movl	-1668(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L906
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L907
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L908:
	movq	(%rax), %rax
	movq	%rax, -1624(%rbp)
.L909:
	cmpq	$0, -1624(%rbp)
	jne	.L889
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-1664(%rbp), %rsi
	movl	-1668(%rbp), %r8d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L339:
	subl	$1, %r8d
	je	.L2076
	cmpl	$47, %edx
	ja	.L340
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L2011:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movq	%r14, %r13
	movl	%edi, %fs:(%rax)
.L957:
	movl	-1524(%rbp), %edi
	movl	$-1, %eax
	testl	%edi, %edi
	cmovne	%edi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L2058:
	movq	-1568(%rbp), %rdi
	movsbl	%dl, %esi
	movq	%r10, -1616(%rbp)
	movl	%r9d, -1544(%rbp)
	call	char_buffer_add_slow
	movl	-1544(%rbp), %r9d
	movq	-1616(%rbp), %r10
	jmp	.L762
.L125:
	movl	-1528(%rbp), %r12d
	andl	$512, %r12d
	jne	.L138
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L139
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L140
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L141:
	movq	(%rax), %rax
.L142:
	movl	%r15d, (%rax)
	movq	-1504(%rbp), %rbx
	jmp	.L31
.L2037:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movq	%r14, %r13
	movl	%esi, %fs:(%rax)
.L980:
	movl	-1524(%rbp), %esi
	movl	$-1, %eax
	testl	%esi, %esi
	cmovne	%esi, %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L918:
	movl	-1668(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L934
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L935
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L936:
	movq	(%rax), %rax
	movq	%rax, -1656(%rbp)
.L937:
	cmpq	$0, -1656(%rbp)
	jne	.L889
	movq	%r14, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	-1640(%rbp), %rax
	movl	%ebx, -1616(%rbp)
	movq	%rcx, -1600(%rbp)
	leaq	(%rax,%rax), %r11
	leaq	1(%rax), %r13
	movq	%r11, %rbx
	jmp	.L987
.L2077:
	cmpq	%rbx, %r13
	jnb	.L989
	movq	-1584(%rbp), %rax
	movq	%r13, %rbx
	movq	(%rax), %rdi
.L987:
	movq	%rbx, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2077
	movq	-1640(%rbp), %r13
	movq	-1584(%rbp), %rsi
	movq	%rbx, %r11
	movq	-1600(%rbp), %rcx
	movl	-1616(%rbp), %ebx
	movq	%r11, -1640(%rbp)
	movq	%rax, (%rsi)
	addq	%rax, %r13
	jmp	.L986
.L233:
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L237:
	subl	$1, %r8d
	je	.L2078
	cmpl	$47, %edx
	ja	.L238
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %edi
	movl	%edi, %fs:(%rax)
.L396:
	movq	-1104(%rbp), %rcx
	movl	$16, %r8d
	movl	$-1, %r13d
	jmp	.L475
.L995:
	testl	$8448, -1528(%rbp)
	movq	-1656(%rbp), %rax
	leaq	1(%rax), %r15
	movb	$0, (%rax)
	je	.L997
	movq	-1584(%rbp), %rbx
	movq	%r15, %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	cmpq	-1640(%rbp), %rsi
	je	.L997
	movq	%rdx, -1536(%rbp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1536(%rbp), %rdx
	je	.L997
	movq	%rax, (%rbx)
.L997:
	movq	%r15, -1656(%rbp)
	addl	$1, -1524(%rbp)
	xorl	%r12d, %r12d
	movq	-1504(%rbp), %rbx
	movq	%rdx, %r15
	movq	$0, -1584(%rbp)
	jmp	.L31
.L113:
	movq	-1664(%rbp), %rsi
	movl	-1668(%rbp), %r8d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L117:
	subl	$1, %r8d
	je	.L2079
	cmpl	$47, %edx
	ja	.L118
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L248
	movq	%r14, %r13
	jmp	.L246
.L1151:
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L2005:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$84, %fs:(%rax)
	jmp	.L41
.L863:
	call	__strtof_internal
	testl	%ebx, %ebx
	jne	.L836
	movq	-1496(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L1836
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L876
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L877
	movl	%ecx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L878:
	movq	(%rax), %rax
.L879:
	movss	%xmm0, (%rax)
	movq	-1088(%rbp), %rsi
	jmp	.L850
.L1971:
	movq	%r14, %r13
	jmp	.L41
.L1983:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L349
	movq	%r14, %r13
	jmp	.L347
.L2003:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L528
	movq	%r14, %r13
	jmp	.L41
.L2015:
	movq	%r12, %r9
	movq	%r15, %r12
	movq	-1576(%rbp), %r15
	jmp	.L965
.L2007:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$84, %fs:(%rax)
	jmp	.L41
.L1847:
	movq	%r14, %r13
	jmp	.L41
.L539:
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L543:
	subl	$1, %r8d
	je	.L1902
	cmpl	$47, %ecx
	ja	.L544
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L284:
	subq	$288, %rsp
	movq	-1648(%rbp), %rdi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rdi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L285
.L734:
	testb	$-128, -1528(%rbp)
	je	.L1136
	movq	-1680(%rbp), %rax
	movq	-1560(%rbp), %rdx
	subq	%rax, %rsi
	testq	%rsi, %rsi
	jle	.L1137
	movzbl	(%rdx), %edi
	cmpb	%dil, (%rax)
	jne	.L1138
.L749:
	addq	$1, %rdx
	movq	%rdx, %rdi
	subq	-1560(%rbp), %rdi
	cmpq	%rsi, %rdi
	jge	.L747
	movq	-1680(%rbp), %rax
	movzbl	(%rax,%rdi), %eax
	cmpb	%al, (%rdx)
	je	.L749
.L1139:
	movq	%rdx, %rsi
	jmp	.L757
.L932:
	subq	$288, %rsp
	movq	-1648(%rbp), %rsi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L933
.L931:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L283:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L2009:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L366
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L352
.L904:
	subq	$288, %rsp
	movq	-1648(%rbp), %rdi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rdi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L905
.L903:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L919:
	movq	-1664(%rbp), %rsi
	movl	-1668(%rbp), %r8d
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L923:
	subl	$1, %r8d
	je	.L2080
	cmpl	$47, %edx
	ja	.L924
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L2042:
	movq	%r15, %rdx
	movl	-1576(%rbp), %r12d
	movq	-1592(%rbp), %r15
	movq	%r13, -1656(%rbp)
	movq	%rcx, %r14
	movl	%ebx, %r13d
.L993:
	subq	$1, %rdx
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	movq	%rdx, -1536(%rbp)
	call	_IO_sputbackc
	movq	-1536(%rbp), %rdx
	jmp	.L985
.L616:
	movl	-1668(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L629
.L1895:
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L630
	movl	%ecx, %edx
	addq	-1432(%rbp), %rdx
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L631:
	movq	(%rdx), %rdx
	movb	%al, (%rdx)
	jmp	.L551
.L2002:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L2081
	cmpl	$108, 1020(%rbx)
	movq	__libc_errno@gottpoff(%rip), %rdx
	movq	%r14, %r13
	movl	%fs:(%rdx), %eax
	jne	.L41
.L1048:
	movl	%eax, %fs:(%rdx)
	jmp	.L41
.L2070:
	movl	%eax, %r13d
	movq	-1504(%rbp), %rbx
	jmp	.L31
.L2006:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L263
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1544(%rbp), %r12d
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
.L261:
	testl	%r12d, %r12d
	jne	.L1873
	testl	$8448, -1528(%rbp)
	je	.L267
	movq	-1584(%rbp), %rbx
	movq	-1624(%rbp), %rsi
	movq	(%rbx), %rdi
	subq	%rdi, %rsi
	movq	%rsi, %rax
	sarq	$2, %rax
	cmpq	-1640(%rbp), %rax
	je	.L267
	call	realloc@PLT
	testq	%rax, %rax
	je	.L267
	movq	%rax, (%rbx)
.L267:
	addl	$1, -1524(%rbp)
	movq	-1504(%rbp), %rbx
	movq	$0, -1584(%rbp)
	jmp	.L31
.L138:
	movl	-1668(%rbp), %eax
	testl	%eax, %eax
	jne	.L151
	movl	-1448(%rbp), %edx
	cmpl	$47, %edx
	ja	.L152
	movl	%edx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -1448(%rbp)
.L153:
	movq	(%rax), %rax
.L154:
	movb	%r15b, (%rax)
	xorl	%r12d, %r12d
	movq	-1504(%rbp), %rbx
	jmp	.L31
.L1764:
	movq	%r14, %r13
	jmp	.L41
.L1129:
	movb	$101, -1576(%rbp)
	movl	$1, %ebx
	jmp	.L703
.L565:
	movl	-1668(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L1895
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L582:
	subl	$1, -1668(%rbp)
	je	.L1908
	cmpl	$47, %ecx
	ja	.L583
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L1980:
	leaq	-1496(%rbp), %rsi
	call	__strtof128_internal
	testl	%ebx, %ebx
	jne	.L836
	movq	-1496(%rbp), %rdx
	cmpq	-1088(%rbp), %rdx
	je	.L1833
	cmpl	$0, -1668(%rbp)
	jne	.L838
	movl	-1448(%rbp), %ecx
	cmpl	$47, %ecx
	ja	.L839
	movl	%ecx, %eax
	addq	-1432(%rbp), %rax
	addl	$8, %ecx
	movl	%ecx, -1448(%rbp)
.L840:
	movq	(%rax), %rax
.L841:
	movaps	%xmm0, (%rax)
	movq	-1088(%rbp), %rsi
	jmp	.L850
.L1965:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L1866
	movq	%r14, %r13
	jmp	.L643
.L1993:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L109
	movq	%r14, %r13
	jmp	.L107
.L2000:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L2082
	movq	%r14, %r13
	jmp	.L41
.L2001:
	movq	%r14, %rdi
	movl	%r8d, -1536(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	-1536(%rbp), %r8d
	jne	.L2083
	cmpl	$105, 1020(%rbx)
	movq	%r14, %r13
	jne	.L41
	movq	-1520(%rbp), %rdi
	movq	__libc_errno@gottpoff(%rip), %rdx
	movq	112(%rdi), %rcx
	movl	%fs:(%rdx), %eax
	cmpl	$108, 1020(%rcx)
	je	.L1048
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	%r12, -1656(%rbp)
	movq	%r11, -1640(%rbp)
	movq	%rcx, %r15
	movl	-1544(%rbp), %r12d
	jmp	.L304
.L2050:
	movq	-1568(%rbp), %rdi
	movl	%ebx, %esi
	call	char_buffer_add_slow
	testl	%r14d, %r14d
	je	.L1825
	movb	$1, -1536(%rbp)
	jmp	.L722
.L271:
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L275:
	subl	$1, %r8d
	je	.L2084
	cmpl	$47, %edx
	ja	.L276
	addl	$8, %edx
	movl	$1, %esi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L891:
	movq	-1664(%rbp), %rdi
	movl	-1668(%rbp), %r8d
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L895:
	subl	$1, %r8d
	je	.L2085
	cmpl	$47, %edx
	ja	.L896
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L934:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L938:
	subl	$1, -1668(%rbp)
	je	.L2086
	cmpl	$47, %edx
	ja	.L939
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L1979:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L1978:
	movq	-1568(%rbp), %rdi
	xorl	%esi, %esi
	call	char_buffer_add_slow
	jmp	.L832
.L126:
	movq	-1664(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L130:
	subl	$1, -1668(%rbp)
	je	.L2087
	cmpl	$47, %edx
	ja	.L131
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	-1584(%rbp), %rax
	addq	$1, %r13
	movl	%r8d, -1576(%rbp)
	movq	%r13, %rsi
	movq	%rcx, -1552(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1101
	movq	-1584(%rbp), %rsi
	movq	-1552(%rbp), %rcx
	movl	-1576(%rbp), %r8d
	movq	%rax, (%rsi)
	addq	%rcx, %rax
	jmp	.L199
.L2022:
	movl	-1552(%rbp), %eax
	xorl	%esi, %esi
	movq	%r14, -1600(%rbp)
	movq	%r15, -1576(%rbp)
	movl	%r13d, -1592(%rbp)
	movq	%rsi, %r14
	movl	%eax, -1696(%rbp)
	leaq	-1392(%rbp), %rax
	movq	%rax, -1536(%rbp)
	leaq	-1480(%rbp), %rax
	movq	%rax, -1632(%rbp)
	leaq	-1472(%rbp), %rax
	movq	%rax, -1616(%rbp)
.L407:
	movq	-1608(%rbp), %rax
	movq	-1688(%rbp), %rsi
	leal	48(%r14), %edi
	movq	224(%rax,%r14,8), %rbx
	movq	-1536(%rbp), %rax
	movq	%rbx, (%rax,%r14,8)
	call	__towctrans
	movq	-1632(%rbp), %rsi
	movq	-1616(%rbp), %rdi
	movq	$0, (%rsi)
	movq	%rsi, %rdx
	movl	%eax, %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %r12
	je	.L403
	movl	-1552(%rbp), %r10d
	xorl	%r13d, %r13d
	movq	%rbx, %r15
	testl	%r10d, %r10d
	jle	.L405
	movl	%r13d, %eax
	movq	%r12, %r13
	movq	%rbx, %r12
	movl	%eax, %ebx
.L406:
	movq	%r15, %rdi
	addl	$1, %ebx
	call	strlen@PLT
	cmpl	-1552(%rbp), %ebx
	leaq	1(%r15,%rax), %r15
	jne	.L406
	movq	%r12, %rbx
	movq	%r13, %r12
.L405:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	subq	%rbx, %rdx
	leaq	31(%r12,%rdx), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1704(%rbp)
	call	__mempcpy@PLT
	movq	-1616(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	-1704(%rbp), %rcx
	movb	$0, (%rax)
	movq	%rcx, -1312(%rbp,%r14,8)
	addq	$1, %r14
	cmpq	$10, %r14
	jne	.L407
	movq	-1576(%rbp), %r15
	movl	-1592(%rbp), %r13d
	movq	-1600(%rbp), %r14
	jmp	.L402
.L1968:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L651
	movq	%r14, %r13
	jmp	.L41
.L1800:
	movq	%r14, %r13
	jmp	.L41
.L1987:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L301
	movq	%r14, %r13
	jmp	.L299
.L604:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L608:
	subl	$1, -1668(%rbp)
	je	.L1904
	cmpl	$47, %ecx
	ja	.L609
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L1120:
	movq	-1560(%rbp), %r12
	jmp	.L448
.L286:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L290:
	subl	$1, -1668(%rbp)
	je	.L2088
	cmpl	$47, %edx
	ja	.L291
	addl	$8, %edx
	movl	$1, %esi
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L906:
	movq	-1664(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L910:
	subl	$1, -1668(%rbp)
	je	.L2089
	cmpl	$47, %edx
	ja	.L911
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L989:
	testl	$8192, -1528(%rbp)
	movq	-1600(%rbp), %r13
	jne	.L1160
	movq	-1584(%rbp), %rax
	movq	-1640(%rbp), %rdi
	addl	$1, -1524(%rbp)
	movq	$0, -1584(%rbp)
	movq	(%rax), %rax
	movb	$0, -1(%rax,%rdi)
	jmp	.L41
.L181:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L185:
	subl	$1, -1668(%rbp)
	je	.L2090
	cmpl	$47, %edx
	ja	.L186
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L185
.L2036:
	movq	%r13, %rdi
	movl	%r14d, %r13d
	movl	%r13d, %ebx
	movq	%rdi, %r14
.L1043:
	cmpq	%r12, -1560(%rbp)
	movl	%ebx, %r13d
	jnb	.L459
	movq	%r12, %rdx
	subq	-1560(%rbp), %rdx
	subq	%rdx, %rax
	cmpl	$-1, %ebx
	movq	%rax, -1104(%rbp)
	je	.L460
	movzbl	%bl, %esi
	movq	%r14, %rdi
	subq	$1, %r15
	call	_IO_sputbackc
.L460:
	leaq	-1(%r12), %r13
	cmpq	%r13, -1560(%rbp)
	jnb	.L461
	movq	%r13, %rbx
.L462:
	movzbl	(%rbx), %esi
	movq	%r14, %rdi
	subq	$1, %rbx
	call	_IO_sputbackc
	cmpq	%rbx, -1560(%rbp)
	jne	.L462
	movq	-1560(%rbp), %rax
	subq	%r12, %rax
	addq	$1, %rax
	addq	%rax, %r15
	addq	%rax, %r13
.L461:
	movzbl	0(%r13), %r13d
	movq	-1104(%rbp), %rax
	jmp	.L459
.L166:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L170:
	subl	$1, -1668(%rbp)
	je	.L2091
	cmpl	$47, %edx
	ja	.L171
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L170
.L2081:
	movzbl	%al, %eax
	cmpl	$108, (%rbx,%rax,4)
	je	.L526
.L1797:
	movl	%r13d, %r15d
	movq	%r14, %r13
	jmp	.L1069
.L179:
	subq	$288, %rsp
	movq	-1648(%rbp), %rdi
	movl	$1, %edx
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	$0, (%rax)
	movq	%rdi, 8(%rax)
	movq	%rax, -1648(%rbp)
	xorl	%eax, %eax
	jmp	.L180
.L178:
	testl	$8192, -1528(%rbp)
	movl	$-1, %eax
	movq	%r14, %r13
	cmove	-1524(%rbp), %eax
	movl	%eax, -1524(%rbp)
	jmp	.L41
.L1967:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L649
.L403:
	movq	-1576(%rbp), %r15
	movl	-1592(%rbp), %r13d
	movq	-1600(%rbp), %r14
	movq	$0, -1688(%rbp)
	jmp	.L402
.L1833:
	movq	%r14, %r13
	jmp	.L41
.L1160:
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L1771:
	movq	%r14, %r13
	jmp	.L41
.L1834:
	fstp	%st(0)
	movq	%r14, %r13
	jmp	.L41
.L1958:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L196
	movq	%r14, %r13
	jmp	.L194
.L2071:
	movq	-1584(%rbp), %rax
	addq	$1, %r11
	movq	%rdx, -1632(%rbp)
	movq	%r11, %rsi
	movl	%r8d, -1616(%rbp)
	movq	%rcx, -1600(%rbp)
	movq	%r10, -1592(%rbp)
	movq	%r11, -1552(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1552(%rbp), %r11
	movq	-1592(%rbp), %r10
	movq	-1600(%rbp), %rcx
	movl	-1616(%rbp), %r8d
	movq	-1632(%rbp), %rdx
	je	.L2092
	movq	-1584(%rbp), %rsi
	leaq	(%rax,%rdx), %r12
	movq	%rax, (%rsi)
	jmp	.L305
.L1843:
	movq	%r14, %r13
	jmp	.L41
.L1137:
	xorl	%edi, %edi
.L747:
	cmpq	%rdi, %rsi
	je	.L750
	movq	%rdx, %rsi
	jmp	.L746
.L2021:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L398
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L396
.L1774:
	movl	-1544(%rbp), %r12d
	jmp	.L261
.L139:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%esi, %esi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L143:
	subl	$1, -1668(%rbp)
	je	.L2093
	cmpl	$47, %edx
	ja	.L144
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L143
.L2038:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L2094
	movq	%r14, %r13
	jmp	.L980
.L2051:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movl	%r14d, -1544(%rbp)
	movq	%r13, %r14
	movl	%ebx, %r13d
	movl	%esi, %fs:(%rax)
	jmp	.L721
.L2017:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$84, %fs:(%rax)
	jmp	.L41
.L2074:
	movq	-1584(%rbp), %rbx
	leaq	4(%rdx), %rsi
	addq	$1, -1640(%rbp)
	movq	(%rbx), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1105
	movq	-1600(%rbp), %rdx
	movq	%rax, (%rbx)
	addq	%rdx, %rax
	movq	%rax, -1624(%rbp)
	jmp	.L250
.L151:
	movq	-1664(%rbp), %rdi
	xorl	%esi, %esi
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rdx
	movq	-1304(%rbp), %rcx
	movd	%xmm0, %eax
	xorl	%edi, %edi
	movq	%rdx, -1296(%rbp)
	movd	%xmm0, %edx
.L155:
	subl	$1, -1668(%rbp)
	je	.L2095
	cmpl	$47, %edx
	ja	.L156
	addl	$8, %edx
	movl	$1, %edi
	jmp	.L155
.L1846:
	movq	%r14, %r13
	jmp	.L41
.L1825:
	movl	%r14d, -1544(%rbp)
	movq	%r13, %r14
	movl	%ebx, %r13d
	jmp	.L721
.L2012:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L2096
	movq	%r14, %r13
	jmp	.L957
.L629:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L633:
	subl	$1, -1668(%rbp)
	je	.L1908
	cmpl	$47, %ecx
	ja	.L634
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L633
.L2049:
	cmpq	%rdx, -1096(%rbp)
	je	.L2097
	leaq	1(%rdx), %rax
	movq	%rax, -1104(%rbp)
	movb	%r13b, (%rdx)
.L711:
	movl	-1528(%rbp), %eax
	andb	$127, %al
	orb	$8, %ah
	cmpl	$-1, %r13d
	movl	%eax, -1528(%rbp)
	je	.L2098
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2099
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L715:
	addq	$1, %r12
.L713:
	movl	-1544(%rbp), %eax
	movq	-1104(%rbp), %rdx
	testl	%eax, %eax
	jle	.L1131
	subl	$1, %eax
	movb	$112, -1576(%rbp)
	xorl	%ebx, %ebx
	movl	%eax, -1544(%rbp)
	jmp	.L703
.L1811:
	movq	%r14, %r13
	jmp	.L41
.L1974:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L1868
	movq	%r14, %r13
	jmp	.L41
.L1809:
	movq	%r14, %r13
	jmp	.L41
.L1973:
	movq	-1568(%rbp), %rdi
	movsbl	%al, %esi
	call	char_buffer_add_slow
	jmp	.L671
.L1977:
	movl	-1544(%rbp), %eax
	testl	%eax, %eax
	jle	.L681
	subl	$1, %eax
	movl	%eax, -1544(%rbp)
.L681:
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	je	.L2100
	leaq	1(%rax), %rdx
	movq	%rdx, -1104(%rbp)
	movb	%r13b, (%rax)
.L683:
	cmpl	$0, -1544(%rbp)
	je	.L1812
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2101
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L685:
	movq	-1520(%rbp), %rdi
	movzbl	%al, %edx
	movq	112(%rdi), %rcx
	cmpl	$110, (%rcx,%rdx,4)
	jne	.L2102
	movl	-1544(%rbp), %edi
	testl	%edi, %edi
	jle	.L686
	subl	$1, %edi
	movl	%edi, -1544(%rbp)
.L686:
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	je	.L2103
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L688:
	cmpl	$0, -1544(%rbp)
	je	.L1813
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2104
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L690:
	movq	-1520(%rbp), %rsi
	movzbl	%al, %edx
	movq	112(%rsi), %rcx
	cmpl	$105, (%rcx,%rdx,4)
	jne	.L2105
	movl	-1544(%rbp), %esi
	testl	%esi, %esi
	jle	.L691
	subl	$1, %esi
	movl	%esi, -1544(%rbp)
.L691:
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	je	.L2106
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L693:
	cmpl	$0, -1544(%rbp)
	je	.L1814
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2107
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %eax
.L695:
	movq	-1520(%rbp), %rdi
	movzbl	%al, %edx
	movq	112(%rdi), %rcx
	cmpl	$116, (%rcx,%rdx,4)
	jne	.L2108
	movl	-1544(%rbp), %edi
	testl	%edi, %edi
	jle	.L696
	subl	$1, %edi
	movl	%edi, -1544(%rbp)
.L696:
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	je	.L2109
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L698:
	cmpl	$0, -1544(%rbp)
	je	.L1815
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2110
	leaq	1(%rax), %rdx
	leaq	7(%r12), %r15
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L701:
	movq	-1520(%rbp), %rsi
	movzbl	%r13b, %eax
	movq	112(%rsi), %rdx
	cmpl	$121, (%rdx,%rax,4)
	jne	.L2111
	movq	-1104(%rbp), %rax
	cmpq	-1096(%rbp), %rax
	movsbl	%r13b, %esi
	jne	.L702
	movq	-1568(%rbp), %rdi
	call	char_buffer_add_slow
	jmp	.L663
.L851:
	movq	-1664(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %eax
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L855:
	subl	$1, -1668(%rbp)
	je	.L2112
	cmpl	$47, %ecx
	ja	.L856
	addl	$8, %ecx
	movl	$1, %ebx
	jmp	.L855
.L617:
	movq	-1664(%rbp), %rdi
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm0
	movups	%xmm0, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm0, %edx
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm0, %ecx
.L621:
	subl	$1, -1668(%rbp)
	je	.L1906
	cmpl	$47, %ecx
	ja	.L622
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L621
.L864:
	movq	-1664(%rbp), %rdi
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm1
	movups	%xmm1, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm1, %eax
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm1, %ecx
.L868:
	subl	$1, -1668(%rbp)
	je	.L2113
	cmpl	$47, %ecx
	ja	.L869
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L868
.L1778:
	movq	%r14, %r13
	jmp	.L41
.L1808:
	movq	%r14, %r13
	jmp	.L41
.L1972:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L668
	movq	%r14, %r13
	jmp	.L41
.L1835:
	movq	%r14, %r13
	jmp	.L41
.L1806:
	movq	%r14, %r13
	jmp	.L41
.L2063:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L1867
	movq	%r14, %r13
	jmp	.L41
.L1804:
	movq	%r14, %r13
	jmp	.L41
.L2062:
	movq	-1568(%rbp), %rdi
	movsbl	%al, %esi
	call	char_buffer_add_slow
	jmp	.L659
.L1803:
	movq	%r14, %r13
	jmp	.L41
.L2061:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L656
	movq	%r14, %r13
	jmp	.L41
.L2059:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L654
.L2048:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L706
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L708
.L2047:
	movq	-1568(%rbp), %rdi
	movl	$48, %esi
	call	char_buffer_add_slow
	jmp	.L705
.L1970:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L666
.L876:
	movq	-1664(%rbp), %rsi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	movdqu	(%rsi), %xmm1
	movups	%xmm1, -1312(%rbp)
	movq	16(%rsi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm1, %eax
	movq	%rcx, -1296(%rbp)
	movd	%xmm1, %ecx
.L880:
	subl	$1, -1668(%rbp)
	je	.L2114
	cmpl	$47, %ecx
	ja	.L881
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L880
.L1836:
	movq	%r14, %r13
	jmp	.L41
.L1975:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L676
.L1152:
	movq	%rcx, %rdx
	jmp	.L950
.L2008:
	movq	-1640(%rbp), %rax
	leaq	0(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2115
	movq	-1584(%rbp), %rsi
	movq	%rax, (%rsi)
	movq	-1640(%rbp), %rsi
	addq	%rbx, %rax
	movq	%rax, -1624(%rbp)
	movq	%rsi, %rax
	addq	%rsi, %rax
	movq	%rax, -1640(%rbp)
	jmp	.L361
.L2019:
	movq	-1584(%rbp), %rax
	leaq	4(%rdx), %rsi
	addq	$1, -1640(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1600(%rbp), %rdx
	je	.L2116
	movq	-1584(%rbp), %rsi
	movq	%rax, (%rsi)
	addq	%rdx, %rax
	movq	%rax, -1624(%rbp)
	jmp	.L966
.L2064:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L663
.L877:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L878
.L2115:
	movq	-1584(%rbp), %rax
	leaq	4(%rbx), %rsi
	addq	$1, -1640(%rbp)
	movq	(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2117
	movq	-1584(%rbp), %rdi
	movq	%rax, (%rdi)
	addq	%rbx, %rax
	movq	%rax, -1624(%rbp)
	jmp	.L361
.L140:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L141
.L2104:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L690
	movq	%r14, %r13
	jmp	.L41
.L1813:
	movq	%r14, %r13
	jmp	.L41
.L2103:
	movq	-1568(%rbp), %rdi
	movsbl	%al, %esi
	call	char_buffer_add_slow
	jmp	.L688
.L2102:
	movq	%r14, %r13
	jmp	.L41
.L2101:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L685
	movq	%r14, %r13
	jmp	.L41
.L1812:
	movq	%r14, %r13
	jmp	.L41
.L2100:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L683
.L1976:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L678
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L663
.L2107:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L695
	movq	%r14, %r13
	jmp	.L41
.L1814:
	movq	%r14, %r13
	jmp	.L41
.L2106:
	movq	-1568(%rbp), %rdi
	movsbl	%al, %esi
	call	char_buffer_add_slow
	jmp	.L693
.L2105:
	movq	%r14, %r13
	jmp	.L41
.L2108:
	movq	%r14, %r13
	jmp	.L41
.L852:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L853
.L223:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L222
.L2067:
	testb	%dil, %dil
	je	.L226
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L226:
	testb	%sil, %sil
	je	.L227
	movq	%rcx, -1304(%rbp)
.L227:
	cmpl	$47, %eax
	ja	.L228
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L229:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L221
.L1981:
	movq	%r14, %r13
	jmp	.L41
.L618:
	movq	-1440(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L619
.L544:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L543
.L1838:
	movq	%r14, %r13
	jmp	.L41
.L920:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L921
.L1138:
	movq	-1560(%rbp), %rdx
	jmp	.L1139
.L592:
	movq	-1440(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L593
.L1133:
	movq	-1680(%rbp), %rsi
	jmp	.L739
.L924:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L923
.L2080:
	testb	%dil, %dil
	je	.L927
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L927:
	testb	%sil, %sil
	je	.L928
	movq	%rcx, -1304(%rbp)
.L928:
	cmpl	$47, %eax
	ja	.L929
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L930:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L922
.L896:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L895
.L2085:
	testb	%dil, %dil
	je	.L899
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L899:
	testb	%sil, %sil
	je	.L900
	movq	%rcx, -1304(%rbp)
.L900:
	cmpl	$47, %eax
	ja	.L901
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L902:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L894
.L929:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L930
.L901:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L902
.L114:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L115
.L750:
	movzbl	(%rdx), %esi
	cmpl	%esi, %ebx
	jne	.L1139
	cmpb	$0, 1(%rdx)
	leaq	1(%rdx), %rsi
	jne	.L1876
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L758:
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L2118
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %ebx
.L754:
	movzbl	(%rsi), %eax
	addq	$1, %r12
	subl	$1, %r9d
	cmpl	%ebx, %eax
	jne	.L757
	addq	$1, %rsi
	cmpb	$0, (%rsi)
	je	.L1051
.L1876:
	testl	%r9d, %r9d
	jne	.L758
	movl	%r14d, -1544(%rbp)
	movq	%r13, %r14
	movl	%ebx, %r13d
.L1050:
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	subq	$1, %r12
	call	_IO_sputbackc
	jmp	.L721
.L127:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L128
.L228:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L229
.L2118:
	movq	%r13, %rdi
	movq	%rsi, -1616(%rbp)
	movl	%r9d, -1544(%rbp)
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movl	-1544(%rbp), %r9d
	movq	-1616(%rbp), %rsi
	jne	.L754
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%fs:(%rdx), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L757
.L583:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L582
.L1908:
	testb	%r9b, %r9b
	je	.L637
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %edx
.L637:
	testb	%dil, %dil
	je	.L638
	movq	%rsi, -1304(%rbp)
.L638:
	cmpl	$47, %edx
	ja	.L639
	movl	%edx, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1312(%rbp)
.L640:
	movq	(%rcx), %rdx
	movb	%al, (%rdx)
	jmp	.L551
.L939:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L938
.L2086:
	testb	%dil, %dil
	je	.L942
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L942:
	testb	%sil, %sil
	je	.L943
	movq	%rcx, -1304(%rbp)
.L943:
	cmpl	$47, %eax
	ja	.L944
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L945:
	movq	(%rdx), %rax
	movq	%rax, -1656(%rbp)
	jmp	.L937
.L639:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1304(%rbp)
	jmp	.L640
.L944:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L945
.L2111:
	movq	%r14, %r13
	jmp	.L41
.L2110:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	je	.L2119
	leaq	7(%r12), %r15
	jmp	.L701
.L1815:
	movq	%r14, %r13
	jmp	.L41
.L2109:
	movq	-1568(%rbp), %rdi
	movsbl	%al, %esi
	call	char_buffer_add_slow
	jmp	.L698
.L2119:
	movq	%r14, %r13
	jmp	.L41
.L2075:
	movq	-1568(%rbp), %rdi
	movsbl	%bl, %esi
	call	char_buffer_add_slow
	jmp	.L723
.L2096:
	leaq	1(%r15), %r9
	jmp	.L960
.L320:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L321
.L1906:
	testb	%r9b, %r9b
	je	.L625
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %edx
.L625:
	testb	%dil, %dil
	je	.L626
	movq	%rsi, -1304(%rbp)
.L626:
	cmpl	$47, %edx
	ja	.L627
	movl	%edx, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1312(%rbp)
.L628:
	movq	(%rcx), %rdx
	movl	%eax, (%rdx)
	jmp	.L551
.L2069:
	movq	-1504(%rbp), %rbx
	movq	-1536(%rbp), %r15
	jmp	.L31
.L627:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1304(%rbp)
	jmp	.L628
.L156:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L155
.L2095:
	testb	%dil, %dil
	je	.L159
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L159:
	testb	%sil, %sil
	je	.L160
	movq	%rcx, -1304(%rbp)
.L160:
	cmpl	$47, %eax
	ja	.L161
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L162:
	movq	(%rdx), %rax
	jmp	.L154
.L1832:
	movq	%r14, %r13
	jmp	.L41
.L2044:
	movslq	%eax, %rdx
	addq	%rdx, %rdx
	jmp	.L1001
.L634:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L633
.L881:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L880
.L2114:
	testb	%r9b, %r9b
	je	.L884
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %eax
.L884:
	testb	%dil, %dil
	je	.L885
	movq	%rsi, -1304(%rbp)
.L885:
	cmpl	$47, %eax
	ja	.L886
	movl	%eax, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L887:
	movq	(%rcx), %rax
	jmp	.L879
.L2065:
	movl	-1600(%rbp), %esi
	movq	-1568(%rbp), %rdi
	call	char_buffer_add_slow
	movzbl	-1536(%rbp), %r15d
	movb	%r15b, -1592(%rbp)
	jmp	.L723
.L2094:
	leaq	1(%r15), %rdx
	jmp	.L983
.L2116:
	testl	$8192, -1528(%rbp)
	movq	%r14, %r13
	jne	.L1156
	movq	-1584(%rbp), %rax
	addl	$1, -1524(%rbp)
	movq	$0, -1584(%rbp)
	movq	(%rax), %rax
	movl	$0, -4(%rax,%rdx)
	jmp	.L41
.L1101:
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L886:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L887
.L1156:
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L1823:
	cmpl	$-1, %ebx
	movl	%r14d, -1544(%rbp)
	movq	%r13, %r14
	movl	%ebx, %r13d
	je	.L721
	jmp	.L1050
.L571:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L570
.L865:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L866
.L622:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L621
.L2073:
	testl	$2048, -1528(%rbp)
	jne	.L771
	cmpq	%rdi, %rax
	je	.L1167
	testb	$1, %r15b
	je	.L1872
.L1167:
	leaq	.LC0(%rip), %rdi
	call	__wctrans@PLT
	testq	%rax, %rax
	movq	%rax, -1688(%rbp)
	je	.L774
	cmpl	$-1, %r13d
	je	.L2120
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2121
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %r13d
.L778:
	addq	$1, %r12
.L776:
	movq	-1688(%rbp), %rsi
	movl	$46, %edi
	call	__towctrans
	leaq	-1312(%rbp), %rcx
	movq	-1104(%rbp), %rdx
	subq	-1088(%rbp), %rdx
	cmpq	-1600(%rbp), %rdx
	leaq	-1472(%rbp), %rsi
	movl	%eax, -1348(%rbp)
	leaq	187(%rcx), %r8
	movq	$0, -1472(%rbp)
	movq	%rcx, -1552(%rbp)
	movq	%rsi, -1616(%rbp)
	movq	%rsi, %rdx
	movl	%eax, %esi
	movq	%r8, %rdi
	movq	%r8, -1536(%rbp)
	sete	%bl
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %rsi
	movq	-1536(%rbp), %r8
	je	.L780
	movq	-1680(%rbp), %rdi
	movb	$0, -1125(%rbp,%rax)
	orq	$-1, %rcx
	xorl	%eax, %eax
	xorl	%edx, %edx
	repnz scasb
	movq	-1600(%rbp), %rdi
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rdi,%rax), %rcx
	movq	-1104(%rbp), %rax
	subq	-1088(%rbp), %rax
	cmpq	%rax, %rcx
	jne	.L781
	movq	-1680(%rbp), %rdi
	movq	%r8, %rsi
	call	strcmp
	xorl	%edx, %edx
	testl	%eax, %eax
	sete	%dl
.L781:
	orl	%edx, %ebx
.L782:
	testb	%bl, %bl
	jne	.L2122
.L774:
	movq	-1104(%rbp), %rax
	testq	%rax, %rax
	jne	.L830
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L2122:
	movzbl	-1528(%rbp), %eax
	xorl	%ebx, %ebx
	movq	%r12, -1696(%rbp)
	movq	%rbx, %r12
	shrb	$7, %al
	movb	%al, -1704(%rbp)
	movb	%al, -1632(%rbp)
	leaq	-1392(%rbp), %rax
	movq	%rax, -1536(%rbp)
.L787:
	imulq	$17, %r12, %rbx
	movl	%r12d, -1708(%rbp)
	addq	-1552(%rbp), %rbx
	cmpq	$10, %r12
	je	.L784
	movq	-1688(%rbp), %rsi
	leal	48(%r12), %edi
	call	__towctrans
	movq	-1536(%rbp), %rsi
	movq	%rbx, %rdi
	movl	%eax, (%rsi,%r12,4)
	movq	-1616(%rbp), %rsi
	movq	$0, (%rsi)
	movq	%rsi, %rdx
	movl	%eax, %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %rsi
	je	.L2123
.L785:
	movslq	-1708(%rbp), %rax
	leaq	-48(%rbp), %rdi
	imulq	$17, %rax, %rax
	addq	%rdi, %rax
	movb	$0, -1264(%rsi,%rax)
.L786:
	addq	$1, %r12
	cmpq	$11, %r12
	jne	.L787
	movsbl	-1576(%rbp), %eax
	movq	-1696(%rbp), %r12
	movl	%r13d, %ebx
	movl	%eax, -1696(%rbp)
.L788:
	movq	-1104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2124
	cmpb	$0, -1592(%rbp)
	je	.L790
	movzbl	-1576(%rbp), %eax
	cmpb	%al, -1(%rdx)
	je	.L2125
.L791:
	movl	-1544(%rbp), %eax
	movq	-1552(%rbp), %r8
	movl	$2147483647, %edx
	movb	%r15b, -1616(%rbp)
	movq	%r14, %r13
	movq	%r12, %r15
	testl	%eax, %eax
	movq	%r8, %r12
	cmovg	%eax, %edx
	xorl	%ecx, %ecx
	movl	%ebx, %eax
	movl	%edx, -1632(%rbp)
	movl	%ecx, -1536(%rbp)
.L823:
	subq	%r12, %r15
	movl	-1632(%rbp), %r14d
	movq	%r12, %rbx
	movq	%r15, -1688(%rbp)
	jmp	.L795
.L802:
	addq	$1, %rbx
	cmpb	$0, (%rbx)
	je	.L1826
	testl	%r14d, %r14d
	je	.L797
	movq	8(%r13), %rax
	cmpq	16(%r13), %rax
	jnb	.L2126
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r13)
	movzbl	(%rax), %eax
.L799:
	subl	$1, %r14d
.L795:
	movzbl	(%rbx), %edx
	movq	-1688(%rbp), %rdi
	leaq	(%rbx,%rdi), %r15
	cmpl	%eax, %edx
	je	.L802
	jmp	.L801
.L830:
	subq	-1088(%rbp), %rax
	cmpq	%rax, -1600(%rbp)
	jne	.L1872
	movq	%r14, %r13
	jmp	.L41
.L2123:
	movq	-1696(%rbp), %r12
	jmp	.L774
.L784:
	movq	-1688(%rbp), %rsi
	movl	$44, %edi
	call	__towctrans
	testl	%eax, %eax
	movl	%eax, -1352(%rbp)
	movq	-1616(%rbp), %rdx
	setne	%al
	andb	%al, -1632(%rbp)
	movq	-1536(%rbp), %rax
	movq	%rbx, %rdi
	movq	$0, (%rdx)
	movl	40(%rax), %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	movq	%rax, %rsi
	jne	.L785
	cmpb	$0, -1632(%rbp)
	je	.L786
	movq	-1560(%rbp), %rdi
	xorl	%eax, %eax
	movq	%rsi, %rcx
	repnz scasb
	notq	%rcx
	leaq	-1(%rcx), %rax
	cmpq	$16, %rax
	jbe	.L2127
	movb	$0, -1632(%rbp)
	jmp	.L786
.L2126:
	movq	%r13, %rdi
	call	__uflow
	cmpl	$-1, %eax
	jne	.L799
	movq	__libc_errno@gottpoff(%rip), %rdi
	movl	%fs:(%rdi), %esi
	movl	%esi, -1512(%rbp)
.L801:
	cmpb	$0, (%rbx)
	je	.L1826
	cmpq	%r12, %rbx
	ja	.L2128
.L819:
	addl	$1, -1536(%rbp)
	addq	$17, %r12
	movl	-1536(%rbp), %edi
	cmpl	$12, %edi
	jne	.L823
	movq	%r13, %r14
	movq	%r15, %r12
	movl	%eax, %r13d
.L815:
	cmpl	$-1, %r13d
	je	.L774
	movzbl	%r13b, %esi
	movq	%r14, %rdi
	subq	$1, %r12
	call	_IO_sputbackc
	jmp	.L774
.L797:
	cmpq	%rbx, %r12
	jnb	.L819
.L1072:
	movzbl	%al, %esi
	movq	%r13, %rdi
	subq	$1, %r15
	call	_IO_sputbackc
.L820:
	movq	%rbx, %r14
.L821:
	subq	$1, %r14
	cmpq	%r14, %r12
	je	.L2129
	movzbl	(%r14), %esi
	movq	%r13, %rdi
	call	_IO_sputbackc
	jmp	.L821
.L1826:
	movl	%eax, %ebx
	movq	%r13, %rax
	movl	%r14d, %r13d
	movq	%rax, %r14
	movl	-1544(%rbp), %eax
	movl	-1536(%rbp), %ecx
	movq	%r15, %r12
	movzbl	-1616(%rbp), %r15d
	testl	%eax, %eax
	cmovle	%eax, %r13d
	cmpl	$9, %ecx
	movl	%r13d, -1544(%rbp)
	jg	.L805
	addl	$48, %ecx
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	movzbl	%cl, %esi
	je	.L1871
	leaq	1(%rdx), %rsi
	movq	%rsi, -1104(%rbp)
	movb	%cl, (%rdx)
.L793:
	cmpl	$0, -1544(%rbp)
	je	.L1830
	cmpl	$-1, %ebx
	je	.L2130
	movq	8(%r14), %rax
	cmpq	16(%r14), %rax
	jnb	.L2131
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movzbl	(%rax), %ebx
.L826:
	addq	$1, %r12
	cmpl	$0, -1544(%rbp)
	jle	.L788
	subl	$1, -1544(%rbp)
	jmp	.L788
.L2128:
	cmpl	$-1, %eax
	je	.L820
	jmp	.L1072
.L2129:
	leaq	-1(%rbx), %rsi
	movq	%r12, %rax
	subq	%rsi, %rax
	addq	%rax, %r15
	leaq	-1(%r12), %rax
	subq	%rsi, %rax
	movzbl	(%rbx,%rax), %eax
	jmp	.L819
.L2131:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %ebx
	jne	.L826
	movl	%eax, %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L774
.L2130:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movl	%ebx, %r13d
	movl	%esi, %fs:(%rax)
	jmp	.L774
.L783:
.L1830:
	movl	%ebx, %r13d
	jmp	.L774
.L1871:
	movq	-1568(%rbp), %rdi
	call	char_buffer_add_slow
	jmp	.L793
.L805:
	cmpl	$11, %ecx
	jne	.L809
	testb	$1, %r15b
	movq	-1680(%rbp), %r13
	jne	.L809
.L807:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L2132
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	movsbl	%al, %esi
	je	.L2133
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L811:
	addq	$1, %r13
	jmp	.L807
.L2127:
	movq	-1552(%rbp), %rax
	movl	%ecx, %ecx
	movq	-1560(%rbp), %rsi
	addq	$170, %rax
	movq	%rax, %rdi
	rep movsb
	jmp	.L786
.L790:
	movq	%rdx, %rcx
	subq	-1088(%rbp), %rcx
	cmpq	%rcx, -1600(%rbp)
	jnb	.L791
	movq	-1520(%rbp), %rax
	movzbl	%bl, %ecx
	movq	112(%rax), %rsi
	movzbl	-1576(%rbp), %eax
	cmpb	(%rsi,%rcx,4), %al
	jne	.L791
	cmpq	-1096(%rbp), %rdx
	je	.L2134
	movzbl	-1576(%rbp), %eax
	leaq	1(%rdx), %rcx
	movb	$1, -1592(%rbp)
	movl	$1, %r15d
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
	jmp	.L793
.L2124:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L2134:
	movl	-1696(%rbp), %esi
	movq	-1568(%rbp), %rdi
	movl	$1, %r15d
	call	char_buffer_add_slow
	movb	$1, -1592(%rbp)
	jmp	.L793
.L2125:
	leal	-43(%rbx), %ecx
	andl	$-3, %ecx
	jne	.L791
	cmpq	-1096(%rbp), %rdx
	movsbl	%bl, %esi
	je	.L1871
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%bl, (%rdx)
	jmp	.L793
.L2133:
	movq	-1568(%rbp), %rdi
	call	char_buffer_add_slow
	jmp	.L811
.L2132:
	movl	$1, %r15d
	jmp	.L793
.L809:
	xorl	$1, %r15d
	testb	%r15b, -1704(%rbp)
	je	.L1829
	cmpl	$10, %ecx
	je	.L2135
.L1829:
	movl	%ebx, %r13d
	jmp	.L815
.L2135:
	movq	-1560(%rbp), %r13
.L813:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L2136
	movq	-1104(%rbp), %rdx
	cmpq	-1096(%rbp), %rdx
	movsbl	%al, %esi
	je	.L2137
	leaq	1(%rdx), %rcx
	movq	%rcx, -1104(%rbp)
	movb	%al, (%rdx)
.L817:
	addq	$1, %r13
	jmp	.L813
.L780:
	movq	-1680(%rbp), %rdi
	xorl	%eax, %eax
	movq	%rsi, %rcx
	movq	-1104(%rbp), %rdx
	repnz scasb
	notq	%rcx
	leaq	-1(%rcx), %rax
	cmpq	$16, %rax
	ja	.L774
	addq	-1600(%rbp), %rax
	subq	-1088(%rbp), %rdx
	movl	%ecx, %ecx
	movq	%r8, %rdi
	movq	-1680(%rbp), %rsi
	rep movsb
	cmpq	%rax, %rdx
	sete	%al
	orl	%eax, %ebx
	jmp	.L782
.L771:
	cmpq	-1600(%rbp), %rax
	jne	.L1064
	movq	%r14, %r13
	jmp	.L41
.L2072:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	movl	$12, %fs:(%rax)
	jmp	.L41
.L1831:
	movq	%r14, %r13
	jmp	.L41
.L161:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L162
.L2120:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movl	%esi, %fs:(%rax)
	jmp	.L776
.L2121:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L778
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L776
.L2137:
	movq	-1568(%rbp), %rdi
	call	char_buffer_add_slow
	jmp	.L817
.L2136:
	xorl	%r15d, %r15d
	jmp	.L793
.L935:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L936
.L336:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L337
.L131:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L130
.L2087:
	testb	%dil, %dil
	je	.L134
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L134:
	testb	%sil, %sil
	je	.L135
	movq	%rcx, -1304(%rbp)
.L135:
	cmpl	$47, %eax
	ja	.L136
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L137:
	movq	(%rdx), %rax
	jmp	.L129
.L605:
	movq	-1440(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L606
.L1904:
	testb	%r9b, %r9b
	je	.L612
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %edx
.L612:
	testb	%dil, %dil
	je	.L613
	movq	%rsi, -1304(%rbp)
.L613:
	cmpl	$47, %edx
	ja	.L614
	movl	%edx, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1312(%rbp)
.L615:
	movq	(%rcx), %rdx
	jmp	.L607
.L614:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1304(%rbp)
	jmp	.L615
.L558:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L557
.L136:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L137
.L1775:
	movq	%r14, %r13
	jmp	.L41
.L272:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L273
.L144:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L143
.L2093:
	testb	%dil, %dil
	je	.L147
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L147:
	testb	%sil, %sil
	je	.L148
	movq	%rcx, -1304(%rbp)
.L148:
	cmpl	$47, %eax
	ja	.L149
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L150:
	movq	(%rdx), %rax
	jmp	.L142
.L340:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L339
.L856:
	addq	$8, %rsi
	movl	$1, %r9d
	jmp	.L855
.L2112:
	testb	%bl, %bl
	je	.L859
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %eax
.L859:
	testb	%r9b, %r9b
	je	.L860
	movq	%rsi, -1304(%rbp)
.L860:
	cmpl	$47, %eax
	ja	.L861
	movl	%eax, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L862:
	movq	(%rcx), %rax
	movq	%rdi, %rsi
	jmp	.L854
.L2076:
	testb	%dil, %dil
	je	.L343
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L343:
	testb	%sil, %sil
	je	.L344
	movq	%rcx, -1304(%rbp)
.L344:
	cmpl	$47, %eax
	ja	.L345
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L346:
	movq	(%rdx), %rax
	movq	%rax, -1624(%rbp)
	jmp	.L338
.L149:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L150
.L861:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L862
.L345:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L346
.L182:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L183
.L219:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L220
.L596:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L595
.L238:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L237
.L2078:
	testb	%dil, %dil
	je	.L241
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L241:
	testb	%sil, %sil
	je	.L242
	movq	%rcx, -1304(%rbp)
.L242:
	cmpl	$47, %eax
	ja	.L243
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L244:
	movq	(%rdx), %rax
	movq	%rax, -1624(%rbp)
	jmp	.L236
.L152:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L153
.L1105:
	movq	%r14, %r13
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L243:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L244
.L1131:
	movb	$112, -1576(%rbp)
	xorl	%ebx, %ebx
	jmp	.L703
.L2099:
	movq	%r14, %rdi
	call	__uflow
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L715
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1512(%rbp)
	jmp	.L713
.L2098:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	-1512(%rbp), %esi
	movl	%esi, %fs:(%rax)
	jmp	.L713
.L2097:
	movq	-1568(%rbp), %rdi
	movsbl	%r13b, %esi
	call	char_buffer_add_slow
	jmp	.L711
.L911:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L910
.L2089:
	testb	%dil, %dil
	je	.L914
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L914:
	testb	%sil, %sil
	je	.L915
	movq	%rcx, -1304(%rbp)
.L915:
	cmpl	$47, %eax
	ja	.L916
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L917:
	movq	(%rdx), %rax
	movq	%rax, -1624(%rbp)
	jmp	.L909
.L869:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L868
.L2113:
	testb	%r9b, %r9b
	je	.L872
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %eax
.L872:
	testb	%dil, %dil
	je	.L873
	movq	%rsi, -1304(%rbp)
.L873:
	cmpl	$47, %eax
	ja	.L874
	movl	%eax, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L875:
	movq	(%rcx), %rax
	jmp	.L867
.L916:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L917
.L874:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L875
.L324:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L323
.L2068:
	testb	%dil, %dil
	je	.L327
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L327:
	testb	%sil, %sil
	je	.L328
	movq	%rcx, -1304(%rbp)
.L328:
	cmpl	$47, %eax
	ja	.L329
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L330:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L322
.L186:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L185
.L2090:
	testb	%dil, %dil
	je	.L189
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L189:
	testb	%sil, %sil
	je	.L190
	movq	%rcx, -1304(%rbp)
.L190:
	cmpl	$47, %eax
	ja	.L191
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L192:
	movq	(%rdx), %rax
	movq	%rax, -1656(%rbp)
	jmp	.L184
.L329:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L330
.L191:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L192
.L287:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L288
.L838:
	movq	-1664(%rbp), %rdi
	xorl	%r9d, %r9d
	movdqu	(%rdi), %xmm1
	movups	%xmm1, -1312(%rbp)
	movq	16(%rdi), %rcx
	movq	-1304(%rbp), %rsi
	movd	%xmm1, %eax
	xorl	%edi, %edi
	movq	%rcx, -1296(%rbp)
	movd	%xmm1, %ecx
.L842:
	subl	$1, -1668(%rbp)
	je	.L2138
	cmpl	$47, %ecx
	ja	.L843
	addl	$8, %ecx
	movl	$1, %r9d
	jmp	.L842
.L2082:
	movzbl	%al, %eax
	cmpl	$110, (%rbx,%rax,4)
	jne	.L1797
	movq	-1520(%rbp), %rax
	movq	112(%rax), %rbx
	jmp	.L1056
.L839:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L840
.L843:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L842
.L2138:
	testb	%r9b, %r9b
	je	.L846
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %eax
.L846:
	testb	%dil, %dil
	je	.L847
	movq	%rsi, -1304(%rbp)
.L847:
	cmpl	$47, %eax
	ja	.L848
	movl	%eax, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L849:
	movq	(%rcx), %rax
	jmp	.L841
.L2035:
	movl	%eax, %ebx
	jmp	.L454
.L276:
	addq	$8, %rcx
	movl	$1, %edi
	jmp	.L275
.L2084:
	testb	%sil, %sil
	je	.L279
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L279:
	testb	%dil, %dil
	je	.L280
	movq	%rcx, -1304(%rbp)
.L280:
	cmpl	$47, %eax
	ja	.L281
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L282:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L274
.L171:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L170
.L2091:
	testb	%dil, %dil
	je	.L174
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L174:
	testb	%sil, %sil
	je	.L175
	movq	%rcx, -1304(%rbp)
.L175:
	cmpl	$47, %eax
	ja	.L176
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L177:
	movq	(%rdx), %rax
	movq	%rax, -1584(%rbp)
	jmp	.L169
.L281:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L282
.L176:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L177
.L1768:
	movq	%r14, %r13
	jmp	.L41
.L167:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L168
.L2083:
	movzbl	%al, %eax
	cmpl	$105, (%rbx,%rax,4)
	jne	.L1797
	movq	-1520(%rbp), %rax
	movq	112(%rax), %rbx
	jmp	.L1058
.L848:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L849
.L118:
	addq	$8, %rcx
	movl	$1, %esi
	jmp	.L117
.L2079:
	testb	%dil, %dil
	je	.L121
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L121:
	testb	%sil, %sil
	je	.L122
	movq	%rcx, -1304(%rbp)
.L122:
	cmpl	$47, %eax
	ja	.L123
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L124:
	movq	(%rdx), %rax
	jmp	.L116
.L1837:
	movq	%r14, %r13
	jmp	.L41
.L892:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L893
.L123:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L124
.L1902:
	testb	%r9b, %r9b
	je	.L599
	movl	%ecx, -1312(%rbp)
	movl	%ecx, %edx
.L599:
	testb	%dil, %dil
	je	.L600
	movq	%rsi, -1304(%rbp)
.L600:
	cmpl	$47, %edx
	ja	.L601
	movl	%edx, %ecx
	addq	-1296(%rbp), %rcx
	addl	$8, %edx
	movl	%edx, -1312(%rbp)
.L602:
	movq	(%rcx), %rdx
	jmp	.L594
.L2045:
	addq	$1, %r12
	jmp	.L963
.L601:
	movq	-1304(%rbp), %rcx
	leaq	8(%rcx), %rdx
	movq	%rdx, -1304(%rbp)
	jmp	.L602
.L291:
	addq	$8, %rcx
	movl	$1, %edi
	jmp	.L290
.L2088:
	testb	%sil, %sil
	je	.L294
	movl	%edx, -1312(%rbp)
	movl	%edx, %eax
.L294:
	testb	%dil, %dil
	je	.L295
	movq	%rcx, -1304(%rbp)
.L295:
	cmpl	$47, %eax
	ja	.L296
	movl	%eax, %edx
	addq	-1296(%rbp), %rdx
	addl	$8, %eax
	movl	%eax, -1312(%rbp)
.L297:
	movq	(%rdx), %rax
	movq	%rax, -1656(%rbp)
	jmp	.L289
.L2026:
	movq	-1552(%rbp), %r15
	jmp	.L1039
.L630:
	movq	-1440(%rbp), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, -1440(%rbp)
	jmp	.L631
.L51:
	movq	-1520(%rbp), %rsi
	addq	$1, %r15
	movq	104(%rsi), %rcx
	jmp	.L48
.L2092:
	testl	$8192, -1528(%rbp)
	movq	%r14, %r13
	jne	.L1109
	movq	-1584(%rbp), %rax
	addl	$1, -1524(%rbp)
	movq	$0, -1584(%rbp)
	movq	(%rax), %rax
	movb	$0, -1(%rax,%rdx)
	jmp	.L41
.L2117:
	testl	$8192, -1528(%rbp)
	movq	%r14, %r13
	jne	.L1112
	movq	-1584(%rbp), %rax
	addl	$1, -1524(%rbp)
	movq	$0, -1584(%rbp)
	movq	(%rax), %rax
	movl	$0, -4(%rax,%rbx)
	jmp	.L41
.L1109:
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L1112:
	movl	$-1, -1524(%rbp)
	jmp	.L41
.L907:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L908
.L234:
	movq	-1440(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -1440(%rbp)
	jmp	.L235
.L609:
	addq	$8, %rsi
	movl	$1, %edi
	jmp	.L608
.L296:
	movq	-1304(%rbp), %rdx
	leaq	8(%rdx), %rax
	movq	%rax, -1304(%rbp)
	jmp	.L297
	.size	__vfscanf_internal, .-__vfscanf_internal
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.13126, @object
	.size	__PRETTY_FUNCTION__.13126, 19
__PRETTY_FUNCTION__.13126:
	.string	"__vfscanf_internal"
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.hidden	strcmp
	.hidden	__wcrtomb
	.hidden	__towctrans
	.hidden	__strtof128_internal
	.hidden	__strtof_internal
	.hidden	__strtod_internal
	.hidden	__assert_fail
	.hidden	__lll_lock_wait_private
	.hidden	__strtoul_internal
	.hidden	__strtold_internal
	.hidden	__libc_scratch_buffer_set_array_size
	.hidden	__strtol_internal
	.hidden	__mbrtowc
	.hidden	__uflow
	.hidden	_IO_sputbackc
	.hidden	__mbrlen
	.hidden	strlen
	.hidden	__libc_scratch_buffer_grow_preserve
