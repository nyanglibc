	.text
	.p2align 4,,15
	.globl	__renameat
	.hidden	__renameat
	.type	__renameat, @function
__renameat:
	movq	%rcx, %r10
	movl	$264, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/renameat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__renameat, .-__renameat
	.weak	renameat
	.set	renameat,__renameat
