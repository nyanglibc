	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.tmpnam_r
	.previous
#NO_APP
	.p2align 4,,15
	.globl	tmpnam_r
	.type	tmpnam_r, @function
tmpnam_r:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L4
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$20, %esi
	movq	%rdi, %rbx
	call	__path_search
	testl	%eax, %eax
	jne	.L4
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rbx, %rdi
	call	__gen_tempname
	testl	%eax, %eax
	jne	.L4
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	tmpnam_r, .-tmpnam_r
	.section	.gnu.warning.tmpnam_r
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_tmpnam_r, @object
	.size	__evoke_link_warning_tmpnam_r, 57
__evoke_link_warning_tmpnam_r:
	.string	"the use of `tmpnam_r' is dangerous, better use `mkstemp'"
	.hidden	__gen_tempname
	.hidden	__path_search
