	.text
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC0:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"inf"
	.section	.rodata.str4.4
	.align 4
.LC2:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC3:
	.string	"nan"
	.globl	__unordtf2
	.globl	__letf2
	.globl	__floatsitf
	.globl	__getf2
	.globl	__divtf3
	.text
	.p2align 4,,15
	.globl	__printf_size
	.type	__printf_size, @function
__printf_size:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%edi, %edi
	subq	$120, %rsp
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	8(%rsi), %rsi
	movl	(%r14), %ebp
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rsi,2), %eax
	andw	$256, %ax
	setne	%dil
	leaq	0(,%rdi,4), %rsi
	addq	%rdi, %rsi
	cmpw	$1, %ax
	leaq	64(%rsp), %rax
	sbbl	%ebx, %ebx
	leaq	units.13569(%rip), %rdi
	movq	%rax, 56(%rsp)
	movzbl	13(%r14), %eax
	andl	$24, %ebx
	addl	$1000, %ebx
	leaq	(%rdi,%rsi,2), %r12
	movq	(%rdx), %rsi
	movl	%eax, %r15d
	shrb	$2, %r15b
	andl	$1, %r15d
	testb	$16, %al
	movzbl	%r15b, %ecx
	movl	%ecx, 28(%rsp)
	je	.L3
	movdqa	(%rsi), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movdqa	(%rsp), %xmm2
	movdqa	.LC4(%rip), %xmm3
	pand	%xmm2, %xmm3
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L97
	movdqa	32(%rsp), %xmm3
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jg	.L148
.L97:
	movl	%ebx, %edi
	movaps	%xmm2, 32(%rsp)
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L7
	cmpb	$0, 1(%r12)
	movdqa	32(%rsp), %xmm2
	jne	.L9
	jmp	.L7
.L172:
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L7:
	movl	16(%r14), %eax
	testl	%ebp, %ebp
	movdqu	(%r14), %xmm0
	movaps	%xmm0, 80(%rsp)
	movzbl	93(%rsp), %edx
	movl	%eax, 96(%rsp)
	movl	$3, %eax
	cmovs	%eax, %ebp
	movl	%r15d, %eax
	andl	$1, %eax
	sall	$2, %eax
	movl	$102, 88(%rsp)
	movl	%ebp, 80(%rsp)
	andl	$-5, %edx
	orl	%edx, %eax
	testb	$32, 92(%rsp)
	movb	%al, 93(%rsp)
	je	.L64
	cmpl	$32, 96(%rsp)
	je	.L163
.L64:
	movl	4(%r14), %eax
	leaq	56(%rsp), %rdx
	leaq	80(%rsp), %rsi
	movq	%r13, %rdi
	subl	$1, %eax
	movl	%eax, 84(%rsp)
	call	__printf_fp
	testl	%eax, %eax
	movl	%eax, %r9d
	jle	.L1
	movl	28(%rsp), %edx
	movsbl	(%r12), %eax
	testl	%edx, %edx
	je	.L72
	movq	160(%r13), %rdx
	testq	%rdx, %rdx
	je	.L73
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L73
	leaq	4(%rcx), %rsi
	cmpl	$-1, %eax
	movq	%rsi, 32(%rdx)
	movl	%eax, (%rcx)
	sete	%al
.L75:
	testb	%al, %al
	jne	.L38
.L77:
	addl	$1, %r9d
.L1:
	addq	$120, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	12(%r14), %edx
	testb	$1, %dl
	je	.L13
	fldt	(%rsi)
	fld	%st(0)
	fstpt	64(%rsp)
	fucomi	%st(0), %st
	jp	.L171
	fxam
	fnstsw	%ax
	movl	%eax, %esi
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L15
	movl	%ebx, (%rsp)
	fildl	(%rsp)
	fxch	%st(1)
	fucomi	%st(1), %st
	jb	.L172
	cmpb	$0, 1(%r12)
	jne	.L17
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	cmpb	$0, 1(%r12)
	je	.L173
.L17:
	fdiv	%st(1), %st
	addq	$1, %r12
	fucomi	%st(1), %st
	jnb	.L18
	fstp	%st(1)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L173:
	fstp	%st(1)
.L156:
	fstpt	64(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	cmpb	$0, 1(%r12)
	je	.L149
.L9:
	movdqa	%xmm2, %xmm0
	movdqa	(%rsp), %xmm1
	addq	$1, %r12
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm2
	jns	.L11
.L149:
	movaps	%xmm2, 64(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	movsd	(%rsi), %xmm1
	ucomisd	%xmm1, %xmm1
	movsd	%xmm1, 64(%rsp)
	jp	.L14
	movapd	%xmm1, %xmm0
	andpd	.LC6(%rip), %xmm0
	ucomisd	.LC7(%rip), %xmm0
	ja	.L150
	pxor	%xmm0, %xmm0
	cvtsi2sd	%ebx, %xmm0
	ucomisd	%xmm0, %xmm1
	jb	.L7
	cmpb	$0, 1(%r12)
	jne	.L23
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L24:
	cmpb	$0, 1(%r12)
	je	.L157
.L23:
	divsd	%xmm0, %xmm1
	addq	$1, %r12
	ucomisd	%xmm0, %xmm1
	jnb	.L24
.L157:
	movsd	%xmm1, 64(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L148:
	movmskps	%xmm2, %eax
	movzbl	12(%r14), %edx
	andl	$8, %eax
	movl	%eax, %ebx
.L10:
	cmpl	%ebp, 4(%r14)
	movl	%edx, %eax
	leaq	.LC0(%rip), %r15
	cmovge	4(%r14), %ebp
	andl	$32, %eax
	testl	%ebx, %ebx
	leaq	.LC1(%rip), %r8
	je	.L80
	testb	%al, %al
	leal	-4(%rbp), %r12d
	je	.L96
	leaq	.LC1(%rip), %r8
	leaq	.LC0(%rip), %r15
	xorl	%ebp, %ebp
.L89:
	movl	28(%rsp), %r11d
	testl	%r11d, %r11d
	je	.L32
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L33
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L33
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rdx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L4:
	movzbl	12(%r14), %edx
	cmpl	%ebp, 4(%r14)
	leaq	.LC2(%rip), %r15
	leaq	.LC3(%rip), %r8
	cmovge	4(%r14), %ebp
	movl	%edx, %eax
	andl	$32, %eax
	.p2align 4,,10
	.p2align 3
.L80:
	testb	$80, %dl
	jne	.L25
	testb	%al, %al
	leal	-3(%rbp), %r12d
	jne	.L93
	xorl	%ebx, %ebx
	testl	%r12d, %r12d
	movl	%r12d, %ebp
	jle	.L93
.L83:
	movl	28(%rsp), %r12d
	movslq	%ebp, %r9
	movq	%r8, 32(%rsp)
	movq	%r9, %rdx
	movq	%r9, (%rsp)
	movl	$32, %esi
	movq	%r13, %rdi
	testl	%r12d, %r12d
	je	.L28
	call	_IO_wpadn@PLT
	movq	(%rsp), %r9
	movq	32(%rsp), %r8
.L29:
	cmpq	%r9, %rax
	movl	%ebp, %r12d
	jne	.L38
.L27:
	testl	%ebx, %ebx
	jne	.L89
	movzbl	12(%r14), %edx
.L82:
	testb	$64, %dl
	je	.L40
	movl	28(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L41
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L42
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L42
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rdx)
	.p2align 4,,10
	.p2align 3
.L51:
	addl	$1, %ebp
.L39:
	movl	28(%rsp), %edi
	testl	%edi, %edi
	je	.L53
	xorl	%ebx, %ebx
	addl	$1, %ebp
.L57:
	movq	160(%r13), %rax
	movl	(%r15,%rbx,4), %esi
	testq	%rax, %rax
	je	.L54
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L54
	leaq	4(%rdx), %rdi
	cmpl	$-1, %esi
	movq	%rdi, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L56:
	testb	%al, %al
	jne	.L38
	leal	0(%rbp,%rbx), %r9d
	addq	$1, %rbx
	cmpq	$3, %rbx
	jne	.L57
	testb	$32, 12(%r14)
	je	.L1
	testl	%r12d, %r12d
	jle	.L1
	movslq	%r12d, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movl	%r9d, (%rsp)
	call	_IO_wpadn@PLT
	movl	(%rsp), %r9d
.L63:
	cmpq	%rax, %rbx
	jne	.L38
	addl	%r12d, %r9d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	subl	$4, %ebp
	testb	%al, %al
	jne	.L164
	xorl	%ebx, %ebx
.L88:
	testl	%ebp, %ebp
	jg	.L83
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L32:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L165
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$45, (%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L15:
	fstp	%st(0)
	movl	%esi, %ebx
	andl	$512, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L150:
	movmskpd	%xmm1, %eax
	andl	$1, %eax
	movl	%eax, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	3(%r8), %r15
	movq	%r8, %rbx
.L61:
	addq	$1, %rbx
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	movzbl	-1(%rbx), %esi
	jnb	.L166
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	%sil, (%rax)
.L60:
	leal	0(%rbp,%rbx), %eax
	subl	%r8d, %eax
	cmpq	%r15, %rbx
	movl	%eax, %r9d
	jne	.L61
	testb	$32, 12(%r14)
	je	.L1
	testl	%r12d, %r12d
	jle	.L1
	movslq	%r12d, %rbx
	movl	$32, %esi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movl	%r9d, (%rsp)
	call	_IO_padn
	movl	(%rsp), %r9d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L40:
	andl	$16, %edx
	je	.L39
	movl	28(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L47
	movq	160(%r13), %rax
	testq	%rax, %rax
	je	.L48
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L48
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r13, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%ebp, %ebp
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L28:
	call	_IO_padn
	movq	32(%rsp), %r8
	movq	(%rsp), %r9
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r13, %rdi
	movq	%r8, (%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$-1, %r9d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L96:
	movl	%r12d, %ebp
	leaq	.LC0(%rip), %r15
	leaq	.LC1(%rip), %r8
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L72:
	movq	40(%r13), %rdx
	cmpq	48(%r13), %rdx
	jnb	.L167
	leaq	1(%rdx), %rcx
	movq	%rcx, 40(%r13)
	movb	%al, (%rdx)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L41:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L168
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$43, (%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L171:
	fstp	%st(0)
.L14:
	cmpl	%ebp, 4(%r14)
	movl	%edx, %eax
	leaq	.LC2(%rip), %r15
	cmovge	4(%r14), %ebp
	leaq	.LC3(%rip), %r8
	andl	$32, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L47:
	movq	40(%r13), %rax
	cmpq	48(%r13), %rax
	jnb	.L169
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r13)
	movb	$32, (%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	56(%rsp), %rdx
	leaq	80(%rsp), %rsi
	movq	%r13, %rdi
	movl	$0, 84(%rsp)
	call	__printf_fp
	testl	%eax, %eax
	movl	%eax, %r9d
	jle	.L1
	movl	28(%rsp), %esi
	movsbl	(%r12), %eax
	testl	%esi, %esi
	je	.L65
	movq	160(%r13), %rdx
	testq	%rdx, %rdx
	je	.L66
	movq	32(%rdx), %rsi
	cmpq	40(%rdx), %rsi
	jnb	.L66
	leaq	4(%rsi), %rdi
	cmpl	$-1, %eax
	movq	%rdi, 32(%rdx)
	movl	%eax, (%rsi)
	sete	%al
.L68:
	testb	%al, %al
	jne	.L38
	movl	4(%r14), %eax
	addl	$1, %r9d
	cmpl	%r9d, %eax
	jle	.L1
	movl	28(%rsp), %ecx
	movl	%eax, %edx
	subl	%r9d, %edx
	movslq	%edx, %rdx
	testl	%ecx, %ecx
	je	.L70
	movl	$32, %esi
	movq	%r13, %rdi
	movl	%r9d, (%rsp)
	call	_IO_wpadn@PLT
	movl	(%rsp), %r9d
	movl	%r9d, %edx
.L71:
	movl	4(%r14), %r9d
	movl	%r9d, %ecx
	subl	%edx, %ecx
	movslq	%ecx, %rdx
	cmpq	%rax, %rdx
	je	.L1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r8, (%rsp)
	movl	$45, %esi
.L161:
	movq	%r13, %rdi
	call	__woverflow
	movq	(%rsp), %r8
	cmpl	$-1, %eax
	sete	%al
.L50:
	testb	%al, %al
	je	.L51
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r8, (%rsp)
	movl	$45, %esi
.L160:
	movq	%r13, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movq	(%rsp), %r8
	sete	%al
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L73:
	movsbl	%al, %esi
	movq	%r13, %rdi
	movl	%r9d, (%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	(%rsp), %r9d
	sete	%al
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r8, (%rsp)
	movl	$43, %esi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L65:
	movq	40(%r13), %rdx
	cmpq	48(%r13), %rdx
	jnb	.L170
	leaq	1(%rdx), %rsi
	addl	$1, %r9d
	movq	%rsi, 40(%r13)
	movb	%al, (%rdx)
	movl	4(%r14), %eax
	cmpl	%eax, %r9d
	jge	.L1
.L70:
	subl	%r9d, %eax
	movl	$32, %esi
	movq	%r13, %rdi
	movslq	%eax, %rdx
	movl	%r9d, (%rsp)
	call	_IO_padn
	movl	(%rsp), %r9d
	movl	%r9d, %edx
	jmp	.L71
.L167:
	movzbl	%al, %esi
	movq	%r13, %rdi
	movl	%r9d, (%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movl	(%rsp), %r9d
	sete	%al
	jmp	.L75
.L168:
	movq	%r8, (%rsp)
	movl	$43, %esi
	jmp	.L160
.L48:
	movq	%r8, (%rsp)
	movl	$32, %esi
	jmp	.L161
.L169:
	movq	%r8, (%rsp)
	movl	$32, %esi
	jmp	.L160
.L66:
	movsbl	%al, %esi
	movq	%r13, %rdi
	movl	%r9d, (%rsp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	(%rsp), %r9d
	sete	%al
	jmp	.L68
.L170:
	movzbl	%al, %esi
	movq	%r13, %rdi
	movl	%r9d, (%rsp)
	call	__overflow
	cmpl	$-1, %eax
	movl	(%rsp), %r9d
	sete	%al
	jmp	.L68
.L164:
	movl	%ebp, %r12d
	xorl	%ebp, %ebp
	jmp	.L82
	.size	__printf_size, .-__printf_size
	.globl	printf_size
	.set	printf_size,__printf_size
	.p2align 4,,15
	.globl	printf_size_info
	.type	printf_size_info, @function
printf_size_info:
	testq	%rsi, %rsi
	je	.L175
	movzbl	12(%rdi), %eax
	sall	$8, %eax
	andl	$256, %eax
	orl	$7, %eax
	movl	%eax, (%rdx)
.L175:
	movl	$1, %eax
	ret
	.size	printf_size_info, .-printf_size_info
	.section	.rodata
	.align 16
	.type	units.13569, @object
	.size	units.13569, 20
units.13569:
	.string	" kmgtpezy"
	.string	" KMGTPEZY"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	4294967295
	.long	2146435071
	.hidden	__overflow
	.hidden	__woverflow
	.hidden	_IO_padn
	.hidden	__printf_fp
