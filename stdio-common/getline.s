	.text
	.p2align 4,,15
	.globl	__getline
	.hidden	__getline
	.type	__getline, @function
__getline:
	movq	%rdx, %rcx
	movl	$10, %edx
	jmp	_IO_getdelim@PLT
	.size	__getline, .-__getline
	.weak	getline
	.set	getline,__getline
