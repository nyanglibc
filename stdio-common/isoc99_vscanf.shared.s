	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__isoc99_vscanf
	.type	__isoc99_vscanf, @function
__isoc99_vscanf:
.LFB68:
	.cfi_startproc
	movq	stdin@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$2, %ecx
	movq	(%rax), %rdi
	jmp	__vfscanf_internal
	.cfi_endproc
.LFE68:
	.size	__isoc99_vscanf, .-__isoc99_vscanf
	.hidden	__vfscanf_internal
