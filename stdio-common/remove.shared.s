	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_remove
	.hidden	__GI_remove
	.type	__GI_remove, @function
__GI_remove:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__unlink
	testl	%eax, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$-1, %eax
	cmpl	$21, %fs:(%rdx)
	jne	.L1
	movq	%rbx, %rdi
	call	__rmdir
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	negl	%eax
.L1:
	popq	%rbx
	ret
	.size	__GI_remove, .-__GI_remove
	.globl	remove
	.set	remove,__GI_remove
	.hidden	__rmdir
	.hidden	__unlink
