	.text
	.p2align 4,,15
	.globl	__flockfile
	.hidden	__flockfile
	.type	__flockfile, @function
__flockfile:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	136(%rdi), %rdx
	orl	$128, 116(%rdi)
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdx)
	je	.L2
	movq	%rdi, %rbx
#APP
# 28 "../sysdeps/pthread/flockfile.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, (%rdx)
# 0 "" 2
#NO_APP
.L4:
	movq	136(%rbx), %rdx
	movq	%rbp, 8(%rdx)
.L2:
	addl	$1, 4(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, (%rdx)
	je	.L4
	movq	%rdx, %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.size	__flockfile, .-__flockfile
	.weak	flockfile
	.set	flockfile,__flockfile
	.globl	_IO_flockfile
	.set	_IO_flockfile,__flockfile
	.hidden	__lll_lock_wait_private
