	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__register_printf_type
	.type	__register_printf_type, @function
__register_printf_type:
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 38 "reg-type.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	cmpq	$0, __printf_va_arg_table(%rip)
	je	.L4
.L8:
	movl	pa_next_type(%rip), %r8d
	cmpl	$256, %r8d
	je	.L15
	leal	1(%r8), %eax
	movq	__printf_va_arg_table(%rip), %rdx
	movl	%eax, pa_next_type(%rip)
	movslq	%r8d, %rax
	movq	%rbx, -64(%rdx,%rax,8)
.L7:
#APP
# 57 "reg-type.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	subl	$1, lock(%rip)
.L1:
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$8, %esi
	movl	$248, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, __printf_va_arg_table(%rip)
	jne	.L8
	movl	$-1, %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
#APP
# 57 "reg-type.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 57 "reg-type.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$28, %fs:(%rax)
	jmp	.L7
	.size	__register_printf_type, .-__register_printf_type
	.weak	register_printf_type
	.set	register_printf_type,__register_printf_type
	.data
	.align 4
	.type	pa_next_type, @object
	.size	pa_next_type, 4
pa_next_type:
	.long	8
	.local	lock
	.comm	lock,4,4
	.hidden	__printf_va_arg_table
	.globl	__printf_va_arg_table
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	__printf_va_arg_table, @object
	.size	__printf_va_arg_table, 8
__printf_va_arg_table:
	.zero	8
	.hidden	__lll_lock_wait_private
