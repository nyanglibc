	.text
	.p2align 4,,15
	.type	hack_digit, @function
hack_digit:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L5
	cmpl	$102, 4(%rdi)
	je	.L25
.L5:
	movq	40(%rdi), %r9
	movq	%rdi, %rbx
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rdi
	testq	%r9, %r9
	je	.L26
	cmpq	%rdx, %r9
	jg	.L13
	movq	%rdx, %rcx
	movq	32(%rbx), %r8
	movq	%rdi, %rdx
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	call	__mpn_divrem
	movq	24(%rbx), %rdx
	subq	40(%rbx), %rdx
	movq	48(%rbx), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movq	40(%rbx), %rdx
	movq	(%rcx), %rbp
	testq	%rdx, %rdx
	movq	%rdx, 24(%rbx)
	je	.L8
	movq	16(%rbx), %rdi
	addl	$48, %ebp
	cmpq	$0, -8(%rdi,%rdx,8)
	jne	.L7
	leaq	-1(%rdx), %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	subq	$1, %rax
	cmpq	$0, (%rdi,%rax,8)
	jne	.L7
.L10:
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, 24(%rbx)
	jne	.L27
.L9:
	movq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$48, %ebp
.L7:
	movl	$10, %ecx
	movq	%rdi, %rsi
	call	__mpn_mul_1
	testq	%rax, %rax
	je	.L1
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rcx
	leaq	1(%rdx), %rsi
	movq	%rsi, 24(%rbx)
	movq	%rax, (%rcx,%rdx,8)
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	-8(%rdi,%rdx,8), %rbx
	movl	$10, %ecx
	subq	$1, %rdx
	movq	%rdi, %rsi
	movq	(%rbx), %rbp
	call	__mpn_mul_1
	movq	%rax, (%rbx)
	addq	$8, %rsp
	addl	$48, %ebp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	8(%rdi), %eax
	movl	$48, %ebp
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, 8(%rdi)
	jle	.L5
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addl	$48, %ebp
	jmp	.L9
	.size	hack_digit, .-hack_digit
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"to_outpunct"
	.text
	.p2align 4,,15
	.type	_i18n_number_rewrite, @function
_i18n_number_rewrite:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %rbx
	subq	$1128, %rsp
	call	__wctrans@PLT
	movl	$46, %edi
	movq	%rax, %rbp
	movq	%rax, %rsi
	call	__towctrans
	movq	%rbp, %rsi
	movl	$44, %edi
	movl	%eax, %r15d
	call	__towctrans
	testq	%rbp, %rbp
	leaq	80(%rsp), %r12
	jne	.L65
.L29:
	leaq	16(%r12), %rax
	subq	%r14, %r13
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$1024, 88(%rsp)
	movq	%rax, 80(%rsp)
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L46
	movq	80(%rsp), %r15
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__mempcpy@PLT
	movq	%rax, %r13
	leaq	48(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 8(%rsp)
.L35:
	subq	$1, %r13
	cmpq	%r13, %r15
	ja	.L66
.L45:
	movsbl	0(%r13), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L67
	testq	%rbp, %rbp
	jne	.L68
.L39:
	subq	$1, %r13
	movb	%al, -1(%rbx)
	subq	$1, %rbx
	cmpq	%r13, %r15
	jbe	.L45
.L66:
	movq	80(%rsp), %rdi
	addq	$16, %r12
	cmpq	%r12, %rdi
	je	.L28
	call	free@PLT
.L28:
	addq	$1128, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rcx
	subl	$7, %eax
	cltq
	movq	%fs:(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	64(%rcx,%rax,8), %r14
	movq	%r14, %rdi
	call	strlen
	subq	%rax, %rbx
	testq	%rax, %rax
	leaq	-1(%rax), %rcx
	je	.L35
	.p2align 4,,10
	.p2align 3
.L38:
	movzbl	(%r14,%rcx), %eax
	movb	%al, (%rbx,%rcx)
	subq	$1, %rcx
	cmpq	$-1, %rcx
	jne	.L38
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%eax, %ecx
	andl	$-3, %ecx
	cmpb	$44, %cl
	jne	.L39
	cmpb	$46, %al
	movq	(%rsp), %rsi
	cmove	8(%rsp), %rsi
	movq	%rsi, %rcx
.L42:
	movl	(%rcx), %edi
	addq	$4, %rcx
	leal	-16843009(%rdi), %eax
	notl	%edi
	andl	%edi, %eax
	andl	$-2139062144, %eax
	je	.L42
	movl	%eax, %edi
	shrl	$16, %edi
	testl	$32896, %eax
	cmove	%edi, %eax
	leaq	2(%rcx), %rdi
	movl	%eax, %edx
	cmove	%rdi, %rcx
	addb	%al, %dl
	sbbq	$3, %rcx
	subq	%rsi, %rcx
	subq	%rcx, %rbx
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rax
	je	.L35
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	(%rsi,%rax), %ecx
	movb	%cl, (%rbx,%rax)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L44
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	16(%rsp), %rdi
	movq	%r12, %rdx
	movl	%r15d, %esi
	movl	%eax, (%rsp)
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	movl	(%rsp), %ecx
	je	.L69
	movb	$0, 16(%rsp,%rax)
.L31:
	leaq	48(%rsp), %rdi
	movq	%r12, %rdx
	movl	%ecx, %esi
	movq	$0, 80(%rsp)
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L70
	movb	$0, 48(%rsp,%rax)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r14, %rbx
	jmp	.L28
.L69:
	movl	$46, %edx
	movw	%dx, 16(%rsp)
	jmp	.L31
.L70:
	movl	$44, %eax
	movw	%ax, 48(%rsp)
	jmp	.L29
	.size	_i18n_number_rewrite, .-_i18n_number_rewrite
	.p2align 4,,15
	.type	__guess_grouping.part.0, @function
__guess_grouping.part.0:
	movzbl	(%rsi), %ecx
	xorl	%r9d, %r9d
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$1, %rsi
	movzbl	(%rsi), %ecx
	addl	$1, %r9d
	subl	%r8d, %edi
	cmpb	$127, %cl
	je	.L71
	testb	%cl, %cl
	js	.L71
	testb	%cl, %cl
	je	.L77
.L74:
	movsbl	%cl, %r8d
	cmpl	%r8d, %edi
	ja	.L75
.L71:
	movl	%r9d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	leal	-1(%rdi), %eax
	xorl	%edx, %edx
	divl	%r8d
	addl	%eax, %r9d
	movl	%r9d, %eax
	ret
	.size	__guess_grouping.part.0, .-__guess_grouping.part.0
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC1:
	.string	"N"
	.string	""
	.string	""
	.string	"A"
	.string	""
	.string	""
	.string	"N"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC2:
	.string	"NAN"
	.section	.rodata.str4.4
	.align 4
.LC3:
	.string	"I"
	.string	""
	.string	""
	.string	"N"
	.string	""
	.string	""
	.string	"F"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC4:
	.string	"INF"
	.section	.rodata.str4.4
	.align 4
.LC5:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC6:
	.string	"nan"
	.section	.rodata.str4.4
	.align 4
.LC7:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC8:
	.string	"inf"
	.globl	__unordtf2
	.globl	__letf2
	.section	.rodata.str4.4
	.align 4
.LC13:
	.string	"0"
	.string	""
	.string	""
	.string	"."
	.string	""
	.string	""
	.string	"0"
	.string	""
	.string	""
	.string	"0"
	.string	""
	.string	""
	.string	"0"
	.string	""
	.string	""
	.string	"1"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.text
	.p2align 4,,15
	.globl	__printf_fp_l
	.hidden	__printf_fp_l
	.type	__printf_fp_l, @function
__printf_fp_l:
	pushq	%rbp
	movq	%rsi, %r11
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$280, %rsp
	movzbl	13(%rdx), %eax
	movq	%rdx, -184(%rbp)
	movzbl	12(%rdi), %r12d
	movq	%rsi, -232(%rbp)
	movl	$0, -148(%rbp)
	movl	$0, -112(%rbp)
	movl	%eax, %edx
	movl	%eax, %edi
	shrb	$2, %dl
	andl	$1, %edx
	andl	$1, %edi
	movl	%edx, -212(%rbp)
	movl	%edi, %esi
	jne	.L79
	movq	8(%r11), %rdx
	testb	%r12b, %r12b
	movq	64(%rdx), %rdi
	movq	%rdi, -248(%rbp)
	movl	88(%rdx), %edi
	movl	%edi, -216(%rbp)
	js	.L80
.L392:
	movq	$0, -224(%rbp)
	movl	$0, -272(%rbp)
	movq	$0, -280(%rbp)
.L81:
	testb	$16, %al
	movq	(%rcx), %rdx
	je	.L89
	movdqa	(%rdx), %xmm3
	movdqa	%xmm3, %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm3, -176(%rbp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L666
	movdqa	-176(%rbp), %xmm2
	pand	.LC9(%rip), %xmm2
	movdqa	.LC10(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, -208(%rbp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L94
	movdqa	-208(%rbp), %xmm2
	movdqa	.LC10(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L94
	movaps	-176(%rbp), %xmm4
	movmskps	%xmm4, %r13d
	andl	$8, %r13d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L732:
	fstp	%st(0)
.L640:
	movq	-184(%rbp), %rax
	leaq	.LC7(%rip), %r14
	leaq	.LC8(%rip), %r15
	movl	%r13d, -148(%rbp)
	movslq	8(%rax), %rdx
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	leaq	.LC3(%rip), %rdx
	andw	$256, %ax
	leaq	.LC4(%rip), %rax
	cmovne	%rdx, %r14
	cmovne	%rax, %r15
.L93:
	movq	-184(%rbp), %rax
	movl	4(%rax), %ecx
	movl	%r12d, %eax
	shrb	$5, %al
	xorl	$1, %eax
	andl	$1, %eax
	testl	%r13d, %r13d
	je	.L667
	subl	$4, %ecx
	testl	%ecx, %ecx
	setg	%dl
	jle	.L437
	testb	%al, %al
	je	.L437
.L362:
	movl	-212(%rbp), %eax
	movslq	%ecx, %r12
	movl	%ecx, -176(%rbp)
	movq	%r12, %rdx
	movl	$32, %esi
	movq	%rbx, %rdi
	testl	%eax, %eax
	jne	.L668
	call	_IO_padn
	movl	-176(%rbp), %ecx
.L105:
	cmpq	%r12, %rax
	jne	.L647
	movl	-148(%rbp), %eax
	movl	%ecx, %r13d
	movl	$1, %edx
	testl	%eax, %eax
	je	.L402
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L109
.L672:
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L110
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L110
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$45, (%rsi)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-232(%rbp), %r10
	movq	32(%r10), %rdx
	movq	80(%rdx), %rdi
	cmpb	$0, (%rdi)
	movq	%rdi, -248(%rbp)
	jne	.L82
	movq	8(%r10), %rdi
	movq	64(%rdi), %rdi
	movq	%rdi, -248(%rbp)
.L82:
	movl	408(%rdx), %edi
	testl	%edi, %edi
	movl	%edi, -216(%rbp)
	jne	.L83
	movq	-232(%rbp), %rdi
	movq	8(%rdi), %rdi
	movl	88(%rdi), %edi
	movl	%edi, -216(%rbp)
.L83:
	testb	%r12b, %r12b
	jns	.L392
	movq	96(%rdx), %rdi
	movq	%rdi, -224(%rbp)
.L85:
	movq	-224(%rbp), %rdi
	movzbl	(%rdi), %edx
	movb	%dl, -176(%rbp)
	subl	$1, %edx
	cmpb	$125, %dl
	ja	.L392
	movl	-212(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L86
	testb	%sil, %sil
	movq	-232(%rbp), %rsi
	jne	.L87
	movq	8(%rsi), %rdx
	movq	$0, -280(%rbp)
	movl	96(%rdx), %esi
	movl	$0, %edx
	testl	%esi, %esi
	movl	%esi, -272(%rbp)
	cmovne	%rdi, %rdx
	movq	%rdx, -224(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L89:
	testb	$1, %r12b
	je	.L96
	fldt	(%rdx)
	fxam
	fnstsw	%ax
	movl	%eax, %r13d
	andl	$512, %r13d
	fucomi	%st(0), %st
	jp	.L731
	movl	%eax, %edx
	movl	%eax, %r13d
	andb	$69, %dh
	andl	$512, %r13d
	cmpb	$5, %dh
	je	.L732
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r13
	leaq	-148(%rbp), %rcx
	subq	$16, %rsp
	movl	$2, %esi
	leaq	8(%rax), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	fstpt	(%rsp)
	call	__mpn_extract_long_double
	movl	%eax, %r12d
	popq	%rdx
	popq	%rcx
	sall	$6, %r12d
	movq	%rax, -88(%rbp)
	subl	$63, %r12d
.L95:
	movl	-104(%rbp), %ecx
	movq	$0, -72(%rbp)
	movl	%ecx, %edx
	sarl	$31, %edx
	movl	%edx, %esi
	xorl	%ecx, %esi
	subl	%edx, %esi
	addl	$63, %esi
	sarl	$6, %esi
	addl	$4, %esi
	movslq	%esi, %rsi
	leaq	30(,%rsi,8), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdi
	subq	%rdx, %rsp
	leaq	15(%rsp), %rsi
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdi
	andq	$-16, %rsi
	movq	%rdi, -96(%rbp)
	andq	$-16, %rdx
	cmpl	$2, %ecx
	movq	%rsi, -64(%rbp)
	movq	%rdx, -80(%rbp)
	jle	.L141
	leal	(%r12,%rcx), %esi
	leal	63(%rsi), %edx
	testl	%esi, %esi
	cmovns	%esi, %edx
	sarl	$6, %edx
	testb	$63, %sil
	jne	.L142
	movq	%rax, %rsi
	movslq	%edx, %rdx
	subq	$1, %rsi
	js	.L143
	leaq	(%rdi,%rdx,8), %r8
.L144:
	movq	0(%r13,%rsi,8), %rdi
	movq	%rdi, (%r8,%rsi,8)
	subq	$1, %rsi
	cmpq	$-1, %rsi
	jne	.L144
.L143:
	addq	%rdx, %rax
	movq	%rax, -88(%rbp)
.L145:
	testq	%rdx, %rdx
	jle	.L149
	movq	-96(%rbp), %rax
	leaq	(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L146:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L146
.L149:
	leaq	312+_fpioconst_pow10(%rip), %r15
	movq	-72(%rbp), %rdx
	movl	$0, -176(%rbp)
	movl	$12, %r13d
	xorl	%r14d, %r14d
	leaq	-312(%r15), %r12
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L670:
	movq	-184(%rbp), %rax
	testb	$16, 13(%rax)
	jne	.L669
	movq	(%r15), %rax
	movq	8(%r15), %rdx
	leaq	__tens(%rip), %rcx
	leaq	(%rcx,%rax,8), %rsi
	movq	%rdx, -56(%rbp)
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-56(%rbp), %rdx
.L153:
	cmpq	%rdx, -88(%rbp)
	jg	.L155
	je	.L156
.L632:
	movq	-72(%rbp), %rdx
.L150:
	subl	$1, %r13d
	cmpq	%r12, %r15
	jbe	.L158
.L677:
	movl	-104(%rbp), %ecx
.L147:
	subq	$24, %r15
	movl	16(%r15), %eax
	addl	%r14d, %eax
	subl	$1, %eax
	cmpl	%ecx, %eax
	jg	.L150
	testq	%rdx, %rdx
	movq	-64(%rbp), %rdi
	je	.L670
	movq	(%r15), %rcx
	movq	8(%r15), %rax
	leaq	__tens(%rip), %rsi
	leaq	8(%rsi,%rcx,8), %rcx
	movq	-80(%rbp), %rsi
	leaq	-1(%rax), %r8
	call	__mpn_mul
	movq	8(%r15), %rdx
	addq	-72(%rbp), %rdx
	testq	%rax, %rax
	je	.L154
	subq	$1, %rdx
	movq	%rdx, -56(%rbp)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L96:
	movsd	(%rdx), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L671
	movapd	%xmm0, %xmm1
	movmskpd	%xmm0, %r13d
	andpd	.LC11(%rip), %xmm1
	andl	$1, %r13d
	ucomisd	.LC12(%rip), %xmm1
	ja	.L640
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r13
	leaq	-148(%rbp), %rcx
	movl	$2, %esi
	leaq	8(%rax), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	__mpn_extract_double
	movl	%eax, %r12d
	movq	%rax, -88(%rbp)
	sall	$6, %r12d
	subl	$52, %r12d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L667:
	testb	$80, %r12b
	jne	.L102
	subl	$3, %ecx
	testl	%ecx, %ecx
	setg	%dl
	jle	.L364
.L631:
	testb	%al, %al
	jne	.L362
.L364:
	testb	$64, %r12b
	je	.L117
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L118
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L119
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L119
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$43, (%rsi)
	.p2align 4,,10
	.p2align 3
.L128:
	addl	$1, %r13d
.L116:
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L130
	xorl	%r15d, %r15d
	addl	$1, %r13d
.L134:
	movq	160(%rbx), %rax
	movl	(%r14,%r15,4), %esi
	testq	%rax, %rax
	je	.L131
	movq	32(%rax), %rdi
	cmpq	40(%rax), %rdi
	jnb	.L131
	leaq	4(%rdi), %r8
	cmpl	$-1, %esi
	movq	%r8, 32(%rax)
	movl	%esi, (%rdi)
	sete	%al
.L133:
	testb	%al, %al
	jne	.L647
	leal	0(%r13,%r15), %r12d
	addq	$1, %r15
	cmpq	$3, %r15
	jne	.L134
	movq	-184(%rbp), %rax
	testb	$32, 12(%rax)
	je	.L78
	testb	%dl, %dl
	je	.L78
	movslq	%ecx, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movl	%ecx, -176(%rbp)
	call	_IO_wpadn@PLT
	movl	-176(%rbp), %ecx
.L140:
	cmpq	%r13, %rax
	jne	.L647
	addl	%ecx, %r12d
.L78:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movq	80(%rdx), %rdi
	movq	%rdi, -224(%rbp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L102:
	subl	$4, %ecx
	testl	%ecx, %ecx
	setg	%dl
	jle	.L364
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L668:
	call	_IO_wpadn@PLT
	movl	-176(%rbp), %ecx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L437:
	movl	-212(%rbp), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	jne	.L672
.L109:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L673
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	$45, (%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-184(%rbp), %rax
	leaq	.LC1(%rip), %r14
	leaq	.LC2(%rip), %r15
	movaps	-176(%rbp), %xmm5
	movslq	8(%rax), %rdx
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movmskps	%xmm5, %r13d
	movq	%fs:(%rax), %rax
	andl	$8, %r13d
	movl	%r13d, -148(%rbp)
	movzwl	(%rax,%rdx,2), %eax
	leaq	.LC5(%rip), %rdx
	andw	$256, %ax
	leaq	.LC6(%rip), %rax
	cmove	%rdx, %r14
	cmove	%rax, %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	3(%r15), %r8
	movq	%r15, %r14
.L138:
	addq	$1, %r14
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	movzbl	-1(%r14), %esi
	jnb	.L674
	leaq	1(%rax), %rdi
	movq	%rdi, 40(%rbx)
	movb	%sil, (%rax)
.L137:
	movl	%r14d, %r12d
	subl	%r15d, %r12d
	addl	%r13d, %r12d
	cmpq	%r8, %r14
	jne	.L138
	movq	-184(%rbp), %rax
	testb	$32, 12(%rax)
	je	.L78
	testb	%dl, %dl
	je	.L78
	movslq	%ecx, %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movl	%ecx, -176(%rbp)
	call	_IO_padn
	movl	-176(%rbp), %ecx
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-240(%rbp), %r13
	movq	-256(%rbp), %rbx
.L327:
	movq	-184(%rbp), %rcx
	movq	%r14, %r15
	testb	$8, 13(%rcx)
	jne	.L675
.L332:
	subq	%r15, %rax
	cmpq	$20, %rax
	movq	%rax, %rcx
	movq	%rax, %r8
	jle	.L676
	movq	216(%rbx), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rsi
	movq	%rax, %rcx
	subq	%rdx, %rcx
	subq	%rdx, %rsi
	cmpq	%rsi, %rcx
	jnb	.L379
	movq	56(%rax), %rax
	movq	%r15, %r13
.L339:
	movq	%r8, %rdx
	movq	%r8, -192(%rbp)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-192(%rbp), %r8
	cmpq	%r8, %rax
	je	.L340
.L652:
	movl	-304(%rbp), %eax
	testl	%eax, %eax
	jne	.L647
	movq	%r14, %rdi
	call	free@PLT
.L646:
	movq	-296(%rbp), %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L647:
	movl	$-1, %r12d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-64(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	js	.L632
	movq	-56(%rbp), %rdx
.L155:
	movq	-64(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	salq	$3, %rdx
	call	memcpy@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rax
	bsrq	-8(%rax,%rdx,8), %rax
	movl	%edx, %ecx
	sall	$6, %ecx
	leal	-129(%rcx), %r14d
	movl	%r13d, %ecx
	subl	$1, %r13d
	xorq	$63, %rax
	subl	%eax, %r14d
	movl	$1, %eax
	sall	%cl, %eax
	orl	%eax, -176(%rbp)
	cmpq	%r12, %r15
	ja	.L677
.L158:
	movl	-176(%rbp), %eax
	testq	%rdx, %rdx
	movl	%eax, -104(%rbp)
	jle	.L160
	movq	-80(%rbp), %rdi
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L161
	movq	-96(%rbp), %r11
	cmpq	$0, (%r11)
	jne	.L162
	leaq	8(%rdi), %r9
	movl	$1, %eax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L679:
	movq	(%r11,%rax,8), %r8
	leaq	1(%rax), %r10
	leaq	8(%r9), %r9
	testq	%r8, %r8
	jne	.L678
	movq	%r10, %rax
.L164:
	movq	(%r9), %rcx
	movslq	%eax, %r12
	leaq	0(,%rax,8), %r13
	movq	%r9, %rsi
	testq	%rcx, %rcx
	je	.L679
	bsrq	-8(%rdi,%rdx,8), %r8
	xorq	$63, %r8
	testl	%r8d, %r8d
	movl	%r8d, %r9d
	je	.L369
.L370:
	movq	(%r11,%r13), %rax
	rep bsfq	%rcx, %rcx
	movl	%ecx, %r8d
	testq	%rax, %rax
	je	.L170
	xorl	%r8d, %r8d
	rep bsfq	%rax, %r8
	cmpl	%r8d, %ecx
	cmovle	%ecx, %r8d
.L170:
	movl	$64, %r14d
	subl	%r9d, %r14d
	testl	%r12d, %r12d
	jne	.L171
.L388:
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	cmpl	%r8d, %r14d
	jg	.L680
.L372:
	subq	%r15, %rdx
	movl	%r14d, %ecx
	addl	$1, %r12d
	call	__mpn_rshift
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rdx
	movl	%r14d, %ecx
	movslq	%r12d, %r12
	subq	%r12, -72(%rbp)
	leaq	(%rdi,%r13), %rsi
	subq	%r15, %rdx
	call	__mpn_rshift
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpq	$0, -8(%rcx,%rax,8)
	jne	.L176
	movq	%rdx, %rax
	subq	%r12, %rax
.L176:
	movq	%rax, -88(%rbp)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L117:
	andl	$16, %r12d
	je	.L116
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L124
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L125
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	jnb	.L125
	leaq	4(%rsi), %rdi
	movq	%rdi, 32(%rax)
	movl	$32, (%rsi)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rbx, %rdi
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	call	__woverflow
	cmpl	$-1, %eax
	movl	-176(%rbp), %ecx
	movzbl	-208(%rbp), %edx
	sete	%al
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L141:
	testl	%ecx, %ecx
	js	.L681
	addl	%r12d, %ecx
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	__mpn_lshift
	movq	-88(%rbp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, -88(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movl	$0, -104(%rbp)
.L160:
	movq	-184(%rbp), %rsi
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movslq	8(%rsi), %rcx
	movl	4(%rsi), %eax
	movq	%fs:(%rdx), %rdx
	movl	(%rsi), %esi
	movl	%eax, -288(%rbp)
	movq	%rcx, %rax
	movl	(%rdx,%rcx,4), %ecx
	movl	%esi, -268(%rbp)
	cmpb	$101, %cl
	movl	%ecx, -312(%rbp)
	je	.L682
	cmpb	$102, -312(%rbp)
	je	.L683
	movl	-268(%rbp), %ecx
	testl	%ecx, %ecx
	js	.L408
	movl	$1, %edx
	cmovne	%ecx, %edx
	movl	%edx, -268(%rbp)
.L209:
	movl	-112(%rbp), %r13d
	movl	-104(%rbp), %edx
	testl	%r13d, %r13d
	je	.L684
	cmpl	$4, %edx
	jle	.L213
.L211:
	subl	$2, %eax
	movl	$1, -284(%rbp)
	movl	%eax, -108(%rbp)
	movl	-268(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -192(%rbp)
	cltq
	addq	$8, %rax
	movq	%rax, -264(%rbp)
.L214:
	movq	-184(%rbp), %rax
	movl	$0, -176(%rbp)
	testb	$8, 12(%rax)
	movl	$0, %eax
	cmovne	-192(%rbp), %eax
	movl	%eax, -256(%rbp)
.L205:
	movq	-224(%rbp), %rax
	movl	$0, -300(%rbp)
	testq	%rax, %rax
	je	.L215
	movzbl	(%rax), %eax
	movl	$1, %edx
	movb	%al, -240(%rbp)
	subl	$1, %eax
	cmpb	$125, %al
	jbe	.L685
.L216:
	addq	%rdx, -264(%rbp)
.L215:
	movq	-264(%rbp), %rcx
	movabsq	$4611686018427387900, %rax
	cmpq	%rax, %rcx
	ja	.L217
	movslq	-192(%rbp), %rax
	cmpq	%rcx, %rax
	ja	.L217
	movq	-264(%rbp), %rax
	leaq	8(,%rax,4), %r12
	movq	%r12, %rdi
	call	__libc_alloca_cutoff
	testl	%eax, %eax
	jne	.L219
	cmpq	$4096, %r12
	ja	.L686
.L219:
	addq	$30, %r12
	movl	$1, -304(%rbp)
	andq	$-16, %r12
	subq	%r12, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -296(%rbp)
.L220:
	movl	-112(%rbp), %r12d
	movq	-296(%rbp), %rax
	testl	%r12d, %r12d
	leaq	8(%rax), %r13
	je	.L221
	cmpl	$102, -108(%rbp)
	je	.L687
.L221:
	movl	-284(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L688
	movl	-284(%rbp), %eax
	movq	-208(%rbp), %r15
	movq	%rbx, -176(%rbp)
	movq	%r13, %rbx
	leal	-1(%rax), %r14d
	movq	-296(%rbp), %rax
	leaq	12(%rax,%r14,4), %r12
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r15, %rdi
	addq	$4, %rbx
	call	hack_digit
	movl	%eax, -4(%rbx)
	cmpq	%rbx, %r12
	jne	.L223
	movl	-284(%rbp), %ecx
	movq	-176(%rbp), %rbx
	leaq	4(%r13,%r14,4), %rax
	movl	%ecx, -308(%rbp)
.L224:
	movq	-184(%rbp), %rcx
	testb	$8, 12(%rcx)
	jne	.L226
	movl	-256(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L689
.L226:
	movl	-216(%rbp), %ecx
	leaq	4(%rax), %r12
	movl	$1, -176(%rbp)
	movl	%ecx, (%rax)
.L227:
	movl	-256(%rbp), %r14d
	movl	$0, -240(%rbp)
	xorl	%r15d, %r15d
.L228:
	cmpl	%r15d, %r14d
	movl	%r14d, %esi
	jg	.L230
	cmpl	%r15d, -192(%rbp)
	jle	.L231
.L692:
	cmpq	$1, -88(%rbp)
	jle	.L690
.L230:
	movq	-208(%rbp), %rdi
	addl	$1, %r15d
	addq	$4, %r12
	call	hack_digit
	movl	%eax, -4(%r12)
	cmpl	$48, %eax
	jne	.L414
	movl	-176(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L691
.L414:
	cmpl	%r15d, %r14d
	movl	$1, -176(%rbp)
	movl	%r14d, %esi
	jg	.L230
	cmpl	%r15d, -192(%rbp)
	jg	.L692
.L231:
	movl	-4(%r12), %r14d
	cmpl	-216(%rbp), %r14d
	jne	.L232
	movl	-8(%r12), %r14d
.L232:
	movq	-208(%rbp), %rdi
	movl	%esi, -176(%rbp)
	call	hack_digit
	cmpl	$48, %eax
	movl	-176(%rbp), %esi
	setne	%r8b
	cmpl	$53, %eax
	setne	%dil
	andb	%dil, %r8b
	jne	.L233
	movq	-88(%rbp), %r9
	cmpq	$1, %r9
	je	.L693
	cmpq	$0, -72(%rbp)
	je	.L694
.L416:
	movl	$1, %r8d
.L233:
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw -128(%rbp)
# 0 "" 2
#NO_APP
	movzwl	-128(%rbp), %edi
	andw	$3072, %di
	cmpw	$1024, %di
	je	.L239
	jbe	.L695
	cmpw	$2048, %di
	je	.L242
	cmpw	$3072, %di
	jne	.L238
.L244:
	cmpl	%esi, %r15d
	jg	.L272
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L274:
	subl	$1, %r15d
	subq	$4, %r12
	cmpl	%esi, %r15d
	jle	.L273
.L272:
	cmpl	$48, -4(%r12)
	je	.L274
.L273:
	testl	%r15d, %r15d
	jne	.L275
	movq	-184(%rbp), %rax
	testb	$8, 12(%rax)
	jne	.L275
	movl	-216(%rbp), %eax
	cmpl	%eax, -4(%r12)
	je	.L696
	.p2align 4,,10
	.p2align 3
.L275:
	movq	-224(%rbp), %rax
	testq	%rax, %rax
	je	.L276
	movl	-308(%rbp), %ecx
	cmpl	%ecx, -284(%rbp)
	movl	%ecx, %r14d
	je	.L277
	movzbl	(%rax), %eax
	movl	$0, -300(%rbp)
	movb	%al, -176(%rbp)
	subl	$1, %eax
	cmpb	$125, %al
	jbe	.L697
.L276:
	movl	-108(%rbp), %eax
	cmpl	$102, %eax
	je	.L284
	movl	-112(%rbp), %edx
	movl	-104(%rbp), %esi
	testl	%edx, %edx
	jne	.L698
	movl	%eax, (%r12)
	movl	$43, %eax
.L359:
	cmpl	$9, %esi
	leaq	8(%r12), %rdi
	movl	%eax, 4(%r12)
	jle	.L288
	movl	$10, %ecx
	.p2align 4,,10
	.p2align 3
.L289:
	leal	(%rcx,%rcx,4), %ecx
	addl	%ecx, %ecx
	cmpl	%esi, %ecx
	jle	.L289
	movl	$-858993459, %r8d
.L290:
	movl	%ecx, %eax
	addq	$4, %rdi
	mull	%r8d
	movl	%esi, %eax
	movl	%edx, %ecx
	cltd
	shrl	$3, %ecx
	idivl	%ecx
	addl	$48, %eax
	cmpl	$10, %ecx
	movl	%edx, %esi
	movl	%eax, -4(%rdi)
	jg	.L290
	movl	%edx, -104(%rbp)
.L378:
	addl	$48, %esi
	leaq	4(%rdi), %r12
	movl	%esi, (%rdi)
.L284:
	movl	-148(%rbp), %eax
	movq	-184(%rbp), %rcx
	testl	%eax, %eax
	movzbl	12(%rcx), %edx
	jne	.L291
	testb	$80, %dl
	je	.L292
.L291:
	subl	$1, -288(%rbp)
.L292:
	movq	%r12, %r14
	movl	-288(%rbp), %ecx
	subq	%r13, %r14
	movq	%r14, %rsi
	sarq	$2, %rsi
	subl	%esi, %ecx
	andl	$32, %edx
	movl	%ecx, -208(%rbp)
	jne	.L426
	movq	-184(%rbp), %rsi
	movl	16(%rsi), %esi
	cmpl	$48, %esi
	je	.L426
	testl	%ecx, %ecx
	jle	.L426
	movl	-212(%rbp), %eax
	movslq	%ecx, %r15
	movq	%rbx, %rdi
	movq	%r15, %rdx
	testl	%eax, %eax
	jne	.L699
	call	_IO_padn
.L295:
	cmpq	%r15, %rax
	je	.L700
.L658:
	movl	-304(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L647
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L690:
	movq	-96(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L230
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L691:
	movl	-256(%rbp), %eax
	addl	$1, -192(%rbp)
	movl	$0, -176(%rbp)
	testl	%eax, %eax
	jle	.L228
	addl	$1, -240(%rbp)
	movl	-240(%rbp), %ecx
	leal	(%rax,%rcx), %r14d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L426:
	movl	$0, -176(%rbp)
.L293:
	testl	%eax, %eax
	je	.L297
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L298
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L299
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L299
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$45, (%rdx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r13
	leaq	-148(%rbp), %rcx
	movdqa	-176(%rbp), %xmm0
	movl	$2, %esi
	leaq	8(%rax), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	call	__mpn_extract_float128
	movl	%eax, %r12d
	movq	%rax, -88(%rbp)
	sall	$6, %r12d
	subl	$112, %r12d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L297:
	movq	-184(%rbp), %rax
	movzbl	12(%rax), %eax
	testb	$64, %al
	je	.L305
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L306
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L307
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L307
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$43, (%rdx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L682:
	testl	%esi, %esi
	movl	%eax, -108(%rbp)
	js	.L406
	movslq	%esi, %rax
	movl	%esi, -192(%rbp)
	addq	$8, %rax
	movq	%rax, -264(%rbp)
.L635:
	movl	-192(%rbp), %eax
	movl	$1, -176(%rbp)
	movl	$2147483647, -268(%rbp)
	movl	$1, -284(%rbp)
	movl	%eax, -256(%rbp)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%rbx, %rdi
	movq	%r8, -192(%rbp)
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	call	__overflow
	cmpl	$-1, %eax
	movl	-176(%rbp), %ecx
	movzbl	-208(%rbp), %edx
	movq	-192(%rbp), %r8
	jne	.L137
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-224(%rbp), %rsi
	movl	%ecx, %edi
	call	__guess_grouping.part.0
	movl	%eax, -300(%rbp)
.L277:
	movl	-300(%rbp), %eax
	testl	%eax, %eax
	je	.L276
	movl	-308(%rbp), %r15d
	movslq	%eax, %rcx
	movq	%r12, %rdx
	movq	%rcx, -176(%rbp)
	leaq	0(%r13,%r15,4), %rsi
	addq	%rcx, %r15
	salq	$2, %r15
	subq	%rsi, %rdx
	leaq	0(%r13,%r15), %rdi
	sarq	$2, %rdx
	call	__wmemmove
	movq	-224(%rbp), %r10
	movl	-272(%rbp), %r11d
	leaq	-4(%r13,%r15), %r9
	movq	-176(%rbp), %rcx
	movsbl	(%r10), %edi
	.p2align 4,,10
	.p2align 3
.L282:
	movl	%r14d, %r8d
	movq	%r9, %rdx
	movl	%r14d, %eax
	subl	%edi, %r8d
	.p2align 4,,10
	.p2align 3
.L278:
	leal	-1(%rax), %esi
	subq	$4, %rdx
	movq	%rsi, %rax
	movl	0(%r13,%rsi,4), %esi
	cmpl	%r8d, %eax
	movl	%esi, 4(%rdx)
	jne	.L278
	leal	-1(%rdi), %edx
	negq	%rdx
	leaq	(%r9,%rdx,4), %rdx
	leaq	-8(%rdx), %r9
	movl	%r11d, -4(%rdx)
	movzbl	1(%r10), %edx
	cmpb	$127, %dl
	je	.L283
	testb	%dl, %dl
	js	.L283
	testb	%dl, %dl
	je	.L280
	addq	$1, %r10
.L281:
	movsbl	%dl, %edi
	movl	%r8d, %r14d
	cmpl	%r8d, %edi
	jb	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	leal	-1(%rax), %edx
	subq	$4, %r9
	cmpq	%r13, %r9
	movq	%rdx, %rax
	movl	0(%r13,%rdx,4), %edx
	movl	%edx, 4(%r9)
	ja	.L283
	leaq	(%r12,%rcx,4), %r12
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L280:
	movzbl	(%r10), %edx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L86:
	testb	%sil, %sil
	movq	-232(%rbp), %rsi
	je	.L701
	movq	32(%rsi), %rdx
	movq	88(%rdx), %rsi
.L629:
	movq	%rsi, -280(%rbp)
	cmpb	$1, (%rsi)
	movl	$0, %edx
	sbbl	%edi, %edi
	notl	%edi
	andl	$-2, %edi
	movl	%edi, -272(%rbp)
	cmpb	$0, (%rsi)
	cmovne	-224(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L118:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L702
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L154:
	subq	$2, %rdx
	movq	%rdx, -56(%rbp)
	jmp	.L153
.L288:
	leaq	12(%r12), %rdi
	movl	$48, 8(%r12)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L685:
	movq	-224(%rbp), %rsi
	movl	-284(%rbp), %edi
	call	__guess_grouping.part.0
	leal	1(%rax), %edx
	movl	%eax, -300(%rbp)
	movslq	%edx, %rdx
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L683:
	movl	-268(%rbp), %eax
	movl	$102, -108(%rbp)
	testl	%eax, %eax
	js	.L407
	movslq	%eax, %rdx
	movl	%eax, -192(%rbp)
.L207:
	movl	-112(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L208
	movslq	-104(%rbp), %rax
	movl	$1, -176(%rbp)
	movl	$2147483647, -268(%rbp)
	leal	1(%rax), %ecx
	leaq	2(%rdx,%rax), %rax
	movq	%rax, -264(%rbp)
	movl	-192(%rbp), %eax
	movl	%ecx, -284(%rbp)
	movl	%eax, -256(%rbp)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L684:
	cmpl	%edx, -268(%rbp)
	jle	.L211
	movl	-268(%rbp), %ecx
	leal	1(%rdx), %eax
	movl	$102, -108(%rbp)
	movl	%eax, -284(%rbp)
	subl	%eax, %ecx
	movl	%ecx, -192(%rbp)
.L354:
	movslq	-268(%rbp), %rax
	addq	$5, %rax
	movq	%rax, -264(%rbp)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L695:
	testw	%di, %di
	jne	.L238
	cmpl	$52, %eax
	jle	.L244
	andl	$1, %r14d
	jne	.L247
.L637:
	testb	%r8b, %r8b
	je	.L244
.L247:
	testl	%r15d, %r15d
	movl	-4(%r12), %edi
	leaq	-4(%r12), %rax
	je	.L418
	movl	-216(%rbp), %edx
	xorl	%r8d, %r8d
	cmpl	%edx, %edi
	jne	.L665
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$48, (%rax)
	subq	$4, %rax
	movl	(%rax), %edi
	addl	$1, %r8d
	cmpl	%edx, %edi
	je	.L250
.L665:
	cmpl	$57, %edi
	je	.L703
	movl	-240(%rbp), %edx
	testl	%edx, %edx
	jle	.L384
	movl	-256(%rbp), %ecx
	leal	-1(%rcx,%rdx), %r9d
	cmpl	%r8d, %ecx
	cmove	%r9d, %esi
.L384:
	addl	$1, %edi
	movl	%edi, (%rax)
.L253:
	cmpl	-216(%rbp), %edi
	je	.L704
	cmpl	%esi, %r15d
	jg	.L272
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L239:
	movl	-148(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L244
.L610:
	cmpl	$52, %eax
	jle	.L637
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L242:
	movl	-148(%rbp), %edi
	testl	%edi, %edi
	jne	.L244
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L669:
	movq	8(%r15), %rax
	leaq	__tens(%rip), %rcx
	addq	$8, %rdi
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	movq	%rax, -56(%rbp)
	movq	(%r15), %rax
	leaq	(%rcx,%rax,8), %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	$0, (%rax)
	addl	$64, -104(%rbp)
	movq	-56(%rbp), %rdx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L217:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L681:
	movl	%r12d, %ecx
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	__mpn_lshift
	movq	-88(%rbp), %rdx
	movl	-104(%rbp), %edi
	leaq	312+_fpioconst_pow10(%rip), %r12
	movl	$12, %r15d
	movl	$0, -176(%rbp)
	movq	%rbx, -192(%rbp)
	leaq	1(%rdx), %rcx
	negl	%edi
	movq	%rcx, -88(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx,%rdx,8)
	leaq	-128(%rbp), %rax
	movl	$1, -112(%rbp)
	movl	%edi, -104(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	8(%rbx,%rcx,8), %rsi
	movq	%rdx, %r8
	movq	%r9, %rcx
	movq	%rax, %rdx
	call	__mpn_mul
.L180:
	movq	-88(%rbp), %rdi
	movq	8(%r12), %rdx
	addq	%rdi, %rdx
	testq	%rax, %rax
	je	.L181
	subq	$1, %rdx
	movq	%rdx, -56(%rbp)
.L182:
	movq	-64(%rbp), %rsi
	leaq	0(,%rdx,8), %r8
	movl	-104(%rbp), %r9d
	movq	%rdx, %r13
	subq	%rdi, %r13
	leaq	-8(%rsi,%r8), %rax
	addq	$1, %r13
	leal	3(%r9), %r10d
	sall	$6, %r13d
	bsrq	(%rax), %rcx
	xorq	$63, %rcx
	movl	%ecx, %edi
	movl	%ecx, %ebx
	notl	%edi
	addl	%edi, %r13d
	movl	%r9d, %edi
	cmpl	%r13d, %r10d
	je	.L705
	leal	2(%r9), %eax
	cmpl	%eax, %r13d
	jle	.L187
.L178:
	leaq	24+_fpioconst_pow10(%rip), %rax
	subl	$1, %r15d
	cmpq	%rax, %r12
	je	.L198
	testl	%edi, %edi
	jle	.L706
.L199:
	subq	$24, %r12
	cmpl	%edi, 20(%r12)
	jg	.L178
	movq	8(%r12), %rax
	movq	-88(%rbp), %rdx
	leaq	__tens(%rip), %rbx
	movq	-96(%rbp), %r9
	movq	-64(%rbp), %rdi
	movq	(%r12), %rcx
	subq	$1, %rax
	cmpq	%rax, %rdx
	jl	.L707
	leaq	8(%rbx,%rcx,8), %rcx
	movq	%rax, %r8
	movq	%r9, %rsi
	call	__mpn_mul
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	subq	$2, %rdx
	movq	%rdx, -56(%rbp)
	jmp	.L182
.L722:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	.p2align 4,,10
	.p2align 3
.L187:
	movl	%r9d, %edi
	movl	$1, %eax
	movl	%r15d, %ecx
	subl	%r13d, %edi
	sall	%cl, %eax
	orl	%eax, -176(%rbp)
	movl	%ebx, %eax
	movl	%edi, -104(%rbp)
	subl	%edi, %eax
	testl	%edi, %edi
	cmovs	%eax, %ebx
	movq	(%rsi), %rax
	testq	%rax, %rax
	jne	.L191
	leaq	8(%rsi), %rax
	movl	$1, %ecx
	xorl	%r10d, %r10d
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%r11, %rcx
	movl	%r8d, %r10d
.L192:
	movq	(%rax), %r9
	movq	%rax, %r14
	addq	$8, %rax
	leal	1(%r10), %r8d
	leaq	0(,%rcx,8), %r13
	leaq	1(%rcx), %r11
	testq	%r9, %r9
	je	.L404
	cmpl	$63, %ebx
	je	.L708
	movl	$63, %eax
	rep bsfq	%r9, %r9
	movq	-96(%rbp), %rdi
	subl	%ebx, %eax
	cmpl	%eax, %r9d
	movl	%eax, %ecx
	jge	.L709
	movslq	%r10d, %rbx
	leaq	-8(%rsi,%r13), %rsi
	subq	%rbx, %rdx
.L634:
	call	__mpn_rshift
	movq	-56(%rbp), %rax
	movl	-104(%rbp), %edi
	subq	%rbx, %rax
	movq	%rax, -88(%rbp)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L705:
	cmpl	$60, %ecx
	jg	.L184
	movl	$60, %ecx
	movl	$10, %edi
	movq	$0, -128(%rbp)
	subl	%ebx, %ecx
	salq	%cl, %rdi
	movq	%rdi, %rcx
	movq	%rdi, -120(%rbp)
.L185:
	cmpq	%rcx, (%rax)
	jb	.L187
	je	.L189
.L633:
	movl	%r9d, %edi
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L305:
	testb	$16, %al
	je	.L304
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	je	.L312
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L313
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L313
	leaq	4(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movl	$32, (%rdx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L298:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L710
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$45, (%rax)
.L316:
	movq	-184(%rbp), %rax
	addl	$1, -176(%rbp)
	movzbl	12(%rax), %eax
.L304:
	testb	$32, %al
	jne	.L318
	movq	-184(%rbp), %rax
	cmpl	$48, 16(%rax)
	jne	.L318
	movl	-208(%rbp), %eax
	testl	%eax, %eax
	jg	.L711
.L318:
	movl	-212(%rbp), %r11d
	movq	-184(%rbp), %rax
	testl	%r11d, %r11d
	jne	.L322
	testb	$8, 13(%rax)
	jne	.L712
.L323:
	movq	-248(%rbp), %rdi
	call	strlen
	movq	-280(%rbp), %rdi
	movq	%rax, -192(%rbp)
	testq	%rdi, %rdi
	je	.L427
	call	strlen
	movq	%rax, %rcx
	movq	%rax, -232(%rbp)
	movslq	-300(%rbp), %rax
	imulq	%rcx, %rax
.L324:
	movq	-264(%rbp), %rcx
	movq	-192(%rbp), %rsi
	movl	-304(%rbp), %r10d
	leaq	2(%rcx,%rsi), %rdx
	addq	%rdx, %rax
	testl	%r10d, %r10d
	movq	%rax, -224(%rbp)
	je	.L713
	movq	-224(%rbp), %rax
	addq	$30, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
.L326:
	cmpq	%r13, %r12
	jbe	.L428
	movq	%rbx, -256(%rbp)
	movq	%r14, %rax
	movq	%r12, %rbx
	movq	%r13, -240(%rbp)
	movl	-272(%rbp), %r15d
	movl	-216(%rbp), %r12d
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L328:
	cmpl	%edx, %r15d
	je	.L714
	movb	%dl, (%rax)
	addq	$1, %rax
.L329:
	addq	$4, %r13
	cmpq	%r13, %rbx
	jbe	.L715
.L331:
	movl	0(%r13), %edx
	cmpl	%r12d, %edx
	jne	.L328
	movq	-192(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L322:
	testb	$8, 13(%rax)
	jne	.L360
	xorl	%r15d, %r15d
.L361:
	sarq	$2, %r14
	cmpq	%r13, %r12
	movq	%r14, %r8
	jne	.L333
	testq	%r14, %r14
	movl	-176(%rbp), %r12d
	je	.L335
	movl	-176(%rbp), %r14d
	movq	%r8, %r12
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L717:
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L341
	leaq	4(%rdx), %rcx
	cmpl	$-1, %esi
	movq	%rcx, 32(%rax)
	movl	%esi, (%rdx)
	sete	%al
.L343:
	testb	%al, %al
	jne	.L658
	addl	$1, %r14d
	subq	$1, %r12
	je	.L716
.L334:
	movq	160(%rbx), %rax
	addq	$4, %r13
	movl	-4(%r13), %esi
	testq	%rax, %rax
	jne	.L717
.L341:
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L714:
	movq	-232(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-184(%rbp), %rax
	movzbl	12(%rax), %r12d
	jmp	.L364
.L671:
	movmskpd	%xmm0, %r13d
	andl	$1, %r13d
	jmp	.L641
.L731:
	fstp	%st(0)
.L641:
	movq	-184(%rbp), %rax
	leaq	.LC5(%rip), %r14
	leaq	.LC6(%rip), %r15
	movl	%r13d, -148(%rbp)
	movslq	8(%rax), %rdx
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	leaq	.LC1(%rip), %rdx
	andw	$256, %ax
	leaq	.LC2(%rip), %rax
	cmovne	%rdx, %r14
	cmovne	%rax, %r15
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%esi, %r8d
	movslq	%edx, %rdx
	sarl	$31, %r8d
	leaq	(%rdi,%rdx,8), %rdi
	movq	%rax, %rdx
	shrl	$26, %r8d
	leal	(%rsi,%r8), %ecx
	movq	%r13, %rsi
	andl	$63, %ecx
	subl	%r8d, %ecx
	call	__mpn_lshift
	movl	-104(%rbp), %ecx
	movq	-88(%rbp), %rsi
	addl	%ecx, %r12d
	leal	63(%r12), %edx
	testl	%r12d, %r12d
	cmovns	%r12d, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rsi
	testq	%rax, %rax
	movq	%rsi, -88(%rbp)
	je	.L145
	leaq	1(%rsi), %rdi
	movq	%rdi, -88(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rax, (%rdi,%rsi,8)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L333:
	movq	216(%rbx), %rax
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	%rax, %rsi
	subq	%rcx, %rdx
	subq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jbe	.L718
	movq	56(%rax), %rax
	xorl	%r14d, %r14d
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L712:
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	imulq	-264(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L676:
	movl	-176(%rbp), %eax
	xorl	%r13d, %r13d
	testq	%rcx, %rcx
	leal	1(%rax), %r8d
	je	.L639
	movq	%r14, -176(%rbp)
	movq	%rcx, %r12
	movl	%r8d, %r14d
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	%dl, (%rax)
.L347:
	leal	(%r14,%r13), %eax
	addq	$1, %r13
	cmpq	%r13, %r12
	je	.L719
.L337:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	movzbl	(%r15,%r13), %edx
	jb	.L345
	movzbl	%dl, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L347
	movl	-304(%rbp), %edi
	movq	-176(%rbp), %r14
	movl	%eax, %r12d
	testl	%edi, %edi
	jne	.L647
	movq	%r14, %rdi
	call	free@PLT
	movq	-296(%rbp), %rdi
	call	free@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L701:
	movq	8(%rsi), %rdx
	movq	72(%rdx), %rsi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L87:
	movq	32(%rsi), %rdx
	xorl	%edi, %edi
	movq	%rdi, -280(%rbp)
	movl	416(%rdx), %esi
	testl	%esi, %esi
	movl	%esi, -272(%rbp)
	movq	-224(%rbp), %rsi
	cmove	%rdi, %rsi
	movq	%rsi, -224(%rbp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L124:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L720
	leaq	1(%rax), %rsi
	movq	%rsi, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L694:
	testq	%r9, %r9
	movq	%r9, %rdi
	je	.L233
	movq	-96(%rbp), %r10
	cmpq	$0, -8(%r10,%r9,8)
	jne	.L416
	.p2align 4,,10
	.p2align 3
.L237:
	subq	$1, %rdi
	je	.L233
	cmpq	$0, -8(%r10,%rdi,8)
	je	.L237
	jmp	.L416
.L306:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L721
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$43, (%rax)
	jmp	.L316
.L699:
	call	_IO_wpadn@PLT
	jmp	.L295
.L213:
	movl	-268(%rbp), %eax
	movl	$102, -108(%rbp)
	movl	$0, -284(%rbp)
	movl	%eax, -192(%rbp)
	jmp	.L354
.L407:
	movl	$6, %edx
	movl	$6, -192(%rbp)
	jmp	.L207
.L711:
	movslq	-208(%rbp), %r15
	movl	-212(%rbp), %eax
	movl	$48, %esi
	movq	%rbx, %rdi
	testl	%eax, %eax
	movq	%r15, %rdx
	je	.L319
	call	_IO_wpadn@PLT
.L320:
	cmpq	%r15, %rax
	jne	.L658
	movl	-208(%rbp), %ecx
	addl	%ecx, -176(%rbp)
	jmp	.L318
.L184:
	movq	-240(%rbp), %rdi
	movl	$64, %ecx
	movabsq	$-6917529027641081856, %rax
	subl	%ebx, %ecx
	movl	$2, %edx
	movq	%rax, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	-104(%rbp), %r9d
	leal	2(%r9), %eax
	movl	%r9d, %edi
	cmpl	%eax, %r13d
	jle	.L722
	leal	3(%r9), %eax
	cmpl	%eax, %r13d
	jne	.L178
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	-120(%rbp), %rcx
	leaq	0(,%rdx,8), %r8
	leaq	-8(%rsi,%r8), %rax
	jmp	.L185
.L696:
	subq	$4, %r12
	jmp	.L275
.L709:
	movq	%r14, %rsi
.L196:
	movslq	%r8d, %rbx
	subq	%rbx, %rdx
	jmp	.L634
.L299:
	movl	$45, %esi
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	sete	%al
.L315:
	testb	%al, %al
	je	.L316
	jmp	.L658
.L710:
	movl	$45, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L315
.L716:
	movl	%r14d, %r12d
	xorl	%r14d, %r14d
.L335:
	movl	-304(%rbp), %esi
	testl	%esi, %esi
	je	.L723
.L348:
	movq	-184(%rbp), %rax
	testb	$32, 12(%rax)
	je	.L78
	movl	-208(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L78
	movslq	%ecx, %r13
	movl	-212(%rbp), %ecx
	movl	16(%rax), %esi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	testl	%ecx, %ecx
	je	.L349
	call	_IO_wpadn@PLT
.L350:
	addl	-208(%rbp), %r12d
	cmpq	%r13, %rax
	jne	.L652
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L408:
	movl	$6, -268(%rbp)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r12, %r8
.L249:
	cmpl	-216(%rbp), %edi
	jne	.L259
	movq	%rax, %r8
	subq	$4, %rax
.L259:
	cmpq	%rax, %r13
	ja	.L261
	movl	-4(%r8), %r9d
	cmpl	$57, %r9d
	jne	.L263
	movq	$-4, %r8
	subq	-296(%rbp), %r8
	leaq	-4(%rax), %rdi
	addq	%rdi, %r8
	notq	%r8
	andq	$-4, %r8
	addq	%rax, %r8
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L260:
	movl	(%rdi), %r9d
	cmpl	$57, %r9d
	jne	.L263
	subq	$4, %rdi
.L264:
	cmpq	%r8, %rdi
	movl	$48, (%rax)
	movq	%rdi, %rax
	jne	.L260
.L261:
	cmpl	$102, -108(%rbp)
	je	.L265
	cmpl	$1, -112(%rbp)
	movq	-296(%rbp), %rax
	movl	$49, 8(%rax)
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	addl	%eax, -104(%rbp)
	jne	.L267
	movl	$0, -112(%rbp)
.L267:
	movl	-308(%rbp), %eax
	movl	-268(%rbp), %ecx
	addl	%r15d, %eax
	cmpl	%ecx, %eax
	jle	.L244
	subl	%ecx, %eax
	movslq	%eax, %rdi
	subl	%eax, %r15d
	salq	$2, %rdi
	subq	%rdi, %r12
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L719:
	movq	-176(%rbp), %r14
.L639:
	movl	%eax, %r12d
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L693:
	movq	-96(%rbp), %rdi
	cmpq	$0, (%rdi)
	movl	$1, %edi
	cmovne	%edi, %r8d
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L687:
	movq	-296(%rbp), %rax
	movl	-216(%rbp), %ecx
	subl	$1, -104(%rbp)
	movl	$0, -308(%rbp)
	movl	$48, 8(%rax)
	leaq	16(%rax), %r12
	movl	%ecx, 12(%rax)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L263:
	addl	$1, %r9d
	movl	%r9d, (%rax)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L250:
	cmpl	-256(%rbp), %r8d
	jne	.L357
	movl	-240(%rbp), %ecx
	testl	%ecx, %ecx
	setg	%sil
	cmpb	$1, %sil
	adcl	$-1, %ecx
	movl	%ecx, -240(%rbp)
.L357:
	cmpb	$103, -312(%rbp)
	je	.L724
.L254:
	movl	(%rax), %edi
.L642:
	movl	-256(%rbp), %esi
	addl	-240(%rbp), %esi
	jmp	.L253
.L724:
	cmpl	$102, -108(%rbp)
	jne	.L254
	movq	-184(%rbp), %rcx
	testb	$8, 12(%rcx)
	je	.L254
	movq	-296(%rbp), %rcx
	leaq	12(%rcx), %rsi
	cmpq	%rsi, %rax
	jne	.L254
	movq	-296(%rbp), %rcx
	cmpl	$48, 8(%rcx)
	movl	12(%rcx), %edi
	jne	.L642
	movl	-256(%rbp), %ecx
	movl	-240(%rbp), %esi
	leal	-1(%rcx,%rsi), %esi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L678:
	bsrq	-8(%rdi,%rdx,8), %r9
	xorq	$63, %r9
	testl	%r9d, %r9d
	je	.L369
	movl	$64, %r14d
	rep bsfq	%r8, %r8
	subl	%r9d, %r14d
.L171:
	cmpl	%r14d, %r8d
	movslq	%r12d, %r15
	jge	.L372
	subq	$8, %r13
	subl	$1, %r12d
	movl	%r14d, %ecx
	movslq	%r12d, %r12
	leaq	(%rdi,%r13), %rsi
	subq	%r12, %rdx
	call	__mpn_rshift
	movq	-96(%rbp), %rdi
	movq	-88(%rbp), %rdx
	movl	%r14d, %ecx
	subq	%r15, -72(%rbp)
	leaq	(%rdi,%r13), %rsi
	subq	%r12, %rdx
	call	__mpn_rshift
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	%rdx, %rax
	subq	%r12, %rax
	cmpq	$0, -8(%rcx,%rax,8)
	jne	.L176
	movq	%rdx, %rax
	subq	%r15, %rax
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L406:
	movq	$14, -264(%rbp)
	movl	$6, -192(%rbp)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L689:
	movl	-192(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L413
	cmpq	$1, -88(%rbp)
	jg	.L226
	movq	-96(%rbp), %rdx
	cmpq	$0, (%rdx)
	jne	.L226
.L413:
	movq	%rax, %r12
	movl	$1, -176(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L369:
	subq	%r12, %rdx
	testq	%rdx, %rdx
	jle	.L166
	leaq	(%rdi,%rax,8), %r8
	xorl	%esi, %esi
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L725:
	movq	(%r8,%rsi,8), %rcx
.L167:
	movq	%rcx, (%rdi,%rsi,8)
	movq	-72(%rbp), %rdx
	addq	$1, %rsi
	subq	%r12, %rdx
	cmpq	%rsi, %rdx
	jg	.L725
.L166:
	movq	%rdx, -72(%rbp)
	movq	-88(%rbp), %rdx
	subq	%r12, %rdx
	testq	%rdx, %rdx
	jle	.L168
	leaq	(%r11,%rax,8), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rcx,%rax,8), %rdx
	movq	%rdx, (%r11,%rax,8)
	movq	-88(%rbp), %rdx
	addq	$1, %rax
	subq	%r12, %rdx
	cmpq	%rax, %rdx
	jg	.L169
.L168:
	movq	%rdx, -88(%rbp)
	jmp	.L160
.L208:
	leaq	2(%rdx), %rax
	movq	%rax, -264(%rbp)
	jmp	.L635
.L110:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$45, %esi
.L662:
	movq	%rbx, %rdi
	call	__woverflow
	movzbl	-208(%rbp), %edx
	movl	-176(%rbp), %ecx
	cmpl	$-1, %eax
	sete	%al
.L127:
	testb	%al, %al
	je	.L128
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L340:
	movl	-176(%rbp), %r12d
	addl	%r8d, %r12d
	jmp	.L335
.L688:
	movq	%r13, %rax
	movl	$0, -308(%rbp)
	jmp	.L224
.L198:
	testl	%edi, %edi
	movq	-192(%rbp), %rbx
	jle	.L200
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movl	$10, %ecx
	movq	-64(%rbp), %rdi
	call	__mpn_mul_1
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%eax, %eax
	movl	$4, %ecx
	movq	%rdx, -56(%rbp)
	rep bsfq	(%rsi), %rax
	cmpl	$4, -104(%rbp)
	cmovle	-104(%rbp), %ecx
	cmpl	%eax, %ecx
	jle	.L201
	movl	$64, %eax
	movq	-96(%rbp), %rdi
	subl	%ecx, %eax
	movl	%eax, %ecx
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L202
	movq	-56(%rbp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, -56(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx,%rdx,8)
.L202:
	movq	-56(%rbp), %rax
	orl	$1, -176(%rbp)
	movq	%rax, -88(%rbp)
.L200:
	movl	-176(%rbp), %eax
	movl	%eax, -104(%rbp)
	jmp	.L160
.L427:
	xorl	%eax, %eax
	movq	$0, -232(%rbp)
	jmp	.L324
.L704:
	movl	-4(%rax), %edi
	movq	%rax, %r8
	subq	$4, %rax
	jmp	.L249
.L706:
	movq	-192(%rbp), %rbx
	jmp	.L200
.L686:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -296(%rbp)
	je	.L647
	movl	$0, -304(%rbp)
	jmp	.L220
.L349:
	call	_IO_padn
	jmp	.L350
.L700:
	movl	-208(%rbp), %ecx
	movl	-148(%rbp), %eax
	movl	%ecx, -176(%rbp)
	jmp	.L293
.L673:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$45, %esi
.L661:
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	movl	-176(%rbp), %ecx
	movzbl	-208(%rbp), %edx
	sete	%al
	jmp	.L127
.L119:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$43, %esi
	jmp	.L662
.L698:
	cmpb	$103, -312(%rbp)
	jne	.L286
	cmpl	$4, %esi
	je	.L726
.L286:
	movl	%eax, (%r12)
	movl	$45, %eax
	jmp	.L359
.L708:
	movslq	%r8d, %r8
	subq	%r8, %rdx
.L375:
	testq	%rdx, %rdx
	jle	.L194
	movq	-96(%rbp), %r9
	leaq	(%rsi,%rcx,8), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L195:
	movq	(%rcx,%rax,8), %rdx
	movq	%rdx, (%r9,%rax,8)
	movq	-56(%rbp), %rdx
	addq	$1, %rax
	subq	%r8, %rdx
	cmpq	%rax, %rdx
	jg	.L195
.L194:
	movq	%rdx, -88(%rbp)
	jmp	.L178
.L265:
	movl	-268(%rbp), %ecx
	cmpl	%ecx, -308(%rbp)
	movq	-296(%rbp), %rax
	je	.L727
	leaq	4(%rax), %r13
	movl	$49, 4(%rax)
	addl	$1, -308(%rbp)
	jmp	.L267
.L428:
	movq	%r14, %rax
	jmp	.L327
.L312:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L728
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
	jmp	.L316
.L201:
	movq	-96(%rbp), %rdi
	call	__mpn_rshift
	jmp	.L202
.L191:
	cmpl	$63, %ebx
	je	.L729
	movl	$63, %ecx
	rep bsfq	%rax, %rax
	xorl	%r8d, %r8d
	subl	%ebx, %ecx
	movq	-96(%rbp), %rdi
	cmpl	%ecx, %eax
	jge	.L196
	leal	1(%rbx), %ecx
	call	__mpn_lshift
	movq	-56(%rbp), %rbx
	movq	-96(%rbp), %rcx
	movl	-104(%rbp), %edi
	leaq	1(%rbx), %rdx
	movq	%rdx, -88(%rbp)
	movq	%rax, -8(%rcx,%rdx,8)
	jmp	.L178
.L319:
	call	_IO_padn
	jmp	.L320
.L723:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	free@PLT
	movq	-296(%rbp), %rdi
	call	free@PLT
	movq	$0, -296(%rbp)
	jmp	.L348
.L189:
	movq	-128(%rbp), %rax
	cmpq	%rax, -16(%rsi,%r8)
	jnb	.L633
	jmp	.L187
.L713:
	movq	%rax, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L326
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-224(%rbp), %r12
	movq	%rax, %rsi
	movq	%r14, %rdi
	addq	%r14, %r12
	movq	%r12, %rdx
	call	_i18n_number_rewrite
	movq	%rax, %r15
	movq	%r12, %rax
	jmp	.L332
.L718:
	xorl	%r14d, %r14d
.L379:
	movq	%r8, -224(%rbp)
	movq	%rax, -192(%rbp)
	call	_IO_vtable_check
	movl	-212(%rbp), %r9d
	movq	-192(%rbp), %rax
	movq	-224(%rbp), %r8
	testl	%r9d, %r9d
	movq	56(%rax), %rax
	cmove	%r15, %r13
	jmp	.L339
.L360:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_i18n_number_rewrite
	movq	%rax, %r15
	jmp	.L361
.L702:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$43, %esi
	jmp	.L661
.L125:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$32, %esi
	jmp	.L662
.L680:
	movl	%r9d, %ecx
	movq	%rdi, %rsi
	movl	%r9d, -176(%rbp)
	call	__mpn_lshift
	movq	-96(%rbp), %rdi
	movl	-176(%rbp), %r9d
	movq	-88(%rbp), %rdx
	movl	%r9d, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L160
	movq	-88(%rbp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, -88(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rax, (%rcx,%rdx,8)
	jmp	.L160
.L161:
	bsrq	-8(%rdi,%rdx,8), %rax
	xorq	$63, %rax
	testl	%eax, %eax
	movl	%eax, %r9d
	je	.L160
	movq	-96(%rbp), %r11
	movq	%rdi, %rsi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L162:
	bsrq	-8(%rdi,%rdx,8), %rax
	xorq	$63, %rax
	testl	%eax, %eax
	movl	%eax, %r9d
	je	.L160
	movl	$64, %r14d
	xorl	%r8d, %r8d
	movq	%rdi, %rsi
	rep bsfq	(%r11), %r8
	subl	%eax, %r14d
	xorl	%r13d, %r13d
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$43, %esi
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L315
.L727:
	movl	-216(%rbp), %ecx
	movl	$49, (%rax)
	movl	%ecx, 4(%rax)
	movq	-184(%rbp), %rax
	testb	$8, 12(%rax)
	jne	.L436
	xorl	%eax, %eax
	testl	%r15d, %r15d
	je	.L269
.L436:
	movslq	-308(%rbp), %rax
	movq	-296(%rbp), %rcx
	movl	$48, 8(%rcx,%rax,4)
	leal	1(%r15), %eax
.L269:
	movl	-308(%rbp), %r15d
	addl	$1, -104(%rbp)
	movq	-296(%rbp), %r13
	movl	$1, -308(%rbp)
	addl	%eax, %r15d
	movq	-184(%rbp), %rax
	movslq	8(%rax), %rdi
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$256, %ax
	cmpw	$1, %ax
	sbbl	%eax, %eax
	andl	$32, %eax
	addl	$69, %eax
	movl	%eax, -108(%rbp)
	jmp	.L267
.L720:
	movb	%dl, -208(%rbp)
	movl	%ecx, -176(%rbp)
	movl	$32, %esi
	jmp	.L661
.L729:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L375
.L313:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	__woverflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L315
.L721:
	movl	$43, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L315
.L728:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	sete	%al
	jmp	.L315
.L726:
	leaq	.LC13(%rip), %rsi
	movl	$6, %edx
	movq	%r13, %rdi
	call	__wmemcpy
	movl	-216(%rbp), %eax
	movl	%eax, 4(%r13)
	leaq	8(%r13), %rax
	cmpq	%r12, %rax
	jbe	.L730
	addq	$20, %r12
	jmp	.L284
.L238:
	call	abort
.L730:
	movq	%r12, %rdx
	leaq	24(%r13), %rdi
	movl	$48, %esi
	subq	%rax, %rdx
	addq	$16, %r12
	sarq	$2, %rdx
	call	__wmemset
	jmp	.L284
	.size	__printf_fp_l, .-__printf_fp_l
	.p2align 4,,15
	.globl	___printf_fp
	.type	___printf_fp, @function
___printf_fp:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	movq	%rsi, %rax
	movq	%fs:(%rcx), %rsi
	movq	%rdx, %rcx
	movq	%rax, %rdx
	jmp	__printf_fp_l
	.size	___printf_fp, .-___printf_fp
	.globl	__printf_fp
	.hidden	__printf_fp
	.set	__printf_fp,___printf_fp
	.p2align 4,,15
	.globl	__guess_grouping
	.hidden	__guess_grouping
	.type	__guess_grouping, @function
__guess_grouping:
	movzbl	(%rsi), %eax
	subl	$1, %eax
	cmpb	$125, %al
	ja	.L735
	jmp	__guess_grouping.part.0
	.p2align 4,,10
	.p2align 3
.L735:
	xorl	%eax, %eax
	ret
	.size	__guess_grouping, .-__guess_grouping
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC10:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC11:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC12:
	.long	4294967295
	.long	2146435071
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	__wmemset
	.hidden	abort
	.hidden	__wmemcpy
	.hidden	_IO_vtable_check
	.hidden	__wmemmove
	.hidden	__overflow
	.hidden	__mpn_extract_float128
	.hidden	__libc_alloca_cutoff
	.hidden	__mpn_lshift
	.hidden	__woverflow
	.hidden	__mpn_rshift
	.hidden	__mpn_cmp
	.hidden	__mpn_extract_double
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	__mpn_extract_long_double
	.hidden	_IO_padn
	.hidden	__wcrtomb
	.hidden	strlen
	.hidden	_nl_current_LC_CTYPE
	.hidden	__libc_scratch_buffer_set_array_size
	.hidden	__towctrans
	.hidden	__mpn_mul_1
	.hidden	__mpn_divrem
