	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	cuserid
	.type	cuserid, @function
cuserid:
.LFB64:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx
	subq	$1088, %rsp
	.cfi_def_cfa_offset 1104
	call	__geteuid
	leaq	64(%rsp), %rdx
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %r8
	movl	$1024, %ecx
	movl	%eax, %edi
	call	__getpwuid_r
	testl	%eax, %eax
	jne	.L2
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L2
	leaq	name.9962(%rip), %rdx
	testq	%rbx, %rbx
	cmove	%rdx, %rbx
	movl	$8, %edx
	movb	$0, 8(%rbx)
	movq	(%rax), %rsi
	movq	%rbx, %rdi
	call	__GI_strncpy
.L1:
	addq	$1088, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L1
	movb	$0, (%rbx)
	addq	$1088, %rsp
	.cfi_def_cfa_offset 16
	movq	%rbx, %rax
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE64:
	.size	cuserid, .-cuserid
	.local	name.9962
	.comm	name.9962,9,8
	.hidden	__getpwuid_r
	.hidden	__geteuid
