	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	putw
	.type	putw, @function
putw:
	subq	$24, %rsp
	movq	%rsi, %rcx
	movl	$1, %edx
	movl	%edi, 12(%rsp)
	leaq	12(%rsp), %rdi
	movl	$4, %esi
	call	__GI__IO_fwrite
	testq	%rax, %rax
	sete	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	putw, .-putw
