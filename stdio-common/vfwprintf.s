	.text
	.p2align 4,,15
	.globl	__vfwprintf
	.type	__vfwprintf, @function
__vfwprintf:
.LFB68:
	xorl	%ecx, %ecx
	jmp	__vfwprintf_internal
.LFE68:
	.size	__vfwprintf, .-__vfwprintf
	.weak	vfwprintf
	.set	vfwprintf,__vfwprintf
	.hidden	__vfwprintf_internal
