	.text
	.p2align 4,,15
	.globl	__snprintf
	.hidden	__snprintf
	.type	__snprintf, @function
__snprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L3
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L3:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vsnprintf_internal
	addq	$216, %rsp
	ret
	.size	__snprintf, .-__snprintf
	.weak	snprintf
	.set	snprintf,__snprintf
	.hidden	__vsnprintf_internal
