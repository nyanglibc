	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vfprintf
	.type	__vfprintf, @function
__vfprintf:
.LFB68:
	xorl	%ecx, %ecx
	jmp	__vfprintf_internal
.LFE68:
	.size	__vfprintf, .-__vfprintf
	.globl	__GI_vfprintf
	.hidden	__GI_vfprintf
	.set	__GI_vfprintf,__vfprintf
	.globl	vfprintf
	.set	vfprintf,__GI_vfprintf
	.globl	_IO_vfprintf
	.set	_IO_vfprintf,__vfprintf
	.hidden	__vfprintf_internal
