	.text
	.p2align 4,,15
	.type	locked_vfxprintf, @function
locked_vfxprintf:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rdx, %r13
	movl	%ecx, %r14d
	subq	$40, %rsp
	movl	192(%r12), %eax
	movq	%rsi, -72(%rbp)
	testl	%eax, %eax
	jle	.L12
	call	strlen
	leaq	1(%rax), %r15
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, %r15
	ja	.L13
	leaq	0(,%r15,4), %rbx
	movq	%rbx, %rdi
	call	__libc_alloca_cutoff
	cmpq	$4096, %rbx
	jbe	.L5
	testl	%eax, %eax
	je	.L14
.L5:
	addq	$30, %rbx
	leaq	-56(%rbp), %rcx
	leaq	-72(%rbp), %rsi
	andq	$-16, %rbx
	movq	%r15, %rdx
	movq	$0, -56(%rbp)
	subq	%rbx, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdi
	call	__mbsrtowcs
	cmpl	$-1, %eax
	jne	.L15
.L7:
	movl	$-1, %eax
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__vfwprintf_internal
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %rdi
	call	__vfprintf_internal
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L7
	leaq	-56(%rbp), %rcx
	leaq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	$0, -56(%rbp)
	call	__mbsrtowcs
	cmpl	$-1, %eax
	je	.L9
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__vfwprintf_internal
.L9:
	movq	%rbx, %rdi
	movl	%eax, -76(%rbp)
	call	free@PLT
	movl	-76(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	locked_vfxprintf, .-locked_vfxprintf
	.p2align 4,,15
	.globl	__vfxprintf
	.hidden	__vfxprintf
	.type	__vfxprintf, @function
__vfxprintf:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdi, %rdi
	cmove	stderr(%rip), %rbx
	movl	(%rbx), %r8d
	andl	$32768, %r8d
	jne	.L18
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L19
#APP
# 71 "fxprintf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L20
	movl	$1, %r8d
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %r8d, (%rdi)
# 0 "" 2
#NO_APP
.L21:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L19:
	addl	$1, 4(%rdi)
.L18:
	movq	%rbx, %rdi
	call	locked_vfxprintf
	testl	$32768, (%rbx)
	movl	%eax, %r8d
	jne	.L16
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L16
	movq	$0, 8(%rdi)
#APP
# 73 "fxprintf.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L25
	subl	$1, (%rdi)
.L16:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %r9d
	movl	%r8d, %eax
	lock cmpxchgl	%r9d, (%rdi)
	je	.L21
	movl	%ecx, 28(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movl	28(%rsp), %ecx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L25:
#APP
# 73 "fxprintf.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L16
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 73 "fxprintf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
	.size	__vfxprintf, .-__vfxprintf
	.p2align 4,,15
	.globl	__fxprintf
	.hidden	__fxprintf
	.type	__fxprintf, @function
__fxprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L29
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L29:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vfxprintf
	addq	$216, %rsp
	ret
	.size	__fxprintf, .-__fxprintf
	.p2align 4,,15
	.globl	__fxprintf_nocancel
	.hidden	__fxprintf_nocancel
	.type	__fxprintf_nocancel, @function
__fxprintf_nocancel:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$232, %rsp
	testb	%al, %al
	movq	%rdx, 64(%rsp)
	movq	%rcx, 72(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r9, 88(%rsp)
	je	.L42
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm1, 112(%rsp)
	movaps	%xmm2, 128(%rsp)
	movaps	%xmm3, 144(%rsp)
	movaps	%xmm4, 160(%rsp)
	movaps	%xmm5, 176(%rsp)
	movaps	%xmm6, 192(%rsp)
	movaps	%xmm7, 208(%rsp)
.L42:
	testq	%rbx, %rbx
	leaq	256(%rsp), %rax
	cmove	stderr(%rip), %rbx
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movl	$16, 24(%rsp)
	movl	$48, 28(%rsp)
	movq	%rax, 40(%rsp)
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L34
	movq	136(%rbx), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L35
#APP
# 95 "fxprintf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L36
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L37:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L35:
	addl	$1, 4(%rdi)
.L34:
	movl	116(%rbx), %ebp
	leaq	24(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%ebp, %eax
	orl	$2, %eax
	movl	%eax, 116(%rbx)
	call	locked_vfxprintf
	testl	$32768, (%rbx)
	movl	%eax, %r8d
	movl	%ebp, 116(%rbx)
	jne	.L31
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L31
	movq	$0, 8(%rdi)
#APP
# 102 "fxprintf.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L41
	subl	$1, (%rdi)
.L31:
	addq	$232, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L37
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L41:
#APP
# 102 "fxprintf.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L31
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 102 "fxprintf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L31
	.size	__fxprintf_nocancel, .-__fxprintf_nocancel
	.hidden	__lll_lock_wait_private
	.hidden	__vfprintf_internal
	.hidden	__vfwprintf_internal
	.hidden	__mbsrtowcs
	.hidden	__libc_alloca_cutoff
	.hidden	strlen
