	.text
	.p2align 4,,15
	.globl	__vprintf
	.type	__vprintf, @function
__vprintf:
.LFB45:
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdout(%rip), %rdi
	xorl	%ecx, %ecx
	jmp	__vfprintf_internal
.LFE45:
	.size	__vprintf, .-__vprintf
	.globl	vprintf
	.set	vprintf,__vprintf
	.hidden	__vfprintf_internal
