	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__funlockfile
	.hidden	__funlockfile
	.type	__funlockfile, @function
__funlockfile:
	movq	136(%rdi), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 27 "../sysdeps/pthread/funlockfile.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L4
	subl	$1, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
#APP
# 27 "../sysdeps/pthread/funlockfile.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 27 "../sysdeps/pthread/funlockfile.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L1:
	rep ret
	.size	__funlockfile, .-__funlockfile
	.weak	funlockfile
	.set	funlockfile,__funlockfile
	.globl	_IO_funlockfile
	.set	_IO_funlockfile,__funlockfile
