	.text
	.p2align 4,,15
	.globl	__dprintf
	.hidden	__dprintf
	.type	__dprintf, @function
__dprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L3
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L3:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vdprintf_internal
	addq	$216, %rsp
	ret
	.size	__dprintf, .-__dprintf
	.weak	dprintf
	.hidden	dprintf
	.set	dprintf,__dprintf
	.hidden	__vdprintf_internal
