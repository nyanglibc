	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	movq	__printf_modifier_table(%rip), %rax
	testq	%rax, %rax
	je	.L1
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rax,%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L4
	movq	__printf_modifier_table(%rip), %rax
.L3:
	addq	$8, %rbp
	cmpq	$2040, %rbp
	jne	.L5
	addq	$8, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
.L1:
	rep ret
	.size	free_mem, .-free_mem
	.text
	.p2align 4,,15
	.globl	__register_printf_modifier
	.type	__register_printf_modifier, @function
__register_printf_modifier:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %eax
	subl	$1, %eax
	cmpl	$254, %eax
	ja	.L17
	movq	%rdi, %rbx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$255, %edx
	ja	.L17
.L16:
	addq	$4, %rbx
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L19
	movl	next_bit(%rip), %eax
	subl	$16, %eax
	cmpl	$7, %eax
	jbe	.L35
	movq	%rdi, %rbp
#APP
# 65 "reg-modifier.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L21
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L22:
	cmpq	$0, __printf_modifier_table(%rip)
	je	.L23
.L26:
	subq	%rbp, %rbx
	leaq	16(%rbx), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L24
	movzbl	0(%rbp), %edx
	movq	__printf_modifier_table(%rip), %rax
	sarq	$2, %rbx
	movl	next_bit(%rip), %ecx
	leaq	4(%rbp), %rsi
	leaq	12(%r12), %rdi
	movq	(%rax,%rdx,8), %rax
	movq	%rbx, %rdx
	movq	%rax, (%r12)
	leal	1(%rcx), %eax
	movl	%eax, next_bit(%rip)
	movl	$1, %eax
	sall	%cl, %eax
	movl	%eax, 8(%r12)
	call	__wmemcpy
	movzbl	0(%rbp), %edx
	movq	__printf_modifier_table(%rip), %rax
	movl	8(%r12), %r8d
	movq	%r12, (%rax,%rdx,8)
.L27:
#APP
# 92 "reg-modifier.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L28
	subl	$1, lock(%rip)
.L15:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$8, %esi
	movl	$255, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, __printf_modifier_table(%rip)
	jne	.L26
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$-1, %r8d
	jmp	.L27
.L28:
	xorl	%eax, %eax
#APP
# 92 "reg-modifier.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 92 "reg-modifier.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, lock(%rip)
	je	.L22
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L22
.L35:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$28, %fs:(%rax)
	jmp	.L15
	.size	__register_printf_modifier, .-__register_printf_modifier
	.weak	register_printf_modifier
	.set	register_printf_modifier,__register_printf_modifier
	.p2align 4,,15
	.globl	__handle_registered_modifier_mb
	.hidden	__handle_registered_modifier_mb
	.type	__handle_registered_modifier_mb, @function
__handle_registered_modifier_mb:
	movq	(%rdi), %r11
	movq	__printf_modifier_table(%rip), %rax
	movzbl	(%r11), %edx
	movq	(%rax,%rdx,8), %r9
	testq	%r9, %r9
	je	.L45
	pushq	%r14
	pushq	%r13
	xorl	%r14d, %r14d
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	movzbl	1(%r11), %ebp
	leaq	1(%r11), %rbx
	xorl	%r13d, %r13d
	movl	%ebp, %r10d
	.p2align 4,,10
	.p2align 3
.L43:
	testb	%r10b, %r10b
	leaq	12(%r9), %r8
	movl	12(%r9), %edx
	je	.L46
	testl	%edx, %edx
	je	.L47
	cmpl	%ebp, %edx
	jne	.L40
	movq	%rbx, %rcx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L58:
	testl	%edx, %edx
	je	.L44
	cmpl	%edx, %eax
	jne	.L40
.L41:
	addq	$1, %rcx
	movzbl	(%rcx), %eax
	addq	$4, %r8
	movl	(%r8), %edx
	testb	%al, %al
	jne	.L58
.L38:
	testl	%edx, %edx
	je	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%r9), %r9
	testq	%r9, %r9
	jne	.L43
	testl	%r13d, %r13d
	movl	$1, %eax
	je	.L36
	orw	%r13w, 14(%rsi)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
.L36:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L47:
	movq	%rbx, %rcx
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rcx, %rax
	movslq	%r12d, %rdx
	subq	%r11, %rax
	cmpq	%rdx, %rax
	jle	.L40
	movl	%eax, %r12d
	movl	8(%r9), %r13d
	movq	%rcx, %r14
	jmp	.L40
.L46:
	movq	%rbx, %rcx
	jmp	.L38
.L45:
	movl	$1, %eax
	ret
	.size	__handle_registered_modifier_mb, .-__handle_registered_modifier_mb
	.p2align 4,,15
	.globl	__handle_registered_modifier_wc
	.hidden	__handle_registered_modifier_wc
	.type	__handle_registered_modifier_wc, @function
__handle_registered_modifier_wc:
	movq	(%rdi), %r11
	movq	__printf_modifier_table(%rip), %rax
	movl	(%r11), %edx
	movq	(%rax,%rdx,8), %r9
	testq	%r9, %r9
	je	.L68
	pushq	%r13
	pushq	%r12
	xorl	%r13d, %r13d
	pushq	%rbp
	pushq	%rbx
	leaq	4(%r11), %rbx
	movl	4(%r11), %r10d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L66:
	testl	%r10d, %r10d
	leaq	12(%r9), %rcx
	movl	12(%r9), %eax
	je	.L69
	testl	%eax, %eax
	je	.L70
	cmpl	%r10d, %eax
	jne	.L63
	movq	%rbx, %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L81:
	testl	%eax, %eax
	je	.L67
	cmpl	%r8d, %eax
	jne	.L63
.L64:
	addq	$4, %rdx
	movl	(%rdx), %r8d
	addq	$4, %rcx
	movl	(%rcx), %eax
	testl	%r8d, %r8d
	jne	.L81
.L61:
	testl	%eax, %eax
	je	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	movq	(%r9), %r9
	testq	%r9, %r9
	jne	.L66
	testl	%r12d, %r12d
	movl	$1, %eax
	je	.L59
	orw	%r12w, 14(%rsi)
	xorl	%eax, %eax
	movq	%r13, (%rdi)
.L59:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L70:
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rdx, %rax
	movslq	%ebp, %rcx
	subq	%r11, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	jle	.L63
	movl	%eax, %ebp
	movl	8(%r9), %r12d
	movq	%rdx, %r13
	jmp	.L63
.L69:
	movq	%rbx, %rdx
	jmp	.L61
.L68:
	movl	$1, %eax
	ret
	.size	__handle_registered_modifier_wc, .-__handle_registered_modifier_wc
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	next_bit
	.comm	next_bit,4,4
	.local	lock
	.comm	lock,4,4
	.hidden	__printf_modifier_table
	.comm	__printf_modifier_table,8,8
	.hidden	__lll_lock_wait_private
	.hidden	__wmemcpy
