	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___dprintf
	.hidden	__GI___dprintf
	.type	__GI___dprintf, @function
__GI___dprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L3
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L3:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vdprintf_internal
	addq	$216, %rsp
	ret
	.size	__GI___dprintf, .-__GI___dprintf
	.globl	__dprintf
	.set	__dprintf,__GI___dprintf
	.globl	dprintf
	.set	dprintf,__dprintf
	.weak	__GI_dprintf
	.hidden	__GI_dprintf
	.set	__GI_dprintf,__dprintf
	.hidden	__vdprintf_internal
