	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	__GI__itoa_upper_digits
	.globl	__GI__itoa_upper_digits
	.section	.rodata
	.align 32
	.type	__GI__itoa_upper_digits, @object
	.size	__GI__itoa_upper_digits, 36
__GI__itoa_upper_digits:
	.ascii	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	.globl	_itoa_upper_digits
	.set	_itoa_upper_digits,__GI__itoa_upper_digits
