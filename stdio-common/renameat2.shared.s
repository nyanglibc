	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___renameat2
	.hidden	__GI___renameat2
	.type	__GI___renameat2, @function
__GI___renameat2:
	testl	%r8d, %r8d
	je	.L8
	movq	%rcx, %r10
	movl	$316, %ecx
	movl	%ecx, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/renameat2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L9
	cmpl	$-1, %eax
	je	.L10
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	jmp	__GI___renameat
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rcx
	movl	%fs:(%rcx), %edx
.L4:
	cmpl	$38, %edx
	movl	$-1, %eax
	jne	.L1
	movl	$22, %fs:(%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rcx
	negl	%edx
	movl	%edx, %fs:(%rcx)
	jmp	.L4
	.size	__GI___renameat2, .-__GI___renameat2
	.globl	__renameat2
	.set	__renameat2,__GI___renameat2
	.weak	renameat2
	.set	renameat2,__renameat2
