	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isoc99_vfscanf
	.hidden	__GI___isoc99_vfscanf
	.type	__GI___isoc99_vfscanf, @function
__GI___isoc99_vfscanf:
.LFB68:
	.cfi_startproc
	movl	$2, %ecx
	jmp	__vfscanf_internal
	.cfi_endproc
.LFE68:
	.size	__GI___isoc99_vfscanf, .-__GI___isoc99_vfscanf
	.globl	__isoc99_vfscanf
	.set	__isoc99_vfscanf,__GI___isoc99_vfscanf
	.hidden	__vfscanf_internal
