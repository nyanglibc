	.text
	.p2align 4,,15
	.globl	__isoc99_sscanf
	.hidden	__isoc99_sscanf
	.type	__isoc99_sscanf, @function
__isoc99_sscanf:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$448, %rsp
	testb	%al, %al
	movq	%rdx, 288(%rsp)
	movq	%rcx, 296(%rsp)
	movq	%r8, 304(%rsp)
	movq	%r9, 312(%rsp)
	je	.L3
	movaps	%xmm0, 320(%rsp)
	movaps	%xmm1, 336(%rsp)
	movaps	%xmm2, 352(%rsp)
	movaps	%xmm3, 368(%rsp)
	movaps	%xmm4, 384(%rsp)
	movaps	%xmm5, 400(%rsp)
	movaps	%xmm6, 416(%rsp)
	movaps	%xmm7, 432(%rsp)
.L3:
	leaq	32(%rsp), %rbx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movl	$32768, %esi
	movq	%rbx, %rdi
	movq	$0, 168(%rsp)
	call	_IO_no_init@PLT
	leaq	_IO_str_jumps(%rip), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, 248(%rsp)
	call	_IO_str_init_static_internal@PLT
	leaq	480(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$2, %ecx
	movl	$16, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	272(%rsp), %rax
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vfscanf_internal
	addq	$448, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__isoc99_sscanf, .-__isoc99_sscanf
	.hidden	__vfscanf_internal
	.hidden	_IO_str_jumps
