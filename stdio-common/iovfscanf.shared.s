	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __IO_vfscanf,_IO_vfscanf@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__IO_vfscanf
	.type	__IO_vfscanf, @function
__IO_vfscanf:
	pushq	%rbx
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	call	__vfscanf_internal
	testq	%rbx, %rbx
	jne	.L8
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%edx, %edx
	cmpl	$-1, %eax
	sete	%dl
	movl	%edx, (%rbx)
	popq	%rbx
	ret
	.size	__IO_vfscanf, .-__IO_vfscanf
	.hidden	__vfscanf_internal
