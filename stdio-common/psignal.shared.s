	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	": "
.LC2:
	.string	"%s%s%s\n"
.LC3:
	.string	"%s%sUnknown signal %d\n"
.LC4:
	.string	"Unknown signal"
.LC5:
	.string	"%s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	psignal
	.type	psignal, @function
psignal:
.LFB67:
	.cfi_startproc
	pushq	%r12
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%edi, %ebp
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	subq	$16, %rsp
	.cfi_def_cfa_offset 48
	testq	%rsi, %rsi
	je	.L8
	cmpb	$0, (%rsi)
	movq	%rsi, %rbx
	leaq	.LC1(%rip), %r12
	je	.L8
.L2:
	cmpl	$64, %ebp
	ja	.L3
	leaq	__GI___sys_siglist(%rip), %rdx
	movslq	%ebp, %rax
	movq	(%rdx,%rax,8), %rsi
	movl	$5, %edx
	testq	%rsi, %rsi
	jne	.L13
.L3:
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movl	%ebp, %r8d
	xorl	%eax, %eax
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L15
	movq	8(%rsp), %rdx
	leaq	.LC5(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	8(%rsp), %rdi
	call	free@PLT
	addq	$16, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	.LC0(%rip), %r12
	movq	%r12, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC4(%rip), %rsi
	movl	$5, %edx
.L13:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r8
	movq	%r12, %rcx
	movq	%rbx, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$16, %rsp
	.cfi_def_cfa_offset 32
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%rbp
	.cfi_def_cfa_offset 16
	popq	%r12
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE67:
	.size	psignal, .-psignal
	.hidden	__fxprintf
