	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/tty"
#NO_APP
	.text
	.p2align 4,,15
	.globl	ctermid
	.type	ctermid, @function
ctermid:
	testq	%rdi, %rdi
	leaq	.LC0(%rip), %rax
	je	.L1
	movabsq	$8751747723086357551, %rax
	movb	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdi, %rax
.L1:
	rep ret
	.size	ctermid, .-ctermid
