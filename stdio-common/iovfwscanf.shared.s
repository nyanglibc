	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __IO_vfwscanf,_IO_vfwscanf@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__IO_vfwscanf
	.type	__IO_vfwscanf, @function
__IO_vfwscanf:
	pushq	%rbx
	movq	%rcx, %rbx
	xorl	%ecx, %ecx
	call	__vfwscanf_internal
	testq	%rbx, %rbx
	jne	.L8
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%edx, %edx
	cmpl	$-1, %eax
	sete	%dl
	movl	%edx, (%rbx)
	popq	%rbx
	ret
	.size	__IO_vfwscanf, .-__IO_vfwscanf
	.hidden	__vfwscanf_internal
