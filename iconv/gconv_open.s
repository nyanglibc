	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"//"
.LC1:
	.string	"INTERNAL"
	.text
	.p2align 4,,15
	.globl	__gconv_open
	.hidden	__gconv_open
	.type	__gconv_open, @function
__gconv_open:
	pushq	%rbp
	movl	$3, %ecx
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %r14
	pushq	%rbx
	xorl	%ebx, %ebx
	movl	%edx, %r15d
	subq	$72, %rsp
	cmpb	$0, 17(%rdi)
	movzbl	16(%rdi), %eax
	movq	8(%rdi), %r12
	movq	%rsi, -112(%rbp)
	movq	(%rdi), %r13
	movq	%r14, %rdi
	setne	%bl
	movb	%al, -97(%rbp)
	movq	%r12, %rsi
	addl	%ebx, %ebx
	repz cmpsb
	je	.L28
.L3:
	movl	$3, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	repz cmpsb
	je	.L29
.L4:
	leaq	-56(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	movl	%r15d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__gconv_find_transform
	testl	%eax, %eax
	movl	%eax, -104(%rbp)
	movq	$0, -96(%rbp)
	je	.L30
.L5:
	movq	-112(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	%rbx, (%rax)
	movl	-104(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-56(%rbp), %r12
	leaq	(%r12,%r12,2), %r13
	salq	$4, %r13
	leaq	16(%r13), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, -96(%rbp)
	movq	-64(%rbp), %rcx
	je	.L6
	leaq	16(%rax), %rdi
	xorl	%esi, %esi
	movq	%rcx, 8(%rax)
	movq	%r12, (%rax)
	movq	%r13, %rdx
	call	memset@PLT
	testq	%r12, %r12
	je	.L5
	leaq	56(%r14), %r15
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	movq	-56(%rbp), %r12
	leaq	-1(%r12), %rax
	cmpq	%r13, %rax
	jbe	.L8
	movq	-64(%rbp), %rcx
	movl	%ebx, -24(%r15)
	imull	$8160, 84(%rcx,%r14), %eax
	movq	%rcx, -80(%rbp)
	movslq	%eax, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -88(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -40(%r15)
	movq	-80(%rbp), %rcx
	je	.L9
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %r13
	addq	$48, %r15
	addq	$104, %r14
	addq	%rdx, %rax
	movq	%rax, -80(%r15)
	cmpq	%r13, %r12
	jbe	.L5
.L10:
	cmpb	$0, -97(%rbp)
	leaq	1(%r13), %rax
	movq	%r15, -8(%r15)
	movq	%rax, -72(%rbp)
	je	.L7
	movq	-64(%rbp), %rax
	leaq	_nl_C_locobj(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	24(%rax,%r14), %rdi
	call	__strcasecmp_l
	movl	%ebx, %edx
	orl	$8, %edx
	testl	%eax, %eax
	cmove	%edx, %ebx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L29:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	176(%rax), %r14
	movq	%r14, %rdi
	call	strlen
	leaq	33(%rax), %rcx
	movq	%rax, %rdx
	movq	%r14, %rsi
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	movq	%r13, %rdi
	call	__mempcpy@PLT
	movl	$12079, %edx
	movb	$0, 2(%rax)
	movw	%dx, (%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	176(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -72(%rbp)
	call	strlen
	leaq	33(%rax), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	movq	%r12, %rdi
	call	__mempcpy@PLT
	movl	$12079, %ecx
	movb	$0, 2(%rax)
	movw	%cx, (%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%fs:(%rbx), %eax
	movl	%eax, -72(%rbp)
.L12:
	movq	%r12, %rsi
	movq	%rcx, %rdi
	call	__gconv_close_transform
	movl	-72(%rbp), %eax
	movl	$3, -104(%rbp)
	movq	$0, -96(%rbp)
	movl	%eax, %fs:(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	0(%r13,%r13,2), %rax
	movq	-96(%rbp), %rsi
	orl	$1, %ebx
	salq	$4, %rax
	movl	%ebx, 32(%rsi,%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rbx
	testq	%r13, %r13
	leaq	-1(%r13), %r14
	movl	%fs:(%rbx), %eax
	movl	%eax, -72(%rbp)
	je	.L14
	leaq	0(%r13,%r13,2), %rax
	movq	-96(%rbp), %rsi
	salq	$4, %rax
	leaq	-32(%rsi,%rax), %r15
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%r15), %rdi
	subq	$1, %r14
	subq	$48, %r15
	call	free@PLT
	cmpq	$-1, %r14
	jne	.L11
	movq	-56(%rbp), %r12
	movq	-64(%rbp), %rcx
.L14:
	movq	-96(%rbp), %rdi
	movq	%rcx, -80(%rbp)
	call	free@PLT
	movq	-80(%rbp), %rcx
	jmp	.L12
	.size	__gconv_open, .-__gconv_open
	.hidden	__gconv_close_transform
	.hidden	strlen
	.hidden	_nl_current_LC_CTYPE
	.hidden	__strcasecmp_l
	.hidden	_nl_C_locobj
	.hidden	__gconv_find_transform
