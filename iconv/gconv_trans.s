	.text
	.p2align 4,,15
	.globl	__gconv_transliterate
	.hidden	__gconv_transliterate
	.type	__gconv_transliterate, @function
__gconv_transliterate:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$152, %rsp
	movq	(%rcx), %rax
	cmpq	$0, (%rdi)
	movq	%rdi, 24(%rsp)
	movq	%rsi, 32(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%r9, 64(%rsp)
	movq	%rax, (%rsp)
	movq	40(%rdi), %rax
	movq	%rax, 56(%rsp)
	je	.L2
#APP
# 58 "gconv_trans.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 56(%rsp)
.L2:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	movl	552(%rsi), %eax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L3
	movq	(%rsp), %rcx
	leaq	4(%rcx), %rax
	cmpq	%rax, %r12
	jb	.L65
	movq	560(%rsi), %rax
	movq	568(%rsi), %r14
	movq	584(%rsi), %r11
	movq	$0, 16(%rsp)
	movq	%rax, 72(%rsp)
	movq	576(%rsi), %rax
	movq	%rax, 88(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 40(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
.L20:
	movq	16(%rsp), %r15
	addq	8(%rsp), %r15
	xorl	%ebx, %ebx
	movq	72(%rsp), %rax
	shrq	%r15
	movl	(%rax,%r15,4), %eax
	movq	%rax, %r13
	movl	(%r14,%rax,4), %edx
	movq	(%rsp), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L66:
	addl	$1, %ebx
	leal	(%rbx,%r13), %edx
	movl	(%r14,%rdx,4), %edx
	testl	%edx, %edx
	je	.L7
	addq	$4, %rax
	cmpq	%rax, %r12
	jbe	.L9
.L8:
	cmpl	%edx, (%rax)
	je	.L66
	testl	%ebx, %ebx
	jne	.L67
	movq	(%rsp), %rax
.L10:
	cmpq	%rax, %r12
	jbe	.L18
	leal	(%rbx,%r13), %edx
	movl	(%rax), %eax
	cmpl	%eax, (%r14,%rdx,4)
	jb	.L18
	movq	%r15, 8(%rsp)
.L19:
	movq	8(%rsp), %rcx
	cmpq	%rcx, 16(%rsp)
	jb	.L20
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	movl	608(%rsi), %eax
	testl	%eax, %eax
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L24:
	movl	592(%rsi), %ebx
	testl	%ebx, %ebx
	jne	.L69
.L38:
	movl	$6, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movl	608(%rsi), %eax
	testl	%eax, %eax
	jne	.L70
	movl	592(%rsi), %ebx
	testl	%ebx, %ebx
	je	.L38
	movq	(%rsp), %rcx
	movq	600(%rsi), %rbp
	leaq	4(%rcx), %rax
	movq	%rbp, 128(%rsp)
	cmpq	%rax, %r12
	jnb	.L29
	xorl	%eax, %eax
	cmpq	%r12, %rcx
	setne	%al
	leal	4(%rax,%rax,2), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rsp), %rdi
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %r12
	jnb	.L71
	cmpq	%r12, (%rsp)
	movl	$4, %eax
	je	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$7, %eax
.L1:
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	cmpq	%r12, %rcx
	movl	$4, %eax
	jne	.L9
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	1(%r15), %rax
	movq	%rax, 16(%rsp)
	jmp	.L19
.L67:
	testl	%edx, %edx
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	88(%rsp), %rax
	movq	%r14, 96(%rsp)
	movq	%r15, 104(%rsp)
	movq	56(%rsp), %r14
	movl	%r13d, 116(%rsp)
	movq	%r12, 120(%rsp)
	movq	%r11, %r13
	movl	(%rax,%r15,4), %eax
	movq	64(%rsp), %r15
	movl	%ebx, 112(%rsp)
	movl	(%r11,%rax,4), %edx
	movl	%eax, %r12d
	.p2align 4,,10
	.p2align 3
.L17:
	testl	%edx, %edx
	leaq	0(%r13,%rax,4), %rsi
	je	.L42
	leaq	4(%r13,%rax,4), %rax
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rax, %rbx
	addq	$4, %rax
	movl	-4(%rax), %r8d
	addq	$1, %rbp
	testl	%r8d, %r8d
	jne	.L12
.L11:
	movq	(%r15), %rax
	movq	%r14, %rdi
	movq	%rsi, 128(%rsp)
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check
	pushq	$0
	xorl	%r9d, %r9d
	pushq	$0
	movq	%rbx, %rcx
	movq	56(%rsp), %r8
	movq	64(%rsp), %rdx
	movq	48(%rsp), %rsi
	movq	40(%rsp), %rdi
	call	*%r14
	cmpl	$6, %eax
	popq	%rsi
	popq	%rdi
	jne	.L72
	leal	1(%r12,%rbp), %eax
	movl	0(%r13,%rax,4), %edx
	movq	%rax, %r12
	testl	%edx, %edx
	jne	.L17
	movslq	112(%rsp), %rax
	movq	(%rsp), %rcx
	movq	%r13, %r11
	movq	96(%rsp), %r14
	movq	104(%rsp), %r15
	movl	116(%rsp), %r13d
	movq	120(%rsp), %r12
	movq	%rax, %rbx
	leaq	(%rcx,%rax,4), %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rsi, %rbx
	xorl	%ebp, %ebp
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L71:
	movq	616(%rsi), %rcx
	movl	(%rdi), %edi
	movl	%eax, %edx
.L34:
	testl	%edx, %edx
	jle	.L24
	movl	(%rcx), %eax
	cmpl	%eax, %edi
	jb	.L24
	subl	$1, %edx
	leaq	3(%rdx,%rdx,2), %rdx
	leaq	(%rcx,%rdx,4), %r8
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	%edi, 4(%rcx)
	jb	.L26
	movl	%edi, %ebx
	xorl	%edx, %edx
	subl	%eax, %ebx
	movl	%ebx, %eax
	divl	8(%rcx)
	testl	%edx, %edx
	je	.L73
.L26:
	addq	$12, %rcx
	cmpq	%r8, %rcx
	je	.L24
	movl	(%rcx), %eax
	cmpl	%edi, %eax
	jbe	.L25
	movl	592(%rsi), %ebx
	testl	%ebx, %ebx
	je	.L38
.L69:
	movq	600(%rsi), %rbp
	movq	%rbp, 128(%rsp)
.L29:
	movq	64(%rsp), %rax
	movq	56(%rsp), %r15
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check
	leaq	128(%rsp), %rdx
	pushq	$0
	pushq	$0
	leaq	0(%rbp,%rbx,4), %rcx
	movq	48(%rsp), %rsi
	movq	40(%rsp), %rdi
	leaq	152(%rsp), %r8
	xorl	%r9d, %r9d
	call	*%r15
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	je	.L1
	cmpl	$4, %eax
	jne	.L33
	movq	208(%rsp), %rax
	addq	$1, (%rax)
	movq	80(%rsp), %rax
	addq	$4, (%rax)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	movq	136(%rsp), %rdx
	movq	64(%rsp), %rcx
	movq	%rdx, (%rcx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$4, %eax
	movl	112(%rsp), %ebx
	je	.L74
	cmpl	$5, %eax
	jne	.L33
	jmp	.L1
.L68:
	movl	%eax, %edx
	movq	80(%rsp), %rax
	movq	616(%rsi), %rcx
	movq	(%rax), %rax
	movq	%rax, (%rsp)
	movl	(%rax), %edi
	jmp	.L34
.L74:
	movq	80(%rsp), %rcx
	movslq	%ebx, %rax
	salq	$2, %rax
	addq	%rax, (%rcx)
	movq	208(%rsp), %rax
	addq	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L33
.L73:
	movq	(%rsp), %rax
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movq	%rax, (%rcx)
	movq	208(%rsp), %rax
	addq	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L1
	.size	__gconv_transliterate, .-__gconv_transliterate
	.hidden	_dl_mcount_wrapper_check
	.hidden	_nl_current_LC_CTYPE
