	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gconv_alias_compare
	.hidden	__gconv_alias_compare
	.type	__gconv_alias_compare, @function
__gconv_alias_compare:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	__GI_strcmp
	.size	__gconv_alias_compare, .-__gconv_alias_compare
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_derivation, @function
free_derivation:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$0, 24(%rdi)
	movq	16(%rdi), %rdx
	je	.L4
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	(%rdx,%rbx), %rax
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jle	.L5
	cmpq	$0, (%rax)
	je	.L5
	movq	64(%rax), %rbp
#APP
# 185 "gconv_db.c" 1
	ror $2*8+1, %rbp
xor %fs:48, %rbp
# 0 "" 2
#NO_APP
	testq	%rbp, %rbp
	je	.L5
	movq	%rbp, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	16(%r13), %rdi
	addq	%rbx, %rdi
	call	*%rbp
	movq	16(%r13), %rdx
.L5:
	addq	$1, %r12
	addq	$104, %rbx
	cmpq	%r12, 24(%r13)
	ja	.L6
.L4:
	testq	%rdx, %rdx
	je	.L7
	movq	24(%rdx), %rdi
	call	free@PLT
	movq	24(%r13), %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	16(%r13), %rdx
	leaq	(%rdx,%rax,8), %rax
	movq	-72(%rax), %rdi
	call	free@PLT
	movq	16(%r13), %rdi
	call	free@PLT
.L7:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	free@PLT
	.size	free_derivation, .-free_derivation
	.text
	.p2align 4,,15
	.type	derivation_compare, @function
derivation_compare:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L16
	movq	8(%rbp), %rsi
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__GI_strcmp
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	derivation_compare, .-derivation_compare
	.p2align 4,,15
	.globl	__gconv_get_modules_db
	.type	__gconv_get_modules_db, @function
__gconv_get_modules_db:
	movq	__gconv_modules_db(%rip), %rax
	ret
	.size	__gconv_get_modules_db, .-__gconv_get_modules_db
	.p2align 4,,15
	.globl	__gconv_get_alias_db
	.type	__gconv_get_alias_db, @function
__gconv_get_alias_db:
	movq	__gconv_alias_db(%rip), %rax
	ret
	.size	__gconv_get_alias_db, .-__gconv_get_alias_db
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gconv_db.c"
.LC1:
	.string	"step->__end_fct == NULL"
	.text
	.p2align 4,,15
	.globl	__gconv_release_step
	.hidden	__gconv_release_step
	.type	__gconv_release_step, @function
__gconv_release_step:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L22
	subl	$1, 16(%rbx)
	jne	.L21
	movq	64(%rbx), %rbp
#APP
# 213 "gconv_db.c" 1
	ror $2*8+1, %rbp
xor %fs:48, %rbp
# 0 "" 2
#NO_APP
	testq	%rbp, %rbp
	je	.L24
	movq	%rbp, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	%rbx, %rdi
	call	*%rbp
	movq	(%rbx), %rdi
.L24:
	call	__gconv_release_shlib
	movq	$0, (%rbx)
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$0, 64(%rbx)
	je	.L21
	leaq	__PRETTY_FUNCTION__.9300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$226, %edx
	call	__GI___assert_fail
	.size	__gconv_release_step, .-__gconv_release_step
	.section	.rodata.str1.1
.LC2:
	.string	"-"
	.text
	.p2align 4,,15
	.type	find_derivation, @function
find_derivation:
	pushq	%rbp
	movq	%rsi, %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$136, %rsp
	testq	%rsi, %rsi
	movq	%rdi, -136(%rbp)
	cmove	%rdi, %rax
	testq	%rcx, %rcx
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -104(%rbp)
	je	.L191
	movq	-144(%rbp), %rax
	leaq	-80(%rbp), %rdi
	leaq	derivation_compare(%rip), %rdx
	leaq	known_derivations(%rip), %rsi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -80(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -72(%rbp)
	call	__GI___tfind
	testq	%rax, %rax
	je	.L192
.L97:
	movq	(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	24(%rax), %r13
	movq	16(%rax), %rcx
	movq	-120(%rbp), %rax
	movq	%rcx, (%rdx)
	movq	%r13, %r12
	movq	%rcx, -88(%rbp)
	movq	%r13, (%rax)
	leaq	0(%r13,%r13,2), %rax
	leaq	0(%r13,%rax,4), %r14
	salq	$3, %r14
	testq	%r12, %r12
	leaq	-104(%rcx,%r14), %rbx
	je	.L193
.L44:
	movl	16(%rbx), %eax
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, 16(%rbx)
	jne	.L37
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	__gconv_find_shlib
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L194
	movq	24(%rax), %rdx
	movq	32(%rax), %r15
	movq	40(%rax), %rax
	movq	$0, 48(%rbx)
	movq	%r15, 56(%rbx)
	movq	%rdx, 40(%rbx)
	movq	%rax, 64(%rbx)
	xorl	%eax, %eax
#APP
# 409 "gconv_db.c" 1
	ror $2*8+1, %r15
xor %fs:48, %r15
# 0 "" 2
#NO_APP
	testq	%r15, %r15
	je	.L43
	movq	%r15, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	%rbx, %rdi
	call	*%r15
	movq	48(%rbx), %rax
.L43:
#APP
# 415 "gconv_db.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 48(%rbx)
.L37:
	subq	$1, %r12
	subq	$104, %rbx
	testq	%r12, %r12
	jne	.L44
.L193:
	xorl	%r8d, %r8d
	jmp	.L32
.L192:
	movl	$64, %r12d
	subq	%r12, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %r15
	movq	%rax, -160(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, (%r15)
	call	__GI_strlen
	subq	%r12, %rsp
	movq	%rax, 8(%r15)
	movq	$0, 16(%r15)
	leaq	15(%rsp), %r12
	movq	$0, 24(%r15)
	movq	$0, 32(%r15)
	movq	$0, 40(%r15)
	movq	%rbx, %rdi
	andq	$-16, %r12
	movq	%rbx, (%r12)
	call	__GI_strlen
	movq	%rax, 8(%r12)
	leaq	40(%r12), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	%r12, 40(%r15)
	movq	%rax, -168(%rbp)
.L95:
	movq	-160(%rbp), %r15
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movl	$2147483647, -124(%rbp)
	movl	$2147483647, -96(%rbp)
	movq	%r14, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	-96(%rbp), %r13d
	jg	.L45
	jne	.L46
	movl	-124(%rbp), %eax
	cmpl	%eax, 16(%r15)
	jge	.L45
.L46:
	movq	__gconv_modules_db(%rip), %rbx
	testq	%rbx, %rbx
	je	.L45
	movq	(%r15), %r12
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L45
.L71:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L68
	jns	.L69
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L71
	.p2align 4,,10
	.p2align 3
.L45:
	movq	40(%r15), %r15
	testq	%r15, %r15
	je	.L72
	movl	20(%r15), %r13d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L195:
	subq	$64, %rsp
	movq	%r12, %rdi
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, %r14
	movq	%r12, (%rdx)
	call	__GI_strlen
	movq	%rax, 8(%r14)
	movl	-88(%rbp), %eax
	movl	%r13d, 20(%r14)
	movq	%rbx, 24(%r14)
	movq	%r15, 32(%r14)
	movq	$0, 40(%r14)
	movl	%eax, 16(%r14)
	movq	-168(%rbp), %rax
	movq	%r14, (%rax)
	leaq	40(%r14), %rax
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L55:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L45
	movl	20(%r15), %r13d
.L68:
	movq	8(%rbx), %r12
	leaq	.LC2(%rip), %rdi
	movl	$2, %ecx
	movl	16(%r15), %r14d
	movq	%r12, %rsi
	repz cmpsb
	cmove	-104(%rbp), %r12
	addl	20(%rbx), %r14d
	movq	-136(%rbp), %rsi
	addl	16(%rbx), %r13d
	movq	%r12, %rdi
	movl	%r14d, -88(%rbp)
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L49
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L50
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L49
.L50:
	cmpl	%r13d, -96(%rbp)
	jg	.L110
	jne	.L55
	movl	-88(%rbp), %edx
	cmpl	%edx, -124(%rbp)
	jle	.L55
.L110:
	movq	-160(%rbp), %r14
	movq	%r12, %rdi
	movq	(%r14), %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L58
	.p2align 4,,10
	.p2align 3
.L196:
	movq	40(%r14), %r14
	testq	%r14, %r14
	je	.L195
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L196
.L58:
	cmpl	%r13d, 20(%r14)
	jg	.L101
	jne	.L55
	movl	-88(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jle	.L55
	.p2align 4,,10
	.p2align 3
.L101:
	movq	-160(%rbp), %rdi
	movq	%rbx, 24(%r14)
	movq	%r15, 32(%r14)
	.p2align 4,,10
	.p2align 3
.L64:
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L61
	movq	32(%rdi), %rdx
	movl	16(%rax), %esi
	movl	20(%rax), %ecx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	je	.L62
	.p2align 4,,10
	.p2align 3
.L63:
	movq	32(%rdx), %rdx
	addl	16(%rax), %esi
	addl	20(%rax), %ecx
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L63
.L62:
	movl	%esi, 20(%rdi)
	movl	%ecx, 16(%rdi)
.L61:
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L64
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L55
	movl	-124(%rbp), %edi
	movl	-96(%rbp), %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L198:
	jne	.L121
	cmpl	%edi, %ecx
	jge	.L121
.L65:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L197
.L112:
	movl	%ecx, %edi
.L67:
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movl	20(%rcx), %edx
	addl	16(%rsi), %edx
	movl	16(%rcx), %ecx
	addl	20(%rsi), %ecx
	cmpl	%r8d, %edx
	movl	%edx, 20(%rax)
	movl	%ecx, 16(%rax)
	jge	.L198
	movq	40(%rax), %rax
	movl	%edx, %r8d
	testq	%rax, %rax
	jne	.L112
.L197:
	movl	%r8d, -96(%rbp)
	movl	%ecx, -124(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L51
	movq	%rax, %r14
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L199:
	movq	40(%r14), %r14
	testq	%r14, %r14
	je	.L51
.L53:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L199
	cmpl	%r13d, 20(%r14)
	jg	.L99
	je	.L200
.L54:
	cmpl	%r13d, -96(%rbp)
	jg	.L108
.L201:
	jne	.L55
	movl	-124(%rbp), %ecx
	movl	-88(%rbp), %eax
	cmpl	%eax, %ecx
	cmovle	%ecx, %eax
	movl	%eax, -124(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	subq	$64, %rsp
	movq	%r12, %rdi
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, %r14
	movq	%r12, (%rdx)
	call	__GI_strlen
	movq	%rax, 8(%r14)
	movl	-88(%rbp), %eax
	cmpl	%r13d, -96(%rbp)
	movl	%r13d, 20(%r14)
	movq	%rbx, 24(%r14)
	movq	%r15, 32(%r14)
	movl	%eax, 16(%r14)
	movq	-112(%rbp), %rax
	movq	%r14, -112(%rbp)
	movq	%rax, 40(%r14)
	jle	.L201
.L108:
	movl	-88(%rbp), %eax
	movl	%r13d, -96(%rbp)
	movl	%eax, -124(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L200:
	movl	-88(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jle	.L54
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-88(%rbp), %eax
	movq	%rbx, 24(%r14)
	movq	%r15, 32(%r14)
	movl	%r13d, 20(%r14)
	movl	%eax, 16(%r14)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-112(%rbp), %r14
	testq	%r14, %r14
	je	.L74
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.L75
	movl	20(%r14), %ecx
	cmpl	%ecx, 20(%rax)
	jl	.L113
	jne	.L75
	movl	16(%r14), %ecx
	cmpl	%ecx, 16(%rax)
	cmovl	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L75:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L76
	xorl	%r12d, %r12d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r13, %r12
.L77:
	movq	32(%rax), %rax
	leaq	1(%r12), %r13
	testq	%rax, %rax
	jne	.L115
	leaq	0(%r13,%r13,2), %rax
	leaq	0(%r13,%rax,4), %rdi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L78
	movq	-120(%rbp), %rax
	testq	%r13, %r13
	movq	%r13, (%rax)
	je	.L79
	leaq	(%r12,%r12,2), %rax
	movq	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	leaq	(%r12,%rax,4), %rax
	leaq	(%rbx,%rax,8), %r15
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r14, %rsi
	call	__gconv_get_builtin_trans
.L91:
	subq	$104, %r14
	testq	%r12, %r12
	movq	32(%r15), %r15
	leaq	-1(%r12), %rax
	movq	%r12, %r13
	je	.L202
	movq	%rax, %r12
.L92:
	testq	%r12, %r12
	je	.L203
	movq	32(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%r14)
.L82:
	movq	-120(%rbp), %rax
	cmpq	%r13, (%rax)
	je	.L204
	leaq	0(%r13,%r13,2), %rax
	leaq	0(%r13,%rax,4), %rax
	movq	24(%rbx,%rax,8), %rax
	movq	%rax, 32(%r14)
.L84:
	movq	24(%r15), %rax
	movl	$1, 16(%r14)
	movq	$0, 96(%r14)
	movq	24(%rax), %rdi
	cmpb	$47, (%rdi)
	jne	.L85
	call	__gconv_find_shlib
	testq	%rax, %rax
	je	.L189
	movq	(%rax), %rsi
	movq	%rax, (%r14)
	movq	%rsi, 8(%r14)
	movq	24(%rax), %rsi
	movq	%rsi, 40(%r14)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	$0, 48(%r14)
	movq	%rsi, 56(%r14)
#APP
# 308 "gconv_db.c" 1
	ror $2*8+1, %rsi
xor %fs:48, %rsi
# 0 "" 2
#NO_APP
	testq	%rsi, %rsi
	movq	%rax, 64(%r14)
	je	.L117
	movq	%rsi, %rdi
	movq	%rsi, -96(%rbp)
	call	__GI__dl_mcount_wrapper_check
	movq	%r14, %rdi
	movq	-96(%rbp), %rsi
	call	*%rsi
	testl	%eax, %eax
	jne	.L88
	movq	48(%r14), %rax
.L87:
#APP
# 329 "gconv_db.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 48(%r14)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L203:
	movq	-144(%rbp), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, 24(%rbx)
	jne	.L82
	movl	$1, %r13d
.L81:
	movq	-120(%rbp), %rax
	cmpq	%r13, (%rax)
	jbe	.L205
	xorl	%r8d, %r8d
.L89:
	leaq	0(%r13,%r13,2), %rax
	movq	-120(%rbp), %r12
	movl	%r8d, %r14d
	leaq	0(%r13,%rax,4), %rax
	leaq	(%rbx,%rax,8), %r15
.L93:
	movq	%r15, %rdi
	addq	$1, %r13
	addq	$104, %r15
	call	__gconv_release_step
	cmpq	(%r12), %r13
	jb	.L93
	movq	%rbx, %rdi
	movl	%r14d, -96(%rbp)
	xorl	%ebx, %ebx
	call	free@PLT
	movq	-88(%rbp), %rdi
	xorl	%r13d, %r13d
	call	free@PLT
	movq	-112(%rbp), %rdi
	call	free@PLT
	movq	-120(%rbp), %rax
	movl	-96(%rbp), %r8d
	movq	$0, (%rax)
	movq	-176(%rbp), %rax
	testl	%r8d, %r8d
	movq	$0, (%rax)
	jne	.L94
	movl	$1, %r8d
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%r15), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, -112(%rbp)
	movq	%rax, 32(%r14)
	jne	.L84
.L189:
	leaq	1(%r12), %r13
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L117:
	xorl	%eax, %eax
	jmp	.L87
.L202:
	movq	-120(%rbp), %rax
	movq	(%rax), %r13
.L79:
	movq	-176(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rbx, (%rax)
.L94:
	movq	-144(%rbp), %rdi
	movl	%r8d, -88(%rbp)
	call	__GI_strlen
	movq	-104(%rbp), %rdi
	leaq	1(%rax), %r15
	call	__GI_strlen
	leaq	1(%rax), %r12
	leaq	32(%r15,%r12), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movl	-88(%rbp), %r8d
	je	.L32
	leaq	32(%rax), %rdi
	movq	-144(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rdi, (%rax)
	call	__GI_mempcpy@PLT
	movq	-104(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	leaq	derivation_compare(%rip), %rdx
	leaq	known_derivations(%rip), %rsi
	movq	%rax, 8(%r14)
	movq	%rbx, 16(%r14)
	movq	%r13, 24(%r14)
	movq	%r14, %rdi
	call	__GI___tsearch
	testq	%rax, %rax
	movl	-88(%rbp), %r8d
	je	.L206
.L32:
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	-104(%rbp), %rax
	movq	%rdx, -80(%rbp)
	leaq	-80(%rbp), %rdi
	leaq	derivation_compare(%rip), %rdx
	leaq	known_derivations(%rip), %rsi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -72(%rbp)
	call	__GI___tfind
	testq	%rax, %rax
	jne	.L97
	subq	$64, %rsp
	movq	%rbx, %rdi
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %r15
	movq	%rbx, (%rax)
	movq	%rax, -160(%rbp)
	call	__GI_strlen
	movq	%rax, 8(%r15)
	leaq	40(%r15), %rax
	movq	$0, 16(%r15)
	movq	$0, 24(%r15)
	movq	$0, 32(%r15)
	movq	$0, 40(%r15)
	movq	%rax, -168(%rbp)
	movq	%rbx, -144(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%edi, %ecx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-176(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movl	$1, %r8d
	movq	$0, (%rax)
	movq	-120(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L94
.L113:
	movq	%rax, %r14
	jmp	.L75
.L88:
	movl	%eax, %r8d
	xorl	%eax, %eax
	subq	$1, %r13
#APP
# 321 "gconv_db.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 64(%r14)
	movq	-120(%rbp), %rax
	cmpq	%r13, (%rax)
	ja	.L89
	movq	%rbx, %rdi
	movl	%r8d, -96(%rbp)
	xorl	%ebx, %ebx
	call	free@PLT
	movq	-88(%rbp), %rdi
	xorl	%r13d, %r13d
	call	free@PLT
	movq	-112(%rbp), %rdi
	call	free@PLT
	movq	-120(%rbp), %rax
	movl	-96(%rbp), %r8d
	movq	$0, (%rax)
	movq	-176(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L94
.L76:
	xorl	%edi, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L207
.L78:
	movq	-120(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movl	$3, %r8d
	movq	$0, (%rax)
	movq	-176(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L94
.L206:
	movq	%r14, %rdi
	call	free@PLT
	movl	-88(%rbp), %r8d
	jmp	.L32
.L194:
	subl	$1, 16(%rbx)
	cmpq	%r12, %r13
	jbe	.L42
	leaq	(%r12,%r12,2), %rax
	movq	-88(%rbp), %rcx
	leaq	(%r12,%rax,4), %rax
	leaq	(%rcx,%r14), %r13
	leaq	(%rcx,%rax,8), %rbx
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rdi
	addq	$104, %rbx
	call	__gconv_release_step
	cmpq	%rbx, %r13
	jne	.L41
.L42:
	movl	$1, %r8d
	jmp	.L32
.L207:
	movq	-120(%rbp), %rax
	xorl	%r13d, %r13d
	movq	$0, (%rax)
	jmp	.L79
.L205:
	movq	%rbx, %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	call	free@PLT
	movq	-88(%rbp), %rdi
	call	free@PLT
	movq	-112(%rbp), %rdi
	call	free@PLT
	movq	-120(%rbp), %rax
	movl	$1, %r8d
	movq	$0, (%rax)
	movq	-176(%rbp), %rax
	movq	$0, (%rax)
	jmp	.L94
	.size	find_derivation, .-find_derivation
	.p2align 4,,15
	.globl	__gconv_compare_alias
	.hidden	__gconv_compare_alias
	.type	__gconv_compare_alias, @function
__gconv_compare_alias:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	call	__gconv_load_conf
	leaq	12(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__gconv_compare_alias_cache
	testl	%eax, %eax
	jne	.L209
	movl	12(%rsp), %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	16(%rsp), %r12
	leaq	__gconv_alias_compare(%rip), %rdx
	leaq	__gconv_alias_db(%rip), %rsi
	movq	%rbp, 16(%rsp)
	movq	%r12, %rdi
	call	__GI___tfind
	testq	%rax, %rax
	je	.L212
	movq	(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	cmovne	%rax, %rbp
.L212:
	leaq	__gconv_alias_compare(%rip), %rdx
	leaq	__gconv_alias_db(%rip), %rsi
	movq	%r12, %rdi
	movq	%rbx, 16(%rsp)
	call	__GI___tfind
	testq	%rax, %rax
	je	.L214
	movq	(%rax), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	cmovne	%rax, %rbx
.L214:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_strcmp
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__gconv_compare_alias, .-__gconv_compare_alias
	.p2align 4,,15
	.globl	__gconv_find_transform
	.hidden	__gconv_find_transform
	.type	__gconv_find_transform, @function
__gconv_find_transform:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movl	%r8d, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	movq	%rdx, %r12
	subq	$40, %rsp
	call	__gconv_load_conf
#APP
# 731 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L221
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
.L222:
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__gconv_lookup_cache
	cmpl	$2, %eax
	movl	%eax, %r8d
	je	.L223
#APP
# 737 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L224
	subl	$1, __gconv_lock(%rip)
.L220:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	cmpq	$0, __gconv_modules_db(%rip)
	je	.L266
	leaq	16(%rsp), %r15
	leaq	__gconv_alias_compare(%rip), %rdx
	leaq	__gconv_alias_db(%rip), %rsi
	movq	%rbx, 16(%rsp)
	movq	%r15, %rdi
	call	__GI___tfind
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L228
	movq	(%rax), %rax
	movq	8(%rax), %rcx
.L228:
	leaq	__gconv_alias_compare(%rip), %rdx
	leaq	__gconv_alias_db(%rip), %rsi
	movq	%r15, %rdi
	movq	%rcx, 8(%rsp)
	movq	%rbp, 16(%rsp)
	xorl	%r15d, %r15d
	call	__GI___tfind
	testq	%rax, %rax
	movq	8(%rsp), %rcx
	je	.L229
	movq	(%rax), %rax
	movq	8(%rax), %r15
.L229:
	andl	$1, %r13d
	jne	.L267
.L230:
	movq	%r12, %r8
	movq	%r14, %r9
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	find_derivation
	movl	%eax, %r8d
#APP
# 771 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L234
	subl	$1, __gconv_lock(%rip)
.L235:
	testl	%r8d, %r8d
	jne	.L220
	xorl	%r8d, %r8d
	cmpq	$0, (%r12)
	sete	%r8b
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L266:
#APP
# 744 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L227
	subl	$1, __gconv_lock(%rip)
	movl	$1, %r8d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __gconv_lock(%rip)
	je	.L222
	leaq	__gconv_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%eax, %eax
#APP
# 737 "gconv_db.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L220
.L265:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__gconv_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 763 "gconv_db.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L231
	testq	%r15, %r15
	movq	8(%rsp), %rcx
	je	.L232
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	je	.L231
.L232:
	testq	%rcx, %rcx
	je	.L230
	movq	%rcx, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L231
	testq	%r15, %r15
	movq	8(%rsp), %rcx
	je	.L230
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	jne	.L230
	.p2align 4,,10
	.p2align 3
.L231:
#APP
# 763 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L233
	subl	$1, __gconv_lock(%rip)
	movl	$-1, %r8d
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L234:
	xorl	%eax, %eax
#APP
# 771 "gconv_db.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L235
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__gconv_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 771 "gconv_db.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L227:
	xorl	%eax, %eax
#APP
# 744 "gconv_db.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$1, %r8d
	jle	.L220
	jmp	.L265
.L233:
	xorl	%eax, %eax
#APP
# 763 "gconv_db.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$-1, %r8d
	jle	.L220
	jmp	.L265
	.size	__gconv_find_transform, .-__gconv_find_transform
	.p2align 4,,15
	.globl	__gconv_close_transform
	.hidden	__gconv_close_transform
	.type	__gconv_close_transform, @function
__gconv_close_transform:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$8, %rsp
#APP
# 790 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L269
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
.L270:
	testq	%r12, %r12
	je	.L271
	leaq	(%r12,%r12,2), %rax
	leaq	-104(%r13), %rbp
	leaq	(%r12,%rax,4), %rax
	leaq	-104(%r13,%rax,8), %rbx
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%rbx, %rdi
	subq	$104, %rbx
	call	__gconv_release_step
	cmpq	%rbp, %rbx
	jne	.L272
.L271:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__gconv_release_cache
#APP
# 804 "gconv_db.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L273
	subl	$1, __gconv_lock(%rip)
.L274:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __gconv_lock(%rip)
	je	.L270
	leaq	__gconv_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%eax, %eax
#APP
# 804 "gconv_db.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L274
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__gconv_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 804 "gconv_db.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L274
	.size	__gconv_close_transform, .-__gconv_close_transform
	.section	__libc_freeres_fn
	.p2align 4,,15
	.type	free_modules_db, @function
free_modules_db:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L293
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L284
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L283:
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	je	.L295
.L284:
	movq	24(%rbx), %rax
	movq	40(%rbx), %rbp
	cmpb	$47, (%rax)
	jne	.L283
	movq	%rbx, %rdi
	movq	%rbp, %rbx
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L284
.L295:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	call	free_modules_db
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L284
.L294:
	call	free_modules_db
	jmp	.L284
	.size	free_modules_db, .-free_modules_db
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	subq	$8, %rsp
	call	_nl_locale_subfreeres
	call	_nl_finddomain_subfreeres
	movq	__gconv_alias_db(%rip), %rdi
	testq	%rdi, %rdi
	je	.L297
	movq	free@GOTPCREL(%rip), %rsi
	call	__GI___tdestroy
.L297:
	movq	__gconv_modules_db(%rip), %rdi
	testq	%rdi, %rdi
	je	.L298
	call	free_modules_db
.L298:
	movq	known_derivations(%rip), %rdi
	testq	%rdi, %rdi
	je	.L296
	leaq	free_derivation(%rip), %rsi
	addq	$8, %rsp
	jmp	__GI___tdestroy
	.p2align 4,,10
	.p2align 3
.L296:
	addq	$8, %rsp
	ret
	.size	free_mem, .-free_mem
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9300, @object
	.size	__PRETTY_FUNCTION__.9300, 21
__PRETTY_FUNCTION__.9300:
	.string	"__gconv_release_step"
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	known_derivations
	.comm	known_derivations,8,8
	.hidden	__gconv_lock
	.comm	__gconv_lock,4,4
	.hidden	__gconv_modules_db
	.comm	__gconv_modules_db,8,8
	.hidden	__gconv_alias_db
	.comm	__gconv_alias_db,8,8
	.hidden	_nl_finddomain_subfreeres
	.hidden	_nl_locale_subfreeres
	.hidden	__gconv_release_cache
	.hidden	__lll_lock_wait_private
	.hidden	__gconv_lookup_cache
	.hidden	__gconv_compare_alias_cache
	.hidden	__gconv_load_conf
	.hidden	__gconv_get_builtin_trans
	.hidden	__gconv_find_shlib
	.hidden	__gconv_release_shlib
