	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"=INTERNAL->ucs4"
.LC1:
	.string	"gconv_builtin.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cnt < sizeof (map) / sizeof (map[0])"
	.text
	.p2align 4,,15
	.globl	__gconv_get_builtin_trans
	.hidden	__gconv_get_builtin_trans
	.type	__gconv_get_builtin_trans, @function
__gconv_get_builtin_trans:
	pushq	%r13
	pushq	%r12
	leaq	map(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %r12
	leaq	.LC0(%rip), %rsi
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rbx, %rax
	salq	$5, %rax
	movq	0(%r13,%rax), %rsi
.L4:
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L2
	addq	$1, %rbx
	cmpq	$12, %rbx
	jne	.L7
	leaq	__PRETTY_FUNCTION__.8660(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$70, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	map(%rip), %rax
	salq	$5, %rbx
	movq	$0, 56(%rbp)
	movq	$0, 64(%rbp)
	movq	$0, 0(%rbp)
	addq	%rax, %rbx
	movq	$0, 8(%rbp)
	movl	$0, 88(%rbp)
	movq	8(%rbx), %rax
	movq	%rax, 40(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 48(%rbp)
	movsbl	24(%rbx), %eax
	movl	%eax, 72(%rbp)
	movsbl	25(%rbx), %eax
	movl	%eax, 76(%rbp)
	movsbl	26(%rbx), %eax
	movl	%eax, 80(%rbp)
	movsbl	27(%rbx), %eax
	movl	%eax, 84(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__gconv_get_builtin_trans, .-__gconv_get_builtin_trans
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.8660, @object
	.size	__PRETTY_FUNCTION__.8660, 26
__PRETTY_FUNCTION__.8660:
	.string	"__gconv_get_builtin_trans"
	.section	.rodata.str1.1
.LC3:
	.string	"=ucs4->INTERNAL"
.LC4:
	.string	"=INTERNAL->ucs4le"
.LC5:
	.string	"=ucs4le->INTERNAL"
.LC6:
	.string	"=INTERNAL->utf8"
.LC7:
	.string	"=utf8->INTERNAL"
.LC8:
	.string	"=ucs2->INTERNAL"
.LC9:
	.string	"=INTERNAL->ucs2"
.LC10:
	.string	"=ascii->INTERNAL"
.LC11:
	.string	"=INTERNAL->ascii"
.LC12:
	.string	"=ucs2reverse->INTERNAL"
.LC13:
	.string	"=INTERNAL->ucs2reverse"
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	map, @object
	.size	map, 384
map:
	.quad	.LC0
	.quad	__gconv_transform_internal_ucs4
	.quad	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC3
	.quad	__gconv_transform_ucs4_internal
	.quad	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC4
	.quad	__gconv_transform_internal_ucs4le
	.quad	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC5
	.quad	__gconv_transform_ucs4le_internal
	.quad	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC6
	.quad	__gconv_transform_internal_utf8
	.quad	0
	.byte	4
	.byte	4
	.byte	1
	.byte	6
	.zero	4
	.quad	.LC7
	.quad	__gconv_transform_utf8_internal
	.quad	__gconv_btwoc_ascii
	.byte	1
	.byte	6
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC8
	.quad	__gconv_transform_ucs2_internal
	.quad	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC9
	.quad	__gconv_transform_internal_ucs2
	.quad	0
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.zero	4
	.quad	.LC10
	.quad	__gconv_transform_ascii_internal
	.quad	__gconv_btwoc_ascii
	.byte	1
	.byte	1
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC11
	.quad	__gconv_transform_internal_ascii
	.quad	0
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.zero	4
	.quad	.LC12
	.quad	__gconv_transform_ucs2reverse_internal
	.quad	0
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.zero	4
	.quad	.LC13
	.quad	__gconv_transform_internal_ucs2reverse
	.quad	0
	.byte	4
	.byte	4
	.byte	2
	.byte	2
	.zero	4
	.hidden	__assert_fail
	.hidden	strcmp
