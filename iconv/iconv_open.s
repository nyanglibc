	.text
	.p2align 4,,15
	.globl	iconv_open
	.type	iconv_open, @function
iconv_open:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rdx
	subq	$56, %rsp
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	call	__gconv_create_spec
	testq	%rax, %rax
	movq	$-1, %rdx
	je	.L1
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	__gconv_open
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__gconv_destroy_spec
	testl	%ebp, %ebp
	jne	.L3
	movq	8(%rsp), %rdx
.L1:
	addq	$56, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subl	$1, %ebp
	cmpl	$1, %ebp
	jbe	.L10
.L5:
	movq	$-1, %rdx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L5
	.size	iconv_open, .-iconv_open
	.hidden	__gconv_destroy_spec
	.hidden	__gconv_open
	.hidden	__gconv_create_spec
