	.text
	.p2align 4,,15
	.globl	iconv_close
	.type	iconv_close, @function
iconv_close:
	cmpq	$-1, %rdi
	je	.L8
	subq	$8, %rsp
	call	__gconv_close
	xorl	%edi, %edi
	testl	%eax, %eax
	setne	%dil
	addq	$8, %rsp
	movl	%edi, %eax
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	%edi, %eax
	ret
	.size	iconv_close, .-iconv_close
	.hidden	__gconv_close
