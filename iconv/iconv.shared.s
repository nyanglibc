	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"iconv.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"!\"Nothing like this should happen\""
#NO_APP
	.text
	.p2align 4,,15
	.globl	iconv
	.type	iconv, @function
iconv:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%r8, %r14
	subq	$24, %rsp
	testq	%rcx, %rcx
	je	.L2
	testq	%rsi, %rsi
	movq	(%rcx), %rbp
	je	.L17
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L17
	movq	%rbp, %r8
.L18:
	movq	0(%r13), %rdx
	addq	(%r14), %r8
	leaq	8(%rsp), %r9
	movq	%r12, %rcx
	movq	%r15, %rsi
	addq	%rbx, %rdx
	call	__gconv
	subq	(%r15), %rbx
	addq	%rbx, 0(%r13)
	testq	%rbp, %rbp
	je	.L5
.L6:
	subq	(%r12), %rbp
	addq	%rbp, (%r14)
.L5:
	cmpl	$8, %eax
	ja	.L7
	leaq	.L9(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L8-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L8-.L9
	.long	.L10-.L9
	.long	.L11-.L9
	.long	.L12-.L9
	.long	.L13-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	addq	$24, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$24, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	addq	$24, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %fs:(%rax)
	addq	$24, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rsi, %rsi
	je	.L15
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L15
	movq	0, %r8
	xorl	%ebp, %ebp
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%rbp, %rbp
	je	.L15
	movq	(%r14), %r8
	leaq	8(%rsp), %r9
	movq	%r12, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	%rbp, %r8
	call	__gconv
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	8(%rsp), %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__gconv
	jmp	.L5
.L7:
	leaq	__PRETTY_FUNCTION__.8661(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$91, %edx
	call	__GI___assert_fail
	.size	iconv, .-iconv
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.8661, @object
	.size	__PRETTY_FUNCTION__.8661, 6
__PRETTY_FUNCTION__.8661:
	.string	"iconv"
	.hidden	__gconv
