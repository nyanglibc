	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gconv_close
	.hidden	__gconv_close
	.type	__gconv_close, @function
__gconv_close:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	16(%rdi), %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r13
.L5:
	testb	$1, 16(%rbx)
	je	.L2
.L4:
	movq	%rbp, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__gconv_close_transform
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rbx), %rdi
	addq	$48, %rbx
	testq	%rdi, %rdi
	je	.L5
	call	free@PLT
	testb	$1, -32(%rbx)
	je	.L5
	jmp	.L4
	.size	__gconv_close, .-__gconv_close
	.hidden	__gconv_close_transform
