	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gconv_btwoc_ascii
	.type	__gconv_btwoc_ascii, @function
__gconv_btwoc_ascii:
	movzbl	%sil, %eax
	testb	%sil, %sil
	movl	$-1, %edx
	cmovs	%edx, %eax
	ret
	.size	__gconv_btwoc_ascii, .-__gconv_btwoc_ascii
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../iconv/skeleton.c"
.LC1:
	.string	"outbufstart == NULL"
.LC2:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC4:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	__gconv_transform_internal_ucs4
	.type	__gconv_transform_internal_ucs4, @function
__gconv_transform_internal_ucs4:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r10
	movq	%rcx, %r11
	movq	%r9, %r15
	subq	$104, %rsp
	testb	$1, 16(%rsi)
	movq	%rax, 56(%rsp)
	leaq	48(%rsi), %rax
	movq	%r8, 32(%rsp)
	movl	160(%rsp), %ebx
	movq	$0, 16(%rsp)
	movq	%rax, 64(%rsp)
	jne	.L6
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 16(%rsp)
	je	.L6
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L6:
	testl	%ebx, %ebx
	jne	.L76
	movq	32(%rsp), %rdx
	movl	168(%rsp), %esi
	testq	%rdx, %rdx
	movq	%rdx, %rax
	cmove	%r13, %rax
	testl	%esi, %esi
	movq	(%rax), %rbp
	movq	8(%r13), %rax
	movq	%rax, 8(%rsp)
	jne	.L77
.L12:
	leaq	88(%rsp), %rax
	movq	%r11, (%rsp)
	movq	%rax, 72(%rsp)
.L37:
	testq	%r15, %r15
	movq	(%r10), %r12
	movq	$0, 24(%rsp)
	je	.L17
	movq	(%r15), %rax
	movq	%rax, 24(%rsp)
.L17:
	movq	(%rsp), %r11
	movq	8(%rsp), %rbx
	subq	%r12, %r11
	subq	%rbp, %rbx
	cmpq	%r11, %rbx
	movq	%rbx, %rax
	cmovg	%r11, %rax
	leaq	3(%rax), %rbx
	testq	%rax, %rax
	cmovns	%rax, %rbx
	sarq	$2, %rbx
	testq	%rbx, %rbx
	je	.L44
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L19:
	movl	(%r12,%rax,4), %edx
	bswap	%edx
	movl	%edx, 0(%rbp,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L19
	salq	$2, %rbx
	leaq	(%r12,%rbx), %rax
	addq	%rbp, %rbx
.L18:
	cmpq	%rax, (%rsp)
	movq	%rax, (%r10)
	movl	$4, %r14d
	je	.L20
	leaq	4(%rbx), %rax
	cmpq	%rax, 8(%rsp)
	sbbl	%r14d, %r14d
	andl	$-2, %r14d
	addl	$7, %r14d
.L20:
	cmpq	$0, 32(%rsp)
	jne	.L78
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L79
	cmpq	%rbp, %rbx
	movq	%r11, 48(%rsp)
	jbe	.L72
	movq	16(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%r10, 40(%rsp)
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %ecx
	xorl	%r8d, %r8d
	movq	%r15, %r9
	pushq	%rcx
	movq	%rbx, %rcx
	pushq	$0
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	popq	%rdx
	cmpl	$4, %eax
	popq	%rcx
	movq	40(%rsp), %r10
	je	.L24
	movq	88(%rsp), %rcx
	movq	48(%rsp), %r11
	cmpq	%rcx, %rbx
	jne	.L80
.L25:
	testl	%eax, %eax
	jne	.L49
.L36:
	movq	0(%r13), %rbp
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$5, %r14d
	je	.L36
.L72:
	movq	(%rsp), %r11
.L23:
	movl	168(%rsp), %eax
	testl	%eax, %eax
	je	.L5
	cmpl	$7, %r14d
	je	.L81
.L5:
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rbp, %rbx
	movq	%r12, %rax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L77:
	movq	32(%r13), %rcx
	movl	(%rcx), %edi
	movl	%edi, %esi
	andl	$7, %esi
	je	.L12
	testq	%rdx, %rdx
	jne	.L82
	movq	(%r10), %rax
	movslq	%esi, %rdx
	cmpq	%r11, %rax
	jnb	.L14
	cmpl	$3, %esi
	jle	.L16
.L15:
	movzbl	7(%rcx), %eax
	addq	$4, %rbp
	movb	%al, -4(%rbp)
	movzbl	6(%rcx), %eax
	movb	%al, -3(%rbp)
	movzbl	5(%rcx), %eax
	movb	%al, -2(%rbp)
	movzbl	4(%rcx), %eax
	movb	%al, -1(%rbp)
	andl	$-8, (%rcx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L83:
	cmpq	$4, %rdx
	je	.L15
.L16:
	addq	$1, %rax
	addq	$1, %rdx
	movq	%rax, (%r10)
	movzbl	-1(%rax), %esi
	cmpq	%rax, %r11
	movb	%sil, 3(%rcx,%rdx)
	jne	.L83
.L14:
	cmpq	$3, %rdx
	ja	.L15
	andl	$-8, %edi
	movl	$7, %r14d
	orl	%edi, %edx
	movl	%edx, (%rcx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L78:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L79:
	movq	(%rsp), %r11
	movq	%rbx, 0(%r13)
	jmp	.L23
.L80:
	xorl	%edx, %edx
	testq	%r15, %r15
	je	.L26
	movq	(%r15), %rdx
.L26:
	cmpq	%rdx, 24(%rsp)
	je	.L84
	movq	%rcx, %rdi
	movq	%r12, (%r10)
	subq	%rbp, %rdi
	cmpq	%r11, %rdi
	cmovle	%rdi, %r11
	leaq	3(%r11), %rdi
	testq	%r11, %r11
	cmovns	%r11, %rdi
	sarq	$2, %rdi
	testq	%rdi, %rdi
	je	.L48
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L29:
	movl	(%r12,%rdx,4), %esi
	bswap	%esi
	movl	%esi, 0(%rbp,%rdx,4)
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L29
	salq	$2, %rdi
	leaq	0(%rbp,%rdi), %rdx
	addq	%rdi, %r12
.L28:
	cmpq	%r12, (%rsp)
	movq	%r12, (%r10)
	je	.L30
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rcx
	jnb	.L30
	cmpq	%rdx, %rcx
	jne	.L33
	cmpq	%rbp, %rcx
	jne	.L25
	subl	$1, 20(%r13)
	jmp	.L25
.L84:
	subq	%rcx, %rbx
	subq	%rbx, (%r10)
	jmp	.L25
.L30:
	cmpq	%rdx, %rcx
	jne	.L33
	leaq	__PRETTY_FUNCTION__.9984(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L76:
	cmpq	$0, 32(%rsp)
	jne	.L85
	movq	32(%r13), %rax
	xorl	%r14d, %r14d
	movq	$0, (%rax)
	testb	$1, 16(%r13)
	jne	.L5
	movq	16(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%r15, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r14
	popq	%rdi
	movl	%eax, %r14d
	popq	%r8
	jmp	.L5
.L81:
	movq	(%r10), %rsi
	movq	%r11, %rdx
	subq	%rsi, %rdx
	cmpq	$4, %rdx
	ja	.L38
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rcx
	je	.L40
.L39:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L39
.L40:
	movl	(%rcx), %eax
	movq	%r11, (%r10)
	movl	$7, %r14d
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L5
.L48:
	movq	%rbp, %rdx
	jmp	.L28
.L85:
	leaq	__PRETTY_FUNCTION__.9984(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L38:
	leaq	__PRETTY_FUNCTION__.9984(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L33:
	leaq	__PRETTY_FUNCTION__.9984(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L49:
	movq	(%rsp), %r11
	movl	%eax, %r14d
	jmp	.L23
.L82:
	leaq	__PRETTY_FUNCTION__.9984(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_ucs4, .-__gconv_transform_internal_ucs4
	.p2align 4,,15
	.globl	__gconv_transform_ucs4_internal
	.type	__gconv_transform_ucs4_internal, @function
__gconv_transform_ucs4_internal:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r11
	movq	%rcx, %rbp
	movq	%r9, %r15
	subq	$104, %rsp
	movq	%rax, 56(%rsp)
	leaq	48(%rsi), %rax
	movq	%r8, 24(%rsp)
	movl	160(%rsp), %ebx
	movq	$0, 8(%rsp)
	movq	%rax, 64(%rsp)
	movl	16(%rsi), %eax
	testb	$1, %al
	jne	.L87
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 8(%rsp)
	je	.L87
	movq	%rsi, %rdx
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 8(%rsp)
.L87:
	testl	%ebx, %ebx
	jne	.L178
	movq	24(%rsp), %rbx
	movl	168(%rsp), %esi
	leaq	80(%rsp), %rcx
	movq	8(%r13), %r12
	testq	%rbx, %rbx
	movq	%rbx, %rdx
	cmove	%r13, %rdx
	testq	%r15, %r15
	movq	(%rdx), %r10
	movl	$0, %edx
	movq	$0, 80(%rsp)
	cmovne	%rcx, %rdx
	testl	%esi, %esi
	movq	%rdx, 48(%rsp)
	jne	.L179
.L94:
	leaq	88(%rsp), %rdi
	movq	$0, 16(%rsp)
	movq	%rdi, 72(%rsp)
.L132:
	movq	(%r11), %rdi
	testq	%r15, %r15
	movq	%rdi, (%rsp)
	je	.L102
	movq	(%r15), %rdi
	addq	%rdi, 16(%rsp)
.L102:
	movq	%r10, %rbx
	movq	(%rsp), %rdi
	andl	$2, %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movl	%edx, (%rbx)
	movq	%rsi, %rbx
.L107:
	movq	%rcx, %rdi
.L103:
	leaq	4(%rdi), %rcx
	cmpq	%rcx, %rbp
	jb	.L108
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r12
	jb	.L108
	movl	-4(%rcx), %edx
	bswap	%edx
	testl	%edx, %edx
	jns	.L104
	cmpq	$0, 48(%rsp)
	je	.L142
	testl	%eax, %eax
	jne	.L180
	movq	%rdi, (%r11)
	movl	$6, %r14d
.L105:
	cmpq	$0, 24(%rsp)
	jne	.L181
.L110:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L182
	cmpq	%rbx, %r10
	movq	%r10, 40(%rsp)
	jnb	.L112
	movq	8(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%r11, 32(%rsp)
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %r9
	pushq	%rsi
	pushq	$0
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	popq	%rdx
	cmpl	$4, %eax
	popq	%rcx
	movq	32(%rsp), %r11
	je	.L113
	movq	88(%rsp), %rdx
	movq	40(%rsp), %r10
	cmpq	%rbx, %rdx
	jne	.L183
.L114:
	testl	%eax, %eax
	jne	.L146
.L131:
	movq	80(%rsp), %rax
	movq	0(%r13), %r10
	movq	%rax, 16(%rsp)
	movl	16(%r13), %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L108:
	cmpq	%rdi, %rbp
	movq	%rdi, (%r11)
	movl	$4, %r14d
	je	.L105
	leaq	4(%rbx), %rax
	cmpq	%rax, %r12
	sbbl	%r14d, %r14d
	andl	$-2, %r14d
	addl	$7, %r14d
	cmpq	$0, 24(%rsp)
	je	.L110
.L181:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
.L86:
	addq	$104, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	cmpl	$5, %r14d
	je	.L131
.L112:
	cmpl	$7, %r14d
	jne	.L86
	movl	168(%rsp), %eax
	testl	%eax, %eax
	je	.L86
	movq	(%r11), %rsi
	movq	%rbp, %rdx
	subq	%rsi, %rdx
	cmpq	$4, %rdx
	ja	.L134
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rcx
	je	.L136
.L135:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L135
.L136:
	movl	(%rcx), %eax
	movq	%rbp, (%r11)
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L180:
	movq	48(%rsp), %rdi
	addq	$1, (%rdi)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L179:
	movq	32(%r13), %rsi
	movl	(%rsi), %edi
	movl	%edi, %edx
	andl	$7, %edx
	je	.L94
	testq	%rbx, %rbx
	jne	.L184
	movq	(%r11), %r8
	movslq	%edx, %r9
	cmpq	%r8, %rbp
	jbe	.L140
	cmpl	$3, %edx
	jg	.L141
	leaq	1(%r8), %rdx
	movq	%r9, %rcx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L185:
	addq	$1, %rdx
	cmpq	$4, %rcx
	je	.L97
.L98:
	movq	%rdx, (%r11)
	movzbl	-1(%rdx), %ebx
	addq	$1, %rcx
	cmpq	%rbp, %rdx
	movq	%rdx, %r8
	movb	%bl, 3(%rsi,%rcx)
	jne	.L185
.L96:
	cmpq	$3, %rcx
	jbe	.L186
.L97:
	cmpb	$-128, 4(%rsi)
	ja	.L187
	movzbl	7(%rsi), %eax
	addq	$4, %r10
	movb	%al, -4(%r10)
	movzbl	6(%rsi), %eax
	movb	%al, -3(%r10)
	movzbl	5(%rsi), %eax
	movb	%al, -2(%r10)
	movzbl	4(%rsi), %eax
	movb	%al, -1(%r10)
	movl	(%rsi), %edi
.L101:
	andl	$-8, %edi
	movl	16(%r13), %eax
	movl	%edi, (%rsi)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%rbx, 0(%r13)
	movq	80(%rsp), %rax
	addq	%rax, (%r15)
	jmp	.L112
.L183:
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L115
	movq	(%r15), %rcx
.L115:
	addq	80(%rsp), %rcx
	cmpq	%rcx, 16(%rsp)
	je	.L188
	movq	(%rsp), %rbx
	movl	16(%r13), %r9d
	movq	%r10, %rdi
	movq	48(%rsp), %r14
	movq	%rbx, (%r11)
	andl	$2, %r9d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%ecx, (%rdi)
	movq	%r8, %rdi
.L121:
	movq	%rsi, %rbx
.L117:
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %rbp
	jb	.L123
	leaq	4(%rdi), %r8
	cmpq	%r8, %rdx
	jb	.L189
	movl	-4(%rsi), %ecx
	bswap	%ecx
	testl	%ecx, %ecx
	jns	.L118
	testq	%r14, %r14
	je	.L119
	testl	%r9d, %r9d
	jne	.L190
	movq	%rbx, (%r11)
.L176:
	cmpq	%rdi, %rdx
	jne	.L128
.L122:
	leaq	__PRETTY_FUNCTION__.10091(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L188:
	subq	%rdx, %rbx
	subq	%rbx, (%r11)
	jmp	.L114
.L142:
	movq	%r10, %rbx
	movl	$6, %r14d
	jmp	.L105
.L178:
	cmpq	$0, 24(%rsp)
	jne	.L191
	movq	32(%r13), %rax
	xorl	%r14d, %r14d
	movq	$0, (%rax)
	testb	$1, 16(%r13)
	jne	.L86
	movq	8(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%r15, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r14
	popq	%rdi
	movl	%eax, %r14d
	popq	%r8
	jmp	.L86
.L190:
	addq	$1, (%r14)
	jmp	.L121
.L141:
	movq	%r9, %rcx
	jmp	.L97
.L189:
	cmpq	%rbx, %rbp
	movq	%rbx, (%rsp)
	movq	%rbx, (%r11)
	je	.L176
.L127:
	cmpq	%rdi, %rdx
	jne	.L128
	cmpq	%r10, %rdx
	jne	.L114
	subl	$1, 20(%r13)
	jmp	.L114
.L123:
	cmpq	%rbx, %rbp
	movq	%rbx, (%r11)
	je	.L176
	leaq	4(%rdi), %rcx
	cmpq	%rcx, %rdx
	jnb	.L176
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L187:
	testb	$2, %al
	jne	.L101
	subq	%rcx, %r9
	movl	$6, %r14d
	addq	%r9, %r8
	movq	%r8, (%r11)
	jmp	.L86
.L186:
	andl	$-8, %edi
	movl	$7, %r14d
	orl	%edi, %ecx
	movl	%ecx, (%rsi)
	jmp	.L86
.L140:
	movq	%r9, %rcx
	jmp	.L96
.L119:
	cmpq	%r10, %rdx
	je	.L122
.L128:
	leaq	__PRETTY_FUNCTION__.10091(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L134:
	leaq	__PRETTY_FUNCTION__.10091(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L184:
	leaq	__PRETTY_FUNCTION__.10091(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L191:
	leaq	__PRETTY_FUNCTION__.10091(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L146:
	movl	%eax, %r14d
	jmp	.L112
	.size	__gconv_transform_ucs4_internal, .-__gconv_transform_ucs4_internal
	.p2align 4,,15
	.globl	__gconv_transform_internal_ucs4le
	.type	__gconv_transform_internal_ucs4le, @function
__gconv_transform_internal_ucs4le:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$104, %rsp
	testb	$1, 16(%rsi)
	movq	%rax, 56(%rsp)
	leaq	48(%rsi), %rax
	movq	%rcx, (%rsp)
	movq	%r8, 40(%rsp)
	movq	%r9, 8(%rsp)
	movl	160(%rsp), %r12d
	movq	%rax, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L193
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 24(%rsp)
	je	.L193
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L193:
	testl	%r12d, %r12d
	jne	.L254
	movq	40(%rsp), %rdx
	movl	168(%rsp), %edi
	testq	%rdx, %rdx
	movq	%rdx, %rax
	cmove	%rbx, %rax
	testl	%edi, %edi
	movq	(%rax), %r12
	movq	8(%rbx), %rax
	movq	%rax, 16(%rsp)
	jne	.L255
.L199:
	leaq	88(%rsp), %rax
	movq	%rax, 72(%rsp)
.L220:
	movq	8(%rsp), %rax
	movq	(%r15), %r13
	movq	$0, 32(%rsp)
	testq	%rax, %rax
	je	.L204
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)
.L204:
	movq	(%rsp), %rsi
	movq	16(%rsp), %rdx
	movq	%r12, %rdi
	movl	$4, %ebp
	subq	%r13, %rsi
	subq	%r12, %rdx
	cmpq	%rsi, %rdx
	movq	%rdx, %rax
	movq	%rsi, 48(%rsp)
	cmovg	%rsi, %rax
	movq	%r13, %rsi
	leaq	3(%rax), %rdx
	testq	%rax, %rax
	cmovns	%rax, %rdx
	andq	$-4, %rdx
	leaq	0(%r13,%rdx), %rax
	movq	%rax, (%r15)
	call	__GI_mempcpy@PLT
	movq	(%rsp), %rsi
	cmpq	(%r15), %rsi
	movq	%rax, %r14
	je	.L205
	leaq	4(%rax), %rax
	cmpq	%rax, 16(%rsp)
	sbbl	%ebp, %ebp
	andl	$-2, %ebp
	addl	$7, %ebp
.L205:
	cmpq	$0, 40(%rsp)
	jne	.L256
	addl	$1, 20(%rbx)
	testb	$1, 16(%rbx)
	jne	.L257
	cmpq	%r12, %r14
	jbe	.L208
	movq	24(%rsp), %rdi
	movq	(%rbx), %rax
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %ecx
	xorl	%r8d, %r8d
	pushq	%rcx
	movq	%r14, %rcx
	pushq	$0
	movq	24(%rsp), %r9
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %ecx
	popq	%rdx
	popq	%rsi
	je	.L209
	movq	88(%rsp), %rbp
	cmpq	%r14, %rbp
	jne	.L258
.L210:
	testl	%ecx, %ecx
	jne	.L230
.L219:
	movq	(%rbx), %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	$5, %ebp
	je	.L219
.L208:
	cmpl	$7, %ebp
	jne	.L192
	movl	168(%rsp), %eax
	testl	%eax, %eax
	jne	.L259
.L192:
	addq	$104, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	movq	32(%rbx), %rcx
	movl	(%rcx), %edi
	movl	%edi, %esi
	andl	$7, %esi
	je	.L199
	testq	%rdx, %rdx
	jne	.L260
	movq	(%r15), %rax
	movq	(%rsp), %r8
	movslq	%esi, %rdx
	cmpq	%rax, %r8
	jbe	.L201
	cmpl	$3, %esi
	jg	.L202
	addq	$1, %rax
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L261:
	addq	$1, %rax
	cmpq	$4, %rdx
	je	.L202
.L203:
	movq	%rax, (%r15)
	movzbl	-1(%rax), %esi
	addq	$1, %rdx
	cmpq	%rax, %r8
	movb	%sil, 3(%rcx,%rdx)
	jne	.L261
.L201:
	cmpq	$3, %rdx
	jbe	.L262
.L202:
	movzbl	4(%rcx), %eax
	addq	$4, %r12
	movb	%al, -4(%r12)
	movzbl	5(%rcx), %eax
	movb	%al, -3(%r12)
	movzbl	6(%rcx), %eax
	movb	%al, -2(%r12)
	movzbl	7(%rcx), %eax
	movb	%al, -1(%r12)
	andl	$-8, (%rcx)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L256:
	movq	40(%rsp), %rax
	movq	%r14, (%rax)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r14, (%rbx)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L258:
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L211
	movq	(%rdi), %rax
.L211:
	cmpq	%rax, 32(%rsp)
	jne	.L212
	movq	%r14, %r10
	subq	%rbp, %r10
	subq	%r10, (%r15)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L254:
	cmpq	$0, 40(%rsp)
	jne	.L263
	movq	32(%rbx), %rax
	xorl	%ebp, %ebp
	movq	$0, (%rax)
	testb	$1, 16(%rbx)
	jne	.L192
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%r12
	movq	24(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%rbx
	popq	%r8
	movl	%eax, %ebp
	popq	%r9
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L212:
	movq	48(%rsp), %rax
	movq	%rbp, %rdx
	movq	%r13, %rsi
	subq	%r12, %rdx
	movq	%r12, %rdi
	movl	%ecx, 32(%rsp)
	cmpq	%rax, %rdx
	movq	%rax, %r14
	cmovle	%rdx, %r14
	leaq	3(%r14), %rdx
	testq	%r14, %r14
	cmovns	%r14, %rdx
	andq	$-4, %rdx
	leaq	0(%r13,%rdx), %rax
	movq	%rax, (%r15)
	call	__GI_mempcpy@PLT
	movq	(%rsp), %rcx
	cmpq	(%r15), %rcx
	je	.L213
	leaq	4(%rax), %rsi
	movq	88(%rsp), %rdx
	movl	32(%rsp), %ecx
	cmpq	%rsi, %rbp
	jnb	.L264
	cmpq	%rdx, %rax
	jne	.L216
	cmpq	%r12, %rax
	jne	.L210
	subl	$1, 20(%rbx)
	jmp	.L210
.L264:
	cmpq	%rdx, %rax
	jne	.L216
.L215:
	leaq	__PRETTY_FUNCTION__.10192(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L262:
	andl	$-8, %edi
	movl	$7, %ebp
	orl	%edi, %edx
	movl	%edx, (%rcx)
	jmp	.L192
.L259:
	movq	(%r15), %rdi
	movq	(%rsp), %rdx
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L221
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbx), %rsi
	je	.L223
.L222:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L222
.L223:
	movq	(%rsp), %rax
	movl	$7, %ebp
	movq	%rax, (%r15)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L192
.L213:
	cmpq	%rax, 88(%rsp)
	je	.L215
.L216:
	leaq	__PRETTY_FUNCTION__.10192(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L230:
	movl	%ecx, %ebp
	jmp	.L208
.L221:
	leaq	__PRETTY_FUNCTION__.10192(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L260:
	leaq	__PRETTY_FUNCTION__.10192(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L263:
	leaq	__PRETTY_FUNCTION__.10192(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_ucs4le, .-__gconv_transform_internal_ucs4le
	.p2align 4,,15
	.globl	__gconv_transform_ucs4le_internal
	.type	__gconv_transform_ucs4le_internal, @function
__gconv_transform_ucs4le_internal:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r10
	movq	%rcx, %rbp
	movq	%r9, %r14
	subq	$104, %rsp
	movq	%rax, 56(%rsp)
	leaq	48(%rsi), %rax
	movq	%r8, 24(%rsp)
	movl	160(%rsp), %ebx
	movq	$0, 8(%rsp)
	movq	%rax, 64(%rsp)
	movl	16(%rsi), %eax
	testb	$1, %al
	jne	.L266
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rcx
	movq	%rcx, 8(%rsp)
	je	.L266
	movq	%rcx, %rdx
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 8(%rsp)
.L266:
	testl	%ebx, %ebx
	jne	.L360
	movq	24(%rsp), %rbx
	movl	168(%rsp), %esi
	leaq	80(%rsp), %rcx
	movq	8(%r13), %r12
	testq	%rbx, %rbx
	movq	%rbx, %rdx
	cmove	%r13, %rdx
	testq	%r14, %r14
	movq	(%rdx), %r11
	movl	$0, %edx
	movq	$0, 80(%rsp)
	cmovne	%rcx, %rdx
	testl	%esi, %esi
	movq	%rdx, 48(%rsp)
	jne	.L361
.L273:
	leaq	88(%rsp), %rsi
	movq	%r14, %r15
	movq	$0, 16(%rsp)
	movq	%r11, %r14
	movq	%rsi, 72(%rsp)
.L310:
	movq	(%r10), %rcx
	testq	%r15, %r15
	movq	%rcx, (%rsp)
	je	.L281
	movq	(%r15), %rdi
	addq	%rdi, 16(%rsp)
.L281:
	movq	%r14, %rbx
	movq	(%rsp), %rdi
	andl	$2, %eax
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	movl	%esi, (%rbx)
	movq	%rcx, %rbx
.L286:
	movq	%rdx, %rdi
.L282:
	leaq	4(%rdi), %rdx
	cmpq	%rdx, %rbp
	jb	.L287
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r12
	jb	.L362
	movl	-4(%rdx), %esi
	testl	%esi, %esi
	jns	.L283
	cmpq	$0, 48(%rsp)
	je	.L322
	testl	%eax, %eax
	jne	.L363
	movq	%rdi, (%r10)
	movl	$6, %r11d
.L284:
	cmpq	$0, 24(%rsp)
	jne	.L364
.L290:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L365
	cmpq	%rbx, %r14
	jnb	.L353
	movq	8(%rsp), %rdi
	movq	0(%r13), %rax
	movl	%r11d, 44(%rsp)
	movq	%r10, 32(%rsp)
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %edi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %r9
	pushq	%rdi
	pushq	$0
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	popq	%rdx
	cmpl	$4, %eax
	popq	%rcx
	movq	32(%rsp), %r10
	movl	44(%rsp), %r11d
	je	.L293
	movq	88(%rsp), %rdx
	cmpq	%rbx, %rdx
	jne	.L366
.L294:
	testl	%eax, %eax
	jne	.L324
.L309:
	movq	80(%rsp), %rax
	movq	0(%r13), %r14
	movq	%rax, 16(%rsp)
	movl	16(%r13), %eax
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L287:
	xorl	%r11d, %r11d
	cmpq	%rdi, %rbp
	movq	%rdi, (%r10)
	setne	%r11b
	cmpq	$0, 24(%rsp)
	leal	4(%r11,%r11,2), %r11d
	je	.L290
.L364:
	movq	24(%rsp), %rax
	movl	%r11d, %r15d
	movq	%rbx, (%rax)
.L265:
	addq	$104, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%r11d, %r11d
	cmpq	%rdi, %rbp
	movq	%rdi, (%r10)
	setne	%r11b
	addl	$4, %r11d
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L293:
	cmpl	$5, %r11d
	je	.L309
.L353:
	movl	%r11d, %r15d
.L292:
	cmpl	$7, %r15d
	jne	.L265
	movl	168(%rsp), %eax
	testl	%eax, %eax
	je	.L265
	movq	(%r10), %rsi
	movq	%rbp, %rdx
	subq	%rsi, %rdx
	cmpq	$4, %rdx
	ja	.L312
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rcx
	je	.L314
.L313:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L313
.L314:
	movl	(%rcx), %eax
	movq	%rbp, (%r10)
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L363:
	movq	48(%rsp), %rcx
	addq	$1, (%rcx)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L361:
	movq	32(%r13), %rsi
	movl	(%rsi), %r8d
	movl	%r8d, %edi
	andl	$7, %edi
	je	.L273
	testq	%rbx, %rbx
	jne	.L367
	movq	(%r10), %rdx
	movslq	%edi, %rcx
	cmpq	%rdx, %rbp
	jbe	.L275
	cmpl	$3, %edi
	jg	.L276
	addq	$1, %rdx
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L368:
	addq	$1, %rdx
	cmpq	$4, %rcx
	je	.L276
.L277:
	movq	%rdx, (%r10)
	movzbl	-1(%rdx), %edi
	addq	$1, %rcx
	cmpq	%rdx, %rbp
	movb	%dil, 3(%rsi,%rcx)
	jne	.L368
.L275:
	cmpq	$3, %rcx
	jbe	.L369
.L276:
	cmpb	$-128, 7(%rsi)
	ja	.L370
	movzbl	4(%rsi), %eax
	addq	$4, %r11
	movb	%al, -4(%r11)
	movzbl	5(%rsi), %eax
	movb	%al, -3(%r11)
	movzbl	6(%rsi), %eax
	movb	%al, -2(%r11)
	movzbl	7(%rsi), %eax
	movb	%al, -1(%r11)
	movl	(%rsi), %r8d
.L280:
	andl	$-8, %r8d
	movl	16(%r13), %eax
	movl	%r8d, (%rsi)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r15, %r14
	movq	%rbx, 0(%r13)
	movq	80(%rsp), %rax
	movl	%r11d, %r15d
	addq	%rax, (%r14)
	jmp	.L292
.L366:
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L295
	movq	(%r15), %rcx
.L295:
	addq	80(%rsp), %rcx
	cmpq	16(%rsp), %rcx
	je	.L371
	movq	(%rsp), %r11
	movl	16(%r13), %r9d
	movq	%r14, %rsi
	movq	48(%rsp), %rbx
	movq	%r11, (%r10)
	andl	$2, %r9d
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%r8d, (%rsi)
	movq	%rdi, %rsi
.L301:
	movq	%rcx, %r11
.L297:
	leaq	4(%r11), %rcx
	cmpq	%rcx, %rbp
	jb	.L300
	leaq	4(%rsi), %rdi
	cmpq	%rdi, %rdx
	jb	.L372
	movl	-4(%rcx), %r8d
	testl	%r8d, %r8d
	jns	.L298
	testq	%rbx, %rbx
	je	.L299
	testl	%r9d, %r9d
	jne	.L373
.L300:
	movq	%r11, (%r10)
.L358:
	cmpq	%rsi, %rdx
	jne	.L307
.L302:
	leaq	__PRETTY_FUNCTION__.10300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L371:
	subq	%rdx, %rbx
	subq	%rbx, (%r10)
	jmp	.L294
.L322:
	movq	%r14, %rbx
	movl	$6, %r11d
	jmp	.L284
.L360:
	cmpq	$0, 24(%rsp)
	jne	.L374
	movq	32(%r13), %rax
	xorl	%r15d, %r15d
	movq	$0, (%rax)
	testb	$1, 16(%r13)
	jne	.L265
	movq	8(%rsp), %r15
	movq	%r15, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%r14, %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	popq	%rdi
	movl	%eax, %r15d
	popq	%r8
	jmp	.L265
.L373:
	addq	$1, (%rbx)
	jmp	.L301
.L369:
	andl	$-8, %r8d
	movl	$7, %r15d
	orl	%r8d, %ecx
	movl	%ecx, (%rsi)
	jmp	.L265
.L370:
	testb	$2, %al
	jne	.L280
	movl	$6, %r15d
	jmp	.L265
.L372:
	cmpq	%r11, %rbp
	movq	%r11, (%rsp)
	movq	%r11, (%r10)
	je	.L358
	cmpq	%rsi, %rdx
	jne	.L307
	cmpq	%r14, %rdx
	jne	.L294
	subl	$1, 20(%r13)
	jmp	.L294
.L299:
	cmpq	%r14, %rdx
	je	.L302
.L307:
	leaq	__PRETTY_FUNCTION__.10300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L312:
	leaq	__PRETTY_FUNCTION__.10300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L374:
	leaq	__PRETTY_FUNCTION__.10300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L324:
	movl	%eax, %r15d
	jmp	.L292
.L367:
	leaq	__PRETTY_FUNCTION__.10300(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_ucs4le_internal, .-__gconv_transform_ucs4le_internal
	.p2align 4,,15
	.globl	__gconv_transform_ascii_internal
	.type	__gconv_transform_ascii_internal, @function
__gconv_transform_ascii_internal:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%r9, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rcx, %r12
	subq	$104, %rsp
	movq	%rdx, 8(%rsp)
	movl	16(%rsi), %edx
	movq	%rax, 56(%rsp)
	leaq	48(%rsi), %rax
	movq	%r8, 24(%rsp)
	movl	160(%rsp), %ebx
	movq	$0, 16(%rsp)
	testb	$1, %dl
	movq	%rax, 48(%rsp)
	jne	.L376
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 16(%rsp)
	je	.L376
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L376:
	testl	%ebx, %ebx
	jne	.L432
	movq	24(%rsp), %rax
	leaq	80(%rsp), %rcx
	movq	8(%rbp), %r13
	testq	%rax, %rax
	cmove	%rbp, %rax
	testq	%r15, %r15
	movq	(%rax), %r14
	movl	$0, %eax
	movq	$0, 80(%rsp)
	cmovne	%rcx, %rax
	xorl	%r10d, %r10d
	movq	%rax, 72(%rsp)
	leaq	88(%rsp), %rax
	movq	%rax, 64(%rsp)
.L408:
	movq	8(%rsp), %rax
	testq	%r15, %r15
	movq	(%rax), %r11
	je	.L383
	addq	(%r15), %r10
.L383:
	cmpq	%r12, %r11
	je	.L414
	leaq	4(%r14), %rdi
	cmpq	%rdi, %r13
	jb	.L415
	movq	%r11, %rax
	movq	%r14, %rbx
	movl	$4, 4(%rsp)
	andl	$2, %edx
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	1(%rax), %rax
	movl	%ecx, (%rbx)
	movq	%rdi, %rbx
	movq	%rax, %rsi
.L387:
	cmpq	%rax, %r12
	je	.L384
	leaq	4(%rbx), %rdi
	cmpq	%rdi, %r13
	jb	.L416
.L385:
	movzbl	(%rax), %ecx
	movq	%rax, %rsi
	testb	%cl, %cl
	jns	.L386
	cmpq	$0, 72(%rsp)
	je	.L418
	testl	%edx, %edx
	jne	.L433
.L418:
	movl	$6, 4(%rsp)
.L384:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rax
	movq	%rsi, (%rax)
	jne	.L434
.L389:
	addl	$1, 20(%rbp)
	testb	$1, 16(%rbp)
	jne	.L435
	cmpq	%r14, %rbx
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	jbe	.L375
	movq	16(%rsp), %rdi
	movq	0(%rbp), %rax
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %edx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %r9
	pushq	%rdx
	pushq	$0
	movq	80(%rsp), %rdx
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	popq	%rdx
	popq	%rcx
	je	.L392
	movq	88(%rsp), %rdx
	movq	32(%rsp), %r11
	movq	40(%rsp), %r10
	cmpq	%rbx, %rdx
	jne	.L436
.L393:
	testl	%eax, %eax
	jne	.L420
.L407:
	movq	80(%rsp), %r10
	movl	16(%rbp), %edx
	movq	0(%rbp), %r14
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L416:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rax
	movl	$5, 4(%rsp)
	movq	%rsi, (%rax)
	je	.L389
.L434:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L392:
	cmpl	$5, 4(%rsp)
	je	.L407
.L375:
	movl	4(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	movq	72(%rsp), %rdi
	leaq	1(%rax), %rax
	movl	$6, 4(%rsp)
	movq	%rax, %rsi
	addq	$1, (%rdi)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%r11, %rsi
	movq	%r14, %rbx
	movl	$5, 4(%rsp)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%rbx, 0(%rbp)
	movq	80(%rsp), %rax
	addq	%rax, (%r15)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L436:
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L394
	movq	(%r15), %rcx
.L394:
	addq	80(%rsp), %rcx
	cmpq	%rcx, %r10
	je	.L437
	movq	8(%rsp), %rdi
	cmpq	%r12, %r11
	movl	16(%rbp), %r8d
	movq	%r11, (%rdi)
	je	.L438
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rdx
	jb	.L398
	movq	%r14, %r9
	andl	$2, %r8d
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	1(%r11), %rdi
	movl	%ecx, (%r9)
	movq	%rsi, %r9
.L403:
	cmpq	%rdi, %r12
	je	.L439
	leaq	4(%r9), %rsi
	movq	%rdi, %r11
	cmpq	%rsi, %rdx
	jb	.L400
.L399:
	movzbl	(%r11), %ecx
	testb	%cl, %cl
	jns	.L401
	cmpq	$0, 72(%rsp)
	je	.L402
	testl	%r8d, %r8d
	jne	.L440
.L402:
	movq	8(%rsp), %rax
	cmpq	%r9, %rdx
	movq	%r11, (%rax)
	je	.L397
.L405:
	leaq	__PRETTY_FUNCTION__.10422(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L437:
	subq	%rdx, %rbx
	movq	8(%rsp), %rdi
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovns	%rbx, %rdx
	sarq	$2, %rdx
	subq	%rdx, (%rdi)
	jmp	.L393
.L414:
	movq	%r11, %rsi
	movq	%r14, %rbx
	movl	$4, 4(%rsp)
	jmp	.L384
.L432:
	cmpq	$0, 24(%rsp)
	jne	.L441
	movq	32(%rbp), %rax
	movl	$0, 4(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%rbp)
	jne	.L375
	movq	16(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r14
	movl	%eax, 20(%rsp)
	popq	%rsi
	popq	%rdi
	jmp	.L375
.L400:
	movq	8(%rsp), %rbx
	cmpq	%r9, %rdx
	movq	%rdi, (%rbx)
	jne	.L405
	cmpq	%r14, %rdx
	jne	.L393
.L410:
	subl	$1, 20(%rbp)
	jmp	.L393
.L438:
	cmpq	%r14, %rdx
	jne	.L405
.L397:
	leaq	__PRETTY_FUNCTION__.10422(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L439:
	movq	8(%rsp), %rax
	cmpq	%r9, %rdx
	movq	%r12, (%rax)
	je	.L397
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L440:
	movq	72(%rsp), %rbx
	leaq	1(%r11), %rdi
	addq	$1, (%rbx)
	jmp	.L403
.L398:
	cmpq	%r14, %rdx
	je	.L410
	jmp	.L405
.L441:
	leaq	__PRETTY_FUNCTION__.10422(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L420:
	movl	%eax, 4(%rsp)
	jmp	.L375
	.size	__gconv_transform_ascii_internal, .-__gconv_transform_ascii_internal
	.section	.rodata.str1.1
.LC5:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC7:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC8:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC9:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC10:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC11:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	__gconv_transform_internal_ascii
	.type	__gconv_transform_internal_ascii, @function
__gconv_transform_internal_ascii:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r14d
	movq	%rsi, 64(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 88(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 32(%rsp)
	testb	$1, %r14b
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 56(%rsp)
	jne	.L443
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 56(%rsp)
	je	.L443
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 56(%rsp)
.L443:
	testl	%ebx, %ebx
	jne	.L587
	movq	8(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	112(%rsp), %rdx
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	testq	%r15, %r15
	movq	(%rax), %r11
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, 40(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movq	%rax, 80(%rsp)
	movl	216(%rsp), %eax
	testl	%eax, %eax
	jne	.L588
.L520:
	movq	$0, 24(%rsp)
.L450:
	leaq	136(%rsp), %rax
	movq	%r15, 16(%rsp)
	movq	%r11, %r15
	movq	40(%rsp), %r11
	movq	%rax, 96(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L475:
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.L481
	movq	(%rax), %rax
	addq	%rax, 24(%rsp)
.L481:
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r15, %rbx
	movq	%r13, %rax
	movl	$4, %r10d
	andl	$2, %r14d
.L482:
	cmpq	%rax, %rbp
	je	.L483
.L489:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L523
	cmpq	%rbx, %r11
	jbe	.L524
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L589
	leaq	1(%rbx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L489
	.p2align 4,,10
	.p2align 3
.L483:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L590
.L490:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L591
	cmpq	%r15, %rbx
	jbe	.L527
	movq	56(%rsp), %r14
	movq	(%r12), %rax
	movl	%r10d, 48(%rsp)
	movq	%r11, 40(%rsp)
	movq	%r14, %rdi
	movq	%rax, 120(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r14
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r14d
	popq	%rdi
	movq	40(%rsp), %r11
	movl	48(%rsp), %r10d
	je	.L494
	movq	120(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L592
.L493:
	testl	%r14d, %r14d
	jne	.L530
.L507:
	movq	112(%rsp), %rax
	movl	16(%r12), %r14d
	movq	(%r12), %r15
	movq	%rax, 24(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %r13
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L523:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rdi
	movl	$7, %r10d
	movq	%rax, (%rdi)
	je	.L490
.L590:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L442:
	addq	$152, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	movl	$5, %r10d
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L589:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L593
	cmpq	$0, 80(%rsp)
	je	.L526
	testb	$8, 16(%r12)
	jne	.L594
.L487:
	testl	%r14d, %r14d
	jne	.L595
.L526:
	movl	$6, %r10d
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L494:
	cmpl	$5, %r10d
	movl	%r10d, %r14d
	jne	.L493
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L588:
	movq	32(%r12), %rbx
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	je	.L520
	testq	%rdi, %rdi
	jne	.L596
	cmpl	$4, %edx
	movq	%r13, 128(%rsp)
	movq	%r11, 136(%rsp)
	ja	.L452
	leaq	120(%rsp), %rdi
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movq	%rdx, 24(%rsp)
	movq	%rdi, 16(%rsp)
.L453:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L453
	movq	%r13, %rcx
	subq	%rax, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbp
	jb	.L597
	cmpq	40(%rsp), %r11
	movl	$5, %r10d
	jnb	.L442
	movq	24(%rsp), %rcx
	leaq	1(%r13), %rax
	leaq	119(%rsp), %r8
.L461:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %edi
	addq	$1, %rcx
	movq	%rax, %r9
	addq	$1, %rax
	cmpq	$3, %rcx
	movb	%dil, (%r8,%rcx)
	ja	.L532
	cmpq	%r9, %rbp
	ja	.L461
.L532:
	movq	16(%rsp), %rax
	movq	%rcx, 24(%rsp)
	movq	%rax, 128(%rsp)
	movl	120(%rsp), %eax
	cmpl	$127, %eax
	ja	.L598
	leaq	1(%r11), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%r11)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	16(%rsp), %rax
	movq	%rax, 128(%rsp)
	je	.L472
.L586:
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
.L465:
	subq	16(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L599
	movq	8(%rsp), %rdi
	subq	%rdx, %rax
	andl	$-8, %esi
	movq	136(%rsp), %r11
	movl	16(%r12), %r14d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r13
	movq	112(%rsp), %rax
	movl	%esi, (%rbx)
	movq	%rax, 24(%rsp)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L591:
	movq	16(%rsp), %r15
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%r15)
.L492:
	cmpl	$7, %r10d
	jne	.L442
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L442
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L509
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L511
.L510:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L510
.L511:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r11, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	movl	%eax, %r10d
	popq	%r8
	cmpl	$6, %r10d
	popq	%r9
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movq	40(%rsp), %r11
	je	.L487
	cmpl	$5, %r10d
	jne	.L482
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L527:
	movl	%r10d, %r14d
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rcx, 128(%rsp)
	movq	%rcx, %rax
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L595:
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L592:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L496
	movq	(%rsi), %rax
.L496:
	addq	112(%rsp), %rax
	cmpq	24(%rsp), %rax
	je	.L600
	movq	8(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r15, %rdx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r13, (%rax)
	andl	$2, %ebx
.L498:
	cmpq	%r13, %rbp
	je	.L499
.L506:
	leaq	4(%r13), %rcx
	cmpq	%rcx, %rbp
	jb	.L499
	cmpq	%rdx, %r10
	jbe	.L500
	movl	0(%r13), %eax
	cmpl	$127, %eax
	ja	.L601
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rdx
	leaq	4(%rax), %r13
	cmpq	%r13, %rbp
	movq	%r13, 128(%rsp)
	jne	.L506
.L499:
	cmpq	120(%rsp), %rdx
	movq	8(%rsp), %rax
	movq	%r13, (%rax)
	je	.L602
.L514:
	leaq	__PRETTY_FUNCTION__.10575(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L600:
	movq	8(%rsp), %rsi
	subq	%r10, %rbx
	leaq	0(,%rbx,4), %rax
	subq	%rax, (%rsi)
	jmp	.L493
.L587:
	cmpq	$0, 32(%rsp)
	jne	.L603
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L442
	movq	56(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	xorl	%edx, %edx
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r14
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L442
.L530:
	movl	%r14d, %r10d
	jmp	.L492
.L605:
	movq	%r11, 40(%rsp)
	movq	%r10, 24(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	%r12, %rsi
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %rdx
	movq	24(%rsp), %r10
	movq	40(%rsp), %r11
	je	.L504
	cmpl	$5, %eax
	jne	.L498
.L500:
	cmpq	120(%rsp), %rdx
	movq	8(%rsp), %rax
	movq	%r13, (%rax)
	jne	.L514
	cmpq	%r15, %rdx
	jne	.L493
	subl	$1, 20(%r12)
	jmp	.L493
.L601:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L604
	cmpq	$0, 80(%rsp)
	je	.L499
	testb	$8, 16(%r12)
	jne	.L605
.L504:
	testl	%ebx, %ebx
	je	.L499
	movq	80(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L498
.L597:
	movq	%rbp, %rdx
	movq	8(%rsp), %rsi
	subq	%r13, %rdx
	addq	%rax, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rsi)
	ja	.L455
	addq	$1, %r13
	cmpq	%rax, %rdx
	jbe	.L457
	movq	24(%rsp), %rax
.L458:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %ecx
	addq	$1, %r13
	movb	%cl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L458
.L457:
	movl	$7, %r10d
	jmp	.L442
.L598:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L606
	cmpq	$0, 80(%rsp)
	je	.L521
	testb	$8, %r14b
	jne	.L607
	andl	$2, %r14d
	movl	$6, %r10d
	movq	16(%rsp), %rax
	je	.L442
.L470:
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L471:
	cmpq	16(%rsp), %rax
	jne	.L586
.L521:
	movl	$6, %r10d
	jmp	.L442
.L476:
	testl	%r10d, %r10d
	jne	.L442
.L472:
	movq	112(%rsp), %rax
	movl	16(%r12), %r14d
	movq	%rax, 24(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %r13
	jmp	.L450
.L604:
	movq	%rcx, 128(%rsp)
	movq	%rcx, %r13
	jmp	.L498
.L606:
	movq	16(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L465
.L607:
	movq	16(%rsp), %rax
	addq	24(%rsp), %rax
	leaq	128(%rsp), %rcx
	movq	%r11, 96(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, 56(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	104(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__GI___gconv_transliterate
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%r13
	movq	96(%rsp), %r11
	je	.L608
	movq	128(%rsp), %rax
	cmpq	16(%rsp), %rax
	jne	.L586
	cmpl	$7, %r10d
	jne	.L476
	movq	16(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 48(%rsp)
	je	.L609
	movl	(%rbx), %eax
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %rdi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jle	.L610
	cmpq	$4, 24(%rsp)
	ja	.L611
	movq	24(%rsp), %rsi
	orl	%esi, %eax
	testq	%rsi, %rsi
	movl	%eax, (%rbx)
	je	.L457
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
.L480:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 24(%rsp)
	jne	.L480
	jmp	.L457
.L608:
	andl	$2, %r14d
	movq	128(%rsp), %rax
	je	.L471
	jmp	.L470
.L599:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L455:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L452:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L603:
	leaq	__PRETTY_FUNCTION__.10575(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L602:
	leaq	__PRETTY_FUNCTION__.10575(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L509:
	leaq	__PRETTY_FUNCTION__.10575(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L596:
	leaq	__PRETTY_FUNCTION__.10575(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L611:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__GI___assert_fail
.L610:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__GI___assert_fail
.L609:
	leaq	__PRETTY_FUNCTION__.10505(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_ascii, .-__gconv_transform_internal_ascii
	.p2align 4,,15
	.globl	__gconv_transform_internal_utf8
	.type	__gconv_transform_internal_utf8, @function
__gconv_transform_internal_utf8:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$136, %rsp
	movl	16(%r12), %r10d
	movq	%rsi, 40(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 64(%rsp)
	movq	%rdx, (%rsp)
	movq	%r8, 16(%rsp)
	testb	$1, %r10b
	movq	%r9, 32(%rsp)
	movl	192(%rsp), %ebx
	movq	%rsi, 48(%rsp)
	movq	$0, 8(%rsp)
	jne	.L613
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 8(%rsp)
	je	.L613
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 8(%rsp)
.L613:
	testl	%ebx, %ebx
	jne	.L790
	movq	(%rsp), %rax
	movq	16(%rsp), %rdi
	leaq	96(%rsp), %rdx
	movq	8(%r12), %r14
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 32(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 96(%rsp)
	cmovne	%rdx, %rax
	movq	%rax, 56(%rsp)
	movl	200(%rsp), %eax
	testl	%eax, %eax
	jne	.L791
.L620:
	leaq	120(%rsp), %rax
	movq	%rax, 72(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 80(%rsp)
	.p2align 4,,10
	.p2align 3
.L648:
	movq	%r13, 112(%rsp)
	movq	%r15, 120(%rsp)
	movq	%r15, %rbx
	movq	%r13, %rax
	movl	$4, %r11d
	andl	$2, %r10d
.L654:
	cmpq	%rax, %rbp
	je	.L655
.L664:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L702
	cmpq	%rbx, %r14
	jbe	.L708
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L656
	leaq	1(%rbx), %rcx
	movq	%rcx, 120(%rsp)
	movb	%dl, (%rbx)
.L657:
	movq	112(%rsp), %rax
	movq	120(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 112(%rsp)
	jne	.L664
	.p2align 4,,10
	.p2align 3
.L655:
	cmpq	$0, 16(%rsp)
	movq	(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L792
.L665:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L793
	cmpq	%r15, %rbx
	jbe	.L711
	movq	8(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 24(%rsp)
	movq	%rax, 104(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	200(%rsp), %esi
	leaq	104(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rsi
	pushq	$0
	movq	48(%rsp), %r9
	movq	64(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rdi
	movl	24(%rsp), %r11d
	je	.L669
	movq	104(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L794
.L668:
	testl	%r10d, %r10d
	jne	.L717
.L683:
	movq	(%rsp), %rax
	movl	16(%r12), %r10d
	movq	(%r12), %r15
	movq	(%rax), %r13
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L702:
	cmpq	$0, 16(%rsp)
	movq	(%rsp), %rdi
	movl	$7, %r11d
	movq	%rax, (%rdi)
	je	.L665
.L792:
	movq	16(%rsp), %rax
	movq	%rbx, (%rax)
.L612:
	addq	$136, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	movl	$5, %r11d
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L656:
	leal	-55296(%rdx), %ecx
	cmpl	$2047, %ecx
	jbe	.L658
	testl	%edx, %edx
	js	.L658
	testl	$-2048, %edx
	je	.L704
	testl	$-65536, %edx
	je	.L705
	testl	$-2097152, %edx
	je	.L706
	xorl	%ecx, %ecx
	testl	$-67108864, %edx
	setne	%cl
	addq	$5, %rcx
.L659:
	leaq	(%rbx,%rcx), %rsi
	cmpq	%rsi, %r14
	jb	.L708
	movl	$-256, %eax
	sarl	%cl, %eax
	movb	%al, (%rbx)
	addq	%rcx, 120(%rsp)
.L660:
	movl	%edx, %eax
	subq	$1, %rcx
	shrl	$6, %edx
	andl	$63, %eax
	orl	$-128, %eax
	cmpq	$1, %rcx
	movb	%al, (%rbx,%rcx)
	jne	.L660
	orb	%dl, (%rbx)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L669:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L668
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L791:
	movq	32(%r12), %rax
	movq	%rax, 24(%rsp)
	movl	(%rax), %eax
	movl	%eax, 72(%rsp)
	andl	$7, %eax
	je	.L620
	testq	%rdi, %rdi
	jne	.L795
	cmpl	$4, %eax
	movq	%r13, 112(%rsp)
	movq	%r15, 120(%rsp)
	ja	.L622
	leaq	104(%rsp), %rsi
	movq	24(%rsp), %rcx
	cltq
	xorl	%ebx, %ebx
	movq	%rsi, 72(%rsp)
.L623:
	movzbl	4(%rcx,%rbx), %edx
	movb	%dl, (%rsi,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L623
	movq	%r13, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L796
	cmpq	%r14, %r15
	jnb	.L699
	leaq	1(%r13), %rax
	leaq	103(%rsp), %rsi
.L631:
	movq	%rax, 112(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%dl, (%rsi,%rbx)
	ja	.L719
	cmpq	%rcx, %rbp
	ja	.L631
.L719:
	movq	72(%rsp), %rax
	movq	%rax, 112(%rsp)
	movl	104(%rsp), %eax
	cmpl	$127, %eax
	ja	.L633
	leaq	1(%r15), %rdx
	movq	%rdx, 120(%rsp)
	movb	%al, (%r15)
.L634:
	movq	112(%rsp), %rax
	addq	$4, %rax
	cmpq	72(%rsp), %rax
	movq	%rax, 112(%rsp)
	je	.L780
.L644:
	movq	24(%rsp), %rsi
	subq	72(%rsp), %rax
	movl	(%rsi), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L797
	movq	(%rsp), %rsi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	120(%rsp), %r15
	movl	16(%r12), %r10d
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r13
	movq	24(%rsp), %rax
	movl	%edx, (%rax)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L793:
	movq	32(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	96(%rsp), %rax
	addq	%rax, (%rsi)
.L667:
	cmpl	$7, %r11d
	jne	.L612
	movl	200(%rsp), %eax
	testl	%eax, %eax
	je	.L612
	movq	(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L685
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rcx
	je	.L687
.L686:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L686
.L687:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L711:
	movl	%r11d, %r10d
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L794:
	movq	(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r15, %rdx
	movq	%r13, 112(%rsp)
	movq	%r15, 120(%rsp)
	movq	%r13, (%rax)
	andl	$2, %ebx
.L671:
	cmpq	%r13, %rbp
	je	.L672
.L682:
	leaq	4(%r13), %rax
	cmpq	%rax, %rbp
	jb	.L672
	cmpq	%rdx, %r11
	jbe	.L673
	movl	0(%r13), %eax
	cmpl	$127, %eax
	ja	.L674
	leaq	1(%rdx), %rcx
	movq	%rcx, 120(%rsp)
	movb	%al, (%rdx)
.L675:
	movq	112(%rsp), %rax
	movq	120(%rsp), %rdx
	leaq	4(%rax), %r13
	cmpq	%r13, %rbp
	movq	%r13, 112(%rsp)
	jne	.L682
.L672:
	cmpq	104(%rsp), %rdx
	movq	(%rsp), %rax
	movq	%r13, (%rax)
	je	.L798
.L690:
	leaq	__PRETTY_FUNCTION__.10744(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L699:
	movl	$5, %r11d
	jmp	.L612
.L658:
	cmpq	$0, 56(%rsp)
	je	.L710
	testb	$8, 16(%r12)
	jne	.L799
.L661:
	testl	%r10d, %r10d
	jne	.L800
.L710:
	movl	$6, %r11d
	jmp	.L655
.L704:
	movl	$2, %ecx
	jmp	.L659
.L790:
	cmpq	$0, 16(%rsp)
	jne	.L801
	movq	32(%r12), %rax
	xorl	%r11d, %r11d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L612
	movq	8(%rsp), %r15
	movq	%r15, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	200(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	48(%rsp), %r9
	movq	64(%rsp), %rsi
	movq	56(%rsp), %rdi
	call	*%r15
	movl	%eax, %r11d
	popq	%rax
	popq	%rdx
	jmp	.L612
.L705:
	movl	$3, %ecx
	jmp	.L659
.L717:
	movl	%r10d, %r11d
	jmp	.L667
.L706:
	movl	$4, %ecx
	jmp	.L659
.L804:
	movl	%r10d, 92(%rsp)
	movq	%r11, 24(%rsp)
	subq	$8, %rsp
	pushq	64(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, %r8
	movq	88(%rsp), %r9
	movq	96(%rsp), %rcx
	movq	%r12, %rsi
	movq	80(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	112(%rsp), %r13
	movq	120(%rsp), %rdx
	movq	24(%rsp), %r11
	movl	92(%rsp), %r10d
	je	.L679
	cmpl	$5, %eax
	jne	.L671
.L673:
	cmpq	104(%rsp), %rdx
	movq	(%rsp), %rax
	movq	%r13, (%rax)
	jne	.L690
	cmpq	%r15, %rdx
	jne	.L668
	subl	$1, 20(%r12)
	jmp	.L668
.L674:
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L676
	testl	%eax, %eax
	js	.L676
	testl	$-2048, %eax
	je	.L712
	testl	$-65536, %eax
	je	.L713
	testl	$-2097152, %eax
	je	.L714
	xorl	%ecx, %ecx
	testl	$-67108864, %eax
	setne	%cl
	addq	$5, %rcx
.L677:
	leaq	(%rdx,%rcx), %rsi
	cmpq	%rsi, %r11
	jb	.L673
	movl	$-256, %esi
	sarl	%cl, %esi
	movb	%sil, (%rdx)
	addq	%rcx, 120(%rsp)
.L678:
	movl	%eax, %esi
	subq	$1, %rcx
	shrl	$6, %eax
	andl	$63, %esi
	orl	$-128, %esi
	cmpq	$1, %rcx
	movb	%sil, (%rdx,%rcx)
	jne	.L678
	orb	%al, (%rdx)
	jmp	.L675
.L799:
	movl	%r10d, 24(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	64(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, %rsi
	movq	88(%rsp), %r9
	movq	96(%rsp), %rcx
	movq	80(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	movl	%eax, %r11d
	popq	%r8
	cmpl	$6, %r11d
	popq	%r9
	movq	112(%rsp), %rax
	movq	120(%rsp), %rbx
	movl	24(%rsp), %r10d
	je	.L661
	cmpl	$5, %r11d
	jne	.L654
	jmp	.L655
.L800:
	movq	56(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r11d
	movq	%rax, 112(%rsp)
	addq	$1, (%rsi)
	jmp	.L654
.L796:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L625
	addq	$1, %r13
	cmpq	%rax, %rbx
	movq	24(%rsp), %rcx
	jnb	.L627
.L628:
	movq	%r13, 112(%rsp)
	movzbl	-1(%r13), %edx
	addq	$1, %r13
	movb	%dl, 4(%rcx,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L628
.L627:
	movl	$7, %r11d
	jmp	.L612
.L633:
	leal	-55296(%rax), %edx
	cmpl	$2047, %edx
	jbe	.L635
	testl	%eax, %eax
	js	.L635
	testl	$-2048, %eax
	je	.L695
	testl	$-65536, %eax
	je	.L696
	testl	$-2097152, %eax
	je	.L697
	xorl	%ecx, %ecx
	testl	$-67108864, %eax
	setne	%cl
	addq	$5, %rcx
.L636:
	leaq	(%r15,%rcx), %rdx
	cmpq	%rdx, %r14
	jb	.L699
	movl	$-256, %edx
	sarl	%cl, %edx
	movb	%dl, (%r15)
	addq	%rcx, 120(%rsp)
.L638:
	movl	%eax, %edx
	subq	$1, %rcx
	shrl	$6, %eax
	andl	$63, %edx
	orl	$-128, %edx
	cmpq	$1, %rcx
	movb	%dl, (%r15,%rcx)
	jne	.L638
	orb	%al, (%r15)
	jmp	.L634
.L805:
	movq	72(%rsp), %rax
	movl	%r10d, 92(%rsp)
	leaq	112(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	addq	%rbx, %rax
	movq	%rax, 88(%rsp)
	pushq	64(%rsp)
	movq	%rax, %r8
	movq	80(%rsp), %rdi
	leaq	136(%rsp), %r9
	call	__GI___gconv_transliterate
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r11d
	popq	%r13
	movl	92(%rsp), %r10d
	je	.L802
	movq	112(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L644
	cmpl	$7, %r11d
	je	.L803
	testl	%r11d, %r11d
	jne	.L612
.L780:
	movq	(%rsp), %rax
	movl	16(%r12), %r10d
	movq	(%rax), %r13
	jmp	.L620
.L676:
	cmpq	$0, 56(%rsp)
	je	.L672
	testb	$8, 16(%r12)
	jne	.L804
.L679:
	testl	%ebx, %ebx
	je	.L672
	movq	56(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 112(%rsp)
	addq	$1, (%rax)
	jmp	.L671
.L712:
	movl	$2, %ecx
	jmp	.L677
.L714:
	movl	$4, %ecx
	jmp	.L677
.L713:
	movl	$3, %ecx
	jmp	.L677
.L635:
	cmpq	$0, 56(%rsp)
	je	.L700
	testb	$8, %r10b
	jne	.L805
	andb	$2, %r10b
	movl	$6, %r11d
	je	.L612
.L643:
	movq	56(%rsp), %rax
	addq	$4, 112(%rsp)
	addq	$1, (%rax)
.L642:
	movq	112(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L644
.L700:
	movl	$6, %r11d
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L803:
	movq	72(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 80(%rsp)
	je	.L806
	movq	24(%rsp), %rax
	movq	%rbx, %rsi
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L807
	cmpq	$4, %rbx
	ja	.L808
	movq	24(%rsp), %rsi
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%rsi)
	je	.L627
	movq	72(%rsp), %rcx
	xorl	%eax, %eax
.L653:
	movzbl	(%rcx,%rax), %edx
	movq	24(%rsp), %rsi
	movb	%dl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L653
	jmp	.L627
.L802:
	andb	$2, %r10b
	je	.L642
	jmp	.L643
.L808:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__GI___assert_fail
.L807:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__GI___assert_fail
.L806:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__GI___assert_fail
.L685:
	leaq	__PRETTY_FUNCTION__.10744(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L798:
	leaq	__PRETTY_FUNCTION__.10744(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L797:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L695:
	movl	$2, %ecx
	jmp	.L636
.L697:
	movl	$4, %ecx
	jmp	.L636
.L696:
	movl	$3, %ecx
	jmp	.L636
.L625:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L622:
	leaq	__PRETTY_FUNCTION__.10666(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L801:
	leaq	__PRETTY_FUNCTION__.10744(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L795:
	leaq	__PRETTY_FUNCTION__.10744(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_utf8, .-__gconv_transform_internal_utf8
	.section	.rodata.str1.1
.LC12:
	.string	"ch != 0xc0 && ch != 0xc1"
	.text
	.p2align 4,,15
	.globl	__gconv_transform_utf8_internal
	.type	__gconv_transform_utf8_internal, @function
__gconv_transform_utf8_internal:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$120, %rsp
	movq	%r9, 40(%rsp)
	movl	16(%rsi), %r9d
	movq	%rax, 48(%rsp)
	leaq	48(%rsi), %rax
	movq	%rdx, 8(%rsp)
	movq	%r8, 24(%rsp)
	movl	176(%rsp), %ebx
	testb	$1, %r9b
	movq	%rax, 56(%rsp)
	movq	$0, 16(%rsp)
	jne	.L810
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 16(%rsp)
	je	.L810
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L810:
	testl	%ebx, %ebx
	jne	.L1011
	movq	8(%rsp), %rax
	movq	24(%rsp), %rbx
	leaq	96(%rsp), %rdx
	movl	184(%rsp), %esi
	movq	8(%r12), %r13
	testq	%rbx, %rbx
	movq	(%rax), %r14
	movq	%rbx, %rax
	cmove	%r12, %rax
	cmpq	$0, 40(%rsp)
	movq	(%rax), %r10
	movl	$0, %eax
	movq	$0, 96(%rsp)
	cmovne	%rdx, %rax
	testl	%esi, %esi
	movq	%rax, 72(%rsp)
	jne	.L1012
.L817:
	leaq	104(%rsp), %rax
	cmpq	%r14, %rbp
	movq	%r10, %r15
	movq	%rax, 64(%rsp)
	je	.L927
	.p2align 4,,10
	.p2align 3
.L1018:
	leaq	4(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rbx
	cmpq	%rsi, %r13
	jb	.L929
	movl	$4, %r10d
	andl	$2, %r9d
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L858:
	movl	%eax, (%rbx)
	movq	%rcx, %rdx
	movq	%rsi, %rbx
.L866:
	cmpq	%rdx, %rbp
	je	.L856
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r13
	jb	.L929
.L857:
	movzbl	(%rdx), %eax
	leaq	1(%rdx), %rcx
	cmpl	$127, %eax
	movl	%eax, %edi
	jbe	.L858
	subl	$194, %eax
	cmpl	$29, %eax
	ja	.L859
	movl	%edi, %eax
	movl	$2, %r11d
	andl	$31, %eax
.L860:
	leaq	(%rdx,%r11), %rdi
	movq	%rdi, %r8
	movq	%rdi, 32(%rsp)
	movl	$1, %edi
	cmpq	%r8, %rbp
	jb	.L1013
	.p2align 4,,10
	.p2align 3
.L867:
	movzbl	(%rdx,%rdi), %ecx
	movl	%ecx, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	jne	.L872
	sall	$6, %eax
	andl	$63, %ecx
	addq	$1, %rdi
	orl	%ecx, %eax
	cmpq	%rdi, %r11
	jne	.L867
	movq	%r11, %rdi
.L873:
	cmpq	$2, %r11
	je	.L874
	leal	-4(%r11,%r11,4), %ecx
	movl	%eax, %r11d
	shrl	%cl, %r11d
	testl	%r11d, %r11d
	jne	.L874
	.p2align 4,,10
	.p2align 3
.L865:
	cmpq	$0, 72(%rsp)
	je	.L932
	testl	%r9d, %r9d
	jne	.L1014
.L932:
	movl	$6, %r10d
.L856:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L1015
.L876:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L1016
	cmpq	%rbx, %r15
	jnb	.L1009
	movq	16(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r10d, 32(%rsp)
	movq	%rax, 104(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	184(%rsp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rsi
	pushq	$0
	movq	56(%rsp), %r9
	movq	80(%rsp), %rdx
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	popq	%rdx
	cmpl	$4, %eax
	popq	%rcx
	movl	32(%rsp), %r10d
	je	.L879
	movq	104(%rsp), %rdi
	cmpq	%rbx, %rdi
	jne	.L1017
.L880:
	testl	%eax, %eax
	jne	.L941
.L907:
	movq	8(%rsp), %rax
	movl	16(%r12), %r9d
	movq	(%r12), %r15
	movq	(%rax), %r14
	cmpq	%r14, %rbp
	jne	.L1018
.L927:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	%r15, %rbx
	movl	$4, %r10d
	movq	%rdx, (%rax)
	je	.L876
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	24(%rsp), %rax
	movl	%r10d, %r15d
	movq	%rbx, (%rax)
.L809:
	addq	$120, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L874:
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L865
	movq	32(%rsp), %rcx
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L929:
	movl	$5, %r10d
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L879:
	cmpl	$5, %r10d
	je	.L907
.L1009:
	movl	%r10d, %r15d
.L878:
	movl	184(%rsp), %eax
	testl	%eax, %eax
	je	.L809
	cmpl	$7, %r15d
	jne	.L809
	movq	8(%rsp), %rax
	movq	32(%r12), %r8
	movq	%rbp, %r10
	movq	(%rax), %r9
	movzbl	(%r9), %edx
	subq	%r9, %r10
	movl	%r10d, (%r8)
	leal	-192(%rdx), %ecx
	movl	%edx, %eax
	cmpl	$1, %ecx
	jbe	.L1019
	subl	$194, %edx
	cmpl	$29, %edx
	ja	.L910
	andl	$31, %eax
	movl	$1, %esi
	movl	$512, %edi
.L911:
	leaq	1(%r9), %rcx
	movq	8(%rsp), %r11
	cmpq	%rbp, %rcx
	movq	%rcx, (%r11)
	jnb	.L915
.L916:
	movzbl	(%rcx), %edx
	sall	$6, %eax
	addq	$1, %rcx
	movq	%rcx, (%r11)
	andl	$63, %edx
	orl	%edx, %eax
	cmpq	%rbp, %rcx
	jne	.L916
	subq	%rcx, %r9
	leaq	1(%r9,%rsi), %rsi
.L915:
	leal	(%rsi,%rsi,2), %ecx
	orl	%r10d, %edi
	movl	%edi, (%r8)
	addl	%ecx, %ecx
	sall	%cl, %eax
	movl	%eax, 4(%r8)
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L859:
	movl	%edi, %eax
	andl	$-16, %eax
	cmpb	$-32, %al
	jne	.L861
	movl	%edi, %eax
	movl	$3, %r11d
	andl	$15, %eax
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L872:
	cmpq	%rdi, %r11
	ja	.L865
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	32(%r12), %rdi
	movl	(%rdi), %eax
	movl	%eax, %r11d
	andl	$7, %r11d
	je	.L817
	testq	%rbx, %rbx
	jne	.L1020
	movl	%eax, %edx
	leaq	inmask.10838(%rip), %rsi
	movl	4(%rdi), %ecx
	sarl	$8, %edx
	movzbl	%al, %eax
	movslq	%edx, %rdx
	movzbl	-2(%rsi,%rdx), %esi
	movb	%sil, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L820:
	subq	$1, %rdx
	cmpq	%rdx, %rax
	jbe	.L819
	movl	%ecx, %esi
	andl	$63, %esi
	orl	$-128, %esi
	movb	%sil, 104(%rsp,%rdx)
.L819:
	shrl	$6, %ecx
	cmpq	$1, %rdx
	ja	.L820
	leaq	4(%r10), %rbx
	cmpq	%rbx, %r13
	jb	.L821
	orb	%cl, 104(%rsp)
	leaq	104(%rsp), %rsi
	leaq	103(%rsp), %r8
	movq	%r14, %rdx
	movq	%rsi, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L822:
	addq	$1, %rdx
	movzbl	-1(%rdx), %ecx
	addq	$1, %rax
	cmpq	$5, %rax
	setbe	%sil
	cmpq	%rdx, %rbp
	movb	%cl, (%r8,%rax)
	seta	%cl
	testb	%cl, %sil
	jne	.L822
	movzbl	104(%rsp), %r8d
	cmpl	$127, %r8d
	movl	%r8d, %edx
	ja	.L1021
	movq	32(%rsp), %rax
	movl	%r8d, (%r10)
	movl	(%rdi), %r11d
	leaq	1(%rax), %r15
	andl	$7, %r11d
.L832:
	subq	32(%rsp), %r15
	movslq	%r11d, %r11
	cmpq	%r11, %r15
	jle	.L1022
	movq	8(%rsp), %rax
	subq	%r11, %r15
	movl	16(%r12), %r9d
	addq	%r15, %r14
	movq	%rbx, %r10
	movq	%r14, (%rax)
	movl	$0, (%rdi)
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	72(%rsp), %rax
	addq	%rdi, %rdx
	movl	$6, %r10d
	addq	$1, (%rax)
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	40(%rsp), %rsi
	movq	%rbx, (%r12)
	movl	%r10d, %r15d
	movq	96(%rsp), %rax
	addq	%rax, (%rsi)
	jmp	.L878
.L1013:
	cmpq	%rcx, %rbp
	jbe	.L934
	movzbl	1(%rdx), %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L865
	movq	%rbp, %r8
	leaq	2(%rdx), %rsi
	subq	%rdx, %r8
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L871:
	movzbl	(%rcx), %eax
	addq	$1, %rsi
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L865
.L870:
	addq	$1, %rdi
	movq	%rsi, %rcx
	cmpq	%rdi, %r8
	jne	.L871
.L868:
	cmpq	%rcx, %rbp
	jne	.L1023
	movl	$7, %r10d
	jmp	.L856
.L1017:
	movq	8(%rsp), %rsi
	cmpq	%r14, %rbp
	movl	16(%r12), %edx
	movq	%r14, (%rsi)
	je	.L1024
	leaq	4(%r15), %rsi
	cmpq	%rsi, %rdi
	jb	.L883
	andl	$2, %edx
	movq	%r15, %r8
	movl	%edx, %r11d
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L886:
	movl	%edx, (%r8)
	movq	%rcx, %r14
	movq	%rsi, %r8
.L895:
	cmpq	%r14, %rbp
	je	.L1025
	leaq	4(%r8), %rsi
	cmpq	%rsi, %rdi
	jb	.L885
.L884:
	movzbl	(%r14), %edx
	leaq	1(%r14), %rcx
	cmpl	$127, %edx
	movl	%edx, %r9d
	jbe	.L886
	subl	$194, %edx
	cmpl	$29, %edx
	ja	.L887
	movl	%r9d, %edx
	movl	$2, %ebx
	andl	$31, %edx
.L888:
	leaq	(%r14,%rbx), %r10
	movl	$1, %r9d
	cmpq	%r10, %rbp
	movq	%r10, 32(%rsp)
	jb	.L1026
	.p2align 4,,10
	.p2align 3
.L896:
	movzbl	(%r14,%r9), %ecx
	movl	%ecx, %r10d
	andl	$-64, %r10d
	cmpb	$-128, %r10b
	jne	.L901
	sall	$6, %edx
	andl	$63, %ecx
	addq	$1, %r9
	orl	%ecx, %edx
	cmpq	%r9, %rbx
	jne	.L896
	movq	%rbx, %r9
.L902:
	cmpq	$2, %rbx
	je	.L903
	leal	-4(%rbx,%rbx,4), %ecx
	movl	%edx, %ebx
	shrl	%cl, %ebx
	testl	%ebx, %ebx
	je	.L893
.L903:
	leal	-55296(%rdx), %ecx
	cmpl	$2047, %ecx
	jbe	.L893
	movq	32(%rsp), %rcx
	jmp	.L886
.L821:
	movl	$5, %r15d
	jmp	.L809
.L861:
	movl	%edi, %eax
	andl	$-8, %eax
	cmpb	$-16, %al
	jne	.L862
	movl	%edi, %eax
	movl	$4, %r11d
	andl	$7, %eax
	jmp	.L860
.L1011:
	cmpq	$0, 24(%rsp)
	jne	.L1027
	movq	32(%r12), %rax
	xorl	%r15d, %r15d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L809
	movq	16(%rsp), %r15
	movq	%r15, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	184(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	56(%rsp), %r9
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	call	*%r15
	popq	%rdi
	movl	%eax, %r15d
	popq	%r8
	jmp	.L809
.L885:
	movq	8(%rsp), %rsi
	cmpq	%r8, %rdi
	movq	%r14, (%rsi)
	jne	.L905
	cmpq	%r15, %rdi
	jne	.L880
.L919:
	subl	$1, 20(%r12)
	jmp	.L880
.L940:
	movl	$1, %edx
.L898:
	movq	%rdx, %r9
.L893:
	cmpq	$0, 72(%rsp)
	je	.L894
	testl	%r11d, %r11d
	jne	.L1028
.L894:
	movq	8(%rsp), %rax
	cmpq	%r8, %rdi
	movq	%r14, (%rax)
	je	.L882
.L905:
	leaq	__PRETTY_FUNCTION__.10931(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L887:
	movl	%r9d, %edx
	andl	$-16, %edx
	cmpb	$-32, %dl
	jne	.L889
	movl	%r9d, %edx
	movl	$3, %ebx
	andl	$15, %edx
	jmp	.L888
.L934:
	movl	$1, %r8d
	jmp	.L868
.L1024:
	cmpq	%r15, %rdi
	jne	.L905
.L882:
	leaq	__PRETTY_FUNCTION__.10931(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L889:
	movl	%r9d, %edx
	andl	$-8, %edx
	cmpb	$-16, %dl
	jne	.L890
	movl	%r9d, %edx
	movl	$4, %ebx
	andl	$7, %edx
	jmp	.L888
.L901:
	cmpq	%r9, %rbx
	ja	.L893
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	8(%rsp), %rax
	cmpq	%r8, %rdi
	movq	%rbp, (%rax)
	je	.L882
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L1028:
	movq	72(%rsp), %rsi
	addq	%r9, %r14
	addq	$1, (%rsi)
	jmp	.L895
.L883:
	cmpq	%r15, %rdi
	je	.L919
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	32(%rsp), %rsi
	addq	%rax, %rsi
	movq	%rsi, 80(%rsp)
	leal	-194(%r8), %esi
	cmpl	$29, %esi
	movl	%esi, 92(%rsp)
	ja	.L824
	movl	%r8d, %esi
	movl	$2, %ecx
	andl	$31, %esi
	movl	%esi, 64(%rsp)
.L825:
	movq	32(%rsp), %rsi
	leaq	(%rsi,%rcx), %r15
	cmpq	%r15, 80(%rsp)
	movl	$1, %esi
	jb	.L1029
	movl	64(%rsp), %edx
	jmp	.L833
.L1031:
	sall	$6, %edx
	andl	$63, %eax
	addq	$1, %rsi
	orl	%eax, %edx
	cmpq	%rsi, %rcx
	je	.L1030
.L833:
	movq	32(%rsp), %rax
	movzbl	(%rax,%rsi), %eax
	movl	%eax, %r8d
	andl	$-64, %r8d
	cmpb	$-128, %r8b
	je	.L1031
	cmpq	%rsi, %rcx
	movl	%edx, 64(%rsp)
	jbe	.L840
.L830:
	cmpq	$0, 72(%rsp)
	je	.L831
	andl	$2, %r9d
	jne	.L1032
.L831:
	movl	$6, %r15d
	jmp	.L809
.L1026:
	cmpq	%rcx, %rbp
	jbe	.L897
	movzbl	1(%r14), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L940
	movq	%rbp, %r9
	leaq	2(%r14), %rsi
	movl	$1, %edx
	subq	%r14, %r9
	jmp	.L899
.L900:
	movzbl	(%rcx), %ecx
	addq	$1, %rsi
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L898
.L899:
	addq	$1, %rdx
	movq	%rsi, %rcx
	cmpq	%rdx, %r9
	jne	.L900
.L897:
	cmpq	%rbp, %rcx
	je	.L894
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L910:
	movl	%eax, %edx
	andl	$-16, %edx
	cmpb	$-32, %dl
	jne	.L912
	andl	$15, %eax
	movl	$2, %esi
	movl	$768, %edi
	jmp	.L911
.L862:
	movl	%edi, %eax
	andl	$-4, %eax
	cmpb	$-8, %al
	jne	.L863
	movl	%edi, %eax
	movl	$5, %r11d
	andl	$3, %eax
	jmp	.L860
.L1030:
	movl	%edx, 64(%rsp)
	movq	%rcx, %rsi
.L840:
	cmpq	$2, %rcx
	je	.L841
	movl	64(%rsp), %edx
	leal	-4(%rcx,%rcx,4), %ecx
	movl	%edx, %eax
	shrl	%cl, %eax
	testl	%eax, %eax
	je	.L830
	leal	-55296(%rdx), %eax
	cmpl	$2047, %eax
	jbe	.L830
	cmpq	32(%rsp), %r15
	movl	%edx, (%r10)
	jne	.L1033
	movl	16(%r12), %r9d
	jmp	.L817
.L824:
	movl	%r8d, %ecx
	andl	$-16, %ecx
	cmpb	$-32, %cl
	jne	.L826
	movl	%r8d, %esi
	movl	$3, %ecx
	andl	$15, %esi
	movl	%esi, 64(%rsp)
	jmp	.L825
.L1029:
	movq	32(%rsp), %rbx
	leaq	1(%rbx), %rcx
	cmpq	%rcx, 80(%rsp)
	jbe	.L834
	movzbl	105(%rsp), %ecx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	jne	.L830
	addq	$2, %rbx
.L836:
	movq	%rbx, %rsi
	subq	32(%rsp), %rsi
	cmpq	%rbx, 80(%rsp)
	movq	%rbx, %rcx
	jbe	.L1034
	movzbl	(%rcx), %ecx
	addq	$1, %rbx
	andl	$-64, %ecx
	cmpb	$-128, %cl
	je	.L836
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L1032:
	movq	32(%rsp), %rax
	movq	%r10, %rbx
	leaq	(%rax,%rsi), %r15
	movq	72(%rsp), %rsi
	addq	$1, (%rsi)
	cmpq	%rax, %r15
	jne	.L832
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L841:
	movl	64(%rsp), %eax
	subl	$55296, %eax
	cmpl	$2047, %eax
	jbe	.L830
	movl	64(%rsp), %eax
	movl	%eax, (%r10)
	movq	32(%rsp), %rax
	movl	(%rdi), %r11d
	leaq	2(%rax), %r15
	andl	$7, %r11d
	jmp	.L832
.L1022:
	leaq	__PRETTY_FUNCTION__.10849(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L863:
	movl	%edi, %eax
	andl	$-2, %eax
	cmpb	$-4, %al
	jne	.L930
	movl	%edi, %eax
	movl	$6, %r11d
	andl	$1, %eax
	jmp	.L860
.L826:
	movl	%r8d, %ecx
	andl	$-8, %ecx
	cmpb	$-16, %cl
	jne	.L827
	movl	%r8d, %esi
	movl	$4, %ecx
	andl	$7, %esi
	movl	%esi, 64(%rsp)
	jmp	.L825
.L1034:
	jne	.L830
	movq	32(%rsp), %rsi
	leaq	6(%rsi), %rcx
	cmpq	%rcx, 80(%rsp)
	je	.L1035
.L843:
	movslq	%r11d, %r11
	movq	%rax, %rcx
	movq	8(%rsp), %rsi
	subq	%r11, %rcx
	subl	$192, %r8d
	addq	%rcx, %r14
	cmpl	$1, %r8d
	movq	%r14, (%rsi)
	movl	%eax, (%rdi)
	jbe	.L1036
	cmpl	$29, 92(%rsp)
	ja	.L848
	andl	$31, %edx
	movl	$512, %r8d
	movl	$1, %esi
.L849:
	movq	32(%rsp), %rbx
	leaq	1(%rbx), %r9
	cmpq	%r9, 80(%rsp)
	jbe	.L853
.L854:
	movzbl	(%r9), %ecx
	sall	$6, %edx
	addq	$1, %r9
	andl	$63, %ecx
	orl	%ecx, %edx
	cmpq	%r9, 80(%rsp)
	jne	.L854
	movq	32(%rsp), %r10
	subq	80(%rsp), %r10
	leaq	1(%r10,%rsi), %rsi
.L853:
	leal	(%rsi,%rsi,2), %ecx
	orl	%r8d, %eax
	movl	$7, %r15d
	movl	%eax, (%rdi)
	addl	%ecx, %ecx
	sall	%cl, %edx
	movl	%edx, 4(%rdi)
	jmp	.L809
.L848:
	movl	%edx, %ecx
	andl	$-16, %ecx
	cmpb	$-32, %cl
	jne	.L850
	andl	$15, %edx
	movl	$768, %r8d
	movl	$2, %esi
	jmp	.L849
.L1036:
	leaq	__PRETTY_FUNCTION__.10849(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$483, %edx
	call	__GI___assert_fail
.L1035:
	leaq	__PRETTY_FUNCTION__.10849(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__GI___assert_fail
.L850:
	movl	%edx, %ecx
	andl	$-8, %ecx
	cmpb	$-16, %cl
	jne	.L851
	andl	$7, %edx
	movl	$1024, %r8d
	movl	$3, %esi
	jmp	.L849
.L1033:
	movl	(%rdi), %r11d
	andl	$7, %r11d
	jmp	.L832
.L834:
	je	.L843
	jmp	.L830
.L827:
	movl	%r8d, %ecx
	andl	$-4, %ecx
	cmpb	$-8, %cl
	jne	.L828
	movl	%r8d, %esi
	movl	$5, %ecx
	andl	$3, %esi
	movl	%esi, 64(%rsp)
	jmp	.L825
.L930:
	xorl	%edi, %edi
.L864:
	addq	$1, %rdi
	leaq	(%rdx,%rdi), %rax
	cmpq	%rax, %rbp
	jbe	.L865
	movzbl	(%rdx,%rdi), %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L865
	cmpq	$5, %rdi
	jne	.L864
	jmp	.L865
.L851:
	movl	%edx, %ecx
	andl	$-4, %ecx
	cmpb	$-8, %cl
	jne	.L852
	andl	$3, %edx
	movl	$1280, %r8d
	movl	$4, %esi
	jmp	.L849
.L828:
	movl	%r8d, %ecx
	andl	$-2, %ecx
	cmpb	$-4, %cl
	jne	.L923
	movl	%r8d, %esi
	movl	$6, %ecx
	andl	$1, %esi
	movl	%esi, 64(%rsp)
	jmp	.L825
.L852:
	andl	$1, %edx
	movl	$1536, %r8d
	movl	$5, %esi
	jmp	.L849
.L923:
	movq	32(%rsp), %rdx
	xorl	%esi, %esi
.L829:
	addq	$1, %rsi
	leaq	(%rdx,%rsi), %rax
	cmpq	%rax, 80(%rsp)
	jbe	.L830
	movzbl	(%rdx,%rsi), %eax
	andl	$-64, %eax
	cmpb	$-128, %al
	jne	.L830
	cmpq	$5, %rsi
	jne	.L829
	jmp	.L830
.L941:
	movl	%eax, %r15d
	jmp	.L878
.L1020:
	leaq	__PRETTY_FUNCTION__.10931(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L1027:
	leaq	__PRETTY_FUNCTION__.10931(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L912:
	movl	%eax, %edx
	andl	$-8, %edx
	cmpb	$-16, %dl
	jne	.L913
	andl	$7, %eax
	movl	$3, %esi
	movl	$1024, %edi
	jmp	.L911
.L890:
	movl	%r9d, %edx
	andl	$-4, %edx
	cmpb	$-8, %dl
	jne	.L891
	movl	%r9d, %edx
	movl	$5, %ebx
	andl	$3, %edx
	jmp	.L888
.L1019:
	leaq	__PRETTY_FUNCTION__.10931(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$794, %edx
	call	__GI___assert_fail
.L1023:
	movq	%r8, %rdi
	jmp	.L865
.L913:
	movl	%eax, %edx
	andl	$-4, %edx
	cmpb	$-8, %dl
	jne	.L914
	andl	$3, %eax
	movl	$4, %esi
	movl	$1280, %edi
	jmp	.L911
.L891:
	movl	%r9d, %edx
	andl	$-2, %edx
	cmpb	$-4, %dl
	jne	.L937
	movl	%r9d, %edx
	movl	$6, %ebx
	andl	$1, %edx
	jmp	.L888
.L914:
	andl	$1, %eax
	movl	$5, %esi
	movl	$1536, %edi
	jmp	.L911
.L937:
	xorl	%r9d, %r9d
.L892:
	addq	$1, %r9
	leaq	(%r14,%r9), %rdx
	cmpq	%rdx, %rbp
	jbe	.L893
	movzbl	(%r14,%r9), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L893
	cmpq	$5, %r9
	jne	.L892
	jmp	.L893
	.size	__gconv_transform_utf8_internal, .-__gconv_transform_utf8_internal
	.p2align 4,,15
	.globl	__gconv_transform_ucs2_internal
	.type	__gconv_transform_ucs2_internal, @function
__gconv_transform_ucs2_internal:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	addq	$104, %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rcx, %rbp
	movq	%r9, %r15
	subq	$104, %rsp
	movq	%rdi, 48(%rsp)
	leaq	48(%rsi), %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 24(%rsp)
	movl	160(%rsp), %ebx
	movq	%rdi, 56(%rsp)
	movl	16(%rsi), %edi
	movq	$0, 16(%rsp)
	testb	$1, %dil
	jne	.L1038
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 16(%rsp)
	je	.L1038
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L1038:
	testl	%ebx, %ebx
	jne	.L1137
	movq	8(%rsp), %rax
	movq	24(%rsp), %rsi
	leaq	80(%rsp), %rdx
	movl	168(%rsp), %r8d
	movq	8(%r12), %r11
	testq	%rsi, %rsi
	movq	(%rax), %r14
	movq	%rsi, %rax
	cmove	%r12, %rax
	testq	%r15, %r15
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 80(%rsp)
	cmovne	%rdx, %rax
	testl	%r8d, %r8d
	movq	%rax, 72(%rsp)
	jne	.L1138
.L1096:
	xorl	%r10d, %r10d
.L1045:
	leaq	88(%rsp), %rax
	movq	%rax, 64(%rsp)
	movq	%r11, %rax
	movq	%r14, %r11
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1060:
	testq	%r15, %r15
	je	.L1061
	addq	(%r15), %r10
.L1061:
	cmpq	%r11, %rbp
	je	.L1098
	leaq	2(%r11), %rax
	cmpq	%rax, %rbp
	jb	.L1099
	leaq	4(%r13), %rcx
	movq	%r13, %rbx
	cmpq	%rcx, %r14
	jb	.L1100
	movl	$4, 4(%rsp)
	andl	$2, %edi
.L1063:
	movzwl	-2(%rax), %edx
	leaq	-2(%rax), %r8
	leal	10240(%rdx), %esi
	cmpw	$2047, %si
	jbe	.L1139
	movl	%edx, (%rbx)
	movq	%rcx, %rbx
.L1065:
	cmpq	%rax, %rbp
	je	.L1062
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L1101
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L1102
	movq	%rdx, %rax
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1139:
	cmpq	$0, 72(%rsp)
	je	.L1104
	testl	%edi, %edi
	jne	.L1140
.L1104:
	movq	%r8, %rax
	movl	$6, 4(%rsp)
.L1062:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L1141
.L1067:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L1142
	cmpq	%r13, %rbx
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	jbe	.L1069
	movq	16(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %r9
	pushq	%rsi
	pushq	$0
	movq	80(%rsp), %rdx
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	popq	%rdx
	popq	%rcx
	je	.L1070
	movq	88(%rsp), %rdx
	movq	32(%rsp), %r11
	movq	40(%rsp), %r10
	cmpq	%rbx, %rdx
	jne	.L1143
.L1071:
	testl	%eax, %eax
	jne	.L1109
.L1085:
	movq	8(%rsp), %rax
	movq	80(%rsp), %r10
	movl	16(%r12), %edi
	movq	(%r12), %r13
	movq	(%rax), %r11
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1101:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rdi
	movl	$7, 4(%rsp)
	movq	%rax, (%rdi)
	je	.L1067
.L1141:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
.L1037:
	movl	4(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1070:
	cmpl	$5, 4(%rsp)
	je	.L1085
.L1069:
	movl	168(%rsp), %eax
	testl	%eax, %eax
	je	.L1037
	cmpl	$7, 4(%rsp)
	jne	.L1037
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1087
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L1089
.L1088:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1088
.L1089:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1102:
	movl	$5, 4(%rsp)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	32(%r12), %rdx
	movl	(%rdx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.L1096
	testq	%rsi, %rsi
	jne	.L1144
	cmpl	$4, %ecx
	ja	.L1047
	movzbl	4(%rdx), %esi
	cmpl	$1, %ecx
	movb	%sil, 88(%rsp)
	je	.L1048
	movzbl	5(%rdx), %esi
	movl	$2, %r9d
	movb	%sil, 89(%rsp)
	movq	%r14, %rsi
.L1049:
	cmpq	%rsi, %rbp
	jb	.L1145
	leaq	4(%r13), %r8
	cmpq	%r8, %r11
	jnb	.L1054
	movl	$5, 4(%rsp)
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%r13, %rbx
	movq	%r11, %rax
	movl	$7, 4(%rsp)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	72(%rsp), %rcx
	movl	$6, 4(%rsp)
	addq	$1, (%rcx)
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	%rbx, (%r12)
	movq	80(%rsp), %rax
	addq	%rax, (%r15)
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%r11, %rax
	movl	$5, 4(%rsp)
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1143:
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L1072
	movq	(%r15), %rcx
.L1072:
	addq	80(%rsp), %rcx
	cmpq	%r10, %rcx
	je	.L1146
	movq	8(%rsp), %rdi
	cmpq	%r11, %rbp
	movq	%r11, (%rdi)
	je	.L1147
	leaq	2(%r11), %rcx
	cmpq	%rcx, %rbp
	jb	.L1106
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rdx
	jb	.L1077
	movl	16(%r12), %r9d
	movq	%r13, %r10
	andl	$2, %r9d
.L1078:
	movzwl	-2(%rcx), %esi
	leaq	-2(%rcx), %rbx
	leal	10240(%rsi), %r8d
	cmpw	$2047, %r8w
	jbe	.L1148
	movl	%esi, (%r10)
	movq	%rdi, %r10
.L1081:
	cmpq	%rcx, %rbp
	je	.L1149
	leaq	2(%rcx), %rsi
	cmpq	%rsi, %rbp
	jb	.L1076
	leaq	4(%r10), %rdi
	cmpq	%rdi, %rdx
	jb	.L1079
	movq	%rsi, %rcx
	jmp	.L1078
.L1146:
	subq	%rdx, %rbx
	movq	8(%rsp), %rdi
	movq	%rbx, %rdx
	shrq	$63, %rdx
	addq	%rbx, %rdx
	sarq	%rdx
	subq	%rdx, (%rdi)
	jmp	.L1071
.L1048:
	leaq	1(%r14), %rsi
	movl	$1, %r9d
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	%r13, %rbx
	movq	%rbp, %rax
	movl	$4, 4(%rsp)
	jmp	.L1062
.L1054:
	movzbl	(%r14), %esi
	movb	%sil, 88(%rsp,%r9)
	movzwl	88(%rsp), %esi
	leal	10240(%rsi), %r9d
	cmpw	$2047, %r9w
	jbe	.L1150
	movl	%esi, 0(%r13)
	movl	(%rdx), %eax
	movq	%r8, %r13
	movl	%eax, %ecx
	andl	$7, %ecx
.L1058:
	cmpl	$1, %ecx
	movslq	%ecx, %rsi
	jg	.L1151
	movq	8(%rsp), %rdi
	subq	%rsi, %r14
	andl	$-8, %eax
	addq	$2, %r14
	movq	80(%rsp), %r10
	movq	%r14, (%rdi)
	movl	%eax, (%rdx)
	movl	16(%r12), %edi
	jmp	.L1045
.L1137:
	cmpq	$0, 24(%rsp)
	jne	.L1152
	movq	32(%r12), %rax
	movl	$0, 4(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L1037
	movq	16(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	call	*%r14
	movl	%eax, 20(%rsp)
	popq	%r9
	popq	%r10
	jmp	.L1037
.L1148:
	cmpq	$0, 72(%rsp)
	je	.L1108
	testl	%r9d, %r9d
	jne	.L1153
.L1108:
	movq	%rbx, %rcx
.L1076:
	movq	8(%rsp), %rax
	cmpq	%r10, %rdx
	movq	%rcx, (%rax)
	je	.L1075
.L1083:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L1079:
	movq	8(%rsp), %rdi
	cmpq	%r10, %rdx
	movq	%rcx, (%rdi)
	jne	.L1083
	cmpq	%r13, %rdx
	jne	.L1071
.L1091:
	subl	$1, 20(%r12)
	jmp	.L1071
.L1145:
	movq	%rbp, %rcx
	movq	8(%rsp), %rax
	subq	%r14, %rcx
	addq	%r9, %rcx
	cmpq	$4, %rcx
	movq	%rbp, (%rax)
	ja	.L1051
	subq	%r9, %r14
	cmpq	%rcx, %r9
	movq	%r9, %rax
	jnb	.L1053
.L1052:
	movzbl	(%r14,%rax), %esi
	movb	%sil, 4(%rdx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L1052
.L1053:
	movl	$7, 4(%rsp)
	jmp	.L1037
.L1150:
	cmpq	$0, 72(%rsp)
	je	.L1057
	andl	$2, %edi
	jne	.L1154
.L1057:
	movl	$6, 4(%rsp)
	jmp	.L1037
.L1147:
	cmpq	%r13, %rdx
	jne	.L1083
.L1075:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
.L1153:
	movq	72(%rsp), %rdi
	addq	$1, (%rdi)
	jmp	.L1081
.L1077:
	cmpq	%r13, %rdx
	je	.L1091
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	%r13, %r10
	movq	%r11, %rcx
	jmp	.L1076
.L1149:
	movq	8(%rsp), %rax
	cmpq	%r10, %rdx
	movq	%rbp, (%rax)
	je	.L1075
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	72(%rsp), %rdi
	addq	$1, (%rdi)
	jmp	.L1058
.L1151:
	leaq	__PRETTY_FUNCTION__.11015(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L1144:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L1047:
	leaq	__PRETTY_FUNCTION__.11015(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L1087:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L1051:
	leaq	__PRETTY_FUNCTION__.11015(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L1109:
	movl	%eax, 4(%rsp)
	jmp	.L1069
.L1152:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_ucs2_internal, .-__gconv_transform_ucs2_internal
	.p2align 4,,15
	.globl	__gconv_transform_internal_ucs2
	.type	__gconv_transform_internal_ucs2, @function
__gconv_transform_internal_ucs2:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%rsi), %r15d
	movq	%rdi, 88(%rsp)
	addq	$104, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rdi, 56(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 24(%rsp)
	testb	$1, %r15b
	movq	%r9, 16(%rsp)
	movl	208(%rsp), %ebx
	movq	%rdi, 64(%rsp)
	movq	$0, 48(%rsp)
	jne	.L1156
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 48(%rsp)
	je	.L1156
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 48(%rsp)
.L1156:
	testl	%ebx, %ebx
	jne	.L1316
	movq	8(%rsp), %rax
	movq	24(%rsp), %rcx
	leaq	112(%rsp), %rdx
	testq	%rcx, %rcx
	movq	(%rax), %r14
	movq	%rcx, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r13
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movq	%rax, 72(%rsp)
	movl	216(%rsp), %eax
	testl	%eax, %eax
	jne	.L1317
.L1235:
	xorl	%r11d, %r11d
.L1163:
	leaq	136(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 104(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 80(%rsp)
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.L1194
	addq	(%rax), %r11
.L1194:
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r13, %rbx
	movq	%r14, %rax
	movl	$4, %r10d
	andl	$2, %r15d
.L1195:
	cmpq	%rax, %rbp
	je	.L1196
.L1203:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L1240
	leaq	2(%rbx), %rsi
	cmpq	%rsi, (%rsp)
	jb	.L1241
	movl	(%rax), %ecx
	cmpl	$65535, %ecx
	ja	.L1318
	leal	-55296(%rcx), %edi
	cmpl	$2047, %edi
	jbe	.L1319
	movq	%rdx, %rax
	movw	%cx, (%rbx)
	movq	%rsi, 136(%rsp)
	cmpq	%rax, %rbp
	movq	%rdx, 128(%rsp)
	movq	%rsi, %rbx
	jne	.L1203
	.p2align 4,,10
	.p2align 3
.L1196:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L1320
.L1204:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L1321
	cmpq	%r13, %rbx
	movq	%r11, 40(%rsp)
	jbe	.L1246
	movq	48(%rsp), %r15
	movq	(%r12), %rax
	movl	%r10d, 32(%rsp)
	movq	%r15, %rdi
	movq	%rax, 120(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	32(%rsp), %r9
	movq	96(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r15d
	popq	%rdi
	movl	32(%rsp), %r10d
	je	.L1208
	movq	120(%rsp), %r10
	movq	40(%rsp), %r11
	cmpq	%rbx, %r10
	jne	.L1322
.L1207:
	testl	%r15d, %r15d
	jne	.L1249
.L1222:
	movq	8(%rsp), %rax
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	movq	(%r12), %r13
	movq	(%rax), %r14
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1240:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rsi
	movl	$7, %r10d
	movq	%rax, (%rsi)
	je	.L1204
.L1320:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
.L1155:
	addq	$152, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1241:
	movl	$5, %r10d
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1318:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L1323
	cmpq	$0, 72(%rsp)
	je	.L1245
	testb	$8, 16(%r12)
	jne	.L1324
.L1200:
	testl	%r15d, %r15d
	jne	.L1325
.L1245:
	movl	$6, %r10d
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1208:
	cmpl	$5, %r10d
	movl	%r10d, %r15d
	jne	.L1207
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1319:
	cmpq	$0, 72(%rsp)
	je	.L1245
	testl	%r15d, %r15d
	je	.L1245
	movq	72(%rsp), %rax
	movq	%rdx, 128(%rsp)
	movl	$6, %r10d
	addq	$1, (%rax)
	movq	%rdx, %rax
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	32(%r12), %rbx
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	je	.L1235
	testq	%rcx, %rcx
	jne	.L1326
	cmpl	$4, %edx
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	ja	.L1165
	leaq	120(%rsp), %rdi
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movq	%rdx, %r11
	movq	%rdi, 32(%rsp)
.L1166:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L1166
	movq	%r14, %rcx
	subq	%rax, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbp
	jb	.L1327
	leaq	2(%r13), %r8
	cmpq	%r8, (%rsp)
	movl	$5, %r10d
	jb	.L1155
	leaq	1(%r14), %rax
	leaq	119(%rsp), %r9
.L1174:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r11
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	$3, %r11
	movb	%cl, (%r9,%r11)
	ja	.L1251
	cmpq	%rdi, %rbp
	ja	.L1174
.L1251:
	movq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	movl	120(%rsp), %eax
	cmpl	$65535, %eax
	ja	.L1328
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L1329
	movw	%ax, 0(%r13)
	movq	%r8, 136(%rsp)
.L1314:
	movq	32(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
.L1178:
	subq	32(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L1330
	subq	%rdx, %rax
	movq	8(%rsp), %rdx
	andl	$-8, %esi
	movq	136(%rsp), %r13
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	addq	(%rdx), %rax
	movq	%rax, (%rdx)
	movq	%rax, %r14
	movl	%esi, (%rbx)
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1325:
	movq	72(%rsp), %rdx
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 128(%rsp)
	addq	$1, (%rdx)
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	16(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L1206:
	cmpl	$7, %r10d
	jne	.L1155
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L1155
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1224
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L1226
.L1225:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1225
.L1226:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1246:
	movl	%r10d, %r15d
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	%r11, 32(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	movl	%eax, %r10d
	popq	%r8
	cmpl	$6, %r10d
	popq	%r9
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movq	32(%rsp), %r11
	je	.L1200
	cmpl	$5, %r10d
	jne	.L1195
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	%rdx, 128(%rsp)
	movq	%rdx, %rax
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1210
	movq	(%rsi), %rax
.L1210:
	addq	112(%rsp), %rax
	cmpq	%r11, %rax
	je	.L1331
	movq	8(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r13, %rdx
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r14, (%rax)
	andl	$2, %ebx
.L1212:
	cmpq	%r14, %rbp
	je	.L1213
.L1221:
	leaq	4(%r14), %rax
	cmpq	%rax, %rbp
	jb	.L1213
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L1214
	movl	(%r14), %ecx
	cmpl	$65535, %ecx
	ja	.L1332
	leal	-55296(%rcx), %edi
	cmpl	$2047, %edi
	jbe	.L1333
	movq	%rax, %r14
	movw	%cx, (%rdx)
	movq	%rsi, 136(%rsp)
	cmpq	%r14, %rbp
	movq	%rax, 128(%rsp)
	movq	%rsi, %rdx
	jne	.L1221
.L1213:
	cmpq	120(%rsp), %rdx
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	je	.L1334
.L1229:
	leaq	__PRETTY_FUNCTION__.11241(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L1331:
	movq	8(%rsp), %rdi
	subq	%r10, %rbx
	leaq	(%rbx,%rbx), %rax
	subq	%rax, (%rdi)
	jmp	.L1207
.L1316:
	cmpq	$0, 24(%rsp)
	jne	.L1335
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L1155
	movq	48(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r14
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L1155
.L1249:
	movl	%r15d, %r10d
	jmp	.L1206
.L1337:
	movq	%r10, 32(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r14
	movq	136(%rsp), %rdx
	movq	32(%rsp), %r10
	je	.L1218
	cmpl	$5, %eax
	jne	.L1212
.L1214:
	cmpq	120(%rsp), %rdx
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	jne	.L1229
	cmpq	%rdx, %r13
	jne	.L1207
	subl	$1, 20(%r12)
	jmp	.L1207
.L1332:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L1336
	cmpq	$0, 72(%rsp)
	je	.L1213
	testb	$8, 16(%r12)
	jne	.L1337
.L1218:
	testl	%ebx, %ebx
	je	.L1213
	movq	72(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L1212
.L1333:
	cmpq	$0, 72(%rsp)
	je	.L1213
	testl	%ebx, %ebx
	je	.L1213
	movq	72(%rsp), %rdi
	movq	%rax, 128(%rsp)
	movq	%rax, %r14
	addq	$1, (%rdi)
	jmp	.L1212
.L1327:
	movq	%rbp, %rdx
	movq	8(%rsp), %rsi
	subq	%r14, %rdx
	addq	%rax, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rsi)
	ja	.L1168
	addq	$1, %r14
	cmpq	%rax, %rdx
	jbe	.L1170
.L1171:
	movq	%r14, 128(%rsp)
	movzbl	-1(%r14), %eax
	addq	$1, %r14
	movb	%al, 4(%rbx,%r11)
	addq	$1, %r11
	cmpq	%r11, %rdx
	jne	.L1171
.L1170:
	movl	$7, %r10d
	jmp	.L1155
.L1328:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L1314
	cmpq	$0, 72(%rsp)
	je	.L1236
	testb	$8, %r15b
	jne	.L1338
	andl	$2, %r15d
	movl	$6, %r10d
	movq	32(%rsp), %rax
	je	.L1155
.L1183:
	movq	72(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L1184:
	cmpq	32(%rsp), %rax
	je	.L1236
.L1315:
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jmp	.L1178
.L1329:
	cmpq	$0, 72(%rsp)
	movl	$6, %r10d
	je	.L1155
	andl	$2, %r15d
	je	.L1155
	movq	32(%rsp), %rax
	movq	72(%rsp), %rdi
	addq	$4, %rax
	addq	$1, (%rdi)
	movq	%rax, 128(%rsp)
	jmp	.L1178
.L1336:
	movq	%rax, 128(%rsp)
	movq	%rax, %r14
	jmp	.L1212
.L1236:
	movl	$6, %r10d
	jmp	.L1155
.L1338:
	movq	32(%rsp), %rax
	movq	%r11, 80(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	addq	%r11, %rax
	movq	%rax, 48(%rsp)
	pushq	80(%rsp)
	movq	%rax, %r8
	movq	104(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__GI___gconv_transliterate
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%r14
	movq	80(%rsp), %r11
	je	.L1339
	movq	128(%rsp), %rax
	cmpq	32(%rsp), %rax
	jne	.L1315
	cmpl	$7, %r10d
	jne	.L1189
	movq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 40(%rsp)
	je	.L1340
	movl	(%rbx), %eax
	movq	8(%rsp), %rdi
	movq	%r11, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, (%rdi)
	cmpq	%rdx, %r11
	jle	.L1341
	cmpq	$4, %r11
	ja	.L1342
	orl	%r11d, %eax
	testq	%r11, %r11
	movl	%eax, (%rbx)
	je	.L1170
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L1193:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r11
	jne	.L1193
	jmp	.L1170
.L1168:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L1342:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__GI___assert_fail
.L1341:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__GI___assert_fail
.L1340:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__GI___assert_fail
.L1189:
	testl	%r10d, %r10d
	jne	.L1155
	movq	8(%rsp), %rax
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	movq	(%rax), %r14
	jmp	.L1163
.L1339:
	andb	$2, %r15b
	movq	128(%rsp), %rax
	je	.L1184
	jmp	.L1183
.L1165:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L1326:
	leaq	__PRETTY_FUNCTION__.11241(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L1224:
	leaq	__PRETTY_FUNCTION__.11241(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L1330:
	leaq	__PRETTY_FUNCTION__.11170(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L1335:
	leaq	__PRETTY_FUNCTION__.11241(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L1334:
	leaq	__PRETTY_FUNCTION__.11241(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_ucs2, .-__gconv_transform_internal_ucs2
	.p2align 4,,15
	.globl	__gconv_transform_ucs2reverse_internal
	.type	__gconv_transform_ucs2reverse_internal, @function
__gconv_transform_ucs2reverse_internal:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%r9, %r15
	subq	$104, %rsp
	movq	%rax, 48(%rsp)
	leaq	48(%rsi), %rax
	movl	16(%rsi), %esi
	movq	%rdx, (%rsp)
	movq	%r8, 24(%rsp)
	movl	160(%rsp), %ebx
	movq	%rax, 56(%rsp)
	testb	$1, %sil
	movq	$0, 8(%rsp)
	jne	.L1344
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 8(%rsp)
	je	.L1344
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 8(%rsp)
.L1344:
	testl	%ebx, %ebx
	jne	.L1444
	movq	(%rsp), %rax
	movq	24(%rsp), %rdi
	leaq	80(%rsp), %rdx
	movl	168(%rsp), %r8d
	movq	8(%r12), %r11
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	testq	%r15, %r15
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 80(%rsp)
	cmovne	%rdx, %rax
	testl	%r8d, %r8d
	movq	%rax, 72(%rsp)
	jne	.L1445
.L1401:
	xorl	%r10d, %r10d
.L1351:
	leaq	88(%rsp), %rax
	movq	%rax, 64(%rsp)
	movq	%r11, %rax
	movq	%r14, %r11
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1366:
	testq	%r15, %r15
	je	.L1367
	addq	(%r15), %r10
.L1367:
	cmpq	%r11, %rbp
	je	.L1403
	leaq	2(%r11), %rdx
	cmpq	%rbp, %rdx
	ja	.L1404
	leaq	4(%r13), %rcx
	movq	%r13, %rbx
	cmpq	%rcx, %r14
	jb	.L1405
	andl	$2, %esi
.L1369:
	movzwl	-2(%rdx), %eax
	leaq	-2(%rdx), %r8
	rolw	$8, %ax
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L1446
	movzwl	%ax, %eax
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L1371:
	cmpq	%rdx, %rbp
	je	.L1443
	leaq	2(%rdx), %rax
	cmpq	%rax, %rbp
	jb	.L1406
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L1407
	movq	%rax, %rdx
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1446:
	cmpq	$0, 72(%rsp)
	je	.L1409
	testl	%esi, %esi
	jne	.L1447
.L1409:
	movq	%r8, %rdx
	movl	$6, 20(%rsp)
.L1368:
	cmpq	$0, 24(%rsp)
	movq	(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L1448
.L1373:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L1449
	cmpq	%r13, %rbx
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	jbe	.L1375
	movq	8(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 88(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movq	%r15, %r9
	pushq	%rsi
	pushq	$0
	movq	80(%rsp), %rdx
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	popq	%rdx
	popq	%rcx
	je	.L1376
	movq	88(%rsp), %rdx
	movq	32(%rsp), %r11
	movq	40(%rsp), %r10
	cmpq	%rdx, %rbx
	jne	.L1450
.L1377:
	testl	%eax, %eax
	jne	.L1415
.L1388:
	movq	(%rsp), %rax
	movq	80(%rsp), %r10
	movl	16(%r12), %esi
	movq	(%r12), %r13
	movq	(%rax), %r11
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1406:
	cmpq	$0, 24(%rsp)
	movq	(%rsp), %rax
	movl	$7, 20(%rsp)
	movq	%rdx, (%rax)
	je	.L1373
.L1448:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
.L1343:
	movl	20(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1376:
	cmpl	$5, 20(%rsp)
	je	.L1388
.L1375:
	movl	168(%rsp), %eax
	testl	%eax, %eax
	je	.L1343
	cmpl	$7, 20(%rsp)
	jne	.L1343
	movq	(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1390
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L1392
.L1391:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1391
.L1392:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1407:
	movl	$5, 20(%rsp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	32(%r12), %rdx
	movl	(%rdx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.L1401
	testq	%rdi, %rdi
	jne	.L1451
	cmpl	$4, %ecx
	ja	.L1353
	movzbl	4(%rdx), %edi
	cmpl	$1, %ecx
	movb	%dil, 88(%rsp)
	je	.L1354
	movzbl	5(%rdx), %edi
	movl	$2, %r9d
	movb	%dil, 89(%rsp)
	movq	%r14, %rdi
.L1355:
	cmpq	%rdi, %rbp
	jb	.L1452
	leaq	4(%r13), %r8
	cmpq	%r8, %r11
	jnb	.L1360
	movl	$5, 20(%rsp)
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	%r13, %rbx
	movq	%rbp, %rdx
.L1443:
	movl	$4, 20(%rsp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	%r13, %rbx
	movq	%r11, %rdx
	movl	$7, 20(%rsp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	%rbx, (%r12)
	movq	80(%rsp), %rax
	addq	%rax, (%r15)
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	%r11, %rdx
	movl	$5, 20(%rsp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1450:
	xorl	%ecx, %ecx
	testq	%r15, %r15
	je	.L1378
	movq	(%r15), %rcx
.L1378:
	addq	80(%rsp), %rcx
	cmpq	%r10, %rcx
	je	.L1453
	movq	(%rsp), %rsi
	cmpq	%r11, %rbp
	movq	%r11, (%rsi)
	je	.L1411
	leaq	2(%r11), %rsi
	cmpq	%rsi, %rbp
	jb	.L1412
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rdx
	jb	.L1381
	movl	16(%r12), %r9d
	movq	%r13, %r10
	andl	$2, %r9d
.L1382:
	movzwl	-2(%rsi), %ecx
	leaq	-2(%rsi), %rbx
	rolw	$8, %cx
	leal	10240(%rcx), %r8d
	cmpw	$2047, %r8w
	jbe	.L1454
	movzwl	%cx, %ecx
	movl	%ecx, (%r10)
	movq	%rdi, %r10
.L1385:
	cmpq	%rsi, %rbp
	je	.L1380
	leaq	2(%rsi), %rcx
	cmpq	%rcx, %rbp
	jb	.L1380
	leaq	4(%r10), %rdi
	cmpq	%rdi, %rdx
	jb	.L1383
	movq	%rcx, %rsi
	jmp	.L1382
.L1453:
	subq	%rdx, %rbx
	movq	(%rsp), %rsi
	movq	%rbx, %rdx
	shrq	$63, %rdx
	addq	%rbx, %rdx
	sarq	%rdx
	subq	%rdx, (%rsi)
	jmp	.L1377
.L1354:
	leaq	1(%r14), %rdi
	movl	$1, %r9d
	jmp	.L1355
.L1360:
	movzbl	(%r14), %edi
	movb	%dil, 88(%rsp,%r9)
	movzwl	88(%rsp), %edi
	rolw	$8, %di
	leal	10240(%rdi), %r9d
	cmpw	$2047, %r9w
	jbe	.L1455
	movzwl	%di, %edi
	movl	%edi, 0(%r13)
	movl	(%rdx), %eax
	movq	%r8, %r13
	movl	%eax, %ecx
	andl	$7, %ecx
.L1364:
	cmpl	$1, %ecx
	movslq	%ecx, %rsi
	jg	.L1456
	subq	%rsi, %r14
	movq	(%rsp), %rsi
	andl	$-8, %eax
	addq	$2, %r14
	movq	80(%rsp), %r10
	movq	%r14, (%rsi)
	movl	%eax, (%rdx)
	movl	16(%r12), %esi
	jmp	.L1351
.L1444:
	cmpq	$0, 24(%rsp)
	jne	.L1457
	movq	32(%r12), %rax
	movl	$0, 20(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L1343
	movq	8(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	168(%rsp), %eax
	movq	%r15, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	call	*%r14
	movl	%eax, 36(%rsp)
	popq	%r9
	popq	%r10
	jmp	.L1343
.L1454:
	cmpq	$0, 72(%rsp)
	je	.L1414
	testl	%r9d, %r9d
	jne	.L1458
.L1414:
	movq	%rbx, %rsi
.L1380:
	movq	(%rsp), %rax
	cmpq	%r10, %rdx
	movq	%rsi, (%rax)
	je	.L1459
.L1394:
	leaq	__PRETTY_FUNCTION__.11396(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L1383:
	movq	(%rsp), %rdi
	cmpq	%rdx, %r10
	movq	%rsi, (%rdi)
	jne	.L1394
	cmpq	%r13, %r10
	jne	.L1377
.L1396:
	subl	$1, 20(%r12)
	jmp	.L1377
.L1452:
	movq	%rbp, %rcx
	movq	(%rsp), %rax
	subq	%r14, %rcx
	addq	%r9, %rcx
	cmpq	$4, %rcx
	movq	%rbp, (%rax)
	ja	.L1357
	subq	%r9, %r14
	cmpq	%rcx, %r9
	movq	%r9, %rax
	jnb	.L1359
.L1358:
	movzbl	(%r14,%rax), %esi
	movb	%sil, 4(%rdx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L1358
.L1359:
	movl	$7, 20(%rsp)
	jmp	.L1343
.L1455:
	cmpq	$0, 72(%rsp)
	je	.L1363
	andl	$2, %esi
	jne	.L1460
.L1363:
	movl	$6, 20(%rsp)
	jmp	.L1343
.L1458:
	movq	72(%rsp), %rdi
	addq	$1, (%rdi)
	jmp	.L1385
.L1381:
	cmpq	%r13, %rdx
	je	.L1396
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	%r13, %r10
	movq	%r11, %rsi
	jmp	.L1380
.L1460:
	movq	72(%rsp), %rsi
	addq	$1, (%rsi)
	jmp	.L1364
.L1411:
	movq	%r13, %r10
	movq	%rbp, %rsi
	jmp	.L1380
.L1357:
	leaq	__PRETTY_FUNCTION__.11325(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L1390:
	leaq	__PRETTY_FUNCTION__.11396(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L1457:
	leaq	__PRETTY_FUNCTION__.11396(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L1456:
	leaq	__PRETTY_FUNCTION__.11325(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L1451:
	leaq	__PRETTY_FUNCTION__.11396(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L1353:
	leaq	__PRETTY_FUNCTION__.11325(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L1415:
	movl	%eax, 20(%rsp)
	jmp	.L1375
.L1459:
	leaq	__PRETTY_FUNCTION__.11396(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_ucs2reverse_internal, .-__gconv_transform_ucs2reverse_internal
	.p2align 4,,15
	.globl	__gconv_transform_internal_ucs2reverse
	.type	__gconv_transform_internal_ucs2reverse, @function
__gconv_transform_internal_ucs2reverse:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r15d
	movq	%rsi, 64(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 88(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	testb	$1, %r15b
	movq	%r9, 24(%rsp)
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 56(%rsp)
	jne	.L1462
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 56(%rsp)
	je	.L1462
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 56(%rsp)
.L1462:
	testl	%ebx, %ebx
	jne	.L1623
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	112(%rsp), %rdx
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 24(%rsp)
	movq	(%rax), %r13
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, 8(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movq	%rax, 80(%rsp)
	movl	216(%rsp), %eax
	testl	%eax, %eax
	jne	.L1624
.L1541:
	xorl	%r11d, %r11d
.L1469:
	leaq	136(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L1500
	addq	(%rax), %r11
.L1500:
	movl	$4, %r10d
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r13, %rbx
	movq	%r14, %rdx
	andl	$2, %r15d
	movl	%r10d, %eax
.L1501:
	cmpq	%rbp, %rdx
	je	.L1622
.L1509:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rbp
	jb	.L1546
	leaq	2(%rbx), %rdi
	cmpq	%rdi, 8(%rsp)
	jb	.L1547
	movl	(%rdx), %ecx
	cmpl	$65535, %ecx
	ja	.L1625
	leal	-55296(%rcx), %r8d
	cmpl	$2047, %r8d
	jbe	.L1626
	movq	%rsi, %rdx
	rolw	$8, %cx
	cmpq	%rbp, %rdx
	movw	%cx, (%rbx)
	movq	%rdi, 136(%rsp)
	movq	%rsi, 128(%rsp)
	movq	%rdi, %rbx
	jne	.L1509
.L1622:
	cmpq	$0, 32(%rsp)
	movl	%eax, %r10d
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	je	.L1510
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L1461:
	addq	$152, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1546:
	movl	$7, %r10d
.L1502:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L1627
.L1510:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L1628
	cmpq	%r13, %rbx
	movq	%r11, 48(%rsp)
	jbe	.L1552
	movq	56(%rsp), %r15
	movq	(%r12), %rax
	movl	%r10d, 40(%rsp)
	movq	%r15, %rdi
	movq	%rax, 120(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	40(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r15d
	popq	%rdi
	movl	40(%rsp), %r10d
	je	.L1514
	movq	120(%rsp), %r10
	movq	48(%rsp), %r11
	cmpq	%rbx, %r10
	jne	.L1629
.L1513:
	testl	%r15d, %r15d
	jne	.L1555
.L1528:
	movq	16(%rsp), %rax
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	movq	(%r12), %r13
	movq	(%rax), %r14
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	$5, %r10d
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1625:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L1630
	cmpq	$0, 80(%rsp)
	je	.L1551
	testb	$8, 16(%r12)
	jne	.L1631
.L1506:
	testl	%r15d, %r15d
	jne	.L1632
.L1551:
	movl	$6, %r10d
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1514:
	cmpl	$5, %r10d
	movl	%r10d, %r15d
	jne	.L1513
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1626:
	cmpq	$0, 80(%rsp)
	je	.L1551
	testl	%r15d, %r15d
	je	.L1551
	movq	80(%rsp), %rdi
	movq	%rsi, 128(%rsp)
	movq	%rsi, %rdx
	addq	$1, (%rdi)
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	32(%r12), %rbx
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	je	.L1541
	testq	%rdi, %rdi
	jne	.L1633
	cmpl	$4, %edx
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	ja	.L1471
	leaq	120(%rsp), %rdi
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movq	%rdx, %r11
	movq	%rdi, 40(%rsp)
.L1472:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L1472
	movq	%r14, %rcx
	subq	%rax, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbp
	jb	.L1634
	leaq	2(%r13), %rdi
	cmpq	%rdi, 8(%rsp)
	movl	$5, %r10d
	jb	.L1461
	leaq	1(%r14), %rax
	leaq	119(%rsp), %r9
.L1480:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r11
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	$3, %r11
	movb	%cl, (%r9,%r11)
	ja	.L1557
	cmpq	%r8, %rbp
	ja	.L1480
.L1557:
	movq	40(%rsp), %rax
	movq	%rax, 128(%rsp)
	movl	120(%rsp), %eax
	cmpl	$65535, %eax
	ja	.L1635
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L1636
	rolw	$8, %ax
	movw	%ax, 0(%r13)
	movq	%rdi, 136(%rsp)
.L1620:
	movq	40(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
.L1484:
	subq	40(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L1637
	movq	16(%rsp), %rdi
	subq	%rdx, %rax
	andl	$-8, %esi
	movq	136(%rsp), %r13
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r14
	movl	%esi, (%rbx)
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	80(%rsp), %rax
	addq	$4, %rdx
	movq	%rdx, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	24(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rsi)
.L1512:
	cmpl	$7, %r10d
	jne	.L1461
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L1461
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1530
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L1532
.L1531:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1531
.L1532:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1552:
	movl	%r10d, %r15d
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	%r11, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	popq	%r8
	cmpl	$6, %eax
	popq	%r9
	movq	128(%rsp), %rdx
	movq	136(%rsp), %rbx
	movq	40(%rsp), %r11
	je	.L1506
	cmpl	$5, %eax
	jne	.L1501
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	%rsi, 128(%rsp)
	movq	%rsi, %rdx
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	24(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1516
	movq	(%rsi), %rax
.L1516:
	addq	112(%rsp), %rax
	cmpq	%r11, %rax
	je	.L1638
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r13, %rcx
	movq	%r14, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r14, (%rax)
	andl	$2, %ebx
.L1518:
	cmpq	%r14, %rbp
	je	.L1519
.L1527:
	leaq	4(%r14), %rdx
	cmpq	%rdx, %rbp
	jb	.L1519
	leaq	2(%rcx), %rsi
	cmpq	%rsi, %r10
	jb	.L1520
	movl	(%r14), %eax
	cmpl	$65535, %eax
	ja	.L1639
	leal	-55296(%rax), %edi
	cmpl	$2047, %edi
	jbe	.L1640
	movq	%rdx, %r14
	rolw	$8, %ax
	cmpq	%r14, %rbp
	movw	%ax, (%rcx)
	movq	%rsi, 136(%rsp)
	movq	%rdx, 128(%rsp)
	movq	%rsi, %rcx
	jne	.L1527
.L1519:
	cmpq	120(%rsp), %rcx
	movq	16(%rsp), %rax
	movq	%r14, (%rax)
	je	.L1641
.L1535:
	leaq	__PRETTY_FUNCTION__.11551(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$746, %edx
	call	__GI___assert_fail
.L1638:
	movq	16(%rsp), %rsi
	subq	%r10, %rbx
	leaq	(%rbx,%rbx), %rax
	subq	%rax, (%rsi)
	jmp	.L1513
.L1623:
	cmpq	$0, 32(%rsp)
	jne	.L1642
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L1461
	movq	56(%rsp), %r14
	movq	%r14, %rdi
	call	__GI__dl_mcount_wrapper_check
	movl	216(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	40(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r14
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L1461
.L1555:
	movl	%r15d, %r10d
	jmp	.L1512
.L1644:
	movq	%r10, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	104(%rsp), %rdi
	movq	(%rax), %rdx
	call	__GI___gconv_transliterate
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r14
	movq	136(%rsp), %rcx
	movq	40(%rsp), %r10
	je	.L1524
	cmpl	$5, %eax
	jne	.L1518
.L1520:
	cmpq	120(%rsp), %rcx
	movq	16(%rsp), %rax
	movq	%r14, (%rax)
	jne	.L1535
	cmpq	%r13, %rcx
	jne	.L1513
	subl	$1, 20(%r12)
	jmp	.L1513
.L1639:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L1643
	cmpq	$0, 80(%rsp)
	je	.L1519
	testb	$8, 16(%r12)
	jne	.L1644
.L1524:
	testl	%ebx, %ebx
	je	.L1519
	movq	80(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L1518
.L1640:
	cmpq	$0, 80(%rsp)
	je	.L1519
	testl	%ebx, %ebx
	je	.L1519
	movq	80(%rsp), %rax
	movq	%rdx, 128(%rsp)
	movq	%rdx, %r14
	addq	$1, (%rax)
	jmp	.L1518
.L1634:
	movq	%rbp, %rdx
	movq	16(%rsp), %rsi
	subq	%r14, %rdx
	addq	%rax, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rsi)
	ja	.L1474
	addq	$1, %r14
	cmpq	%rax, %rdx
	jbe	.L1476
.L1477:
	movq	%r14, 128(%rsp)
	movzbl	-1(%r14), %eax
	addq	$1, %r14
	movb	%al, 4(%rbx,%r11)
	addq	$1, %r11
	cmpq	%r11, %rdx
	jne	.L1477
.L1476:
	movl	$7, %r10d
	jmp	.L1461
.L1635:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L1620
	cmpq	$0, 80(%rsp)
	je	.L1542
	testb	$8, %r15b
	jne	.L1645
	andl	$2, %r15d
	movl	$6, %r10d
	movq	40(%rsp), %rax
	je	.L1461
.L1489:
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L1490:
	cmpq	40(%rsp), %rax
	je	.L1542
.L1621:
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jmp	.L1484
.L1636:
	cmpq	$0, 80(%rsp)
	movl	$6, %r10d
	je	.L1461
	andl	$2, %r15d
	je	.L1461
	movq	40(%rsp), %rax
	movq	80(%rsp), %rdi
	addq	$4, %rax
	addq	$1, (%rdi)
	movq	%rax, 128(%rsp)
	jmp	.L1484
.L1643:
	movq	%rdx, 128(%rsp)
	movq	%rdx, %r14
	jmp	.L1518
.L1542:
	movl	$6, %r10d
	jmp	.L1461
.L1645:
	movq	40(%rsp), %rax
	movq	%r11, 96(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	addq	%r11, %rax
	movq	%rax, 56(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	104(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__GI___gconv_transliterate
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%r14
	movq	96(%rsp), %r11
	je	.L1646
	movq	128(%rsp), %rax
	cmpq	40(%rsp), %rax
	jne	.L1621
	cmpl	$7, %r10d
	jne	.L1495
	movq	40(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 48(%rsp)
	je	.L1647
	movl	(%rbx), %eax
	movq	%r11, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	16(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r11
	jle	.L1648
	cmpq	$4, %r11
	ja	.L1649
	orl	%r11d, %eax
	testq	%r11, %r11
	movl	%eax, (%rbx)
	je	.L1476
	movq	40(%rsp), %rcx
	xorl	%eax, %eax
.L1499:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r11
	jne	.L1499
	jmp	.L1476
.L1474:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__GI___assert_fail
.L1649:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__GI___assert_fail
.L1648:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__GI___assert_fail
.L1647:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__GI___assert_fail
.L1495:
	testl	%r10d, %r10d
	jne	.L1461
	movq	16(%rsp), %rax
	movq	112(%rsp), %r11
	movl	16(%r12), %r15d
	movq	(%rax), %r14
	jmp	.L1469
.L1646:
	andb	$2, %r15b
	movq	128(%rsp), %rax
	je	.L1490
	jmp	.L1489
.L1471:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__GI___assert_fail
.L1633:
	leaq	__PRETTY_FUNCTION__.11551(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$564, %edx
	call	__GI___assert_fail
.L1530:
	leaq	__PRETTY_FUNCTION__.11551(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$799, %edx
	call	__GI___assert_fail
.L1637:
	leaq	__PRETTY_FUNCTION__.11480(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__GI___assert_fail
.L1642:
	leaq	__PRETTY_FUNCTION__.11551(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$420, %edx
	call	__GI___assert_fail
.L1641:
	leaq	__PRETTY_FUNCTION__.11551(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$747, %edx
	call	__GI___assert_fail
	.size	__gconv_transform_internal_ucs2reverse, .-__gconv_transform_internal_ucs2reverse
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	__PRETTY_FUNCTION__.11480, @object
	.size	__PRETTY_FUNCTION__.11480, 33
__PRETTY_FUNCTION__.11480:
	.string	"internal_ucs2reverse_loop_single"
	.align 32
	.type	__PRETTY_FUNCTION__.11551, @object
	.size	__PRETTY_FUNCTION__.11551, 39
__PRETTY_FUNCTION__.11551:
	.string	"__gconv_transform_internal_ucs2reverse"
	.align 32
	.type	__PRETTY_FUNCTION__.11325, @object
	.size	__PRETTY_FUNCTION__.11325, 33
__PRETTY_FUNCTION__.11325:
	.string	"ucs2reverse_internal_loop_single"
	.align 32
	.type	__PRETTY_FUNCTION__.11396, @object
	.size	__PRETTY_FUNCTION__.11396, 39
__PRETTY_FUNCTION__.11396:
	.string	"__gconv_transform_ucs2reverse_internal"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11170, @object
	.size	__PRETTY_FUNCTION__.11170, 26
__PRETTY_FUNCTION__.11170:
	.string	"internal_ucs2_loop_single"
	.section	.rodata.str1.32
	.align 32
	.type	__PRETTY_FUNCTION__.11241, @object
	.size	__PRETTY_FUNCTION__.11241, 32
__PRETTY_FUNCTION__.11241:
	.string	"__gconv_transform_internal_ucs2"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.11015, @object
	.size	__PRETTY_FUNCTION__.11015, 26
__PRETTY_FUNCTION__.11015:
	.string	"ucs2_internal_loop_single"
	.section	.rodata.str1.32
	.align 32
	.type	__PRETTY_FUNCTION__.11086, @object
	.size	__PRETTY_FUNCTION__.11086, 32
__PRETTY_FUNCTION__.11086:
	.string	"__gconv_transform_ucs2_internal"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10849, @object
	.size	__PRETTY_FUNCTION__.10849, 26
__PRETTY_FUNCTION__.10849:
	.string	"utf8_internal_loop_single"
	.section	.rodata
	.type	inmask.10838, @object
	.size	inmask.10838, 5
inmask.10838:
	.byte	-64
	.byte	-32
	.byte	-16
	.byte	-8
	.byte	-4
	.section	.rodata.str1.32
	.align 32
	.type	__PRETTY_FUNCTION__.10931, @object
	.size	__PRETTY_FUNCTION__.10931, 32
__PRETTY_FUNCTION__.10931:
	.string	"__gconv_transform_utf8_internal"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10666, @object
	.size	__PRETTY_FUNCTION__.10666, 26
__PRETTY_FUNCTION__.10666:
	.string	"internal_utf8_loop_single"
	.section	.rodata.str1.32
	.align 32
	.type	__PRETTY_FUNCTION__.10744, @object
	.size	__PRETTY_FUNCTION__.10744, 32
__PRETTY_FUNCTION__.10744:
	.string	"__gconv_transform_internal_utf8"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10505, @object
	.size	__PRETTY_FUNCTION__.10505, 27
__PRETTY_FUNCTION__.10505:
	.string	"internal_ascii_loop_single"
	.section	.rodata.str1.32
	.align 32
	.type	__PRETTY_FUNCTION__.10575, @object
	.size	__PRETTY_FUNCTION__.10575, 33
__PRETTY_FUNCTION__.10575:
	.string	"__gconv_transform_internal_ascii"
	.align 32
	.type	__PRETTY_FUNCTION__.10422, @object
	.size	__PRETTY_FUNCTION__.10422, 33
__PRETTY_FUNCTION__.10422:
	.string	"__gconv_transform_ascii_internal"
	.align 32
	.type	__PRETTY_FUNCTION__.10300, @object
	.size	__PRETTY_FUNCTION__.10300, 34
__PRETTY_FUNCTION__.10300:
	.string	"__gconv_transform_ucs4le_internal"
	.align 32
	.type	__PRETTY_FUNCTION__.10192, @object
	.size	__PRETTY_FUNCTION__.10192, 34
__PRETTY_FUNCTION__.10192:
	.string	"__gconv_transform_internal_ucs4le"
	.align 32
	.type	__PRETTY_FUNCTION__.10091, @object
	.size	__PRETTY_FUNCTION__.10091, 32
__PRETTY_FUNCTION__.10091:
	.string	"__gconv_transform_ucs4_internal"
	.align 32
	.type	__PRETTY_FUNCTION__.9984, @object
	.size	__PRETTY_FUNCTION__.9984, 32
__PRETTY_FUNCTION__.9984:
	.string	"__gconv_transform_internal_ucs4"
