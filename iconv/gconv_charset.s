	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/TRANSLIT"
.LC1:
	.string	",TRANSLIT"
.LC2:
	.string	"/IGNORE"
.LC3:
	.string	",IGNORE"
	.text
	.p2align 4,,15
	.type	gconv_parse_code, @function
gconv_parse_code:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movb	$0, 8(%rdi)
	movb	$0, 9(%rdi)
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rbx), %rbp
	movq	%rbp, %rdi
	call	strlen
	testq	%rax, %rax
	je	.L2
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movsbq	-1(%rbp,%rax), %rsi
	movq	%fs:(%rdx), %rdi
	leaq	-1(%rax), %rdx
	movq	%rsi, %rcx
	testb	$32, 1(%rdi,%rsi,2)
	jne	.L30
	cmpb	$44, %sil
	sete	%sil
	cmpb	$47, %cl
	sete	%cl
	orb	%cl, %sil
	jne	.L30
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L6:
	movsbq	-1(%rbp,%rdx), %rcx
	leaq	-1(%rdx), %rsi
	testb	$32, 1(%rdi,%rcx,2)
	movq	%rcx, %rax
	jne	.L20
	cmpb	$47, %cl
	sete	%cl
	cmpb	$44, %al
	sete	%al
	orb	%al, %cl
	je	.L4
.L20:
	movq	%rsi, %rdx
.L30:
	testq	%rdx, %rdx
	jne	.L6
.L2:
	movb	$0, 0(%rbp)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L38:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L4:
	movb	$0, 0(%rbp,%rdx)
	movq	(%rbx), %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L1
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$44, %al
	je	.L36
	cmpb	$47, %al
	jne	.L7
	addl	$1, %ecx
.L36:
	movq	%rdx, %rbp
.L7:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L10
	cmpl	$1, %ecx
	jle	.L1
	testq	%rbp, %rbp
	je	.L1
	leaq	_nl_C_locobj(%rip), %rdx
	leaq	.LC0(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp_l
	testl	%eax, %eax
	jne	.L12
.L14:
	movb	$1, 8(%rbx)
.L13:
	leaq	_nl_C_locobj(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp_l
	testl	%eax, %eax
	jne	.L15
.L17:
	movb	$1, 9(%rbx)
.L16:
	movb	$0, 0(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	_nl_C_locobj(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp_l
	testl	%eax, %eax
	je	.L14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	_nl_C_locobj(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp_l
	testl	%eax, %eax
	je	.L17
	jmp	.L16
	.size	gconv_parse_code, .-gconv_parse_code
	.p2align 4,,15
	.globl	__gconv_create_spec
	.hidden	__gconv_create_spec
	.type	__gconv_create_spec, @function
__gconv_create_spec:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$32, %rsp
	call	__strdup
	movq	%r13, %rdi
	movq	%rax, (%rsp)
	call	__strdup
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	(%rsp), %r12
	je	.L59
	testq	%r12, %r12
	je	.L59
	movq	%rsp, %rdi
	call	gconv_parse_code
	leaq	16(%rsp), %rdi
	call	gconv_parse_code
	movzbl	24(%rsp), %eax
	movq	%rbp, %rdi
	movb	%al, 16(%rbx)
	movzbl	25(%rsp), %eax
	movb	%al, 17(%rbx)
	call	strlen
	leaq	3(%rax), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, (%rbx)
	je	.L71
	movq	%r13, %rdi
	call	strlen
	leaq	3(%rax), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	movq	(%rsp), %r12
	je	.L72
	movzbl	(%r12), %esi
	testb	%sil, %sil
	je	.L73
	movq	104+_nl_C_locobj(%rip), %r11
	movq	120+_nl_C_locobj(%rip), %r13
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	movabsq	$2251799813701639, %r10
	.p2align 4,,10
	.p2align 3
.L49:
	movsbq	%sil, %r8
	leal	-44(%rsi), %ecx
	movzwl	(%r11,%r8,2), %edx
	shrw	$3, %dx
	andl	$1, %edx
	cmpb	$51, %cl
	ja	.L45
	movq	%r10, %r14
	shrq	%cl, %r14
	movq	%r14, %rcx
	andl	$1, %ecx
	orl	%ecx, %edx
.L45:
	testb	%dl, %dl
	leal	1(%r9), %ecx
	je	.L46
	movl	0(%r13,%r8,4), %edx
	addq	$1, %rbp
	movb	%dl, -1(%rbp)
.L47:
	addq	$1, %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L49
	cmpl	$1, %r9d
	jg	.L48
	cmpl	$1, %ecx
	leaq	1(%rbp), %rdx
	movb	$47, 0(%rbp)
	jne	.L60
.L44:
	leaq	1(%rdx), %rbp
	movb	$47, (%rdx)
.L48:
	movq	16(%rsp), %rdi
	movb	$0, 0(%rbp)
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L74
	movq	104+_nl_C_locobj(%rip), %r11
	movq	120+_nl_C_locobj(%rip), %rbp
	xorl	%r9d, %r9d
	movabsq	$2251799813701639, %r10
	.p2align 4,,10
	.p2align 3
.L57:
	movsbq	%sil, %r8
	leal	-44(%rsi), %ecx
	movzwl	(%r11,%r8,2), %edx
	shrw	$3, %dx
	andl	$1, %edx
	cmpb	$51, %cl
	ja	.L53
	movq	%r10, %r14
	shrq	%cl, %r14
	movq	%r14, %rcx
	andl	$1, %ecx
	orl	%ecx, %edx
.L53:
	testb	%dl, %dl
	leal	1(%r9), %ecx
	je	.L54
	movl	0(%rbp,%r8,4), %edx
	addq	$1, %rax
	movb	%dl, -1(%rax)
.L55:
	addq	$1, %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L57
	cmpl	$1, %r9d
	jg	.L56
	cmpl	$1, %ecx
	leaq	1(%rax), %rdx
	movb	$47, (%rax)
	jne	.L61
.L52:
	leaq	1(%rdx), %rax
	movb	$47, (%rdx)
.L56:
	movb	$0, (%rax)
.L40:
	movq	%r12, %rdi
	call	free@PLT
	movq	16(%rsp), %rdi
	call	free@PLT
	addq	$32, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	cmpb	$47, %sil
	jne	.L55
	cmpl	$3, %ecx
	je	.L56
	leal	2(%r9), %edx
	movb	$47, (%rax)
	movl	%ecx, %r9d
	addq	$1, %rax
	movl	%edx, %ecx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L46:
	cmpb	$47, %sil
	jne	.L47
	cmpl	$3, %ecx
	je	.L48
	leal	2(%r9), %edx
	movb	$47, 0(%rbp)
	movl	%ecx, %r9d
	addq	$1, %rbp
	movl	%edx, %ecx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%ebx, %ebx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rdx, %rbp
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%rdx, %rax
	jmp	.L56
.L71:
	movq	(%rsp), %r12
	xorl	%ebx, %ebx
	jmp	.L40
.L73:
	leaq	1(%rbp), %rdx
	movb	$47, 0(%rbp)
	jmp	.L44
.L74:
	leaq	1(%rax), %rdx
	movb	$47, (%rax)
	jmp	.L52
.L72:
	movq	%rbp, %rdi
	call	free@PLT
	movq	$0, (%rbx)
	xorl	%ebx, %ebx
	jmp	.L40
	.size	__gconv_create_spec, .-__gconv_create_spec
	.p2align 4,,15
	.globl	__gconv_destroy_spec
	.hidden	__gconv_destroy_spec
	.type	__gconv_destroy_spec, @function
__gconv_destroy_spec:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %rdi
	popq	%rbx
	jmp	free@PLT
	.size	__gconv_destroy_spec, .-__gconv_destroy_spec
	.hidden	__strdup
	.hidden	__strcasecmp_l
	.hidden	_nl_C_locobj
	.hidden	strlen
