	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gconv.c"
.LC1:
	.string	"irreversible != NULL"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"outbuf != NULL && *outbuf != NULL"
	.text
	.p2align 4,,15
	.globl	__gconv
	.hidden	__gconv
	.type	__gconv, @function
__gconv:
	cmpq	$-1, %rdi
	je	.L15
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%r9, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rbp
	subq	$40, %rsp
	movq	(%rdi), %rax
	movq	%rcx, 24(%rsp)
	subq	$1, %rax
	testq	%r9, %r9
	movq	%rax, 16(%rsp)
	je	.L35
	movq	24(%rsp), %rax
	xorl	%edx, %edx
	movq	$0, (%r9)
	testq	%rax, %rax
	je	.L4
	movq	(%rax), %rdx
.L4:
	movq	16(%rsp), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%r15, %rax
	movq	%rdx, 16(%rax)
	movq	%r8, 24(%rax)
	movq	8(%r15), %rax
	cmpq	$0, (%rax)
	movq	40(%rax), %rbx
	je	.L5
#APP
# 52 "gconv.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L5:
	testq	%rbp, %rbp
	je	.L6
	movq	0(%rbp), %r14
	testq	%r14, %r14
	je	.L6
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L11
	cmpq	$0, (%rax)
	je	.L11
	leaq	16(%r15), %rax
	movq	%rax, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L36:
	movq	0(%rbp), %rdx
	cmpq	%r14, %rdx
	je	.L13
	movq	8(%r15), %rcx
	movslq	72(%rcx), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %r12
	jb	.L13
	movq	%rdx, %r14
.L14:
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check
	movq	8(%r15), %rdi
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r12, %rcx
	pushq	$0
	movq	%rbp, %rdx
	movq	24(%rsp), %rsi
	movq	%r13, %r9
	call	*%rbx
	cmpl	$4, %eax
	popq	%rdx
	popq	%rcx
	je	.L36
.L13:
	movq	24(%rsp), %rsi
	cmpq	$0, (%rsi)
	je	.L1
	movq	16(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rdx
	salq	$4, %rdx
	movq	16(%r15,%rdx), %rdx
	movq	%rdx, (%rsi)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check
	movq	16(%rsp), %rax
	movq	8(%r15), %rdi
	leaq	16(%r15), %rsi
	movq	%r13, %r9
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	cmpq	$0, 16(%r15,%rax)
	pushq	$0
	sete	%al
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movzbl	%al, %eax
	xorl	%edx, %edx
	addl	$1, %eax
	pushq	%rax
	call	*%rbx
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	jne	.L9
	leaq	36(%r15), %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$0, (%rcx)
	addq	$1, %rdx
	addq	$48, %rcx
	cmpq	%rdx, 16(%rsp)
	jnb	.L10
.L9:
	cmpq	$0, 24(%rsp)
	jne	.L13
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$8, %eax
	ret
.L11:
	leaq	__PRETTY_FUNCTION__.9147(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$73, %edx
	call	__assert_fail
.L35:
	leaq	__PRETTY_FUNCTION__.9147(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$43, %edx
	call	__assert_fail
	.size	__gconv, .-__gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9147, @object
	.size	__PRETTY_FUNCTION__.9147, 8
__PRETTY_FUNCTION__.9147:
	.string	"__gconv"
	.hidden	__assert_fail
	.hidden	_dl_mcount_wrapper_check
