	.text
	.p2align 4,,15
	.type	known_compare, @function
known_compare:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	strcmp
	.size	known_compare, .-known_compare
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	do_release_all, @function
do_release_all:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	__libc_dlclose
.L4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.size	do_release_all, .-do_release_all
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	subq	$8, %rsp
	movq	loaded(%rip), %rdi
	leaq	do_release_all(%rip), %rsi
	call	__tdestroy
	movq	$0, loaded(%rip)
	addq	$8, %rsp
	ret
	.size	free_mem, .-free_mem
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gconv_dl.c"
.LC1:
	.string	"obj->counter > 0"
	.text
	.p2align 4,,15
	.type	do_release_shlib, @function
do_release_shlib:
	testl	%esi, %esi
	je	.L12
	cmpl	$3, %esi
	je	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	pushq	%rbx
	movq	(%rdi), %rbx
	cmpq	%rbx, %rdx
	je	.L29
	movl	8(%rbx), %eax
	leal	2(%rax), %edx
	cmpl	$2, %edx
	ja	.L11
	subl	$1, %eax
	cmpl	$-3, %eax
	movl	%eax, 8(%rbx)
	jne	.L11
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	__libc_dlclose
	movq	$0, 16(%rbx)
.L11:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	8(%rdx), %eax
	testl	%eax, %eax
	jle	.L30
	subl	$1, %eax
	movl	%eax, 8(%rdx)
	popq	%rbx
	ret
.L30:
	leaq	__PRETTY_FUNCTION__.9306(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$165, %edx
	call	__assert_fail
	.size	do_release_shlib, .-do_release_shlib
	.section	.rodata.str1.1
.LC2:
	.string	"found->handle == NULL"
.LC3:
	.string	"gconv"
.LC4:
	.string	"gconv_init"
.LC5:
	.string	"gconv_end"
	.text
	.p2align 4,,15
	.globl	__gconv_find_shlib
	.hidden	__gconv_find_shlib
	.type	__gconv_find_shlib, @function
__gconv_find_shlib:
	pushq	%r12
	pushq	%rbp
	leaq	known_compare(%rip), %rdx
	pushq	%rbx
	leaq	loaded(%rip), %rsi
	subq	$16, %rsp
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rdi
	call	__tfind
	testq	%rax, %rax
	je	.L51
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L50
.L35:
	movl	8(%rbx), %eax
	movq	16(%rbx), %rdx
	cmpl	$-2, %eax
	jge	.L37
	testq	%rdx, %rdx
	jne	.L52
	movq	(%rbx), %rdi
	movl	$-2147483646, %esi
	call	__libc_dlopen_mode
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	je	.L50
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	call	__libc_dlsym
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	je	.L53
	movq	16(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	call	__libc_dlsym
	movq	16(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, 32(%rbx)
	call	__libc_dlsym
	movq	24(%rbx), %rdx
	movl	$1, 8(%rbx)
#APP
# 132 "gconv_dl.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
# 134 "gconv_dl.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rdx, 24(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rbx, %rax
	movq	32(%rbx), %rdx
#APP
# 133 "gconv_dl.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	testq	%rdx, %rdx
	je	.L31
	testl	%eax, %eax
	movl	$0, %edx
	cmovs	%edx, %eax
	addl	$1, %eax
	movl	%eax, 8(%rbx)
.L31:
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	8(%rsp), %r12
	movq	%r12, %rdi
	call	strlen
	leaq	49(%rax), %rdi
	movq	%rax, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L50
	leaq	48(%rax), %rdi
	leaq	1(%rbp), %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	leaq	known_compare(%rip), %rdx
	leaq	loaded(%rip), %rsi
	movq	%rax, (%rbx)
	movl	$-3, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, %rdi
	call	__tsearch
	testq	%rax, %rax
	jne	.L35
	movq	%rbx, %rdi
	call	free@PLT
.L50:
	addq	$16, %rsp
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movq	loaded(%rip), %rdi
	leaq	do_release_shlib(%rip), %rsi
	movq	%rbx, %rdx
	xorl	%ebx, %ebx
	call	__twalk_r
	jmp	.L31
.L52:
	leaq	__PRETTY_FUNCTION__.9298(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$114, %edx
	call	__assert_fail
	.size	__gconv_find_shlib, .-__gconv_find_shlib
	.p2align 4,,15
	.globl	__gconv_release_shlib
	.hidden	__gconv_release_shlib
	.type	__gconv_release_shlib, @function
__gconv_release_shlib:
	movq	%rdi, %rdx
	movq	loaded(%rip), %rdi
	leaq	do_release_shlib(%rip), %rsi
	jmp	__twalk_r
	.size	__gconv_release_shlib, .-__gconv_release_shlib
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9306, @object
	.size	__PRETTY_FUNCTION__.9306, 17
__PRETTY_FUNCTION__.9306:
	.string	"do_release_shlib"
	.align 16
	.type	__PRETTY_FUNCTION__.9298, @object
	.size	__PRETTY_FUNCTION__.9298, 19
__PRETTY_FUNCTION__.9298:
	.string	"__gconv_find_shlib"
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	loaded
	.comm	loaded,8,8
	.hidden	__twalk_r
	.hidden	__tsearch
	.hidden	strlen
	.hidden	__libc_dlsym
	.hidden	__libc_dlopen_mode
	.hidden	__tfind
	.hidden	__assert_fail
	.hidden	__tdestroy
	.hidden	__libc_dlclose
	.hidden	strcmp
