	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	cfmakeraw
	.type	cfmakeraw, @function
cfmakeraw:
	movl	8(%rdi), %eax
	andl	$-1516, (%rdi)
	andl	$-2, 4(%rdi)
	andl	$-32844, 12(%rdi)
	andl	$-305, %eax
	orl	$48, %eax
	movl	%eax, 8(%rdi)
	movl	$256, %eax
	movw	%ax, 22(%rdi)
	ret
	.size	cfmakeraw, .-cfmakeraw
