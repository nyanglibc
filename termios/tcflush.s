	.text
	.p2align 4,,15
	.globl	tcflush
	.type	tcflush, @function
tcflush:
	movl	%esi, %edx
	xorl	%eax, %eax
	movl	$21515, %esi
	jmp	__ioctl
	.size	tcflush, .-tcflush
	.hidden	__ioctl
