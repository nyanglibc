	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	tcflow
	.type	tcflow, @function
tcflow:
	movl	%esi, %edx
	xorl	%eax, %eax
	movl	$21514, %esi
	jmp	__GI___ioctl
	.size	tcflow, .-tcflow
