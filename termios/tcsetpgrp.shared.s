	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	tcsetpgrp
	.type	tcsetpgrp, @function
tcsetpgrp:
	subq	$24, %rsp
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	movl	%esi, 12(%rsp)
	movl	$21520, %esi
	call	__GI___ioctl
	addq	$24, %rsp
	ret
	.size	tcsetpgrp, .-tcsetpgrp
