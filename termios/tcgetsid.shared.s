	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	tcgetsid
	.type	tcgetsid, @function
tcgetsid:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	movl	tiocgsid_does_not_work.3152(%rip), %eax
	testl	%eax, %eax
	jne	.L2
	movq	__libc_errno@gottpoff(%rip), %rbp
	leaq	12(%rsp), %rdx
	xorl	%eax, %eax
	movl	$21545, %esi
	movl	%fs:0(%rbp), %r12d
	call	__GI___ioctl
	testl	%eax, %eax
	js	.L11
	movl	12(%rsp), %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$22, %fs:0(%rbp)
	movl	$-1, %eax
	jne	.L1
	movl	$1, tiocgsid_does_not_work.3152(%rip)
	movl	%r12d, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%ebx, %edi
	call	__GI_tcgetpgrp
	cmpl	$-1, %eax
	je	.L7
	movl	%eax, %edi
	call	__GI_getsid
	cmpl	$-1, %eax
	jne	.L1
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$3, %fs:(%rdx)
	je	.L12
.L7:
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$25, %fs:(%rdx)
	jmp	.L1
	.size	tcgetsid, .-tcgetsid
	.local	tiocgsid_does_not_work.3152
	.comm	tiocgsid_does_not_work.3152,4,4
