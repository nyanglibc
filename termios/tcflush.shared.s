	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	tcflush
	.type	tcflush, @function
tcflush:
	movl	%esi, %edx
	xorl	%eax, %eax
	movl	$21515, %esi
	jmp	__GI___ioctl
	.size	tcflush, .-tcflush
