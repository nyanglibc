	.text
	.p2align 4,,15
	.globl	cfsetspeed
	.type	cfsetspeed, @function
cfsetspeed:
	pushq	%rbp
	leaq	speeds(%rip), %rax
	pushq	%rbx
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	subq	$8, %rsp
	leaq	256(%rax), %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	%esi, (%rax)
	je	.L8
	addq	$8, %rax
	cmpq	%rdx, %rax
	je	.L5
	movl	4(%rax), %ebx
.L6:
	cmpl	%esi, %ebx
	jne	.L2
.L8:
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	cfsetispeed
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	cfsetospeed
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	cfsetspeed, .-cfsetspeed
	.section	.rodata
	.align 32
	.type	speeds, @object
	.size	speeds, 256
speeds:
	.long	0
	.long	0
	.long	50
	.long	1
	.long	75
	.long	2
	.long	110
	.long	3
	.long	134
	.long	4
	.long	150
	.long	5
	.long	200
	.long	6
	.long	300
	.long	7
	.long	600
	.long	8
	.long	1200
	.long	9
	.long	1200
	.long	9
	.long	1800
	.long	10
	.long	2400
	.long	11
	.long	4800
	.long	12
	.long	9600
	.long	13
	.long	19200
	.long	14
	.long	38400
	.long	15
	.long	57600
	.long	4097
	.long	115200
	.long	4098
	.long	230400
	.long	4099
	.long	460800
	.long	4100
	.long	500000
	.long	4101
	.long	576000
	.long	4102
	.long	921600
	.long	4103
	.long	1000000
	.long	4104
	.long	1152000
	.long	4105
	.long	1500000
	.long	4106
	.long	2000000
	.long	4107
	.long	2500000
	.long	4108
	.long	3000000
	.long	4109
	.long	3500000
	.long	4110
	.long	4000000
	.long	4111
	.hidden	cfsetospeed
	.hidden	cfsetispeed
