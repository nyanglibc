	.text
	.p2align 4,,15
	.globl	__tcgetattr
	.hidden	__tcgetattr
	.type	__tcgetattr, @function
__tcgetattr:
	movl	$16, %ecx
	movq	%rsi, %r8
	leaq	-56(%rsp), %rdx
	movl	$21505, %esi
	movl	%ecx, %eax
#APP
# 38 "../sysdeps/unix/sysv/linux/tcgetattr.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L5
	testl	%eax, %eax
	jne	.L1
	movl	-44(%rsp), %ecx
	movl	-56(%rsp), %edx
	movdqu	-39(%rsp), %xmm0
	movzwl	-23(%rsp), %esi
	movq	$0, 36(%r8)
	movl	%ecx, 12(%r8)
	movzbl	-40(%rsp), %ecx
	movl	%edx, (%r8)
	movl	-52(%rsp), %edx
	movups	%xmm0, 17(%r8)
	movw	%si, 33(%r8)
	movl	$0, 44(%r8)
	movl	%edx, 4(%r8)
	movb	%cl, 16(%r8)
	movl	-48(%rsp), %edx
	movzbl	-21(%rsp), %ecx
	movb	$0, 48(%r8)
	movl	%edx, 8(%r8)
	andl	$4111, %edx
	movl	%edx, 52(%r8)
	movl	%edx, 56(%r8)
	movb	%cl, 35(%r8)
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__tcgetattr, .-__tcgetattr
	.weak	tcgetattr
	.set	tcgetattr,__tcgetattr
