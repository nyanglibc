	.text
	.p2align 4,,15
	.globl	__tcsetattr
	.type	__tcsetattr, @function
__tcsetattr:
	cmpl	$1, %esi
	movq	%rdx, %rax
	je	.L9
	cmpl	$2, %esi
	je	.L4
	testl	%esi, %esi
	je	.L12
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$21506, %esi
.L3:
	movl	(%rax), %edx
	leaq	17(%rax), %r8
	movdqu	17(%rax), %xmm0
	andl	$2147483647, %edx
	movl	%edx, -56(%rsp)
	movl	4(%rax), %edx
	movups	%xmm0, -39(%rsp)
	movl	%edx, -52(%rsp)
	movl	8(%rax), %edx
	movl	%edx, -48(%rsp)
	movl	12(%rax), %edx
	movl	%edx, -44(%rsp)
	movzbl	16(%rax), %edx
	movzwl	33(%rax), %eax
	movb	%dl, -40(%rsp)
	leaq	-56(%rsp), %rdx
	movw	%ax, -23(%rsp)
	movzbl	18(%r8), %eax
	movb	%al, -21(%rsp)
	movl	$16, %eax
#APP
# 78 "../sysdeps/unix/sysv/linux/tcsetattr.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$21508, %esi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$21507, %esi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__tcsetattr, .-__tcsetattr
	.weak	tcsetattr
	.hidden	tcsetattr
	.set	tcsetattr,__tcsetattr
