	.text
	.p2align 4,,15
	.globl	tcsetpgrp
	.type	tcsetpgrp, @function
tcsetpgrp:
	subq	$24, %rsp
	xorl	%eax, %eax
	leaq	12(%rsp), %rdx
	movl	%esi, 12(%rsp)
	movl	$21520, %esi
	call	__ioctl
	addq	$24, %rsp
	ret
	.size	tcsetpgrp, .-tcsetpgrp
	.hidden	__ioctl
