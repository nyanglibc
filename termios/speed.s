	.text
	.p2align 4,,15
	.globl	cfgetospeed
	.type	cfgetospeed, @function
cfgetospeed:
	movl	8(%rdi), %eax
	andl	$4111, %eax
	ret
	.size	cfgetospeed, .-cfgetospeed
	.p2align 4,,15
	.globl	cfgetispeed
	.type	cfgetispeed, @function
cfgetispeed:
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L3
	movl	8(%rdi), %eax
	andl	$4111, %eax
.L3:
	rep ret
	.size	cfgetispeed, .-cfgetispeed
	.p2align 4,,15
	.globl	cfsetospeed
	.hidden	cfsetospeed
	.type	cfsetospeed, @function
cfsetospeed:
	testl	$-4112, %esi
	je	.L7
	leal	-4097(%rsi), %eax
	cmpl	$14, %eax
	ja	.L12
.L7:
	movl	8(%rdi), %eax
	movl	%esi, 56(%rdi)
	andl	$-4112, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, 8(%rdi)
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	cfsetospeed, .-cfsetospeed
	.p2align 4,,15
	.globl	cfsetispeed
	.hidden	cfsetispeed
	.type	cfsetispeed, @function
cfsetispeed:
	testl	$-4112, %esi
	je	.L14
	leal	-4097(%rsi), %eax
	cmpl	$14, %eax
	ja	.L18
	movl	(%rdi), %eax
	movl	%esi, 52(%rdi)
.L17:
	andl	$2147483647, %eax
	movl	%eax, (%rdi)
	movl	8(%rdi), %eax
	andl	$-4112, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%esi, %esi
	movl	%esi, 52(%rdi)
	movl	(%rdi), %eax
	jne	.L17
	orl	$-2147483648, %eax
	movl	%eax, (%rdi)
	xorl	%eax, %eax
	ret
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	cfsetispeed, .-cfsetispeed
