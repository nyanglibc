	.text
	.p2align 4,,15
	.globl	tcgetpgrp
	.hidden	tcgetpgrp
	.type	tcgetpgrp, @function
tcgetpgrp:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$21519, %esi
	leaq	12(%rsp), %rdx
	call	__ioctl
	testl	%eax, %eax
	js	.L3
	movl	12(%rsp), %eax
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	tcgetpgrp, .-tcgetpgrp
	.hidden	__ioctl
