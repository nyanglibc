	.text
	.p2align 4,,15
	.globl	tcsendbreak
	.type	tcsendbreak, @function
tcsendbreak:
	testl	%esi, %esi
	jle	.L4
	addl	$99, %esi
	movl	$1374389535, %edx
	movl	%esi, %eax
	movl	$21541, %esi
	mull	%edx
	xorl	%eax, %eax
	shrl	$5, %edx
	jmp	__ioctl
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%edx, %edx
	movl	$21513, %esi
	xorl	%eax, %eax
	jmp	__ioctl
	.size	tcsendbreak, .-tcsendbreak
	.hidden	__ioctl
