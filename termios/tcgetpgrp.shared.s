	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_tcgetpgrp
	.hidden	__GI_tcgetpgrp
	.type	__GI_tcgetpgrp, @function
__GI_tcgetpgrp:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$21519, %esi
	leaq	12(%rsp), %rdx
	call	__GI___ioctl
	testl	%eax, %eax
	js	.L3
	movl	12(%rsp), %eax
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__GI_tcgetpgrp, .-__GI_tcgetpgrp
	.globl	tcgetpgrp
	.set	tcgetpgrp,__GI_tcgetpgrp
