	.text
#APP
	.section .gnu.warning.getpw
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s:%s:%lu:%lu:%s:%s:%s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__getpw
	.type	__getpw, @function
__getpw:
.LFB24:
	testq	%rsi, %rsi
	je	.L10
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	movl	%edi, %r12d
	movl	$70, %edi
	movq	%rsi, %rbx
	subq	$64, %rsp
	call	__sysconf
	leaq	30(%rax), %rdx
	leaq	-64(%rbp), %rsi
	leaq	-72(%rbp), %r8
	movq	%rax, %rcx
	movl	%r12d, %edi
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	__getpwuid_r
	testl	%eax, %eax
	jne	.L5
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L5
	movl	20(%rax), %r9d
	movl	16(%rax), %r8d
	subq	$8, %rsp
	movq	8(%rax), %rcx
	pushq	40(%rax)
	leaq	.LC0(%rip), %rsi
	pushq	32(%rax)
	pushq	24(%rax)
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	sprintf
	addq	$32, %rsp
	sarl	$31, %eax
.L1:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
.LFE24:
	.size	__getpw, .-__getpw
	.weak	getpw
	.set	getpw,__getpw
	.section	.gnu.warning.getpw
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getpw, @object
	.size	__evoke_link_warning_getpw, 58
__evoke_link_warning_getpw:
	.string	"the `getpw' function is dangerous and should not be used."
	.hidden	sprintf
	.hidden	__getpwuid_r
	.hidden	__sysconf
