	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section .gnu.warning.getpwuid
	.previous
#NO_APP
	.p2align 4,,15
	.globl	getpwuid
	.type	getpwuid, @function
getpwuid:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	subq	$16, %rsp
#APP
# 116 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer(%rip), %rdx
	movq	buffer_size.10612(%rip), %rbx
	testq	%rdx, %rdx
	je	.L18
.L5:
	leaq	8(%rsp), %r14
	leaq	resbuf.10613(%rip), %r13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer(%rip)
.L12:
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	__getpwuid_r
	cmpl	$34, %eax
	jne	.L19
	movq	buffer_size.10612(%rip), %rax
	movq	buffer(%rip), %rbp
	leaq	(%rax,%rax), %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rbx, buffer_size.10612(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L7
	movq	%rbp, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, buffer(%rip)
	movl	$12, %fs:(%rax)
.L9:
	movq	$0, 8(%rsp)
.L13:
#APP
# 163 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	subl	$1, lock(%rip)
.L11:
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	cmpq	$0, buffer(%rip)
	jne	.L13
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1024, %edi
	movq	$1024, buffer_size.10612(%rip)
	movl	$1024, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, buffer(%rip)
	jne	.L5
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L11
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	getpwuid, .-getpwuid
	.local	resbuf.10613
	.comm	resbuf.10613,48,32
	.local	buffer_size.10612
	.comm	buffer_size.10612,8,8
	.section	.gnu.warning.getpwuid
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getpwuid, @object
	.size	__evoke_link_warning_getpwuid, 132
__evoke_link_warning_getpwuid:
	.string	"Using 'getpwuid' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__getpwuid_r
