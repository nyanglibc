	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%s:%s:::%s:%s:%s\n"
.LC2:
	.string	"%s:%s:%lu:%lu:%s:%s:%s\n"
	.text
	.p2align 4,,15
	.globl	putpwent
	.type	putpwent, @function
putpwent:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L5
	testq	%rsi, %rsi
	je	.L5
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	%rsi, %rbp
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	8(%rbx), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	32(%rbx), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	40(%rbx), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	24(%rbx), %rdi
	leaq	8(%rsp), %rsi
	call	__nss_rewrite_field
	testq	%rax, %rax
	je	.L14
	movq	(%rbx), %rdx
	movq	40(%rbx), %rdi
	movzbl	(%rdx), %ecx
	subl	$43, %ecx
	andl	$253, %ecx
	je	.L32
	movq	32(%rbx), %rsi
	movq	8(%rbx), %rcx
	leaq	.LC0(%rip), %r8
	testq	%rdi, %rdi
	movl	20(%rbx), %r9d
	cmove	%r8, %rdi
	testq	%rsi, %rsi
	cmove	%r8, %rsi
	testq	%rcx, %rcx
	cmove	%r8, %rcx
	movl	16(%rbx), %r8d
	subq	$8, %rsp
	pushq	%rdi
	pushq	%rsi
	leaq	.LC2(%rip), %rsi
	pushq	%rax
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	fprintf
	addq	$32, %rsp
	movl	%eax, %ebx
.L10:
	movq	8(%rsp), %rdi
	call	free@PLT
	testl	%ebx, %ebx
	movl	$0, %eax
	cmovle	%ebx, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	32(%rbx), %r9
	movq	8(%rbx), %rcx
	leaq	.LC0(%rip), %rsi
	testq	%rdi, %rdi
	movq	%rax, %r8
	cmove	%rsi, %rdi
	testq	%r9, %r9
	cmove	%rsi, %r9
	testq	%rcx, %rcx
	cmove	%rsi, %rcx
	subq	$8, %rsp
	leaq	.LC1(%rip), %rsi
	pushq	%rdi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	call	fprintf
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
.L14:
	movl	$-1, %eax
	jmp	.L1
	.size	putpwent, .-putpwent
	.hidden	fprintf
	.hidden	__nss_rewrite_field
	.hidden	__nss_valid_field
