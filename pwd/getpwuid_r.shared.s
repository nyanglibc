	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_getpwuid_r,getpwuid_r@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getpwuid_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__getpwuid_r
	.hidden	__getpwuid_r
	.type	__getpwuid_r, @function
__getpwuid_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	leaq	56(%rsp), %r13
	leaq	48(%rsp), %r12
	movq	%rsi, 8(%rsp)
	leaq	.LC0(%rip), %rsi
	movl	%edi, 32(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rcx, (%rsp)
	xorl	%edx, %edx
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%r8, 40(%rsp)
	call	__GI___nss_passwd_lookup2
	testl	%eax, %eax
	movl	%eax, 36(%rsp)
	jne	.L2
	xorl	%ebp, %ebp
	movq	$0, 24(%rsp)
	movq	%fs:0, %r15
	movq	__libc_errno@gottpoff(%rip), %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L3:
	testl	%ebp, %ebp
	je	.L6
	cmpl	$1, %eax
	jne	.L7
	movl	$22, %fs:(%rbx)
	xorl	%ebp, %ebp
	movl	$-1, %r14d
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	__GI___nss_next2
	testl	%eax, %eax
	jne	.L35
.L13:
	movq	56(%rsp), %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	(%r15,%rbx), %r8
	movq	(%rsp), %rcx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	movl	32(%rsp), %edi
	call	*56(%rsp)
	cmpl	$-2, %eax
	movl	%eax, %r14d
	jne	.L3
	cmpl	$34, %fs:(%rbx)
	je	.L36
	testl	%ebp, %ebp
	je	.L6
.L7:
	movq	48(%rsp), %rax
	movl	$22, %fs:(%rbx)
	movl	8(%rax), %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L17
.L11:
	cmpq	$0, 24(%rsp)
	je	.L37
.L10:
	movl	$22, %fs:(%rbx)
	movl	$-1, %r14d
	movl	$1, %ebp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movq	48(%rsp), %rax
	leal	4(%r14,%r14), %ecx
	movl	8(%rax), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L18
	cmpl	$1, %r14d
	je	.L11
.L18:
	xorl	%ebp, %ebp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$1, %r14d
	movl	$1, %ebp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	movq	24(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r14d
	movq	40(%rsp), %rax
	je	.L16
	movq	$0, (%rax)
	jbe	.L15
	movl	%fs:(%rbx), %eax
	cmpl	$34, %eax
	movl	%eax, 36(%rsp)
	je	.L38
.L1:
	movl	36(%rsp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	8(%rsp), %rdx
	movq	%rdx, (%rax)
.L15:
	movl	36(%rsp), %eax
	movl	%eax, %fs:(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rsp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rsp)
	jne	.L10
	movq	40(%rsp), %rax
	movl	$12, %fs:(%rbx)
	movl	$12, 36(%rsp)
	movq	$0, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	movq	24(%rsp), %rdi
	call	free@PLT
	movq	40(%rsp), %rax
	movl	$34, 36(%rsp)
	movq	$0, (%rax)
	jmp	.L1
.L38:
	cmpl	$-2, %r14d
	je	.L1
.L19:
	movl	$22, 36(%rsp)
	jmp	.L15
.L2:
	movq	40(%rsp), %rax
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	$0, (%rax)
	movl	%fs:(%rbx), %eax
	cmpl	$34, %eax
	movl	%eax, 36(%rsp)
	jne	.L1
	jmp	.L19
	.size	__getpwuid_r, .-__getpwuid_r
	.globl	__new_getpwuid_r
	.set	__new_getpwuid_r,__getpwuid_r
