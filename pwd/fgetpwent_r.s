	.text
	.p2align 4,,15
	.globl	_nss_files_parse_pwent
	.hidden	_nss_files_parse_pwent
	.type	_nss_files_parse_pwent, @function
_nss_files_parse_pwent:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$10, %esi
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	strchr@PLT
	testq	%rax, %rax
	je	.L2
	movb	$0, (%rax)
.L2:
	movq	%rbx, 0(%rbp)
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L3
	testb	%al, %al
	jne	.L4
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$58, %al
	je	.L3
.L4:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L102
.L3:
	testb	%al, %al
	je	.L6
	movb	$0, (%rbx)
	addq	$1, %rbx
.L6:
	cmpb	$0, (%rbx)
	jne	.L7
	movq	0(%rbp), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L103
.L7:
	movq	%rbx, 8(%rbp)
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L9
	testb	%al, %al
	jne	.L10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$58, %al
	je	.L9
.L10:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L104
.L9:
	testb	%al, %al
	jne	.L105
	movq	0(%rbp), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L13
.L107:
	cmpb	$0, (%rbx)
	jne	.L106
.L27:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movb	$0, (%rbx)
	movq	0(%rbp), %rax
	addq	$1, %rbx
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L107
.L13:
	leaq	8(%rsp), %r13
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	strtoul
	movq	8(%rsp), %r12
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cmpq	%rbx, %r12
	movl	%eax, 16(%rbp)
	je	.L27
	movzbl	(%r12), %eax
	cmpb	$58, %al
	je	.L108
	testb	%al, %al
	jne	.L27
.L26:
	movl	$10, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strtoul
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movl	%eax, 20(%rbp)
	movq	8(%rsp), %rax
	cmpq	%r12, %rax
	je	.L27
.L95:
	movzbl	(%rax), %edx
	cmpb	$58, %dl
	je	.L109
	testb	%dl, %dl
	jne	.L27
.L23:
	movq	%rax, 24(%rbp)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L98
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$1, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L31
.L98:
	cmpb	$58, %dl
	jne	.L110
.L31:
	testb	%dl, %dl
	je	.L34
	movb	$0, (%rax)
	addq	$1, %rax
.L34:
	movq	%rax, 32(%rbp)
	movzbl	(%rax), %edx
	cmpb	$58, %dl
	je	.L35
	testb	%dl, %dl
	jne	.L36
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L111:
	cmpb	$58, %dl
	je	.L35
.L36:
	addq	$1, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L111
.L35:
	testb	%dl, %dl
	je	.L38
	movb	$0, (%rax)
	addq	$1, %rax
.L38:
	movq	%rax, 40(%rbp)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movq	$0, 8(%rbp)
	movq	$0, 16(%rbp)
	movl	$1, %eax
	movq	$0, 24(%rbp)
	movq	$0, 32(%rbp)
	movq	$0, 40(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	8(%rsp), %r13
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	strtoul
	movq	8(%rsp), %r12
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cmpq	%rbx, %r12
	movl	%eax, 16(%rbp)
	je	.L112
.L15:
	movzbl	(%r12), %eax
	cmpb	$58, %al
	je	.L113
	testb	%al, %al
	jne	.L27
.L17:
	cmpb	$0, (%r12)
	je	.L27
	movl	$10, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	strtoul
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movl	%eax, 20(%rbp)
	movq	8(%rsp), %rax
	cmpq	%r12, %rax
	jne	.L95
	movl	$0, 20(%rbp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$1, %r12
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$1, %r12
	jmp	.L26
.L112:
	movl	$0, 16(%rbp)
	jmp	.L15
.L109:
	addq	$1, %rax
	jmp	.L23
	.size	_nss_files_parse_pwent, .-_nss_files_parse_pwent
	.p2align 4,,15
	.globl	__fgetpwent_r
	.hidden	__fgetpwent_r
	.type	__fgetpwent_r, @function
__fgetpwent_r:
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	leaq	_nss_files_parse_pwent(%rip), %r8
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__nss_fgetent_r
	testl	%eax, %eax
	movl	$0, %edx
	cmovne	%rdx, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__fgetpwent_r, .-__fgetpwent_r
	.weak	fgetpwent_r
	.set	fgetpwent_r,__fgetpwent_r
	.hidden	__nss_fgetent_r
	.hidden	strtoul
