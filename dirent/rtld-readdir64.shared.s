	.text
	.p2align 4,,15
	.globl	__readdir64
	.type	__readdir64, @function
__readdir64:
	pushq	%r12
	movl	rtld_errno(%rip), %r12d
	pushq	%rbp
	leaq	96(%rdi), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	64(%rdi), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	addq	%rbx, %rax
	movzwl	16(%rax), %ecx
	addq	%rcx, %rdx
	cmpq	$0, (%rax)
	movq	8(%rax), %rcx
	movq	%rdx, 64(%rbx)
	movq	%rcx, 72(%rbx)
	jne	.L1
.L7:
	cmpq	%rdx, 56(%rbx)
	leaq	96(%rdx), %rax
	ja	.L3
	movq	48(%rbx), %rdx
	movl	(%rbx), %edi
	movq	%rbp, %rsi
	call	__getdents64@PLT
	cmpq	$0, %rax
	jle	.L15
	movq	%rax, 56(%rbx)
	xorl	%edx, %edx
	movl	$96, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	jne	.L16
.L5:
	movl	%r12d, rtld_errno(%rip)
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L16:
	xorl	%eax, %eax
	cmpl	$2, rtld_errno(%rip)
	jne	.L1
	jmp	.L5
	.size	__readdir64, .-__readdir64
	.weak	readdir
	.set	readdir,__readdir64
	.weak	readdir64
	.set	readdir64,__readdir64
	.globl	__readdir
	.set	__readdir,__readdir64
	.hidden	rtld_errno
