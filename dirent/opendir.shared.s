	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__alloc_dir
	.hidden	__alloc_dir
	.type	__alloc_dir, @function
__alloc_dir:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	subq	$16, %rsp
	testb	%sil, %sil
	je	.L13
	cmpq	$32768, 56(%rcx)
	movl	$32768, %ebp
	movl	$1048576, %eax
	cmovnb	56(%rcx), %rbp
	cmpq	$1048576, %rbp
	cmova	%rax, %rbp
	leaq	48(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L14
.L6:
	movl	%r12d, (%rbx)
	movl	$0, 4(%rbx)
	movq	%rbp, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	$0, 40(%rbx)
.L1:
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	movl	$1, %edx
	movl	$2, %esi
	movq	%rcx, 8(%rsp)
	call	__GI___fcntl64_nocancel
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	js	.L5
	cmpq	$32768, 56(%rcx)
	movl	$32768, %ebp
	movl	$1048576, %eax
	cmovnb	56(%rcx), %rbp
	cmpq	$1048576, %rbp
	cmova	%rax, %rbp
	leaq	48(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L6
.L5:
	addq	$16, %rsp
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r12d, %edi
	call	__GI___close_nocancel
	jmp	.L1
	.size	__alloc_dir, .-__alloc_dir
	.p2align 4,,15
	.type	opendir_tail, @function
opendir_tail:
	testl	%edi, %edi
	js	.L19
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$152, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L17
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L24
	movq	%rbp, %rcx
	movl	%ebx, %edi
	xorl	%edx, %edx
	movl	$1, %esi
	call	__alloc_dir
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$20, %fs:(%rax)
.L17:
	movl	%ebx, %edi
	call	__GI___close_nocancel
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
	ret
	.size	opendir_tail, .-opendir_tail
	.p2align 4,,15
	.globl	__opendirat
	.hidden	__opendirat
	.type	__opendirat, @function
__opendirat:
	cmpb	$0, (%rsi)
	je	.L26
	subq	$8, %rsp
	movl	$591872, %edx
	xorl	%eax, %eax
	call	__GI___openat_nocancel
	addq	$8, %rsp
	movl	%eax, %edi
	jmp	opendir_tail
	.p2align 4,,10
	.p2align 3
.L26:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__opendirat, .-__opendirat
	.p2align 4,,15
	.globl	__opendir
	.hidden	__opendir
	.type	__opendir, @function
__opendir:
	cmpb	$0, (%rdi)
	je	.L31
	subq	$8, %rsp
	movl	$591872, %esi
	xorl	%eax, %eax
	call	__GI___open_nocancel
	addq	$8, %rsp
	movl	%eax, %edi
	jmp	opendir_tail
	.p2align 4,,10
	.p2align 3
.L31:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__opendir, .-__opendir
	.weak	opendir
	.set	opendir,__opendir
