	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getdents64
	.hidden	__GI___getdents64
	.type	__GI___getdents64, @function
__GI___getdents64:
	cmpq	$2147483647, %rdx
	movl	$2147483647, %eax
	cmova	%rax, %rdx
	movl	$217, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/getdents64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__GI___getdents64, .-__GI___getdents64
	.globl	__getdents64
	.set	__getdents64,__GI___getdents64
	.globl	__getdents
	.hidden	__getdents
	.set	__getdents,__getdents64
	.weak	getdents64
	.set	getdents64,__getdents64
