	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___readdir64
	.hidden	__GI___readdir64
	.type	__GI___readdir64, @function
__GI___readdir64:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	__libc_errno@gottpoff(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%fs:(%r12), %r14d
#APP
# 37 "../sysdeps/unix/sysv/linux/readdir64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	4(%rdi), %r13
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 4(%rdi)
# 0 "" 2
#NO_APP
.L3:
	movq	24(%rbx), %rax
	leaq	48(%rbx), %rbp
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	addq	%rbx, %r8
	movzwl	16(%r8), %edx
	addq	%rdx, %rax
	cmpq	$0, (%r8)
	movq	8(%r8), %rdx
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	jne	.L8
.L9:
	cmpq	%rax, 16(%rbx)
	leaq	48(%rax), %r8
	ja	.L5
	movq	8(%rbx), %rdx
	movl	(%rbx), %edi
	movq	%rbp, %rsi
	call	__GI___getdents64
	cmpq	$0, %rax
	jle	.L19
	movq	%rax, 16(%rbx)
	movl	$48, %r8d
	xorl	%eax, %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L19:
	jne	.L20
.L7:
	movl	%r14d, %fs:(%r12)
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L8:
#APP
# 84 "../sysdeps/unix/sysv/linux/readdir64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	subl	$1, 4(%rbx)
.L1:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L20:
	xorl	%r8d, %r8d
	cmpl	$2, %fs:(%r12)
	jne	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
#APP
# 84 "../sysdeps/unix/sysv/linux/readdir64.c" 1
	xchgl %eax, 4(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 84 "../sysdeps/unix/sysv/linux/readdir64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L3
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	__GI___readdir64, .-__GI___readdir64
	.globl	__readdir64
	.set	__readdir64,__GI___readdir64
	.weak	readdir
	.set	readdir,__readdir64
	.weak	readdir64
	.set	readdir64,__readdir64
	.globl	__readdir
	.set	__readdir,__readdir64
	.hidden	__lll_lock_wait_private
