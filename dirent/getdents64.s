	.text
	.p2align 4,,15
	.globl	__getdents64
	.hidden	__getdents64
	.type	__getdents64, @function
__getdents64:
	cmpq	$2147483647, %rdx
	movl	$2147483647, %eax
	cmova	%rax, %rdx
	movl	$217, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/getdents64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__getdents64, .-__getdents64
	.globl	__getdents
	.hidden	__getdents
	.set	__getdents,__getdents64
	.weak	getdents64
	.set	getdents64,__getdents64
