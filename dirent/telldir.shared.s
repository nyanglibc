	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	telldir
	.type	telldir, @function
telldir:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
#APP
# 28 "../sysdeps/unix/sysv/linux/telldir.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	4(%rdi), %rbp
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 4(%rdi)
# 0 "" 2
#NO_APP
.L3:
	movq	32(%rbx), %r8
#APP
# 30 "../sysdeps/unix/sysv/linux/telldir.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	subl	$1, 4(%rbx)
.L1:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	je	.L3
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/telldir.c" 1
	xchgl %eax, 4(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/telldir.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	telldir, .-telldir
	.hidden	__lll_lock_wait_private
