	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getdirentries64
	.type	getdirentries64, @function
getdirentries64:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	xorl	%esi, %esi
	movl	$1, %edx
	call	__lseek64
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movq	%rax, %r14
	call	__GI___getdents64
	cmpq	$-1, %rax
	je	.L1
	movq	%r14, 0(%r13)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	getdirentries64, .-getdirentries64
	.weak	getdirentries
	.set	getdirentries,getdirentries64
	.hidden	__lseek64
