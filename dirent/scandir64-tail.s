	.text
	.p2align 4,,15
	.globl	__scandir64_tail
	.hidden	__scandir64_tail
	.type	__scandir64_tail, @function
__scandir64_tail:
.LFB47:
	testq	%rdi, %rdi
	je	.L36
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	pxor	%xmm0, %xmm0
	subq	$88, %rsp
	movq	__libc_errno@gottpoff(%rip), %r14
	leaq	48(%rsp), %r13
	movq	%rcx, 32(%rsp)
	movq	%rsi, 24(%rsp)
	movups	%xmm0, 56(%rsp)
	movl	%fs:(%r14), %eax
	movq	%rdi, 48(%rsp)
	movl	%eax, 44(%rsp)
	movl	$0, %fs:(%r14)
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
.L4:
	movq	%r15, %rdi
	movq	%r13, %rbp
.LEHB0:
	call	__readdir64
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L11
.L12:
	testq	%r12, %r12
	je	.L9
	movq	%r13, %rbp
	movq	%rbx, %rdi
	call	*%r12
	testl	%eax, %eax
	movl	$0, %fs:(%r14)
	je	.L4
.L9:
	movq	64(%rsp), %rbp
	cmpq	16(%rsp), %rbp
	je	.L37
.L7:
	movzwl	16(%rbx), %edx
	movq	%rdx, %rdi
	movq	%rdx, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	je	.L11
	movq	(%rsp), %rdx
	leaq	1(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rcx, 64(%rsp)
	call	memcpy@PLT
	movq	8(%rsp), %rsi
	movq	%r15, %rdi
	movl	$0, %fs:(%r14)
	movq	%rax, (%rsi,%rbp,8)
	movq	%r13, %rbp
	call	__readdir64
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L12
.L11:
	movl	%fs:(%r14), %eax
	leaq	48(%rsp), %rbp
	testl	%eax, %eax
	jne	.L13
	movq	%r15, %rdi
	call	__closedir
	movq	32(%rsp), %rcx
	movq	64(%rsp), %rsi
	testq	%rcx, %rcx
	je	.L14
	movq	8(%rsp), %rdi
	movl	$8, %edx
	leaq	48(%rsp), %rbp
	call	qsort
	movq	64(%rsp), %rsi
.L14:
	movq	24(%rsp), %rax
	movq	8(%rsp), %rcx
	testl	%esi, %esi
	movq	%rcx, (%rax)
	movl	%esi, %eax
	js	.L1
	movl	44(%rsp), %ecx
	movl	%ecx, %fs:(%r14)
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%rsp), %rax
	testq	%rax, %rax
	jne	.L38
	movl	$80, %esi
	movq	$10, 16(%rsp)
.L10:
	movq	8(%rsp), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L11
	movq	%rax, 56(%rsp)
	movq	%rax, 8(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L38:
	addq	%rax, %rax
	movq	%rax, 16(%rsp)
	leaq	0(,%rax,8), %rsi
	jmp	.L10
.L13:
	movq	%rbp, %rdi
	call	__scandir_cancel_handler
.LEHE0:
	addq	$88, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L36:
	movl	$-1, %eax
	ret
.L19:
	movq	%rax, %rbx
	movq	%rbp, %rdi
.LEHB1:
	call	__scandir_cancel_handler
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE1:
.LFE47:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA47:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE47-.LLSDACSB47
.LLSDACSB47:
	.uleb128 .LEHB0-.LFB47
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L19-.LFB47
	.uleb128 0
	.uleb128 .LEHB1-.LFB47
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE47:
	.text
	.size	__scandir64_tail, .-__scandir64_tail
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__scandir_cancel_handler
	.hidden	qsort
	.hidden	__closedir
	.hidden	__readdir64
