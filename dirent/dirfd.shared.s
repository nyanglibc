	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__dirfd
	.type	__dirfd, @function
__dirfd:
	movl	(%rdi), %eax
	ret
	.size	__dirfd, .-__dirfd
	.weak	__GI_dirfd
	.hidden	__GI_dirfd
	.set	__GI_dirfd,__dirfd
	.globl	dirfd
	.set	dirfd,__GI_dirfd
