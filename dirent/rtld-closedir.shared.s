	.text
	.p2align 4,,15
	.globl	__closedir
	.hidden	__closedir
	.type	__closedir, @function
__closedir:
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbx
	movl	(%rdi), %ebx
	call	*__rtld_free(%rip)
	movl	%ebx, %edi
	popq	%rbx
	jmp	__GI___close_nocancel
.L5:
	movl	$22, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__closedir, .-__closedir
	.weak	closedir
	.set	closedir,__closedir
	.hidden	rtld_errno
	.hidden	__rtld_free
