	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fdopendir
	.hidden	__fdopendir
	.type	__fdopendir, @function
__fdopendir:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$152, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L11
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L13
	xorl	%eax, %eax
	movl	$3, %esi
	movl	%ebx, %edi
	call	__GI___fcntl64_nocancel
	cmpl	$-1, %eax
	je	.L11
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L14
	movq	%rbp, %rcx
	movl	%eax, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	__alloc_dir
.L1:
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$20, %fs:(%rax)
.L11:
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
	.size	__fdopendir, .-__fdopendir
	.weak	fdopendir
	.set	fdopendir,__fdopendir
	.hidden	__alloc_dir
