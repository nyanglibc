	.text
	.p2align 4,,15
	.globl	__alphasort64
	.type	__alphasort64, @function
__alphasort64:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	addq	$19, %rsi
	addq	$19, %rdi
	jmp	strcoll
	.size	__alphasort64, .-__alphasort64
	.weak	alphasort
	.set	alphasort,__alphasort64
	.weak	alphasort64
	.set	alphasort64,__alphasort64
	.hidden	strcoll
