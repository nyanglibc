	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__readdir64_r
	.type	__readdir64_r, @function
__readdir64_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbp
	subq	$24, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 4(%rsp)
#APP
# 38 "../sysdeps/unix/sysv/linux/readdir64_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	leaq	4(%rdi), %rbx
	testl	%eax, %eax
	movq	%rbx, 8(%rsp)
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 4(%rdi)
# 0 "" 2
#NO_APP
.L3:
	movq	24(%r12), %r14
	leaq	48(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	%r14, 16(%r12)
	leaq	48(%r14), %rsi
	jbe	.L30
.L6:
	leaq	(%r12,%rsi), %r15
	movzwl	16(%r15), %eax
	movq	8(%r15), %rdx
	movq	%rdx, 32(%r12)
	addq	%rax, %r14
	cmpq	$275, %rax
	movq	%r14, 24(%r12)
	jbe	.L12
	leaq	19(%r15), %rdi
	call	__GI_strlen
	cmpq	$255, %rax
	ja	.L13
	addq	$20, %rax
.L12:
	cmpq	$0, (%r15)
	je	.L4
	cmpl	$8, %eax
	movl	%eax, %ecx
	jb	.L31
	movq	(%r15), %rdx
	leaq	8(%rbp), %rdi
	movq	%r15, %rsi
	andq	$-8, %rdi
	movq	%rdx, 0(%rbp)
	movl	%eax, %edx
	movq	-8(%r15,%rdx), %rcx
	movq	%rcx, -8(%rbp,%rdx)
	movq	%rbp, %rcx
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
.L16:
	movq	%rbp, 0(%r13)
	xorl	%r9d, %r9d
	movw	%ax, 16(%rbp)
.L11:
#APP
# 115 "../sysdeps/unix/sysv/linux/readdir64_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	subl	$1, 4(%r12)
.L1:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	8(%r12), %rdx
	movl	(%r12), %edi
	movq	%rbx, %rsi
	call	__GI___getdents64
	cmpq	$0, %rax
	jle	.L32
	movq	%rax, 16(%r12)
	movl	$48, %esi
	xorl	%r14d, %r14d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$36, 40(%r12)
	movq	$0, (%r15)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	testb	$4, %al
	jne	.L33
	testl	%ecx, %ecx
	je	.L16
	movzbl	(%r15), %edx
	testb	$2, %cl
	movb	%dl, 0(%rbp)
	je	.L16
	movzwl	-2(%r15,%rcx), %edx
	movw	%dx, -2(%rbp,%rcx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L32:
	jne	.L8
.L28:
	movl	40(%r12), %r9d
.L9:
	movq	$0, 0(%r13)
	jmp	.L11
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r9d
	cmpl	$2, %r9d
	je	.L34
	movl	%r9d, 40(%r12)
	jmp	.L9
.L19:
	xorl	%eax, %eax
#APP
# 115 "../sysdeps/unix/sysv/linux/readdir64_r.c" 1
	xchgl %eax, 4(%r12)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	8(%rsp), %rdi
	movl	$202, %eax
#APP
# 115 "../sysdeps/unix/sysv/linux/readdir64_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L2:
	leaq	4(%rdi), %rbx
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L3
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.L33:
	movl	(%r15), %edx
	movl	%edx, 0(%rbp)
	movl	-4(%r15,%rcx), %edx
	movl	%edx, -4(%rbp,%rcx)
	jmp	.L16
.L34:
	movl	4(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	jmp	.L28
	.size	__readdir64_r, .-__readdir64_r
	.weak	readdir64_r
	.set	readdir64_r,__readdir64_r
	.weak	readdir_r
	.set	readdir_r,__readdir64_r
	.globl	__readdir_r
	.set	__readdir_r,__readdir64_r
	.hidden	__lll_lock_wait_private
