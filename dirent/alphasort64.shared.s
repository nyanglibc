	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__alphasort64
	.type	__alphasort64, @function
__alphasort64:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	addq	$19, %rsi
	addq	$19, %rdi
	jmp	__GI_strcoll
	.size	__alphasort64, .-__alphasort64
	.weak	alphasort
	.set	alphasort,__alphasort64
	.weak	alphasort64
	.set	alphasort64,__alphasort64
