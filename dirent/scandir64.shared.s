	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__scandir64
	.type	__scandir64, @function
__scandir64:
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r12
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	call	__opendir
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movq	%rax, %rdi
	jmp	__scandir64_tail
	.size	__scandir64, .-__scandir64
	.weak	scandir
	.set	scandir,__scandir64
	.weak	scandir64
	.set	scandir64,__scandir64
	.hidden	__scandir64_tail
	.hidden	__opendir
