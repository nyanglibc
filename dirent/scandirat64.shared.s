	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	scandirat64
	.type	scandirat64, @function
scandirat64:
	pushq	%r12
	pushq	%rbp
	movq	%r8, %r12
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	call	__opendirat
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movq	%rax, %rdi
	jmp	__scandir64_tail
	.size	scandirat64, .-scandirat64
	.weak	scandirat
	.set	scandirat,scandirat64
	.hidden	__scandir64_tail
	.hidden	__opendirat
