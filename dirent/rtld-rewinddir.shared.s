	.text
	.p2align 4,,15
	.globl	__rewinddir
	.hidden	__rewinddir
	.type	__rewinddir, @function
__rewinddir:
	pushq	%rbx
	movq	%rdi, %rbx
	movl	(%rdi), %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__lseek
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	movl	$0, 80(%rbx)
	popq	%rbx
	ret
	.size	__rewinddir, .-__rewinddir
	.weak	rewinddir
	.set	rewinddir,__rewinddir
	.hidden	__lseek
