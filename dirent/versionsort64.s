	.text
	.p2align 4,,15
	.globl	__versionsort64
	.type	__versionsort64, @function
__versionsort64:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	addq	$19, %rsi
	addq	$19, %rdi
	jmp	__strverscmp
	.size	__versionsort64, .-__versionsort64
	.weak	versionsort
	.set	versionsort,__versionsort64
	.weak	versionsort64
	.set	versionsort64,__versionsort64
	.hidden	__strverscmp
