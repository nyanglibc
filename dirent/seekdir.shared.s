	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	seekdir
	.type	seekdir, @function
seekdir:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 29 "../sysdeps/unix/sysv/linux/seekdir.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	4(%rdi), %r12
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 4(%rdi)
# 0 "" 2
#NO_APP
.L3:
	movl	(%rbx), %edi
	xorl	%edx, %edx
	movq	%rbp, %rsi
	call	__GI___lseek
	movq	$0, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rbp, 32(%rbx)
#APP
# 34 "../sysdeps/unix/sysv/linux/seekdir.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	subl	$1, 4(%rbx)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L3
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/seekdir.c" 1
	xchgl %eax, 4(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/seekdir.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	seekdir, .-seekdir
	.hidden	__lll_lock_wait_private
