	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__versionsort64
	.type	__versionsort64, @function
__versionsort64:
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	addq	$19, %rsi
	addq	$19, %rdi
	jmp	__GI___strverscmp
	.size	__versionsort64, .-__versionsort64
	.weak	versionsort
	.set	versionsort,__versionsort64
	.weak	versionsort64
	.set	versionsort64,__versionsort64
