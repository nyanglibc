	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__scandir_cancel_handler
	.hidden	__scandir_cancel_handler
	.type	__scandir_cancel_handler, @function
__scandir_cancel_handler:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	cmpq	$0, 16(%rdi)
	movq	8(%rdi), %r12
	je	.L2
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%r12,%rbx,8), %rdi
	addq	$1, %rbx
	call	free@PLT
	cmpq	%rbx, 16(%rbp)
	ja	.L3
.L2:
	movq	%r12, %rdi
	call	free@PLT
	popq	%rbx
	movq	0(%rbp), %rdi
	popq	%rbp
	popq	%r12
	jmp	__closedir
	.size	__scandir_cancel_handler, .-__scandir_cancel_handler
	.hidden	__closedir
