	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__closedir
	.hidden	__closedir
	.type	__closedir, @function
__closedir:
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbx
	movl	(%rdi), %ebx
	call	free@PLT
	movl	%ebx, %edi
	popq	%rbx
	jmp	__GI___close_nocancel
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__closedir, .-__closedir
	.weak	closedir
	.set	closedir,__closedir
