	.text
	.p2align 4,,15
	.globl	__dirfd
	.type	__dirfd, @function
__dirfd:
	movl	(%rdi), %eax
	ret
	.size	__dirfd, .-__dirfd
	.weak	dirfd
	.hidden	dirfd
	.set	dirfd,__dirfd
