	.text
	.p2align 4,,15
	.type	init, @function
init:
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L9
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	subq	$8, %rsp
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	je	.L1
	leaq	last_result(%rip), %rax
	movq	%rax, static_buf(%rip)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	last_result(%rip), %rax
	movq	%rax, static_buf(%rip)
	ret
	.size	init, .-init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	": "
.LC2:
	.string	"out of memory"
.LC3:
	.string	"%s%s%s"
.LC4:
	.string	"%s%s%s: %s"
	.text
	.p2align 4,,15
	.globl	__dlerror
	.hidden	__dlerror
	.type	__dlerror, @function
__dlerror:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpq	$0, __pthread_once@GOTPCREL(%rip)
	movq	$0, 8(%rsp)
	je	.L11
	leaq	init(%rip), %rsi
	leaq	once(%rip), %rdi
	call	__pthread_once@PLT
	movq	static_buf(%rip), %rbx
.L12:
	testq	%rbx, %rbx
	je	.L51
.L17:
	movl	4(%rbx), %edx
	movq	24(%rbx), %rax
	testl	%edx, %edx
	je	.L20
	testq	%rax, %rax
	je	.L50
	leaq	.LC2(%rip), %rdi
	movl	$14, %ecx
	movq	%rax, %rsi
	repz cmpsb
	je	.L23
	movq	%rax, %rdi
	call	free@PLT
.L23:
	movq	$0, 24(%rbx)
.L50:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%rax, %rax
	je	.L50
	movl	(%rbx), %edi
	movq	%rax, 8(%rsp)
	testl	%edi, %edi
	je	.L52
	call	strerror@PLT
	movq	24(%rbx), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbp
	call	__dcgettext
	movq	16(%rbx), %rdx
	movq	%rax, %r8
	leaq	.LC1(%rip), %rcx
	leaq	.LC0(%rip), %rax
	leaq	8(%rsp), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rbp, %r9
	cmpb	$0, (%rdx)
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__asprintf
	cmpl	$-1, %eax
	je	.L53
.L29:
	movq	24(%rbx), %rax
	leaq	.LC2(%rip), %rdi
	movl	$14, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L54
.L31:
	movq	8(%rsp), %rax
	movq	%rax, 24(%rbx)
.L30:
	movl	$1, 4(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	once(%rip), %eax
	movq	static_buf(%rip), %rbx
	testl	%eax, %eax
	jne	.L12
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L14
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	jne	.L15
	movq	static_buf(%rip), %rbx
	movl	once(%rip), %eax
.L16:
	orl	$2, %eax
	testq	%rbx, %rbx
	movl	%eax, once(%rip)
	jne	.L17
.L51:
	cmpq	$0, __pthread_getspecific@GOTPCREL(%rip)
	je	.L19
	movl	key(%rip), %edi
	call	__pthread_getspecific@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L17
.L19:
	leaq	last_result(%rip), %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rsi
	call	__dcgettext
	movq	16(%rbx), %rdx
	movq	%rax, %r8
	leaq	.LC1(%rip), %rcx
	leaq	.LC0(%rip), %rax
	leaq	8(%rsp), %rdi
	leaq	.LC3(%rip), %rsi
	cmpb	$0, (%rdx)
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__asprintf
	cmpl	$-1, %eax
	jne	.L29
.L53:
	movq	8(%rsp), %rax
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L15:
	movl	once(%rip), %eax
.L14:
	leaq	last_result(%rip), %rbx
	movq	%rbx, static_buf(%rip)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rax, %rdi
	call	free@PLT
	jmp	.L31
	.size	__dlerror, .-__dlerror
	.p2align 4,,15
	.type	check_free.isra.0, @function
check_free.isra.0:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L64
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	.LC2(%rip), %rdi
	movl	$14, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L67
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rax, %rdi
	call	free@PLT
	movq	$0, (%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	rep ret
	.size	check_free.isra.0, .-check_free.isra.0
	.p2align 4,,15
	.type	free_key_mem, @function
free_key_mem:
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$24, %rdi
	call	check_free.isra.0
	movq	%rbx, %rdi
	call	free@PLT
	cmpq	$0, __pthread_setspecific@GOTPCREL(%rip)
	je	.L68
	popq	%rbx
	movl	key(%rip), %edi
	xorl	%esi, %esi
	jmp	__pthread_setspecific@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	popq	%rbx
	ret
	.size	free_key_mem, .-free_key_mem
	.p2align 4,,15
	.globl	_dlerror_run
	.hidden	_dlerror_run
	.type	_dlerror_run, @function
_dlerror_run:
	cmpq	$0, __pthread_once@GOTPCREL(%rip)
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L72
	leaq	init(%rip), %rsi
	leaq	once(%rip), %rdi
	call	__pthread_once@PLT
	movq	static_buf(%rip), %rbx
.L73:
	testq	%rbx, %rbx
	je	.L99
.L78:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L82
	cmpb	$0, 8(%rbx)
	jne	.L100
.L83:
	movq	$0, 24(%rbx)
.L82:
	leaq	8(%rbx), %rdx
	leaq	24(%rbx), %rsi
	leaq	16(%rbx), %rdi
	movq	%r12, %r8
	movq	%rbp, %rcx
	call	_dl_catch_error
	movl	%eax, (%rbx)
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	sete	%dl
	setne	%al
	movl	%edx, 4(%rbx)
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	call	free@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L72:
	movl	once(%rip), %eax
	movq	static_buf(%rip), %rbx
	testl	%eax, %eax
	jne	.L73
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L75
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	jne	.L76
	movq	static_buf(%rip), %rbx
	movl	once(%rip), %eax
.L77:
	orl	$2, %eax
	testq	%rbx, %rbx
	movl	%eax, once(%rip)
	jne	.L78
.L99:
	cmpq	$0, __pthread_getspecific@GOTPCREL(%rip)
	je	.L81
	movl	key(%rip), %edi
	call	__pthread_getspecific@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L78
.L81:
	movl	$32, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L101
	cmpq	$0, __pthread_setspecific@GOTPCREL(%rip)
	je	.L78
	movl	key(%rip), %edi
	movq	%rax, %rsi
	call	__pthread_setspecific@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	movl	once(%rip), %eax
.L75:
	leaq	last_result(%rip), %rbx
	movq	%rbx, static_buf(%rip)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	last_result(%rip), %rbx
	jmp	.L78
	.size	_dlerror_run, .-_dlerror_run
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	fini, @function
fini:
	leaq	24+last_result(%rip), %rdi
	jmp	check_free.isra.0
	.size	fini, .-fini
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	fini
	.section	.rodata.str1.1
.LC5:
	.string	"_dlfcn_hook"
	.text
	.p2align 4,,15
	.globl	__libc_register_dlfcn_hook
	.hidden	__libc_register_dlfcn_hook
	.type	__libc_register_dlfcn_hook, @function
__libc_register_dlfcn_hook:
	leaq	.LC5(%rip), %rsi
	subq	$8, %rsp
	call	__libc_dlsym_private
	testq	%rax, %rax
	je	.L103
	leaq	_dlfcn_hooks(%rip), %rdx
	movq	%rdx, (%rax)
.L103:
	addq	$8, %rsp
	ret
	.size	__libc_register_dlfcn_hook, .-__libc_register_dlfcn_hook
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	_dlfcn_hooks, @object
	.size	_dlfcn_hooks, 104
_dlfcn_hooks:
	.quad	__dlopen
	.quad	__dlclose
	.quad	__dlsym
	.quad	__dlvsym
	.quad	__dlerror
	.quad	__dladdr
	.quad	__dladdr1
	.quad	__dlinfo
	.quad	__dlmopen
	.zero	32
	.local	once
	.comm	once,4,4
	.local	key
	.comm	key,4,4
	.local	static_buf
	.comm	static_buf,8,8
	.local	last_result
	.comm	last_result,32,32
	.weak	__pthread_setspecific
	.weak	__pthread_getspecific
	.weak	__pthread_once
	.weak	__pthread_key_create
	.hidden	__dlmopen
	.hidden	__dlinfo
	.hidden	__dladdr1
	.hidden	__dladdr
	.hidden	__dlvsym
	.hidden	__dlsym
	.hidden	__dlclose
	.hidden	__dlopen
	.hidden	__libc_dlsym_private
	.hidden	_dl_catch_error
	.hidden	__asprintf
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
