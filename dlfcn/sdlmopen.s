	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"invalid namespace"
	.text
	.p2align 4,,15
	.type	dlmopen_doit, @function
dlmopen_doit:
	pushq	%rbx
	cmpq	$0, (%rdi)
	jne	.L6
	movq	32(%rdi), %rdx
	movl	16(%rdi), %esi
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	leaq	.LC0(%rip), %rax
	movq	__libc_argv(%rip), %r9
	movl	__libc_argc(%rip), %r8d
	orl	$-2147483648, %esi
	testq	%rdi, %rdi
	cmove	%rax, %rdi
	subq	$8, %rsp
	pushq	__environ(%rip)
	xorl	%ecx, %ecx
	call	_dl_open
	movq	%rax, 24(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
.L6:
	leaq	.LC1(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$22, %edi
	call	_dl_signal_error
	.size	dlmopen_doit, .-dlmopen_doit
	.p2align 4,,15
	.globl	__dlmopen
	.hidden	__dlmopen
	.type	__dlmopen, @function
__dlmopen:
	subq	$56, %rsp
	movq	%rdi, (%rsp)
	leaq	dlmopen_doit(%rip), %rdi
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rsi
	movl	%edx, 16(%rsp)
	movq	%rcx, 32(%rsp)
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L9
	movq	24(%rsp), %rdi
	call	__libc_register_dl_open_hook
	movq	24(%rsp), %rdi
	call	__libc_register_dlfcn_hook
	movq	24(%rsp), %rax
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	addq	$56, %rsp
	ret
	.size	__dlmopen, .-__dlmopen
	.hidden	__libc_register_dlfcn_hook
	.hidden	__libc_register_dl_open_hook
	.hidden	_dlerror_run
	.hidden	_dl_signal_error
	.hidden	_dl_open
	.hidden	__libc_argc
	.hidden	__libc_argv
