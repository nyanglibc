	.p2align 4,,15
	.globl	__dladdr
	.hidden	__dladdr
	.type	__dladdr, @function
__dladdr:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_dl_addr
	.size	__dladdr, .-__dladdr
	.hidden	_dl_addr
