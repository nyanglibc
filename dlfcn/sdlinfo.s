	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unsupported dlinfo request"
	.text
	.p2align 4,,15
	.type	dlinfo_doit, @function
dlinfo_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	cmpl	$10, 8(%rbx)
	ja	.L2
	movl	8(%rbx), %eax
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L2-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L2-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	cmpq	$0, 1120(%rdi)
	jne	.L16
.L12:
	movq	16(%rbx), %rdx
	movq	%rax, (%rdx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	16(%rbx), %rax
	movq	48(%rdi), %rdx
	movq	%rdx, (%rax)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	16(%rbx), %rax
	movq	%rdi, (%rax)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	popq	%rbx
	jmp	_dl_rtld_di_serinfo@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%rbx), %rsi
	movl	$1, %edx
	popq	%rbx
	jmp	_dl_rtld_di_serinfo@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	848(%rdi), %rsi
	movq	16(%rbx), %rdi
	popq	%rbx
	jmp	strcpy
	.p2align 4,,10
	.p2align 3
.L9:
	movq	16(%rbx), %rax
	movq	$0, (%rax)
	movq	1120(%rdi), %rdx
	movq	%rdx, (%rax)
	popq	%rbx
	ret
.L2:
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L16:
	call	_dl_tls_get_addr_soft
	jmp	.L12
	.size	dlinfo_doit, .-dlinfo_doit
	.p2align 4,,15
	.globl	__dlinfo
	.hidden	__dlinfo
	.type	__dlinfo, @function
__dlinfo:
	subq	$40, %rsp
	movq	%rdi, (%rsp)
	leaq	dlinfo_doit(%rip), %rdi
	movl	%esi, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rdx, 16(%rsp)
	call	_dlerror_run
	testl	%eax, %eax
	setne	%al
	addq	$40, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	__dlinfo, .-__dlinfo
	.hidden	_dlerror_run
	.hidden	_dl_tls_get_addr_soft
	.hidden	_dl_signal_error
	.hidden	strcpy
