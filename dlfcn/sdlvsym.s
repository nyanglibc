	.text
	.p2align 4,,15
	.type	dlvsym_doit, @function
dlvsym_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_dl_vsym@PLT
	movq	%rax, 32(%rbx)
	popq	%rbx
	ret
	.size	dlvsym_doit, .-dlvsym_doit
	.p2align 4,,15
	.globl	__dlvsym
	.hidden	__dlvsym
	.type	__dlvsym, @function
__dlvsym:
	pushq	%rbx
	subq	$48, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	je	.L5
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L5:
	leaq	dlvsym_doit(%rip), %rdi
	movq	%rsp, %rsi
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L8
	movq	32(%rsp), %rbx
.L6:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L4
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L4:
	addq	$48, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%ebx, %ebx
	jmp	.L6
	.size	__dlvsym, .-__dlvsym
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	_dlerror_run
