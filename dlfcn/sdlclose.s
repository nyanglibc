	.text
	.p2align 4,,15
	.type	dlclose_doit, @function
dlclose_doit:
	jmp	_dl_close
	.size	dlclose_doit, .-dlclose_doit
	.p2align 4,,15
	.globl	__dlclose
	.hidden	__dlclose
	.type	__dlclose, @function
__dlclose:
	movq	%rdi, %rsi
	leaq	dlclose_doit(%rip), %rdi
	subq	$8, %rsp
	call	_dlerror_run
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	__dlclose, .-__dlclose
	.hidden	_dlerror_run
	.hidden	_dl_close
