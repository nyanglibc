	.text
	.p2align 4,,15
	.type	dlsym_doit, @function
dlsym_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_dl_sym@PLT
	movq	%rax, 24(%rbx)
	popq	%rbx
	ret
	.size	dlsym_doit, .-dlsym_doit
	.p2align 4,,15
	.globl	__dlsym
	.hidden	__dlsym
	.type	__dlsym, @function
__dlsym:
	pushq	%rbx
	subq	$32, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdx, 16(%rsp)
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	je	.L5
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L5:
	leaq	dlsym_doit(%rip), %rdi
	movq	%rsp, %rsi
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L8
	movq	24(%rsp), %rbx
.L6:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L4
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L4:
	addq	$32, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%ebx, %ebx
	jmp	.L6
	.size	__dlsym, .-__dlsym
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	_dlerror_run
