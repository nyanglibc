	.text
#APP
	.section .gnu.warning.dlopen
	.previous
#NO_APP
	.p2align 4,,15
	.globl	dlopen
	.type	dlopen, @function
dlopen:
	movq	(%rsp), %rdx
	jmp	__dlopen@PLT
	.size	dlopen, .-dlopen
	.section	.gnu.warning.dlopen
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_dlopen, @object
	.size	__evoke_link_warning_dlopen, 130
__evoke_link_warning_dlopen:
	.string	"Using 'dlopen' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
