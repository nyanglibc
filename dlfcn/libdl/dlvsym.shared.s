	.text
	.p2align 4,,15
	.type	dlvsym_doit, @function
dlvsym_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_dl_vsym@PLT
	movq	%rax, 32(%rbx)
	popq	%rbx
	ret
	.size	dlvsym_doit, .-dlvsym_doit
	.p2align 4,,15
	.globl	__dlvsym
	.hidden	__dlvsym
	.type	__dlvsym, @function
__dlvsym:
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	je	.L10
	movq	72(%rsp), %rax
	movq	_rtld_global@GOTPCREL(%rip), %rbx
	xorl	%ebp, %ebp
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rax, 24(%rsp)
	leaq	2440(%rbx), %rdi
	call	*3984(%rbx)
	leaq	dlvsym_doit(%rip), %rdi
	movq	%rsp, %rsi
	call	_dlerror_run
	testl	%eax, %eax
	je	.L11
.L7:
	leaq	2440(%rbx), %rdi
	call	*3992(%rbx)
	addq	$56, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	32(%rsp), %rbp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__GI__dlfcn_hook(%rip), %rax
	movq	72(%rsp), %rcx
	call	*24(%rax)
	addq	$56, %rsp
	movq	%rax, %rbp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__dlvsym, .-__dlvsym
	.weak	dlvsym
	.set	dlvsym,__dlvsym
	.hidden	_dlerror_run
