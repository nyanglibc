	.text
	.p2align 4,,15
	.type	dlclose_doit, @function
dlclose_doit:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	jmp	*768(%rax)
	.size	dlclose_doit, .-dlclose_doit
	.p2align 4,,15
	.globl	__dlclose
	.hidden	__dlclose
	.type	__dlclose, @function
__dlclose:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L4
	movq	__GI__dlfcn_hook(%rip), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rdi, %rsi
	leaq	dlclose_doit(%rip), %rdi
	subq	$8, %rsp
	call	_dlerror_run
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	__dlclose, .-__dlclose
	.globl	dlclose
	.set	dlclose,__dlclose
	.hidden	_dlerror_run
