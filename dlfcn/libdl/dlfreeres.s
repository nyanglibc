	.text
	.p2align 4,,15
	.globl	__libdl_freeres
	.type	__libdl_freeres, @function
__libdl_freeres:
	cmpq	$0, __dlerror_main_freeres@GOTPCREL(%rip)
	je	.L1
	jmp	__dlerror_main_freeres@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	__libdl_freeres, .-__libdl_freeres
	.weak	__dlerror_main_freeres
