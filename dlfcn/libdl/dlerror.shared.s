	.text
	.p2align 4,,15
	.type	init, @function
init:
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L9
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	subq	$8, %rsp
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	je	.L1
	leaq	last_result(%rip), %rax
	movq	%rax, static_buf(%rip)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	last_result(%rip), %rax
	movq	%rax, static_buf(%rip)
	ret
	.size	init, .-init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory"
	.text
	.p2align 4,,15
	.type	check_free, @function
check_free:
	movq	24(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L23
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	movl	$14, %ecx
	subq	$48, %rsp
	repz cmpsb
	jne	.L26
.L10:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	8(%rsp), %rdx
	leaq	16(%rsp), %rsi
	leaq	check_free(%rip), %rdi
	xorl	%ecx, %ecx
	movq	$0, 8(%rsp)
	call	_dl_addr@PLT
	testl	%eax, %eax
	je	.L10
	movq	8(%rsp), %rax
	cmpq	$0, 48(%rax)
	jne	.L10
	movq	24(%rbx), %rdi
	call	free@PLT
	movq	$0, 24(%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	rep ret
	.size	check_free, .-check_free
	.p2align 4,,15
	.type	free_key_mem, @function
free_key_mem:
	pushq	%rbx
	movq	%rdi, %rbx
	call	check_free
	movq	%rbx, %rdi
	call	free@PLT
	cmpq	$0, __pthread_setspecific@GOTPCREL(%rip)
	je	.L27
	popq	%rbx
	movl	key(%rip), %edi
	xorl	%esi, %esi
	jmp	__pthread_setspecific@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	popq	%rbx
	ret
	.size	free_key_mem, .-free_key_mem
	.section	.rodata.str1.1
.LC1:
	.string	""
.LC2:
	.string	": "
.LC3:
	.string	"%s%s%s"
.LC4:
	.string	"%s%s%s: %s"
	.text
	.p2align 4,,15
	.globl	__dlerror
	.hidden	__dlerror
	.type	__dlerror, @function
__dlerror:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	$0, 8(%rsp)
	cmpq	$0, 664(%rax)
	jne	.L31
	movq	__GI__dlfcn_hook(%rip), %rax
	call	*32(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	$0, __pthread_once@GOTPCREL(%rip)
	je	.L33
	leaq	init(%rip), %rsi
	leaq	once(%rip), %rdi
	call	__pthread_once@PLT
	movq	static_buf(%rip), %rbx
.L34:
	testq	%rbx, %rbx
	je	.L73
.L39:
	movl	4(%rbx), %edx
	movq	24(%rbx), %rax
	testl	%edx, %edx
	je	.L42
	testq	%rax, %rax
	je	.L72
	leaq	.LC0(%rip), %rdi
	movl	$14, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L74
.L45:
	movq	$0, 24(%rbx)
.L72:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	once(%rip), %eax
	movq	static_buf(%rip), %rbx
	testl	%eax, %eax
	jne	.L34
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L36
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	jne	.L37
	movq	static_buf(%rip), %rbx
	movl	once(%rip), %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L37:
	movl	once(%rip), %eax
.L36:
	leaq	last_result(%rip), %rbx
	movq	%rbx, static_buf(%rip)
.L38:
	orl	$2, %eax
	movl	%eax, once(%rip)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%rax, %rax
	je	.L72
	movl	(%rbx), %edi
	movq	%rax, 8(%rsp)
	testl	%edi, %edi
	jne	.L47
	movq	_libc_intl_domainname@GOTPCREL(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rsi
	call	__dcgettext@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %r8
	leaq	.LC2(%rip), %rcx
	leaq	.LC1(%rip), %rax
	leaq	8(%rsp), %rdi
	leaq	.LC3(%rip), %rsi
	cmpb	$0, (%rdx)
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__asprintf@PLT
.L49:
	cmpl	$-1, %eax
	je	.L75
	movq	24(%rbx), %rax
	leaq	.LC0(%rip), %rdi
	movl	$14, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L76
.L53:
	movq	8(%rsp), %rax
	movq	%rax, 24(%rbx)
.L52:
	movl	$1, 4(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	$0, __pthread_getspecific@GOTPCREL(%rip)
	je	.L41
	movl	key(%rip), %edi
	call	__pthread_getspecific@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L39
.L41:
	leaq	last_result(%rip), %rbx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	call	strerror@PLT
	movq	24(%rbx), %rsi
	movq	_libc_intl_domainname@GOTPCREL(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbp
	call	__dcgettext@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %r8
	leaq	.LC2(%rip), %rcx
	leaq	.LC1(%rip), %rax
	leaq	8(%rsp), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rbp, %r9
	cmpb	$0, (%rdx)
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__asprintf@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%rax, %rdi
	call	free@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rsp), %rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%rax, %rdi
	call	free@PLT
	jmp	.L45
	.size	__dlerror, .-__dlerror
	.globl	dlerror
	.set	dlerror,__dlerror
	.p2align 4,,15
	.globl	_dlerror_run
	.hidden	_dlerror_run
	.type	_dlerror_run, @function
_dlerror_run:
	cmpq	$0, __pthread_once@GOTPCREL(%rip)
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L78
	leaq	init(%rip), %rsi
	leaq	once(%rip), %rdi
	call	__pthread_once@PLT
	movq	static_buf(%rip), %rbx
.L79:
	testq	%rbx, %rbx
	je	.L105
.L84:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L88
	cmpb	$0, 8(%rbx)
	jne	.L106
.L89:
	movq	$0, 24(%rbx)
.L88:
	leaq	8(%rbx), %rdx
	leaq	24(%rbx), %rsi
	leaq	16(%rbx), %rdi
	movq	%r12, %r8
	movq	%rbp, %rcx
	call	_dl_catch_error@PLT
	movl	%eax, (%rbx)
	movq	24(%rbx), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	sete	%dl
	setne	%al
	movl	%edx, 4(%rbx)
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	call	free@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L78:
	movl	once(%rip), %eax
	movq	static_buf(%rip), %rbx
	testl	%eax, %eax
	jne	.L79
	cmpq	$0, __pthread_key_create@GOTPCREL(%rip)
	je	.L81
	leaq	free_key_mem(%rip), %rsi
	leaq	key(%rip), %rdi
	call	__pthread_key_create@PLT
	testl	%eax, %eax
	jne	.L82
	movq	static_buf(%rip), %rbx
	movl	once(%rip), %eax
.L83:
	orl	$2, %eax
	testq	%rbx, %rbx
	movl	%eax, once(%rip)
	jne	.L84
.L105:
	cmpq	$0, __pthread_getspecific@GOTPCREL(%rip)
	je	.L87
	movl	key(%rip), %edi
	call	__pthread_getspecific@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L84
.L87:
	movl	$32, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L107
	cmpq	$0, __pthread_setspecific@GOTPCREL(%rip)
	je	.L84
	movl	key(%rip), %edi
	movq	%rax, %rsi
	call	__pthread_setspecific@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L82:
	movl	once(%rip), %eax
.L81:
	leaq	last_result(%rip), %rbx
	movq	%rbx, static_buf(%rip)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	last_result(%rip), %rbx
	jmp	.L84
	.size	_dlerror_run, .-_dlerror_run
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	fini, @function
fini:
	leaq	last_result(%rip), %rdi
	jmp	check_free
	.size	fini, .-fini
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	fini
	.text
	.p2align 4,,15
	.globl	__dlerror_main_freeres
	.hidden	__dlerror_main_freeres
	.type	__dlerror_main_freeres, @function
__dlerror_main_freeres:
	leaq	last_result(%rip), %rdi
	subq	$24, %rsp
	call	check_free
	movl	once(%rip), %eax
	testl	%eax, %eax
	je	.L109
	cmpq	$0, static_buf(%rip)
	jne	.L109
	cmpq	$0, __pthread_getspecific@GOTPCREL(%rip)
	je	.L109
	movl	key(%rip), %edi
	call	__pthread_getspecific@PLT
	testq	%rax, %rax
	je	.L109
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	check_free
	movq	8(%rsp), %rax
	movq	%rax, %rdi
	call	free@PLT
	cmpq	$0, __pthread_setspecific@GOTPCREL(%rip)
	je	.L109
	movl	key(%rip), %edi
	xorl	%esi, %esi
	addq	$24, %rsp
	jmp	__pthread_setspecific@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$24, %rsp
	ret
	.size	__dlerror_main_freeres, .-__dlerror_main_freeres
	.hidden	__GI__dlfcn_hook
	.globl	__GI__dlfcn_hook
	.bss
	.align 8
	.type	__GI__dlfcn_hook, @object
	.size	__GI__dlfcn_hook, 8
__GI__dlfcn_hook:
	.zero	8
	.globl	_dlfcn_hook
	.set	_dlfcn_hook,__GI__dlfcn_hook
	.local	once
	.comm	once,4,4
	.local	key
	.comm	key,4,4
	.local	static_buf
	.comm	static_buf,8,8
	.local	last_result
	.comm	last_result,32,32
	.weak	__pthread_getspecific
	.weak	__pthread_once
	.weak	__pthread_setspecific
	.weak	__pthread_key_create
