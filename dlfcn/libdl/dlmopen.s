	.text
#APP
	.section .gnu.warning.dlmopen
	.previous
#NO_APP
	.p2align 4,,15
	.globl	dlmopen
	.type	dlmopen, @function
dlmopen:
	movq	(%rsp), %rcx
	jmp	__dlmopen@PLT
	.size	dlmopen, .-dlmopen
	.section	.gnu.warning.dlmopen
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_dlmopen, @object
	.size	__evoke_link_warning_dlmopen, 131
__evoke_link_warning_dlmopen:
	.string	"Using 'dlmopen' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
