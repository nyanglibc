	.text
	.p2align 4,,15
	.type	init, @function
init:
	movl	%edi, __dlfcn_argc(%rip)
	movq	%rsi, __dlfcn_argv(%rip)
	ret
	.size	init, .-init
	.section	.init_array,"aw"
	.align 8
	.type	init_array, @object
	.size	init_array, 8
init_array:
	.quad	init
	.hidden	__dlfcn_argv
	.comm	__dlfcn_argv,8,8
	.hidden	__dlfcn_argc
	.comm	__dlfcn_argc,4,4
