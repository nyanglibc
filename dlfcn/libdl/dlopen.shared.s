	.text
#APP
	.symver __dlopen_check,dlopen@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"invalid mode parameter"
#NO_APP
	.text
	.p2align 4,,15
	.type	dlopen_doit, @function
dlopen_doit:
	pushq	%rbx
	movl	8(%rdi), %esi
	testl	$-1073746192, %esi
	jne	.L7
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	__environ@GOTPCREL(%rip), %rdx
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	orl	$-2147483648, %esi
	movq	__dlfcn_argv(%rip), %r9
	movl	__dlfcn_argc(%rip), %r8d
	movq	$-2, %rcx
	testq	%rdi, %rdi
	movq	(%rdx), %r10
	movq	24(%rbx), %rdx
	movq	760(%rax), %rax
	je	.L8
	subq	$8, %rsp
	pushq	%r10
	call	*%rax
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	leaq	.LC0(%rip), %rdi
	pushq	%r10
	call	*%rax
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
.L7:
	movq	_libc_intl_domainname@GOTPCREL(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$5, %edx
	call	__dcgettext@PLT
	xorl	%edx, %edx
	movq	%rax, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
	.size	dlopen_doit, .-dlopen_doit
	.p2align 4,,15
	.globl	__dlopen
	.hidden	__dlopen
	.type	__dlopen, @function
__dlopen:
	subq	$40, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L10
	movq	__GI__dlfcn_hook(%rip), %rax
	movq	40(%rsp), %rdx
	call	*(%rax)
.L9:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	40(%rsp), %rax
	movq	%rdi, (%rsp)
	leaq	dlopen_doit(%rip), %rdi
	movl	%esi, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rax, 24(%rsp)
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L13
	movq	16(%rsp), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	jmp	.L9
	.size	__dlopen, .-__dlopen
	.globl	__dlopen_check
	.set	__dlopen_check,__dlopen
	.hidden	_dlerror_run
	.hidden	__dlfcn_argc
	.hidden	__dlfcn_argv
