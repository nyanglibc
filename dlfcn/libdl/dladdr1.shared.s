	.text
	.p2align 4,,15
	.globl	__dladdr1
	.hidden	__dladdr1
	.type	__dladdr1, @function
__dladdr1:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L2
	movq	__GI__dlfcn_hook(%rip), %rax
	jmp	*48(%rax)
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1, %ecx
	je	.L4
	cmpl	$2, %ecx
	movl	$0, %ecx
	jne	.L6
	jmp	_dl_addr@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rdx, %rcx
.L6:
	xorl	%edx, %edx
	jmp	_dl_addr@PLT
	.size	__dladdr1, .-__dladdr1
	.globl	dladdr1
	.set	dladdr1,__dladdr1
