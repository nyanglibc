	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"invalid namespace"
.LC2:
	.string	"invalid mode"
	.text
	.p2align 4,,15
	.type	dlmopen_doit, @function
dlmopen_doit:
	pushq	%rbx
	movq	(%rdi), %rcx
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rcx, %rcx
	je	.L2
	testq	%rdi, %rdi
	je	.L10
	movl	16(%rbx), %esi
	testl	$256, %esi
	jne	.L11
	movq	__environ@GOTPCREL(%rip), %rdx
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	subq	$8, %rsp
	movq	__dlfcn_argv(%rip), %r9
	movl	__dlfcn_argc(%rip), %r8d
	orl	$-2147483648, %esi
	movq	(%rdx), %r10
	movq	760(%rax), %rax
	movq	32(%rbx), %rdx
	pushq	%r10
	call	*%rax
	movq	%rax, 24(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__environ@GOTPCREL(%rip), %rdx
	movl	16(%rbx), %esi
	leaq	.LC0(%rip), %r11
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	__dlfcn_argv(%rip), %r9
	movl	__dlfcn_argc(%rip), %r8d
	movq	(%rdx), %r10
	orl	$-2147483648, %esi
	testq	%rdi, %rdi
	cmove	%r11, %rdi
	subq	$8, %rsp
	movq	32(%rbx), %rdx
	movq	760(%rax), %rax
	pushq	%r10
	call	*%rax
	movq	%rax, 24(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$22, %edi
	call	_dl_signal_error@PLT
.L10:
	leaq	.LC1(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$22, %edi
	call	_dl_signal_error@PLT
	.size	dlmopen_doit, .-dlmopen_doit
	.p2align 4,,15
	.globl	__dlmopen
	.hidden	__dlmopen
	.type	__dlmopen, @function
__dlmopen:
	subq	$56, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L13
	movq	__GI__dlfcn_hook(%rip), %rax
	movq	56(%rsp), %rcx
	call	*64(%rax)
.L12:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	56(%rsp), %rax
	movq	%rdi, (%rsp)
	leaq	dlmopen_doit(%rip), %rdi
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rsi
	movl	%edx, 16(%rsp)
	movq	%rax, 32(%rsp)
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L16
	movq	24(%rsp), %rax
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	jmp	.L12
	.size	__dlmopen, .-__dlmopen
	.globl	dlmopen
	.set	dlmopen,__dlmopen
	.hidden	_dlerror_run
	.hidden	__dlfcn_argc
	.hidden	__dlfcn_argv
