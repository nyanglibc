	.text
	.p2align 4,,15
	.type	dlsym_doit, @function
dlsym_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rsi
	movq	(%rdi), %rdi
	call	_dl_sym@PLT
	movq	%rax, 24(%rbx)
	popq	%rbx
	ret
	.size	dlsym_doit, .-dlsym_doit
	.p2align 4,,15
	.globl	__dlsym
	.hidden	__dlsym
	.type	__dlsym, @function
__dlsym:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	je	.L10
	movq	56(%rsp), %rax
	movq	_rtld_global@GOTPCREL(%rip), %rbx
	xorl	%ebp, %ebp
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	2440(%rbx), %rdi
	call	*3984(%rbx)
	leaq	dlsym_doit(%rip), %rdi
	movq	%rsp, %rsi
	call	_dlerror_run
	testl	%eax, %eax
	je	.L11
.L7:
	leaq	2440(%rbx), %rdi
	call	*3992(%rbx)
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	24(%rsp), %rbp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__GI__dlfcn_hook(%rip), %rax
	movq	56(%rsp), %rdx
	call	*16(%rax)
	addq	$40, %rsp
	movq	%rax, %rbp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__dlsym, .-__dlsym
	.globl	dlsym
	.set	dlsym,__dlsym
	.hidden	_dlerror_run
