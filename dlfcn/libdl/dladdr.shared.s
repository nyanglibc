	.text
	.p2align 4,,15
	.globl	__dladdr
	.hidden	__dladdr
	.type	__dladdr, @function
__dladdr:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L2
	movq	__GI__dlfcn_hook(%rip), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_dl_addr@PLT
	.size	__dladdr, .-__dladdr
	.globl	dladdr
	.set	dladdr,__dladdr
