	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"invalid mode parameter"
	.text
	.p2align 4,,15
	.type	dlopen_doit, @function
dlopen_doit:
	pushq	%rbx
	movl	8(%rdi), %esi
	testl	$-1073746192, %esi
	jne	.L6
	movq	24(%rdi), %rdx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	leaq	.LC0(%rip), %rax
	orl	$-2147483648, %esi
	movq	__libc_argv(%rip), %r9
	movl	__libc_argc(%rip), %r8d
	testq	%rdi, %rdi
	cmove	%rax, %rdi
	subq	$8, %rsp
	pushq	__environ(%rip)
	xorl	%ecx, %ecx
	call	_dl_open
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
.L6:
	leaq	.LC1(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext
	xorl	%edx, %edx
	movq	%rax, %rcx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_signal_error
	.size	dlopen_doit, .-dlopen_doit
	.p2align 4,,15
	.globl	__dlopen
	.hidden	__dlopen
	.type	__dlopen, @function
__dlopen:
	subq	$40, %rsp
	movq	%rdi, (%rsp)
	leaq	dlopen_doit(%rip), %rdi
	movl	%esi, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rdx, 24(%rsp)
	call	_dlerror_run
	testl	%eax, %eax
	jne	.L9
	movq	16(%rsp), %rdi
	call	__libc_register_dl_open_hook
	movq	16(%rsp), %rdi
	call	__libc_register_dlfcn_hook
	movq	16(%rsp), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	addq	$40, %rsp
	ret
	.size	__dlopen, .-__dlopen
	.hidden	__libc_register_dlfcn_hook
	.hidden	__libc_register_dl_open_hook
	.hidden	_dlerror_run
	.hidden	_dl_signal_error
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	_dl_open
	.hidden	__libc_argc
	.hidden	__libc_argv
