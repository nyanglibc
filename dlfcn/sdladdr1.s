	.text
	.p2align 4,,15
	.globl	__dladdr1
	.hidden	__dladdr1
	.type	__dladdr1, @function
__dladdr1:
	cmpl	$1, %ecx
	je	.L3
	cmpl	$2, %ecx
	movl	$0, %ecx
	jne	.L5
	jmp	_dl_addr
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rdx, %rcx
.L5:
	xorl	%edx, %edx
	jmp	_dl_addr
	.size	__dladdr1, .-__dladdr1
	.hidden	_dl_addr
