	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_getgrent_r,getgrent_r@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"setgrent"
#NO_APP
	.text
	.p2align 4,,15
	.globl	setgrent
	.type	setgrent, @function
setgrent:
.LFB70:
	subq	$8, %rsp
#APP
# 124 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	pushq	$0
	leaq	last_nip(%rip), %r8
	pushq	$0
	leaq	startp(%rip), %rcx
	leaq	nip(%rip), %rdx
	leaq	__GI___nss_group_lookup2(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	xorl	%r9d, %r9d
	call	__nss_setent
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%fs:(%r8), %r9d
#APP
# 129 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L4
	subl	$1, lock(%rip)
.L5:
	movl	%r9d, %fs:(%r8)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 129 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L5
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 129 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L5
.LFE70:
	.size	setgrent, .-setgrent
	.section	.rodata.str1.1
.LC1:
	.string	"endgrent"
	.text
	.p2align 4,,15
	.globl	endgrent
	.type	endgrent, @function
endgrent:
.LFB71:
	cmpq	$0, startp(%rip)
	je	.L15
	subq	$8, %rsp
#APP
# 142 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L11:
	leaq	last_nip(%rip), %r8
	leaq	startp(%rip), %rcx
	leaq	nip(%rip), %rdx
	leaq	__GI___nss_group_lookup2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%r9d, %r9d
	call	__nss_endent
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%fs:(%r8), %r9d
#APP
# 146 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	movl	%r9d, %fs:(%r8)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 146 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 146 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L11
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L11
.LFE71:
	.size	endgrent, .-endgrent
	.section	.rodata.str1.1
.LC2:
	.string	"getgrent_r"
	.text
	.p2align 4,,15
	.globl	__getgrent_r
	.hidden	__getgrent_r
	.type	__getgrent_r, @function
__getgrent_r:
.LFB72:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
#APP
# 159 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	movl	$1, %edi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edi, lock(%rip)
# 0 "" 2
#NO_APP
.L20:
	subq	$8, %rsp
	leaq	last_nip(%rip), %r9
	leaq	startp(%rip), %r8
	pushq	$0
	leaq	.LC2(%rip), %rdi
	pushq	%rcx
	leaq	nip(%rip), %rcx
	pushq	%rdx
	leaq	__GI___nss_group_lookup2(%rip), %rdx
	pushq	%rsi
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	pushq	$0
	pushq	$0
	call	__nss_getent_r
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%eax, %r9d
	movl	%fs:(%r8), %ebx
#APP
# 165 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	addq	$64, %rsp
	testl	%eax, %eax
	jne	.L21
	subl	$1, lock(%rip)
.L22:
	movl	%ebx, %fs:(%r8)
	addq	$32, %rsp
	movl	%r9d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, lock(%rip)
	je	.L20
	leaq	lock(%rip), %rdi
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
#APP
# 165 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L22
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 165 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L22
.LFE72:
	.size	__getgrent_r, .-__getgrent_r
	.globl	__new_getgrent_r
	.set	__new_getgrent_r,__getgrent_r
	.local	lock
	.comm	lock,4,4
	.local	startp
	.comm	startp,8,8
	.local	last_nip
	.comm	last_nip,8,8
	.local	nip
	.comm	nip,8,8
	.hidden	__nss_getent_r
	.hidden	__nss_endent
	.hidden	__lll_lock_wait_private
	.hidden	__nss_setent
