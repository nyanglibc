	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__nss_files_parse_grent
	.hidden	__GI__nss_files_parse_grent
	.type	__GI__nss_files_parse_grent, @function
__GI__nss_files_parse_grent:
.LFB74:
	pushq	%r15
	pushq	%r14
	leaq	(%rdx,%rcx), %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%r8, %rbp
	subq	$24, %rsp
	cmpq	%rdi, %rdx
	ja	.L35
	cmpq	%rdi, %r15
	ja	.L95
.L35:
	movq	%r14, %r13
.L2:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	__GI_strchr@PLT
	testq	%rax, %rax
	je	.L3
	movb	$0, (%rax)
.L3:
	movq	%rbx, (%r12)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L87
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L4
.L87:
	cmpb	$58, %al
	jne	.L96
.L4:
	testb	%al, %al
	je	.L7
	movb	$0, (%rbx)
	addq	$1, %rbx
.L7:
	cmpb	$0, (%rbx)
	jne	.L8
	movq	(%r12), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L8
	testq	%r13, %r13
	movq	$0, 8(%r12)
	movl	$0, 16(%r12)
	je	.L97
.L23:
	addq	$7, %r13
	andq	$-8, %r13
	leaq	16(%r13), %rdi
	movq	%r13, %rcx
.L24:
	cmpq	%rdi, %r15
	jb	.L98
.L25:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L26
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rsi
	movsbq	%al, %rdx
	testb	$32, 1(%rsi,%rdx,2)
	je	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rsi,%rdx,2)
	movq	%rdx, %rax
	jne	.L28
	cmpb	$44, %dl
	je	.L29
	testb	%dl, %dl
	je	.L29
.L89:
	movq	%rbx, %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$44, %al
	je	.L32
.L30:
	movq	%rbx, %rdx
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L99
.L32:
	cmpq	%rsi, %rbx
	jbe	.L29
	movq	%rsi, (%rcx)
	movzbl	1(%rdx), %eax
	addq	$8, %rcx
	leaq	16(%rcx), %rdi
.L29:
	testb	%al, %al
	je	.L24
.L34:
	movb	$0, (%rbx)
	addq	$1, %rbx
	cmpq	%rdi, %r15
	jnb	.L25
.L98:
	movl	$34, 0(%rbp)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, 8(%r12)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L88
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L10
.L88:
	cmpb	$58, %al
	jne	.L100
.L10:
	testb	%al, %al
	jne	.L101
	movq	(%r12), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	jne	.L14
.L103:
	cmpb	$0, (%rbx)
	jne	.L102
.L20:
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movb	$0, (%rbx)
	movq	(%r12), %rax
	addq	$1, %rbx
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L103
.L14:
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%rbx, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movl	%eax, 16(%r12)
	movq	8(%rsp), %rax
	cmpq	%rbx, %rax
	je	.L20
.L93:
	movzbl	(%rax), %edx
	cmpb	$58, %dl
	je	.L104
	testb	%dl, %dl
	jne	.L20
	movq	%rax, %rbx
.L106:
	testq	%r13, %r13
	jne	.L23
.L97:
	cmpq	%rbx, %r15
	jbe	.L36
	cmpq	%rbx, %r14
	jbe	.L105
.L36:
	movq	%r14, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L95:
	call	__GI_strlen@PLT
	leaq	1(%rbx,%rax), %r13
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	$44, %al
	jne	.L89
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r13, %r13
	movq	$0, (%rcx)
	je	.L37
	movq	%r13, 24(%r12)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%rbx, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movl	%eax, 16(%r12)
	movq	8(%rsp), %rax
	cmpq	%rbx, %rax
	jne	.L93
	movl	$0, 16(%r12)
	jmp	.L93
.L104:
	addq	$1, %rax
	movq	%rax, %rbx
	jmp	.L106
.L105:
	movq	%rbx, %rdi
	call	__GI_strlen@PLT
	leaq	1(%rbx,%rax), %r13
	jmp	.L23
.L37:
	movl	$-1, %eax
	jmp	.L1
.LFE74:
	.size	__GI__nss_files_parse_grent, .-__GI__nss_files_parse_grent
	.globl	_nss_files_parse_grent
	.set	_nss_files_parse_grent,__GI__nss_files_parse_grent
	.p2align 4,,15
	.globl	__fgetgrent_r
	.hidden	__fgetgrent_r
	.type	__fgetgrent_r, @function
__fgetgrent_r:
.LFB75:
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	leaq	__GI__nss_files_parse_grent(%rip), %r8
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__nss_fgetent_r
	testl	%eax, %eax
	movl	$0, %edx
	cmovne	%rdx, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE75:
	.size	__fgetgrent_r, .-__fgetgrent_r
	.weak	fgetgrent_r
	.set	fgetgrent_r,__fgetgrent_r
	.hidden	__nss_fgetent_r
