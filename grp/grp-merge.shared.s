	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___copy_grp
	.hidden	__GI___copy_grp
	.type	__GI___copy_grp, @function
__GI___copy_grp:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	128(%rsp), %eax
	movq	112(%rsp), %r15
	movq	%rsi, 24(%rsp)
	movq	%rcx, 32(%rsp)
	movq	120(%rsp), %rbx
	movq	136(%rsp), %r13
	movl	%eax, 16(%rsi)
	movq	%r15, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rbp
	cmpq	%r12, %rbp
	ja	.L28
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__GI_memcpy@PLT
	movq	24(%rsp), %rax
	movq	%rbx, %rdi
	movq	%r14, (%rax)
	call	__GI_strlen
	leaq	1(%rax), %rdx
	leaq	0(%rbp,%rdx), %r15
	cmpq	%r12, %r15
	ja	.L28
	addq	%r14, %rbp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI_memcpy@PLT
	movq	24(%rsp), %rax
	movq	%rbp, 8(%rax)
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L5
	xorl	%eax, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rbp, %rax
.L6:
	leaq	1(%rax), %rbp
	cmpq	$0, 0(%r13,%rbp,8)
	jne	.L14
	leaq	16(,%rax,8), %rax
	movq	%rsi, 8(%rsp)
	xorl	%ebx, %ebx
	movq	%rax, %rdi
	movq	%rax, 40(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	8(%rsp), %rsi
	jne	.L9
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L8:
	addq	%r14, %r15
	movq	%r8, 8(%rsp)
	movq	%r15, %rdi
	call	__GI_memcpy@PLT
	movq	16(%rsp), %rax
	movq	8(%rsp), %r8
	movq	%r15, (%rax,%rbx)
	addq	$8, %rbx
	movq	%r8, %r15
	movq	0(%r13,%rbx), %rsi
	testq	%rsi, %rsi
	je	.L13
.L9:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	__GI_strlen
	leaq	1(%rax), %rdx
	movq	8(%rsp), %rsi
	leaq	(%rdx,%r15), %r8
	cmpq	%r12, %r8
	jbe	.L8
.L29:
	movq	16(%rsp), %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$34, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L12
	movq	%r15, %r8
	xorl	%ebp, %ebp
	movq	$8, 40(%rsp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L13:
	movq	16(%rsp), %rax
	leaq	(%r14,%r8), %rdi
	movq	$0, (%rax,%rbx)
	movq	%rdi, %rax
	andl	$7, %eax
	je	.L10
	addq	$8, %r8
	subq	%rax, %r8
	leaq	(%r14,%r8), %rdi
.L10:
	movq	24(%rsp), %rax
	movq	%rdi, 24(%rax)
	movq	40(%rsp), %rax
	leaq	(%r8,%rax), %rbx
	cmpq	%r12, %rbx
	ja	.L29
	movq	16(%rsp), %r15
	movq	40(%rsp), %rdx
	movq	%r15, %rsi
	call	__GI_memcpy@PLT
	movq	%r15, %rdi
	call	free@PLT
	leaq	8(%rbx), %rdx
	cmpq	%r12, %rdx
	ja	.L28
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
	movq	%rbp, (%r14,%rbx)
	testq	%rcx, %rcx
	je	.L1
	addq	%rdx, %r14
	movq	%r14, (%rcx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$12, %eax
	jmp	.L1
	.size	__GI___copy_grp, .-__GI___copy_grp
	.globl	__copy_grp
	.set	__copy_grp,__GI___copy_grp
	.p2align 4,,15
	.globl	__GI___merge_grp
	.hidden	__GI___merge_grp
	.type	__GI___merge_grp, @function
__GI___merge_grp:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	subq	$56, %rsp
	movl	16(%rdi), %eax
	cmpl	%eax, 16(%r8)
	movq	%rsi, 8(%rsp)
	movq	%rcx, (%rsp)
	movq	%r9, 16(%rsp)
	jne	.L52
	movq	(%rdi), %rsi
	movq	(%r8), %rdi
	movq	%rdx, %rbx
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L52
	movq	24(%rbp), %rcx
	movq	-8(%rbx), %r15
	movq	(%rcx), %r13
	testq	%r13, %r13
	je	.L42
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$1, %rax
	cmpq	$0, (%rcx,%rax,8)
	jne	.L36
	addq	%r15, %rax
.L35:
	leaq	8(,%rax,8), %rax
	movq	%rax, %rdi
	movq	%rax, 40(%rsp)
	call	malloc@PLT
	movq	%rax, %rcx
	movl	$12, %eax
	testq	%rcx, %rcx
	je	.L31
	movq	24(%r12), %rsi
	salq	$3, %r15
	movq	%rcx, %rdi
	movq	%r15, %rdx
	movq	%rcx, 32(%rsp)
	call	__GI_memcpy@PLT
	subq	8(%rsp), %rbx
	subq	%r15, %rbx
	testq	%r13, %r13
	leaq	-16(%rbx), %r14
	je	.L44
	addq	32(%rsp), %r15
	movl	$8, %ebx
	movq	%r15, 24(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L38:
	addq	8(%rsp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__GI_memcpy@PLT
	movq	24(%rsp), %rax
	movq	%r14, -8(%rax,%rbx)
	movq	24(%rbp), %rax
	movq	%r15, %r14
	movq	(%rax,%rbx), %r13
	addq	$8, %rbx
	testq	%r13, %r13
	je	.L37
.L39:
	movq	%r13, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rdx
	leaq	(%rdx,%r14), %r15
	cmpq	%r15, (%rsp)
	jnb	.L38
.L53:
	movq	32(%rsp), %rdi
	call	free@PLT
	addq	$56, %rsp
	movl	$34, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L44:
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L37:
	movq	32(%rsp), %rax
	movq	40(%rsp), %rsi
	movq	$0, -8(%rax,%rsi)
	movq	8(%rsp), %rsi
	leaq	(%rsi,%r15), %rdi
	movq	%rdi, %rax
	andl	$7, %eax
	je	.L40
	leaq	8(%r15), %r9
	subq	%rax, %r9
	leaq	(%rsi,%r9), %rdi
	movq	%r9, %r15
.L40:
	movq	40(%rsp), %rax
	movq	%rdi, 24(%r12)
	leaq	(%rax,%r15), %r9
	cmpq	%r9, (%rsp)
	jb	.L53
	movq	32(%rsp), %rbx
	movq	40(%rsp), %rdx
	movq	%rbx, %rsi
	call	__GI_memcpy@PLT
	movq	%rbx, %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	pushq	24(%r12)
	pushq	16(%r12)
	xorl	%ecx, %ecx
	pushq	8(%r12)
	pushq	(%r12)
	movq	%rbp, %rsi
	movq	48(%rsp), %rdx
	movq	32(%rsp), %rdi
	call	__GI___copy_grp
	addq	$32, %rsp
.L31:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r15, %rax
	jmp	.L35
	.size	__GI___merge_grp, .-__GI___merge_grp
	.globl	__merge_grp
	.set	__merge_grp,__GI___merge_grp
