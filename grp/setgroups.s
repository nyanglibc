	.text
	.p2align 4,,15
	.globl	setgroups
	.hidden	setgroups
	.type	setgroups, @function
setgroups:
	cmpq	$0, __nptl_setxid@GOTPCREL(%rip)
	jne	.L11
	movl	$116, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/setgroups.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$56, %rsp
	movq	%rdi, 8(%rsp)
	movq	%rsp, %rdi
	movl	$116, (%rsp)
	movq	%rsi, 16(%rsp)
	call	__nptl_setxid@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	setgroups, .-setgroups
	.weak	__nptl_setxid
	.hidden	__nptl_setxid
