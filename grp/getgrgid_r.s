	.text
#APP
	.section .gnu.warning.getgrgid_r
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getgrgid_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__getgrgid_r
	.hidden	__getgrgid_r
	.type	__getgrgid_r, @function
__getgrgid_r:
.LFB70:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	leaq	.LC0(%rip), %rsi
	movq	%rdx, %r13
	xorl	%edx, %edx
	subq	$136, %rsp
	leaq	72(%rsp), %rax
	leaq	88(%rsp), %rcx
	movl	%edi, 40(%rsp)
	movq	%r8, 56(%rsp)
	movq	$0, 80(%rsp)
	movq	%rax, %rdi
	movq	%rcx, 8(%rsp)
	movq	%rax, 16(%rsp)
	call	__nss_group_lookup2
	testl	%eax, %eax
	movl	%eax, 44(%rsp)
	jne	.L2
	movq	__libc_errno@gottpoff(%rip), %r15
	xorl	%ebx, %ebx
	movq	$0, 24(%rsp)
	movq	%r15, %rax
	addq	%fs:0, %rax
	movq	%rax, (%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 48(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L12:
	movq	88(%rsp), %rdi
	call	_dl_mcount_wrapper_check
	movq	(%rsp), %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movl	40(%rsp), %edi
	call	*88(%rsp)
	cmpl	$-2, %eax
	movl	%eax, %r14d
	je	.L44
	testl	%ebx, %ebx
	je	.L6
	cmpl	$1, %eax
	jne	.L7
	movq	80(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	%r13, %r9
	movq	32(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rcx
	call	__merge_grp
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L8
	cmpl	$34, %eax
	movl	%eax, %fs:(%r15)
	movl	$0, %ebx
	movl	$-1, %r14d
	jne	.L9
	movl	$-2, %r14d
	.p2align 4,,10
	.p2align 3
.L9:
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%r14d, %r8d
	call	__nss_next2
	testl	%eax, %eax
	je	.L12
	movq	24(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r14d
	movq	56(%rsp), %rax
	jne	.L45
	movq	%rbp, (%rax)
.L14:
	movl	44(%rsp), %eax
	movl	%eax, %fs:(%r15)
.L1:
	movl	44(%rsp), %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	$34, %fs:(%r15)
	je	.L46
	testl	%ebx, %ebx
	je	.L9
.L7:
	pushq	120(%rsp)
	pushq	120(%rsp)
	xorl	%ecx, %ecx
	pushq	120(%rsp)
	pushq	120(%rsp)
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	movl	$1, %ebx
	call	__copy_grp
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L8
	movl	%eax, %fs:(%r15)
.L8:
	movq	72(%rsp), %rax
	movl	$1, %r14d
	movl	8(%rax), %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L9
.L16:
	cmpq	$0, 24(%rsp)
	je	.L47
.L10:
	pushq	24(%rbp)
	pushq	16(%rbp)
	movq	%r12, %rdi
	pushq	8(%rbp)
	pushq	0(%rbp)
	movq	80(%rsp), %rcx
	movq	56(%rsp), %rdx
	movq	64(%rsp), %rsi
	call	__copy_grp
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L19
	xorl	%r14d, %r14d
	cmpl	$34, %eax
	movl	%eax, %fs:(%r15)
	setne	%r14b
	movl	$1, %ebx
	subl	$2, %r14d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	leal	4(%rax,%rax), %ecx
	movq	72(%rsp), %rax
	movl	8(%rax), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L9
	cmpl	$1, %r14d
	jne	.L9
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %r14d
	movl	$1, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rsp)
	jne	.L10
	movq	56(%rsp), %rax
	movl	$12, %fs:(%r15)
	movl	$12, 44(%rsp)
	movq	$0, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	movq	$0, (%rax)
	jbe	.L14
	movl	%fs:(%r15), %eax
	cmpl	$34, %eax
	movl	%eax, 44(%rsp)
	jne	.L1
	cmpl	$-2, %r14d
	je	.L1
.L21:
	movl	$22, 44(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L46:
	movq	24(%rsp), %rdi
	call	free@PLT
	movq	56(%rsp), %rax
	movl	$34, 44(%rsp)
	movq	$0, (%rax)
	jmp	.L1
.L2:
	movq	56(%rsp), %rax
	movq	__libc_errno@gottpoff(%rip), %r15
	movq	$0, (%rax)
	movl	%fs:(%r15), %eax
	cmpl	$34, %eax
	movl	%eax, 44(%rsp)
	jne	.L1
	jmp	.L21
.LFE70:
	.size	__getgrgid_r, .-__getgrgid_r
	.globl	__new_getgrgid_r
	.set	__new_getgrgid_r,__getgrgid_r
	.weak	getgrgid_r
	.set	getgrgid_r,__new_getgrgid_r
	.section	.gnu.warning.getgrgid_r
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getgrgid_r, @object
	.size	__evoke_link_warning_getgrgid_r, 134
__evoke_link_warning_getgrgid_r:
	.string	"Using 'getgrgid_r' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.hidden	__copy_grp
	.hidden	__nss_next2
	.hidden	__merge_grp
	.hidden	_dl_mcount_wrapper_check
	.hidden	__nss_group_lookup2
