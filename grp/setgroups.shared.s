	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_setgroups
	.hidden	__GI_setgroups
	.type	__GI_setgroups, @function
__GI_setgroups:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	jne	.L11
	movl	$116, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/setgroups.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$56, %rsp
	movq	224+__libc_pthread_functions(%rip), %rax
	movq	%rdi, 8(%rsp)
	movl	$116, (%rsp)
	movq	%rsp, %rdi
	movq	%rsi, 16(%rsp)
#APP
# 33 "../sysdeps/unix/sysv/linux/setgroups.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI_setgroups, .-__GI_setgroups
	.globl	setgroups
	.set	setgroups,__GI_setgroups
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
