	.text
#APP
	.section .gnu.warning.getgrouplist
	.previous
	.section .gnu.warning.initgroups
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getgrent_r"
.LC1:
	.string	"setgrent"
.LC2:
	.string	"endgrent"
#NO_APP
	.text
	.p2align 4,,15
	.type	compat_call.constprop.1, @function
compat_call.constprop.1:
.LFB86:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	leaq	.LC0(%rip), %rsi
	subq	$1144, %rsp
	movq	(%r9), %r13
	movl	%edx, 20(%rsp)
	movq	%rcx, 24(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 56(%rsp)
	call	__nss_lookup_function
	testq	%rax, %rax
	je	.L22
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L5
	movq	%rax, %rdi
	call	_dl_mcount_wrapper_check
	call	*%r14
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L5
.L1:
	addq	$1144, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	__nss_lookup_function
	movq	%rax, 48(%rsp)
	leaq	96(%rsp), %rax
	movq	__libc_errno@gottpoff(%rip), %r14
	addq	%fs:0, %r14
	movq	$1024, 104(%rsp)
	movq	%rax, 40(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r12, %rdi
	call	_dl_mcount_wrapper_check
	movq	%r14, %rcx
	movq	104(%rsp), %rdx
	movq	96(%rsp), %rsi
	movq	8(%rsp), %rdi
	call	*%r12
	cmpl	$-2, %eax
	movl	%eax, %ebx
	je	.L41
	cmpl	$1, %eax
	jne	.L9
	movl	80(%rsp), %ebx
	cmpl	20(%rsp), %ebx
	je	.L4
	movq	88(%rsp), %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L20
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$8, %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4
.L20:
	movq	%rbp, %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L11
	movq	24(%rsp), %rax
	movq	(%rax), %rdx
	cmpq	$0, %rdx
	jle	.L12
	cmpl	0(%r13), %ebx
	je	.L4
	xorl	%eax, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	0(%r13,%rax,4), %ebx
	je	.L4
.L13:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L14
.L15:
	movq	32(%rsp), %rax
	cmpq	(%rax), %rdx
	je	.L42
.L16:
	movq	24(%rsp), %rax
	movl	%ebx, 0(%r13,%rdx,4)
	addq	$1, %rdx
	movq	%rdx, (%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L41:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	je	.L8
.L9:
	movl	$1, %ebx
.L6:
	movq	40(%rsp), %rax
	movq	96(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L21
	call	free@PLT
.L21:
	movq	48(%rsp), %r14
	testq	%r14, %r14
	je	.L1
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check
	movq	%r14, %rax
	call	*%rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	40(%rsp), %rdi
	call	__libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L4
	jmp	.L6
.L12:
	jne	.L4
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	$0, 1200(%rsp)
	jle	.L17
	cmpq	1200(%rsp), %rdx
	je	.L9
	addq	%rdx, %rdx
	cmpq	1200(%rsp), %rdx
	cmovg	1200(%rsp), %rdx
	movq	%rdx, %rbx
.L19:
	leaq	0(,%rbx,4), %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L9
	movq	56(%rsp), %rax
	movq	%r13, (%rax)
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
	movq	24(%rsp), %rax
	movl	80(%rsp), %ebx
	movq	(%rax), %rdx
	jmp	.L16
.L17:
	leaq	(%rdx,%rdx), %rbx
	jmp	.L19
.L22:
	movl	$-1, %ebx
	jmp	.L1
.LFE86:
	.size	compat_call.constprop.1, .-compat_call.constprop.1
	.section	.rodata.str1.1
.LC3:
	.string	"initgroups.c"
.LC4:
	.string	"*size > 0"
.LC5:
	.string	"files"
.LC6:
	.string	"initgroups_dyn"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Illegal status in internal_getgrouplist.\n"
	.text
	.p2align 4,,15
	.type	internal_getgrouplist, @function
internal_getgrouplist:
.LFB82:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpq	$0, (%rdx)
	movq	%rdi, 8(%rsp)
	jle	.L94
	movq	(%rcx), %rax
	leaq	24(%rsp), %r14
	movl	%esi, %ebx
	movl	$5, %edi
	movq	%rdx, %rbp
	movq	%rcx, %r12
	movq	%r8, %r13
	movl	%esi, (%rax)
	movq	%r14, %rsi
	movq	$1, 16(%rsp)
	call	__nss_database_get
	testb	%al, %al
	je	.L45
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L45
	movb	$1, use_initgroups_entry(%rip)
.L50:
	cmpq	$0, (%rdi)
	leaq	16(%rsp), %r14
	je	.L48
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC6(%rip), %rsi
	movq	16(%rsp), %r15
	call	__nss_lookup_function
	testq	%rax, %rax
	je	.L95
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	_dl_mcount_wrapper_check
	movq	__libc_errno@gottpoff(%rip), %rdx
	addq	%fs:0, %rdx
	subq	$8, %rsp
	movq	%rbp, %rcx
	movq	%r13, %r9
	movq	%r12, %r8
	movl	%ebx, %esi
	pushq	%rdx
	movq	%r14, %rdx
	movq	24(%rsp), %rdi
	movq	16(%rsp), %rax
	call	*%rax
	popq	%rdx
	popq	%rcx
.L52:
	movq	16(%rsp), %r8
	movq	%r15, %rdi
	xorl	%r11d, %r11d
.L53:
	cmpq	%r8, %rdi
	jge	.L57
.L96:
	testq	%r15, %r15
	jle	.L56
	movq	(%r12), %rcx
	leaq	(%rcx,%rdi,4), %r9
	movl	(%r9), %esi
	cmpl	(%rcx), %esi
	je	.L54
	xorl	%edx, %edx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	%esi, (%rcx,%rdx,4)
	je	.L54
.L58:
	addq	$1, %rdx
	cmpq	%rdx, %r15
	jne	.L55
.L56:
	addq	$1, %rdi
	cmpq	%r8, %rdi
	jl	.L96
.L57:
	testb	%r11b, %r11b
	jne	.L97
.L59:
	leal	2(%rax), %ecx
	cmpl	$4, %ecx
	ja	.L98
	cmpb	$0, use_initgroups_entry(%rip)
	movq	24(%rsp), %rdx
	jne	.L64
	cmpl	$1, %eax
	je	.L61
.L64:
	movl	8(%rdx), %eax
	addl	%ecx, %ecx
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L48
.L61:
	leaq	16(%rdx), %rdi
	testq	%rdi, %rdi
	movq	%rdi, 24(%rsp)
	je	.L48
	cmpq	$0, 16(%rdx)
	jne	.L47
.L48:
	movq	16(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L45:
	movq	%r14, %rsi
	movl	$2, %edi
	call	__nss_database_get
	testb	%al, %al
	je	.L49
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L49
	movb	$0, use_initgroups_entry(%rip)
	jmp	.L50
.L49:
	leaq	.LC5(%rip), %rdi
	call	__nss_action_parse@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, 24(%rsp)
	movb	$0, use_initgroups_entry(%rip)
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L54:
	subq	$1, %r8
	movl	$1, %r11d
	movl	(%rcx,%r8,4), %edx
	movl	%edx, (%r9)
	jmp	.L53
.L95:
	subq	$8, %rsp
	movq	%r12, %r9
	movq	%rbp, %r8
	pushq	%r13
	movq	24(%rsp), %rsi
	movq	%r14, %rcx
	movq	40(%rsp), %rdi
	movl	%ebx, %edx
	call	compat_call.constprop.1
	popq	%rsi
	popq	%rdi
	jmp	.L52
.L97:
	movq	%r8, 16(%rsp)
	jmp	.L59
.L98:
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
.L94:
	leaq	__PRETTY_FUNCTION__.13359(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$68, %edx
	call	__assert_fail
.LFE82:
	.size	internal_getgrouplist, .-internal_getgrouplist
	.p2align 4,,15
	.globl	getgrouplist
	.type	getgrouplist, @function
getgrouplist:
.LFB83:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	$1, %edi
	movl	%esi, %r12d
	movq	%rdx, %rbx
	movl	$-1, %r14d
	subq	$16, %rsp
	movl	(%rcx), %eax
	testl	%eax, %eax
	cmovg	(%rcx), %edi
	movslq	%edi, %rdi
	movq	%rdi, (%rsp)
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L99
	leaq	8(%rsp), %rcx
	movq	$-1, %r8
	movl	%r12d, %esi
	movq	%rbp, %rdi
	movq	%rsp, %rdx
	call	internal_getgrouplist
	cmpl	%eax, 0(%r13)
	movl	%eax, %edx
	movq	8(%rsp), %r12
	cmovle	0(%r13), %edx
	movq	%rbx, %rdi
	movl	%eax, %ebp
	movq	%r12, %rsi
	movslq	%edx, %rdx
	salq	$2, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	call	free@PLT
	cmpl	%ebp, 0(%r13)
	movl	%ebp, 0(%r13)
	cmovge	%ebp, %r14d
.L99:
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.LFE83:
	.size	getgrouplist, .-getgrouplist
	.p2align 4,,15
	.globl	initgroups
	.type	initgroups, @function
initgroups:
.LFB84:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$3, %edi
	subq	$32, %rsp
	call	__sysconf
	testq	%rax, %rax
	movq	%rax, %r12
	jle	.L107
	cmpq	$64, %rax
	movl	$64, %edi
	cmovle	%rax, %rdi
	movq	%rdi, 16(%rsp)
	salq	$2, %rdi
.L108:
	call	malloc@PLT
	movq	%rax, %rdx
	movq	%rax, 24(%rsp)
	movl	$-1, %eax
	testq	%rdx, %rdx
	je	.L106
	leaq	24(%rsp), %rcx
	leaq	16(%rsp), %rdx
	movl	%ebp, %esi
	movq	%rbx, %rdi
	movq	%r12, %r8
	call	internal_getgrouplist
	movslq	%eax, %rbp
	movq	%rbp, %rbx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L116:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$22, %fs:(%rdx)
	jne	.L110
	subl	$1, %ebx
	subq	$1, %rbp
	testl	%ebx, %ebx
	jle	.L110
.L111:
	movq	24(%rsp), %rsi
	movq	%rbp, %rdi
	call	setgroups
	cmpl	$-1, %eax
	je	.L116
.L110:
	movq	24(%rsp), %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %eax
.L106:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movq	$16, 16(%rsp)
	movl	$64, %edi
	jmp	.L108
.LFE84:
	.size	initgroups, .-initgroups
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.13359, @object
	.size	__PRETTY_FUNCTION__.13359, 22
__PRETTY_FUNCTION__.13359:
	.string	"internal_getgrouplist"
	.section	.gnu.warning.initgroups
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_initgroups, @object
	.size	__evoke_link_warning_initgroups, 134
__evoke_link_warning_initgroups:
	.string	"Using 'initgroups' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	.gnu.warning.getgrouplist
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getgrouplist, @object
	.size	__evoke_link_warning_getgrouplist, 136
__evoke_link_warning_getgrouplist:
	.string	"Using 'getgrouplist' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.local	use_initgroups_entry
	.comm	use_initgroups_entry,1,1
	.hidden	setgroups
	.hidden	__sysconf
	.hidden	__assert_fail
	.hidden	__libc_fatal
	.hidden	__nss_database_get
	.hidden	__libc_scratch_buffer_grow
	.hidden	strcmp
	.hidden	_dl_mcount_wrapper_check
	.hidden	__nss_lookup_function
