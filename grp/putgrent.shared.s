	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%s"
.LC2:
	.string	",%s"
.LC3:
	.string	"%s:%s::"
.LC4:
	.string	"%s:%s:%lu:"
#NO_APP
	.text
	.p2align 4,,15
	.globl	putgrent
	.type	putgrent, @function
putgrent:
.LFB64:
	testq	%rdi, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L5
	testq	%rsi, %rsi
	je	.L5
	movq	%rdi, %rbp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	%rsi, %r12
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	8(%rbp), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L5
	movq	24(%rbp), %rdi
	call	__nss_valid_list_field
	testb	%al, %al
	je	.L5
	movl	(%r12), %edx
	andl	$32768, %edx
	jne	.L6
	movq	136(%r12), %rdi
	movq	%fs:16, %rbx
	cmpq	%rbx, 8(%rdi)
	je	.L7
#APP
# 45 "putgrent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L8
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L9:
	movq	136(%r12), %rdi
	movq	%rbx, 8(%rdi)
.L7:
	addl	$1, 4(%rdi)
.L6:
	movq	0(%rbp), %rdx
	movq	8(%rbp), %rcx
	movzbl	(%rdx), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L60
	movl	16(%rbp), %r8d
	leaq	.LC0(%rip), %rax
	testq	%rcx, %rcx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__GI_fprintf
.L12:
	testl	%eax, %eax
	js	.L58
	movq	24(%rbp), %rax
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L18
	xorl	%ebx, %ebx
	leaq	.LC2(%rip), %r14
	leaq	.LC1(%rip), %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L20:
	movq	24(%rbp), %rax
	addq	$1, %rbx
	movq	(%rax,%rbx,8), %rdx
	testq	%rdx, %rdx
	je	.L18
.L22:
	testq	%rbx, %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	cmovne	%r14, %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	testl	%eax, %eax
	jns	.L20
.L58:
	testl	$32768, (%r12)
	jne	.L50
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L50
	movq	$0, 8(%rdi)
#APP
# 56 "putgrent.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L21
	subl	$1, (%rdi)
.L50:
	movl	$-1, %r8d
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC0(%rip), %rax
	testq	%rcx, %rcx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	__GI_fprintf
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	movq	40(%r12), %rax
	cmpq	48(%r12), %rax
	jnb	.L61
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r12)
	movb	$10, (%rax)
	movl	(%r12), %r8d
	andl	$32768, %r8d
	jne	.L29
.L26:
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 73 "putgrent.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L27
	subl	$1, (%rdi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%r8d, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$10, %esi
	movq	%r12, %rdi
	call	__GI___overflow
	sarl	$31, %eax
	testl	$32768, (%r12)
	movl	%eax, %r8d
	jne	.L1
	jmp	.L26
.L21:
#APP
# 66 "putgrent.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L50
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 66 "putgrent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	$-1, %r8d
	jmp	.L1
.L8:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L9
	call	__lll_lock_wait_private
	jmp	.L9
.L27:
#APP
# 73 "putgrent.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 73 "putgrent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.LFE64:
	.size	putgrent, .-putgrent
	.hidden	__lll_lock_wait_private
	.hidden	__nss_valid_list_field
	.hidden	__nss_valid_field
