	.text
	.p2align 4,,15
	.globl	__cache_sysconf
	.hidden	__cache_sysconf
	.type	__cache_sysconf, @function
__cache_sysconf:
	subl	$185, %edi
	cmpl	$12, %edi
	ja	.L15
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L3-.L4
	.long	.L15-.L4
	.long	.L15-.L4
	.long	.L5-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.long	.L11-.L4
	.long	.L12-.L4
	.long	.L13-.L4
	.long	.L14-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L14:
	movq	424+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	344+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	352+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	360+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	368+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	376+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	384+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	392+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	400+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	408+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	416+_dl_x86_cpu_features(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	$-1, %rax
	ret
	.size	__cache_sysconf, .-__cache_sysconf
