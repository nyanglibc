	.text
	.p2align 4,,15
	.globl	__argz_stringify
	.hidden	__argz_stringify
	.type	__argz_stringify, @function
__argz_stringify:
	testq	%rsi, %rsi
	je	.L8
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	1(%rdx), %rbp
	movb	%r12b, (%rdx)
.L3:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__strnlen
	subq	%rax, %rbx
	leaq	0(%rbp,%rax), %rdx
	movq	%rbx, %rax
	leaq	-1(%rbx), %rbx
	cmpq	$1, %rax
	ja	.L12
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	rep ret
	.size	__argz_stringify, .-__argz_stringify
	.weak	argz_stringify
	.set	argz_stringify,__argz_stringify
	.hidden	__strnlen
