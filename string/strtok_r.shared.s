	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtok_r
	.hidden	__GI___strtok_r
	.type	__GI___strtok_r, @function
__GI___strtok_r:
	testq	%rdi, %rdi
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L12
.L2:
	cmpb	$0, (%rbx)
	je	.L9
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	__GI_strspn
	addq	%rax, %rbx
	cmpb	$0, (%rbx)
	je	.L9
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_strcspn
	addq	%rbx, %rax
	cmpb	$0, (%rax)
	je	.L10
	movb	$0, (%rax)
	addq	$1, %rax
.L10:
	movq	%rax, 0(%rbp)
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rdx), %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, 0(%rbp)
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI___strtok_r, .-__GI___strtok_r
	.globl	__strtok_r
	.set	__strtok_r,__GI___strtok_r
	.weak	strtok_r
	.set	strtok_r,__strtok_r
