	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"strcoll_l.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((uintptr_t) table) % __alignof__ (table[0]) == 0"
	.align 8
.LC2:
	.string	"((uintptr_t) indirect) % __alignof__ (indirect[0]) == 0"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___strcoll_l
	.hidden	__GI___strcoll_l
	.type	__GI___strcoll_l, @function
__GI___strcoll_l:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$232, %rsp
	movq	24(%rdx), %rax
	movl	64(%rax), %ebx
	testq	%rbx, %rbx
	movl	%ebx, 12(%rsp)
	movq	%rbx, 200(%rsp)
	je	.L263
	movzbl	(%rdi), %edx
	movzbl	(%rsi), %ecx
	testb	%dl, %dl
	je	.L3
	testb	%cl, %cl
	je	.L3
	movq	%rdi, 208(%rsp)
	movq	72(%rax), %rdi
	movq	96(%rax), %rbx
	movq	88(%rax), %r10
	movq	%rsi, 216(%rsp)
	movq	%rdi, (%rsp)
	movq	80(%rax), %rdi
	movq	104(%rax), %rax
	movq	%rbx, 40(%rsp)
	movq	%rdi, 16(%rsp)
	andl	$3, %edi
	movq	%rax, 64(%rsp)
	jne	.L264
	testb	$3, 64(%rsp)
	jne	.L265
	movq	$0, 184(%rsp)
	movq	$0, 88(%rsp)
	xorl	%eax, %eax
	movb	$0, 198(%rsp)
	movq	$0, 104(%rsp)
	movq	%r10, %r14
.L115:
	imulq	200(%rsp), %rax
	movq	184(%rsp), %rdi
	movq	(%rsp), %rdx
	xorl	%r12d, %r12d
	movq	216(%rsp), %rsi
	movq	208(%rsp), %r13
	movq	%r14, %r15
	movq	$-1, 56(%rsp)
	movq	$0, 96(%rsp)
	addq	%rdi, %rdx
	movl	$0, 176(%rsp)
	movl	$0, 168(%rsp)
	movq	$0, 112(%rsp)
	movl	%edi, 172(%rsp)
	movzbl	(%rdx,%rax), %eax
	andl	$4, %eax
	movb	%al, 199(%rsp)
	movq	$-1, %rax
	movq	%rax, 32(%rsp)
	movq	%rax, 48(%rsp)
	movq	%rax, 24(%rsp)
	xorl	%eax, %eax
	testl	%eax, %eax
	jne	.L134
.L285:
	movl	168(%rsp), %eax
	movl	172(%rsp), %r14d
	movq	%r13, 128(%rsp)
	movq	$0, 80(%rsp)
	movq	%rsi, 136(%rsp)
	movl	%r12d, 192(%rsp)
	.p2align 4,,10
	.p2align 3
.L58:
	movq	48(%rsp), %rbx
	addq	$1, 80(%rsp)
	cmpq	$-1, %rbx
	je	.L135
	cmpq	%rbx, 24(%rsp)
	je	.L11
	movq	144(%rsp), %rsi
	movl	%eax, %r8d
	jbe	.L13
	movq	%r15, 72(%rsp)
	movl	%r14d, 120(%rsp)
	movq	24(%rsp), %r15
	movq	16(%rsp), %r14
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r12, %rsi
.L15:
	addq	$1, %rbx
	cmpq	%r15, %rbx
	je	.L266
.L12:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %r12
	movslq	(%r14,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %r8
	jns	.L138
	movq	40(%rsp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L16:
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	js	.L267
.L17:
	testq	%rax, %rax
	je	.L141
	movzbl	5(%rdx), %edi
	cmpb	%dil, 1(%rsi)
	jne	.L22
	xorl	%edx, %edx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movzbl	1(%rsi,%rdx), %edi
	cmpb	%dil, (%rcx,%rdx)
	jne	.L22
.L23:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L24
	leaq	(%r12,%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$1, %rbx
	cmpq	%r15, %rbx
	jne	.L12
	.p2align 4,,10
	.p2align 3
.L266:
	movq	72(%rsp), %r15
	movl	120(%rsp), %r14d
	andl	$16777215, %r8d
.L13:
	subq	$1, 24(%rsp)
	movq	128(%rsp), %r13
.L14:
	leal	1(%r8), %eax
	testl	%r14d, %r14d
	movslq	%r8d, %r8
	movzbl	(%r15,%r8), %r10d
	je	.L56
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L57:
	addl	%r10d, %eax
	addl	$1, %edx
	movslq	%eax, %rcx
	addl	$1, %eax
	cmpl	%r14d, %edx
	movzbl	(%r15,%rcx), %r10d
	jne	.L57
.L56:
	testl	%r10d, %r10d
	je	.L58
	movq	136(%rsp), %rsi
	movl	192(%rsp), %r12d
	movl	%eax, 168(%rsp)
.L9:
	movl	176(%rsp), %eax
	testl	%eax, %eax
	jne	.L153
.L286:
	movq	16(%rsp), %r14
	movl	172(%rsp), %r11d
	movq	%rsi, 136(%rsp)
	movq	$0, 72(%rsp)
	movq	%r13, 176(%rsp)
	movl	%r10d, 192(%rsp)
	.p2align 4,,10
	.p2align 3
.L108:
	movq	56(%rsp), %rax
	addq	$1, 72(%rsp)
	cmpq	$-1, %rax
	je	.L154
	cmpq	%rax, 32(%rsp)
	je	.L61
	movq	%rax, %rbp
	movq	152(%rsp), %rsi
	movl	%r12d, %edi
	jbe	.L63
	movl	%r11d, 128(%rsp)
	movq	%r15, 120(%rsp)
	movq	32(%rsp), %r11
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%r13, %rsi
.L66:
	addq	$1, %rbp
	cmpq	%rbp, %r11
	je	.L268
.L62:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %r13
	movslq	(%r14,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rdi
	jns	.L157
	movq	40(%rsp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L67:
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	js	.L269
.L68:
	testq	%rax, %rax
	je	.L160
	movzbl	1(%rsi), %ebx
	cmpb	%bl, 5(%rdx)
	jne	.L73
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	1(%rsi,%rdx), %ebx
	cmpb	%bl, (%rcx,%rdx)
	jne	.L73
.L74:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L75
	leaq	0(%r13,%rdx), %rsi
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$1, %rbp
	cmpq	%rbp, %r11
	jne	.L62
	.p2align 4,,10
	.p2align 3
.L268:
	movq	120(%rsp), %r15
	movl	128(%rsp), %r11d
	andl	$16777215, %edi
.L63:
	subq	$1, 32(%rsp)
	movq	136(%rsp), %rsi
.L64:
	leal	1(%rdi), %r12d
	testl	%r11d, %r11d
	movslq	%edi, %rdi
	movzbl	(%r15,%rdi), %ebp
	je	.L106
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L107:
	addl	%ebp, %r12d
	addl	$1, %eax
	movslq	%r12d, %rdx
	addl	$1, %r12d
	cmpl	%r11d, %eax
	movzbl	(%r15,%rdx), %ebp
	jne	.L107
.L106:
	testl	%ebp, %ebp
	je	.L108
	movl	192(%rsp), %r10d
	movq	176(%rsp), %r13
	testl	%r10d, %r10d
	je	.L270
.L234:
	cmpb	$0, 199(%rsp)
	movq	72(%rsp), %rax
	movq	80(%rsp), %rdi
	setne	%r9b
	cmpq	%rdi, %rax
	je	.L110
	testb	%r9b, %r9b
	je	.L110
	cmpq	%rdi, %rax
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
.L1:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L16
	movl	$4, %edi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	jns	.L17
.L267:
	testq	%rax, %rax
	je	.L139
	movzbl	5(%rdx), %r9d
	movzbl	1(%rsi), %edi
	cmpb	%dil, %r9b
	jne	.L140
	xorl	%edx, %edx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	(%rcx,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L19
.L20:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L26
	xorl	%eax, %eax
.L18:
	movq	64(%rsp), %rdi
	subq	%r8, %rax
	leaq	(%r12,%rdx), %rsi
	movl	(%rdi,%rax,4), %r8d
	jmp	.L15
.L140:
	movl	%edi, %r10d
	movl	%r9d, %r11d
	.p2align 4,,10
	.p2align 3
.L19:
	cmpb	%r10b, %r11b
	ja	.L120
	leaq	(%rcx,%rax), %rbp
	movzbl	0(%rbp), %r13d
	cmpb	%dil, %r13b
	jne	.L142
	xorl	%edx, %edx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	movzbl	0(%rbp,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L27
.L28:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L29
	movl	%r13d, %edi
.L121:
	xorl	%r10d, %r10d
	cmpb	%dil, %r9b
	jne	.L31
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r9d
	movzbl	1(%rsi,%r10), %edi
	cmpb	%dil, %r9b
	je	.L30
.L31:
	movl	%r9d, %r11d
	xorl	%eax, %eax
	movl	%edi, %r9d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L271:
	movzbl	1(%rsi,%r10), %r9d
	movzbl	(%rcx,%r10), %r11d
.L32:
	movzbl	%r9b, %edi
	movzbl	%r11b, %r9d
	salq	$8, %rax
	subl	%r9d, %edi
	addq	$1, %r10
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%r10, %rdx
	ja	.L271
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L120:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L16
.L142:
	movl	%edi, %r10d
	movl	%r13d, %r11d
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	%r10b, %r11b
	jb	.L120
	movq	%rax, %rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L67
	movl	$4, %edi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	jns	.L68
.L269:
	testq	%rax, %rax
	je	.L158
	movzbl	5(%rdx), %r9d
	movzbl	1(%rsi), %r8d
	cmpb	%r9b, %r8b
	jne	.L159
	xorl	%edx, %edx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L77:
	movzbl	(%rcx,%rdx), %ebx
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %bl
	jne	.L70
.L71:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L77
	xorl	%eax, %eax
.L69:
	subq	%rdi, %rax
	movq	64(%rsp), %rdi
	leaq	0(%r13,%rdx), %rsi
	movl	(%rdi,%rax,4), %edi
	jmp	.L66
.L159:
	movl	%r8d, %r10d
	movl	%r9d, %ebx
	.p2align 4,,10
	.p2align 3
.L70:
	cmpb	%r10b, %bl
	ja	.L128
	leaq	(%rcx,%rax), %r12
	movzbl	(%r12), %r15d
	cmpb	%r8b, %r15b
	jne	.L161
	xorl	%edx, %edx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movzbl	(%r12,%rdx), %ebx
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %bl
	jne	.L78
.L79:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L80
	movl	%r15d, %r8d
.L129:
	xorl	%r10d, %r10d
	cmpb	%r8b, %r9b
	jne	.L82
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r9d
	movzbl	1(%rsi,%r10), %r8d
	cmpb	%r8b, %r9b
	je	.L81
.L82:
	movl	%r9d, %ebx
	xorl	%eax, %eax
	movl	%r8d, %r9d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L272:
	movzbl	1(%rsi,%r10), %r9d
	movzbl	(%rcx,%r10), %ebx
.L83:
	movzbl	%r9b, %r8d
	movzbl	%bl, %r9d
	salq	$8, %rax
	subl	%r9d, %r8d
	addq	$1, %r10
	movslq	%r8d, %r8
	addq	%r8, %rax
	cmpq	%r10, %rdx
	ja	.L272
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L128:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L67
.L161:
	movl	%r8d, %r10d
	movl	%r15d, %ebx
	.p2align 4,,10
	.p2align 3
.L78:
	cmpb	%r10b, %bl
	jb	.L128
	movq	%rax, %rdx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L61:
	movq	104(%rsp), %rdi
	cmpq	%rdi, 96(%rsp)
	jnb	.L273
	movl	164(%rsp), %edi
	movq	$-1, 56(%rsp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L11:
	movq	88(%rsp), %rdi
	cmpq	%rdi, 112(%rsp)
	jnb	.L274
	movl	160(%rsp), %r8d
	movq	$-1, 48(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L160:
	xorl	%edx, %edx
	leaq	0(%r13,%rdx), %rsi
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L141:
	xorl	%edx, %edx
	leaq	(%r12,%rdx), %rsi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r15, 120(%rsp)
	movl	%r12d, %edi
	movq	104(%rsp), %r13
	movq	96(%rsp), %rcx
	movl	%r11d, %r15d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r12, %rsi
.L84:
	movl	%r8d, %eax
	shrl	$24, %r8d
	movq	(%rsp), %rbx
	imull	12(%rsp), %r8d
	andl	$16777215, %eax
	leaq	1(%r13), %rcx
	movl	%edi, %r12d
	leal	(%r8,%r15), %edx
	movslq	%edx, %rdx
	testb	$2, (%rbx,%rdx)
	je	.L169
	movl	%eax, %edi
	movq	%rcx, %r13
.L60:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L277
	movslq	(%r14,%rax,4), %rax
	leaq	1(%rsi), %r12
	testq	%rax, %rax
	movq	%rax, %r8
	jns	.L163
	movq	40(%rsp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L85:
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	js	.L278
.L86:
	testq	%rax, %rax
	je	.L166
	movzbl	1(%rsi), %ebx
	cmpb	%bl, 5(%rdx)
	jne	.L91
	xorl	%edx, %edx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	movzbl	1(%rsi,%rdx), %ebx
	cmpb	%bl, (%rcx,%rdx)
	jne	.L91
.L92:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L93
	leaq	(%r12,%rdx), %rsi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L85
	movl	$4, %ebx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	jns	.L86
.L278:
	testq	%rax, %rax
	je	.L164
	movzbl	5(%rdx), %r10d
	movzbl	1(%rsi), %r9d
	cmpb	%r9b, %r10b
	jne	.L165
	xorl	%edx, %edx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L95:
	movzbl	(%rcx,%rdx), %ebx
	movzbl	1(%rsi,%rdx), %r11d
	cmpb	%r11b, %bl
	jne	.L88
.L89:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L95
	xorl	%eax, %eax
.L87:
	movq	64(%rsp), %rbx
	subq	%r8, %rax
	leaq	(%r12,%rdx), %rsi
	movl	(%rbx,%rax,4), %r8d
	jmp	.L84
.L165:
	movl	%r9d, %r11d
	movl	%r10d, %ebx
	.p2align 4,,10
	.p2align 3
.L88:
	cmpb	%r11b, %bl
	ja	.L132
	leaq	(%rcx,%rax), %rbp
	movzbl	0(%rbp), %ebx
	cmpb	%r9b, %bl
	movb	%bl, 56(%rsp)
	jne	.L167
	xorl	%edx, %edx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	0(%rbp,%rdx), %ebx
	movzbl	1(%rsi,%rdx), %r11d
	cmpb	%r11b, %bl
	jne	.L96
.L97:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L98
	movzbl	56(%rsp), %r9d
.L133:
	xorl	%r11d, %r11d
	cmpb	%r9b, %r10b
	jne	.L100
	.p2align 4,,10
	.p2align 3
.L99:
	addq	$1, %r11
	movzbl	(%rcx,%r11), %r10d
	movzbl	1(%rsi,%r11), %r9d
	cmpb	%r9b, %r10b
	je	.L99
.L100:
	xorl	%eax, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L279:
	movzbl	1(%rsi,%r11), %r9d
	movzbl	(%rcx,%r11), %r10d
.L101:
	subl	%r10d, %r9d
	salq	$8, %rax
	addq	$1, %r11
	movslq	%r9d, %r9
	addq	%r9, %rax
	cmpq	%rdx, %r11
	jb	.L279
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L132:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L85
.L167:
	movzbl	56(%rsp), %ebx
	movl	%r9d, %r11d
	.p2align 4,,10
	.p2align 3
.L96:
	cmpb	%r11b, %bl
	jb	.L132
	movq	%rax, %rdx
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r15, 72(%rsp)
	movl	%eax, %r8d
	movq	88(%rsp), %rbp
	movzbl	198(%rsp), %r12d
	movq	112(%rsp), %rsi
	movl	%r14d, %r15d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%r14, %r13
.L33:
	movl	%ecx, %edx
	movl	%ecx, %eax
	movq	(%rsp), %rdi
	sarl	$24, %eax
	andl	$16777215, %edx
	testq	%rbp, %rbp
	cmove	%eax, %r12d
	shrl	$24, %ecx
	leaq	1(%rbp), %rsi
	imull	12(%rsp), %ecx
	movl	%r8d, %eax
	addl	%r15d, %ecx
	movslq	%ecx, %rcx
	testb	$2, (%rdi,%rcx)
	je	.L150
	movl	%edx, %r8d
	movq	%rsi, %rbp
.L10:
	movzbl	0(%r13), %edx
	testb	%dl, %dl
	je	.L280
	movq	16(%rsp), %rax
	leaq	1(%r13), %r14
	movslq	(%rax,%rdx,4), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	jns	.L144
	movq	40(%rsp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L34:
	movslq	(%rdx), %rcx
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%ecx, %ecx
	js	.L281
.L35:
	testq	%rax, %rax
	je	.L147
	movzbl	5(%rdx), %edi
	cmpb	%dil, 1(%r13)
	jne	.L40
	xorl	%edx, %edx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	movzbl	1(%r13,%rdx), %ebx
	cmpb	%bl, (%rsi,%rdx)
	jne	.L40
.L41:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L42
	leaq	(%r14,%rdx), %r13
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	(%rsi,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L34
	movl	$4, %edi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movslq	(%rdx), %rcx
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%ecx, %ecx
	jns	.L35
.L281:
	testq	%rax, %rax
	je	.L145
	movzbl	5(%rdx), %r9d
	movzbl	1(%r13), %edi
	cmpb	%dil, %r9b
	jne	.L146
	xorl	%edx, %edx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L44:
	movzbl	(%rsi,%rdx), %r11d
	movzbl	1(%r13,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L37
.L38:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L44
	xorl	%eax, %eax
.L36:
	movq	64(%rsp), %rdi
	subq	%rcx, %rax
	leaq	(%r14,%rdx), %r13
	movl	(%rdi,%rax,4), %ecx
	jmp	.L33
.L146:
	movl	%edi, %r10d
	movl	%r9d, %r11d
	.p2align 4,,10
	.p2align 3
.L37:
	cmpb	%r10b, %r11b
	ja	.L124
	leaq	(%rsi,%rax), %rbx
	movzbl	(%rbx), %edx
	cmpb	%dl, %dil
	movb	%dl, 48(%rsp)
	jne	.L148
	xorl	%edx, %edx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%r13,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L45
.L46:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L47
	movzbl	48(%rsp), %edi
.L125:
	xorl	%r10d, %r10d
	cmpb	%r9b, %dil
	jne	.L49
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$1, %r10
	movzbl	(%rsi,%r10), %r9d
	movzbl	1(%r13,%r10), %edi
	cmpb	%dil, %r9b
	je	.L48
.L49:
	xorl	%eax, %eax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L282:
	movzbl	1(%r13,%r10), %edi
	movzbl	(%rsi,%r10), %r9d
.L50:
	subl	%r9d, %edi
	salq	$8, %rax
	addq	$1, %r10
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%r10, %rdx
	ja	.L282
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L124:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rsi, %rdx
	jmp	.L34
.L148:
	movzbl	48(%rsp), %r11d
	movl	%edi, %r10d
	.p2align 4,,10
	.p2align 3
.L45:
	cmpb	%r10b, %r11b
	jb	.L124
	movq	%rax, %rdx
	jmp	.L125
.L147:
	xorl	%edx, %edx
	leaq	(%r14,%rdx), %r13
	jmp	.L33
.L166:
	xorl	%edx, %edx
	leaq	(%r12,%rdx), %rsi
	jmp	.L84
.L158:
	xorl	%edx, %edx
	jmp	.L69
.L139:
	xorl	%edx, %edx
	jmp	.L18
.L280:
	movl	%r15d, %r14d
	movq	72(%rsp), %r15
	movb	%r12b, 198(%rsp)
	movq	%rsi, 112(%rsp)
.L52:
	movq	112(%rsp), %rdi
	movq	88(%rsp), %rbx
	cmpq	%rbx, %rdi
	ja	.L54
	cmpq	%rbp, %rdi
	je	.L151
	cmpq	%rbx, %rdi
	jb	.L151
	movq	%rbp, 88(%rsp)
	movq	$-1, 48(%rsp)
	jmp	.L14
.L277:
	movl	%r15d, %r11d
	movq	120(%rsp), %r15
	movq	%rcx, 96(%rsp)
.L102:
	movq	96(%rsp), %rax
	movq	104(%rsp), %rbx
	cmpq	%rbx, %rax
	ja	.L104
	cmpq	%r13, %rax
	je	.L170
	cmpq	%rbx, %rax
	jb	.L170
	movq	%r13, 104(%rsp)
	movq	$-1, 56(%rsp)
	jmp	.L64
.L54:
	movq	112(%rsp), %rdi
	leaq	-1(%rdi), %rbx
	cmpq	%rbp, %rdi
	movq	%rbx, 24(%rsp)
	jnb	.L55
	cmpq	%rbx, 88(%rsp)
	jb	.L283
	movq	128(%rsp), %rdi
	movl	%r8d, 160(%rsp)
	movl	%eax, %r8d
	movq	88(%rsp), %rax
	movq	%r13, 128(%rsp)
	movq	%rbp, 88(%rsp)
	movq	%rdi, 144(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L14
.L104:
	movq	96(%rsp), %rax
	leaq	-1(%rax), %rbx
	cmpq	%r13, %rax
	movq	%rbx, 32(%rsp)
	jnb	.L105
	cmpq	%rbx, 104(%rsp)
	jb	.L284
	movq	136(%rsp), %rax
	movl	%edi, 164(%rsp)
	movl	%r12d, %edi
	movq	%rsi, 136(%rsp)
	movq	%rax, 152(%rsp)
	movq	104(%rsp), %rax
	movq	%r13, 104(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L64
.L150:
	movl	%r15d, %r14d
	movq	%rbp, 112(%rsp)
	movb	%r12b, 198(%rsp)
	movq	72(%rsp), %r15
	movl	%edx, %r8d
	movq	%rsi, %rbp
	jmp	.L52
.L169:
	movl	%r15d, %r11d
	movq	%r13, 96(%rsp)
	movq	120(%rsp), %r15
	movl	%eax, %edi
	movq	%rcx, %r13
	jmp	.L102
.L145:
	xorl	%edx, %edx
	jmp	.L36
.L164:
	xorl	%edx, %edx
	jmp	.L87
.L284:
	subq	$2, %rax
	movl	%edi, 164(%rsp)
	movl	%r12d, %edi
	movq	%rax, 32(%rsp)
.L105:
	movq	136(%rsp), %rax
	movq	%rsi, 136(%rsp)
	movq	%rax, 152(%rsp)
	movq	104(%rsp), %rax
	movq	%r13, 104(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L64
.L283:
	subq	$2, %rdi
	movl	%r8d, 160(%rsp)
	movl	%eax, %r8d
	movq	%rdi, 24(%rsp)
.L55:
	movq	128(%rsp), %rax
	movq	%r13, 128(%rsp)
	movq	%rax, 144(%rsp)
	movq	88(%rsp), %rax
	movq	%rbp, 88(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L14
.L110:
	movslq	168(%rsp), %rdi
	movslq	%r12d, %rax
	movq	%rdi, %r8
	subq	%rax, %rdi
	addq	%r15, %rdi
	jmp	.L113
.L111:
	subl	$1, %r10d
	addl	$1, %r8d
	leal	1(%rax), %r12d
	subl	$1, %ebp
	addq	$1, %rax
	testl	%r10d, %r10d
	jle	.L174
	testl	%ebp, %ebp
	jle	.L174
.L113:
	movzbl	(%rdi,%rax), %edx
	movzbl	(%r15,%rax), %ecx
	movl	%eax, %r12d
	cmpb	%cl, %dl
	je	.L111
	movl	%edx, %eax
	subl	%ecx, %eax
	jne	.L1
.L112:
	movl	%r10d, %eax
	movl	%ebp, 176(%rsp)
	movl	%r8d, 168(%rsp)
	testl	%eax, %eax
	je	.L285
.L134:
	movl	%eax, %r10d
	movl	176(%rsp), %eax
	movq	$0, 80(%rsp)
	testl	%eax, %eax
	je	.L286
.L153:
	testl	%r10d, %r10d
	movl	176(%rsp), %ebp
	movq	$0, 72(%rsp)
	jne	.L234
.L270:
	orl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L274:
	movq	24(%rsp), %rax
	movq	136(%rsp), %rsi
	xorl	%r10d, %r10d
	movl	192(%rsp), %r12d
	movl	$0, 168(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L9
.L273:
	movl	192(%rsp), %r10d
	movq	%r15, %r14
	testl	%r10d, %r10d
	jne	.L287
.L116:
	cmpq	$0, 184(%rsp)
	je	.L288
.L109:
	addq	$1, 184(%rsp)
	movzbl	198(%rsp), %eax
	movq	184(%rsp), %rdi
	cmpq	%rdi, 200(%rsp)
	jne	.L115
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L174:
	cmpl	%ebp, %r10d
	je	.L112
	testb	%r9b, %r9b
	je	.L112
	movl	%r10d, %eax
	subl	%ebp, %eax
	jmp	.L1
.L170:
	movl	192(%rsp), %r10d
	movq	%r15, %r14
	movq	%r13, 104(%rsp)
	testl	%r10d, %r10d
	je	.L116
.L287:
	movl	$1, %eax
	jmp	.L1
.L151:
	movq	88(%rsp), %rax
	movq	136(%rsp), %rsi
	xorl	%r10d, %r10d
	movl	192(%rsp), %r12d
	movl	%r8d, 168(%rsp)
	movq	%rbp, 88(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L9
.L263:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__GI_strcmp
.L3:
	xorl	%eax, %eax
	testb	%dl, %dl
	setne	%al
	testb	%cl, %cl
	setne	%cl
	movzbl	%cl, %ecx
	subl	%ecx, %eax
	jmp	.L1
.L264:
	leaq	__PRETTY_FUNCTION__.8042(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$288, %edx
	call	__GI___assert_fail
.L265:
	leaq	__PRETTY_FUNCTION__.8042(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$291, %edx
	call	__GI___assert_fail
.L288:
	movq	216(%rsp), %rsi
	movq	208(%rsp), %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L109
	jmp	.L1
	.size	__GI___strcoll_l, .-__GI___strcoll_l
	.globl	__strcoll_l
	.set	__strcoll_l,__GI___strcoll_l
	.weak	strcoll_l
	.set	strcoll_l,__strcoll_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8042, @object
	.size	__PRETTY_FUNCTION__.8042, 12
__PRETTY_FUNCTION__.8042:
	.string	"__strcoll_l"
