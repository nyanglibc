	.text
	.p2align 4,,15
	.globl	memfrob
	.type	memfrob, @function
memfrob:
	testq	%rsi, %rsi
	movq	%rdi, %rax
	je	.L2
	addq	%rdi, %rsi
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rdx
	xorb	$42, -1(%rdx)
	cmpq	%rsi, %rdx
	jne	.L3
.L2:
	rep ret
	.size	memfrob, .-memfrob
