 .text
.globl memcmp
.type memcmp,@function
.align 1<<4
memcmp:
 test %rdx, %rdx
 jz .Lfinz
 cmpq $1, %rdx
 jbe .Lfinr1b
 subq %rdi, %rsi
 movq %rdx, %r10
 cmpq $32, %r10
 jae .Lgt32
.Lsmall:
 testq $1, %r10
 jz .Ls2b
 movzbl (%rdi), %eax
 movzbl (%rdi, %rsi), %edx
 subq $1, %r10
 je .Lfinz1
 addq $1, %rdi
 subl %edx, %eax
 jnz .Lexit
.Ls2b:
 testq $2, %r10
 jz .Ls4b
 movzwl (%rdi), %eax
 movzwl (%rdi, %rsi), %edx
 subq $2, %r10
 je .Lfin2_7
 addq $2, %rdi
 cmpl %edx, %eax
 jnz .Lfin2_7
.Ls4b:
 testq $4, %r10
 jz .Ls8b
 movl (%rdi), %eax
 movl (%rdi, %rsi), %edx
 subq $4, %r10
 je .Lfin2_7
 addq $4, %rdi
 cmpl %edx, %eax
 jnz .Lfin2_7
.Ls8b:
 testq $8, %r10
 jz .Ls16b
 movq (%rdi), %rax
 movq (%rdi, %rsi), %rdx
 subq $8, %r10
 je .Lfin2_7
 addq $8, %rdi
 cmpq %rdx, %rax
 jnz .Lfin2_7
.Ls16b:
 movdqu (%rdi), %xmm1
 movdqu (%rdi, %rsi), %xmm0
 pcmpeqb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 xorl %eax, %eax
 subl $0xffff, %edx
 jz .Lfinz
 bsfl %edx, %ecx
 leaq (%rdi, %rcx), %rcx
 movzbl (%rcx), %eax
 movzbl (%rsi, %rcx), %edx
 jmp .Lfinz1
 .p2align 4,, 4
.Lfinr1b:
 movzbl (%rdi), %eax
 movzbl (%rsi), %edx
.Lfinz1:
 subl %edx, %eax
.Lexit:
 ret
 .p2align 4,, 4
.Lfin2_7:
 cmpq %rdx, %rax
 jz .Lfinz
 movq %rax, %r11
 subq %rdx, %r11
 bsfq %r11, %rcx
 sarq $3, %rcx
 salq $3, %rcx
 sarq %cl, %rax
 movzbl %al, %eax
 sarq %cl, %rdx
 movzbl %dl, %edx
 subl %edx, %eax
 ret
 .p2align 4,, 4
.Lfinz:
 xorl %eax, %eax
 ret
 .p2align 4,, 4
.Lgt32:
 movq %rdx, %r11
 addq %rdi, %r11
 movq %rdi, %r8
 andq $15, %r8
 jz .L16am
 movdqu (%rdi), %xmm1
 movdqu (%rdi, %rsi), %xmm0
 pcmpeqb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 subl $0xffff, %edx
 jnz .Lneq
 neg %r8
 leaq 16(%rdi, %r8), %rdi
.L16am:
 testq $15, %rsi
 jz .LATR
 testq $16, %rdi
 jz .LA32
 movdqu (%rdi, %rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
.LA32:
 movq %r11, %r10
 andq $-32, %r10
 cmpq %r10, %rdi
        jae .Lmt16
 testq $32, %rdi
 jz .LA64
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
.LA64:
 movq %r11, %r10
 andq $-64, %r10
 cmpq %r10, %rdi
        jae .Lmt32
.LA64main:
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 cmpq %rdi, %r10
 jne .LA64main
.Lmt32:
 movq %r11, %r10
 andq $-32, %r10
 cmpq %r10, %rdi
        jae .Lmt16
.LA32main:
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqu (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 cmpq %rdi, %r10
 jne .LA32main
.Lmt16:
 subq %rdi, %r11
 je .Lfinz
 movq %r11, %r10
 jmp .Lsmall
 .p2align 4,, 4
.Lneq:
 bsfl %edx, %ecx
 movzbl (%rdi, %rcx), %eax
 addq %rdi, %rsi
 movzbl (%rsi,%rcx), %edx
 jmp .Lfinz1
 .p2align 4,, 4
.LATR:
 movq %r11, %r10
 andq $-32, %r10
 cmpq %r10, %rdi
        jae .Lmt16
 testq $16, %rdi
 jz .LATR32
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 cmpq %rdi, %r10
 je .Lmt16
.LATR32:
 movq %r11, %r10
 andq $-64, %r10
 testq $32, %rdi
 jz .LATR64
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
.LATR64:
 cmpq %rdi, %r10
 je .Lmt32
.LATR64main:
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 cmpq %rdi, %r10
 jne .LATR64main
 movq %r11, %r10
 andq $-32, %r10
 cmpq %r10, %rdi
        jae .Lmt16
.LATR32res:
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 movdqa (%rdi,%rsi), %xmm0
 pcmpeqb (%rdi), %xmm0
 pmovmskb %xmm0, %edx
 subl $0xffff, %edx
 jnz .Lneq
 addq $16, %rdi
 cmpq %r10, %rdi
 jne .LATR32res
 subq %rdi, %r11
 je .Lfinz
 movq %r11, %r10
 jmp .Lsmall
 .p2align 4,, 4
.size memcmp,.-memcmp
.weak bcmp
bcmp = memcmp

