	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__argz_add_sep
	.hidden	__argz_add_sep
	.type	__argz_add_sep, @function
__argz_add_sep:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rdx, %rdi
	movq	%rsi, %r12
	movq	%rdx, %rbx
	call	__GI_strlen
	leaq	1(%rax), %r13
	xorl	%eax, %eax
	cmpq	$1, %r13
	je	.L1
	movq	(%r12), %rsi
	movq	0(%rbp), %rdi
	addq	%r13, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L8
	addq	(%r12), %rax
	movq	%rax, %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	movb	%al, (%rsi)
	movzbl	(%rbx), %edx
	addq	$1, %rsi
.L5:
	addq	$1, %rbx
	testb	%dl, %dl
	je	.L13
.L6:
	movsbl	(%rbx), %eax
	cmpl	%r14d, %eax
	movl	%eax, %edx
	jne	.L3
	cmpq	%rsi, 0(%rbp)
	jnb	.L4
	cmpb	$0, -1(%rsi)
	je	.L4
	movb	$0, (%rsi)
	movzbl	(%rbx), %edx
	addq	$1, %rsi
	addq	$1, %rbx
	testb	%dl, %dl
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	addq	%r13, (%r12)
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	subq	$1, %r13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$12, %eax
	jmp	.L1
	.size	__argz_add_sep, .-__argz_add_sep
	.weak	argz_add_sep
	.set	argz_add_sep,__argz_add_sep
