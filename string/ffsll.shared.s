	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	ffsll
	.type	ffsll, @function
ffsll:
	movl	$-1, %edx
#APP
# 32 "../sysdeps/x86_64/ffsll.c" 1
	bsfq %rdi,%rax
cmoveq %rdx,%rax

# 0 "" 2
#NO_APP
	addl	$1, %eax
	ret
	.size	ffsll, .-ffsll
	.weak	ffsl
	.set	ffsl,ffsll
