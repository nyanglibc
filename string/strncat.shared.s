	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	strncat
	.type	strncat, @function
strncat:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	call	__GI_strlen
	leaq	0(%rbp,%rax), %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__GI___strnlen
	movq	%r12, %rsi
	movb	$0, (%rbx,%rax)
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	__GI_memcpy@PLT
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	strncat, .-strncat
	.globl	__GI___strncat
	.hidden	__GI___strncat
	.set	__GI___strncat,strncat
	.globl	__strncat
	.set	__strncat,__GI___strncat
