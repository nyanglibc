	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__argz_create_sep
	.hidden	__argz_create_sep
	.type	__argz_create_sep, @function
__argz_create_sep:
	pushq	%r14
	pushq	%r13
	movl	%esi, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rcx, %r13
	movq	%rdi, %rbx
	call	__GI_strlen
	leaq	1(%rax), %rbp
	cmpq	$1, %rbp
	jne	.L13
	movq	$0, (%r12)
	xorl	%eax, %eax
	movq	$0, 0(%r13)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L9
	movq	%rax, %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	movb	%r8b, (%rcx)
	addq	$1, %rcx
.L6:
	addq	$1, %rbx
	testb	%dl, %dl
	je	.L14
.L7:
	movsbl	(%rbx), %r8d
	cmpl	%r14d, %r8d
	movl	%r8d, %edx
	jne	.L4
	cmpq	%rcx, %rax
	jnb	.L5
	cmpb	$0, -1(%rcx)
	je	.L5
	movb	$0, (%rcx)
	addq	$1, %rbx
	addq	$1, %rcx
	testb	%dl, %dl
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rbp, %rbp
	jne	.L8
	movq	%rax, %rdi
	call	free@PLT
	movq	$0, (%r12)
.L8:
	popq	%rbx
	movq	%rbp, 0(%r13)
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	subq	$1, %rbp
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$12, %eax
	jmp	.L1
	.size	__argz_create_sep, .-__argz_create_sep
	.weak	argz_create_sep
	.set	argz_create_sep,__argz_create_sep
