	.text
	.p2align 4,,15
	.globl	__strdup
	.hidden	__strdup
	.type	__strdup, @function
__strdup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	strlen
	leaq	1(%rax), %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2
	addq	$8, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	movq	%rax, %rdi
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__strdup, .-__strdup
	.weak	strdup
	.set	strdup,__strdup
	.hidden	strlen
