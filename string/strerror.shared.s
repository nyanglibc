	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	strerror
	.type	strerror, @function
strerror:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rsi
	jmp	__GI___strerror_l
	.size	strerror, .-strerror
