	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_strncpy
	.hidden	__GI_strncpy
	.type	__GI_strncpy, @function
__GI_strncpy:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	%rdx, %rsi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	__GI___strnlen
	cmpq	%rax, %rbx
	movq	%rax, %rbp
	je	.L2
	subq	%rax, %rbx
	leaq	0(%r13,%rax), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	__GI_memset@PLT
.L2:
	addq	$8, %rsp
	movq	%rbp, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI_memcpy@PLT
	.size	__GI_strncpy, .-__GI_strncpy
	.globl	strncpy
	.set	strncpy,__GI_strncpy
