	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	two_way_long_needle, @function
two_way_long_needle:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rcx, %rbx
	movl	$1, %edi
	movl	$1, %edx
	subq	$2088, %rsp
	xorl	%ecx, %ecx
	movq	$-1, %rsi
.L2:
	leaq	(%rdx,%rcx), %rax
	cmpq	%rax, %rbx
	jbe	.L71
.L7:
	leaq	0(%rbp,%rsi), %r8
	movzbl	(%r8,%rdx), %r11d
	cmpb	%r11b, 0(%rbp,%rax)
	jnb	.L3
	movq	%rax, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	leaq	(%rdx,%rcx), %rax
	subq	%rsi, %rdi
	cmpq	%rax, %rbx
	ja	.L7
.L71:
	movl	$1, %r8d
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	$-1, %r13
.L8:
	leaq	(%rdx,%rcx), %rax
	cmpq	%rax, %rbx
	jbe	.L72
.L13:
	leaq	0(%rbp,%r13), %r9
	movzbl	(%r9,%rdx), %r10d
	cmpb	%r10b, 0(%rbp,%rax)
	jbe	.L9
	movq	%rax, %rcx
	movl	$1, %edx
	movq	%rax, %r8
	leaq	(%rdx,%rcx), %rax
	subq	%r13, %r8
	cmpq	%rax, %rbx
	ja	.L13
.L72:
	addq	$1, %r13
	addq	$1, %rsi
	cmpq	%rsi, %r13
	jnb	.L14
	movq	%rdi, %r8
	movq	%rsi, %r13
.L14:
	leaq	32(%rsp), %rax
	leaq	2080(%rsp), %rdx
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L15
	testq	%rbx, %rbx
	leaq	-1(%rbx), %r12
	je	.L16
	movq	%rbp, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movq	%rax, 32(%rsp,%rcx,8)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L17
.L16:
	leaq	0(%rbp,%r8), %rsi
	movq	%r13, %rdx
	movq	%rbp, %rdi
	movq	%r8, 8(%rsp)
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L18
	leaq	-1(%r13), %r11
	movq	8(%rsp), %r8
	movq	%rbx, %r10
	xorl	%esi, %esi
	xorl	%edx, %edx
	subq	%rbx, %r14
	leaq	0(%rbp,%r11), %rax
	subq	%r8, %r10
	movq	%rax, 24(%rsp)
	movl	$1, %eax
	subq	%r13, %rax
	movq	%rax, 8(%rsp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	%r8, %rcx
	jnb	.L67
	testq	%rsi, %rsi
	cmovne	%r10, %rcx
.L67:
	addq	%rcx, %rdx
	xorl	%esi, %esi
	cmpq	%rdx, %r14
	jb	.L69
.L29:
	leaq	(%r15,%rdx), %rax
	movzbl	(%rax,%r12), %ecx
	movq	32(%rsp,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.L73
	cmpq	%rsi, %r13
	movq	%rsi, %rcx
	cmovnb	%r13, %rcx
	leaq	(%rcx,%rdx), %rdi
	leaq	0(%rbp,%rcx), %r9
	addq	%r15, %rdi
	cmpq	%rcx, %r12
	jbe	.L22
	movzbl	(%r9), %ebx
	cmpb	%bl, (%rdi)
	je	.L24
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L74:
	movzbl	(%rax,%rcx), %edi
	cmpb	%dil, 0(%rbp,%rcx)
	jne	.L23
.L24:
	addq	$1, %rcx
	cmpq	%rcx, %r12
	ja	.L74
.L22:
	leaq	(%r11,%rdx), %r9
	movq	%r11, %rdi
	addq	%r15, %r9
	cmpq	%rsi, %r13
	jbe	.L40
	movq	24(%rsp), %rbx
	movzbl	(%rbx), %ebx
	cmpb	%bl, (%r9)
	jne	.L40
	leaq	-1(%rsi), %rbx
	subq	%r13, %r9
	movq	%rax, 16(%rsp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	1(%rcx,%r9), %eax
	cmpb	%al, 0(%rbp,%rcx)
	jne	.L65
	movq	%rcx, %rdi
.L26:
	leaq	-1(%rdi), %rcx
	cmpq	%rcx, %rbx
	jne	.L75
.L65:
	movq	16(%rsp), %rax
.L25:
	addq	$1, %rsi
	cmpq	%rdi, %rsi
	ja	.L1
	addq	%r8, %rdx
	movq	%r10, %rsi
	cmpq	%rdx, %r14
	jnb	.L29
.L69:
	xorl	%eax, %eax
.L1:
	addq	$2088, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	je	.L76
	movq	%rcx, %rsi
	movl	$1, %edi
	addq	$1, %rcx
	movl	$1, %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	je	.L77
	movq	%rcx, %r13
	movl	$1, %r8d
	addq	$1, %rcx
	movl	$1, %edx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L23:
	addq	8(%rsp), %rdx
	jmp	.L67
.L77:
	cmpq	%r8, %rdx
	je	.L12
	addq	$1, %rdx
	jmp	.L8
.L76:
	cmpq	%rdi, %rdx
	je	.L6
	addq	$1, %rdx
	jmp	.L2
.L18:
	movq	%rbx, %rax
	movl	$1, %edi
	leaq	0(%rbp,%r13), %r8
	subq	%r13, %rax
	cmpq	%r13, %rax
	cmovb	%r13, %rax
	xorl	%ecx, %ecx
	subq	%r13, %rdi
	leaq	1(%rax), %rsi
	subq	%rbx, %r14
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	(%r15,%rcx), %rax
	movzbl	(%rax,%r12), %edx
	movq	32(%rsp,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L68
	leaq	(%rcx,%r13), %rdx
	addq	%r15, %rdx
	cmpq	%r13, %r12
	jbe	.L32
	movzbl	(%rdx), %ebx
	movq	%r13, %rdx
	cmpb	%bl, (%r8)
	je	.L34
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L78:
	movzbl	(%rax,%rdx), %ebx
	cmpb	%bl, 0(%rbp,%rdx)
	jne	.L33
.L34:
	addq	$1, %rdx
	cmpq	%rdx, %r12
	ja	.L78
.L32:
	leaq	-1(%r13), %rdx
	leaq	(%rcx,%rdx), %r9
	leaq	0(%rbp,%rdx), %r10
	addq	%r15, %r9
	cmpq	$-1, %rdx
	je	.L1
	movzbl	(%r10), %ebx
	cmpb	%bl, (%r9)
	jne	.L36
	subq	%r13, %r9
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L79:
	movzbl	1(%rdx,%r9), %ebx
	cmpb	%bl, 0(%rbp,%rdx)
	jne	.L36
.L37:
	subq	$1, %rdx
	cmpq	$-1, %rdx
	jne	.L79
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	addq	%rdi, %rcx
.L68:
	addq	%rdx, %rcx
.L31:
	cmpq	%r14, %rcx
	jbe	.L38
	xorl	%eax, %eax
	jmp	.L1
.L36:
	addq	%rsi, %rcx
	jmp	.L31
.L12:
	movq	%rax, %rcx
	movl	$1, %edx
	jmp	.L8
.L6:
	movq	%rax, %rcx
	movl	$1, %edx
	jmp	.L2
.L40:
	movq	%r13, %rdi
	jmp	.L25
	.size	two_way_long_needle, .-two_way_long_needle
	.p2align 4,,15
	.globl	__GI___memmem
	.hidden	__GI___memmem
	.type	__GI___memmem, @function
__GI___memmem:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$280, %rsp
	testq	%rcx, %rcx
	je	.L100
	cmpq	$1, %rcx
	movq	%rdx, %r15
	movq	%rcx, %rbp
	je	.L123
	cmpq	%rsi, %rcx
	ja	.L101
	movq	%rsi, %r14
	subq	%rcx, %r14
	addq	%rdi, %r14
	cmpq	$2, %rcx
	je	.L124
	cmpq	$256, %rcx
	ja	.L125
	leaq	-1(%rcx), %r13
	leaq	16(%rsp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	cmpq	$1, %r13
	rep stosq
	jbe	.L90
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L91:
	movzbl	-1(%r15,%rax), %ecx
	movzbl	(%r15,%rax), %edx
	salq	$3, %rcx
	subq	%rcx, %rdx
	movzbl	%dl, %edx
	movb	%al, 16(%rsp,%rdx)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L91
.L90:
	movzbl	-2(%r15,%rbp), %edx
	movzbl	-1(%r15,%rbp), %eax
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	subq	$9, %rbp
	salq	$3, %rdx
	subq	%rdx, %rax
	movzbl	%al, %eax
	movzbl	16(%rsp,%rax), %edx
	movb	%r13b, 16(%rsp,%rax)
	subq	%rdx, %rdi
	movq	%rdi, 8(%rsp)
.L95:
	cmpq	%rbx, %r14
	jnb	.L93
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L126:
	cmpq	%rbx, %r14
	jb	.L103
.L93:
	addq	%r13, %rbx
	movzbl	-1(%rbx), %edx
	movzbl	(%rbx), %eax
	salq	$3, %rdx
	subq	%rdx, %rax
	movzbl	%al, %eax
	movzbl	16(%rsp,%rax), %eax
	testq	%rax, %rax
	je	.L126
.L103:
	subq	%rax, %rbx
	cmpq	%rax, %r13
	ja	.L95
	cmpq	$14, %r13
	jbe	.L96
	movq	(%r15,%r12), %rax
	cmpq	%rax, (%rbx,%r12)
	jne	.L97
.L96:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	je	.L100
	leaq	-8(%r12), %rax
	cmpq	$8, %r12
	cmovb	%rbp, %rax
	movq	%rax, %r12
.L97:
	addq	8(%rsp), %rbx
	cmpq	%rbx, %r14
	jnb	.L93
.L101:
	xorl	%eax, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rbx, %rax
.L80:
	addq	$280, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movzbl	(%rdx), %eax
	addq	$280, %rsp
	movq	%rsi, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	movl	%eax, %esi
	jmp	__GI_memchr
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%rdx), %edx
	movzbl	1(%r15), %eax
	addq	$1, %rbx
	movzbl	1(%rdi), %ecx
	sall	$16, %edx
	orl	%eax, %edx
	movzbl	(%rdi), %eax
	sall	$16, %eax
	orl	%ecx, %eax
	cmpq	%rbx, %r14
	jnb	.L122
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$1, %rbx
	movzbl	(%rbx), %ecx
	sall	$16, %eax
	orl	%ecx, %eax
	cmpq	%rbx, %r14
	jb	.L84
.L122:
	cmpl	%eax, %edx
	jne	.L127
.L84:
	cmpl	%eax, %edx
	jne	.L101
	leaq	-1(%rbx), %rax
	jmp	.L80
.L125:
	addq	$280, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	two_way_long_needle
	.size	__GI___memmem, .-__GI___memmem
	.globl	__memmem
	.set	__memmem,__GI___memmem
	.weak	__GI_memmem
	.hidden	__GI_memmem
	.set	__GI_memmem,__memmem
	.weak	memmem
	.set	memmem,__GI_memmem
