	.text
	.p2align 4,,15
	.globl	__argz_extract
	.type	__argz_extract, @function
__argz_extract:
	testq	%rsi, %rsi
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	je	.L2
	movq	%rdi, %rbx
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rbx, %rdi
	addq	$8, %rbp
	call	strlen
	movq	%rbx, -8(%rbp)
	leaq	1(%rbx,%rax), %rbx
	notq	%rax
	addq	%rax, %r12
	jne	.L3
.L2:
	movq	$0, 0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__argz_extract, .-__argz_extract
	.weak	argz_extract
	.set	argz_extract,__argz_extract
	.hidden	strlen
