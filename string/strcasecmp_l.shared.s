 .text
.globl __strcasecmp
.type __strcasecmp,@function
.align 1<<4
__strcasecmp:
 movq __libc_tsd_LOCALE@gottpoff(%rip),%rax
 mov %fs:(%rax),%rdx
 .byte 0x0f,0x1f,0x44,0x00,0x00
.size __strcasecmp,.-__strcasecmp
.weak strcasecmp
strcasecmp = __strcasecmp
.globl __GI___strcasecmp
.set __GI___strcasecmp,__strcasecmp
.globl __strcasecmp_l
.type __strcasecmp_l,@function
.align 1<<4
__strcasecmp_l:
 mov (%rdx), %rax
 testl $1, 64 +71*8(%rax)
 jne __strcasecmp_l_nonascii
 mov %esi, %ecx
 mov %edi, %eax
 and $0x3f, %rcx
 and $0x3f, %rax
 .section .rodata.cst16,"aM",@progbits,16
 .align 16
.Lbelowupper:
 .quad 0x4040404040404040
 .quad 0x4040404040404040
.Ltopupper:
 .quad 0x5b5b5b5b5b5b5b5b
 .quad 0x5b5b5b5b5b5b5b5b
.Ltouppermask:
 .quad 0x2020202020202020
 .quad 0x2020202020202020
 .previous
 movdqa .Lbelowupper(%rip), %xmm5
 movdqa .Ltopupper(%rip), %xmm6
 movdqa .Ltouppermask(%rip), %xmm7
 cmp $0x30, %ecx
 ja .Lcrosscache
 cmp $0x30, %eax
 ja .Lcrosscache
 movlpd (%rdi), %xmm1
 movlpd (%rsi), %xmm2
 movhpd 8(%rdi), %xmm1
 movhpd 8(%rsi), %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pxor %xmm0, %xmm0
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless16bytes
 add $16, %rsi
 add $16, %rdi
 .p2align 4
.Lcrosscache:
 and $0xfffffffffffffff0, %rsi
 and $0xfffffffffffffff0, %rdi
 mov $0xffff, %edx
 xor %r8d, %r8d
 and $0xf, %ecx
 and $0xf, %eax
 cmp %eax, %ecx
 je .Lashr_0
 ja .Lbigger
 mov %edx, %r8d
 xchg %ecx, %eax
 xchg %rsi, %rdi
.Lbigger:
 lea 15(%rax), %r9
 sub %rcx, %r9
 lea .Lunaligned_table(%rip), %r10
 movslq (%r10, %r9,4), %r9
 lea (%r10, %r9), %r10
 jmp *%r10
 .p2align 4
.Lashr_0:
 movdqa (%rsi), %xmm1
 pxor %xmm0, %xmm0
 pcmpeqb %xmm1, %xmm0
 movdqa (%rdi), %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jne .Lless32bytes

 mov $16, %rcx
 mov $16, %r9
 pxor %xmm0, %xmm0
 .p2align 4
.Lloop_ashr_0:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 jmp .Lloop_ashr_0
 .p2align 4
.Lashr_1:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $15, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $1, %r9d
 lea 1(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_1:
 add $16, %r10
 jg .Lnibble_ashr_1
.Lgobble_ashr_1:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $1, %xmm3
 pslldq $15, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_1
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $1, %xmm3
 pslldq $15, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_1
 .p2align 4
.Lnibble_ashr_1:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfffe, %edx
 jnz .Lashr_1_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_1
 .p2align 4
.Lashr_1_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $1, %xmm0
 psrldq $1, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_2:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $14, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $2, %r9d
 lea 2(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_2:
 add $16, %r10
 jg .Lnibble_ashr_2
.Lgobble_ashr_2:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $2, %xmm3
 pslldq $14, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_2
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $2, %xmm3
 pslldq $14, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_2
 .p2align 4
.Lnibble_ashr_2:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfffc, %edx
 jnz .Lashr_2_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_2
 .p2align 4
.Lashr_2_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $2, %xmm0
 psrldq $2, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_3:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $13, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $3, %r9d
 lea 3(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_3:
 add $16, %r10
 jg .Lnibble_ashr_3
.Lgobble_ashr_3:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $3, %xmm3
 pslldq $13, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_3
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $3, %xmm3
 pslldq $13, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_3
 .p2align 4
.Lnibble_ashr_3:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfff8, %edx
 jnz .Lashr_3_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_3
 .p2align 4
.Lashr_3_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $3, %xmm0
 psrldq $3, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_4:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $12, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $4, %r9d
 lea 4(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_4:
 add $16, %r10
 jg .Lnibble_ashr_4
.Lgobble_ashr_4:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $4, %xmm3
 pslldq $12, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_4
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $4, %xmm3
 pslldq $12, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_4
 .p2align 4
.Lnibble_ashr_4:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfff0, %edx
 jnz .Lashr_4_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_4
 .p2align 4
.Lashr_4_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $4, %xmm0
 psrldq $4, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_5:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $11, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $5, %r9d
 lea 5(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_5:
 add $16, %r10
 jg .Lnibble_ashr_5
.Lgobble_ashr_5:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $5, %xmm3
 pslldq $11, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_5
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $5, %xmm3
 pslldq $11, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_5
 .p2align 4
.Lnibble_ashr_5:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xffe0, %edx
 jnz .Lashr_5_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_5
 .p2align 4
.Lashr_5_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $5, %xmm0
 psrldq $5, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_6:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $10, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $6, %r9d
 lea 6(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_6:
 add $16, %r10
 jg .Lnibble_ashr_6
.Lgobble_ashr_6:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $6, %xmm3
 pslldq $10, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_6
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $6, %xmm3
 pslldq $10, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_6
 .p2align 4
.Lnibble_ashr_6:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xffc0, %edx
 jnz .Lashr_6_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_6
 .p2align 4
.Lashr_6_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $6, %xmm0
 psrldq $6, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_7:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $9, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $7, %r9d
 lea 7(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_7:
 add $16, %r10
 jg .Lnibble_ashr_7
.Lgobble_ashr_7:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $7, %xmm3
 pslldq $9, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_7
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $7, %xmm3
 pslldq $9, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_7
 .p2align 4
.Lnibble_ashr_7:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xff80, %edx
 jnz .Lashr_7_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_7
 .p2align 4
.Lashr_7_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $7, %xmm0
 psrldq $7, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_8:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $8, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $8, %r9d
 lea 8(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_8:
 add $16, %r10
 jg .Lnibble_ashr_8
.Lgobble_ashr_8:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $8, %xmm3
 pslldq $8, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_8
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $8, %xmm3
 pslldq $8, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_8
 .p2align 4
.Lnibble_ashr_8:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xff00, %edx
 jnz .Lashr_8_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_8
 .p2align 4
.Lashr_8_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $8, %xmm0
 psrldq $8, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_9:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $7, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $9, %r9d
 lea 9(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_9:
 add $16, %r10
 jg .Lnibble_ashr_9
.Lgobble_ashr_9:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $9, %xmm3
 pslldq $7, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_9
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $9, %xmm3
 pslldq $7, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_9
 .p2align 4
.Lnibble_ashr_9:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfe00, %edx
 jnz .Lashr_9_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_9
 .p2align 4
.Lashr_9_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $9, %xmm0
 psrldq $9, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_10:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $6, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $10, %r9d
 lea 10(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_10:
 add $16, %r10
 jg .Lnibble_ashr_10
.Lgobble_ashr_10:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $10, %xmm3
 pslldq $6, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_10
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $10, %xmm3
 pslldq $6, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_10
 .p2align 4
.Lnibble_ashr_10:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xfc00, %edx
 jnz .Lashr_10_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_10
 .p2align 4
.Lashr_10_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $10, %xmm0
 psrldq $10, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_11:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $5, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $11, %r9d
 lea 11(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_11:
 add $16, %r10
 jg .Lnibble_ashr_11
.Lgobble_ashr_11:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $11, %xmm3
 pslldq $5, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_11
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $11, %xmm3
 pslldq $5, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_11
 .p2align 4
.Lnibble_ashr_11:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xf800, %edx
 jnz .Lashr_11_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_11
 .p2align 4
.Lashr_11_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $11, %xmm0
 psrldq $11, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_12:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $4, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $12, %r9d
 lea 12(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_12:
 add $16, %r10
 jg .Lnibble_ashr_12
.Lgobble_ashr_12:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $12, %xmm3
 pslldq $4, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_12
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $12, %xmm3
 pslldq $4, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_12
 .p2align 4
.Lnibble_ashr_12:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xf000, %edx
 jnz .Lashr_12_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_12
 .p2align 4
.Lashr_12_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $12, %xmm0
 psrldq $12, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_13:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $13, %r9d
 lea 13(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_13:
 add $16, %r10
 jg .Lnibble_ashr_13
.Lgobble_ashr_13:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $13, %xmm3
 pslldq $3, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_13
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $13, %xmm3
 pslldq $3, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_13
 .p2align 4
.Lnibble_ashr_13:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xe000, %edx
 jnz .Lashr_13_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_13
 .p2align 4
.Lashr_13_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $13, %xmm0
 psrldq $13, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_14:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $2, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $14, %r9d
 lea 14(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_14:
 add $16, %r10
 jg .Lnibble_ashr_14
.Lgobble_ashr_14:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $14, %xmm3
 pslldq $2, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_14
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $14, %xmm3
 pslldq $2, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_14
 .p2align 4
.Lnibble_ashr_14:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0xc000, %edx
 jnz .Lashr_14_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_14
 .p2align 4
.Lashr_14_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $14, %xmm0
 psrldq $14, %xmm3
 jmp .Laftertail
 .p2align 4
.Lashr_15:
 pxor %xmm0, %xmm0
 movdqa (%rdi), %xmm2
 movdqa (%rsi), %xmm1
 pcmpeqb %xmm1, %xmm0
 pslldq $1, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %r9d
 shr %cl, %edx
 shr %cl, %r9d
 sub %r9d, %edx
 jnz .Lless32bytes
 movdqa (%rdi), %xmm3

 pxor %xmm0, %xmm0
 mov $16, %rcx
 mov $15, %r9d
 lea 15(%rdi), %r10
 and $0xfff, %r10
 sub $0x1000, %r10
 .p2align 4
.Lloop_ashr_15:
 add $16, %r10
 jg .Lnibble_ashr_15
.Lgobble_ashr_15:
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $15, %xmm3
 pslldq $1, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 add $16, %r10
 jg .Lnibble_ashr_15
 movdqa (%rsi, %rcx), %xmm1
 movdqa (%rdi, %rcx), %xmm2
 movdqa %xmm2, %xmm4
 psrldq $15, %xmm3
 pslldq $1, %xmm2
 por %xmm3, %xmm2
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm2, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm2, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm2
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lexit
 add $16, %rcx
 movdqa %xmm4, %xmm3
 jmp .Lloop_ashr_15
 .p2align 4
.Lnibble_ashr_15:
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 test $0x8000, %edx
 jnz .Lashr_15_exittail
 pxor %xmm0, %xmm0
 sub $0x1000, %r10
 jmp .Lgobble_ashr_15
 .p2align 4
.Lashr_15_exittail:
 movdqa (%rsi, %rcx), %xmm1
 psrldq $15, %xmm3
 psrldq $15, %xmm0
 .p2align 4
.Laftertail:
 movdqa %xmm1, %xmm8
 movdqa %xmm6, %xmm9
 movdqa %xmm3, %xmm10
 movdqa %xmm6, %xmm11
 pcmpgtb %xmm5, %xmm8
 pcmpgtb %xmm1, %xmm9
 pcmpgtb %xmm5, %xmm10
 pcmpgtb %xmm3, %xmm11
 pand %xmm9, %xmm8
 pand %xmm11, %xmm10
 pand %xmm7, %xmm8
 pand %xmm7, %xmm10
 por %xmm8, %xmm1
 por %xmm10, %xmm3
 pcmpeqb %xmm3, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 not %edx
 .p2align 4
.Lexit:
 lea -16(%r9, %rcx), %rax
.Lless32bytes:
 lea (%rdi, %rax), %rdi
 lea (%rsi, %rcx), %rsi
 test %r8d, %r8d
 jz .Lret
 xchg %rsi, %rdi
 .p2align 4
.Lret:
.Lless16bytes:
 bsf %rdx, %rdx
 movzbl (%rsi, %rdx), %ecx
 movzbl (%rdi, %rdx), %eax
 leaq _nl_C_LC_CTYPE_tolower+128*4(%rip), %rdx
 movl (%rdx,%rcx,4), %ecx
 movl (%rdx,%rax,4), %eax
 sub %ecx, %eax
 ret
.Lstrcmp_exitz:
 xor %eax, %eax
 ret
 .p2align 4
.LByte0:
 movzx (%rsi), %ecx
 movzx (%rdi), %eax
 leaq _nl_C_LC_CTYPE_tolower+128*4(%rip), %rdx
 movl (%rdx,%rcx,4), %ecx
 movl (%rdx,%rax,4), %eax
 sub %ecx, %eax
 ret
.size __strcasecmp_l,.-__strcasecmp_l
 .section .rodata,"a",@progbits
 .p2align 3
.Lunaligned_table:
 .int .Lashr_1 - .Lunaligned_table
 .int .Lashr_2 - .Lunaligned_table
 .int .Lashr_3 - .Lunaligned_table
 .int .Lashr_4 - .Lunaligned_table
 .int .Lashr_5 - .Lunaligned_table
 .int .Lashr_6 - .Lunaligned_table
 .int .Lashr_7 - .Lunaligned_table
 .int .Lashr_8 - .Lunaligned_table
 .int .Lashr_9 - .Lunaligned_table
 .int .Lashr_10 - .Lunaligned_table
 .int .Lashr_11 - .Lunaligned_table
 .int .Lashr_12 - .Lunaligned_table
 .int .Lashr_13 - .Lunaligned_table
 .int .Lashr_14 - .Lunaligned_table
 .int .Lashr_15 - .Lunaligned_table
 .int .Lashr_0 - .Lunaligned_table
.globl __GI___strcasecmp_l
.set __GI___strcasecmp_l,__strcasecmp_l
.weak strcasecmp_l
strcasecmp_l = __strcasecmp_l
.globl __GI_strcasecmp_l
.set __GI_strcasecmp_l,strcasecmp_l
