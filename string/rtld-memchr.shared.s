 .text
.globl memchr
.type memchr,@function
.align 1<<4
memchr:
 movd %esi, %xmm1
 mov %edi, %ecx
 punpcklbw %xmm1, %xmm1
 test %rdx, %rdx
 jz .Lreturn_null
 punpcklbw %xmm1, %xmm1
 and $63, %ecx
 pshufd $0, %xmm1, %xmm1
 cmp $48, %ecx
 ja .Lcrosscache
 movdqu (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches_1
 sub $16, %rdx
 jbe .Lreturn_null
 add $16, %rdi
 and $15, %ecx
 and $-16, %rdi
 add %rcx, %rdx
 sub $64, %rdx
 jbe .Lexit_loop
 jmp .Lloop_prolog
 .p2align 4
.Lcrosscache:
 and $15, %ecx
 and $-16, %rdi
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 sar %cl, %eax
 test %eax, %eax
 je .Lunaligned_no_match
 bsf %eax, %eax
 sub %rax, %rdx
 jbe .Lreturn_null
 add %rdi, %rax
 add %rcx, %rax
 ret
 .p2align 4
.Lunaligned_no_match:
 neg %rcx
 add $16, %rcx
 sub %rcx, %rdx
 jbe .Lreturn_null
 add $16, %rdi
 sub $64, %rdx
 jbe .Lexit_loop
 .p2align 4
.Lloop_prolog:
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 48(%rdi), %xmm4
 pcmpeqb %xmm1, %xmm4
 add $64, %rdi
 pmovmskb %xmm4, %eax
 test %eax, %eax
 jnz .Lmatches0
 test $0x3f, %rdi
 jz .Lalign64_loop
 sub $64, %rdx
 jbe .Lexit_loop
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 48(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 add $64, %rdi
 test %eax, %eax
 jnz .Lmatches0
 mov %rdi, %rcx
 and $-64, %rdi
 and $63, %ecx
 add %rcx, %rdx
 .p2align 4
.Lalign64_loop:
 sub $64, %rdx
 jbe .Lexit_loop
 movdqa (%rdi), %xmm0
 movdqa 16(%rdi), %xmm2
 movdqa 32(%rdi), %xmm3
 movdqa 48(%rdi), %xmm4
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm1, %xmm2
 pcmpeqb %xmm1, %xmm3
 pcmpeqb %xmm1, %xmm4
 pmaxub %xmm0, %xmm3
 pmaxub %xmm2, %xmm4
 pmaxub %xmm3, %xmm4
 pmovmskb %xmm4, %eax
 add $64, %rdi
 test %eax, %eax
 jz .Lalign64_loop
 sub $64, %rdi
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pcmpeqb 48(%rdi), %xmm1
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 pmovmskb %xmm1, %eax
 bsf %eax, %eax
 lea 48(%rdi, %rax), %rax
 ret
 .p2align 4
.Lexit_loop:
 add $32, %edx
 jle .Lexit_loop_32
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32_1
 sub $16, %edx
 jle .Lreturn_null
 pcmpeqb 48(%rdi), %xmm1
 pmovmskb %xmm1, %eax
 test %eax, %eax
 jnz .Lmatches48_1
 xor %eax, %eax
 ret
 .p2align 4
.Lexit_loop_32:
 add $32, %edx
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches_1
 sub $16, %edx
 jbe .Lreturn_null
 pcmpeqb 16(%rdi), %xmm1
 pmovmskb %xmm1, %eax
 test %eax, %eax
 jnz .Lmatches16_1
 xor %eax, %eax
 ret
 .p2align 4
.Lmatches0:
 bsf %eax, %eax
 lea -16(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches:
 bsf %eax, %eax
 add %rdi, %rax
 ret
 .p2align 4
.Lmatches16:
 bsf %eax, %eax
 lea 16(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches32:
 bsf %eax, %eax
 lea 32(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches_1:
 bsf %eax, %eax
 sub %rax, %rdx
 jbe .Lreturn_null
 add %rdi, %rax
 ret
 .p2align 4
.Lmatches16_1:
 bsf %eax, %eax
 sub %rax, %rdx
 jbe .Lreturn_null
 lea 16(%rdi, %rax), %rax
 ret
 .p2align 4
.Lmatches32_1:
 bsf %eax, %eax
 sub %rax, %rdx
 jbe .Lreturn_null
 lea 32(%rdi, %rax), %rax
 ret
 .p2align 4
.Lmatches48_1:
 bsf %eax, %eax
 sub %rax, %rdx
 jbe .Lreturn_null
 lea 48(%rdi, %rax), %rax
 ret
 .p2align 4
.Lreturn_null:
 xor %eax, %eax
 ret
.size memchr,.-memchr
.globl __memchr
.set __memchr,memchr

