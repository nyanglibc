 .text
.globl __memrchr
.type __memrchr,@function
.align 1<<4
__memrchr:
 movd %esi, %xmm1
 sub $16, %rdx
 jbe .Llength_less16
 punpcklbw %xmm1, %xmm1
 punpcklbw %xmm1, %xmm1
 add %rdx, %rdi
 pshufd $0, %xmm1, %xmm1
 movdqu (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches0
 sub $64, %rdi
 mov %edi, %ecx
 and $15, %ecx
 jz .Lloop_prolog
 add $16, %rdi
 add $16, %rdx
 and $-16, %rdi
 sub %rcx, %rdx
 .p2align 4
.Lloop_prolog:
 sub $64, %rdx
 jbe .Lexit_loop
 movdqa 48(%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches48
 movdqa 32(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 16(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa (%rdi), %xmm4
 pcmpeqb %xmm1, %xmm4
 pmovmskb %xmm4, %eax
 test %eax, %eax
 jnz .Lmatches0
 sub $64, %rdi
 sub $64, %rdx
 jbe .Lexit_loop
 movdqa 48(%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches48
 movdqa 32(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 16(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa (%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches0
 mov %edi, %ecx
 and $63, %ecx
 jz .Lalign64_loop
 add $64, %rdi
 add $64, %rdx
 and $-64, %rdi
 sub %rcx, %rdx
 .p2align 4
.Lalign64_loop:
 sub $64, %rdi
 sub $64, %rdx
 jbe .Lexit_loop
 movdqa (%rdi), %xmm0
 movdqa 16(%rdi), %xmm2
 movdqa 32(%rdi), %xmm3
 movdqa 48(%rdi), %xmm4
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm1, %xmm2
 pcmpeqb %xmm1, %xmm3
 pcmpeqb %xmm1, %xmm4
 pmaxub %xmm3, %xmm0
 pmaxub %xmm4, %xmm2
 pmaxub %xmm0, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jz .Lalign64_loop
 pmovmskb %xmm4, %eax
 test %eax, %eax
 jnz .Lmatches48
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pcmpeqb (%rdi), %xmm1
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 pmovmskb %xmm1, %eax
 bsr %eax, %eax
 add %rdi, %rax
 ret
 .p2align 4
.Lexit_loop:
 add $64, %edx
 cmp $32, %edx
 jbe .Lexit_loop_32
 movdqa 48(%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches48
 movdqa 32(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 16(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches16_1
 cmp $48, %edx
 jbe .Lreturn_null
 pcmpeqb (%rdi), %xmm1
 pmovmskb %xmm1, %eax
 test %eax, %eax
 jnz .Lmatches0_1
 xor %eax, %eax
 ret
 .p2align 4
.Lexit_loop_32:
 movdqa 48(%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches48_1
 cmp $16, %edx
 jbe .Lreturn_null
 pcmpeqb 32(%rdi), %xmm1
 pmovmskb %xmm1, %eax
 test %eax, %eax
 jnz .Lmatches32_1
 xor %eax, %eax
 ret
 .p2align 4
.Lmatches0:
 bsr %eax, %eax
 add %rdi, %rax
 ret
 .p2align 4
.Lmatches16:
 bsr %eax, %eax
 lea 16(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches32:
 bsr %eax, %eax
 lea 32(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches48:
 bsr %eax, %eax
 lea 48(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches0_1:
 bsr %eax, %eax
 sub $64, %rdx
 add %rax, %rdx
 jl .Lreturn_null
 add %rdi, %rax
 ret
 .p2align 4
.Lmatches16_1:
 bsr %eax, %eax
 sub $48, %rdx
 add %rax, %rdx
 jl .Lreturn_null
 lea 16(%rdi, %rax), %rax
 ret
 .p2align 4
.Lmatches32_1:
 bsr %eax, %eax
 sub $32, %rdx
 add %rax, %rdx
 jl .Lreturn_null
 lea 32(%rdi, %rax), %rax
 ret
 .p2align 4
.Lmatches48_1:
 bsr %eax, %eax
 sub $16, %rdx
 add %rax, %rdx
 jl .Lreturn_null
 lea 48(%rdi, %rax), %rax
 ret
 .p2align 4
.Lreturn_null:
 xor %eax, %eax
 ret
 .p2align 4
.Llength_less16_offset0:
 test %edx, %edx
 jz .Lreturn_null
 mov %dl, %cl
 pcmpeqb (%rdi), %xmm1
 mov $1, %edx
 sal %cl, %edx
 sub $1, %edx
 pmovmskb %xmm1, %eax
 and %edx, %eax
 test %eax, %eax
 jz .Lreturn_null
 bsr %eax, %eax
 add %rdi, %rax
 ret
 .p2align 4
.Llength_less16:
 punpcklbw %xmm1, %xmm1
 punpcklbw %xmm1, %xmm1
 add $16, %edx
 pshufd $0, %xmm1, %xmm1
 mov %edi, %ecx
 and $15, %ecx
 jz .Llength_less16_offset0
 mov %cl, %dh
 mov %ecx, %esi
 add %dl, %dh
 and $-16, %rdi
 sub $16, %dh
 ja .Llength_less16_part2
 pcmpeqb (%rdi), %xmm1
 pmovmskb %xmm1, %eax
 sar %cl, %eax
 mov %dl, %cl
 mov $1, %edx
 sal %cl, %edx
 sub $1, %edx
 and %edx, %eax
 test %eax, %eax
 jz .Lreturn_null
 bsr %eax, %eax
 add %rdi, %rax
 add %rsi, %rax
 ret
 .p2align 4
.Llength_less16_part2:
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 mov %dh, %cl
 mov $1, %edx
 sal %cl, %edx
 sub $1, %edx
 and %edx, %eax
 test %eax, %eax
 jnz .Llength_less16_part2_return
 pcmpeqb (%rdi), %xmm1
 pmovmskb %xmm1, %eax
 mov %esi, %ecx
 sar %cl, %eax
 test %eax, %eax
 jz .Lreturn_null
 bsr %eax, %eax
 add %rdi, %rax
 add %rsi, %rax
 ret
 .p2align 4
.Llength_less16_part2_return:
 bsr %eax, %eax
 lea 16(%rax, %rdi), %rax
 ret
.size __memrchr,.-__memrchr
.weak memrchr
memrchr = __memrchr
