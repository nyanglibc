	.text
	.p2align 4,,15
	.globl	strncpy
	.hidden	strncpy
	.type	strncpy, @function
strncpy:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	%rdx, %rsi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	__strnlen
	cmpq	%rax, %rbx
	movq	%rax, %rbp
	je	.L2
	subq	%rax, %rbx
	leaq	0(%r13,%rax), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
.L2:
	addq	$8, %rsp
	movq	%rbp, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	memcpy@PLT
	.size	strncpy, .-strncpy
	.hidden	__strnlen
