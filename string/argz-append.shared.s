	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__argz_append
	.hidden	__argz_append
	.type	__argz_append, @function
__argz_append:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdx, %r14
	subq	$8, %rsp
	movq	(%rsi), %r12
	movq	(%rdi), %rdi
	addq	%rcx, %r12
	movq	%r12, %rsi
	call	realloc@PLT
	movq	%rax, %rbp
	movl	$12, %eax
	testq	%rbp, %rbp
	je	.L1
	movq	(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	addq	%rbp, %rdi
	call	__GI_memcpy@PLT
	movq	%rbp, 0(%r13)
	xorl	%eax, %eax
	movq	%r12, (%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__argz_append, .-__argz_append
	.weak	argz_append
	.set	argz_append,__argz_append
	.p2align 4,,15
	.globl	__argz_add
	.hidden	__argz_add
	.type	__argz_add, @function
__argz_add:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	__GI_strlen
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	1(%rax), %rcx
	jmp	__argz_append
	.size	__argz_add, .-__argz_add
	.weak	argz_add
	.set	argz_add,__argz_add
