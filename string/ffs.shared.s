	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___ffs
	.hidden	__GI___ffs
	.type	__GI___ffs, @function
__GI___ffs:
	movl	$-1, %edx
#APP
# 31 "../sysdeps/x86_64/ffs.c" 1
	bsfl %edi,%eax
cmovel %edx,%eax

# 0 "" 2
#NO_APP
	addl	$1, %eax
	ret
	.size	__GI___ffs, .-__GI___ffs
	.weak	__GI_ffs
	.hidden	__GI_ffs
	.set	__GI_ffs,__GI___ffs
	.globl	ffs
	.set	ffs,__GI_ffs
	.globl	__ffs
	.set	__ffs,__GI___ffs
