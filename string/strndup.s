	.text
	.p2align 4,,15
	.globl	__strndup
	.hidden	__strndup
	.type	__strndup, @function
__strndup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__strnlen
	leaq	1(%rax), %rdi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1
	movb	$0, (%rax,%rbx)
	addq	$8, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__strndup, .-__strndup
	.weak	strndup
	.set	strndup,__strndup
	.hidden	__strnlen
