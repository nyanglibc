	.text
	.p2align 4,,15
	.globl	__ffs
	.hidden	__ffs
	.type	__ffs, @function
__ffs:
	movl	$-1, %edx
#APP
# 31 "../sysdeps/x86_64/ffs.c" 1
	bsfl %edi,%eax
cmovel %edx,%eax

# 0 "" 2
#NO_APP
	addl	$1, %eax
	ret
	.size	__ffs, .-__ffs
	.weak	ffs
	.hidden	ffs
	.set	ffs,__ffs
