	.text
	.p2align 4,,15
	.type	utf8_encode.part.0, @function
utf8_encode.part.0:
	testl	$-2048, %esi
	je	.L4
	testl	$-65536, %esi
	je	.L5
	testl	$-2097152, %esi
	je	.L6
	movl	%esi, %eax
	andl	$-67108864, %eax
	cmpl	$1, %eax
	sbbl	%edx, %edx
	addl	$5, %edx
	cmpl	$1, %eax
	sbbl	%ecx, %ecx
	andl	$-4, %ecx
	subl	$4, %ecx
	cmpl	$1, %eax
	sbbl	%eax, %eax
	addl	$6, %eax
.L2:
	movb	%cl, (%rdi)
	movslq	%edx, %rdx
.L3:
	movl	%esi, %ecx
	sarl	$6, %esi
	andl	$63, %ecx
	orl	$-128, %ecx
	movb	%cl, (%rdi,%rdx)
	subq	$1, %rdx
	testl	%edx, %edx
	jne	.L3
	orb	%sil, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %edx
	movl	$-64, %ecx
	movl	$2, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$2, %edx
	movl	$-32, %ecx
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$3, %edx
	movl	$-16, %ecx
	movl	$4, %eax
	jmp	.L2
	.size	utf8_encode.part.0, .-utf8_encode.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"strxfrm_l.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((uintptr_t) l_data.table) % __alignof__ (l_data.table[0]) == 0"
	.align 8
.LC2:
	.string	"((uintptr_t) l_data.indirect) % __alignof__ (l_data.indirect[0]) == 0"
	.text
	.p2align 4,,15
	.globl	__strxfrm_l
	.hidden	__strxfrm_l
	.type	__strxfrm_l, @function
__strxfrm_l:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	movq	24(%rcx), %rax
	movq	%rdi, -128(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, -96(%rbp)
	movl	64(%rax), %ebx
	testq	%rbx, %rbx
	movq	%rbx, -104(%rbp)
	jne	.L11
	movq	%rdx, %r15
	movq	%rsi, %rdi
	call	strlen
	testq	%r15, %r15
	movq	%rax, -88(%rbp)
	jne	.L662
.L10:
	movq	-88(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L11:
	movq	-208(%rbp), %rbx
	cmpb	$0, (%rbx)
	je	.L663
	movq	72(%rax), %rbx
	movq	96(%rax), %rdi
	movq	88(%rax), %r15
	movq	%rbx, -200(%rbp)
	movq	80(%rax), %rbx
	movq	104(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%rbx, -72(%rbp)
	andl	$3, %ebx
	movq	%rax, -144(%rbp)
	jne	.L664
	testb	$3, -144(%rbp)
	jne	.L665
	subq	$16400, %rsp
	movq	%r15, -88(%rbp)
	movq	-208(%rbp), %rsi
	leaq	15(%rsp), %rax
	subq	$4112, %rsp
	xorl	%r14d, %r14d
	andq	$-16, %rax
	movq	%rax, -80(%rbp)
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -112(%rbp)
	movq	%rax, %r15
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r12, %rsi
.L16:
	movl	%r8d, %eax
	andl	$16777215, %r8d
	leaq	1(%r14), %r13
	sarl	$24, %eax
	cmpb	$0, (%rsi)
	movb	%al, (%r15,%r14)
	movq	-80(%rbp), %rax
	movl	%r8d, (%rax,%r14,4)
	je	.L34
	cmpq	$4095, %r13
	je	.L666
	movq	%r13, %r14
.L35:
	movzbl	(%rsi), %eax
	movq	-72(%rbp), %rbx
	leaq	1(%rsi), %r12
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %r8
	jns	.L364
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L17:
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	js	.L667
.L18:
	testq	%rax, %rax
	je	.L367
	movzbl	5(%rdx), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L23
	xorl	%edx, %edx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movzbl	1(%rsi,%rdx), %ebx
	cmpb	%bl, (%rcx,%rdx)
	jne	.L23
.L24:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L25
	jmp	.L22
.L663:
	cmpq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	je	.L10
	movq	-128(%rbp), %rax
	movb	$0, (%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L17
	movl	$4, %ebx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %r8
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%r8d, %r8d
	jns	.L18
.L667:
	testq	%rax, %rax
	je	.L365
	movzbl	5(%rdx), %r9d
	movzbl	1(%rsi), %edi
	cmpb	%r9b, %dil
	jne	.L366
	xorl	%edx, %edx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%rcx,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L20
.L21:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L27
	xorl	%eax, %eax
.L19:
	movq	-144(%rbp), %rbx
	subq	%r8, %rax
	leaq	(%r12,%rdx), %rsi
	movl	(%rbx,%rax,4), %r8d
	jmp	.L16
.L366:
	movl	%edi, %r10d
	movl	%r9d, %r11d
.L20:
	cmpb	%r10b, %r11b
	ja	.L325
	leaq	(%rcx,%rax), %rbx
	movzbl	(%rbx), %r13d
	cmpb	%r13b, %dil
	jne	.L368
	xorl	%edx, %edx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L28
.L29:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L30
	movzbl	%r13b, %edi
.L326:
	xorl	%r10d, %r10d
	cmpb	%dil, %r9b
	jne	.L32
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r9d
	movzbl	1(%rsi,%r10), %edi
	cmpb	%dil, %r9b
	je	.L31
.L32:
	xorl	%eax, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L668:
	movzbl	1(%rsi,%r10), %edi
	movzbl	(%rcx,%r10), %r9d
.L33:
	subl	%r9d, %edi
	salq	$8, %rax
	addq	$1, %r10
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%rdx, %r10
	jb	.L668
	jmp	.L19
.L325:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L17
.L368:
	movl	%edi, %r10d
	movl	%r13d, %r11d
.L28:
	cmpb	%r10b, %r11b
	jb	.L325
	movq	%rax, %rdx
	jmp	.L326
.L367:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	(%r12,%rdx), %rsi
	jmp	.L16
.L34:
	movq	-112(%rbp), %rax
	movq	-88(%rbp), %r15
	movq	$0, -136(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-80(%rbp), %rax
	leaq	-4(%rax,%r14,4), %rbx
	leaq	-4(%rax,%r13,4), %rax
	movq	%rax, -176(%rbp)
	leaq	-55(%rbp), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -144(%rbp)
.L86:
	movq	-112(%rbp), %rax
	movq	-200(%rbp), %rbx
	addq	-120(%rbp), %rbx
	movzbl	(%rax), %eax
	imulq	-104(%rbp), %rax
	movq	%rbx, -72(%rbp)
	movzbl	(%rbx,%rax), %r9d
	movl	%r9d, %eax
	testb	$4, %al
	jne	.L371
	movq	-80(%rbp), %rax
	movq	%r14, -160(%rbp)
	xorl	%esi, %esi
	movq	-136(%rbp), %r10
	movq	-96(%rbp), %r14
	movq	$-1, %rdx
	leaq	-4(%rax), %r12
	movq	%r12, -152(%rbp)
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L47:
	andl	$1, %r9d
	je	.L38
	cmpq	$-1, %rdx
	je	.L39
	cmpq	%rsi, %rdx
	jnb	.L39
	movq	-152(%rbp), %rax
	leaq	(%rax,%rsi,4), %rcx
	leaq	(%rax,%rdx,4), %r11
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	addl	%edi, %edx
	movq	%r9, %r10
	movl	%edx, (%rcx)
.L41:
	subq	$4, %rcx
	cmpq	%rcx, %r11
	je	.L39
.L43:
	movslq	(%rcx), %rax
	movzbl	(%r15,%rax), %edi
	leal	1(%rax), %edx
	movq	%rax, %r8
	movl	%edx, (%rcx)
	leaq	(%rdi,%r10), %r9
	cmpq	%r9, %r14
	jbe	.L40
	testq	%rdi, %rdi
	leaq	-1(%rdi), %rbx
	je	.L41
	movq	-128(%rbp), %rax
	movslq	%edx, %rdx
	movq	%rcx, -88(%rbp)
	addq	%r15, %rdx
	addq	%rax, %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L42:
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, (%r10,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdi
	jne	.L42
	movq	-88(%rbp), %rcx
	leal	2(%r8,%rbx), %eax
	movq	%r9, %r10
	movl	%eax, (%rcx)
	subq	$4, %rcx
	cmpq	%rcx, %r11
	jne	.L43
.L39:
	movslq	(%r12,%rsi,4), %rdx
	movzbl	(%r15,%rdx), %ecx
	leal	1(%rdx), %eax
	movq	%rdx, %rdi
	movl	%eax, (%r12,%rsi,4)
	leaq	(%rcx,%r10), %r8
	cmpq	%r8, %r14
	ja	.L669
	addl	%ecx, %eax
	movq	%r8, %r10
	movq	$-1, %rdx
	movl	%eax, (%r12,%rsi,4)
.L45:
	movq	-112(%rbp), %rax
	addq	$1, %rsi
	movq	-72(%rbp), %rbx
	movzbl	(%rax,%rsi), %eax
	imulq	-104(%rbp), %rax
	cmpq	%rsi, %r13
	movzbl	(%rbx,%rax), %r9d
	jne	.L47
	cmpq	$-1, %rdx
	movq	-160(%rbp), %r14
	movq	%r10, -88(%rbp)
	je	.L48
	cmpq	%rdx, %r13
	jbe	.L48
	movq	-80(%rbp), %rax
	movq	-176(%rbp), %rcx
	movq	-96(%rbp), %r8
	leaq	-4(%rax,%rdx,4), %r11
	jmp	.L52
.L49:
	addl	%esi, %edx
	movq	%rdi, %r10
	movl	%edx, (%rcx)
.L50:
	subq	$4, %rcx
	cmpq	%rcx, %r11
	je	.L670
.L52:
	movslq	(%rcx), %rax
	movzbl	(%r15,%rax), %esi
	leal	1(%rax), %edx
	movq	%rax, %r9
	movl	%edx, (%rcx)
	leaq	(%rsi,%r10), %rdi
	cmpq	%rdi, %r8
	jbe	.L49
	testq	%rsi, %rsi
	leaq	-1(%rsi), %rbx
	je	.L50
	movq	-128(%rbp), %r12
	movslq	%edx, %rdx
	xorl	%eax, %eax
	addq	%r12, %r10
	leaq	(%r15,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L51:
	movzbl	(%r12,%rax), %edx
	movb	%dl, (%r10,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L51
	leal	2(%r9,%rbx), %eax
	subq	$4, %rcx
	movq	%rdi, %r10
	movl	%eax, 4(%rcx)
	cmpq	%rcx, %r11
	jne	.L52
.L670:
	movq	%r10, -88(%rbp)
.L48:
	addq	$1, -120(%rbp)
	movq	-88(%rbp), %rbx
	cmpq	%rbx, -96(%rbp)
	movq	-120(%rbp), %rax
	jbe	.L85
	cmpq	%rax, -104(%rbp)
	movq	-128(%rbp), %rax
	seta	(%rax,%rbx)
.L85:
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rdi
	addq	$1, %rax
	cmpq	%rdi, -104(%rbp)
	je	.L671
	movq	%rax, -136(%rbp)
	jmp	.L86
.L38:
	cmpq	$-1, %rdx
	cmove	%rsi, %rdx
	jmp	.L45
.L371:
	movq	-136(%rbp), %rdi
	movq	%r13, -88(%rbp)
	movl	$1, %esi
	movq	-144(%rbp), %r13
	movq	%r14, -160(%rbp)
	xorl	%r12d, %r12d
	movq	$-1, %rax
	movl	%r9d, %ebx
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L37:
	andl	$1, %ebx
	je	.L53
	cmpq	$-1, %rax
	je	.L54
	cmpq	%r12, %rax
	jnb	.L54
	movq	-80(%rbp), %rdi
	movq	%r12, -152(%rbp)
	leaq	-4(%rdi), %rdx
	leaq	(%rdx,%rax,4), %rax
	leaq	(%rdx,%r12,4), %r8
	movq	%rax, %r12
	jmp	.L64
.L675:
	cmpl	$127, %esi
	jg	.L672
	leaq	1(%r14), %rdi
	movb	%sil, -55(%rbp)
	movl	$1, %edx
	leaq	(%r10,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	ja	.L673
.L58:
	addl	%r9d, %r11d
	addq	%r10, %r14
	subq	$4, %r8
	movl	%r11d, 4(%r8)
	addq	%rdx, %r14
	cmpq	%r8, %r12
	movl	%ebx, %esi
	je	.L674
.L64:
	movslq	(%r8), %rax
	movzbl	(%r15,%rax), %r10d
	leal	1(%rax), %r11d
	movl	%r11d, (%r8)
	testq	%r10, %r10
	movq	%r10, %r9
	jne	.L675
	subq	$4, %r8
	addl	$1, %esi
	cmpq	%r8, %r12
	jne	.L64
.L674:
	movq	-152(%rbp), %r12
.L54:
	movq	-80(%rbp), %rdi
	movslq	(%rdi,%r12,4), %rax
	movzbl	(%r15,%rax), %r8d
	leal	1(%rax), %r10d
	movl	%r10d, (%rdi,%r12,4)
	testq	%r8, %r8
	movq	%r8, %r9
	je	.L65
	cmpl	$127, %esi
	jg	.L676
	leaq	1(%r14), %rdi
	movb	%sil, -55(%rbp)
	movl	$1, %edx
	leaq	(%r8,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	ja	.L677
.L68:
	movq	-80(%rbp), %rax
	addq	%r8, %r14
	addl	%r9d, %r10d
	addq	%rdx, %r14
	movl	%ebx, %esi
	movl	%r10d, (%rax,%r12,4)
	movq	$-1, %rax
.L73:
	movq	-112(%rbp), %rbx
	addq	$1, %r12
	movzbl	(%rbx,%r12), %edx
	movq	-72(%rbp), %rbx
	imulq	-104(%rbp), %rdx
	cmpq	%r12, -88(%rbp)
	movzbl	(%rbx,%rdx), %ebx
	jne	.L37
	cmpq	$-1, %rax
	movq	-88(%rbp), %r13
	movq	%r14, %r12
	movq	%r14, -88(%rbp)
	movq	-160(%rbp), %r14
	je	.L48
	cmpq	%rax, %r14
	jbe	.L48
	movq	-80(%rbp), %rbx
	movq	%r13, -72(%rbp)
	movq	-168(%rbp), %r11
	movq	-144(%rbp), %r13
	leaq	-4(%rbx,%rax,4), %rbx
	jmp	.L84
.L681:
	cmpl	$127, %esi
	jg	.L678
	leaq	1(%r12), %rdi
	movb	%sil, -55(%rbp)
	movl	$1, %edx
	leaq	(%r9,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	ja	.L679
.L78:
	addl	%r8d, %r10d
	addq	%r9, %r12
	subq	$4, %r11
	movl	%r10d, 4(%r11)
	addq	%rdx, %r12
	cmpq	%r11, %rbx
	movl	$1, %esi
	je	.L680
.L84:
	movslq	(%r11), %rax
	movzbl	(%r15,%rax), %r9d
	leal	1(%rax), %r10d
	movl	%r10d, (%r11)
	testq	%r9, %r9
	movq	%r9, %r8
	jne	.L681
	subq	$4, %r11
	addl	$1, %esi
	cmpq	%r11, %rbx
	jne	.L84
.L680:
	movq	-72(%rbp), %r13
	movq	%r12, -88(%rbp)
	jmp	.L48
.L53:
	cmpq	$-1, %rax
	cmove	%r12, %rax
	jmp	.L73
.L65:
	addl	$1, %esi
	movq	$-1, %rax
	jmp	.L73
.L672:
	movq	%r13, %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rdx
	leaq	(%rdx,%r14), %rdi
	leaq	(%r10,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L58
	testq	%rdx, %rdx
	je	.L60
	movq	-128(%rbp), %rsi
	movzbl	-55(%rbp), %ecx
	xorl	%eax, %eax
	addq	%r14, %rsi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L682:
	movzbl	0(%r13,%rax), %ecx
.L61:
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L682
	jmp	.L60
.L669:
	testq	%rcx, %rcx
	leaq	-1(%rcx), %r11
	movq	$-1, %rdx
	je	.L45
	movq	-128(%rbp), %rbx
	cltq
	xorl	%edx, %edx
	addq	%r15, %rax
	leaq	(%rbx,%r10), %r9
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	(%rax,%rdx), %r10d
	movb	%r10b, (%r9,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L46
	leal	2(%rdi,%r11), %eax
	movq	%r8, %r10
	movq	$-1, %rdx
	movl	%eax, (%r12,%rsi,4)
	jmp	.L45
.L673:
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movb	%sil, (%rax,%r14)
.L60:
	movq	-128(%rbp), %rsi
	movslq	%r11d, %rcx
	xorl	%eax, %eax
	addq	%r15, %rcx
	addq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L62:
	movzbl	(%rcx,%rax), %edi
	movb	%dil, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L62
	jmp	.L58
.L662:
	addq	$1, %rax
	movq	%r15, %rbx
	movq	-208(%rbp), %rsi
	cmpq	%r15, %rax
	movq	-128(%rbp), %rdi
	cmovbe	%rax, %rbx
	movq	%rbx, %rdx
	call	__stpncpy
	jmp	.L10
.L365:
	xorl	%edx, %edx
	jmp	.L19
.L676:
	movq	%r13, %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rdx
	leaq	(%rdx,%r14), %rdi
	leaq	(%r8,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L68
	testq	%rdx, %rdx
	je	.L70
	movq	-128(%rbp), %rsi
	movzbl	-55(%rbp), %ecx
	xorl	%eax, %eax
	addq	%r14, %rsi
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L683:
	movzbl	0(%r13,%rax), %ecx
.L71:
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L683
.L70:
	movq	-128(%rbp), %rsi
	movslq	%r10d, %rcx
	xorl	%eax, %eax
	addq	%r15, %rcx
	addq	%rdi, %rsi
.L72:
	movzbl	(%rcx,%rax), %edi
	movb	%dil, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r8
	jne	.L72
	jmp	.L68
.L678:
	movq	%r13, %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rdx
	leaq	(%rdx,%r12), %rdi
	leaq	(%r9,%rdi), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L78
	testq	%rdx, %rdx
	je	.L80
	movq	-128(%rbp), %rsi
	movzbl	-55(%rbp), %ecx
	xorl	%eax, %eax
	addq	%r12, %rsi
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L684:
	movzbl	0(%r13,%rax), %ecx
.L81:
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L684
.L80:
	movq	-128(%rbp), %rsi
	movslq	%r10d, %rcx
	xorl	%eax, %eax
	addq	%r15, %rcx
	addq	%rdi, %rsi
.L82:
	movzbl	(%rcx,%rax), %edi
	movb	%dil, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L82
	jmp	.L78
.L677:
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movb	%sil, (%rax,%r14)
	jmp	.L70
.L679:
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movb	%sil, (%rax,%r12)
	jmp	.L80
.L671:
	cmpq	$2, %rax
	jbe	.L10
	movq	-136(%rbp), %rdx
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L10
.L661:
	movq	-88(%rbp), %rbx
	cmpq	%rbx, -96(%rbp)
	leaq	-1(%rbx), %rax
	jb	.L659
	movq	-128(%rbp), %rdi
	movb	$0, -1(%rdi,%rbx)
.L659:
	movq	%rax, -88(%rbp)
	jmp	.L10
.L666:
	movq	-112(%rbp), %rax
	movq	-88(%rbp), %r15
	movq	$0, -216(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, 4095(%rax)
	leaq	-55(%rbp), %rax
	movq	%rax, -224(%rbp)
.L321:
	movq	-208(%rbp), %rax
	movq	-72(%rbp), %rbx
	movl	-152(%rbp), %r14d
	movzbl	(%rax), %edx
	movq	%rdx, %rax
	movslq	(%rbx,%rdx,4), %rdx
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	js	.L685
.L88:
	sarl	$24, %ebx
	movslq	%r14d, %rcx
	movzbl	%bl, %edx
	imulq	-104(%rbp), %rdx
	addq	-200(%rbp), %rdx
	testb	$4, (%rdx,%rcx)
	je	.L381
	movq	-216(%rbp), %rbx
	xorl	%r12d, %r12d
	testb	%al, %al
	movq	-208(%rbp), %rsi
	movl	$1, -112(%rbp)
	movq	%r12, %r13
	movq	$0, -80(%rbp)
	movq	%rbx, -88(%rbp)
	movl	%r14d, %ebx
	movq	%r15, %r14
	movl	%ebx, %r15d
	je	.L686
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	1(%rsi), %rbx
	movq	%rbx, -176(%rbp)
	movq	-72(%rbp), %rbx
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rdi
	js	.L687
.L182:
	movl	%edi, %eax
	andl	$16777215, %edi
	leal	1(%rdi), %ebx
	movslq	%edi, %rdi
	shrl	$24, %eax
	movzbl	(%r14,%rdi), %ecx
	testl	%r15d, %r15d
	movl	%ebx, -188(%rbp)
	movq	%rcx, -160(%rbp)
	jle	.L200
	xorl	%edx, %edx
	movl	%ebx, %edi
	.p2align 4,,10
	.p2align 3
.L201:
	addl	%edi, %ecx
	addl	$1, %edx
	leal	1(%rcx), %edi
	cmpl	%edx, %r15d
	movslq	%ecx, %rcx
	movzbl	(%r14,%rcx), %ecx
	jne	.L201
	movl	%edi, -188(%rbp)
	movq	%rcx, -160(%rbp)
.L200:
	movzbl	%al, %eax
	movq	-200(%rbp), %rdx
	addq	-152(%rbp), %rdx
	imulq	-104(%rbp), %rax
	testb	$1, (%rdx,%rax)
	je	.L202
	testq	%r13, %r13
	je	.L203
	cmpq	$0, -80(%rbp)
	je	.L203
	leaq	1(%r13), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L255:
	movzbl	0(%r13), %eax
	movq	-72(%rbp), %rbx
	movq	-184(%rbp), %rcx
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L688
.L204:
	andl	$16777215, %esi
	testl	%r15d, %r15d
	leal	1(%rsi), %r9d
	movslq	%esi, %rsi
	movzbl	(%r14,%rsi), %r8d
	jle	.L222
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L223:
	addl	%r9d, %r8d
	addl	$1, %eax
	leal	1(%r8), %r9d
	cmpl	%eax, %r15d
	movslq	%r8d, %r8
	movzbl	(%r14,%r8), %r8d
	jne	.L223
.L222:
	cmpq	$1, -80(%rbp)
	jbe	.L224
	movl	$1, %r12d
	movq	%r13, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L245:
	movzbl	(%rcx), %eax
	movq	-72(%rbp), %rbx
	leaq	1(%rcx), %r13
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rdi
	js	.L689
	movq	%r13, %rcx
.L225:
	andl	$16777215, %edi
	testl	%r15d, %r15d
	leal	1(%rdi), %r9d
	movslq	%edi, %rdi
	movzbl	(%r14,%rdi), %eax
	jle	.L243
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L244:
	addl	%r9d, %eax
	addl	$1, %edx
	leal	1(%rax), %r9d
	cmpl	%edx, %r15d
	cltq
	movzbl	(%r14,%rax), %eax
	jne	.L244
.L243:
	addq	$1, %r12
	cmpq	-80(%rbp), %r12
	jne	.L245
	movq	-168(%rbp), %r13
	movq	%rax, %r8
.L224:
	testq	%r8, %r8
	je	.L246
	cmpl	$127, -112(%rbp)
	jg	.L690
	movzbl	-112(%rbp), %eax
	movl	$1, %edx
	movb	%al, -55(%rbp)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	leaq	(%rsi,%r8), %rax
	cmpq	%rax, -96(%rbp)
	ja	.L691
.L249:
	addq	-88(%rbp), %r8
	movl	$1, -112(%rbp)
	leaq	(%rdx,%r8), %rax
	movq	%rax, -88(%rbp)
.L254:
	subq	$1, -80(%rbp)
	jne	.L255
.L203:
	cmpq	$0, -160(%rbp)
	je	.L256
	cmpl	$127, -112(%rbp)
	jg	.L692
	movzbl	-112(%rbp), %eax
	movl	$1, %esi
	movb	%al, -55(%rbp)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rdi
	movq	-160(%rbp), %rax
	addq	%rdi, %rax
	cmpq	%rax, -96(%rbp)
	ja	.L693
.L259:
	movq	-88(%rbp), %r8
	addq	-160(%rbp), %r8
	xorl	%r13d, %r13d
	movl	$1, -112(%rbp)
	leaq	(%rsi,%r8), %rax
	movq	%rax, -88(%rbp)
.L264:
	movq	-176(%rbp), %rsi
	movzbl	(%rsi), %eax
	testb	%al, %al
	jne	.L266
.L686:
	movl	%r15d, %eax
	testq	%r13, %r13
	movq	%r14, %r15
	movl	%eax, %r14d
	je	.L157
	cmpq	$0, -80(%rbp)
	je	.L157
	leaq	1(%r13), %rax
	movq	%rax, -168(%rbp)
.L319:
	movzbl	0(%r13), %eax
	movq	-72(%rbp), %rbx
	movq	-168(%rbp), %rcx
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L694
.L268:
	andl	$16777215, %esi
	testl	%r14d, %r14d
	leal	1(%rsi), %r9d
	movslq	%esi, %rsi
	movzbl	(%r15,%rsi), %r8d
	jle	.L286
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L287:
	leal	(%r9,%r8), %edx
	addl	$1, %eax
	cmpl	%eax, %r14d
	leal	1(%rdx), %r9d
	movslq	%edx, %rdx
	movzbl	(%r15,%rdx), %r8d
	jne	.L287
.L286:
	cmpq	$1, -80(%rbp)
	jbe	.L288
	movl	$1, %r12d
	movq	%r13, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L309:
	movzbl	(%rcx), %eax
	movq	-72(%rbp), %rbx
	leaq	1(%rcx), %r13
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rdi
	js	.L695
	movq	%r13, %rcx
.L289:
	andl	$16777215, %edi
	testl	%r14d, %r14d
	leal	1(%rdi), %r9d
	movslq	%edi, %rdi
	movzbl	(%r15,%rdi), %eax
	jle	.L307
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L308:
	addl	%r9d, %eax
	addl	$1, %edx
	leal	1(%rax), %r9d
	cmpl	%edx, %r14d
	cltq
	movzbl	(%r15,%rax), %eax
	jne	.L308
.L307:
	addq	$1, %r12
	cmpq	-80(%rbp), %r12
	jne	.L309
	movq	-160(%rbp), %r13
	movq	%rax, %r8
.L288:
	testq	%r8, %r8
	je	.L310
	cmpl	$127, -112(%rbp)
	jg	.L696
	movzbl	-112(%rbp), %eax
	movl	$1, %edx
	movb	%al, -55(%rbp)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	leaq	(%rsi,%r8), %rax
	cmpq	%rax, -96(%rbp)
	ja	.L697
.L313:
	addq	-88(%rbp), %r8
	movl	$1, -112(%rbp)
	leaq	(%rdx,%r8), %rax
	movq	%rax, -88(%rbp)
.L318:
	subq	$1, -80(%rbp)
	jne	.L319
.L157:
	addq	$1, -152(%rbp)
	movq	-88(%rbp), %rbx
	cmpq	%rbx, -96(%rbp)
	movq	-152(%rbp), %rax
	jbe	.L320
	cmpq	%rax, -104(%rbp)
	movq	-128(%rbp), %rax
	seta	(%rax,%rbx)
.L320:
	movq	-88(%rbp), %rax
	movq	-152(%rbp), %rdi
	addq	$1, %rax
	cmpq	%rdi, -104(%rbp)
	je	.L698
	movq	%rax, -216(%rbp)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L689:
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L226:
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	js	.L699
.L227:
	testq	%rax, %rax
	je	.L417
	movzbl	1(%rcx), %ebx
	cmpb	%bl, 5(%rdx)
	jne	.L232
	xorl	%edx, %edx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L234:
	movzbl	1(%rcx,%rdx), %ebx
	cmpb	%bl, (%rsi,%rdx)
	jne	.L232
.L233:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L234
	leaq	0(%r13,%rdx), %rcx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	(%rsi,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L226
	movl	$4, %ebx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	jns	.L227
.L699:
	testq	%rax, %rax
	je	.L415
	movzbl	5(%rdx), %r9d
	movzbl	1(%rcx), %r8d
	cmpb	%r9b, %r8b
	jne	.L416
	xorl	%edx, %edx
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L236:
	movzbl	(%rsi,%rdx), %r11d
	movzbl	1(%rcx,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L229
.L230:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L236
	xorl	%eax, %eax
.L228:
	movq	-144(%rbp), %rbx
	subq	%rdi, %rax
	leaq	0(%r13,%rdx), %rcx
	movl	(%rbx,%rax,4), %edi
	jmp	.L225
.L416:
	movl	%r8d, %r10d
	movl	%r9d, %r11d
	.p2align 4,,10
	.p2align 3
.L229:
	cmpb	%r10b, %r11b
	ja	.L339
	leaq	(%rsi,%rax), %rbx
	movzbl	(%rbx), %edx
	cmpb	%dl, %r8b
	movb	%dl, -136(%rbp)
	jne	.L418
	xorl	%edx, %edx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L239:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%rcx,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L237
.L238:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L239
	movzbl	-136(%rbp), %r8d
.L340:
	xorl	%r10d, %r10d
	cmpb	%r8b, %r9b
	jne	.L241
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$1, %r10
	movzbl	(%rsi,%r10), %r9d
	movzbl	1(%rcx,%r10), %r8d
	cmpb	%r8b, %r9b
	je	.L240
.L241:
	movl	%r9d, %r11d
	xorl	%eax, %eax
	movl	%r8d, %r9d
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L700:
	movzbl	1(%rcx,%r10), %r9d
	movzbl	(%rsi,%r10), %r11d
.L242:
	movzbl	%r9b, %r8d
	movzbl	%r11b, %r9d
	salq	$8, %rax
	subl	%r9d, %r8d
	addq	$1, %r10
	movslq	%r8d, %r8
	addq	%r8, %rax
	cmpq	%r10, %rdx
	ja	.L700
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L339:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rsi, %rdx
	jmp	.L226
.L418:
	movzbl	-136(%rbp), %r11d
	movl	%r8d, %r10d
	.p2align 4,,10
	.p2align 3
.L237:
	cmpb	%r10b, %r11b
	jb	.L339
	movq	%rax, %rdx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L417:
	xorl	%edx, %edx
	leaq	0(%r13,%rdx), %rcx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L246:
	addl	$1, -112(%rbp)
	jmp	.L254
.L202:
	testq	%r13, %r13
	cmove	%rsi, %r13
	addq	$1, -80(%rbp)
	jmp	.L264
.L415:
	xorl	%edx, %edx
	jmp	.L228
.L688:
	movq	-120(%rbp), %rcx
	movl	$4, %r12d
	subq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L205:
	movslq	(%rcx), %rsi
	leaq	5(%rcx), %rdx
	movzbl	4(%rcx), %eax
	testl	%esi, %esi
	js	.L701
.L206:
	testq	%rax, %rax
	je	.L411
	movzbl	5(%rcx), %ebx
	cmpb	%bl, 1(%r13)
	jne	.L211
	xorl	%ecx, %ecx
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L213:
	movzbl	1(%r13,%rcx), %ebx
	cmpb	%bl, (%rdx,%rcx)
	jne	.L211
.L212:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L213
	addq	-184(%rbp), %rcx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	(%rdx,%rax), %rcx
	addq	$1, %rax
	andl	$3, %eax
	je	.L205
	movq	%r12, %rbx
	subq	%rax, %rbx
	addq	%rbx, %rcx
	movslq	(%rcx), %rsi
	leaq	5(%rcx), %rdx
	movzbl	4(%rcx), %eax
	testl	%esi, %esi
	jns	.L206
.L701:
	testq	%rax, %rax
	je	.L409
	movzbl	5(%rcx), %r8d
	movzbl	1(%r13), %edi
	cmpb	%r8b, %dil
	jne	.L410
	xorl	%ecx, %ecx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L215:
	movzbl	(%rdx,%rcx), %r10d
	movzbl	1(%r13,%rcx), %r9d
	cmpb	%r9b, %r10b
	jne	.L208
.L209:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L215
	xorl	%eax, %eax
.L207:
	movq	-144(%rbp), %rbx
	subq	%rsi, %rax
	addq	-184(%rbp), %rcx
	movl	(%rbx,%rax,4), %esi
	jmp	.L204
.L410:
	movl	%edi, %r9d
	movl	%r8d, %r10d
.L208:
	cmpb	%r9b, %r10b
	ja	.L337
	leaq	(%rdx,%rax), %r11
	movzbl	(%r11), %ebx
	cmpb	%bl, %dil
	jne	.L412
	xorl	%ecx, %ecx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L218:
	movzbl	(%r11,%rcx), %r10d
	movzbl	1(%r13,%rcx), %r9d
	cmpb	%r9b, %r10b
	jne	.L216
.L217:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L218
	movzbl	%bl, %edi
.L338:
	xorl	%r9d, %r9d
	cmpb	%dil, %r8b
	jne	.L220
	.p2align 4,,10
	.p2align 3
.L219:
	addq	$1, %r9
	movzbl	(%rdx,%r9), %r8d
	movzbl	1(%r13,%r9), %edi
	cmpb	%dil, %r8b
	je	.L219
.L220:
	xorl	%eax, %eax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L702:
	movzbl	1(%r13,%r9), %edi
	movzbl	(%rdx,%r9), %r8d
.L221:
	subl	%r8d, %edi
	salq	$8, %rax
	addq	$1, %r9
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%rcx, %r9
	jb	.L702
	jmp	.L207
.L337:
	addq	%rax, %rax
	leaq	4(%rax), %rcx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rcx
	addq	%rdx, %rcx
	jmp	.L205
.L412:
	movl	%edi, %r9d
	movl	%ebx, %r10d
.L216:
	cmpb	%r9b, %r10b
	jb	.L337
	movq	%rax, %rcx
	jmp	.L338
.L690:
	movl	-112(%rbp), %esi
	movq	-224(%rbp), %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rdx
	movq	-88(%rbp), %rax
	leaq	(%rdx,%rax), %rsi
	leaq	(%rsi,%r8), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L249
	testq	%rdx, %rdx
	je	.L251
	movq	-128(%rbp), %rbx
	movq	-88(%rbp), %rdi
	xorl	%eax, %eax
	movzbl	-55(%rbp), %ecx
	movq	-224(%rbp), %r10
	addq	%rbx, %rdi
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L703:
	movzbl	(%r10,%rax), %ecx
.L252:
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L703
.L251:
	movq	-128(%rbp), %rbx
	movslq	%r9d, %r9
	xorl	%eax, %eax
	leaq	(%rbx,%rsi), %rcx
	leaq	(%r14,%r9), %rsi
	.p2align 4,,10
	.p2align 3
.L253:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L253
	jmp	.L249
.L256:
	addl	$1, -112(%rbp)
	xorl	%r13d, %r13d
	jmp	.L264
.L687:
	movq	-120(%rbp), %rdx
	movl	$4, %r12d
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L183:
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	js	.L704
.L184:
	testq	%rax, %rax
	je	.L405
	movzbl	5(%rdx), %ebx
	cmpb	%bl, 1(%rsi)
	jne	.L189
	xorl	%edx, %edx
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L191:
	movzbl	1(%rsi,%rdx), %ebx
	cmpb	%bl, (%rcx,%rdx)
	jne	.L189
.L190:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L191
	addq	%rdx, -176(%rbp)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L183
	movq	%r12, %rbx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	jns	.L184
.L704:
	testq	%rax, %rax
	je	.L403
	movzbl	5(%rdx), %r9d
	movzbl	1(%rsi), %r8d
	cmpb	%r9b, %r8b
	jne	.L404
	xorl	%edx, %edx
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L193:
	movzbl	(%rcx,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L186
.L187:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L193
	xorl	%eax, %eax
.L185:
	movq	-144(%rbp), %rbx
	subq	%rdi, %rax
	addq	%rdx, -176(%rbp)
	movl	(%rbx,%rax,4), %edi
	jmp	.L182
.L404:
	movl	%r8d, %r10d
	movl	%r9d, %r11d
.L186:
	cmpb	%r10b, %r11b
	ja	.L335
	leaq	(%rcx,%rax), %rbx
	movzbl	(%rbx), %edx
	cmpb	%dl, %r8b
	movb	%dl, -136(%rbp)
	jne	.L406
	xorl	%edx, %edx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L196:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%rsi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L194
.L195:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L196
	movzbl	-136(%rbp), %r8d
.L336:
	xorl	%r10d, %r10d
	cmpb	%r8b, %r9b
	jne	.L198
	.p2align 4,,10
	.p2align 3
.L197:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r9d
	movzbl	1(%rsi,%r10), %r8d
	cmpb	%r8b, %r9b
	je	.L197
.L198:
	xorl	%eax, %eax
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L705:
	movzbl	1(%rsi,%r10), %r8d
	movzbl	(%rcx,%r10), %r9d
.L199:
	salq	$8, %rax
	addq	$1, %r10
	movq	%rax, %r11
	movzbl	%r8b, %eax
	movzbl	%r9b, %r8d
	subl	%r8d, %eax
	cltq
	addq	%r11, %rax
	cmpq	%rdx, %r10
	jb	.L705
	jmp	.L185
.L335:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L183
.L406:
	movzbl	-136(%rbp), %r11d
	movl	%r8d, %r10d
.L194:
	cmpb	%r10b, %r11b
	jb	.L335
	movq	%rax, %rdx
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L695:
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L290:
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	js	.L706
.L291:
	testq	%rax, %rax
	je	.L430
	movzbl	5(%rdx), %ebx
	cmpb	%bl, 1(%rcx)
	jne	.L296
	xorl	%edx, %edx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L298:
	movzbl	1(%rcx,%rdx), %ebx
	cmpb	%bl, (%rsi,%rdx)
	jne	.L296
.L297:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L298
	leaq	0(%r13,%rdx), %rcx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	(%rsi,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L290
	movl	$4, %ebx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %rdi
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%edi, %edi
	jns	.L291
.L706:
	testq	%rax, %rax
	je	.L428
	movzbl	5(%rdx), %r9d
	movzbl	1(%rcx), %r8d
	cmpb	%r8b, %r9b
	jne	.L429
	xorl	%edx, %edx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L300:
	movzbl	(%rsi,%rdx), %r11d
	movzbl	1(%rcx,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L293
.L294:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L300
	xorl	%eax, %eax
.L292:
	movq	-144(%rbp), %rbx
	subq	%rdi, %rax
	leaq	0(%r13,%rdx), %rcx
	movl	(%rbx,%rax,4), %edi
	jmp	.L289
.L429:
	movl	%r8d, %r10d
	movl	%r9d, %r11d
	.p2align 4,,10
	.p2align 3
.L293:
	cmpb	%r10b, %r11b
	ja	.L343
	leaq	(%rsi,%rax), %rbx
	movzbl	(%rbx), %edx
	cmpb	%r8b, %dl
	movb	%dl, -136(%rbp)
	jne	.L431
	xorl	%edx, %edx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L303:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%rcx,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L301
.L302:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L303
	movzbl	-136(%rbp), %r8d
.L344:
	xorl	%r10d, %r10d
	cmpb	%r8b, %r9b
	jne	.L305
	.p2align 4,,10
	.p2align 3
.L304:
	addq	$1, %r10
	movzbl	(%rsi,%r10), %r9d
	movzbl	1(%rcx,%r10), %r8d
	cmpb	%r8b, %r9b
	je	.L304
.L305:
	xorl	%eax, %eax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L707:
	movzbl	1(%rcx,%r10), %r8d
	movzbl	(%rsi,%r10), %r9d
.L306:
	subl	%r9d, %r8d
	salq	$8, %rax
	addq	$1, %r10
	movslq	%r8d, %r8
	addq	%r8, %rax
	cmpq	%r10, %rdx
	ja	.L707
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L343:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rsi, %rdx
	jmp	.L290
.L431:
	movzbl	-136(%rbp), %r11d
	movl	%r8d, %r10d
.L301:
	cmpb	%r10b, %r11b
	jb	.L343
	movq	%rax, %rdx
	jmp	.L344
.L430:
	xorl	%edx, %edx
	leaq	0(%r13,%rdx), %rcx
	jmp	.L289
.L310:
	addl	$1, -112(%rbp)
	jmp	.L318
.L691:
	movzbl	-112(%rbp), %edi
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movq	-88(%rbp), %rbx
	movb	%dil, (%rax,%rbx)
	jmp	.L251
.L381:
	movq	-216(%rbp), %rbx
	xorl	%r13d, %r13d
	movq	-208(%rbp), %rdi
	movq	$0, -112(%rbp)
	movq	%rbx, -88(%rbp)
	movq	-200(%rbp), %rbx
	addq	-152(%rbp), %rbx
	testb	%al, %al
	movq	%rbx, -136(%rbp)
	je	.L708
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	1(%rdi), %rbx
	movq	%rbx, -80(%rbp)
	movq	-72(%rbp), %rbx
	movslq	(%rbx,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L709
.L107:
	movl	%esi, %eax
	andl	$16777215, %esi
	shrl	$24, %eax
	leal	1(%rsi), %r12d
	testl	%r14d, %r14d
	movslq	%esi, %rsi
	movzbl	(%r15,%rsi), %esi
	jle	.L125
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L126:
	leal	(%r12,%rsi), %ecx
	addl	$1, %edx
	cmpl	%edx, %r14d
	leal	1(%rcx), %r12d
	movslq	%ecx, %rcx
	movzbl	(%r15,%rcx), %esi
	jne	.L126
.L125:
	movzbl	%al, %eax
	movq	-136(%rbp), %rbx
	imulq	-104(%rbp), %rax
	testb	$1, (%rbx,%rax)
	je	.L127
	testq	%r13, %r13
	movq	-88(%rbp), %rax
	je	.L128
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L129
	movq	%rax, %rbx
	movl	%r12d, -168(%rbp)
	movq	%rsi, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L152:
	movzbl	0(%r13), %eax
	movq	-72(%rbp), %rdi
	leaq	1(%r13), %r12
	movslq	(%rdi,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L710
	movq	%r12, %r13
.L130:
	andl	$16777215, %esi
	testl	%r14d, %r14d
	leal	1(%rsi), %ecx
	movslq	%esi, %rsi
	movzbl	(%r15,%rsi), %eax
	jle	.L148
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L149:
	addl	%ecx, %eax
	addl	$1, %edx
	leal	1(%rax), %ecx
	cmpl	%edx, %r14d
	cltq
	movzbl	(%r15,%rax), %eax
	jne	.L149
.L148:
	movq	-88(%rbp), %rdi
	leaq	(%rdi,%rbx), %rsi
	cmpq	%rsi, -96(%rbp)
	ja	.L711
.L150:
	subq	%rax, %rbx
	jne	.L152
	movl	-168(%rbp), %r12d
	movq	-176(%rbp), %rsi
.L129:
	movq	-88(%rbp), %rax
	addq	-112(%rbp), %rax
	movq	$0, -112(%rbp)
.L128:
	leaq	(%rax,%rsi), %rbx
	xorl	%r13d, %r13d
	cmpq	%rbx, -96(%rbp)
	movq	%rbx, -88(%rbp)
	ja	.L712
.L153:
	movq	-80(%rbp), %rdi
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L156
.L708:
	testq	%r13, %r13
	je	.L157
	movq	-112(%rbp), %r9
	testq	%r9, %r9
	je	.L158
	movl	$4, %r12d
.L181:
	movzbl	0(%r13), %eax
	movq	-72(%rbp), %rdi
	leaq	1(%r13), %rbx
	movslq	(%rdi,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L713
	movq	%rbx, %r13
.L159:
	andl	$16777215, %esi
	testl	%r14d, %r14d
	leal	1(%rsi), %ecx
	movslq	%esi, %rsi
	movzbl	(%r15,%rsi), %eax
	jle	.L177
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L178:
	addl	%ecx, %eax
	addl	$1, %edx
	leal	1(%rax), %ecx
	cmpl	%edx, %r14d
	cltq
	movzbl	(%r15,%rax), %eax
	jne	.L178
.L177:
	movq	-88(%rbp), %rbx
	leaq	(%rbx,%r9), %rdi
	cmpq	%rdi, -96(%rbp)
	ja	.L714
.L179:
	subq	%rax, %r9
	jne	.L181
.L158:
	movq	-112(%rbp), %rbx
	addq	%rbx, -88(%rbp)
	jmp	.L157
.L711:
	testq	%rax, %rax
	je	.L150
	subq	%rax, %rsi
	addq	-128(%rbp), %rsi
	movslq	%ecx, %rcx
	leaq	(%r15,%rcx), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L151:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L151
	jmp	.L150
.L127:
	testq	%r13, %r13
	cmove	%rdi, %r13
	addq	%rsi, -112(%rbp)
	jmp	.L153
.L710:
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L131:
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	js	.L715
.L132:
	testq	%rax, %rax
	je	.L391
	movzbl	1(%r13), %edi
	cmpb	%dil, 5(%rdx)
	jne	.L137
	xorl	%edx, %edx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movzbl	1(%r13,%rdx), %edi
	cmpb	%dil, (%rcx,%rdx)
	jne	.L137
.L138:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L139
	leaq	(%r12,%rdx), %r13
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L131
	movl	$4, %edi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	jns	.L132
.L715:
	testq	%rax, %rax
	je	.L389
	movzbl	5(%rdx), %r8d
	movzbl	1(%r13), %edi
	cmpb	%r8b, %dil
	jne	.L390
	xorl	%edx, %edx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L141:
	movzbl	(%rcx,%rdx), %r10d
	movzbl	1(%r13,%rdx), %r9d
	cmpb	%r9b, %r10b
	jne	.L134
.L135:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L141
	xorl	%eax, %eax
.L133:
	movq	-144(%rbp), %rdi
	subq	%rsi, %rax
	leaq	(%r12,%rdx), %r13
	movl	(%rdi,%rax,4), %esi
	jmp	.L130
.L390:
	movl	%edi, %r9d
	movl	%r8d, %r10d
.L134:
	cmpb	%r9b, %r10b
	ja	.L331
	leaq	(%rcx,%rax), %r11
	movzbl	(%r11), %edx
	cmpb	%dl, %dil
	movb	%dl, -160(%rbp)
	jne	.L392
	xorl	%edx, %edx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	movzbl	(%r11,%rdx), %r10d
	movzbl	1(%r13,%rdx), %r9d
	cmpb	%r9b, %r10b
	jne	.L142
.L143:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L144
	movzbl	-160(%rbp), %edi
.L332:
	xorl	%r9d, %r9d
	cmpb	%dil, %r8b
	jne	.L146
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$1, %r9
	movzbl	(%rcx,%r9), %r8d
	movzbl	1(%r13,%r9), %edi
	cmpb	%dil, %r8b
	je	.L145
.L146:
	xorl	%eax, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L716:
	movzbl	1(%r13,%r9), %edi
	movzbl	(%rcx,%r9), %r8d
.L147:
	subl	%r8d, %edi
	salq	$8, %rax
	addq	$1, %r9
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%rdx, %r9
	jb	.L716
	jmp	.L133
.L331:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L131
.L392:
	movzbl	-160(%rbp), %r10d
	movl	%edi, %r9d
.L142:
	cmpb	%r9b, %r10b
	jb	.L331
	movq	%rax, %rdx
	jmp	.L332
.L709:
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L108:
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	js	.L717
.L109:
	testq	%rax, %rax
	je	.L384
	movzbl	1(%rdi), %ebx
	cmpb	%bl, 5(%rdx)
	jne	.L114
	xorl	%edx, %edx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L116:
	movzbl	1(%rdi,%rdx), %ebx
	cmpb	%bl, (%rcx,%rdx)
	jne	.L114
.L115:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L116
	addq	%rdx, -80(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L108
	movl	$4, %ebx
	subq	%rax, %rbx
	addq	%rbx, %rdx
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	jns	.L109
.L717:
	testq	%rax, %rax
	je	.L382
	movzbl	5(%rdx), %r9d
	movzbl	1(%rdi), %r8d
	cmpb	%r9b, %r8b
	jne	.L383
	xorl	%edx, %edx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L118:
	movzbl	(%rcx,%rdx), %r11d
	movzbl	1(%rdi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L111
.L112:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L118
	xorl	%eax, %eax
.L110:
	movq	-144(%rbp), %rbx
	subq	%rsi, %rax
	addq	%rdx, -80(%rbp)
	movl	(%rbx,%rax,4), %esi
	jmp	.L107
.L383:
	movl	%r8d, %r10d
	movl	%r9d, %r11d
.L111:
	cmpb	%r10b, %r11b
	ja	.L329
	leaq	(%rcx,%rax), %rbx
	movzbl	(%rbx), %r12d
	cmpb	%r12b, %r8b
	jne	.L385
	xorl	%edx, %edx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L121:
	movzbl	(%rbx,%rdx), %r11d
	movzbl	1(%rdi,%rdx), %r10d
	cmpb	%r10b, %r11b
	jne	.L119
.L120:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L121
	movl	%r12d, %r8d
.L330:
	xorl	%r10d, %r10d
	cmpb	%r8b, %r9b
	jne	.L123
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r9d
	movzbl	1(%rdi,%r10), %r8d
	cmpb	%r8b, %r9b
	je	.L122
.L123:
	xorl	%eax, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L718:
	movzbl	1(%rdi,%r10), %r8d
	movzbl	(%rcx,%r10), %r9d
.L124:
	salq	$8, %rax
	addq	$1, %r10
	movq	%rax, %r11
	movzbl	%r8b, %eax
	movzbl	%r9b, %r8d
	subl	%r8d, %eax
	cltq
	addq	%r11, %rax
	cmpq	%rdx, %r10
	jb	.L718
	jmp	.L110
.L329:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L108
.L385:
	movl	%r8d, %r10d
	movl	%r12d, %r11d
.L119:
	cmpb	%r10b, %r11b
	jb	.L329
	movq	%rax, %rdx
	jmp	.L330
.L714:
	testq	%rax, %rax
	je	.L179
	movq	-128(%rbp), %rbx
	subq	%rax, %rdi
	movslq	%ecx, %rdx
	xorl	%esi, %esi
	addq	%r15, %rdx
	leaq	(%rbx,%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L180:
	movzbl	(%rdx,%rsi), %edi
	movb	%dil, (%rcx,%rsi)
	addq	$1, %rsi
	cmpq	%rsi, %rax
	jne	.L180
	jmp	.L179
.L712:
	testq	%rsi, %rsi
	je	.L395
	movq	-128(%rbp), %rbx
	xorl	%edx, %edx
	leaq	(%rbx,%rax), %rcx
	movslq	%r12d, %rax
	addq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L154:
	movzbl	(%rax,%rdx), %edi
	movb	%dil, (%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L154
	xorl	%r13d, %r13d
	jmp	.L153
.L428:
	xorl	%edx, %edx
	jmp	.L292
.L391:
	xorl	%edx, %edx
	leaq	(%r12,%rdx), %r13
	jmp	.L130
.L411:
	xorl	%ecx, %ecx
	addq	-184(%rbp), %rcx
	jmp	.L204
.L685:
	movq	-120(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movl	$4, %r12d
	subq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L89:
	movslq	(%rsi), %rbx
	leaq	5(%rsi), %rcx
	movzbl	4(%rsi), %edx
	testl	%ebx, %ebx
	js	.L719
.L90:
	testq	%rdx, %rdx
	je	.L88
	movzbl	5(%rsi), %esi
	cmpb	%sil, 1(%rdi)
	jne	.L94
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %rsi
	cmpq	%rsi, %rdx
	je	.L88
	movzbl	1(%rdi,%rsi), %r11d
	cmpb	%r11b, (%rcx,%rsi)
	je	.L95
.L94:
	leaq	(%rcx,%rdx), %rsi
	addq	$1, %rdx
	andl	$3, %edx
	je	.L89
	movq	%r12, %rbx
	subq	%rdx, %rbx
	addq	%rbx, %rsi
	movslq	(%rsi), %rbx
	leaq	5(%rsi), %rcx
	movzbl	4(%rsi), %edx
	testl	%ebx, %ebx
	jns	.L90
.L719:
	testq	%rdx, %rdx
	je	.L377
	movzbl	5(%rsi), %esi
	movzbl	1(%rdi), %r8d
	cmpb	%r8b, %sil
	jne	.L378
	xorl	%r9d, %r9d
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L98:
	movzbl	(%rcx,%r9), %r11d
	movzbl	1(%rdi,%r9), %r10d
	cmpb	%r10b, %r11b
	jne	.L92
.L93:
	addq	$1, %r9
	cmpq	%r9, %rdx
	jne	.L98
.L377:
	xorl	%edi, %edi
.L91:
	subq	%rbx, %rdi
	movq	-144(%rbp), %rbx
	movl	(%rbx,%rdi,4), %ebx
	jmp	.L88
.L378:
	movl	%r8d, %r10d
	movl	%esi, %r11d
.L92:
	cmpb	%r10b, %r11b
	ja	.L327
	leaq	(%rcx,%rdx), %r13
	movzbl	0(%r13), %r11d
	cmpb	%r11b, %r8b
	movb	%r11b, -80(%rbp)
	jne	.L379
	xorl	%r9d, %r9d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	0(%r13,%r9), %r11d
	movzbl	1(%rdi,%r9), %r10d
	cmpb	%r10b, %r11b
	jne	.L99
.L100:
	addq	$1, %r9
	cmpq	%r9, %rdx
	jne	.L101
	movzbl	-80(%rbp), %r13d
	movl	%r13d, %r8d
.L328:
	xorl	%r9d, %r9d
	cmpb	%r8b, %sil
	movq	-208(%rbp), %rdi
	jne	.L103
.L102:
	addq	$1, %r9
	movzbl	(%rcx,%r9), %esi
	movzbl	1(%rdi,%r9), %r8d
	cmpb	%r8b, %sil
	je	.L102
.L103:
	xorl	%edi, %edi
	movq	-208(%rbp), %r11
	jmp	.L104
.L720:
	movzbl	1(%r11,%r9), %r8d
	movzbl	(%rcx,%r9), %esi
.L104:
	salq	$8, %rdi
	addq	$1, %r9
	movq	%rdi, %r10
	movzbl	%r8b, %edi
	subl	%esi, %edi
	cmpq	%rdx, %r9
	movslq	%edi, %rsi
	leaq	(%rsi,%r10), %rdi
	jb	.L720
	jmp	.L91
.L379:
	movzbl	-80(%rbp), %r11d
	movl	%r8d, %r10d
.L99:
	cmpb	%r10b, %r11b
	jnb	.L328
.L327:
	addq	%rdx, %rdx
	leaq	4(%rdx), %rsi
	addq	$1, %rdx
	andl	$3, %edx
	subq	%rdx, %rsi
	addq	%rcx, %rsi
	jmp	.L89
.L713:
	movq	-120(%rbp), %rdx
	subq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L160:
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	js	.L721
.L161:
	testq	%rax, %rax
	je	.L400
	movzbl	1(%r13), %edi
	cmpb	%dil, 5(%rdx)
	jne	.L166
	xorl	%edx, %edx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L168:
	movzbl	1(%r13,%rdx), %edi
	cmpb	%dil, (%rcx,%rdx)
	jne	.L166
.L167:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L168
	leaq	(%rbx,%rdx), %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L160
	movq	%r12, %rdi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movslq	(%rdx), %rsi
	leaq	5(%rdx), %rcx
	movzbl	4(%rdx), %eax
	testl	%esi, %esi
	jns	.L161
.L721:
	testq	%rax, %rax
	je	.L398
	movzbl	5(%rdx), %edi
	movl	%edi, %edx
	movb	%dil, -80(%rbp)
	movzbl	1(%r13), %edi
	cmpb	%dl, %dil
	jne	.L399
	xorl	%edx, %edx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L170:
	movzbl	(%rcx,%rdx), %r10d
	movzbl	1(%r13,%rdx), %r8d
	cmpb	%r8b, %r10b
	jne	.L163
.L164:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L170
	xorl	%eax, %eax
.L162:
	leaq	(%rbx,%rdx), %r13
	movq	-144(%rbp), %rbx
	subq	%rsi, %rax
	movl	(%rbx,%rax,4), %esi
	jmp	.L159
.L399:
	movzbl	-80(%rbp), %r10d
	movl	%edi, %r8d
.L163:
	cmpb	%r8b, %r10b
	ja	.L333
	leaq	(%rcx,%rax), %r11
	movzbl	(%r11), %edx
	cmpb	%dl, %dil
	movb	%dl, -136(%rbp)
	jne	.L401
	xorl	%edx, %edx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L173:
	movzbl	(%r11,%rdx), %r10d
	movzbl	1(%r13,%rdx), %r8d
	cmpb	%r8b, %r10b
	jne	.L171
.L172:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L173
	movzbl	-80(%rbp), %r8d
	movzbl	-136(%rbp), %edi
.L334:
	xorl	%r10d, %r10d
	cmpb	%r8b, %dil
	jne	.L175
.L174:
	addq	$1, %r10
	movzbl	(%rcx,%r10), %r8d
	movzbl	1(%r13,%r10), %edi
	cmpb	%dil, %r8b
	je	.L174
.L175:
	xorl	%eax, %eax
	movq	%rax, %r11
	jmp	.L176
.L722:
	movzbl	1(%r13,%r10), %edi
	movzbl	(%rcx,%r10), %r8d
.L176:
	subl	%r8d, %edi
	salq	$8, %r11
	addq	$1, %r10
	movslq	%edi, %rax
	addq	%rax, %r11
	cmpq	%rdx, %r10
	jb	.L722
	movq	%r11, %rax
	jmp	.L162
.L333:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L160
.L401:
	movzbl	-136(%rbp), %r10d
	movl	%edi, %r8d
.L171:
	cmpb	%r8b, %r10b
	jb	.L333
	movzbl	-80(%rbp), %r8d
	movq	%rax, %rdx
	jmp	.L334
.L694:
	movq	-120(%rbp), %rcx
	movl	$4, %ebx
	subq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L269:
	movslq	(%rcx), %rsi
	leaq	5(%rcx), %rdx
	movzbl	4(%rcx), %eax
	testl	%esi, %esi
	js	.L723
.L270:
	testq	%rax, %rax
	je	.L424
	movzbl	5(%rcx), %edi
	cmpb	%dil, 1(%r13)
	jne	.L275
	xorl	%ecx, %ecx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L277:
	movzbl	1(%r13,%rcx), %edi
	cmpb	%dil, (%rdx,%rcx)
	jne	.L275
.L276:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L277
	addq	-168(%rbp), %rcx
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	(%rdx,%rax), %rcx
	addq	$1, %rax
	andl	$3, %eax
	je	.L269
	movq	%rbx, %rdi
	subq	%rax, %rdi
	addq	%rdi, %rcx
	movslq	(%rcx), %rsi
	leaq	5(%rcx), %rdx
	movzbl	4(%rcx), %eax
	testl	%esi, %esi
	jns	.L270
.L723:
	testq	%rax, %rax
	je	.L422
	movzbl	5(%rcx), %r8d
	movzbl	1(%r13), %edi
	cmpb	%dil, %r8b
	jne	.L423
	xorl	%ecx, %ecx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L279:
	movzbl	(%rdx,%rcx), %r10d
	movzbl	1(%r13,%rcx), %r9d
	cmpb	%r9b, %r10b
	jne	.L272
.L273:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L279
	xorl	%eax, %eax
.L271:
	movq	-144(%rbp), %rbx
	subq	%rsi, %rax
	addq	-168(%rbp), %rcx
	movl	(%rbx,%rax,4), %esi
	jmp	.L268
.L423:
	movl	%edi, %r9d
	movl	%r8d, %r10d
.L272:
	cmpb	%r9b, %r10b
	ja	.L341
	leaq	(%rdx,%rax), %r11
	movzbl	(%r11), %r12d
	cmpb	%r12b, %dil
	jne	.L425
	xorl	%ecx, %ecx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L282:
	movzbl	(%r11,%rcx), %r10d
	movzbl	1(%r13,%rcx), %r9d
	cmpb	%r9b, %r10b
	jne	.L280
.L281:
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L282
	movzbl	%r12b, %edi
.L342:
	xorl	%r9d, %r9d
	cmpb	%dil, %r8b
	jne	.L284
.L283:
	addq	$1, %r9
	movzbl	(%rdx,%r9), %r8d
	movzbl	1(%r13,%r9), %edi
	cmpb	%dil, %r8b
	je	.L283
.L284:
	xorl	%eax, %eax
	jmp	.L285
.L724:
	movzbl	1(%r13,%r9), %edi
	movzbl	(%rdx,%r9), %r8d
.L285:
	salq	$8, %rax
	addq	$1, %r9
	movq	%rax, %r10
	movzbl	%r8b, %eax
	subl	%eax, %edi
	movslq	%edi, %rax
	addq	%r10, %rax
	cmpq	%r9, %rcx
	ja	.L724
	jmp	.L271
.L341:
	addq	%rax, %rax
	leaq	4(%rax), %rcx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rcx
	addq	%rdx, %rcx
	jmp	.L269
.L425:
	movl	%edi, %r9d
	movl	%r12d, %r10d
.L280:
	cmpb	%r9b, %r10b
	jb	.L341
	movq	%rax, %rcx
	jmp	.L342
.L384:
	xorl	%edx, %edx
	addq	%rdx, -80(%rbp)
	jmp	.L107
.L696:
	movl	-112(%rbp), %esi
	movq	-224(%rbp), %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rdx
	movq	-88(%rbp), %rax
	leaq	(%rdx,%rax), %rsi
	leaq	(%rsi,%r8), %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L313
	testq	%rdx, %rdx
	je	.L315
	movq	-128(%rbp), %rbx
	movq	-88(%rbp), %rdi
	xorl	%eax, %eax
	movzbl	-55(%rbp), %ecx
	movq	-224(%rbp), %r10
	addq	%rbx, %rdi
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L725:
	movzbl	(%r10,%rax), %ecx
.L316:
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	ja	.L725
.L315:
	movq	-128(%rbp), %rbx
	movslq	%r9d, %r9
	xorl	%eax, %eax
	leaq	(%rbx,%rsi), %rcx
	leaq	(%r15,%r9), %rsi
.L317:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L317
	jmp	.L313
.L405:
	xorl	%edx, %edx
	addq	%rdx, -176(%rbp)
	jmp	.L182
.L692:
	movl	-112(%rbp), %esi
	movq	-224(%rbp), %rdi
	call	utf8_encode.part.0
	movslq	%eax, %rsi
	movq	-88(%rbp), %rax
	leaq	(%rsi,%rax), %rdi
	movq	-160(%rbp), %rax
	addq	%rdi, %rax
	cmpq	%rax, -96(%rbp)
	jbe	.L259
	testq	%rsi, %rsi
	je	.L261
	movq	-128(%rbp), %rbx
	movq	-88(%rbp), %rcx
	xorl	%eax, %eax
	movzbl	-55(%rbp), %edx
	movq	-224(%rbp), %r8
	addq	%rbx, %rcx
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L726:
	movzbl	(%r8,%rax), %edx
.L262:
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	ja	.L726
.L261:
	movslq	-188(%rbp), %rdx
	movq	-128(%rbp), %rbx
	xorl	%eax, %eax
	movq	-160(%rbp), %r8
	leaq	(%rbx,%rdi), %rcx
	addq	%r14, %rdx
.L263:
	movzbl	(%rdx,%rax), %edi
	movb	%dil, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L263
	jmp	.L259
.L389:
	xorl	%edx, %edx
	jmp	.L133
.L409:
	xorl	%ecx, %ecx
	jmp	.L207
.L697:
	movzbl	-112(%rbp), %edi
	movq	-128(%rbp), %rax
	movl	$1, %edx
	movq	-88(%rbp), %rbx
	movb	%dil, (%rax,%rbx)
	jmp	.L315
.L698:
	cmpq	$2, %rax
	jbe	.L10
	movq	-216(%rbp), %rdx
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L10
	jmp	.L661
.L424:
	xorl	%ecx, %ecx
	addq	-168(%rbp), %rcx
	jmp	.L268
.L400:
	xorl	%edx, %edx
	leaq	(%rbx,%rdx), %r13
	jmp	.L159
.L382:
	xorl	%edx, %edx
	jmp	.L110
.L693:
	movzbl	-112(%rbp), %esi
	movq	-128(%rbp), %rax
	movq	-88(%rbp), %rbx
	movb	%sil, (%rax,%rbx)
	movl	$1, %esi
	jmp	.L261
.L403:
	xorl	%edx, %edx
	jmp	.L185
.L422:
	xorl	%ecx, %ecx
	jmp	.L271
.L398:
	xorl	%edx, %edx
	jmp	.L162
.L395:
	movq	%rax, -88(%rbp)
	jmp	.L153
.L665:
	leaq	__PRETTY_FUNCTION__.8204(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$708, %edx
	call	__assert_fail
.L664:
	leaq	__PRETTY_FUNCTION__.8204(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$705, %edx
	call	__assert_fail
	.size	__strxfrm_l, .-__strxfrm_l
	.weak	strxfrm_l
	.set	strxfrm_l,__strxfrm_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8204, @object
	.size	__PRETTY_FUNCTION__.8204, 12
__PRETTY_FUNCTION__.8204:
	.string	"__strxfrm_l"
	.hidden	__assert_fail
	.hidden	__stpncpy
	.hidden	strlen
