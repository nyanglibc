	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	explicit_bzero
	.type	explicit_bzero, @function
explicit_bzero:
	subq	$8, %rsp
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	__GI_memset@PLT
	addq	$8, %rsp
	ret
	.size	explicit_bzero, .-explicit_bzero
