 .text
.globl strcspn
.type strcspn,@function
.align 1<<4
strcspn:
 movq %rdi, %rdx
 movq %rdi, %r8
 subq $256, %rsp
 movl $32, %ecx
 movq %rsp, %rdi
 xorl %eax, %eax
 cld
 rep
 stosq
 movq %rsi, %rax
 .p2align 4
.L2: movb (%rax), %cl
 testb %cl, %cl
 jz .L1
 movb %cl, (%rsp,%rcx)
 movb 1(%rax), %cl
 testb $0xff, %cl
 jz .L1
 movb %cl, (%rsp,%rcx)
 movb 2(%rax), %cl
 testb $0xff, %cl
 jz .L1
 movb %cl, (%rsp,%rcx)
 movb 3(%rax), %cl
 addq $4, %rax
 movb %cl, (%rsp,%rcx)
 testb $0xff, %cl
 jnz .L2
.L1: leaq -4(%rdx), %rax
 .p2align 4
.L3: addq $4, %rax
 movb (%rax), %cl
 cmpb %cl, (%rsp,%rcx)
 je .L4
 movb 1(%rax), %cl
 cmpb %cl, (%rsp,%rcx)
 je .L5
 movb 2(%rax), %cl
 cmpb %cl, (%rsp,%rcx)
 jz .L6
 movb 3(%rax), %cl
 cmpb %cl, (%rsp,%rcx)
 jne .L3
 incq %rax
.L6: incq %rax
.L5: incq %rax
.L4: addq $256, %rsp
 subq %rdx, %rax
 ret
.size strcspn,.-strcspn
.globl __GI_strcspn
.set __GI_strcspn,strcspn
