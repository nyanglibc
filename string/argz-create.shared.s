	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__argz_create
	.type	__argz_create, @function
__argz_create:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	leaq	8(%r12), %rbp
	xorl	%ebx, %ebx
	movq	%rbp, %r15
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %r15
	call	__GI_strlen
	movq	-8(%r15), %rdi
	leaq	1(%rbx,%rax), %rbx
	testq	%rdi, %rdi
	jne	.L3
	testq	%rbx, %rbx
	jne	.L4
.L2:
	movq	$0, 0(%r13)
	xorl	%ebx, %ebx
.L5:
	movq	%rbx, (%r14)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, 0(%r13)
	je	.L8
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L7
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$8, %rbp
.L7:
	call	__GI_stpcpy@PLT
	movq	0(%rbp), %rsi
	leaq	1(%rax), %rdi
	testq	%rsi, %rsi
	jne	.L19
	jmp	.L5
.L8:
	movl	$12, %eax
	jmp	.L1
	.size	__argz_create, .-__argz_create
	.weak	argz_create
	.set	argz_create,__argz_create
