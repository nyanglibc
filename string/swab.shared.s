	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	swab
	.type	swab, @function
swab:
	andq	$-2, %rdx
	cmpq	$1, %rdx
	jle	.L1
	subq	$1, %rdx
	leaq	(%rdi,%rdx), %rax
	addq	%rdx, %rsi
	subq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	(%rax), %ecx
	movzbl	-1(%rax), %edx
	subq	$2, %rax
	subq	$2, %rsi
	movb	%cl, 1(%rsi)
	movb	%dl, 2(%rsi)
	cmpq	%rdi, %rax
	jne	.L3
.L1:
	rep ret
	.size	swab, .-swab
