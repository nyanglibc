 .section .text,"ax",@progbits
.globl __bzero
.type __bzero,@function
.align 1<<4
__bzero:
 mov %rdi, %rax
 mov %rsi, %rdx
 pxor %xmm0, %xmm0
 jmp .Lentry_from_bzero
.size __bzero,.-__bzero
.weak bzero
bzero = __bzero
.globl __wmemset
.type __wmemset,@function
.align 1<<4
__wmemset:
 shl $2, %rdx
 movd %esi, %xmm0
 movq %rdi, %rax
 pshufd $0, %xmm0, %xmm0
 jmp .Lentry_from_bzero
.size __wmemset,.-__wmemset
.globl memset
.type memset,@function
.align 1<<4
memset:
 movd %esi, %xmm0
 movq %rdi, %rax
 punpcklbw %xmm0, %xmm0
 punpcklwd %xmm0, %xmm0
 pshufd $0, %xmm0, %xmm0
.Lentry_from_bzero:
 cmpq $16, %rdx
 jb .Lless_vec
 cmpq $(16 * 2), %rdx
 ja .Lmore_2x_vec
 movdqu %xmm0, -16(%rdi,%rdx)
 movdqu %xmm0, (%rdi)

 ret
.Lmore_2x_vec:
 cmpq $(16 * 4), %rdx
 ja .Lloop_start
 movdqu %xmm0, (%rdi)
 movdqu %xmm0, 16(%rdi)
 movdqu %xmm0, -16(%rdi,%rdx)
 movdqu %xmm0, -(16 * 2)(%rdi,%rdx)
.Lreturn:

 ret
.Lloop_start:
 leaq (16 * 4)(%rdi), %rcx
 movdqu %xmm0, (%rdi)
 andq $-(16 * 4), %rcx
 movdqu %xmm0, -16(%rdi,%rdx)
 movdqu %xmm0, 16(%rdi)
 movdqu %xmm0, -(16 * 2)(%rdi,%rdx)
 movdqu %xmm0, (16 * 2)(%rdi)
 movdqu %xmm0, -(16 * 3)(%rdi,%rdx)
 movdqu %xmm0, (16 * 3)(%rdi)
 movdqu %xmm0, -(16 * 4)(%rdi,%rdx)
 addq %rdi, %rdx
 andq $-(16 * 4), %rdx
 cmpq %rdx, %rcx
 je .Lreturn
.Lloop:
 movdqa %xmm0, (%rcx)
 movdqa %xmm0, 16(%rcx)
 movdqa %xmm0, (16 * 2)(%rcx)
 movdqa %xmm0, (16 * 3)(%rcx)
 addq $(16 * 4), %rcx
 cmpq %rcx, %rdx
 jne .Lloop
 rep
 ret
.Lless_vec:
 movq %xmm0, %rcx
 cmpb $8, %dl
 jae .Lbetween_8_15
 cmpb $4, %dl
 jae .Lbetween_4_7
 cmpb $1, %dl
 ja .Lbetween_2_3
 jb 1f
 movb %cl, (%rdi)
1:

 ret
.Lbetween_8_15:
 movq %rcx, -8(%rdi,%rdx)
 movq %rcx, (%rdi)

 ret
.Lbetween_4_7:
 movl %ecx, -4(%rdi,%rdx)
 movl %ecx, (%rdi)

 ret
.Lbetween_2_3:
 movw %cx, -2(%rdi,%rdx)
 movw %cx, (%rdi)

 ret
.size memset,.-memset
.weak wmemset
wmemset = __wmemset

