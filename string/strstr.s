	.text
	.p2align 4,,15
	.type	two_way_long_needle, @function
two_way_long_needle:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r8
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rcx, %rbx
	movl	$1, %r11d
	movl	$1, %edx
	subq	$2136, %rsp
	xorl	%ecx, %ecx
	movq	$-1, %rdi
.L2:
	leaq	(%rdx,%rcx), %rax
	cmpq	%rax, %rbx
	jbe	.L74
.L7:
	leaq	0(%rbp,%rdx), %rsi
	movzbl	(%rsi,%rdi), %esi
	cmpb	%sil, 0(%rbp,%rax)
	jnb	.L3
	movq	%rax, %rcx
	movl	$1, %edx
	movq	%rax, %r11
	leaq	(%rdx,%rcx), %rax
	subq	%rdi, %r11
	cmpq	%rax, %rbx
	ja	.L7
.L74:
	movl	$1, %r10d
	movl	$1, %edx
	xorl	%esi, %esi
	movq	$-1, %rcx
.L8:
	leaq	(%rdx,%rsi), %rax
	cmpq	%rax, %rbx
	jbe	.L75
.L13:
	leaq	0(%rbp,%rcx), %r9
	movzbl	(%r9,%rdx), %r9d
	cmpb	%r9b, 0(%rbp,%rax)
	jbe	.L9
	movq	%rax, %rsi
	movl	$1, %edx
	movq	%rax, %r10
	leaq	(%rdx,%rsi), %rax
	subq	%rcx, %r10
	cmpq	%rax, %rbx
	ja	.L13
.L75:
	leaq	1(%rcx), %r12
	addq	$1, %rdi
	cmpq	%rdi, %r12
	jnb	.L14
	movq	%r11, %r10
	movq	%rdi, %r12
.L14:
	leaq	80(%rsp), %rax
	leaq	2128(%rsp), %rdx
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L15
	testq	%rbx, %rbx
	je	.L16
	leaq	-1(%rbx), %rax
	movq	%rbp, %rdx
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	(%rdx), %ecx
	addq	$1, %rdx
	movq	%rax, 80(%rsp,%rcx,8)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L17
.L16:
	leaq	0(%rbp,%r10), %rsi
	movq	%r12, %rdx
	movq	%rbp, %rdi
	movq	%r8, 16(%rsp)
	movq	%r10, 8(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	8(%rsp), %r10
	movq	16(%rsp), %r8
	jne	.L76
	leaq	512(%rbx), %rax
	leaq	-1(%rbx), %r15
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	movq	%rbx, %rcx
	movq	%rax, 40(%rsp)
	leaq	-1(%r12), %rax
	movq	%rax, 64(%rsp)
	addq	%rbp, %rax
	movq	%rax, 72(%rsp)
	movq	%rbx, %rax
	subq	%r10, %rax
	movq	%rax, 48(%rsp)
	movl	$1, %eax
	subq	%r12, %rax
	movq	%rax, 56(%rsp)
.L18:
	leaq	(%r14,%rcx), %rbx
	cmpq	%r8, %rbx
	jbe	.L30
.L77:
	movq	40(%rsp), %rsi
	leaq	0(%r13,%r8), %rdi
	movq	%rcx, 32(%rsp)
	movq	%r10, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 8(%rsp)
	call	__strnlen
	movq	8(%rsp), %r8
	movq	16(%rsp), %rdx
	movq	24(%rsp), %r10
	movq	32(%rsp), %rcx
	addq	%rax, %r8
	cmpq	%r8, %rbx
	ja	.L71
.L30:
	movzbl	-1(%r13,%rbx), %eax
	movq	80(%rsp,%rax,8), %rax
	testq	%rax, %rax
	je	.L20
.L78:
	cmpq	%r10, %rax
	jnb	.L69
	testq	%rdx, %rdx
	cmovne	48(%rsp), %rax
.L69:
	addq	%rax, %r14
	xorl	%edx, %edx
	leaq	(%r14,%rcx), %rbx
	cmpq	%r8, %rbx
	ja	.L77
	movzbl	-1(%r13,%rbx), %eax
	movq	80(%rsp,%rax,8), %rax
	testq	%rax, %rax
	jne	.L78
.L20:
	cmpq	%r12, %rdx
	movq	%r12, %rax
	cmovnb	%rdx, %rax
	leaq	(%r14,%rax), %rsi
	leaq	0(%rbp,%rax), %rdi
	addq	%r13, %rsi
	cmpq	%r15, %rax
	jnb	.L23
	movzbl	(%rdi), %edi
	cmpb	%dil, (%rsi)
	jne	.L24
	leaq	0(%r13,%r14), %rsi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L79:
	movzbl	(%rsi,%rax), %edi
	cmpb	%dil, 0(%rbp,%rax)
	jne	.L24
.L25:
	addq	$1, %rax
	cmpq	%r15, %rax
	jb	.L79
.L23:
	movq	64(%rsp), %rax
	leaq	(%r14,%rax), %rdi
	movq	%rax, %rsi
	addq	%r13, %rdi
	cmpq	%r12, %rdx
	jnb	.L43
	movq	72(%rsp), %rax
	movzbl	(%rdi), %ebx
	cmpb	%bl, (%rax)
	jne	.L43
	leaq	-1(%rdx), %r9
	subq	%r12, %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L80:
	movzbl	1(%rax,%rdi), %ebx
	cmpb	%bl, 0(%rbp,%rax)
	jne	.L26
	movq	%rax, %rsi
.L27:
	leaq	-1(%rsi), %rax
	cmpq	%rax, %r9
	jne	.L80
.L26:
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	ja	.L81
.L28:
	addq	%r10, %r14
	movq	48(%rsp), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	addq	56(%rsp), %r14
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L3:
	je	.L82
	movq	%rcx, %rdi
	movl	$1, %r11d
	addq	$1, %rcx
	movl	$1, %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	je	.L83
	movq	%rsi, %rcx
	movl	$1, %r10d
	addq	$1, %rsi
	movl	$1, %edx
	jmp	.L8
.L82:
	cmpq	%r11, %rdx
	je	.L6
	addq	$1, %rdx
	jmp	.L2
.L83:
	cmpq	%r10, %rdx
	je	.L12
	addq	$1, %rdx
	jmp	.L8
.L71:
	xorl	%eax, %eax
.L1:
	addq	$2136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L76:
	movq	%rbx, %rax
	leaq	-1(%rbx), %r14
	movq	%rbx, %rcx
	subq	%r12, %rax
	cmpq	%r12, %rax
	cmovb	%r12, %rax
	xorl	%r15d, %r15d
	addq	$1, %rax
	movq	%rax, 48(%rsp)
	leaq	512(%rbx), %rax
	movq	%rax, 24(%rsp)
	leaq	0(%rbp,%r12), %rax
	movq	%rax, 32(%rsp)
	movl	$1, %eax
	subq	%r12, %rax
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	(%r15,%rcx), %rbx
	cmpq	%r8, %rbx
	jbe	.L40
	movq	24(%rsp), %rsi
	leaq	0(%r13,%r8), %rdi
	movq	%rcx, 16(%rsp)
	movq	%r8, 8(%rsp)
	call	__strnlen
	movq	8(%rsp), %r8
	movq	16(%rsp), %rcx
	addq	%rax, %r8
	cmpq	%r8, %rbx
	ja	.L71
.L40:
	movzbl	-1(%r13,%rbx), %eax
	movq	80(%rsp,%rax,8), %rax
	testq	%rax, %rax
	jne	.L70
	leaq	(%r15,%r12), %rax
	addq	%r13, %rax
	cmpq	%r14, %r12
	jnb	.L34
	movq	32(%rsp), %rdi
	movzbl	(%rax), %eax
	cmpb	%al, (%rdi)
	movq	%r12, %rax
	jne	.L35
	leaq	0(%r13,%r15), %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	(%rdx,%rax), %esi
	cmpb	%sil, 0(%rbp,%rax)
	jne	.L35
.L36:
	addq	$1, %rax
	cmpq	%r14, %rax
	jb	.L84
.L34:
	leaq	-1(%r12), %rax
	leaq	(%r15,%rax), %rdx
	leaq	0(%rbp,%rax), %rsi
	addq	%r13, %rdx
	cmpq	$-1, %rax
	je	.L37
	movzbl	(%rdx), %edi
	cmpb	%dil, (%rsi)
	jne	.L38
	subq	%r12, %rdx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	1(%rax,%rdx), %edi
	cmpb	%dil, 0(%rbp,%rax)
	jne	.L38
.L39:
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L85
.L37:
	leaq	0(%r13,%r15), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	addq	40(%rsp), %r15
.L70:
	addq	%rax, %r15
	jmp	.L31
.L38:
	addq	48(%rsp), %r15
	jmp	.L31
.L6:
	movq	%rax, %rcx
	movl	$1, %edx
	jmp	.L2
.L12:
	movq	%rax, %rsi
	movl	$1, %edx
	jmp	.L8
.L43:
	movq	%r12, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jbe	.L28
.L81:
	leaq	0(%r13,%r14), %rax
	jmp	.L1
	.size	two_way_long_needle, .-two_way_long_needle
	.p2align 4,,15
	.globl	strstr
	.hidden	strstr
	.type	strstr, @function
strstr:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$280, %rsp
	movzbl	(%rsi), %r13d
	testb	%r13b, %r13b
	je	.L107
	movzbl	%r13b, %r12d
	movq	%rsi, %rbp
	movl	%r12d, %esi
	call	strchr
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L90
	movzbl	1(%rbp), %eax
	testb	%al, %al
	je	.L86
	movzbl	2(%rbp), %edx
	testb	%dl, %dl
	jne	.L89
	movzbl	(%rbx), %edx
	sall	$16, %r12d
	orl	%eax, %r12d
	testl	%edx, %edx
	je	.L90
	xorl	%eax, %eax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L139:
	testl	%edx, %edx
	je	.L110
	movq	%rcx, %rbx
.L91:
	sall	$16, %eax
	leaq	1(%rbx), %rcx
	orl	%edx, %eax
	movzbl	1(%rbx), %edx
	cmpl	%eax, %r12d
	jne	.L139
.L110:
	cmpl	%eax, %r12d
	jne	.L90
	subq	$1, %rbx
.L86:
	addq	$280, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rdi, %rbx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L143:
	movq	8(%rsp), %rax
	movl	$2048, %esi
	leaq	(%r12,%rax), %rdi
	call	__strnlen
	addq	%rax, %r12
	cmpq	%r12, %rbx
	jbe	.L101
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%ebx, %ebx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L89:
	cmpb	$0, 3(%rbp)
	je	.L140
	movq	%rbp, %rdi
	call	strlen
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%rbx, %rdi
	orq	$512, %rsi
	movq	%rax, 8(%rsp)
	call	__strnlen
	cmpq	%rax, %r15
	movq	%rax, %r14
	ja	.L90
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L86
	cmpq	$256, %r15
	ja	.L141
	movq	8(%rsp), %rsi
	leaq	16(%rsp), %rdx
	xorl	%eax, %eax
	movl	$32, %ecx
	movq	%rdx, %rdi
	leaq	-1(%rsi), %r15
	subq	%rsi, %r14
	leaq	(%rbx,%r14), %r12
	cmpq	$1, %r15
	rep stosq
	jbe	.L97
	subq	$2, %rsi
	movl	$1, %eax
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$1, %rax
	movl	%ecx, %r13d
.L98:
	movzbl	0(%rbp,%rax), %ecx
	sall	$3, %r13d
	movl	%ecx, %edx
	subl	%r13d, %edx
	cmpq	%rsi, %rax
	movzbl	%dl, %edx
	movb	%al, 16(%rsp,%rdx)
	jne	.L142
.L97:
	movq	8(%rsp), %rcx
	movq	%r15, %r13
	xorl	%r14d, %r14d
	movzbl	-2(%rbp,%rcx), %edx
	movzbl	-1(%rbp,%rcx), %eax
	salq	$3, %rdx
	subq	%rdx, %rax
	movzbl	%al, %eax
	movzbl	16(%rsp,%rax), %edx
	movb	%r15b, 16(%rsp,%rax)
	leaq	-9(%rcx), %rax
	movq	%rax, (%rsp)
	subq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	%r12, %rbx
	jbe	.L101
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	cmpq	%r12, %rbx
	ja	.L112
.L101:
	addq	%r15, %rbx
	movzbl	-1(%rbx), %eax
	movzbl	(%rbx), %edx
	salq	$3, %rax
	subq	%rax, %rdx
	movzbl	%dl, %edx
	movzbl	16(%rsp,%rdx), %eax
	testq	%rax, %rax
	je	.L144
.L112:
	subq	%rax, %rbx
	cmpq	%rax, %r15
	ja	.L99
	cmpq	$14, %r15
	jbe	.L104
	movq	(%rbx,%r14), %rax
	cmpq	%rax, 0(%rbp,%r14)
	jne	.L105
.L104:
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	je	.L86
	leaq	-8(%r14), %rax
	cmpq	$8, %r14
	cmovb	(%rsp), %rax
	movq	%rax, %r14
.L105:
	addq	%r13, %rbx
	jmp	.L99
.L140:
	sall	$8, %edx
	sall	$16, %eax
	sall	$24, %r12d
	orl	%edx, %eax
	movzbl	(%rbx), %edx
	orl	%eax, %r12d
	testl	%edx, %edx
	je	.L90
	xorl	%eax, %eax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L145:
	testl	%edx, %edx
	je	.L111
	movq	%rcx, %rbx
.L94:
	orl	%edx, %eax
	leaq	1(%rbx), %rcx
	movzbl	1(%rbx), %edx
	sall	$8, %eax
	cmpl	%eax, %r12d
	jne	.L145
.L111:
	cmpl	%eax, %r12d
	jne	.L90
	subq	$2, %rbx
	jmp	.L86
.L141:
	movq	8(%rsp), %rcx
	addq	$280, %rsp
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	two_way_long_needle
	.size	strstr, .-strstr
	.hidden	strlen
	.hidden	strchr
	.hidden	__strnlen
