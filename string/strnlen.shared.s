.text
.globl __strnlen
.type __strnlen,@function
.align 1<<4
__strnlen:
 test %rsi, %rsi
 jne .Ln_nonzero
 xor %rax, %rax
 ret
.Ln_nonzero:
 add %rdi, %rsi
 mov %rsi, %r10
 and $-64, %r10
 mov %rsi, %r11
 pxor %xmm0, %xmm0
 pxor %xmm1, %xmm1
 pxor %xmm2, %xmm2
 pxor %xmm3, %xmm3
 movq %rdi, %rax
 movq %rdi, %rcx
 andq $4095, %rcx
 cmpq $4047, %rcx
 ja .Lcross_page
 andq $-16, %rax
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 movq %rdi, %rcx
 xorq %rax, %rcx
 mov %r11, %rsi
 subq %rax, %rsi
 andq $-64, %rax
 testq $-64, %rsi
 je .Lstrnlen_ret
 sarq %cl, %rdx
 test %rdx, %rdx
 je .Lloop
 bsfq %rdx, %rax
 
 ret
 .p2align 4
.Lcross_page:
 andq $-64, %rax
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 movq %rdi, %rcx
 xorq %rax, %rcx
 mov %r11, %rsi
 subq %rax, %rsi
 andq $-64, %rax
 testq $-64, %rsi
 je .Lstrnlen_ret
 sarq %cl, %rdx
 test %rdx, %rdx
 je .Lloop_init
 bsfq %rdx, %rax
 
 ret
.Lstrnlen_ret:
 bts %rsi, %rdx
 sarq %cl, %rdx
 test %rdx, %rdx
 je .Lloop_init
 bsfq %rdx, %rax
 ret
 .p2align 4
.Lloop_init:
 pxor %xmm1, %xmm1
 pxor %xmm2, %xmm2
 pxor %xmm3, %xmm3
 .p2align 4
.Lloop:
 addq $64, %rax
 cmpq %rax, %r10
 je .Lexit_end
 movdqa (%rax), %xmm0
 pminub 16(%rax), %xmm0
 pminub 32(%rax), %xmm0
 pminub 48(%rax), %xmm0
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 testl %edx, %edx
 jne .Lexit
 jmp .Lloop
 .p2align 4
.Lexit_end:
 cmp %rax, %r11
 je .Lfirst
 pxor %xmm0, %xmm0
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
.Lfirst:
 bts %r11, %rdx
 bsfq %rdx, %rdx
 addq %rdx, %rax
 subq %rdi, %rax
 ret
 .p2align 4
.Lexit:
 pxor %xmm0, %xmm0
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 bsfq %rdx, %rdx
 addq %rdx, %rax
 subq %rdi, %rax
 ret
.size __strnlen,.-__strnlen
.globl __GI___strnlen
.set __GI___strnlen,__strnlen
.weak strnlen
strnlen = __strnlen
.globl __GI_strnlen
.set __GI_strnlen,strnlen
