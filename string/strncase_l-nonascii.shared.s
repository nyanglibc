	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__strncasecmp_l_nonascii
	.type	__strncasecmp_l_nonascii, @function
__strncasecmp_l_nonascii:
	cmpq	%rsi, %rdi
	je	.L5
	testq	%rdx, %rdx
	je	.L5
	movq	112(%rcx), %r8
	xorl	%ecx, %ecx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	testb	%r10b, %r10b
	je	.L1
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	je	.L1
.L3:
	movzbl	(%rdi,%rcx), %eax
	movzbl	(%rsi,%rcx), %r9d
	movq	%rax, %r10
	movl	(%r8,%rax,4), %eax
	subl	(%r8,%r9,4), %eax
	je	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__strncasecmp_l_nonascii, .-__strncasecmp_l_nonascii
