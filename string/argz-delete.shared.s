	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_argz_delete
	.hidden	__GI_argz_delete
	.type	__GI_argz_delete, @function
__GI_argz_delete:
	testq	%rdx, %rdx
	je	.L6
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	__GI_strlen
	movq	(%r12), %rdx
	movq	%rbx, %rcx
	subq	0(%rbp), %rcx
	addq	$1, %rax
	movq	%rbx, %rdi
	leaq	(%rbx,%rax), %rsi
	subq	%rax, %rdx
	movq	%rdx, (%r12)
	subq	%rcx, %rdx
	call	__GI_memmove
	cmpq	$0, (%r12)
	je	.L10
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	0(%rbp), %rdi
	call	free@PLT
	movq	$0, 0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	rep ret
	.size	__GI_argz_delete, .-__GI_argz_delete
	.globl	argz_delete
	.set	argz_delete,__GI_argz_delete
