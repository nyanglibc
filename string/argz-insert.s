	.text
	.p2align 4,,15
	.globl	__argz_insert
	.hidden	__argz_insert
	.type	__argz_insert, @function
__argz_insert:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	testq	%rdx, %rdx
	je	.L14
	movq	(%rdi), %rbp
	movq	%rdx, %rbx
	movl	$22, %eax
	cmpq	%rdx, %rbp
	ja	.L1
	movq	(%rsi), %r15
	leaq	0(%rbp,%r15), %rdx
	cmpq	%rdx, %rbx
	jnb	.L1
	cmpq	%rbx, %rbp
	jb	.L12
.L4:
	movq	%rcx, %rdi
	movq	%rcx, 8(%rsp)
	call	strlen
	leaq	1(%rax), %r13
	movq	%rbp, %rdi
	leaq	(%r15,%r13), %rax
	movq	%rax, %rsi
	movq	%rax, (%rsp)
	call	realloc@PLT
	movq	%rax, %r10
	movl	$12, %eax
	testq	%r10, %r10
	je	.L1
	movq	%rbx, %r9
	subq	(%r12), %r9
	subq	%rbp, %rbx
	movq	%r15, %rdx
	movq	%r10, 24(%rsp)
	subq	%rbx, %rdx
	addq	%r10, %r9
	leaq	(%r9,%r13), %rdi
	movq	%r9, %rsi
	movq	%r9, 16(%rsp)
	call	memmove
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r9
	movq	%r13, %rdx
	movq	%rcx, %rsi
	movq	%r9, %rdi
	call	memmove
	movq	(%rsp), %rax
	movq	24(%rsp), %r10
	movq	%r10, (%r12)
	movq	%rax, (%r14)
	xorl	%eax, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	subq	$1, %rbx
.L12:
	cmpb	$0, -1(%rbx)
	jne	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$40, %rsp
	movq	%rcx, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__argz_add
	.size	__argz_insert, .-__argz_insert
	.weak	argz_insert
	.set	argz_insert,__argz_insert
	.hidden	__argz_add
	.hidden	memmove
	.hidden	strlen
