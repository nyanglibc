 .text
.globl __rawmemchr
.type __rawmemchr,@function
.align 1<<4
__rawmemchr:
 movd %rsi, %xmm1
 mov %rdi, %rcx
 punpcklbw %xmm1, %xmm1
 punpcklbw %xmm1, %xmm1
 and $63, %rcx
 pshufd $0, %xmm1, %xmm1
 cmp $48, %rcx
 ja .Lcrosscache
 movdqu (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 add $16, %rdi
 and $-16, %rdi
 jmp .Lloop_prolog
 .p2align 4
.Lcrosscache:
 and $15, %rcx
 and $-16, %rdi
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 sar %cl, %eax
 test %eax, %eax
 je .Lunaligned_no_match
 bsf %eax, %eax
 add %rdi, %rax
 add %rcx, %rax
 ret
 .p2align 4
.Lunaligned_no_match:
 add $16, %rdi
 .p2align 4
.Lloop_prolog:
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 48(%rdi), %xmm4
 pcmpeqb %xmm1, %xmm4
 add $64, %rdi
 pmovmskb %xmm4, %eax
 test %eax, %eax
 jnz .Lmatches0
 test $0x3f, %rdi
 jz .Lalign64_loop
 movdqa (%rdi), %xmm0
 pcmpeqb %xmm1, %xmm0
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 movdqa 16(%rdi), %xmm2
 pcmpeqb %xmm1, %xmm2
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 movdqa 48(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pmovmskb %xmm3, %eax
 add $64, %rdi
 test %eax, %eax
 jnz .Lmatches0
 and $-64, %rdi
 .p2align 4
.Lalign64_loop:
 movdqa (%rdi), %xmm0
 movdqa 16(%rdi), %xmm2
 movdqa 32(%rdi), %xmm3
 movdqa 48(%rdi), %xmm4
 pcmpeqb %xmm1, %xmm0
 pcmpeqb %xmm1, %xmm2
 pcmpeqb %xmm1, %xmm3
 pcmpeqb %xmm1, %xmm4
 pmaxub %xmm0, %xmm3
 pmaxub %xmm2, %xmm4
 pmaxub %xmm3, %xmm4
 pmovmskb %xmm4, %eax
 add $64, %rdi
 test %eax, %eax
 jz .Lalign64_loop
 sub $64, %rdi
 pmovmskb %xmm0, %eax
 test %eax, %eax
 jnz .Lmatches
 pmovmskb %xmm2, %eax
 test %eax, %eax
 jnz .Lmatches16
 movdqa 32(%rdi), %xmm3
 pcmpeqb %xmm1, %xmm3
 pcmpeqb 48(%rdi), %xmm1
 pmovmskb %xmm3, %eax
 test %eax, %eax
 jnz .Lmatches32
 pmovmskb %xmm1, %eax
 bsf %eax, %eax
 lea 48(%rdi, %rax), %rax
 ret
 .p2align 4
.Lmatches0:
 bsf %eax, %eax
 lea -16(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches:
 bsf %eax, %eax
 add %rdi, %rax
 ret
 .p2align 4
.Lmatches16:
 bsf %eax, %eax
 lea 16(%rax, %rdi), %rax
 ret
 .p2align 4
.Lmatches32:
 bsf %eax, %eax
 lea 32(%rax, %rdi), %rax
 ret
.size __rawmemchr,.-__rawmemchr
.weak rawmemchr
rawmemchr = __rawmemchr
.globl __GI___rawmemchr
.set __GI___rawmemchr,__rawmemchr
