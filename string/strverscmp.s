	.text
	.p2align 4,,15
	.globl	__strverscmp
	.hidden	__strverscmp
	.type	__strverscmp, @function
__strverscmp:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L1
	movzbl	(%rdi), %eax
	movzbl	(%rsi), %r9d
	leaq	1(%rdi), %r8
	leaq	1(%rsi), %rdi
	leal	-48(%rax), %edx
	movl	%eax, %ecx
	cmpl	$9, %edx
	setbe	%dl
	xorl	%esi, %esi
	cmpb	$48, %al
	sete	%sil
	movzbl	%dl, %edx
	addl	%esi, %edx
	movzbl	%r9b, %esi
	subl	%esi, %eax
	jne	.L3
	testb	%cl, %cl
	je	.L1
	leaq	next_state.8057(%rip), %r11
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	testb	%cl, %cl
	je	.L1
.L4:
	addq	$1, %r8
	movzbl	-1(%r8), %eax
	movslq	%edx, %rdx
	movzbl	(%r11,%rdx), %esi
	addq	$1, %rdi
	movzbl	-1(%rdi), %r9d
	leal	-48(%rax), %edx
	movl	%eax, %ecx
	cmpl	$9, %edx
	setbe	%dl
	xorl	%r10d, %r10d
	cmpb	$48, %al
	sete	%r10b
	movzbl	%dl, %edx
	addl	%r10d, %edx
	addl	%esi, %edx
	movzbl	%r9b, %esi
	subl	%esi, %eax
	je	.L5
.L3:
	subl	$48, %esi
	xorl	%ecx, %ecx
	leal	(%rdx,%rdx,2), %edx
	cmpl	$9, %esi
	setbe	%cl
	xorl	%esi, %esi
	cmpb	$48, %r9b
	sete	%sil
	addl	%esi, %ecx
	addl	%ecx, %edx
	leaq	result_type.8058(%rip), %rcx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	cmpb	$2, %dl
	je	.L1
	cmpb	$3, %dl
	je	.L7
	movsbl	%dl, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rdi
	cmpl	$9, %edx
	ja	.L10
.L7:
	addq	$1, %r8
	movzbl	-1(%r8), %ecx
	movzbl	(%rdi), %edx
	subl	$48, %ecx
	subl	$48, %edx
	cmpl	$9, %ecx
	jbe	.L8
	cmpl	$9, %edx
	movl	$-1, %edx
	cmovbe	%edx, %eax
	ret
	.size	__strverscmp, .-__strverscmp
	.weak	strverscmp
	.set	strverscmp,__strverscmp
	.section	.rodata
	.align 32
	.type	result_type.8058, @object
	.size	result_type.8058, 36
result_type.8058:
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	-1
	.byte	-1
	.byte	1
	.byte	3
	.byte	3
	.byte	1
	.byte	3
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	1
	.byte	-1
	.byte	2
	.byte	2
	.byte	-1
	.byte	2
	.byte	2
	.align 8
	.type	next_state.8057, @object
	.size	next_state.8057, 12
next_state.8057:
	.byte	0
	.byte	3
	.byte	9
	.byte	0
	.byte	3
	.byte	3
	.byte	0
	.byte	6
	.byte	6
	.byte	0
	.byte	6
	.byte	9
