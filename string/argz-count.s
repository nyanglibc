	.text
	.p2align 4,,15
	.globl	__argz_count
	.hidden	__argz_count
	.type	__argz_count, @function
__argz_count:
	testq	%rsi, %rsi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L4
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rbp, %rdi
	addq	$1, %r12
	call	strlen
	leaq	1(%rbp,%rax), %rbp
	notq	%rax
	addq	%rax, %rbx
	jne	.L3
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%r12d, %r12d
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__argz_count, .-__argz_count
	.weak	argz_count
	.set	argz_count,__argz_count
	.hidden	strlen
