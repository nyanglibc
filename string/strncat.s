	.text
	.p2align 4,,15
	.globl	strncat
	.type	strncat, @function
strncat:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	call	strlen
	leaq	0(%rbp,%rax), %rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__strnlen
	movq	%r12, %rsi
	movb	$0, (%rbx,%rax)
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	memcpy@PLT
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	strncat, .-strncat
	.globl	__strncat
	.hidden	__strncat
	.set	__strncat,strncat
	.hidden	__strnlen
	.hidden	strlen
