	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_strcoll
	.hidden	__GI_strcoll
	.type	__GI_strcoll, @function
__GI_strcoll:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
	jmp	__GI___strcoll_l
	.size	__GI_strcoll, .-__GI_strcoll
	.globl	strcoll
	.set	strcoll,__GI_strcoll
