 .text
.globl strcat
.type strcat,@function
.align 1<<4
strcat:
 movq %rdi, %rcx
 andl $7, %ecx
 movq %rdi, %rax
 movq $0xfefefefefefefeff,%r8
 jz 4f
 neg %ecx
 addl $8,%ecx
0: cmpb $0x0,(%rax)
 je 2f
 incq %rax
 decl %ecx
 jnz 0b
 .p2align 4
4:
 movq (%rax), %rcx
 addq $8,%rax
 movq %r8, %rdx
 addq %rcx, %rdx
 jnc 3f
 xorq %rcx, %rdx
 orq %r8, %rdx
 incq %rdx
 jnz 3f
 movq (%rax), %rcx
 addq $8,%rax
 movq %r8, %rdx
 addq %rcx, %rdx
 jnc 3f
 xorq %rcx, %rdx
 orq %r8, %rdx
 incq %rdx
 jnz 3f
 movq (%rax), %rcx
 addq $8,%rax
 movq %r8, %rdx
 addq %rcx, %rdx
 jnc 3f
 xorq %rcx, %rdx
 orq %r8, %rdx
 incq %rdx
 jnz 3f
 movq (%rax), %rcx
 addq $8,%rax
 movq %r8, %rdx
 addq %rcx, %rdx
 jnc 3f
 xorq %rcx, %rdx
 orq %r8, %rdx
 incq %rdx
 jz 4b
 .p2align 4
3: subq $8,%rax
 testb %cl, %cl
 jz 2f
 incq %rax
 testb %ch, %ch
 jz 2f
 incq %rax
 testl $0x00ff0000, %ecx
 jz 2f
 incq %rax
 testl $0xff000000, %ecx
 jz 2f
 incq %rax
 shrq $32, %rcx
 testb %cl, %cl
 jz 2f
 incq %rax
 testb %ch, %ch
 jz 2f
 incq %rax
 testl $0xff0000, %ecx
 jz 2f
 incq %rax
2:
 movq %rsi, %rcx
 andl $7,%ecx
 movq %rax, %rdx
 jz 22f
 neg %ecx
 addl $8, %ecx
21:
 movb (%rsi), %al
 testb %al, %al
 movb %al, (%rdx)
 jz 24f
 incq %rsi
 incq %rdx
 decl %ecx
 jnz 21b
 .p2align 4
22:
 movq (%rsi), %rax
 addq $8, %rsi
 movq %rax, %r9
 addq %r8, %r9
 jnc 23f
 xorq %rax, %r9
 orq %r8, %r9
 incq %r9
 jnz 23f
 movq %rax, (%rdx)
 addq $8, %rdx
 movq (%rsi), %rax
 addq $8, %rsi
 movq %rax, %r9
 addq %r8, %r9
 jnc 23f
 xorq %rax, %r9
 orq %r8, %r9
 incq %r9
 jnz 23f
 movq %rax, (%rdx)
 addq $8, %rdx
 movq (%rsi), %rax
 addq $8, %rsi
 movq %rax, %r9
 addq %r8, %r9
 jnc 23f
 xorq %rax, %r9
 orq %r8, %r9
 incq %r9
 jnz 23f
 movq %rax, (%rdx)
 addq $8, %rdx
 movq (%rsi), %rax
 addq $8, %rsi
 movq %rax, %r9
 addq %r8, %r9
 jnc 23f
 xorq %rax, %r9
 orq %r8, %r9
 incq %r9
 jnz 23f
 movq %rax, (%rdx)
 addq $8, %rdx
 jmp 22b
 .p2align 4
23:
 movb %al, (%rdx)
 testb %al, %al
 jz 24f
 incq %rdx
 movb %ah, (%rdx)
 testb %ah, %ah
 jz 24f
 incq %rdx
 shrq $16, %rax
 jmp 23b
24:
 movq %rdi, %rax
 retq
.size strcat,.-strcat

