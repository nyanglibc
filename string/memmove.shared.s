 .section .text,"ax",@progbits
.globl __mempcpy_chk
.type __mempcpy_chk,@function
.align 1<<4
__mempcpy_chk:
 cmp %rdx, %rcx
 jb __GI___chk_fail
.size __mempcpy_chk,.-__mempcpy_chk
.globl __mempcpy
.type __mempcpy,@function
.align 1<<4
__mempcpy:
 mov %rdi, %rax
 add %rdx, %rax
 jmp .Lstart
.size __mempcpy,.-__mempcpy
.globl __memmove_chk
.type __memmove_chk,@function
.align 1<<4
__memmove_chk:
 cmp %rdx, %rcx
 jb __GI___chk_fail
.size __memmove_chk,.-__memmove_chk
.globl memmove
.type memmove,@function
.align 1<<4
memmove:
 movq %rdi, %rax
.Lstart:
 cmp $16, %rdx
 jb .Lless_vec
 cmp $(16 * 2), %rdx
 ja .Lmore_2x_vec
.Llast_2x_vec:
 movups (%rsi), %xmm0
 movups -16(%rsi,%rdx), %xmm1
 movups %xmm0, (%rdi)
 movups %xmm1, -16(%rdi,%rdx)

.Lnop:
 ret
.Lless_vec:
 cmpb $8, %dl
 jae .Lbetween_8_15
 cmpb $4, %dl
 jae .Lbetween_4_7
 cmpb $1, %dl
 ja .Lbetween_2_3
 jb 1f
 movzbl (%rsi), %ecx
 movb %cl, (%rdi)
1:
 ret
.Lbetween_8_15:
 movq -8(%rsi,%rdx), %rcx
 movq (%rsi), %rsi
 movq %rcx, -8(%rdi,%rdx)
 movq %rsi, (%rdi)
 ret
.Lbetween_4_7:
 movl -4(%rsi,%rdx), %ecx
 movl (%rsi), %esi
 movl %ecx, -4(%rdi,%rdx)
 movl %esi, (%rdi)
 ret
.Lbetween_2_3:
 movzwl -2(%rsi,%rdx), %ecx
 movzwl (%rsi), %esi
 movw %cx, -2(%rdi,%rdx)
 movw %si, (%rdi)
 ret
.Lmore_2x_vec:
 cmpq $(16 * 8), %rdx
 ja .Lmore_8x_vec
 cmpq $(16 * 4), %rdx
 jb .Llast_4x_vec
 movups (%rsi), %xmm0
 movups 16(%rsi), %xmm1
 movups (16 * 2)(%rsi), %xmm2
 movups (16 * 3)(%rsi), %xmm3
 movups -16(%rsi,%rdx), %xmm4
 movups -(16 * 2)(%rsi,%rdx), %xmm5
 movups -(16 * 3)(%rsi,%rdx), %xmm6
 movups -(16 * 4)(%rsi,%rdx), %xmm7
 movups %xmm0, (%rdi)
 movups %xmm1, 16(%rdi)
 movups %xmm2, (16 * 2)(%rdi)
 movups %xmm3, (16 * 3)(%rdi)
 movups %xmm4, -16(%rdi,%rdx)
 movups %xmm5, -(16 * 2)(%rdi,%rdx)
 movups %xmm6, -(16 * 3)(%rdi,%rdx)
 movups %xmm7, -(16 * 4)(%rdi,%rdx)

 ret
.Llast_4x_vec:
 movups (%rsi), %xmm0
 movups 16(%rsi), %xmm1
 movups -16(%rsi,%rdx), %xmm2
 movups -(16 * 2)(%rsi,%rdx), %xmm3
 movups %xmm0, (%rdi)
 movups %xmm1, 16(%rdi)
 movups %xmm2, -16(%rdi,%rdx)
 movups %xmm3, -(16 * 2)(%rdi,%rdx)

 ret
.Lmore_8x_vec:
 cmpq %rsi, %rdi
 ja .Lmore_8x_vec_backward
 je .Lnop
 movups (%rsi), %xmm4
 movups -16(%rsi, %rdx), %xmm5
 movups -(16 * 2)(%rsi, %rdx), %xmm6
 movups -(16 * 3)(%rsi, %rdx), %xmm7
 movups -(16 * 4)(%rsi, %rdx), %xmm8
 movq %rdi, %r11
 leaq -16(%rdi, %rdx), %rcx
 movq %rdi, %r8
 andq $(16 - 1), %r8
 subq $16, %r8
 subq %r8, %rsi
 subq %r8, %rdi
 addq %r8, %rdx
 cmp __x86_shared_non_temporal_threshold(%rip), %rdx
 ja .Llarge_forward
.Lloop_4x_vec_forward:
 movups (%rsi), %xmm0
 movups 16(%rsi), %xmm1
 movups (16 * 2)(%rsi), %xmm2
 movups (16 * 3)(%rsi), %xmm3
 addq $(16 * 4), %rsi
 subq $(16 * 4), %rdx
 movaps %xmm0, (%rdi)
 movaps %xmm1, 16(%rdi)
 movaps %xmm2, (16 * 2)(%rdi)
 movaps %xmm3, (16 * 3)(%rdi)
 addq $(16 * 4), %rdi
 cmpq $(16 * 4), %rdx
 ja .Lloop_4x_vec_forward
 movups %xmm5, (%rcx)
 movups %xmm6, -16(%rcx)
 movups %xmm7, -(16 * 2)(%rcx)
 movups %xmm8, -(16 * 3)(%rcx)
 movups %xmm4, (%r11)

 ret
.Lmore_8x_vec_backward:
 movups (%rsi), %xmm4
 movups 16(%rsi), %xmm5
 movups (16 * 2)(%rsi), %xmm6
 movups (16 * 3)(%rsi), %xmm7
 movups -16(%rsi,%rdx), %xmm8
 leaq -16(%rdi, %rdx), %r11
 leaq -16(%rsi, %rdx), %rcx
 movq %r11, %r9
 movq %r11, %r8
 andq $(16 - 1), %r8
 subq %r8, %rcx
 subq %r8, %r9
 subq %r8, %rdx
 cmp __x86_shared_non_temporal_threshold(%rip), %rdx
 ja .Llarge_backward
.Lloop_4x_vec_backward:
 movups (%rcx), %xmm0
 movups -16(%rcx), %xmm1
 movups -(16 * 2)(%rcx), %xmm2
 movups -(16 * 3)(%rcx), %xmm3
 subq $(16 * 4), %rcx
 subq $(16 * 4), %rdx
 movaps %xmm0, (%r9)
 movaps %xmm1, -16(%r9)
 movaps %xmm2, -(16 * 2)(%r9)
 movaps %xmm3, -(16 * 3)(%r9)
 subq $(16 * 4), %r9
 cmpq $(16 * 4), %rdx
 ja .Lloop_4x_vec_backward
 movups %xmm4, (%rdi)
 movups %xmm5, 16(%rdi)
 movups %xmm6, (16 * 2)(%rdi)
 movups %xmm7, (16 * 3)(%rdi)
 movups %xmm8, (%r11)

 ret
.Llarge_forward:
 leaq (%rdi, %rdx), %r10
 cmpq %r10, %rsi
 jb .Lloop_4x_vec_forward
.Lloop_large_forward:
 prefetcht0 ((16 * 4) * 2)(%rsi)
 prefetcht0 ((16 * 4) * 3)(%rsi)
 movups (%rsi), %xmm0
 movups 16(%rsi), %xmm1
 movups (16 * 2)(%rsi), %xmm2
 movups (16 * 3)(%rsi), %xmm3
 addq $(16 * 4), %rsi
 subq $(16 * 4), %rdx
 movntdq %xmm0, (%rdi)
 movntdq %xmm1, 16(%rdi)
 movntdq %xmm2, (16 * 2)(%rdi)
 movntdq %xmm3, (16 * 3)(%rdi)
 addq $(16 * 4), %rdi
 cmpq $(16 * 4), %rdx
 ja .Lloop_large_forward
 sfence
 movups %xmm5, (%rcx)
 movups %xmm6, -16(%rcx)
 movups %xmm7, -(16 * 2)(%rcx)
 movups %xmm8, -(16 * 3)(%rcx)
 movups %xmm4, (%r11)

 ret
.Llarge_backward:
 leaq (%rcx, %rdx), %r10
 cmpq %r10, %r9
 jb .Lloop_4x_vec_backward
.Lloop_large_backward:
 prefetcht0 (-(16 * 4) * 2)(%rcx)
 prefetcht0 (-(16 * 4) * 3)(%rcx)
 movups (%rcx), %xmm0
 movups -16(%rcx), %xmm1
 movups -(16 * 2)(%rcx), %xmm2
 movups -(16 * 3)(%rcx), %xmm3
 subq $(16 * 4), %rcx
 subq $(16 * 4), %rdx
 movntdq %xmm0, (%r9)
 movntdq %xmm1, -16(%r9)
 movntdq %xmm2, -(16 * 2)(%r9)
 movntdq %xmm3, -(16 * 3)(%r9)
 subq $(16 * 4), %r9
 cmpq $(16 * 4), %rdx
 ja .Lloop_large_backward
 sfence
 movups %xmm4, (%rdi)
 movups %xmm5, 16(%rdi)
 movups %xmm6, (16 * 2)(%rdi)
 movups %xmm7, (16 * 3)(%rdi)
 movups %xmm8, (%r11)

 ret
.size memmove,.-memmove
.globl __memcpy_chk
.set __memcpy_chk,__memmove_chk
.globl __memcpy
.set __memcpy,memmove
.globl __GI_memmove
.set __GI_memmove,memmove
.globl __memcpy
.set __memcpy,memmove
.globl __GI_memcpy
.set __GI_memcpy,memmove
.globl __GI___mempcpy
.set __GI___mempcpy,__mempcpy
.weak mempcpy
mempcpy = __mempcpy
.globl __GI_mempcpy
.set __GI_mempcpy,mempcpy
.symver __memcpy, memcpy@@GLIBC_2.14
.symver memmove, memcpy@GLIBC_2.2.5
