	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Real-time signal %d"
.LC1:
	.string	"Unknown signal %d"
	.text
	.p2align 4,,15
	.globl	strsignal
	.type	strsignal, @function
strsignal:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	call	__sigdescr_np
	testq	%rax, %rax
	je	.L2
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rsi
	jmp	__dcgettext
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%fs:16, %rbp
	movq	2424(%rbp), %rdi
	leaq	2424(%rbp), %r12
	call	free@PLT
	call	__libc_current_sigrtmin
	cmpl	%ebx, %eax
	jle	.L10
.L3:
	leaq	.LC1(%rip), %rsi
	movl	$5, %edx
.L8:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext
	movl	%ebx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__asprintf
	cmpl	$-1, %eax
	je	.L5
	movq	2424(%rbp), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	call	__libc_current_sigrtmax
	cmpl	%ebx, %eax
	jl	.L3
	call	__libc_current_sigrtmin
	movl	$5, %edx
	subl	%eax, %ebx
	leaq	.LC0(%rip), %rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	movq	$0, 2424(%rbp)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	strsignal, .-strsignal
	.hidden	__libc_current_sigrtmax
	.hidden	__asprintf
	.hidden	__libc_current_sigrtmin
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__sigdescr_np
