	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	strerrorname_np
	.type	strerrorname_np, @function
strerrorname_np:
	jmp	__get_errname
	.size	strerrorname_np, .-strerrorname_np
	.hidden	__get_errname
