	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __old_strtok_r_1c,__strtok_r_1c@GLIBC_2.2.5
	.symver __old_strsep_1c,__strsep_1c@GLIBC_2.2.5
	.symver __old_strsep_2c,__strsep_2c@GLIBC_2.2.5
	.symver __old_strsep_3c,__strsep_3c@GLIBC_2.2.5
	.symver __old_strcspn_c1,__strcspn_c1@GLIBC_2.2.5
	.symver __old_strcspn_c2,__strcspn_c2@GLIBC_2.2.5
	.symver __old_strcspn_c3,__strcspn_c3@GLIBC_2.2.5
	.symver __old_strspn_c1,__strspn_c1@GLIBC_2.2.5
	.symver __old_strspn_c2,__strspn_c2@GLIBC_2.2.5
	.symver __old_strspn_c3,__strspn_c3@GLIBC_2.2.5
	.symver __old_strpbrk_c2,__strpbrk_c2@GLIBC_2.2.5
	.symver __old_strpbrk_c3,__strpbrk_c3@GLIBC_2.2.5
	.symver __old_mempcpy_small,__mempcpy_small@GLIBC_2.2.5
	.symver __old_strcpy_small,__strcpy_small@GLIBC_2.2.5
	.symver __old_stpcpy_small,__stpcpy_small@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__old_strtok_r_1c
	.type	__old_strtok_r_1c, @function
__old_strtok_r_1c:
	movq	%rdi, %rax
	movl	%esi, %edi
	testq	%rax, %rax
	je	.L12
.L2:
	movzbl	(%rax), %ecx
	cmpb	%sil, %cl
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rax
	movzbl	(%rax), %ecx
	cmpb	%dil, %cl
	je	.L4
.L3:
	testb	%cl, %cl
	je	.L8
	leaq	1(%rax), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	%dil, %sil
	leaq	1(%rcx), %r8
	je	.L13
	movq	%r8, %rcx
.L6:
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	jne	.L7
.L5:
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movb	$0, (%rcx)
	movq	%r8, %rcx
	movq	%rcx, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rdx), %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rax, %rcx
	xorl	%eax, %eax
	jmp	.L5
	.size	__old_strtok_r_1c, .-__old_strtok_r_1c
	.p2align 4,,15
	.globl	__old_strsep_1c
	.type	__old_strsep_1c, @function
__old_strsep_1c:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	je	.L14
	movq	%rdi, %rbx
	movsbl	%sil, %esi
	movq	%rbp, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L20
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movb	$0, (%rax)
.L14:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	$0, (%rbx)
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__old_strsep_1c, .-__old_strsep_1c
	.p2align 4,,15
	.globl	__old_strsep_2c
	.type	__old_strsep_2c, @function
__old_strsep_2c:
	movq	(%rdi), %rax
	movl	%esi, %r9d
	movl	%edx, %r8d
	testq	%rax, %rax
	je	.L21
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L28
	cmpb	%sil, %cl
	je	.L29
	cmpb	%dl, %cl
	je	.L29
	movq	%rax, %rcx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	%r8b, %dl
	je	.L24
	cmpb	%r9b, %dl
	je	.L24
.L25:
	addq	$1, %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L27
.L28:
	xorl	%edx, %edx
.L23:
	movq	%rdx, (%rdi)
.L21:
	rep ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	1(%rcx), %rdx
	movb	$0, (%rcx)
	jmp	.L23
	.size	__old_strsep_2c, .-__old_strsep_2c
	.p2align 4,,15
	.globl	__old_strsep_3c
	.type	__old_strsep_3c, @function
__old_strsep_3c:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L50
	movzbl	(%rax), %r8d
	testb	%r8b, %r8b
	je	.L42
	cmpb	%r8b, %dl
	pushq	%rbx
	movl	%edx, %r9d
	sete	%bl
	cmpb	%r8b, %sil
	sete	%dl
	orb	%dl, %bl
	jne	.L43
	cmpb	%cl, %r8b
	je	.L43
	movl	%ecx, %r10d
	movl	%esi, %r11d
	movq	%rax, %rcx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L55:
	cmpb	%r11b, %dl
	je	.L38
.L39:
	addq	$1, %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L54
	cmpb	%r9b, %dl
	sete	%r8b
	cmpb	%r10b, %dl
	sete	%sil
	orb	%sil, %r8b
	je	.L55
.L38:
	leaq	1(%rcx), %rdx
	movb	$0, (%rcx)
	popq	%rbx
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%edx, %edx
	movq	%rdx, (%rdi)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rax, %rcx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L50:
	rep ret
.L42:
	xorl	%edx, %edx
	movq	%rdx, (%rdi)
	ret
	.size	__old_strsep_3c, .-__old_strsep_3c
	.p2align 4,,15
	.globl	__old_strcspn_c1
	.type	__old_strcspn_c1, @function
__old_strcspn_c1:
	movsbl	(%rdi), %eax
	testb	%al, %al
	je	.L59
	cmpl	%eax, %esi
	movl	$0, %eax
	jne	.L58
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	%esi, %edx
	je	.L65
.L58:
	addq	$1, %rax
	movsbl	(%rdi,%rax), %edx
	testb	%dl, %dl
	jne	.L66
.L56:
	rep ret
	.p2align 4,,10
	.p2align 3
.L65:
	rep ret
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
	ret
	.size	__old_strcspn_c1, .-__old_strcspn_c1
	.p2align 4,,15
	.globl	__old_strcspn_c2
	.type	__old_strcspn_c2, @function
__old_strcspn_c2:
	movsbl	(%rdi), %eax
	testb	%al, %al
	je	.L72
	cmpl	%eax, %edx
	je	.L72
	cmpl	%eax, %esi
	je	.L72
	xorl	%eax, %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L87:
	cmpl	%edx, %ecx
	je	.L67
	cmpl	%esi, %ecx
	je	.L86
.L69:
	addq	$1, %rax
	movsbl	(%rdi,%rax), %ecx
	testb	%cl, %cl
	jne	.L87
.L67:
	rep ret
	.p2align 4,,10
	.p2align 3
.L86:
	rep ret
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%eax, %eax
	ret
	.size	__old_strcspn_c2, .-__old_strcspn_c2
	.p2align 4,,15
	.globl	__old_strcspn_c3
	.type	__old_strcspn_c3, @function
__old_strcspn_c3:
	movsbl	(%rdi), %eax
	testb	%al, %al
	je	.L93
	cmpl	%eax, %edx
	setne	%r9b
	cmpl	%eax, %esi
	setne	%r8b
	testb	%r8b, %r9b
	je	.L93
	cmpl	%eax, %ecx
	je	.L93
	xorl	%eax, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L108:
	cmpl	%ecx, %r8d
	je	.L107
.L90:
	addq	$1, %rax
	movsbl	(%rdi,%rax), %r8d
	testb	%r8b, %r8b
	je	.L88
	cmpl	%edx, %r8d
	setne	%r10b
	cmpl	%esi, %r8d
	setne	%r9b
	testb	%r9b, %r10b
	jne	.L108
.L88:
	rep ret
	.p2align 4,,10
	.p2align 3
.L107:
	rep ret
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
	ret
	.size	__old_strcspn_c3, .-__old_strcspn_c3
	.p2align 4,,15
	.globl	__old_strspn_c1
	.type	__old_strspn_c1, @function
__old_strspn_c1:
	movsbl	(%rdi), %eax
	cmpl	%eax, %esi
	movl	%eax, %edx
	movl	$0, %eax
	jne	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$1, %rax
	cmpb	%dl, (%rdi,%rax)
	je	.L111
	rep ret
	.p2align 4,,10
	.p2align 3
.L112:
	rep ret
	.size	__old_strspn_c1, .-__old_strspn_c1
	.p2align 4,,15
	.globl	__old_strspn_c2
	.type	__old_strspn_c2, @function
__old_strspn_c2:
	movsbl	(%rdi), %eax
	cmpl	%eax, %edx
	je	.L119
	cmpl	%eax, %esi
	jne	.L118
.L119:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$1, %rax
	movsbl	(%rdi,%rax), %ecx
	cmpl	%edx, %ecx
	je	.L120
	cmpl	%esi, %ecx
	je	.L120
	rep ret
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%eax, %eax
	ret
	.size	__old_strspn_c2, .-__old_strspn_c2
	.p2align 4,,15
	.globl	__old_strspn_c3
	.type	__old_strspn_c3, @function
__old_strspn_c3:
	movsbl	(%rdi), %eax
	cmpl	%eax, %edx
	sete	%r9b
	cmpl	%eax, %esi
	sete	%r8b
	orb	%r8b, %r9b
	jne	.L126
	cmpl	%eax, %ecx
	jne	.L125
.L126:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$1, %rax
	movsbl	(%rdi,%rax), %r8d
	cmpl	%edx, %r8d
	sete	%r10b
	cmpl	%esi, %r8d
	sete	%r9b
	orb	%r9b, %r10b
	jne	.L127
	cmpl	%ecx, %r8d
	je	.L127
	rep ret
.L125:
	xorl	%eax, %eax
	ret
	.size	__old_strspn_c3, .-__old_strspn_c3
	.p2align 4,,15
	.globl	__old_strpbrk_c2
	.type	__old_strpbrk_c2, @function
__old_strpbrk_c2:
	movsbl	(%rdi), %ecx
	movq	%rdi, %rax
	testb	%cl, %cl
	je	.L133
	cmpl	%esi, %ecx
	je	.L129
	cmpl	%edx, %ecx
	jne	.L130
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	%edx, %ecx
	je	.L129
	cmpl	%esi, %ecx
	je	.L144
.L130:
	addq	$1, %rax
	movsbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L145
.L133:
	xorl	%eax, %eax
.L129:
	rep ret
	.p2align 4,,10
	.p2align 3
.L144:
	rep ret
	.size	__old_strpbrk_c2, .-__old_strpbrk_c2
	.p2align 4,,15
	.globl	__old_strpbrk_c3
	.type	__old_strpbrk_c3, @function
__old_strpbrk_c3:
	movq	%rdi, %rax
	movsbl	(%rdi), %edi
	testb	%dil, %dil
	je	.L151
	cmpl	%edi, %edx
	setne	%r9b
	cmpl	%edi, %esi
	setne	%r8b
	testb	%r8b, %r9b
	je	.L147
	cmpl	%edi, %ecx
	jne	.L148
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L163:
	cmpl	%esi, %r8d
	je	.L162
.L148:
	addq	$1, %rax
	movsbl	(%rax), %r8d
	testb	%r8b, %r8b
	je	.L151
	cmpl	%ecx, %r8d
	setne	%r9b
	cmpl	%edx, %r8d
	setne	%dil
	testb	%dil, %r9b
	jne	.L163
.L147:
	rep ret
	.p2align 4,,10
	.p2align 3
.L162:
	rep ret
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax
	ret
	.size	__old_strpbrk_c3, .-__old_strpbrk_c3
	.p2align 4,,15
	.globl	__old_mempcpy_small
	.type	__old_mempcpy_small, @function
__old_mempcpy_small:
	movq	32(%rsp), %rax
	movq	%rcx, -8(%rsp)
	movq	%r9, -16(%rsp)
	cmpl	$8, %eax
	ja	.L165
	leaq	.L167(%rip), %r9
	movl	%eax, %ecx
	movslq	(%r9,%rcx,4), %rcx
	addq	%r9, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L167:
	.long	.L165-.L167
	.long	.L166-.L167
	.long	.L168-.L167
	.long	.L169-.L167
	.long	.L170-.L167
	.long	.L171-.L167
	.long	.L172-.L167
	.long	.L173-.L167
	.long	.L174-.L167
	.text
	.p2align 4,,10
	.p2align 3
.L166:
	movb	%sil, (%rdi)
.L165:
	addq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	movq	24(%rsp), %rdx
	addq	%rdi, %rax
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	movw	%dx, (%rdi)
	addq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movzwl	-8(%rsp), %edx
	addq	%rdi, %rax
	movw	%dx, (%rdi)
	movzbl	-6(%rsp), %edx
	movb	%dl, 2(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	movl	%r8d, (%rdi)
	addq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	movl	-16(%rsp), %edx
	addq	%rdi, %rax
	movl	%edx, (%rdi)
	movzbl	-12(%rsp), %edx
	movb	%dl, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movl	8(%rsp), %edx
	addq	%rdi, %rax
	movl	%edx, (%rdi)
	movzwl	12(%rsp), %edx
	movw	%dx, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	movl	16(%rsp), %edx
	addq	%rdi, %rax
	movl	%edx, (%rdi)
	movzwl	20(%rsp), %edx
	movw	%dx, 4(%rdi)
	movzbl	22(%rsp), %edx
	movb	%dl, 6(%rdi)
	ret
	.size	__old_mempcpy_small, .-__old_mempcpy_small
	.p2align 4,,15
	.globl	__old_strcpy_small
	.type	__old_strcpy_small, @function
__old_strcpy_small:
	cmpl	$8, 24(%rsp)
	movq	%rdx, -8(%rsp)
	movq	%r8, -16(%rsp)
	movq	%r9, -24(%rsp)
	ja	.L176
	movl	24(%rsp), %edx
	leaq	.L178(%rip), %rax
	movslq	(%rax,%rdx,4), %rdx
	addq	%rax, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L178:
	.long	.L176-.L178
	.long	.L177-.L178
	.long	.L179-.L178
	.long	.L180-.L178
	.long	.L181-.L178
	.long	.L182-.L178
	.long	.L183-.L178
	.long	.L184-.L178
	.long	.L185-.L178
	.text
	.p2align 4,,10
	.p2align 3
.L177:
	movb	$0, (%rdi)
.L176:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	movq	16(%rsp), %rax
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	movw	%si, (%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movzwl	-8(%rsp), %eax
	movw	%ax, (%rdi)
	movzbl	-6(%rsp), %eax
	movb	%al, 2(%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%ecx, (%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	movl	-16(%rsp), %eax
	movl	%eax, (%rdi)
	movzbl	-12(%rsp), %eax
	movb	%al, 4(%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	movl	-24(%rsp), %eax
	movl	%eax, (%rdi)
	movzwl	-20(%rsp), %eax
	movw	%ax, 4(%rdi)
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movl	8(%rsp), %eax
	movl	%eax, (%rdi)
	movzwl	12(%rsp), %eax
	movw	%ax, 4(%rdi)
	movzbl	14(%rsp), %eax
	movb	%al, 6(%rdi)
	movq	%rdi, %rax
	ret
	.size	__old_strcpy_small, .-__old_strcpy_small
	.p2align 4,,15
	.globl	__old_stpcpy_small
	.type	__old_stpcpy_small, @function
__old_stpcpy_small:
	movq	24(%rsp), %rax
	movq	%rdx, -8(%rsp)
	movq	%r8, -16(%rsp)
	movq	%r9, -24(%rsp)
	cmpl	$8, %eax
	ja	.L187
	leaq	.L189(%rip), %r8
	movl	%eax, %edx
	movslq	(%r8,%rdx,4), %rdx
	addq	%r8, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L189:
	.long	.L187-.L189
	.long	.L188-.L189
	.long	.L190-.L189
	.long	.L191-.L189
	.long	.L192-.L189
	.long	.L193-.L189
	.long	.L194-.L189
	.long	.L195-.L189
	.long	.L196-.L189
	.text
	.p2align 4,,10
	.p2align 3
.L188:
	movb	$0, (%rdi)
.L187:
	leaq	-1(%rdi,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	movq	16(%rsp), %rdx
	leaq	-1(%rdi,%rax), %rax
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movw	%si, (%rdi)
	leaq	-1(%rdi,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movzwl	-8(%rsp), %edx
	leaq	-1(%rdi,%rax), %rax
	movw	%dx, (%rdi)
	movzbl	-6(%rsp), %edx
	movb	%dl, 2(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	movl	%ecx, (%rdi)
	leaq	-1(%rdi,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	movl	-16(%rsp), %edx
	leaq	-1(%rdi,%rax), %rax
	movl	%edx, (%rdi)
	movzbl	-12(%rsp), %edx
	movb	%dl, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	movl	-24(%rsp), %edx
	leaq	-1(%rdi,%rax), %rax
	movl	%edx, (%rdi)
	movzwl	-20(%rsp), %edx
	movw	%dx, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	movl	8(%rsp), %edx
	leaq	-1(%rdi,%rax), %rax
	movl	%edx, (%rdi)
	movzwl	12(%rsp), %edx
	movw	%dx, 4(%rdi)
	movzbl	14(%rsp), %edx
	movb	%dl, 6(%rdi)
	ret
	.size	__old_stpcpy_small, .-__old_stpcpy_small
