	.text
	.p2align 4,,15
	.globl	argz_delete
	.hidden	argz_delete
	.type	argz_delete, @function
argz_delete:
	testq	%rdx, %rdx
	je	.L6
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	strlen
	movq	(%r12), %rdx
	movq	%rbx, %rcx
	subq	0(%rbp), %rcx
	addq	$1, %rax
	movq	%rbx, %rdi
	leaq	(%rbx,%rax), %rsi
	subq	%rax, %rdx
	movq	%rdx, (%r12)
	subq	%rcx, %rdx
	call	memmove
	cmpq	$0, (%r12)
	je	.L10
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	0(%rbp), %rdi
	call	free@PLT
	movq	$0, 0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	rep ret
	.size	argz_delete, .-argz_delete
	.hidden	memmove
	.hidden	strlen
