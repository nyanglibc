	.text
	.p2align 4,,15
	.globl	__sigdescr_np
	.hidden	__sigdescr_np
	.type	__sigdescr_np, @function
__sigdescr_np:
	cmpl	$64, %edi
	ja	.L3
	leaq	__sys_siglist(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.size	__sigdescr_np, .-__sigdescr_np
	.weak	sigdescr_np
	.set	sigdescr_np,__sigdescr_np
	.hidden	__sys_siglist
