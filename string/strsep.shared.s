	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strsep
	.hidden	__GI___strsep
	.type	__GI___strsep, @function
__GI___strsep:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	call	__GI_strcspn
	addq	%rbx, %rax
	cmpb	$0, (%rax)
	jne	.L9
	movq	$0, 0(%rbp)
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, 0(%rbp)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI___strsep, .-__GI___strsep
	.weak	strsep
	.set	strsep,__GI___strsep
	.globl	__GI___strsep_g
	.hidden	__GI___strsep_g
	.set	__GI___strsep_g,__GI___strsep
	.globl	__strsep_g
	.set	__strsep_g,__GI___strsep_g
	.globl	__strsep
	.set	__strsep,__GI___strsep
