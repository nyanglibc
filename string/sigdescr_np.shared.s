	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigdescr_np
	.hidden	__GI___sigdescr_np
	.type	__GI___sigdescr_np, @function
__GI___sigdescr_np:
	cmpl	$64, %edi
	ja	.L3
	leaq	__GI___sys_siglist(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.size	__GI___sigdescr_np, .-__GI___sigdescr_np
	.globl	__sigdescr_np
	.set	__sigdescr_np,__GI___sigdescr_np
	.weak	sigdescr_np
	.set	sigdescr_np,__sigdescr_np
