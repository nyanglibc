	.text
	.p2align 4,,15
	.globl	strfry
	.type	strfry, @function
strfry:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	init.5390(%rip), %eax
	testl	%eax, %eax
	je	.L8
.L2:
	movq	%r15, %rdi
	call	strlen
	cmpq	$1, %rax
	movq	%rax, %rbx
	jbe	.L3
	leaq	-1(%rax), %r13
	leaq	rdata.5391(%rip), %r12
	xorl	%r14d, %r14d
	movq	%rsp, %rbp
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__random_r
	movslq	(%rsp), %rax
	movq	%rbx, %rsi
	xorl	%edx, %edx
	subq	%r14, %rsi
	movzbl	(%r15,%r14), %ecx
	divq	%rsi
	addl	%r14d, %edx
	movslq	%edx, %rdx
	addq	%r15, %rdx
	movzbl	(%rdx), %eax
	movb	%al, (%r15,%r14)
	addq	$1, %r14
	movb	%cl, (%rdx)
	cmpq	%r13, %r14
	jne	.L4
.L3:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rsp, %rbp
	movl	$1, %edi
	movq	$0, 16+rdata.5391(%rip)
	movq	%rbp, %rsi
	call	__clock_gettime
	movq	(%rsp), %rdi
	xorl	8(%rsp), %edi
	leaq	rdata.5391(%rip), %rcx
	leaq	state.5392(%rip), %rsi
	movl	$32, %edx
	movl	%edi, %eax
	rorl	$8, %eax
	xorl	%eax, %edi
	call	__initstate_r
	movl	$1, init.5390(%rip)
	jmp	.L2
	.size	strfry, .-strfry
	.local	state.5392
	.comm	state.5392,32,32
	.local	rdata.5391
	.comm	rdata.5391,48,32
	.local	init.5390
	.comm	init.5390,4,4
	.hidden	__initstate_r
	.hidden	__clock_gettime
	.hidden	__random_r
	.hidden	strlen
