	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	__x86_cacheinfo_ifunc, @function
__x86_cacheinfo_ifunc:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movq	408(%rdx), %rax
	movq	%rax, %rcx
	movq	%rax, __x86_raw_data_cache_size(%rip)
	shrq	$63, %rcx
	addq	%rax, %rcx
	xorb	%al, %al
	sarq	%rcx
	movq	%rax, __x86_data_cache_size(%rip)
	movq	%rcx, __x86_raw_data_cache_size_half(%rip)
	movq	%rax, %rcx
	movq	416(%rdx), %rax
	sarq	%rcx
	movq	%rcx, __x86_data_cache_size_half(%rip)
	movq	%rax, %rcx
	movq	%rax, __x86_raw_shared_cache_size(%rip)
	shrq	$63, %rcx
	addq	%rax, %rcx
	xorb	%al, %al
	sarq	%rcx
	movq	%rax, __x86_shared_cache_size(%rip)
	movq	%rcx, __x86_raw_shared_cache_size_half(%rip)
	movq	%rax, %rcx
	movq	424(%rdx), %rax
	sarq	%rcx
	movq	%rcx, __x86_shared_cache_size_half(%rip)
	movq	%rax, __x86_shared_non_temporal_threshold(%rip)
	movq	432(%rdx), %rax
	movq	%rax, __x86_rep_movsb_threshold(%rip)
	movq	440(%rdx), %rax
	movq	%rax, __x86_rep_stosb_threshold(%rip)
	xorl	%eax, %eax
	ret
	.size	__x86_cacheinfo_ifunc, .-__x86_cacheinfo_ifunc
	.globl	__x86_cacheinfo
	.hidden	__x86_cacheinfo
	.type	__x86_cacheinfo, @gnu_indirect_function
	.set	__x86_cacheinfo,__x86_cacheinfo_ifunc
	.p2align 4,,15
	.globl	__cache_sysconf
	.hidden	__cache_sysconf
	.type	__cache_sysconf, @function
__cache_sysconf:
	subl	$185, %edi
	cmpl	$12, %edi
	ja	.L17
	leaq	.L6(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L5-.L6
	.long	.L17-.L6
	.long	.L17-.L6
	.long	.L7-.L6
	.long	.L8-.L6
	.long	.L9-.L6
	.long	.L10-.L6
	.long	.L11-.L6
	.long	.L12-.L6
	.long	.L13-.L6
	.long	.L14-.L6
	.long	.L15-.L6
	.long	.L16-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L16:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	528(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	448(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	456(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	464(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	472(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	480(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	488(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	496(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	504(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	512(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	520(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	$-1, %rax
	ret
	.size	__cache_sysconf, .-__cache_sysconf
	.hidden	__x86_cacheinfo_p
	.globl	__x86_cacheinfo_p
	.section	.data.rel.ro,"aw",@progbits
	.align 8
	.type	__x86_cacheinfo_p, @object
	.size	__x86_cacheinfo_p, 8
__x86_cacheinfo_p:
	.quad	__x86_cacheinfo
	.hidden	__x86_rep_stosb_threshold
	.globl	__x86_rep_stosb_threshold
	.data
	.align 8
	.type	__x86_rep_stosb_threshold, @object
	.size	__x86_rep_stosb_threshold, 8
__x86_rep_stosb_threshold:
	.quad	2048
	.hidden	__x86_rep_movsb_threshold
	.globl	__x86_rep_movsb_threshold
	.align 8
	.type	__x86_rep_movsb_threshold, @object
	.size	__x86_rep_movsb_threshold, 8
__x86_rep_movsb_threshold:
	.quad	2048
	.hidden	__x86_shared_non_temporal_threshold
	.comm	__x86_shared_non_temporal_threshold,8,8
	.hidden	__x86_raw_shared_cache_size
	.globl	__x86_raw_shared_cache_size
	.align 8
	.type	__x86_raw_shared_cache_size, @object
	.size	__x86_raw_shared_cache_size, 8
__x86_raw_shared_cache_size:
	.quad	1048576
	.hidden	__x86_raw_shared_cache_size_half
	.globl	__x86_raw_shared_cache_size_half
	.align 8
	.type	__x86_raw_shared_cache_size_half, @object
	.size	__x86_raw_shared_cache_size_half, 8
__x86_raw_shared_cache_size_half:
	.quad	524288
	.hidden	__x86_shared_cache_size
	.globl	__x86_shared_cache_size
	.align 8
	.type	__x86_shared_cache_size, @object
	.size	__x86_shared_cache_size, 8
__x86_shared_cache_size:
	.quad	1048576
	.hidden	__x86_shared_cache_size_half
	.globl	__x86_shared_cache_size_half
	.align 8
	.type	__x86_shared_cache_size_half, @object
	.size	__x86_shared_cache_size_half, 8
__x86_shared_cache_size_half:
	.quad	524288
	.hidden	__x86_raw_data_cache_size
	.globl	__x86_raw_data_cache_size
	.align 8
	.type	__x86_raw_data_cache_size, @object
	.size	__x86_raw_data_cache_size, 8
__x86_raw_data_cache_size:
	.quad	32768
	.hidden	__x86_raw_data_cache_size_half
	.globl	__x86_raw_data_cache_size_half
	.align 8
	.type	__x86_raw_data_cache_size_half, @object
	.size	__x86_raw_data_cache_size_half, 8
__x86_raw_data_cache_size_half:
	.quad	16384
	.hidden	__x86_data_cache_size
	.globl	__x86_data_cache_size
	.align 8
	.type	__x86_data_cache_size, @object
	.size	__x86_data_cache_size, 8
__x86_data_cache_size:
	.quad	32768
	.hidden	__x86_data_cache_size_half
	.globl	__x86_data_cache_size_half
	.align 8
	.type	__x86_data_cache_size_half, @object
	.size	__x86_data_cache_size_half, 8
__x86_data_cache_size_half:
	.quad	16384
