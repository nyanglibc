	.text
	.p2align 4,,15
	.globl	sigabbrev_np
	.type	sigabbrev_np, @function
sigabbrev_np:
	cmpl	$64, %edi
	ja	.L3
	leaq	__sys_sigabbrev(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	ret
	.size	sigabbrev_np, .-sigabbrev_np
	.hidden	__sys_sigabbrev
