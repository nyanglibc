	.text
	.p2align 4,,15
	.globl	strerror
	.type	strerror, @function
strerror:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rsi
	jmp	__strerror_l
	.size	strerror, .-strerror
	.hidden	__strerror_l
