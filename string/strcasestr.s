	.text
	.p2align 4,,15
	.type	critical_factorization, @function
critical_factorization:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	pushq	%r12
	movl	$1, %r9d
	pushq	%rbp
	xorl	%r10d, %r10d
	pushq	%rbx
	movl	$1, %ebp
	movq	%fs:(%rax), %r8
	movq	$-1, %rax
.L2:
	leaq	(%r10,%r9), %rcx
	cmpq	%rsi, %rcx
	jnb	.L17
.L7:
	leaq	(%rdi,%r9), %rbx
	movzbl	(%rdi,%rcx), %r11d
	movzbl	(%rbx,%rax), %ebx
	movzbl	(%r8,%r11,4), %r11d
	cmpb	%r11b, (%r8,%rbx,4)
	jbe	.L3
	movq	%rcx, %r10
	movl	$1, %r9d
	movq	%rcx, %rbp
	leaq	(%r10,%r9), %rcx
	subq	%rax, %rbp
	cmpq	%rsi, %rcx
	jb	.L7
.L17:
	movq	%rbp, (%rdx)
	movl	$1, %r12d
	movl	$1, %r9d
	xorl	%r10d, %r10d
	movq	$-1, %r11
.L8:
	leaq	(%r10,%r9), %rcx
	cmpq	%rcx, %rsi
	jbe	.L18
.L13:
	leaq	(%rdi,%r11), %rbx
	movzbl	(%rdi,%rcx), %ebp
	movzbl	(%rbx,%r9), %ebx
	movzbl	(%r8,%rbx,4), %ebx
	cmpb	%bl, (%r8,%rbp,4)
	jbe	.L9
	movq	%rcx, %r10
	movl	$1, %r9d
	movq	%rcx, %r12
	leaq	(%r10,%r9), %rcx
	subq	%r11, %r12
	cmpq	%rcx, %rsi
	ja	.L13
.L18:
	addq	$1, %r11
	addq	$1, %rax
	cmpq	%rax, %r11
	jb	.L1
	movq	%r12, (%rdx)
	movq	%r11, %rax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	je	.L19
	movq	%r10, %rax
	movl	$1, %ebp
	addq	$1, %r10
	movl	$1, %r9d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	je	.L20
	movq	%r10, %r11
	movl	$1, %r12d
	addq	$1, %r10
	movl	$1, %r9d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	cmpq	%rbp, %r9
	je	.L6
	addq	$1, %r9
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	%r12, %r9
	je	.L12
	addq	$1, %r9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rcx, %r10
	movl	$1, %r9d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rcx, %r10
	movl	$1, %r9d
	jmp	.L8
	.size	critical_factorization, .-critical_factorization
	.p2align 4,,15
	.type	two_way_long_needle, @function
two_way_long_needle:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$2168, %rsp
	leaq	104(%rsp), %rdx
	movq	%rdi, 16(%rsp)
	movq	%rsi, 8(%rsp)
	movq	%r15, %rdi
	movq	%rcx, %rsi
	call	critical_factorization
	movq	16(%rsp), %r9
	movq	8(%rsp), %r8
	movq	%rax, %rcx
	leaq	2160(%rsp), %rdx
	leaq	112(%rsp), %rax
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rbp, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L22
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	testq	%rbp, %rbp
	movq	%fs:(%rax), %rbx
	je	.L23
	leaq	-1(%rbp), %rax
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L24:
	movzbl	(%rdx), %esi
	addq	$1, %rdx
	movslq	(%rbx,%rsi,4), %rsi
	movq	%rax, 112(%rsp,%rsi,8)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L24
.L23:
	movq	104(%rsp), %rax
	movq	%rcx, %rdx
	movq	%r15, %rdi
	movq	%r8, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%rcx, 8(%rsp)
	leaq	(%r15,%rax), %rsi
	movq	%rax, 40(%rsp)
	call	__strncasecmp@PLT
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r9
	movq	24(%rsp), %r8
	jne	.L82
	leaq	512(%rbp), %rax
	leaq	-1(%rbp), %r13
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	movq	%rax, 48(%rsp)
	leaq	-1(%rcx), %rax
	movq	%rax, 72(%rsp)
	addq	%r15, %rax
	movq	%rax, 80(%rsp)
	movq	%rbp, %rax
	subq	40(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	-1(%r15), %rax
	movq	%rax, 88(%rsp)
	movl	$1, %eax
	subq	%rcx, %rax
	movq	%rax, 64(%rsp)
.L25:
	leaq	0(%rbp,%r12), %rdx
	cmpq	%r8, %rdx
	jbe	.L37
.L83:
	movq	48(%rsp), %rsi
	leaq	(%r9,%r8), %rdi
	movq	%rcx, 32(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r9, 8(%rsp)
	call	__strnlen
	movq	16(%rsp), %r8
	movq	24(%rsp), %rdx
	movq	8(%rsp), %r9
	movq	32(%rsp), %rcx
	addq	%rax, %r8
	cmpq	%r8, %rdx
	ja	.L48
.L37:
	movzbl	-1(%r9,%rdx), %eax
	movslq	(%rbx,%rax,4), %rax
	movq	112(%rsp,%rax,8), %rax
	testq	%rax, %rax
	je	.L27
	cmpq	%rax, 40(%rsp)
	jbe	.L80
	testq	%r14, %r14
	cmovne	56(%rsp), %rax
.L80:
	addq	%rax, %r12
	xorl	%r14d, %r14d
	leaq	0(%rbp,%r12), %rdx
	cmpq	%r8, %rdx
	jbe	.L37
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L27:
	cmpq	%r14, %rcx
	movq	%r14, %rax
	cmovnb	%rcx, %rax
	leaq	(%rax,%r12), %rdx
	leaq	(%r15,%rax), %rsi
	addq	%r9, %rdx
	cmpq	%r13, %rax
	jnb	.L30
	movzbl	(%rsi), %esi
	movzbl	(%rdx), %edx
	movl	(%rbx,%rdx,4), %edi
	cmpl	%edi, (%rbx,%rsi,4)
	jne	.L31
	leaq	(%r9,%r12), %rdi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	(%r15,%rax), %esi
	movzbl	(%rdi,%rax), %edx
	movl	(%rbx,%rdx,4), %edx
	cmpl	%edx, (%rbx,%rsi,4)
	jne	.L31
.L32:
	addq	$1, %rax
	cmpq	%r13, %rax
	jb	.L84
.L30:
	movq	72(%rsp), %rax
	leaq	(%rax,%r12), %rsi
	movq	%rax, %rdx
	addq	%r9, %rsi
	cmpq	%r14, %rcx
	jbe	.L51
	movq	80(%rsp), %rax
	movzbl	(%rsi), %edi
	movzbl	(%rax), %eax
	movl	(%rbx,%rax,4), %eax
	cmpl	%eax, (%rbx,%rdi,4)
	jne	.L51
	movq	%r12, 8(%rsp)
	leaq	-1(%r14), %r10
	subq	%rcx, %rsi
	movq	88(%rsp), %r12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	1(%r12,%rax), %r11d
	movzbl	1(%rsi,%rax), %edi
	movl	(%rbx,%rdi,4), %edi
	cmpl	%edi, (%rbx,%r11,4)
	jne	.L75
	movq	%rax, %rdx
.L34:
	leaq	-1(%rdx), %rax
	cmpq	%r10, %rax
	jne	.L85
.L75:
	addq	$1, %r14
	movq	8(%rsp), %r12
	cmpq	%rdx, %r14
	ja	.L86
.L35:
	addq	40(%rsp), %r12
	movq	56(%rsp), %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L31:
	addq	64(%rsp), %r12
	jmp	.L80
.L48:
	xorl	%eax, %eax
.L21:
	addq	$2168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L82:
	movq	%rbp, %rax
	leaq	-1(%rbp), %r14
	movq	%r8, %r12
	subq	%rcx, %rax
	cmpq	%rcx, %rax
	cmovb	%rcx, %rax
	xorl	%r13d, %r13d
	addq	$1, %rax
	movq	%rax, 56(%rsp)
	movq	%rax, 104(%rsp)
	leaq	512(%rbp), %rax
	movq	%rax, 32(%rsp)
	leaq	(%r15,%rcx), %rax
	movq	%rax, 40(%rsp)
	leaq	-1(%r15), %rax
	movq	%rax, 64(%rsp)
	movl	$1, %eax
	subq	%rcx, %rax
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	0(%rbp,%r13), %rdx
	cmpq	%rdx, %r12
	jnb	.L47
	movq	32(%rsp), %rsi
	leaq	(%r9,%r12), %rdi
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r9, 8(%rsp)
	call	__strnlen
	movq	16(%rsp), %rdx
	addq	%rax, %r12
	movq	8(%rsp), %r9
	movq	24(%rsp), %rcx
	cmpq	%rdx, %r12
	jb	.L48
.L47:
	movzbl	-1(%r9,%rdx), %eax
	movslq	(%rbx,%rax,4), %rax
	movq	112(%rsp,%rax,8), %rax
	testq	%rax, %rax
	jne	.L81
	leaq	(%rcx,%r13), %rax
	addq	%r9, %rax
	cmpq	%r14, %rcx
	jnb	.L41
	movq	40(%rsp), %rdi
	movzbl	(%rax), %eax
	movzbl	(%rdi), %edx
	movl	(%rbx,%rax,4), %eax
	cmpl	%eax, (%rbx,%rdx,4)
	movq	%rcx, %rax
	jne	.L42
	leaq	(%r9,%r13), %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L87:
	movzbl	(%rdi,%rax), %esi
	movzbl	(%r15,%rax), %edx
	movl	(%rbx,%rdx,4), %edx
	cmpl	%edx, (%rbx,%rsi,4)
	jne	.L42
.L43:
	addq	$1, %rax
	cmpq	%r14, %rax
	jb	.L87
.L41:
	leaq	-1(%rcx), %rax
	leaq	(%rax,%r13), %rdx
	leaq	(%r15,%rax), %rsi
	addq	%r9, %rdx
	cmpq	$-1, %rax
	je	.L44
	movzbl	(%rsi), %edi
	movzbl	(%rdx), %esi
	movl	(%rbx,%rsi,4), %esi
	cmpl	%esi, (%rbx,%rdi,4)
	jne	.L45
	subq	%rcx, %rdx
	movq	64(%rsp), %r8
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	1(%rdx,%rax), %edi
	movzbl	1(%r8,%rax), %esi
	movl	(%rbx,%rsi,4), %esi
	cmpl	%esi, (%rbx,%rdi,4)
	jne	.L45
.L46:
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L88
.L44:
	leaq	(%r9,%r13), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L42:
	addq	48(%rsp), %r13
.L81:
	addq	%rax, %r13
	jmp	.L38
.L45:
	addq	56(%rsp), %r13
	jmp	.L38
.L51:
	movq	%rcx, %rdx
	addq	$1, %r14
	cmpq	%rdx, %r14
	jbe	.L35
.L86:
	leaq	(%r9,%r12), %rax
	jmp	.L21
	.size	two_way_long_needle, .-two_way_long_needle
	.p2align 4,,15
	.globl	__strcasestr
	.type	__strcasestr, @function
__strcasestr:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	cmpb	$0, (%rsi)
	je	.L89
	movq	%rdi, %r14
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen
	leaq	256(%rax), %rsi
	movq	%rax, %rbp
	movq	%r13, %rdi
	call	__strnlen
	cmpq	%rax, %rbp
	ja	.L126
	cmpq	$31, %rbp
	jbe	.L150
	movq	%r13, %rdi
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	two_way_long_needle
	movq	%rax, %r13
.L89:
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L128:
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$1, %r15
	cmpq	%rdx, %r15
	ja	.L151
	addq	48(%rsp), %r13
	movq	72(%rsp), %r15
.L122:
	leaq	0(%rbp,%r13), %rdx
	cmpq	%r14, %rdx
	movq	%rdx, (%rsp)
	jbe	.L93
	movq	40(%rsp), %rsi
	leaq	(%r8,%r14), %rdi
	movq	%r10, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	call	__strnlen
	movq	(%rsp), %rdx
	addq	%rax, %r14
	movq	8(%rsp), %r8
	movq	16(%rsp), %r9
	movq	24(%rsp), %r10
	cmpq	%r14, %rdx
	jbe	.L93
.L126:
	xorl	%r13d, %r13d
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	88(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	movq	%rax, 8(%rsp)
	call	critical_factorization
	movq	%rax, %r9
	movq	88(%rsp), %rax
	movq	%r9, %rdx
	movq	%r9, (%rsp)
	leaq	(%r12,%rax), %rsi
	movq	%rax, 48(%rsp)
	call	__strncasecmp@PLT
	testl	%eax, %eax
	movq	(%rsp), %r9
	movq	8(%rsp), %r8
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	jne	.L92
	leaq	-1(%r9), %rdi
	movq	%fs:(%rax), %rbx
	movl	$1, %eax
	subq	%r9, %rax
	leaq	-1(%r12), %r10
	xorl	%r13d, %r13d
	movq	%rdi, 56(%rsp)
	addq	%r12, %rdi
	movq	%rax, 32(%rsp)
	movq	%rdi, 64(%rsp)
	movq	%rbp, %rdi
	subq	48(%rsp), %rdi
	leaq	512(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%rax, 40(%rsp)
	movq	%r14, %rax
	movq	%r8, %r14
	movq	%rdi, 72(%rsp)
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L93:
	cmpq	%r15, %r9
	movq	%r15, %rax
	cmovnb	%r9, %rax
	leaq	(%rax,%r13), %rdx
	leaq	(%r12,%rax), %rcx
	addq	%r8, %rdx
	cmpq	%rax, %rbp
	jbe	.L94
	movzbl	(%rdx), %esi
	movzbl	(%rcx), %edx
	movl	(%rbx,%rdx,4), %ecx
	cmpl	%ecx, (%rbx,%rsi,4)
	jne	.L95
	leaq	(%r8,%r13), %rsi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L152:
	movzbl	(%r12,%rax), %ecx
	movzbl	(%rsi,%rax), %edx
	movl	(%rbx,%rdx,4), %edi
	cmpl	%edi, (%rbx,%rcx,4)
	jne	.L95
.L96:
	addq	$1, %rax
	cmpq	%rax, %rbp
	jne	.L152
.L94:
	movq	56(%rsp), %rax
	leaq	(%rax,%r13), %rcx
	movq	%rax, %rdx
	addq	%r8, %rcx
	cmpq	%r15, %r9
	jbe	.L128
	movq	64(%rsp), %rax
	movzbl	(%rcx), %esi
	movzbl	(%rax), %eax
	movl	(%rbx,%rax,4), %eax
	cmpl	%eax, (%rbx,%rsi,4)
	jne	.L128
	leaq	-1(%r15), %rdi
	subq	%r9, %rcx
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L153:
	movzbl	1(%r10,%rax), %r11d
	movzbl	1(%rcx,%rax), %esi
	movl	(%rbx,%rsi,4), %esi
	cmpl	%esi, (%rbx,%r11,4)
	jne	.L97
	movq	%rax, %rdx
.L98:
	leaq	-1(%rdx), %rax
	cmpq	%rdi, %rax
	jne	.L153
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%fs:(%rax), %r13
	movzbl	(%r12,%r9), %eax
	leaq	-1(%r12), %r10
	leaq	1(%r12), %rcx
	movzbl	0(%r13,%rax,4), %r15d
	movq	%rbp, %rax
	subq	%r9, %rax
	cmpq	%r9, %rax
	cmovb	%r9, %rax
	xorl	%ebx, %ebx
	addq	$1, %rax
	movq	%rax, 64(%rsp)
	movq	%rax, 88(%rsp)
	leaq	(%r14,%r9), %rax
	movq	%rax, (%rsp)
	movl	$1, %eax
	subq	%r9, %rax
	movq	%rax, 56(%rsp)
	leaq	512(%rbp), %rax
	movq	%rax, 8(%rsp)
.L102:
	leaq	(%r9,%rbx), %rdx
	addq	%r14, %rdx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L105:
	testb	%al, %al
	movq	%rsi, %rdx
	je	.L126
.L103:
	leaq	1(%rdx), %rsi
	movzbl	-1(%rsi), %eax
	movl	0(%r13,%rax,4), %eax
	cmpb	%al, %r15b
	jne	.L105
	movq	%rsi, %rax
	subq	(%rsp), %rax
	leaq	1(%r9), %rdi
	leaq	(%r12,%rdi), %r11
	subq	$1, %rax
	cmpq	%rdi, %rbp
	jbe	.L130
	movzbl	(%rsi), %esi
	movzbl	(%r11), %ebx
	addq	$2, %rdx
	movl	0(%r13,%rsi,4), %esi
	movzbl	%sil, %r11d
	cmpl	%r11d, 0(%r13,%rbx,4)
	je	.L108
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$1, %rdx
	movzbl	-1(%rdx), %esi
	movzbl	-1(%rcx,%rdi), %ebx
	movl	0(%r13,%rsi,4), %esi
	movzbl	%sil, %r11d
	cmpl	%r11d, 0(%r13,%rbx,4)
	jne	.L109
.L108:
	addq	$1, %rdi
	cmpq	%rdi, %rbp
	jne	.L111
.L106:
	leaq	(%r14,%r8), %rsi
	cmpq	%rsi, %rdx
	ja	.L123
.L124:
	leaq	-1(%r9), %rdx
	leaq	(%rax,%rdx), %rbx
	leaq	(%r12,%rdx), %rdi
	addq	%r14, %rbx
	cmpq	$-1, %rdx
	je	.L114
	movzbl	(%rbx), %esi
	movzbl	(%rdi), %r11d
	movl	0(%r13,%rsi,4), %esi
	movzbl	%sil, %edi
	cmpl	%edi, 0(%r13,%r11,4)
	jne	.L117
	subq	%r9, %rbx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L119:
	movzbl	1(%rbx,%rdx), %esi
	movzbl	1(%r10,%rdx), %r11d
	movl	0(%r13,%rsi,4), %esi
	movzbl	%sil, %edi
	cmpl	%edi, 0(%r13,%r11,4)
	jne	.L117
.L116:
	subq	$1, %rdx
	cmpq	$-1, %rdx
	jne	.L119
.L114:
	leaq	(%r14,%rax), %r13
	jmp	.L89
.L109:
	testb	%sil, %sil
	je	.L126
	leaq	(%r14,%r8), %rsi
	cmpq	%rsi, %rdx
	jbe	.L112
.L123:
	subq	%r14, %rdx
	movq	%rdx, %r8
.L112:
	movq	56(%rsp), %rbx
	addq	%rax, %rbx
	addq	%rdi, %rbx
	cmpq	%rdi, %rbp
	jbe	.L124
.L121:
	leaq	0(%rbp,%rbx), %rdx
	cmpq	%rdx, %r8
	movq	%rdx, 48(%rsp)
	jnb	.L102
	movq	8(%rsp), %rsi
	leaq	(%r14,%r8), %rdi
	movq	%rcx, 40(%rsp)
	movq	%r10, 32(%rsp)
	movq	%r9, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	__strnlen
	movq	16(%rsp), %r8
	movq	48(%rsp), %rdx
	movq	24(%rsp), %r9
	movq	32(%rsp), %r10
	movq	40(%rsp), %rcx
	addq	%rax, %r8
	cmpq	%r8, %rdx
	jbe	.L102
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L95:
	addq	32(%rsp), %r13
	xorl	%r15d, %r15d
	addq	%rax, %r13
	jmp	.L122
.L151:
	addq	%r8, %r13
	jmp	.L89
.L117:
	testb	%sil, %sil
	je	.L126
	movq	64(%rsp), %rdi
	leaq	(%rdi,%rax), %rbx
	jmp	.L121
.L130:
	movq	%rsi, %rdx
	jmp	.L106
	.size	__strcasestr, .-__strcasestr
	.weak	strcasestr
	.set	strcasestr,__strcasestr
	.hidden	strlen
	.hidden	__strnlen
