	.text
	.p2align 4,,15
	.globl	__strcasecmp_l_nonascii
	.type	__strcasecmp_l_nonascii, @function
__strcasecmp_l_nonascii:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	je	.L1
	movq	112(%rdx), %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rdi
	testb	%r8b, %r8b
	je	.L1
.L3:
	addq	$1, %rsi
	movzbl	(%rdi), %eax
	movzbl	-1(%rsi), %ecx
	movq	%rax, %r8
	movl	(%rdx,%rax,4), %eax
	subl	(%rdx,%rcx,4), %eax
	je	.L4
.L1:
	rep ret
	.size	__strcasecmp_l_nonascii, .-__strcasecmp_l_nonascii
