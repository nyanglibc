	.text
	.p2align 4,,15
	.globl	envz_entry
	.hidden	envz_entry
	.type	envz_entry, @function
envz_entry:
	testq	%rsi, %rsi
	je	.L33
	pushq	%rbx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rdx, %r9
	movq	%rax, %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L35:
	cmpb	$61, %r8b
	je	.L16
	addq	$1, %r9
	subq	$1, %rsi
	leaq	1(%rcx), %r8
	je	.L34
	movq	%r8, %rcx
.L4:
	movzbl	(%r9), %r8d
	movzbl	(%rcx), %r10d
	testb	%r8b, %r8b
	setne	%bl
	cmpb	%r10b, %r8b
	sete	%r11b
	testb	%r11b, %bl
	jne	.L35
.L16:
	testb	%r10b, %r10b
	je	.L8
	cmpb	$61, %r10b
	je	.L8
.L11:
	testb	%r10b, %r10b
	jne	.L10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L36:
	cmpb	$0, (%rcx)
	je	.L9
.L10:
	addq	$1, %rcx
	subq	$1, %rsi
	jne	.L36
.L31:
	xorl	%eax, %eax
.L19:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	subq	$1, %rsi
	leaq	1(%rcx), %rax
	jne	.L2
	xorl	%eax, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	(%r9), %edi
	testb	%dil, %dil
	je	.L19
	cmpb	$61, %dil
	jne	.L11
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movzbl	1(%rcx), %edx
	cmpb	$61, %dl
	je	.L5
	testb	%dl, %dl
	jne	.L31
.L5:
	movzbl	(%r9), %edx
	testb	%dl, %dl
	je	.L19
	cmpb	$61, %dl
	movl	$0, %edx
	cmovne	%rdx, %rax
	popq	%rbx
	ret
.L33:
	xorl	%eax, %eax
	ret
	.size	envz_entry, .-envz_entry
	.p2align 4,,15
	.globl	envz_get
	.type	envz_get, @function
envz_get:
	call	envz_entry
	testq	%rax, %rax
	jne	.L57
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L58:
	cmpb	$61, %dl
	je	.L39
	addq	$1, %rax
.L57:
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L58
.L39:
	addq	$1, %rax
	testb	%dl, %dl
	movl	$0, %edx
	cmove	%rdx, %rax
.L37:
	rep ret
	.size	envz_get, .-envz_get
	.p2align 4,,15
	.globl	envz_remove
	.hidden	envz_remove
	.type	envz_remove, @function
envz_remove:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	call	envz_entry
	testq	%rax, %rax
	je	.L59
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	movq	%rax, %rdx
	jmp	argz_delete
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	envz_remove, .-envz_remove
	.p2align 4,,15
	.globl	envz_add
	.type	envz_add, @function
envz_add:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	subq	$56, %rsp
	call	envz_remove
	testq	%r13, %r13
	je	.L63
	movq	%r12, %rdi
	call	strlen
	movq	%r13, %rdi
	movq	%rax, %r14
	call	strlen
	movq	0(%rbp), %rdx
	movq	%r14, 24(%rsp)
	movq	%rax, 32(%rsp)
	movq	(%rbx), %rdi
	leaq	(%r14,%rdx), %r14
	movq	%rdx, 40(%rsp)
	addq	%r14, %rax
	leaq	2(%rax), %rsi
	movq	%rax, 8(%rsp)
	movq	%rsi, 16(%rsp)
	call	realloc@PLT
	movq	%rax, %r15
	movl	$12, %eax
	testq	%r15, %r15
	je	.L62
	movq	40(%rsp), %rdx
	movq	%r12, %rsi
	leaq	(%r15,%rdx), %rdi
	movq	24(%rsp), %rdx
	call	memcpy@PLT
	movq	32(%rsp), %rdx
	leaq	1(%r15,%r14), %rdi
	movq	%r13, %rsi
	movb	$61, (%r15,%r14)
	call	memcpy@PLT
	movq	8(%rsp), %rax
	movq	16(%rsp), %rsi
	movb	$0, 1(%r15,%rax)
	movq	%r15, (%rbx)
	xorl	%eax, %eax
	movq	%rsi, 0(%rbp)
.L62:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$56, %rsp
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__argz_add
	.size	envz_add, .-envz_add
	.p2align 4,,15
	.globl	envz_merge
	.type	envz_merge, @function
envz_merge:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rcx, %rcx
	movl	%r8d, 12(%rsp)
	je	.L74
	movq	%rdi, %r14
	movq	%rsi, %r13
	movq	%rdx, %rbp
	movq	%rcx, %r12
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L71:
	addq	%rbx, %rbp
	subq	%rbx, %r12
	je	.L68
.L83:
	testb	%cl, %cl
	je	.L68
.L72:
	movq	0(%r13), %rsi
	movq	(%r14), %rdi
	movq	%rbp, %rdx
	call	envz_entry
	movq	%rbp, %rdi
	movq	%rax, %r15
	call	strlen
	testq	%r15, %r15
	leaq	1(%rax), %rbx
	je	.L82
	movl	12(%rsp), %edx
	xorl	%eax, %eax
	movl	$1, %ecx
	testl	%edx, %edx
	je	.L71
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	argz_delete
.L82:
	movq	%rbx, %rcx
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__argz_append
	testl	%eax, %eax
	sete	%cl
	addq	%rbx, %rbp
	subq	%rbx, %r12
	jne	.L83
.L68:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	envz_merge, .-envz_merge
	.p2align 4,,15
	.globl	envz_strip
	.type	envz_strip, @function
envz_strip:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rsi), %r12
	movq	(%rdi), %rbp
	testq	%r12, %r12
	je	.L88
	movq	%rdi, %r14
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L89:
	testq	%r12, %r12
	movq	%rsi, %rbp
	je	.L92
.L87:
	movq	%rbp, %rdi
	call	strlen
	leaq	1(%rax), %rbx
	movl	$61, %esi
	movq	%rbp, %rdi
	call	strchr
	subq	%rbx, %r12
	testq	%rax, %rax
	leaq	0(%rbp,%rbx), %rsi
	jne	.L89
	movq	%r12, %rdx
	movq	%rbp, %rdi
	call	memmove
	testq	%r12, %r12
	jne	.L87
.L92:
	subq	(%r14), %rbp
	popq	%rbx
	movq	%rbp, 0(%r13)
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%ebp, %ebp
	popq	%rbx
	movq	%rbp, 0(%r13)
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	envz_strip, .-envz_strip
	.hidden	memmove
	.hidden	strchr
	.hidden	__argz_append
	.hidden	__argz_add
	.hidden	strlen
	.hidden	argz_delete
