.text
.globl strlen
.type strlen,@function
.align 1<<4
strlen:
 pxor %xmm0, %xmm0
 pxor %xmm1, %xmm1
 pxor %xmm2, %xmm2
 pxor %xmm3, %xmm3
 movq %rdi, %rax
 movq %rdi, %rcx
 andq $4095, %rcx
 cmpq $4047, %rcx
 ja .Lcross_page
 movdqu (%rax), %xmm4
 pcmpeqb %xmm0, %xmm4
 pmovmskb %xmm4, %edx
 test %edx, %edx
 je .Lnext48_bytes
 bsf %edx, %eax
 ret
.Lnext48_bytes:
 andq $-16, %rax
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 movq %rdi, %rcx
 xorq %rax, %rcx
 andq $-64, %rax
 sarq %cl, %rdx
 test %rdx, %rdx
 je .Lloop
 bsfq %rdx, %rax
 
 ret
 .p2align 4
.Lcross_page:
 andq $-64, %rax
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 movq %rdi, %rcx
 xorq %rax, %rcx
 andq $-64, %rax
 sarq %cl, %rdx
 test %rdx, %rdx
 je .Lloop_init
 bsfq %rdx, %rax
 
 ret
 .p2align 4
.Lloop_init:
 pxor %xmm1, %xmm1
 pxor %xmm2, %xmm2
 pxor %xmm3, %xmm3
 .p2align 4
.Lloop:
 movdqa 64(%rax), %xmm0
 pminub 80(%rax), %xmm0
 pminub 96(%rax), %xmm0
 pminub 112(%rax), %xmm0
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 testl %edx, %edx
 jne .Lexit64
 subq $-128, %rax
 movdqa (%rax), %xmm0
 pminub 16(%rax), %xmm0
 pminub 32(%rax), %xmm0
 pminub 48(%rax), %xmm0
 pcmpeqb %xmm3, %xmm0
 pmovmskb %xmm0, %edx
 testl %edx, %edx
 jne .Lexit0
 jmp .Lloop
 .p2align 4
.Lexit64:
 addq $64, %rax
.Lexit0:
 pxor %xmm0, %xmm0
 pcmpeqb (%rax), %xmm0
 pcmpeqb 16(%rax), %xmm1
 pcmpeqb 32(%rax), %xmm2
 pcmpeqb 48(%rax), %xmm3
 pmovmskb %xmm0, %esi
 pmovmskb %xmm1, %edx
 pmovmskb %xmm2, %r8d
 pmovmskb %xmm3, %ecx
 salq $16, %rdx
 salq $16, %rcx
 orq %rsi, %rdx
 orq %r8, %rcx
 salq $32, %rcx
 orq %rcx, %rdx
 bsfq %rdx, %rdx
 addq %rdx, %rax
 subq %rdi, %rax
 ret
.size strlen,.-strlen
.globl __GI_strlen
.set __GI_strlen,strlen
