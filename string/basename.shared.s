	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___basename
	.hidden	__GI___basename
	.type	__GI___basename, @function
__GI___basename:
	pushq	%rbx
	movl	$47, %esi
	movq	%rdi, %rbx
	call	__GI_strrchr
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	movq	%rbx, %rax
	popq	%rbx
	cmovne	%rdx, %rax
	ret
	.size	__GI___basename, .-__GI___basename
	.globl	__basename
	.set	__basename,__GI___basename
	.weak	__GI_basename
	.hidden	__GI_basename
	.set	__GI_basename,__basename
	.weak	basename
	.set	basename,__GI_basename
