	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strdup
	.hidden	__GI___strdup
	.type	__GI___strdup, @function
__GI___strdup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__GI_strlen
	leaq	1(%rax), %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2
	addq	$8, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	movq	%rax, %rdi
	jmp	__GI_memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI___strdup, .-__GI___strdup
	.globl	__strdup
	.set	__strdup,__GI___strdup
	.weak	strdup
	.set	strdup,__strdup
