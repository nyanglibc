	.text
	.p2align 4,,15
	.globl	__basename
	.hidden	__basename
	.type	__basename, @function
__basename:
	pushq	%rbx
	movl	$47, %esi
	movq	%rdi, %rbx
	call	strrchr
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	movq	%rbx, %rax
	popq	%rbx
	cmovne	%rdx, %rax
	ret
	.size	__basename, .-__basename
	.weak	basename
	.hidden	basename
	.set	basename,__basename
	.hidden	strrchr
