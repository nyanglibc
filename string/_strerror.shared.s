	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unknown error "
.LC1:
	.string	"%s%d"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___strerror_r
	.hidden	__GI___strerror_r
	.type	__GI___strerror_r, @function
__GI___strerror_r:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	call	__get_errlist
	testq	%rax, %rax
	movl	$5, %edx
	je	.L5
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movq	%rax, %rsi
	jmp	__GI___dcgettext
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rdx
	movl	%ebx, %r8d
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	__GI___snprintf
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI___strerror_r, .-__GI___strerror_r
	.weak	strerror_r
	.set	strerror_r,__GI___strerror_r
	.globl	__strerror_r
	.set	__strerror_r,__GI___strerror_r
	.hidden	__get_errlist
