	.text
	.p2align 4,,15
	.globl	__strsep
	.hidden	__strsep
	.type	__strsep, @function
__strsep:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	call	strcspn
	addq	%rbx, %rax
	cmpb	$0, (%rax)
	jne	.L9
	movq	$0, 0(%rbp)
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, 0(%rbp)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__strsep, .-__strsep
	.globl	__strsep_g
	.hidden	__strsep_g
	.set	__strsep_g,__strsep
	.weak	strsep
	.set	strsep,__strsep
	.hidden	strcspn
