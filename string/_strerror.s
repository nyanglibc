	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unknown error "
.LC1:
	.string	"%s%d"
	.text
	.p2align 4,,15
	.globl	__strerror_r
	.hidden	__strerror_r
	.type	__strerror_r, @function
__strerror_r:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	call	__get_errlist
	testq	%rax, %rax
	movl	$5, %edx
	je	.L5
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	%rax, %rsi
	jmp	__dcgettext
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC0(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext
	leaq	.LC1(%rip), %rdx
	movl	%ebx, %r8d
	movq	%rax, %rcx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	__snprintf
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__strerror_r, .-__strerror_r
	.weak	strerror_r
	.set	strerror_r,__strerror_r
	.hidden	__snprintf
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__get_errlist
