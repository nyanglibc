	.text
	.p2align 4,,15
	.globl	__strdup
	.hidden	__strdup
	.type	__strdup, @function
__strdup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	strlen
	leaq	1(%rax), %rbx
	movq	%rbx, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L1:
	addq	$8, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__strdup, .-__strdup
	.weak	strdup
	.set	strdup,__strdup
	.hidden	__rtld_malloc
	.hidden	strlen
