	.text
	.p2align 4,,15
	.globl	strcoll
	.hidden	strcoll
	.type	strcoll, @function
strcoll:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
	jmp	__strcoll_l
	.size	strcoll, .-strcoll
	.hidden	__strcoll_l
