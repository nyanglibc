	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__memccpy
	.type	__memccpy, @function
__memccpy:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbx
	movl	%edx, %esi
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%rcx, %rbp
	call	__GI_memchr
	testq	%rax, %rax
	je	.L2
	subq	%rbx, %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	1(%rax), %rdx
	jmp	__GI_mempcpy@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__GI_memcpy@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	__memccpy, .-__memccpy
	.weak	memccpy
	.set	memccpy,__memccpy
