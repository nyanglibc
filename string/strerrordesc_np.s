	.text
	.p2align 4,,15
	.globl	__strerrordesc_np
	.type	__strerrordesc_np, @function
__strerrordesc_np:
	jmp	__get_errlist
	.size	__strerrordesc_np, .-__strerrordesc_np
	.weak	strerrordesc_np
	.set	strerrordesc_np,__strerrordesc_np
	.hidden	__get_errlist
