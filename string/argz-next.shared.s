	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___argz_next
	.hidden	__GI___argz_next
	.type	__GI___argz_next, @function
__GI___argz_next:
	testq	%rdx, %rdx
	je	.L2
	pushq	%rbp
	leaq	(%rdi,%rsi), %rbp
	pushq	%rbx
	xorl	%eax, %eax
	subq	$8, %rsp
	cmpq	%rdx, %rbp
	jbe	.L1
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	__GI_strlen@PLT
	leaq	1(%rbx,%rax), %rax
	cmpq	%rax, %rbp
	jbe	.L11
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rsi, %rsi
	movq	%rdx, %rax
	cmovne	%rdi, %rax
	ret
.L11:
	xorl	%eax, %eax
	jmp	.L1
	.size	__GI___argz_next, .-__GI___argz_next
	.weak	__GI_argz_next
	.hidden	__GI_argz_next
	.set	__GI_argz_next,__GI___argz_next
	.weak	argz_next
	.set	argz_next,__GI_argz_next
	.globl	__argz_next
	.set	__argz_next,__GI___argz_next
