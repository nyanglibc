	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___stpncpy
	.hidden	__GI___stpncpy
	.type	__GI___stpncpy, @function
__GI___stpncpy:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rsi
	movq	%rdi, %rbx
	movq	%r13, %rdi
	movq	%rdx, %rbp
	subq	$8, %rsp
	call	__GI___strnlen
	movq	%rbx, %rdi
	movq	%rax, %r12
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	__GI_memcpy@PLT
	cmpq	%r12, %rbp
	leaq	(%rbx,%r12), %rdi
	je	.L1
	addq	$8, %rsp
	movq	%rbp, %rdx
	xorl	%esi, %esi
	popq	%rbx
	subq	%r12, %rdx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI_memset@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___stpncpy, .-__GI___stpncpy
	.weak	stpncpy
	.set	stpncpy,__GI___stpncpy
	.globl	__stpncpy
	.set	__stpncpy,__GI___stpncpy
