	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unknown error "
.LC1:
	.string	"%s%d"
	.text
	.p2align 4,,15
	.globl	__strerror_l
	.hidden	__strerror_l
	.type	__strerror_l, @function
__strerror_l:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movl	%edi, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%fs:0(%rbp), %r13d
	call	__get_errlist
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L8
	movq	%r14, %rdi
	call	__uselocale
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	%rax, %r12
	movq	%rbx, %rsi
	movl	$5, %edx
	call	__dcgettext
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	__uselocale
.L5:
	movl	%r13d, %fs:0(%rbp)
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%fs:16, %r15
	movq	2432(%r15), %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	__uselocale
	leaq	.LC0(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %r14
	call	__dcgettext
	movq	%r14, %rdi
	movq	%rax, 8(%rsp)
	call	__uselocale
	movq	8(%rsp), %rdx
	leaq	2432(%r15), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	movl	%r12d, %ecx
	call	__asprintf
	cmpl	$-1, %eax
	je	.L3
	movq	2432(%r15), %rbx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$0, 2432(%r15)
	jmp	.L5
	.size	__strerror_l, .-__strerror_l
	.weak	strerror_l
	.set	strerror_l,__strerror_l
	.hidden	__asprintf
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__uselocale
	.hidden	__get_errlist
