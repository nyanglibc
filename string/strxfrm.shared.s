	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	strxfrm
	.type	strxfrm, @function
strxfrm:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI___strxfrm_l
	.size	strxfrm, .-strxfrm
