	.text
	.p2align 4,,15
	.globl	bcopy
	.type	bcopy, @function
bcopy:
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmp	memmove
	.size	bcopy, .-bcopy
	.hidden	memmove
