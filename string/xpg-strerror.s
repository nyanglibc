	.text
	.p2align 4,,15
	.globl	__xpg_strerror_r
	.type	__xpg_strerror_r, @function
__xpg_strerror_r:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	__strerror_r
	cmpq	%rax, %rbp
	je	.L5
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen
	testq	%rbx, %rbx
	movq	%rax, %r13
	jne	.L3
.L4:
	addq	$8, %rsp
	movl	$34, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	-1(%rbx), %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	call	__mempcpy@PLT
	cmpq	%r13, %rbx
	movb	$0, (%rax)
	jbe	.L4
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	movl	$22, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__xpg_strerror_r, .-__xpg_strerror_r
	.hidden	strlen
	.hidden	__strerror_r
