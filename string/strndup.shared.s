	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strndup
	.hidden	__GI___strndup
	.type	__GI___strndup, @function
__GI___strndup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__GI___strnlen
	leaq	1(%rax), %rdi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1
	movb	$0, (%rax,%rbx)
	addq	$8, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI___strndup, .-__GI___strndup
	.globl	__strndup
	.set	__strndup,__GI___strndup
	.weak	strndup
	.set	strndup,__strndup
