	.text
	.p2align 4,,15
	.globl	__argz_next
	.hidden	__argz_next
	.type	__argz_next, @function
__argz_next:
	testq	%rdx, %rdx
	je	.L2
	pushq	%rbp
	leaq	(%rdi,%rsi), %rbp
	pushq	%rbx
	xorl	%eax, %eax
	subq	$8, %rsp
	cmpq	%rdx, %rbp
	jbe	.L1
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rax
	cmpq	%rax, %rbp
	jbe	.L11
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rsi, %rsi
	movq	%rdx, %rax
	cmovne	%rdi, %rax
	ret
.L11:
	xorl	%eax, %eax
	jmp	.L1
	.size	__argz_next, .-__argz_next
	.weak	argz_next
	.hidden	argz_next
	.set	argz_next,__argz_next
