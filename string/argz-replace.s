	.text
	.p2align 4,,15
	.type	str_append, @function
str_append:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	(%rsi), %r13
	movq	(%rdi), %rdi
	addq	%rcx, %r13
	leaq	1(%r13), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2
	movq	0(%rbp), %rdi
	movq	%rax, %r12
	movq	%r14, %rdx
	movq	%r15, %rsi
	addq	%rax, %rdi
	call	__mempcpy@PLT
	movb	$0, (%rax)
	movq	%r12, (%rbx)
	movq	%r13, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rbx), %rdi
	call	free@PLT
	movq	$0, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	str_append, .-str_append
	.p2align 4,,15
	.globl	__argz_replace
	.hidden	__argz_replace
	.type	__argz_replace, @function
__argz_replace:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$120, %rsp
	testq	%rdx, %rdx
	je	.L6
	cmpb	$0, (%rdx)
	je	.L6
	movq	(%rdi), %rax
	movq	%rcx, %r13
	movq	%rdi, 64(%rsp)
	movq	%rdx, %rdi
	movq	%r8, 48(%rsp)
	movq	%rdx, (%rsp)
	movq	%rsi, 72(%rsp)
	movq	$0, 80(%rsp)
	leaq	96(%rsp), %r14
	movq	%rax, 8(%rsp)
	movq	(%rsi), %rax
	xorl	%ebx, %ebx
	movq	$0, 88(%rsp)
	movq	%rax, 24(%rsp)
	call	strlen
	movq	%r13, %rdi
	movq	%rax, 40(%rsp)
	call	strlen
	movq	%rax, %r15
	leaq	88(%rsp), %rax
	movl	$1, 20(%rsp)
	movq	%rax, 56(%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rsp), %rbp
	addq	24(%rsp), %rbp
.L21:
	testq	%rbx, %rbx
	je	.L23
	cmpq	%rbx, %rbp
	ja	.L54
.L24:
	movl	20(%rsp), %eax
	xorl	%ebp, %ebp
	testl	%eax, %eax
	je	.L55
.L6:
	addq	$120, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
	cmpq	%rbx, %rbp
	jbe	.L24
.L25:
	testq	%rbx, %rbx
	je	.L24
	movq	(%rsp), %rsi
	movq	%rbx, %rdi
	call	strstr
	testq	%rax, %rax
	jne	.L56
	movl	20(%rsp), %edx
	testl	%edx, %edx
	jne	.L21
	leaq	80(%rsp), %r12
	movq	56(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	__argz_add
	movl	%eax, %ebp
.L20:
	testl	%ebp, %ebp
	je	.L8
	cmpq	$0, 88(%rsp)
	je	.L6
	movq	80(%rsp), %rdi
	call	free@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L56:
	movq	40(%rsp), %rcx
	movq	%rbx, %rdi
	leaq	104(%rsp), %r12
	leaq	(%rax,%rcx), %rbp
	subq	%rbx, %rax
	movq	%rax, %rsi
	movq	%rax, 96(%rsp)
	call	__strndup
	movq	%rbx, 32(%rsp)
	movq	%rax, 104(%rsp)
	movq	%rbp, %rbx
.L10:
	testq	%rbx, %rbx
	setne	%bpl
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%rax, %rax
	je	.L33
	testb	%bpl, %bpl
	je	.L33
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	str_append
	movq	104(%rsp), %rax
	testq	%rax, %rax
	je	.L11
	movq	(%rsp), %rsi
	movq	%rbx, %rdi
	call	strstr
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L12
	movq	%rax, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	subq	%rbx, %rcx
	movq	%r12, %rdi
	call	str_append
	movq	40(%rsp), %rax
	leaq	0(%rbp,%rax), %rbx
.L13:
	movq	104(%rsp), %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L33:
	testq	%rax, %rax
	movq	32(%rsp), %rbx
	movl	$12, %ebp
	je	.L16
	cmpq	%rbx, 8(%rsp)
	leaq	80(%rsp), %r12
	jnb	.L17
	movl	20(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L57
.L17:
	movq	56(%rsp), %rsi
	movq	%rax, %rdx
	movq	%r12, %rdi
	call	__argz_add
	movl	%eax, %ebp
.L18:
	movq	104(%rsp), %rdi
	call	free@PLT
	movl	$0, 20(%rsp)
.L16:
	movq	48(%rsp), %rax
	testq	%rax, %rax
	je	.L20
	addl	$1, (%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$0, 24(%rsp)
	je	.L24
	movq	8(%rsp), %rbx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rdi
	call	strlen
	movq	%rbx, %rdx
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	str_append
	jmp	.L13
.L55:
	movq	8(%rsp), %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	movq	80(%rsp), %rax
	movq	64(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	88(%rsp), %rax
	movq	72(%rsp), %rcx
	movq	%rax, (%rcx)
	jmp	.L6
.L57:
	movq	8(%rsp), %rdx
	movq	56(%rsp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	subq	%rdx, %rcx
	call	__argz_append
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L18
	movq	104(%rsp), %rax
	jmp	.L17
	.size	__argz_replace, .-__argz_replace
	.weak	argz_replace
	.set	argz_replace,__argz_replace
	.hidden	__argz_append
	.hidden	__strndup
	.hidden	__argz_add
	.hidden	strstr
	.hidden	strlen
