	.text
	.p2align 4,,15
	.type	base64, @function
base64:
	cmpl	$25, %edi
	jbe	.L12
	cmpl	$51, %edi
	jbe	.L13
	cmpl	$61, %edi
	jbe	.L14
	cmpl	$62, %edi
	je	.L6
	cmpl	$63, %edi
	jne	.L15
	movl	$47, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leal	71(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	65(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leal	-4(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$43, %eax
	ret
.L15:
	subq	$8, %rsp
	call	abort@PLT
	.size	base64, .-base64
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-7//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L17
	movabsq	$25769803777, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	32(%rax), %rsi
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L20
	movabsq	$17179869188, %rdx
	movabsq	$25769803777, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.section	.rodata.str1.1
.LC5:
	.string	"wc1 >= 0xd800 && wc1 < 0xdc00"
.LC6:
	.string	"wc2 >= 0xdc00 && wc2 < 0xe000"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC8:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC9:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC10:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC11:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC12:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC14:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rbx
	movq	%rcx, %rbp
	subq	$216, %rsp
	movq	%rbx, 80(%rsp)
	leaq	48(%rsi), %rbx
	movq	%rdi, 48(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	movq	%rbx, 88(%rsp)
	movl	16(%rsi), %ebx
	movq	%r9, 56(%rsp)
	movl	272(%rsp), %r14d
	movq	$0, 64(%rsp)
	testb	$1, %bl
	movl	%ebx, 40(%rsp)
	jne	.L22
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 64(%rsp)
	je	.L22
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 64(%rsp)
.L22:
	testl	%r14d, %r14d
	jne	.L531
	movq	16(%rsp), %rax
	movq	32(%rsp), %rcx
	leaq	144(%rsp), %rdx
	movl	280(%rsp), %r9d
	movq	8(%r12), %r13
	movq	32(%r12), %r15
	movq	(%rax), %rax
	testq	%rcx, %rcx
	movq	%rax, 24(%rsp)
	movq	%rcx, %rax
	cmove	%r12, %rax
	cmpq	$0, 56(%rsp)
	movq	(%rax), %rax
	movq	$0, 144(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%r9d, %r9d
	movq	%rax, 112(%rsp)
	jne	.L532
.L43:
	movq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r15), %rax
	movq	%rax, 72(%rsp)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L533
	leaq	184(%rsp), %rsi
	movq	(%rsp), %rcx
	movq	24(%rsp), %rax
	movl	$4, 8(%rsp)
	movq	%rsi, 96(%rsp)
	leaq	176(%rsp), %rsi
	movq	%rax, 176(%rsp)
	movq	%rcx, 184(%rsp)
	movq	%rcx, %r13
	movq	%rsi, 104(%rsp)
.L164:
	cmpq	%rax, %rbp
	je	.L165
.L197:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L318
	cmpq	%r13, %r14
	jbe	.L354
	movl	(%r15), %r8d
	movl	(%rax), %ebx
	movl	%r8d, %edx
	andl	$24, %edx
	jne	.L166
	cmpl	$127, %ebx
	ja	.L167
	movl	%ebx, %edx
	leaq	direct_tab(%rip), %rsi
	shrl	$3, %edx
	movzbl	(%rsi,%rdx), %ecx
	movl	%ebx, %edx
	andl	$7, %edx
	btl	%edx, %ecx
	jc	.L168
	cmpl	$43, %ebx
	jne	.L320
	leaq	2(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L354
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$43, 0(%r13)
.L288:
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$45, (%rax)
	.p2align 4,,10
	.p2align 3
.L171:
	movq	176(%rsp), %rax
	movq	184(%rsp), %r13
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 176(%rsp)
	jne	.L197
	.p2align 4,,10
	.p2align 3
.L165:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rcx
	movq	%rax, (%rcx)
	jne	.L534
.L198:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L535
	cmpq	%r13, (%rsp)
	jnb	.L330
	movq	64(%rsp), %rbx
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	%rax, 152(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	72(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%rbx
	movl	%eax, %ebx
	cmpl	$4, %ebx
	popq	%rax
	popq	%rdx
	je	.L202
	movq	152(%rsp), %r11
	cmpq	%r13, %r11
	jne	.L536
.L201:
	testl	%ebx, %ebx
	jne	.L348
.L275:
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)
	movl	16(%r12), %eax
	movl	%eax, 40(%rsp)
	movq	(%r12), %rax
	movq	%rax, (%rsp)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$127, %ebx
	ja	.L178
	movl	%ebx, %esi
	leaq	direct_tab(%rip), %rdi
	movl	%ebx, %ecx
	shrl	$3, %esi
	andl	$7, %ecx
	movzbl	(%rdi,%rsi), %edi
	btl	%ecx, %edi
	jnc	.L180
	leaq	xbase64_tab(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	xorl	%edi, %edi
	sarl	%cl, %esi
	movl	%esi, %ecx
	andl	$1, %esi
	cmpl	$15, %edx
	setg	%dil
	andl	$1, %ecx
	leal	1(%rdi,%rcx), %ecx
	movslq	%ecx, %rcx
	addq	%r13, %rcx
	cmpq	%rcx, %r14
	jb	.L354
	cmpl	$15, %edx
	leaq	1(%r13), %rax
	jg	.L537
.L182:
	testb	%sil, %sil
	je	.L183
	movq	%rax, 184(%rsp)
	movb	$45, 0(%r13)
	movq	184(%rsp), %r13
	leaq	1(%r13), %rax
.L183:
	movq	%rax, 184(%rsp)
	movb	%bl, 0(%r13)
	movl	$0, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L178:
	cmpl	$65535, %ebx
	jbe	.L180
	cmpl	$1114111, %ebx
	ja	.L186
	cmpl	$24, %edx
	sete	%dl
	movzbl	%dl, %edx
	leaq	5(%r13,%rdx), %rdx
	cmpq	%rdx, %r14
	jb	.L354
.L290:
	leal	-65536(%rbx), %eax
	movl	%ebx, %edx
	sarl	$3, %r8d
	andl	$1023, %edx
	shrl	$10, %eax
	addl	$56320, %edx
	leal	55296(%rax), %edi
	movl	%r8d, %eax
	movl	%edx, %ebx
	andl	$3, %eax
	sall	$16, %edi
	orl	%edi, %ebx
	cmpl	$2, %eax
	je	.L194
	cmpl	$3, %eax
	je	.L195
	cmpl	$1, %eax
	je	.L538
.L123:
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	cmpl	$65535, %ebx
	jbe	.L320
	cmpl	$1114111, %ebx
	jbe	.L321
.L186:
	cmpq	$0, 112(%rsp)
	je	.L328
	testb	$8, 16(%r12)
	jne	.L539
.L188:
	testb	$2, 40(%rsp)
	jne	.L540
.L328:
	movl	$6, 8(%rsp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L180:
	cmpl	$15, %edx
	jle	.L541
	leaq	3(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L354
	cmpl	$65535, %ebx
	ja	.L190
.L285:
	sarl	$3, %r8d
	movl	%r8d, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L191
	cmpl	$3, %eax
	je	.L192
	cmpl	$1, %eax
	jne	.L123
.L494:
	leaq	1(%r13), %rax
	movl	%ebx, %edi
	shrl	$10, %edi
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	sall	$5, %ebx
	shrl	$4, %edi
	andl	$480, %ebx
	andl	$63, %edi
	leaq	1(%r13), %rax
	orl	$24, %ebx
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movl	%ebx, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	2(%r13), %rdx
	cmpq	%rdx, %r14
	jnb	.L285
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$5, 8(%rsp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L533:
	movq	24(%rsp), %rdx
	cmpq	%rdx, %rbp
	je	.L306
	movq	(%rsp), %r13
	leaq	4(%r13), %r8
	cmpq	%r14, %r8
	ja	.L307
	movl	40(%rsp), %r10d
	movl	$4, 8(%rsp)
	movl	$6, %ebx
	andl	$2, %r10d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L542:
	movzbl	%al, %ecx
	cmpl	$127, %ecx
	ja	.L143
	movl	%ecx, %esi
	leaq	xdirect_tab(%rip), %rdi
	shrl	$3, %esi
	movzbl	(%rdi,%rsi), %edi
	movl	%eax, %esi
	andl	$7, %esi
	btl	%esi, %edi
	jnc	.L143
	movl	%ecx, 0(%r13)
	addq	$1, %rdx
	movq	%r8, %r13
.L147:
	cmpq	%rdx, %rbp
	je	.L140
.L162:
	leaq	4(%r13), %r8
	cmpq	%r8, %r14
	jb	.L308
.L141:
	movl	(%r15), %esi
	movzbl	(%rdx), %eax
	sarl	$3, %esi
	testl	%esi, %esi
	je	.L542
	leal	-65(%rax), %ecx
	movl	4(%r15), %edi
	cmpb	$25, %cl
	ja	.L149
	subl	$65, %eax
.L150:
	cmpl	$6, %esi
	jle	.L156
.L546:
	leal	-6(%rsi), %ecx
	sall	%cl, %eax
	orl	%eax, %edi
	leal	-17(%rsi), %eax
	cmpl	$5, %eax
	ja	.L157
	movl	%edi, %eax
	shrl	$16, %eax
	leal	-55296(%rax), %r9d
	cmpl	$1023, %r9d
	ja	.L543
.L158:
	movl	%edi, 4(%r15)
.L159:
	addq	$1, %rdx
	sall	$3, %ecx
	cmpq	%rdx, %rbp
	movl	%ecx, (%r15)
	jne	.L162
.L140:
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	.p2align 4,,10
	.p2align 3
.L549:
	cmpq	$0, 32(%rsp)
	je	.L198
.L534:
	movq	32(%rsp), %rax
	movq	%r13, (%rax)
.L21:
	movl	8(%rsp), %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	cmpb	$43, %al
	jne	.L544
	leaq	2(%rdx), %rax
	cmpq	%rax, %rbp
	jb	.L309
	cmpb	$45, 1(%rdx)
	je	.L545
	addq	$1, %rdx
	movq	$256, (%r15)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L149:
	leal	-97(%rax), %ecx
	cmpb	$25, %cl
	ja	.L151
	subl	$71, %eax
	cmpl	$6, %esi
	jg	.L546
.L156:
	movl	%ebx, %ecx
	movl	%eax, %r9d
	subl	%esi, %ecx
	shrl	%cl, %r9d
	movzwl	%di, %ecx
	shrl	$16, %edi
	orl	%ecx, %r9d
	movl	%esi, %ecx
	subl	$55296, %edi
	sall	%cl, %eax
	leal	26(%rsi), %ecx
	sall	$26, %eax
	cmpl	$1023, %edi
	movl	%eax, 4(%r15)
	ja	.L236
	leal	-56320(%r9), %eax
	cmpl	$1023, %eax
	ja	.L237
	sall	$10, %edi
	leal	9216(%r9,%rdi), %eax
	movl	%eax, 0(%r13)
	movq	%r8, %r13
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L202:
	movl	8(%rsp), %ebx
	cmpl	$5, %ebx
	jne	.L201
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	%bl, 0(%r13)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$3, %edx
.L170:
	addq	%r13, %rdx
	cmpq	%rdx, %r14
	jb	.L354
	leaq	1(%r13), %rax
	cmpl	$43, %ebx
	movq	%rax, 184(%rsp)
	movb	$43, 0(%r13)
	je	.L288
	cmpl	$65535, %ebx
	movq	184(%rsp), %r13
	jbe	.L494
	cmpl	$1114111, %ebx
	ja	.L123
	movl	%ebx, %eax
	subl	$65536, %ebx
	shrl	$10, %ebx
	andl	$1023, %eax
	leal	55296(%rbx), %ebx
	addl	$56320, %eax
	sall	$16, %ebx
	orl	%eax, %ebx
	movq	184(%rsp), %rax
	movl	%ebx, %edi
	shrl	$20, %edi
	leaq	1(%rax), %rdx
	andl	$63, %edi
	movq	%rdx, 184(%rsp)
	movb	$50, (%rax)
	movq	184(%rsp), %r13
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$14, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
.L495:
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$8, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	sall	$7, %ebx
	shrl	$2, %edi
	andl	$384, %ebx
	andl	$63, %edi
	leaq	1(%r13), %rax
	orl	$16, %ebx
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movl	%ebx, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L318:
	movl	$7, 8(%rsp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L151:
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L547
	cmpb	$43, %al
	je	.L312
	cmpb	$47, %al
	je	.L313
	cmpl	$26, %esi
	jle	.L515
	testl	%edi, %edi
	jne	.L515
	cmpb	$45, %al
	movl	$0, (%r15)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rdx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L157:
	subl	$11, %esi
	cmpl	$5, %esi
	ja	.L158
	movzwl	%di, %eax
	subl	$56320, %eax
	cmpl	$1023, %eax
	jbe	.L158
.L515:
	cmpq	$0, 112(%rsp)
	je	.L317
	testl	%r10d, %r10d
	jne	.L548
.L317:
	movq	16(%rsp), %rax
	movl	$6, 8(%rsp)
	movq	%rdx, (%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$43, 0(%r13)
	movq	%rax, %rdx
	movq	%r8, %r13
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L308:
	movq	16(%rsp), %rax
	movl	$5, 8(%rsp)
	movq	%rdx, (%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L532:
	movl	(%r15), %edx
	movl	%edx, %eax
	andl	$7, %eax
	je	.L43
	testq	%rcx, %rcx
	jne	.L550
	movq	48(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L551
	movq	24(%rsp), %rsi
	cmpl	$4, %eax
	movq	%rsi, 160(%rsp)
	movq	(%rsp), %rsi
	movq	%rsi, 168(%rsp)
	ja	.L81
	leaq	140(%rsp), %r10
	cltq
	xorl	%r14d, %r14d
.L82:
	movzbl	4(%r15,%r14), %ecx
	movb	%cl, (%r10,%r14)
	addq	$1, %r14
	cmpq	%r14, %rax
	jne	.L82
	movq	24(%rsp), %rax
	subq	%r14, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L552
	cmpq	%r13, (%rsp)
	jnb	.L105
	movq	24(%rsp), %rax
	leaq	139(%rsp), %rdi
	addq	$1, %rax
.L90:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r14
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %r14
	movb	%cl, (%rdi,%r14)
	ja	.L359
	cmpq	%rsi, %rbp
	ja	.L90
.L359:
	movl	%edx, %eax
	movq	%r10, 160(%rsp)
	leaq	(%r10,%r14), %r11
	andl	$24, %eax
	movl	140(%rsp), %r8d
	je	.L553
	cmpl	$127, %r8d
	ja	.L109
	movl	%r8d, %esi
	leaq	direct_tab(%rip), %rdi
	movl	%r8d, %ecx
	shrl	$3, %esi
	andl	$7, %ecx
	movzbl	(%rdi,%rsi), %edi
	btl	%ecx, %edi
	jnc	.L111
	leaq	xbase64_tab(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	sarl	%cl, %esi
	movl	%esi, %ebx
	movl	%esi, %ecx
	xorl	%esi, %esi
	andl	$1, %ebx
	cmpl	$15, %eax
	setg	%sil
	andl	$1, %ecx
	leal	1(%rsi,%rcx), %ecx
	movq	(%rsp), %rsi
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	cmpq	%rcx, %r13
	jb	.L105
	cmpl	$15, %eax
	movq	%rsi, %rcx
	leaq	1(%rsi), %rsi
	jle	.L113
	sarl	$3, %edx
	movq	%r10, 24(%rsp)
	movl	%r8d, 8(%rsp)
	movl	%edx, %edi
	movq	%rsi, 168(%rsp)
	andl	$-4, %edi
	call	base64
	movq	(%rsp), %rsi
	movq	24(%rsp), %r10
	movl	8(%rsp), %r8d
	movb	%al, (%rsi)
	movq	168(%rsp), %rcx
	leaq	1(%rcx), %rsi
.L113:
	testb	%bl, %bl
	je	.L114
	movq	%rsi, 168(%rsp)
	movb	$45, (%rcx)
	movq	168(%rsp), %rcx
	leaq	1(%rcx), %rsi
.L114:
	movq	%rsi, 168(%rsp)
	movb	%r8b, (%rcx)
	movl	$0, (%r15)
.L97:
	movq	160(%rsp), %rax
	addq	$4, %rax
	cmpq	%r10, %rax
	movq	%rax, 160(%rsp)
	je	.L491
.L130:
	movl	(%r15), %edx
	subq	%r10, %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L554
	movq	16(%rsp), %rbx
	subq	%rcx, %rax
	andl	$-8, %edx
	addq	(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%rax, 24(%rsp)
	movq	168(%rsp), %rax
	movl	%edx, (%r15)
	movq	%rax, (%rsp)
	movl	16(%r12), %eax
	movl	%eax, 40(%rsp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L321:
	movl	$6, %edx
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L547:
	addl	$4, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L543:
	movl	%eax, 0(%r13)
	sall	$16, %edi
	leal	10(%rsi), %ecx
	movq	%r8, %r13
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	1(%r13), %rax
	movl	%ebx, %edi
	andl	$-4, %r8d
	shrl	$14, %edi
	movq	%rax, 184(%rsp)
	orl	%r8d, %edi
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L190:
	cmpl	$1114111, %ebx
	jbe	.L290
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L195:
	movl	%r8d, %edi
	leaq	1(%r13), %rax
	orl	$3, %edi
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$24, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$18, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$12, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
.L496:
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$6, %edi
	andl	$63, %ebx
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movl	$8, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	1(%r13), %rax
	movl	%ebx, %edi
	shrl	$20, %edi
	movq	%rax, 184(%rsp)
	movb	$50, 0(%r13)
	andl	$63, %edi
	movq	184(%rsp), %r13
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$14, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$8, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$2, %edi
	sall	$7, %ebx
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movl	%ebx, %edi
	movb	%al, 0(%r13)
	andl	$384, %edi
	orl	$16, %edi
	movl	%edi, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L535:
	movq	56(%rsp), %rbx
	movq	%r13, (%r12)
	movq	144(%rsp), %rax
	addq	%rax, (%rbx)
.L200:
	cmpl	$7, 8(%rsp)
	jne	.L21
	movl	280(%rsp), %eax
	testl	%eax, %eax
	je	.L21
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L278
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L280
.L279:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L279
.L280:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L539:
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rcx
	popq	%rsi
	movq	176(%rsp), %rax
	movq	184(%rsp), %r13
	je	.L188
	cmpl	$5, 8(%rsp)
	jne	.L164
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L330:
	movl	8(%rsp), %ebx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L194:
	andl	$-4, %r8d
	leaq	1(%r13), %rax
	movl	%r8d, %edi
	orl	$13, %edi
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$22, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movl	%ebx, %edi
	shrl	$16, %edi
	andl	$63, %edi
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %rax
	movl	%ebx, %edi
	shrl	$4, %edi
	sall	$5, %ebx
	andl	$63, %edi
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$51, (%rax)
	movq	184(%rsp), %r13
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	call	base64
	movl	%ebx, %edi
	movb	%al, 0(%r13)
	andl	$480, %edi
	orl	$24, %edi
	movl	%edi, (%r15)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	1(%r13), %rax
	movl	%ebx, %edi
	andl	$-4, %r8d
	shrl	$12, %edi
	movq	%rax, 184(%rsp)
	orl	%r8d, %edi
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L540:
	movq	112(%rsp), %rbx
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 176(%rsp)
	addq	$1, (%rbx)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L537:
	movl	%r8d, %edi
	movb	%sil, 120(%rsp)
	movq	%rax, 184(%rsp)
	sarl	$3, %edi
	andl	$-4, %edi
	call	base64
	movb	%al, 0(%r13)
	movq	184(%rsp), %r13
	movzbl	120(%rsp), %esi
	leaq	1(%r13), %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L544:
	cmpq	$0, 112(%rsp)
	je	.L317
	testl	%r10d, %r10d
	je	.L317
.L493:
	movq	112(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L309:
	movq	16(%rsp), %rax
	movl	$7, 8(%rsp)
	movq	%rdx, (%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L536:
	movq	16(%rsp), %rax
	movq	24(%rsp), %rcx
	movq	%rcx, (%rax)
	movq	72(%rsp), %rax
	movq	%rax, (%r15)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	movl	16(%r12), %eax
	je	.L555
	leaq	200(%rsp), %rcx
	movl	%eax, 40(%rsp)
	movq	(%rsp), %rsi
	movq	24(%rsp), %rax
	movl	$4, 8(%rsp)
	movq	%r11, %r13
	movq	%rcx, 72(%rsp)
	leaq	192(%rsp), %rcx
	movq	%rsi, 200(%rsp)
	movq	%rax, 192(%rsp)
	movq	%rcx, 96(%rsp)
.L240:
	cmpq	%rax, %rbp
	je	.L556
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L334
	cmpq	%rsi, %r13
	jbe	.L484
	movl	(%r15), %edi
	movl	(%rax), %edx
	movl	%edi, %r8d
	andl	$24, %r8d
	jne	.L242
	cmpl	$127, %edx
	ja	.L243
	movl	%edx, %ecx
	leaq	direct_tab(%rip), %rdi
	shrl	$3, %ecx
	movzbl	(%rdi,%rcx), %edi
	movl	%edx, %ecx
	andl	$7, %ecx
	btl	%ecx, %edi
	jc	.L244
	cmpl	$43, %edx
	jne	.L336
	leaq	2(%rsi), %rdx
	cmpq	%rdx, %r13
	jb	.L484
	leaq	1(%rsi), %rax
	movq	%rax, 200(%rsp)
	movb	$43, (%rsi)
.L291:
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$45, (%rax)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$63, %eax
	jmp	.L150
.L551:
	cmpl	$4, %eax
	ja	.L46
	cltq
	leaq	200(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rax, %rcx
.L47:
	movzbl	4(%r15,%rsi), %r8d
	movb	%r8b, (%rdi,%rsi)
	addq	$1, %rsi
	cmpq	%rax, %rsi
	jne	.L47
	movq	(%rsp), %rsi
	leaq	4(%rsi), %r9
	cmpq	%r9, %r13
	jb	.L105
	movq	24(%rsp), %rsi
	leaq	199(%rsp), %r8
.L48:
	addq	$1, %rsi
	movzbl	-1(%rsi), %r10d
	addq	$1, %rcx
	cmpq	$5, %rcx
	movb	%r10b, (%r8,%rcx)
	ja	.L357
	cmpq	%rsi, %rbp
	ja	.L48
.L357:
	movl	%edx, %esi
	movzbl	200(%rsp), %r10d
	sarl	$3, %esi
	testl	%esi, %esi
	jne	.L51
	movzbl	%r10b, %r11d
	cmpl	$127, %r11d
	ja	.L52
	movl	%r11d, %esi
	leaq	xdirect_tab(%rip), %r8
	shrl	$3, %esi
	movzbl	(%r8,%rsi), %r8d
	movl	%r10d, %esi
	andl	$7, %esi
	btl	%esi, %r8d
	jnc	.L52
	movq	(%rsp), %rax
	leaq	1(%rdi), %rdx
	movq	%r9, (%rsp)
	movl	%r11d, (%rax)
	movl	(%r15), %r14d
	movl	%r14d, %eax
	andl	$7, %eax
.L56:
	subq	%rdi, %rdx
	cmpq	%rax, %rdx
	jle	.L557
	movq	16(%rsp), %rbx
	subq	%rax, %rdx
	movq	(%rbx), %rax
	addq	%rdx, %rax
	movq	%rax, (%rbx)
	movq	%rax, 24(%rsp)
	movl	%r14d, %eax
	andl	$-8, %eax
	movl	%eax, (%r15)
	movl	16(%r12), %eax
	movl	%eax, 40(%rsp)
	jmp	.L43
.L555:
	cmpq	%rcx, %rbp
	je	.L558
	movq	(%rsp), %r8
	andl	$2, %eax
	movl	$4, %r13d
	movl	$6, %r10d
	movl	%eax, 40(%rsp)
	leaq	4(%r8), %rdi
	cmpq	%rdi, %r11
	jb	.L559
	movq	24(%rsp), %rsi
	movl	%r13d, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L208:
	movl	(%r15), %edx
	movzbl	(%rsi), %eax
	sarl	$3, %edx
	testl	%edx, %edx
	jne	.L211
	movzbl	%al, %edx
	cmpl	$127, %edx
	ja	.L212
	movl	%edx, %ecx
	leaq	xdirect_tab(%rip), %r9
	shrl	$3, %ecx
	movzbl	(%r9,%rcx), %r9d
	movl	%eax, %ecx
	andl	$7, %ecx
	btl	%ecx, %r9d
	jc	.L213
.L212:
	cmpb	$43, %al
	jne	.L560
	leaq	2(%rsi), %rax
	cmpq	%rax, %rbp
	jb	.L504
	cmpb	$45, 1(%rsi)
	je	.L561
	addq	$1, %rsi
	movq	$256, (%r15)
.L216:
	cmpq	%rsi, %rbp
	je	.L562
	leaq	4(%r8), %rdi
	cmpq	%rdi, %r11
	jnb	.L208
	movq	%rsi, %rcx
	movq	152(%rsp), %rsi
	movq	16(%rsp), %rax
	cmpq	%rsi, %r8
	movq	%rcx, (%rax)
	je	.L210
.L207:
	leaq	__PRETTY_FUNCTION__.9231(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L575:
	movq	(%rsp), %rax
	addq	$2, %rax
	cmpq	%rax, %r13
	jnb	.L284
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$5, 8(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L242:
	cmpl	$127, %edx
	ja	.L254
	movl	%edx, %r9d
	leaq	direct_tab(%rip), %r11
	movl	%edx, %ecx
	shrl	$3, %r9d
	andl	$7, %ecx
	movzbl	(%r11,%r9), %r10d
	btl	%ecx, %r10d
	jnc	.L256
	leaq	xbase64_tab(%rip), %r11
	xorl	%r10d, %r10d
	movzbl	(%r11,%r9), %r9d
	sarl	%cl, %r9d
	movl	%r9d, %ecx
	andl	$1, %r9d
	andl	$1, %ecx
	cmpl	$15, %r8d
	setg	%r10b
	leal	1(%rcx,%r10), %ecx
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	cmpq	%rcx, %r13
	jb	.L484
	cmpl	$15, %r8d
	leaq	1(%rsi), %rax
	jg	.L563
.L258:
	testb	%r9b, %r9b
	je	.L259
	movq	%rax, 200(%rsp)
	movb	$45, (%rsi)
	movq	200(%rsp), %rsi
	leaq	1(%rsi), %rax
.L259:
	movq	%rax, 200(%rsp)
	movb	%dl, (%rsi)
	movl	$0, (%r15)
	.p2align 4,,10
	.p2align 3
.L247:
	movq	192(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 192(%rsp)
.L502:
	movq	200(%rsp), %rsi
	jmp	.L240
.L256:
	cmpl	$15, %r8d
	jg	.L260
	leaq	2(%rsi), %rcx
	cmpq	%rcx, %r13
	jb	.L484
.L286:
	sarl	$3, %edi
	movl	%edi, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L267
	cmpl	$3, %eax
	je	.L268
	cmpl	$1, %eax
	jne	.L123
	leaq	1(%rsi), %rax
	movl	%edx, %edi
	movq	%rsi, 104(%rsp)
	shrl	$10, %edi
	movl	%edx, 24(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rsi
	movb	%al, (%rsi)
.L499:
	movl	24(%rsp), %edx
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 104(%rsp)
	shrl	$4, %edi
	andl	$63, %edi
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rcx
	movb	%al, (%rcx)
	movl	24(%rsp), %edx
	movl	%edx, %eax
	sall	$5, %eax
	andl	$480, %eax
	orl	$24, %eax
	movl	%eax, (%r15)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L211:
	leal	-65(%rax), %ecx
	movl	4(%r15), %r9d
	cmpb	$25, %cl
	ja	.L221
	subl	$65, %eax
.L222:
	cmpl	$6, %edx
	jle	.L230
	leal	-6(%rdx), %ecx
	sall	%cl, %eax
	orl	%eax, %r9d
	leal	-17(%rdx), %eax
	cmpl	$5, %eax
	jbe	.L564
	subl	$11, %edx
	cmpl	$5, %edx
	ja	.L232
	movzwl	%r9w, %eax
	subl	$56320, %eax
	cmpl	$1023, %eax
	ja	.L522
.L232:
	movl	%r9d, 4(%r15)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$62, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L307:
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	(%rsp), %r13
	movl	$5, 8(%rsp)
	movq	%rdx, (%rax)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L531:
	cmpq	$0, 32(%rsp)
	jne	.L565
	cmpl	$1, %r14d
	movq	32(%r12), %r13
	jne	.L25
	movq	48(%rsp), %rax
	movq	(%r12), %r14
	movq	8(%r12), %rdx
	movq	0(%r13), %r15
	cmpq	$0, 96(%rax)
	je	.L566
	movl	0(%r13), %eax
	movl	%eax, %ecx
	andl	$24, %ecx
	je	.L28
	cmpl	$15, %ecx
	jg	.L29
	leaq	1(%r14), %rbx
	cmpq	%rdx, %rbx
	ja	.L105
	movq	%r14, %rbp
.L31:
	movb	$45, 0(%rbp)
	testb	$1, 16(%r12)
	movq	32(%r12), %rax
	movl	$0, (%rax)
	je	.L567
	movq	%rbx, %r14
.L32:
	movq	%r14, (%r12)
	movl	$0, 8(%rsp)
	jmp	.L21
.L254:
	cmpl	$65535, %edx
	jbe	.L256
	cmpl	$1114111, %edx
	ja	.L262
	xorl	%ecx, %ecx
	cmpl	$24, %r8d
	sete	%cl
	leaq	5(%rsi,%rcx), %rcx
	cmpq	%rcx, %r13
	jb	.L484
.L293:
	leal	-65536(%rdx), %eax
	movl	%edx, %ecx
	sarl	$3, %edi
	andl	$1023, %ecx
	shrl	$10, %eax
	addl	$56320, %ecx
	addl	$55296, %eax
	sall	$16, %eax
	orl	%eax, %ecx
	movl	%edi, %eax
	andl	$3, %eax
	movl	%ecx, 24(%rsp)
	cmpl	$2, %eax
	je	.L270
	cmpl	$3, %eax
	je	.L271
	cmpl	$1, %eax
	jne	.L123
	leaq	1(%rsi), %rax
	movq	%rax, 200(%rsp)
	movb	$50, (%rsi)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	jmp	.L500
.L243:
	cmpl	$65535, %edx
	ja	.L568
.L336:
	movl	$3, %ecx
.L246:
	addq	%rsi, %rcx
	cmpq	%rcx, %r13
	jb	.L484
	leaq	1(%rsi), %rax
	cmpl	$43, %edx
	movq	%rax, 200(%rsp)
	movb	$43, (%rsi)
	je	.L291
	cmpl	$65535, %edx
	jbe	.L569
	cmpl	$1114111, %edx
	ja	.L123
	leal	-65536(%rdx), %eax
	movl	%edx, %ecx
	andl	$1023, %ecx
	shrl	$10, %eax
	addl	$56320, %ecx
	addl	$55296, %eax
	sall	$16, %eax
	orl	%eax, %ecx
	movq	200(%rsp), %rax
	movl	%ecx, 24(%rsp)
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$50, (%rax)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
.L500:
	movl	%ecx, %edi
	shrl	$20, %edi
	andl	$63, %edi
	call	base64
	movq	104(%rsp), %rdx
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	movl	24(%rsp), %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	shrl	$14, %edi
	andl	$63, %edi
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	movl	24(%rsp), %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	shrl	$8, %edi
	andl	$63, %edi
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	movl	24(%rsp), %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	shrl	$2, %edi
	andl	$63, %edi
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movb	%al, (%rdx)
	movl	24(%rsp), %eax
	sall	$7, %eax
	andl	$384, %eax
	orl	$16, %eax
	movl	%eax, (%r15)
	jmp	.L247
.L221:
	leal	-97(%rax), %ecx
	cmpb	$25, %cl
	jbe	.L570
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	ja	.L224
	addl	$4, %eax
	jmp	.L222
.L230:
	movl	%r10d, %ecx
	movl	%eax, %r13d
	subl	%edx, %ecx
	shrl	%cl, %r13d
	movzwl	%r9w, %ecx
	shrl	$16, %r9d
	orl	%ecx, %r13d
	movl	%edx, %ecx
	sall	%cl, %eax
	leal	26(%rdx), %ecx
	sall	$26, %eax
	movl	%eax, 4(%r15)
	leal	-55296(%r9), %eax
	cmpl	$1023, %eax
	ja	.L236
	leal	-56320(%r13), %edx
	cmpl	$1023, %edx
	ja	.L237
	sall	$10, %eax
	leal	9216(%r13,%rax), %eax
	movl	%eax, (%r8)
	movq	%rdi, %r8
.L235:
	sall	$3, %ecx
	addq	$1, %rsi
	movl	%ecx, (%r15)
	jmp	.L216
.L262:
	cmpq	$0, 112(%rsp)
	je	.L345
	testb	$8, 16(%r12)
	je	.L264
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	movq	88(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	movq	192(%rsp), %rax
	movq	200(%rsp), %rsi
	je	.L264
	cmpl	$5, 8(%rsp)
	jne	.L240
.L484:
	movq	%rax, 24(%rsp)
	movl	$5, %eax
.L241:
	movq	16(%rsp), %rcx
	movq	24(%rsp), %rdi
	movq	152(%rsp), %rdx
	movq	%rdi, (%rcx)
.L239:
	cmpq	%rdx, %rsi
	jne	.L207
	cmpq	$5, %rax
	jne	.L206
.L210:
	cmpq	%rsi, (%rsp)
	jne	.L201
.L209:
	subl	$1, 20(%r12)
	jmp	.L201
.L348:
	movl	%ebx, 8(%rsp)
	jmp	.L200
.L244:
	leaq	1(%rsi), %rax
	movq	%rax, 200(%rsp)
	movb	%dl, (%rsi)
	jmp	.L247
.L568:
	cmpl	$1114111, %edx
	jbe	.L337
	cmpq	$0, 112(%rsp)
	je	.L345
	testb	$8, 16(%r12)
	jne	.L571
.L264:
	testb	$2, 40(%rsp)
	jne	.L572
.L345:
	movq	%rax, 24(%rsp)
	movl	$6, %eax
	jmp	.L241
.L213:
	movl	%edx, (%r8)
	addq	$1, %rsi
	movq	%rdi, %r8
	jmp	.L216
.L570:
	subl	$71, %eax
	jmp	.L222
.L564:
	movl	%r9d, %eax
	shrl	$16, %eax
	leal	-55296(%rax), %r13d
	cmpl	$1023, %r13d
	jbe	.L232
	movl	%eax, (%r8)
	sall	$16, %r9d
	leal	10(%rdx), %ecx
	movq	%rdi, %r8
	jmp	.L232
.L553:
	cmpl	$127, %r8d
	ja	.L93
	movl	%r8d, %eax
	leaq	direct_tab(%rip), %rdx
	shrl	$3, %eax
	movzbl	(%rdx,%rax), %edx
	movl	%r8d, %eax
	andl	$7, %eax
	btl	%eax, %edx
	jc	.L94
	cmpl	$43, %r8d
	jne	.L302
	movq	(%rsp), %rax
	addq	$2, %rax
	cmpq	%rax, %r13
	jb	.L105
	movq	(%rsp), %rbx
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	movb	$43, (%rbx)
.L282:
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$45, (%rax)
	jmp	.L97
.L260:
	leaq	3(%rsi), %rcx
	cmpq	%rcx, %r13
	jb	.L484
	cmpl	$65535, %edx
	jbe	.L286
	cmpl	$1114111, %edx
	jbe	.L293
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L548:
	movl	$0, (%r15)
	jmp	.L493
.L566:
	movq	$0, 0(%r13)
	movl	16(%r12), %ebx
.L27:
	andl	$1, %ebx
	jne	.L32
.L35:
	movq	64(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	pushq	%rax
	pushq	$1
.L510:
	movq	72(%rsp), %r9
	movq	104(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	96(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r10
	popq	%r11
	jmp	.L21
.L25:
	movq	$0, 0(%r13)
	testb	$1, 16(%r12)
	movl	$0, 8(%rsp)
	jne	.L21
	movq	64(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	pushq	%rax
	pushq	%r14
	jmp	.L510
.L334:
	movq	%rax, 24(%rsp)
	movl	$7, %eax
	jmp	.L241
.L561:
	movl	$43, (%r8)
	movq	%rax, %rsi
	movq	%rdi, %r8
	jmp	.L216
.L306:
	movq	%rbp, %rdx
	movq	(%rsp), %r13
	movl	$4, 8(%rsp)
	jmp	.L140
.L337:
	movl	$6, %ecx
	jmp	.L246
.L28:
	movl	$0, 0(%r13)
	jmp	.L27
.L504:
	cmpq	152(%rsp), %r8
	movq	16(%rsp), %rax
	movq	%rsi, (%rax)
	jne	.L207
.L206:
	leaq	__PRETTY_FUNCTION__.9231(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L569:
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	movl	%edx, 24(%rsp)
	shrl	$10, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rcx
	movb	%al, (%rcx)
	jmp	.L499
.L224:
	cmpb	$43, %al
	je	.L332
	cmpb	$47, %al
	je	.L333
	testl	%r9d, %r9d
	jne	.L522
	cmpl	$26, %edx
	jle	.L522
	cmpb	$45, %al
	movl	$0, (%r15)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rsi
	jmp	.L216
.L93:
	cmpl	$65535, %r8d
	jbe	.L302
	cmpl	$1114111, %r8d
	ja	.L117
	movl	$6, %eax
.L96:
	movq	(%rsp), %rbx
	addq	%rbx, %rax
	cmpq	%rax, %r13
	jb	.L105
	leaq	1(%rbx), %rax
	cmpl	$43, %r8d
	movq	%rax, 168(%rsp)
	movb	$43, (%rbx)
	je	.L282
	cmpl	$65535, %r8d
	jbe	.L573
	cmpl	$1114111, %r8d
	movq	%r10, 8(%rsp)
	ja	.L123
	leal	-65536(%r8), %ebx
	movl	%r8d, %eax
	andl	$1023, %eax
	shrl	$10, %ebx
	addl	$56320, %eax
	addl	$55296, %ebx
	sall	$16, %ebx
	orl	%eax, %ebx
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$50, (%rax)
.L490:
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$20, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$14, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$8, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$2, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movl	%ebx, %eax
	movq	8(%rsp), %r10
	sall	$7, %eax
	andl	$384, %eax
	orl	$16, %eax
	movl	%eax, (%r15)
	jmp	.L97
.L51:
	leal	-65(%r10), %edx
	movl	4(%r15), %eax
	cmpb	$25, %dl
	ja	.L60
	movzbl	%r10b, %edx
	subl	$65, %edx
.L61:
	cmpl	$6, %esi
	jle	.L68
	leal	-6(%rsi), %ecx
	sall	%cl, %edx
	orl	%edx, %eax
	leal	-17(%rsi), %edx
	cmpl	$5, %edx
	ja	.L69
	movl	%eax, %edx
	shrl	$16, %edx
	leal	-55296(%rdx), %r8d
	cmpl	$1023, %r8d
	jbe	.L70
	movq	(%rsp), %rbx
	leal	10(%rsi), %ecx
	sall	$16, %eax
	movq	%r9, (%rsp)
	movl	%edx, (%rbx)
.L70:
	movl	%eax, 4(%r15)
.L71:
	leal	0(,%rcx,8), %r14d
	xorl	%eax, %eax
	leaq	1(%rdi), %rdx
	movl	%r14d, (%r15)
	jmp	.L56
.L109:
	cmpl	$65535, %r8d
	jbe	.L111
	cmpl	$1114111, %r8d
	ja	.L117
	cmpl	$24, %eax
	movq	(%rsp), %rbx
	sete	%al
	movzbl	%al, %eax
	leaq	5(%rbx,%rax), %rax
	cmpq	%rax, %r13
	jb	.L105
.L287:
	leal	-65536(%r8), %ebx
	movl	%r8d, %eax
	sarl	$3, %edx
	andl	$1023, %eax
	movq	%r10, 8(%rsp)
	shrl	$10, %ebx
	addl	$56320, %eax
	addl	$55296, %ebx
	sall	$16, %ebx
	orl	%eax, %ebx
	movl	%edx, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L127
	cmpl	$3, %eax
	jne	.L574
	movq	(%rsp), %r14
	movl	%edx, %edi
	orl	$3, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$24, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$18, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$12, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$6, %edi
	andl	$63, %ebx
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movq	8(%rsp), %r10
	movb	%al, (%r14)
	movl	$8, (%r15)
	jmp	.L97
.L134:
	cmpl	$0, 8(%rsp)
	jne	.L21
.L491:
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)
	movl	16(%r12), %eax
	movl	%eax, 40(%rsp)
	jmp	.L43
.L552:
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	%rbp, (%rax)
	movq	24(%rsp), %rax
	subq	%rax, %rdx
	addq	%r14, %rdx
	cmpq	$4, %rdx
	ja	.L84
	addq	$1, %rax
	cmpq	%rdx, %r14
	jnb	.L88
.L87:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%r15,%r14)
	addq	$1, %r14
	cmpq	%r14, %rdx
	jne	.L87
.L88:
	movl	$7, 8(%rsp)
	jmp	.L21
.L268:
	leaq	1(%rsi), %rax
	andl	$-4, %edi
	movq	%rsi, 104(%rsp)
	movl	%edx, 24(%rsp)
	movq	%rax, 200(%rsp)
	movl	%edx, %eax
	shrl	$14, %eax
	orl	%eax, %edi
	call	base64
	movq	104(%rsp), %rsi
	movl	24(%rsp), %edx
	movb	%al, (%rsi)
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	shrl	$8, %edi
	andl	$63, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rcx
	movl	24(%rsp), %edx
	movb	%al, (%rcx)
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	shrl	$2, %edi
	andl	$63, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rcx
	movl	24(%rsp), %edx
	movb	%al, (%rcx)
	movl	%edx, %eax
	sall	$7, %eax
	andl	$384, %eax
	orl	$16, %eax
	movl	%eax, (%r15)
	jmp	.L247
.L267:
	leaq	1(%rsi), %rax
	andl	$-4, %edi
	movq	%rsi, 24(%rsp)
	movl	%edx, 104(%rsp)
	movq	%rax, 200(%rsp)
	movl	%edx, %eax
	shrl	$12, %eax
	orl	%eax, %edi
	call	base64
	movq	24(%rsp), %rsi
	movl	104(%rsp), %edx
	movb	%al, (%rsi)
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	shrl	$6, %edi
	andl	$63, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 24(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	24(%rsp), %rcx
	movl	104(%rsp), %edx
	movb	%al, (%rcx)
	movq	200(%rsp), %rcx
	movl	%edx, %edi
	andl	$63, %edi
	leaq	1(%rcx), %rax
	movq	%rcx, 24(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	24(%rsp), %rcx
	movb	%al, (%rcx)
	movl	$8, (%r15)
	jmp	.L247
.L271:
	leaq	1(%rsi), %rax
	orl	$3, %edi
	movq	%rsi, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rsi
	movl	24(%rsp), %edi
	movb	%al, (%rsi)
	movq	200(%rsp), %rdx
	shrl	$24, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	shrl	$18, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	shrl	$12, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	shrl	$6, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movb	%al, (%rdx)
	movl	$8, (%r15)
	jmp	.L247
.L270:
	leaq	1(%rsi), %rax
	andl	$-4, %edi
	movq	%rsi, 104(%rsp)
	orl	$13, %edi
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rsi
	movl	24(%rsp), %edi
	movb	%al, (%rsi)
	movq	200(%rsp), %rdx
	shrl	$22, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	shrl	$16, %edi
	andl	$63, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	104(%rsp), %rdx
	movl	24(%rsp), %edi
	movb	%al, (%rdx)
	movq	200(%rsp), %rax
	shrl	$4, %edi
	andl	$63, %edi
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$51, (%rax)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rdx, 104(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movl	24(%rsp), %edi
	movq	104(%rsp), %rdx
	sall	$5, %edi
	movb	%al, (%rdx)
	movl	%edi, %eax
	andl	$480, %eax
	orl	$24, %eax
	movl	%eax, (%r15)
	jmp	.L247
.L111:
	cmpl	$15, %eax
	jle	.L575
	movq	(%rsp), %rax
	addq	$3, %rax
	cmpq	%rax, %r13
	jb	.L105
	cmpl	$65535, %r8d
	jbe	.L284
	cmpl	$1114111, %r8d
	jbe	.L287
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L572:
	movq	112(%rsp), %rcx
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 192(%rsp)
	addq	$1, (%rcx)
	jmp	.L240
.L563:
	sarl	$3, %edi
	movq	%rsi, 120(%rsp)
	movl	%edx, 104(%rsp)
	andl	$-4, %edi
	movb	%r9b, 24(%rsp)
	movq	%rax, 200(%rsp)
	call	base64
	movq	120(%rsp), %rsi
	movl	104(%rsp), %edx
	movzbl	24(%rsp), %r9d
	movb	%al, (%rsi)
	movq	200(%rsp), %rsi
	leaq	1(%rsi), %rax
	jmp	.L258
.L52:
	cmpb	$43, %r10b
	jne	.L576
	leaq	(%rdi,%rcx), %rsi
	leaq	2(%rdi), %rdx
	cmpq	%rdx, %rsi
	jb	.L57
	cmpb	$45, 201(%rsp)
	je	.L577
	movq	$256, (%r15)
	xorl	%eax, %eax
	movl	$256, %r14d
	leaq	1(%rdi), %rdx
	jmp	.L56
.L560:
	cmpq	$0, 112(%rsp)
	je	.L504
	movl	40(%rsp), %r13d
	testl	%r13d, %r13d
	je	.L504
.L498:
	movq	112(%rsp), %rax
	addq	$1, %rsi
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L216
.L117:
	cmpq	$0, 112(%rsp)
	je	.L98
	testb	$8, %bl
	jne	.L578
	andl	$2, %ebx
	jne	.L121
.L98:
	movl	$6, 8(%rsp)
	jmp	.L21
.L29:
	leaq	2(%r14), %rbx
	movl	$5, 8(%rsp)
	cmpq	%rbx, %rdx
	jb	.L21
	sarl	$3, %eax
	leaq	1(%r14), %rbp
	andl	$-4, %eax
	movl	%eax, %edi
	call	base64
	movb	%al, (%r14)
	jmp	.L31
.L567:
	cmpq	%rbx, %r14
	jnb	.L35
	movq	%r14, 200(%rsp)
	movq	64(%rsp), %r14
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	leaq	200(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	72(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r14
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%rbp
	popq	%r12
	je	.L35
	cmpq	%rbx, 200(%rsp)
	jne	.L579
.L37:
	movl	8(%rsp), %ebx
	testl	%ebx, %ebx
	jne	.L21
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$3, %eax
	jmp	.L96
.L556:
	movslq	8(%rsp), %rax
	movq	%rbp, 24(%rsp)
	jmp	.L241
.L284:
	sarl	$3, %edx
	movq	%r10, 24(%rsp)
	movl	%edx, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L124
	cmpl	$3, %eax
	jne	.L580
	movq	(%rsp), %rbx
	movl	%r8d, %edi
	andl	$-4, %edx
	shrl	$14, %edi
	movl	%r8d, 8(%rsp)
	orl	%edx, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movl	8(%rsp), %r8d
	movb	%al, (%rbx)
	movq	168(%rsp), %rbx
	movl	%r8d, %edi
	leaq	1(%rbx), %rax
	shrl	$8, %edi
	andl	$63, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movl	8(%rsp), %r8d
	movb	%al, (%rbx)
	movq	168(%rsp), %rbx
	movl	%r8d, %edi
	leaq	1(%rbx), %rax
	shrl	$2, %edi
	andl	$63, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movl	8(%rsp), %r8d
	movb	%al, (%rbx)
	movq	24(%rsp), %r10
	movl	%r8d, %eax
	sall	$7, %eax
	andl	$384, %eax
	orl	$16, %eax
	movl	%eax, (%r15)
	jmp	.L97
.L94:
	movq	(%rsp), %rbx
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	movb	%r8b, (%rbx)
	jmp	.L97
.L60:
	leal	-97(%r10), %edx
	cmpb	$25, %dl
	ja	.L62
	movzbl	%r10b, %edx
	subl	$71, %edx
	jmp	.L61
.L68:
	movl	$6, %ecx
	movl	%edx, %r8d
	subl	%esi, %ecx
	shrl	%cl, %r8d
	movzwl	%ax, %ecx
	shrl	$16, %eax
	orl	%ecx, %r8d
	movl	%esi, %ecx
	subl	$55296, %eax
	sall	%cl, %edx
	leal	26(%rsi), %ecx
	sall	$26, %edx
	cmpl	$1023, %eax
	movl	%edx, 4(%r15)
	ja	.L581
	leal	-56320(%r8), %edx
	cmpl	$1023, %edx
	ja	.L582
	movq	(%rsp), %rbx
	sall	$10, %eax
	movq	%r9, (%rsp)
	leal	9216(%r8,%rax), %eax
	movl	%eax, (%rbx)
	jmp	.L71
.L571:
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	movq	88(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rsi
	popq	%rdi
	movq	192(%rsp), %rax
	movq	200(%rsp), %rsi
	je	.L264
	cmpl	$5, 8(%rsp)
	jne	.L502
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	movq	152(%rsp), %rsi
	cmpq	200(%rsp), %rsi
	je	.L210
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L332:
	movl	$62, %eax
	jmp	.L222
.L559:
	movq	16(%rsp), %rax
	movq	24(%rsp), %rsi
	cmpq	%r8, %r11
	movq	%rsi, (%rax)
	je	.L209
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L562:
	movq	16(%rsp), %rsi
	movslq	8(%rsp), %rax
	movq	152(%rsp), %rdx
	movq	%rbp, (%rsi)
	movq	%r8, %rsi
	jmp	.L239
.L333:
	movl	$63, %eax
	jmp	.L222
.L62:
	leal	-48(%r10), %edx
	cmpb	$9, %dl
	ja	.L63
	movzbl	%r10b, %edx
	addl	$4, %edx
	jmp	.L61
.L573:
	movq	168(%rsp), %rbx
	movq	%r10, 24(%rsp)
.L489:
	leaq	1(%rbx), %rax
	movl	%r8d, %edi
	movl	%r8d, 8(%rsp)
	shrl	$10, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%rbx)
	movl	8(%rsp), %r8d
	movq	168(%rsp), %rbx
	movl	%r8d, %edi
	leaq	1(%rbx), %rax
	shrl	$4, %edi
	andl	$63, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%rbx)
	movl	8(%rsp), %r8d
	movq	24(%rsp), %r10
	movl	%r8d, %eax
	sall	$5, %eax
	andl	$480, %eax
	orl	$24, %eax
	movl	%eax, (%r15)
	jmp	.L97
.L580:
	cmpl	$1, %eax
	jne	.L123
	movq	(%rsp), %rbx
	jmp	.L489
.L69:
	subl	$11, %esi
	cmpl	$5, %esi
	ja	.L70
	movzwl	%ax, %edx
	subl	$56320, %edx
	cmpl	$1023, %edx
	jbe	.L70
.L488:
	cmpq	$0, 112(%rsp)
	je	.L98
	andb	$2, %bl
	je	.L98
	movq	112(%rsp), %rax
	movl	$0, (%r15)
	leaq	1(%rdi), %rdx
	addq	$1, (%rax)
	xorl	%eax, %eax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L574:
	cmpl	$1, %eax
	jne	.L123
	movq	(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 168(%rsp)
	movb	$50, (%rsi)
	jmp	.L490
.L522:
	cmpq	$0, 112(%rsp)
	je	.L504
	movl	40(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L504
	movl	$0, (%r15)
	jmp	.L498
.L577:
	movq	(%rsp), %rax
	movq	%r9, (%rsp)
	movl	$43, (%rax)
	movl	(%r15), %r14d
	movl	%r14d, %eax
	andl	$7, %eax
	jmp	.L56
.L124:
	movq	(%rsp), %rbx
	movl	%r8d, %edi
	andl	$-4, %edx
	shrl	$12, %edi
	movl	%r8d, 8(%rsp)
	orl	%edx, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movl	8(%rsp), %r8d
	movb	%al, (%rbx)
	movq	168(%rsp), %rbx
	movl	%r8d, %edi
	leaq	1(%rbx), %rax
	shrl	$6, %edi
	andl	$63, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movl	8(%rsp), %r8d
	movb	%al, (%rbx)
	movq	168(%rsp), %rbx
	movl	%r8d, %edi
	leaq	1(%rbx), %rax
	andl	$63, %edi
	movq	%rax, 168(%rsp)
	call	base64
	movq	24(%rsp), %r10
	movb	%al, (%rbx)
	movl	$8, (%r15)
	jmp	.L97
.L127:
	movq	(%rsp), %r14
	andl	$-4, %edx
	movl	%edx, %edi
	orl	$13, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$22, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %r14
	movl	%ebx, %edi
	shrl	$16, %edi
	andl	$63, %edi
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movq	168(%rsp), %rax
	movl	%ebx, %edi
	shrl	$4, %edi
	andl	$63, %edi
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$51, (%rax)
	movq	168(%rsp), %r14
	leaq	1(%r14), %rax
	movq	%rax, 168(%rsp)
	call	base64
	movb	%al, (%r14)
	movl	%ebx, %eax
	movq	8(%rsp), %r10
	sall	$5, %eax
	andl	$480, %eax
	orl	$24, %eax
	movl	%eax, (%r15)
	jmp	.L97
.L578:
	movq	%r10, 72(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	pushq	120(%rsp)
	movq	40(%rsp), %rdx
	movq	%r11, %r8
	movq	64(%rsp), %rdi
	movq	%r12, %rsi
	leaq	184(%rsp), %r9
	movq	%r11, 56(%rsp)
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rdi
	popq	%r8
	movq	40(%rsp), %r11
	movq	72(%rsp), %r10
	je	.L583
	movq	160(%rsp), %rax
	cmpq	%r10, %rax
	jne	.L130
	cmpl	$7, 8(%rsp)
	jne	.L134
	leaq	4(%r10), %rax
	cmpq	%rax, %r11
	je	.L584
	movl	(%r15), %eax
	movq	%r14, %rbx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r14
	jle	.L585
	cmpq	$4, %r14
	ja	.L586
	orl	%r14d, %eax
	testq	%r14, %r14
	movl	%eax, (%r15)
	je	.L88
	xorl	%eax, %eax
.L138:
	movzbl	(%r10,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L138
	jmp	.L88
.L583:
	andl	$2, %ebx
	je	.L102
.L121:
	movq	112(%rsp), %rax
	addq	$4, 160(%rsp)
	addq	$1, (%rax)
.L102:
	movq	160(%rsp), %rax
	cmpq	%r10, %rax
	jne	.L130
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L63:
	cmpb	$43, %r10b
	je	.L300
	cmpb	$47, %r10b
	je	.L301
	cmpl	$26, %esi
	jle	.L488
	testl	%eax, %eax
	jne	.L488
	cmpb	$45, %r10b
	je	.L587
	movl	$0, (%r15)
	jmp	.L43
.L579:
	movq	%r15, 0(%r13)
	jmp	.L37
.L57:
	leaq	6(%rdi), %rdx
	cmpq	%rdx, %rsi
	je	.L588
	movq	%rcx, %rbx
	subq	%rax, %rbx
	movq	24(%rsp), %rax
	addq	%rbx, %rax
	movq	16(%rsp), %rbx
	testq	%rcx, %rcx
	movq	%rax, (%rbx)
	jle	.L589
	cmpq	$4, %rcx
	jg	.L590
	movl	%ecx, (%r15)
	xorl	%eax, %eax
.L79:
	movzbl	(%rdi,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L79
	jmp	.L88
.L576:
	cmpq	$0, 112(%rsp)
	je	.L98
	andb	$2, %bl
	je	.L98
	movq	112(%rsp), %rbx
	movl	%edx, %r14d
	leaq	1(%rdi), %rdx
	addq	$1, (%rbx)
	jmp	.L56
.L237:
	leaq	__PRETTY_FUNCTION__.9016(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L236:
	leaq	__PRETTY_FUNCTION__.9016(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L558:
	cmpq	(%rsp), %r11
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	je	.L206
	jmp	.L207
.L301:
	movl	$63, %edx
	jmp	.L61
.L300:
	movl	$62, %edx
	jmp	.L61
.L587:
	movl	$0, (%r15)
	xorl	%eax, %eax
	leaq	1(%rdi), %rdx
	jmp	.L56
.L84:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L46:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L582:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L581:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L590:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L586:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L589:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L588:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L585:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L584:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L81:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L550:
	leaq	__PRETTY_FUNCTION__.9231(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L565:
	leaq	__PRETTY_FUNCTION__.9231(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L278:
	leaq	__PRETTY_FUNCTION__.9231(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L557:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L554:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9016, @object
	.size	__PRETTY_FUNCTION__.9016, 15
__PRETTY_FUNCTION__.9016:
	.string	"from_utf7_loop"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9140, @object
	.size	__PRETTY_FUNCTION__.9140, 20
__PRETTY_FUNCTION__.9140:
	.string	"to_utf7_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9035, @object
	.size	__PRETTY_FUNCTION__.9035, 22
__PRETTY_FUNCTION__.9035:
	.string	"from_utf7_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9231, @object
	.size	__PRETTY_FUNCTION__.9231, 6
__PRETTY_FUNCTION__.9231:
	.string	"gconv"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	xbase64_tab, @object
	.size	xbase64_tab, 16
xbase64_tab:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-88
	.byte	-1
	.byte	3
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	7
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	7
	.align 16
	.type	xdirect_tab, @object
	.size	xdirect_tab, 16
xdirect_tab:
	.byte	0
	.byte	38
	.byte	0
	.byte	0
	.byte	-1
	.byte	-9
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	-17
	.byte	-1
	.byte	-1
	.byte	-1
	.byte	63
	.align 16
	.type	direct_tab, @object
	.size	direct_tab, 16
direct_tab:
	.byte	0
	.byte	38
	.byte	0
	.byte	0
	.byte	-127
	.byte	-13
	.byte	-1
	.byte	-121
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	7
	.byte	-2
	.byte	-1
	.byte	-1
	.byte	7
