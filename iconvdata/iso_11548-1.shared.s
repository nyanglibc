	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	movzbl	%sil, %eax
	orb	$40, %ah
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO_11548-1//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L4
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	32(%rax), %rsi
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L7
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	movq	%r9, %r14
	subq	$168, %rsp
	movl	16(%r12), %r15d
	movq	%rdi, 40(%rsp)
	leaq	48(%r12), %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 32(%rsp)
	movl	224(%rsp), %ebx
	testb	$1, %r15b
	movq	%rsi, 80(%rsp)
	movq	%rdi, 72(%rsp)
	movq	$0, 64(%rsp)
	jne	.L9
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 64(%rsp)
	je	.L9
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 64(%rsp)
.L9:
	testl	%ebx, %ebx
	jne	.L169
	movq	8(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	testq	%r14, %r14
	movq	(%rax), %r11
	movq	8(%r12), %rax
	movq	$0, 128(%rsp)
	movq	%rax, 48(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movl	232(%rsp), %edx
	movq	%rax, 104(%rsp)
	movq	40(%rsp), %rax
	testl	%edx, %edx
	movq	96(%rax), %rax
	setne	103(%rsp)
	movzbl	103(%rsp), %ecx
	testq	%rax, %rax
	je	.L100
	testb	%cl, %cl
	je	.L100
	movq	32(%r12), %rbx
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	jne	.L170
.L100:
	movq	$0, 24(%rsp)
.L16:
	leaq	152(%rsp), %rcx
	leaq	144(%rsp), %rsi
	leaq	136(%rsp), %rdi
	movq	%r14, 16(%rsp)
	movq	%r11, %r14
	movq	48(%rsp), %r11
	movq	%rcx, 112(%rsp)
	movq	%rsi, 120(%rsp)
	movq	%rdi, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L90:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	(%rdi), %rdi
	addq	%rdi, 24(%rsp)
.L46:
	testq	%rax, %rax
	je	.L171
	movl	16(%r12), %r15d
	movq	%r14, %rbx
	movq	%r13, %rax
	movq	%r13, 144(%rsp)
	movl	$4, %r10d
	movq	%r14, 152(%rsp)
	andl	$2, %r15d
.L52:
	cmpq	%rax, %rbp
	je	.L53
.L59:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L106
	cmpq	%rbx, %r11
	jbe	.L107
	movl	(%rax), %edx
	movl	%edx, %esi
	xorb	%sil, %sil
	cmpl	$10240, %esi
	jne	.L172
	leaq	1(%rbx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rbx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L59
	.p2align 4,,10
	.p2align 3
.L53:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L173
.L60:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L174
	cmpq	%rbx, %r14
	jnb	.L110
	movq	64(%rsp), %r15
	movq	(%r12), %rax
	movl	%r10d, 56(%rsp)
	movq	%r11, 48(%rsp)
	movq	%r15, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	32(%rsp), %r9
	movq	104(%rsp), %rdx
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r15d
	popq	%rdi
	movq	48(%rsp), %r11
	movl	56(%rsp), %r10d
	je	.L64
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L175
.L63:
	testl	%r15d, %r15d
	jne	.L119
.L89:
	movq	128(%rsp), %rax
	movq	8(%rsp), %rdi
	movq	(%r12), %r14
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	(%rdi), %r13
	movq	96(%rax), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L171:
	cmpq	%r13, %rbp
	je	.L103
	leaq	4(%r14), %rdx
	cmpq	%rdx, %r11
	jb	.L104
	movq	%r11, %rax
	subq	%r14, %rax
	subq	$4, %rax
	shrq	$2, %rax
	leaq	1(%r13,%rax), %rcx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %rax
	movq	%rdx, %rbx
	movzbl	-1(%rax), %edx
	orb	$40, %dh
	cmpq	%rax, %rbp
	movl	%edx, -4(%rbx)
	je	.L176
	cmpq	%rax, %rcx
	leaq	4(%rbx), %rdx
	jne	.L49
	movl	$5, %r10d
.L48:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L60
.L173:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L8:
	addq	$168, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$7, %r10d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$5, %r10d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L64:
	cmpl	$5, %r10d
	movl	%r10d, %r15d
	jne	.L63
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L172:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L177
	cmpq	$0, 104(%rsp)
	je	.L109
	testb	$8, 16(%r12)
	jne	.L178
.L57:
	testl	%r15d, %r15d
	jne	.L179
.L109:
	movl	$6, %r10d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r11, 48(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	112(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r8
	cmpl	$6, %r10d
	popq	%r9
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movq	48(%rsp), %r11
	je	.L57
	cmpl	$5, %r10d
	jne	.L52
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%rbp, %rcx
	movl	$4, %r10d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L174:
	movq	16(%rsp), %r14
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%r14)
.L62:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 103(%rsp)
	je	.L8
	cmpl	$7, %r10d
	jne	.L8
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L92
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L94
.L93:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L93
.L94:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L110:
	movl	%r10d, %r15d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L175:
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L66
	movq	(%rcx), %rax
.L66:
	addq	128(%rsp), %rax
	cmpq	24(%rsp), %rax
	movq	8(%rsp), %rax
	je	.L180
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L181
	movl	16(%r12), %ebx
	movq	%r13, 144(%rsp)
	movq	%r14, %rdx
	movq	%r14, 152(%rsp)
	movl	$4, %eax
	andl	$2, %ebx
.L79:
	cmpq	%r13, %rbp
	je	.L182
.L86:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rbp
	jb	.L113
	cmpq	%rdx, %r10
	jbe	.L116
	movl	0(%r13), %ecx
	movl	%ecx, %edi
	xorb	%dil, %dil
	cmpl	$10240, %edi
	jne	.L183
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	144(%rsp), %rcx
	movq	152(%rsp), %rdx
	leaq	4(%rcx), %r13
	cmpq	%r13, %rbp
	movq	%r13, 144(%rsp)
	jne	.L86
.L182:
	cltq
	movq	%rbp, %r13
.L80:
	movq	8(%rsp), %rcx
	movq	136(%rsp), %r10
	movq	%r13, (%rcx)
.L78:
	cmpq	%r10, %rdx
	jne	.L72
	cmpq	$5, %rax
	jne	.L71
	cmpq	%rdx, %r14
	jne	.L63
.L74:
	subl	$1, 20(%r12)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L179:
	movq	104(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L170:
	testq	%rdi, %rdi
	jne	.L184
	cmpl	$4, %edx
	movq	%r13, 144(%rsp)
	movq	%r11, 152(%rsp)
	ja	.L18
	leaq	136(%rsp), %rdi
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movq	%rdx, 24(%rsp)
	movq	%rdi, 16(%rsp)
.L19:
	movzbl	4(%rbx,%rax), %esi
	movb	%sil, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L19
	movq	%r13, %rax
	subq	%rdx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L185
	cmpq	48(%rsp), %r11
	movl	$5, %r10d
	jnb	.L8
	movq	24(%rsp), %rsi
	leaq	1(%r13), %rax
	leaq	135(%rsp), %r8
.L27:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edi
	addq	$1, %rsi
	movq	%rax, %r9
	addq	$1, %rax
	cmpq	$3, %rsi
	movb	%dil, (%r8,%rsi)
	ja	.L121
	cmpq	%r9, %rbp
	ja	.L27
.L121:
	movq	16(%rsp), %rax
	movq	%rsi, 24(%rsp)
	movq	%rax, 144(%rsp)
	movl	136(%rsp), %eax
	movl	%eax, %esi
	xorb	%sil, %sil
	cmpl	$10240, %esi
	jne	.L186
	leaq	1(%r11), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r11)
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	16(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L38
.L168:
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
.L31:
	subq	16(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L187
	movq	8(%rsp), %rdi
	subq	%rdx, %rax
	andl	$-8, %ecx
	movq	152(%rsp), %r11
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r13
	movq	128(%rsp), %rax
	movl	%ecx, (%rbx)
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L16
.L180:
	movq	40(%rsp), %rcx
	subq	%r10, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rcx)
	je	.L188
	movq	8(%rsp), %rsi
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r13, %rcx
	movq	%r14, %rbx
	movl	$5, %r10d
	jmp	.L48
.L169:
	cmpq	$0, 32(%rsp)
	jne	.L189
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L8
	movq	64(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%ecx, %ecx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rcx
	movl	%eax, %r10d
	popq	%rsi
	jmp	.L8
.L103:
	movq	%rbp, %rcx
	movq	%r14, %rbx
	movl	$4, %r10d
	jmp	.L48
.L119:
	movl	%r15d, %r10d
	jmp	.L62
.L188:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	movq	8(%rsp), %rcx
	cmovs	%rdx, %rbx
	movq	%rbx, %rdx
	sarq	$2, %rdx
	subq	%rdx, %rax
	movq	%rax, (%rcx)
	jmp	.L63
.L181:
	cmpq	%r13, %rbp
	je	.L190
	leaq	4(%r14), %rax
	cmpq	%rax, %r10
	jb	.L191
	movq	%r10, %rdx
	subq	%r14, %rdx
	subq	$4, %rdx
	shrq	$2, %rdx
	leaq	1(%r13,%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$1, %r13
	movq	%rax, %rdx
	movzbl	-1(%r13), %eax
	orb	$40, %ah
	cmpq	%r13, %rbp
	movl	%eax, -4(%rdx)
	je	.L192
	cmpq	%r13, %rcx
	leaq	4(%rdx), %rax
	jne	.L75
	movl	$5, %eax
.L76:
	movq	8(%rsp), %rsi
	movq	%rcx, (%rsi)
	jmp	.L78
.L113:
	movl	$7, %eax
	jmp	.L80
.L194:
	movq	%r10, 48(%rsp)
	movq	%r11, 24(%rsp)
	subq	$8, %rsp
	pushq	112(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r13
	movq	152(%rsp), %rdx
	movq	24(%rsp), %r11
	movq	48(%rsp), %r10
	je	.L84
	cmpl	$5, %eax
	jne	.L79
.L116:
	movl	$5, %eax
	jmp	.L80
.L183:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L193
	cmpq	$0, 104(%rsp)
	je	.L117
	testb	$8, 16(%r12)
	jne	.L194
.L84:
	testl	%ebx, %ebx
	jne	.L195
.L117:
	movl	$6, %eax
	jmp	.L80
.L185:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rdx, %rax
	cmpq	$4, %rax
	ja	.L21
	addq	$1, %r13
	cmpq	%rax, %rdx
	jnb	.L23
	movq	24(%rsp), %rdx
.L24:
	movq	%r13, 144(%rsp)
	movzbl	-1(%r13), %ecx
	addq	$1, %r13
	movb	%cl, 4(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L24
.L23:
	movl	$7, %r10d
	jmp	.L8
.L192:
	movq	%rbp, %rcx
	movl	$4, %eax
	jmp	.L76
.L186:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L196
	cmpq	$0, 104(%rsp)
	je	.L101
	testb	$8, %r15b
	jne	.L197
	andl	$2, %r15d
	movl	$6, %r10d
	movq	16(%rsp), %rax
	je	.L8
.L36:
	movq	104(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
.L37:
	cmpq	16(%rsp), %rax
	jne	.L168
.L101:
	movl	$6, %r10d
	jmp	.L8
.L41:
	testl	%r10d, %r10d
	jne	.L8
.L38:
	movq	128(%rsp), %rax
	movq	8(%rsp), %rsi
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	(%rsi), %r13
	movq	96(%rax), %rax
	jmp	.L16
.L195:
	movq	104(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L79
.L193:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r13
	jmp	.L79
.L191:
	cmpq	%r14, %r10
	je	.L74
.L72:
	leaq	__PRETTY_FUNCTION__.9133(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	cmpq	%r14, %r10
	jne	.L72
.L71:
	leaq	__PRETTY_FUNCTION__.9133(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	movq	16(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L31
.L197:
	movq	16(%rsp), %rax
	addq	24(%rsp), %rax
	leaq	144(%rsp), %rcx
	movq	%r11, 88(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, 64(%rsp)
	pushq	112(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%r13
	movq	88(%rsp), %r11
	je	.L198
	movq	144(%rsp), %rax
	cmpq	16(%rsp), %rax
	jne	.L168
	cmpl	$7, %r10d
	jne	.L41
	movq	16(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 56(%rsp)
	je	.L199
	movl	(%rbx), %eax
	movq	24(%rsp), %rcx
	movl	%eax, %edx
	movq	%rcx, %rsi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	8(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rcx
	jle	.L200
	cmpq	$4, 24(%rsp)
	ja	.L201
	movq	24(%rsp), %rcx
	orl	%ecx, %eax
	testq	%rcx, %rcx
	movl	%eax, (%rbx)
	je	.L23
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
.L45:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 24(%rsp)
	jne	.L45
	jmp	.L23
.L198:
	andl	$2, %r15d
	movq	144(%rsp), %rax
	je	.L37
	jmp	.L36
.L21:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L92:
	leaq	__PRETTY_FUNCTION__.9133(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L189:
	leaq	__PRETTY_FUNCTION__.9133(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L187:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L18:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L184:
	leaq	__PRETTY_FUNCTION__.9133(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L201:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L200:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L199:
	leaq	__PRETTY_FUNCTION__.9059(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9059, @object
	.size	__PRETTY_FUNCTION__.9059, 21
__PRETTY_FUNCTION__.9059:
	.string	"to_iso11548_1_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9133, @object
	.size	__PRETTY_FUNCTION__.9133, 6
__PRETTY_FUNCTION__.9133:
	.string	"gconv"
