	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	testb	%sil, %sil
	movl	(%rax,%rdx,4), %eax
	je	.L1
	testl	%eax, %eax
	movl	$-1, %edx
	cmove	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ANSI_X3.110//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"\325"
.LC2:
	.string	"\345"
.LC3:
	.string	"\327"
.LC4:
	.string	"\326"
.LC5:
	.string	"../iconv/skeleton.c"
.LC6:
	.string	"outbufstart == NULL"
.LC7:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC9:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC10:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC11:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC12:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC13:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC14:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC16:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 56(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	testb	$1, %r11b
	movq	%r9, 48(%rsp)
	movl	224(%rsp), %ebx
	movq	%rdi, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 24(%rsp)
	je	.L15
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L489
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	movl	232(%rsp), %ebx
	movq	8(%r12), %r13
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 48(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 72(%rsp)
	jne	.L490
.L98:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L491
.L104:
	leaq	.LC2(%rip), %r10
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rbx
	movq	%r14, %rax
	movl	$4, 8(%rsp)
.L114:
	cmpq	%rax, %rbp
	je	.L115
.L143:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L223
	cmpq	%rbx, %r13
	jbe	.L224
	movl	(%rax), %edx
	cmpl	$382, %edx
	ja	.L492
	leaq	from_ucs4(%rip), %rdi
	movl	%edx, %ecx
	leaq	(%rdi,%rcx,2), %rsi
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	jne	.L119
	testl	%edx, %edx
	jne	.L486
.L119:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%rbx)
	movzbl	1(%rsi), %eax
	testb	%al, %al
	je	.L140
.L202:
	movq	152(%rsp), %rbx
	cmpq	%rbx, %r13
	jbe	.L493
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
.L140:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L115:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L494
.L144:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L495
	cmpq	%rbx, %r15
	jnb	.L237
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%r8
	popq	%r9
	je	.L148
	movq	136(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L496
.L147:
	testl	%r10d, %r10d
	jne	.L263
.L196:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r15
	movq	(%rax), %r14
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L104
.L491:
	cmpq	%rbp, %r14
	je	.L215
	leaq	4(%r15), %rdx
	movq	%r14, %rax
	movq	%r15, %rbx
	cmpq	%rdx, %r13
	jb	.L217
	leaq	to_ucs4(%rip), %rdi
	leaq	to_ucs4_comb(%rip), %r9
	movl	$4, 8(%rsp)
	andl	$2, %r11d
	.p2align 4,,10
	.p2align 3
.L106:
	movzbl	(%rax), %ecx
	leal	-193(%rcx), %r8d
	movl	%ecx, %esi
	cmpl	$14, %r8d
	jbe	.L497
	movl	%ecx, %ecx
	movl	(%rdi,%rcx,4), %r8d
	movl	$1, %ecx
.L110:
	testl	%r8d, %r8d
	jne	.L111
	testb	%sil, %sil
	jne	.L498
.L111:
	movl	%r8d, (%rbx)
	addq	%rcx, %rax
	movq	%rdx, %rbx
.L112:
	cmpq	%rax, %rbp
	je	.L105
.L109:
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r13
	jnb	.L106
.L217:
	movl	$5, 8(%rsp)
.L105:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	je	.L144
.L494:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L14:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	movl	$7, 8(%rsp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L148:
	movl	8(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L147
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$5, 8(%rsp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L492:
	cmpl	$711, %edx
	je	.L117
	leal	-728(%rdx), %esi
	cmpl	$5, %esi
	ja	.L118
	cmpl	$732, %edx
	je	.L118
	leaq	map.9085(%rip), %rax
	movb	$32, 137(%rsp)
	movzbl	(%rax,%rsi), %ecx
	leaq	136(%rsp), %rsi
	movb	%cl, 136(%rsp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L490:
	movq	32(%r12), %rbx
	movl	(%rbx), %eax
	movl	%eax, %r10d
	andl	$7, %r10d
	je	.L98
	testq	%rdi, %rdi
	jne	.L499
	movq	40(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L500
	cmpl	$4, %r10d
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	ja	.L42
	leaq	136(%rsp), %rcx
	movslq	%r10d, %r10
	xorl	%eax, %eax
	movq	%rcx, 80(%rsp)
.L43:
	movzbl	4(%rbx,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r10, %rax
	jne	.L43
	movq	%r14, %rax
	subq	%r10, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L501
	cmpq	%r13, %r15
	jnb	.L94
	leaq	1(%r14), %rax
	leaq	135(%rsp), %rsi
.L51:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %r10
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %r10
	movb	%dl, (%rsi,%r10)
	ja	.L265
	cmpq	%rcx, %rbp
	ja	.L51
.L265:
	movq	80(%rsp), %rax
	movq	%rax, 144(%rsp)
	addq	%r10, %rax
	movq	%rax, 88(%rsp)
	movl	136(%rsp), %eax
	cmpl	$382, %eax
	ja	.L502
	leaq	from_ucs4(%rip), %rdx
	movl	%eax, %ecx
	testl	%eax, %eax
	leaq	(%rdx,%rcx,2), %rcx
	movzbl	(%rcx), %edx
	je	.L56
	testb	%dl, %dl
	jne	.L56
	cmpq	$0, 72(%rsp)
	je	.L438
	testb	$8, %r11b
	jne	.L472
	andl	$2, %r11d
	jne	.L503
.L438:
	movl	$6, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L118:
	leal	-8212(%rdx), %esi
	cmpl	$9, %esi
	ja	.L120
	leaq	map.9086(%rip), %rdx
	movzbl	(%rdx,%rsi), %ecx
	testb	%cl, %cl
	movb	%cl, 136(%rsp)
	je	.L486
.L454:
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rsi
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	leal	-8482(%rdx), %esi
	cmpl	$4, %esi
	ja	.L126
	leaq	map.9088(%rip), %rdx
	movzbl	(%rdx,%rsi), %ecx
	testb	%cl, %cl
	movb	%cl, 136(%rsp)
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L486:
	cmpq	$0, 72(%rsp)
	je	.L236
	testb	$8, 16(%r12)
	jne	.L504
.L138:
	testb	$2, %r11b
	jne	.L455
.L236:
	movl	$6, 8(%rsp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L493:
	subq	$1, %rbx
	movq	144(%rsp), %rax
	movl	$5, 8(%rsp)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L495:
	movq	48(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L146:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	cmpl	$7, 8(%rsp)
	jne	.L14
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L198
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L200
.L199:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L199
.L200:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	1(%rax), %r10
	cmpq	%r10, %rbp
	jbe	.L218
	movzbl	1(%rax), %ecx
	subl	$32, %ecx
	cmpl	$95, %ecx
	ja	.L505
	leaq	(%r8,%r8,2), %r8
	salq	$5, %r8
	addq	%r8, %rcx
	movl	(%r9,%rcx,4), %r8d
	movl	$2, %ecx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L498:
	cmpq	$0, 72(%rsp)
	je	.L222
	testl	%r11d, %r11d
	jne	.L506
.L222:
	movl	$6, 8(%rsp)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L237:
	movl	8(%rsp), %r10d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%rbx)
	movl	$32, %eax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L496:
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L507
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
.L164:
	cmpq	%r14, %rbp
	je	.L508
.L194:
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbp
	jb	.L245
	cmpq	%rdx, %r11
	jbe	.L246
	movl	(%r14), %ecx
	cmpl	$382, %ecx
	ja	.L509
	leaq	from_ucs4(%rip), %rdi
	movl	%ecx, %esi
	leaq	(%rdi,%rsi,2), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L169
	testl	%ecx, %ecx
	jne	.L488
.L169:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movzbl	1(%rdi), %edx
	testb	%dl, %dl
	je	.L191
.L203:
	movq	152(%rsp), %rcx
	cmpq	%rcx, %r11
	jbe	.L510
	leaq	1(%rcx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rcx)
.L191:
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r14
	cmpq	%r14, %rbp
	movq	%r14, 144(%rsp)
	jne	.L194
.L508:
	cltq
	movq	%rbp, %r14
	jmp	.L165
.L500:
	cmpl	$4, %r10d
	ja	.L25
	movzbl	4(%rbx), %edx
	cmpl	$1, %r10d
	movb	%dl, 152(%rsp)
	movl	$1, %edx
	je	.L26
	movzbl	5(%rbx), %edx
	movb	%dl, 153(%rsp)
	movl	$2, %edx
.L26:
	leaq	4(%r15), %rcx
	cmpq	%rcx, %r13
	jb	.L94
	movzbl	(%r14), %esi
	movb	%sil, 152(%rsp,%rdx)
	movzbl	152(%rsp), %esi
	leal	-193(%rsi), %r9d
	movl	%esi, %edi
	cmpl	$14, %r9d
	jbe	.L511
	leaq	to_ucs4(%rip), %rax
	movl	%esi, %esi
	movl	(%rax,%rsi,4), %edx
	movl	$1, %eax
.L36:
	testb	%dil, %dil
	je	.L206
	testl	%edx, %edx
	je	.L512
.L206:
	leaq	152(%rsp), %rsi
	movl	%edx, (%r15)
	movslq	%eax, %r15
	leaq	(%rsi,%r15), %rax
	movq	%rcx, %r15
.L35:
	subq	%rsi, %rax
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %rcx
	jle	.L513
	movq	16(%rsp), %rdi
	subq	%rdx, %rcx
	andl	$-8, %eax
	addq	%rcx, %r14
	movl	16(%r12), %r11d
	movq	%r14, (%rdi)
	movl	%eax, (%rbx)
	jmp	.L98
.L507:
	cmpq	%rbp, %r14
	je	.L514
	leaq	4(%r15), %rax
	andl	$2, %ebx
	movq	%r15, %rdx
	movl	$4, %r9d
	leaq	to_ucs4(%rip), %rdi
	cmpq	%rax, %r11
	jb	.L515
	.p2align 4,,10
	.p2align 3
.L154:
	movzbl	(%r14), %r8d
	leal	-193(%r8), %esi
	movl	%r8d, %ecx
	cmpl	$14, %esi
	jbe	.L516
	movl	%r8d, %r8d
	movl	$1, %esi
	movl	(%rdi,%r8,4), %r8d
.L160:
	testl	%r8d, %r8d
	jne	.L161
	testb	%cl, %cl
	jne	.L517
.L161:
	movl	%r8d, (%rdx)
	addq	%rsi, %r14
	movq	%rax, %rdx
.L162:
	cmpq	%r14, %rbp
	je	.L518
.L159:
	leaq	4(%rdx), %rax
	cmpq	%rax, %r11
	jnb	.L154
	movl	$5, %eax
.L156:
	movq	16(%rsp), %rsi
	movq	%r14, (%rsi)
	jmp	.L163
.L502:
	cmpl	$711, %eax
	je	.L54
	leal	-728(%rax), %edx
	cmpl	$5, %edx
	ja	.L55
	cmpl	$732, %eax
	je	.L55
	leaq	map.9119(%rip), %rax
	leaq	126(%rsp), %rcx
	movb	$32, 127(%rsp)
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 126(%rsp)
.L56:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%r15)
	movzbl	1(%rcx), %eax
	testb	%al, %al
	jne	.L201
.L92:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	80(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L453
.L64:
	movl	(%rbx), %edx
	subq	80(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L519
	movq	16(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r15
	movl	16(%r12), %r11d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r14
	movl	%edx, (%rbx)
	jmp	.L98
.L57:
	leal	-8482(%rax), %edx
	cmpl	$4, %edx
	ja	.L68
	leaq	map.9123(%rip), %rax
	movzbl	(%rax,%rdx), %edx
	testb	%dl, %dl
	movb	%dl, 126(%rsp)
	jne	.L446
	cmpq	$0, 72(%rsp)
	je	.L438
	testb	$8, %r11b
	je	.L70
	movq	%r10, 104(%rsp)
	movl	%r11d, 100(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	56(%rsp), %rdi
	movq	104(%rsp), %r8
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	movl	%eax, %edi
	popq	%r10
	popq	%r11
	movq	144(%rsp), %rax
	cmpl	$6, %edi
	movl	100(%rsp), %r11d
	movq	%rax, %rdx
	je	.L88
	cmpl	$5, %edi
	movq	104(%rsp), %r10
	jne	.L63
.L62:
	cmpq	80(%rsp), %rax
	jne	.L64
.L94:
	movl	$5, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L504:
	movl	%r11d, 88(%rsp)
	movq	%r10, 80(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	80(%rsp), %r10
	movl	88(%rsp), %r11d
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L138
	cmpl	$5, 8(%rsp)
	jne	.L114
	jmp	.L115
.L528:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L520
	cmpq	$0, 72(%rsp)
	je	.L236
	testb	$8, 16(%r12)
	jne	.L521
.L136:
	testb	$2, %r11b
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L236
	.p2align 4,,10
	.p2align 3
.L455:
	movq	72(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L506:
	movq	72(%rsp), %rsi
	addq	%rcx, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rsi)
	jmp	.L112
.L489:
	cmpq	$0, 32(%rsp)
	jne	.L522
	movq	32(%r12), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	movl	%eax, 24(%rsp)
	popq	%rbp
	popq	%r12
	jmp	.L14
.L263:
	movl	%r10d, 8(%rsp)
	jmp	.L146
.L126:
	leal	-8539(%rdx), %esi
	cmpl	$3, %esi
	jbe	.L523
	leal	-8592(%rdx), %esi
	cmpl	$3, %esi
	ja	.L131
	leal	28(%rdx), %ecx
	movb	%cl, 136(%rsp)
	jmp	.L454
.L505:
	cmpq	$0, 72(%rsp)
	je	.L222
	testl	%r11d, %r11d
	je	.L222
	movq	72(%rsp), %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	movq	%r10, %rax
	jmp	.L109
.L245:
	movl	$7, %eax
.L165:
	movq	16(%rsp), %rdi
	movq	136(%rsp), %r11
	movq	%r14, (%rdi)
.L163:
	cmpq	%r11, %rdx
	jne	.L153
	cmpq	$5, %rax
	jne	.L152
.L188:
	cmpq	%r15, %rdx
	jne	.L147
.L155:
	subl	$1, 20(%r12)
	jmp	.L147
.L523:
	leal	-127(%rdx), %ecx
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rsi
	movb	%cl, 136(%rsp)
	jmp	.L119
.L54:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%r15)
	movl	$32, %eax
.L201:
	movq	152(%rsp), %rdx
	cmpq	%rdx, %r13
	jbe	.L524
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	jmp	.L92
.L525:
	movq	%r11, 80(%rsp)
	movl	%r10d, 8(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	movl	8(%rsp), %r10d
	movq	80(%rsp), %r11
	je	.L189
	cmpl	$5, %eax
	jne	.L164
.L246:
	movl	$5, %eax
	jmp	.L165
.L509:
	cmpl	$711, %ecx
	je	.L167
	leal	-728(%rcx), %edi
	cmpl	$5, %edi
	ja	.L168
	cmpl	$732, %ecx
	je	.L168
	leaq	map.9085(%rip), %rcx
	movb	$32, 127(%rsp)
	movzbl	(%rcx,%rdi), %esi
	leaq	126(%rsp), %rdi
	movb	%sil, 126(%rsp)
	jmp	.L169
.L215:
	movq	%r14, %rax
	movq	%r15, %rbx
	movl	$4, 8(%rsp)
	jmp	.L105
.L170:
	leal	-8482(%rcx), %edi
	cmpl	$4, %edi
	ja	.L176
	leaq	map.9088(%rip), %rcx
	movzbl	(%rcx,%rdi), %esi
	testb	%sil, %sil
	movb	%sil, 126(%rsp)
	jne	.L458
.L488:
	cmpq	$0, 72(%rsp)
	je	.L261
	testb	$8, 16(%r12)
	jne	.L525
.L189:
	testb	$2, %bl
	jne	.L459
.L261:
	movl	$6, %eax
	jmp	.L165
.L510:
	movq	136(%rsp), %rdx
	movq	144(%rsp), %rax
	subq	$1, %rcx
	movq	16(%rsp), %rsi
	cmpq	%rcx, %rdx
	movq	%rax, (%rsi)
	je	.L188
.L153:
	leaq	__PRETTY_FUNCTION__.9185(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L99:
	cmpl	$0, 8(%rsp)
	jne	.L14
.L453:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r14
	jmp	.L98
.L131:
	cmpl	$9472, %edx
	je	.L229
	cmpl	$9474, %edx
	je	.L230
	cmpl	$9532, %edx
	je	.L231
	leal	-9585(%rdx), %esi
	cmpl	$1, %esi
	jbe	.L526
	leal	-9698(%rdx), %esi
	cmpl	$1, %esi
	jbe	.L527
	cmpl	$9834, %edx
	leaq	.LC1(%rip), %rsi
	jne	.L528
.L132:
	movzbl	(%rsi), %eax
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
	jmp	.L140
.L517:
	cmpq	$0, 72(%rsp)
	je	.L244
	testl	%ebx, %ebx
	jne	.L529
.L244:
	movl	$6, %eax
	jmp	.L156
.L516:
	leaq	1(%r14), %r8
	cmpq	%r8, %rbp
	movq	%r8, 8(%rsp)
	jbe	.L240
	movzbl	1(%r14), %r8d
	subl	$32, %r8d
	cmpl	$95, %r8d
	ja	.L530
	leaq	(%rsi,%rsi,2), %rsi
	salq	$5, %rsi
	addq	%rsi, %r8
	leaq	to_ucs4_comb(%rip), %rsi
	movl	(%rsi,%r8,4), %r8d
	movl	$2, %esi
	jmp	.L160
.L501:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r14, %rax
	addq	%r10, %rax
	cmpq	$4, %rax
	ja	.L45
	addq	$1, %r14
	cmpq	%r10, %rax
	jbe	.L49
.L48:
	movq	%r14, 144(%rsp)
	movzbl	-1(%r14), %edx
	addq	$1, %r14
	movb	%dl, 4(%rbx,%r10)
	addq	$1, %r10
	cmpq	%r10, %rax
	jne	.L48
.L49:
	movl	$7, 8(%rsp)
	jmp	.L14
.L168:
	leal	-8212(%rcx), %edi
	cmpl	$9, %edi
	ja	.L170
	leaq	map.9086(%rip), %rcx
	movzbl	(%rcx,%rdi), %esi
	testb	%sil, %sil
	movb	%sil, 126(%rsp)
	je	.L488
.L458:
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdi
	jmp	.L169
.L167:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-49, (%rdx)
	movl	$32, %edx
	jmp	.L203
.L218:
	movl	$7, 8(%rsp)
	jmp	.L105
.L518:
	movslq	%r9d, %rax
	movq	%rbp, %r14
	jmp	.L156
.L539:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L531
	cmpq	$0, 72(%rsp)
	je	.L261
	testb	$8, 16(%r12)
	jne	.L532
.L186:
	testb	$2, %bl
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	je	.L261
.L459:
	movq	72(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L164
.L529:
	movq	72(%rsp), %rax
	addq	%rsi, %r14
	movl	$6, %r9d
	addq	$1, (%rax)
	jmp	.L162
.L515:
	cmpq	%r15, %r11
	je	.L155
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	.LC4(%rip), %rsi
	jmp	.L132
.L230:
	leaq	.LC3(%rip), %rsi
	jmp	.L132
.L176:
	leal	-8539(%rcx), %edi
	cmpl	$3, %edi
	ja	.L180
	leal	-127(%rcx), %esi
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdi
	movb	%sil, 126(%rsp)
	jmp	.L169
.L524:
	subq	$1, %rdx
	movq	144(%rsp), %rax
	cmpq	80(%rsp), %rax
	movq	%rdx, 152(%rsp)
	jne	.L64
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L530:
	cmpq	$0, 72(%rsp)
	je	.L244
	testl	%ebx, %ebx
	je	.L244
	movq	72(%rsp), %rax
	movq	8(%rsp), %r14
	movl	$6, %r9d
	addq	$1, (%rax)
	jmp	.L159
.L55:
	leal	-8212(%rax), %edx
	cmpl	$9, %edx
	ja	.L57
	leaq	map.9120(%rip), %rax
	movzbl	(%rax,%rdx), %edx
	testb	%dl, %dl
	movb	%dl, 126(%rsp)
	jne	.L446
	cmpq	$0, 72(%rsp)
	je	.L438
	testb	$8, %r11b
	jne	.L472
.L70:
	andb	$2, %r11b
	je	.L438
	movq	144(%rsp), %rdx
.L91:
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	cmpq	80(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L64
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L526:
	leal	103(%rdx), %ecx
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rsi
	movb	%cl, 136(%rsp)
	jmp	.L119
.L231:
	movq	%r10, %rsi
	jmp	.L132
.L180:
	leal	-8592(%rcx), %edi
	cmpl	$3, %edi
	ja	.L181
	leal	28(%rcx), %esi
	movb	%sil, 126(%rsp)
	jmp	.L458
.L511:
	leaq	152(%rsp), %rsi
	addq	$1, %rdx
	leaq	(%rsi,%rdx), %r8
	movq	%r8, 8(%rsp)
	leaq	1(%rsi), %r8
	cmpq	%r8, 8(%rsp)
	ja	.L30
	leaq	2(%rsi), %rcx
	cmpq	%rcx, 8(%rsp)
	je	.L533
	movslq	%r10d, %r10
	movq	%rdx, %rcx
	andl	$-8, %eax
	subq	%r10, %rcx
	addq	%rcx, %r14
	movq	16(%rsp), %rcx
	movq	%r14, (%rcx)
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jle	.L534
	cmpq	$4, %rdx
	ja	.L535
	orl	%edx, %eax
	testq	%rdx, %rdx
	movl	%eax, (%rbx)
	je	.L49
	xorl	%eax, %eax
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L49
.L536:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L536
	jmp	.L49
.L512:
	cmpq	$0, 72(%rsp)
	je	.L438
	andl	$2, %r11d
	je	.L438
	leaq	152(%rsp), %rsi
	movq	72(%rsp), %rdi
	addq	%rsi, %rax
	addq	$1, (%rdi)
	cmpq	%rsi, %rax
	jne	.L35
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$9472, %ecx
	je	.L253
	cmpl	$9474, %ecx
	je	.L254
	cmpl	$9532, %ecx
	je	.L255
	leal	-9585(%rcx), %edi
	cmpl	$1, %edi
	jbe	.L537
	leal	-9698(%rcx), %edi
	cmpl	$1, %edi
	jbe	.L538
	cmpl	$9834, %ecx
	leaq	.LC1(%rip), %rdi
	jne	.L539
.L182:
	movzbl	(%rdi), %ecx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L30:
	movzbl	153(%rsp), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L540
	leaq	(%r9,%r9,2), %rdx
	salq	$5, %rdx
	addq	%rdx, %rax
	leaq	to_ucs4_comb(%rip), %rdx
	movl	(%rdx,%rax,4), %edx
	movl	$2, %eax
	jmp	.L36
.L514:
	cmpq	%r15, %r11
	jne	.L153
.L152:
	leaq	__PRETTY_FUNCTION__.9185(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L472:
	movq	%r10, 104(%rsp)
	movl	%r11d, 100(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	movl	%eax, %esi
	popq	%rcx
	popq	%rdi
	movq	144(%rsp), %rax
	cmpl	$6, %esi
	movl	100(%rsp), %r11d
	movq	%rax, %rdx
	je	.L88
	cmpl	$5, %esi
	movq	104(%rsp), %r10
	je	.L62
.L63:
	cmpq	80(%rsp), %rdx
	movq	%rdx, %rax
	jne	.L64
	cmpl	$7, 8(%rsp)
	jne	.L99
	addq	$4, %rdx
	cmpq	%rdx, 88(%rsp)
	je	.L541
	movl	(%rbx), %eax
	movq	%r10, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	16(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r10
	jle	.L542
	cmpq	$4, %r10
	ja	.L543
	orl	%r10d, %eax
	testq	%r10, %r10
	movl	%eax, (%rbx)
	je	.L49
	movq	80(%rsp), %rcx
	xorl	%eax, %eax
.L103:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L103
	jmp	.L49
.L527:
	leal	-8(%rdx), %ecx
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rsi
	movb	%cl, 136(%rsp)
	jmp	.L119
.L545:
	leal	-8(%rax), %edx
	movb	%dl, 126(%rsp)
.L446:
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rcx
	jmp	.L56
.L68:
	leal	-8539(%rax), %edx
	cmpl	$3, %edx
	ja	.L76
	leal	-127(%rax), %edx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rcx
	movb	%dl, 126(%rsp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L88:
	andl	$2, %r11d
	jne	.L91
.L452:
	cmpq	80(%rsp), %rax
	jne	.L64
	jmp	.L438
.L499:
	leaq	__PRETTY_FUNCTION__.9185(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L540:
	cmpq	$0, 72(%rsp)
	je	.L438
	andb	$2, %r11b
	je	.L438
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	movq	%r8, %rax
	jmp	.L35
.L76:
	leal	-8592(%rax), %edx
	cmpl	$3, %edx
	ja	.L77
	leal	28(%rax), %edx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rcx
	movb	%dl, 126(%rsp)
	jmp	.L56
.L521:
	movl	%r11d, 88(%rsp)
	movq	%r10, 80(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rbx
	popq	%rdx
	movq	80(%rsp), %r10
	movl	88(%rsp), %r11d
	je	.L136
	cmpl	$5, %eax
	movq	152(%rsp), %rbx
	movq	144(%rsp), %rax
	jne	.L114
	jmp	.L115
.L533:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L503:
	movq	80(%rsp), %rdx
	jmp	.L91
.L543:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L542:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L541:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L520:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L114
.L77:
	cmpl	$9472, %eax
	je	.L211
	cmpl	$9474, %eax
	je	.L212
	cmpl	$9532, %eax
	je	.L213
	leal	-9585(%rax), %edx
	cmpl	$1, %edx
	jbe	.L544
	leal	-9698(%rax), %edx
	cmpl	$1, %edx
	jbe	.L545
	cmpl	$9834, %eax
	leaq	.LC1(%rip), %rdx
	jne	.L546
.L78:
	movzbl	(%rdx), %eax
	leaq	1(%r15), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r15)
	jmp	.L92
.L535:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L534:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L537:
	leal	103(%rcx), %esi
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdi
	movb	%sil, 126(%rsp)
	jmp	.L169
.L255:
	leaq	.LC2(%rip), %rdi
	jmp	.L182
.L254:
	leaq	.LC3(%rip), %rdi
	jmp	.L182
.L253:
	leaq	.LC4(%rip), %rdi
	jmp	.L182
.L240:
	movl	$7, %eax
	jmp	.L156
.L519:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L42:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L198:
	leaq	__PRETTY_FUNCTION__.9185(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L45:
	leaq	__PRETTY_FUNCTION__.9106(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L25:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L522:
	leaq	__PRETTY_FUNCTION__.9185(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L513:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L538:
	leal	-8(%rcx), %esi
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdi
	movb	%sil, 126(%rsp)
	jmp	.L169
.L532:
	movq	%r11, 80(%rsp)
	movl	%r10d, 8(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movl	8(%rsp), %r10d
	movq	80(%rsp), %r11
	je	.L186
	cmpl	$5, %eax
	je	.L187
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L164
.L531:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r14
	jmp	.L164
.L187:
	movq	136(%rsp), %rdx
	cmpq	152(%rsp), %rdx
	movq	144(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L188
	jmp	.L153
.L546:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L547
	cmpq	$0, 72(%rsp)
	je	.L438
	testb	$8, %r11b
	jne	.L548
	andb	$2, %r11b
	je	.L438
.L86:
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	80(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L64
	jmp	.L438
.L548:
	movq	%r10, 104(%rsp)
	movl	%r11d, 100(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movl	100(%rsp), %r11d
	je	.L83
	cmpl	$5, %eax
	je	.L84
	movq	144(%rsp), %rdx
	movq	104(%rsp), %r10
	jmp	.L63
.L547:
	movq	80(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L64
.L84:
	movq	144(%rsp), %rax
	cmpq	80(%rsp), %rax
	jne	.L64
	jmp	.L94
.L83:
	andb	$2, %r11b
	jne	.L86
	movq	144(%rsp), %rax
	jmp	.L452
.L544:
	leal	103(%rax), %edx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rcx
	movb	%dl, 126(%rsp)
	jmp	.L56
.L213:
	leaq	.LC2(%rip), %rdx
	jmp	.L78
.L212:
	leaq	.LC3(%rip), %rdx
	jmp	.L78
.L211:
	leaq	.LC4(%rip), %rdx
	jmp	.L78
	.size	gconv, .-gconv
	.section	.rodata
	.type	map.9088, @object
	.size	map.9088, 5
map.9088:
	.string	"\324"
	.string	""
	.string	""
	.ascii	"\340"
	.set	map.9123,map.9088
	.align 8
	.type	map.9086, @object
	.size	map.9086, 10
map.9086:
	.string	"\320"
	.string	""
	.string	""
	.string	"\251\271"
	.string	""
	.ascii	"\252\272"
	.set	map.9120,map.9086
	.type	map.9085, @object
	.size	map.9085, 6
map.9085:
	.string	"\306\307\312\316"
	.ascii	"\315"
	.set	map.9119,map.9085
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9106, @object
	.size	__PRETTY_FUNCTION__.9106, 22
__PRETTY_FUNCTION__.9106:
	.string	"to_ansi_x3_110_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9015, @object
	.size	__PRETTY_FUNCTION__.9015, 24
__PRETTY_FUNCTION__.9015:
	.string	"from_ansi_x3_110_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9185, @object
	.size	__PRETTY_FUNCTION__.9185, 6
__PRETTY_FUNCTION__.9185:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 766
from_ucs4:
	.string	""
	.string	""
	.string	"\001"
	.string	"\002"
	.string	"\003"
	.string	"\004"
	.string	"\005"
	.string	"\006"
	.string	"\007"
	.string	"\b"
	.string	"\t"
	.string	"\n"
	.string	"\013"
	.string	"\f"
	.string	"\r"
	.string	"\016"
	.string	"\017"
	.string	"\020"
	.string	"\021"
	.string	"\022"
	.string	"\023"
	.string	"\024"
	.string	"\025"
	.string	"\026"
	.string	"\027"
	.string	"\030"
	.string	"\031"
	.string	"\032"
	.string	"\033"
	.string	"\034"
	.string	"\035"
	.string	"\036"
	.string	"\037"
	.string	" "
	.string	"!"
	.string	"\""
	.string	"\246"
	.string	"\244"
	.string	"%"
	.string	"&"
	.string	"'"
	.string	"("
	.string	")"
	.string	"*"
	.string	"+"
	.string	","
	.string	"-"
	.string	"."
	.string	"/"
	.string	"0"
	.string	"1"
	.string	"2"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"6"
	.string	"7"
	.string	"8"
	.string	"9"
	.string	":"
	.string	";"
	.string	"<"
	.string	"="
	.string	">"
	.string	"?"
	.string	"@"
	.string	"A"
	.string	"B"
	.string	"C"
	.string	"D"
	.string	"E"
	.string	"F"
	.string	"G"
	.string	"H"
	.string	"I"
	.string	"J"
	.string	"K"
	.string	"L"
	.string	"M"
	.string	"N"
	.string	"O"
	.string	"P"
	.string	"Q"
	.string	"R"
	.string	"S"
	.string	"T"
	.string	"U"
	.string	"V"
	.string	"W"
	.string	"X"
	.string	"Y"
	.string	"Z"
	.string	"["
	.string	"\\"
	.string	"]"
	.string	"^"
	.string	"_"
	.string	"`"
	.string	"a"
	.string	"b"
	.string	"c"
	.string	"d"
	.string	"e"
	.string	"f"
	.string	"g"
	.string	"h"
	.string	"i"
	.string	"j"
	.string	"k"
	.string	"l"
	.string	"m"
	.string	"n"
	.string	"o"
	.string	"p"
	.string	"q"
	.string	"r"
	.string	"s"
	.string	"t"
	.string	"u"
	.string	"v"
	.string	"w"
	.string	"x"
	.string	"y"
	.string	"z"
	.string	"{"
	.string	"|"
	.string	"}"
	.string	"~"
	.string	"\177"
	.string	"\200"
	.string	"\201"
	.string	"\202"
	.string	"\203"
	.string	"\204"
	.string	"\205"
	.string	"\206"
	.string	"\207"
	.string	"\210"
	.string	"\211"
	.string	"\212"
	.string	"\213"
	.string	"\214"
	.string	"\215"
	.string	"\216"
	.string	"\217"
	.string	"\220"
	.string	"\221"
	.string	"\222"
	.string	"\223"
	.string	"\224"
	.string	"\225"
	.string	"\226"
	.string	"\227"
	.string	"\230"
	.string	"\231"
	.string	"\232"
	.string	"\233"
	.string	"\234"
	.string	"\235"
	.string	"\236"
	.string	"\237"
	.string	""
	.string	""
	.string	"\241"
	.string	"\242"
	.string	"\243"
	.string	"\250"
	.string	"\245"
	.string	""
	.string	""
	.string	"\247"
	.ascii	"\310 "
	.string	"\323"
	.string	"\343"
	.string	"\253"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\322"
	.ascii	"\305 "
	.string	"\260"
	.string	"\261"
	.string	"\262"
	.string	"\263"
	.ascii	"\302 "
	.string	"\265"
	.string	"\266"
	.string	"\267"
	.ascii	"\313 "
	.string	"\321"
	.string	"\353"
	.string	"\273"
	.string	"\274"
	.string	"\275"
	.string	"\276"
	.string	"\277"
	.ascii	"\301A"
	.ascii	"\302A"
	.ascii	"\303A"
	.ascii	"\304A"
	.ascii	"\310A"
	.ascii	"\312A"
	.string	"\341"
	.ascii	"\313C"
	.ascii	"\301E"
	.ascii	"\302E"
	.ascii	"\303E"
	.ascii	"\310E"
	.ascii	"\301I"
	.ascii	"\302I"
	.ascii	"\303I"
	.ascii	"\310I"
	.string	"\342"
	.ascii	"\304N"
	.ascii	"\301O"
	.ascii	"\302O"
	.ascii	"\303O"
	.ascii	"\304O"
	.ascii	"\310O"
	.string	"\264"
	.string	"\351"
	.ascii	"\301U"
	.ascii	"\302U"
	.ascii	"\303U"
	.ascii	"\310U"
	.ascii	"\302Y"
	.string	"\354"
	.string	"\373"
	.ascii	"\301a"
	.ascii	"\302a"
	.ascii	"\303a"
	.ascii	"\304a"
	.ascii	"\310a"
	.ascii	"\312a"
	.string	"\361"
	.ascii	"\313c"
	.ascii	"\301e"
	.ascii	"\302e"
	.ascii	"\303e"
	.ascii	"\310e"
	.ascii	"\301i"
	.ascii	"\302i"
	.ascii	"\303i"
	.ascii	"\310i"
	.string	"\363"
	.ascii	"\304n"
	.ascii	"\301o"
	.ascii	"\302o"
	.ascii	"\303o"
	.ascii	"\304o"
	.ascii	"\310o"
	.string	"\270"
	.string	"\371"
	.ascii	"\301u"
	.ascii	"\302u"
	.ascii	"\303u"
	.ascii	"\310u"
	.ascii	"\302y"
	.string	"\374"
	.ascii	"\310y"
	.ascii	"\305A"
	.ascii	"\305a"
	.ascii	"\306A"
	.ascii	"\306a"
	.ascii	"\316A"
	.ascii	"\316a"
	.ascii	"\302C"
	.ascii	"\302c"
	.ascii	"\303C"
	.ascii	"\303c"
	.ascii	"\307C"
	.ascii	"\307c"
	.ascii	"\317C"
	.ascii	"\317c"
	.ascii	"\317D"
	.ascii	"\317d"
	.string	""
	.string	""
	.string	"\362"
	.ascii	"\305E"
	.ascii	"\305e"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\307E"
	.ascii	"\307e"
	.ascii	"\316E"
	.ascii	"\316e"
	.ascii	"\317E"
	.ascii	"\317e"
	.ascii	"\303G"
	.ascii	"\303g"
	.ascii	"\306G"
	.ascii	"\306g"
	.ascii	"\307G"
	.ascii	"\307g"
	.ascii	"\313G"
	.ascii	"\313g"
	.ascii	"\303H"
	.ascii	"\303h"
	.string	"\344"
	.string	"\364"
	.ascii	"\304I"
	.ascii	"\304i"
	.ascii	"\305I"
	.ascii	"\305i"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\316I"
	.ascii	"\316i"
	.ascii	"\307I"
	.string	"\365"
	.string	"\346"
	.string	"\366"
	.ascii	"\303J"
	.ascii	"\303j"
	.ascii	"\313K"
	.ascii	"\313k"
	.string	"\360"
	.ascii	"\302L"
	.ascii	"\302l"
	.ascii	"\313L"
	.ascii	"\313l"
	.ascii	"\317L"
	.ascii	"\317l"
	.string	"\347"
	.string	"\367"
	.string	"\350"
	.string	"\370"
	.ascii	"\302N"
	.ascii	"\302n"
	.ascii	"\313N"
	.ascii	"\313n"
	.ascii	"\317N"
	.ascii	"\317n"
	.string	"\357"
	.string	"\356"
	.string	"\376"
	.ascii	"\305O"
	.ascii	"\305o"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\315O"
	.ascii	"\315o"
	.string	"\352"
	.string	"\372"
	.ascii	"\302R"
	.ascii	"\302r"
	.ascii	"\313R"
	.ascii	"\313r"
	.ascii	"\317R"
	.ascii	"\317r"
	.ascii	"\302S"
	.ascii	"\302s"
	.ascii	"\303S"
	.ascii	"\303s"
	.ascii	"\313S"
	.ascii	"\313s"
	.ascii	"\317S"
	.ascii	"\317s"
	.ascii	"\313T"
	.ascii	"\313t"
	.ascii	"\317T"
	.ascii	"\317t"
	.string	"\355"
	.string	"\375"
	.ascii	"\304U"
	.ascii	"\304u"
	.ascii	"\305U"
	.ascii	"\305u"
	.ascii	"\306U"
	.ascii	"\306u"
	.ascii	"\312U"
	.ascii	"\312u"
	.ascii	"\315U"
	.ascii	"\315u"
	.ascii	"\316U"
	.ascii	"\316u"
	.ascii	"\303W"
	.ascii	"\303w"
	.ascii	"\303Y"
	.ascii	"\303y"
	.ascii	"\310Y"
	.ascii	"\302Z"
	.ascii	"\302z"
	.ascii	"\307Z"
	.ascii	"\307z"
	.ascii	"\317Z"
	.ascii	"\317z"
	.align 32
	.type	to_ucs4_comb, @object
	.size	to_ucs4_comb, 5760
to_ucs4_comb:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	192
	.long	0
	.long	0
	.long	0
	.long	200
	.long	0
	.long	0
	.long	0
	.long	204
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	210
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	217
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	224
	.long	0
	.long	0
	.long	0
	.long	232
	.long	0
	.long	0
	.long	0
	.long	236
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	242
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	249
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	180
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	193
	.long	0
	.long	262
	.long	0
	.long	201
	.long	0
	.long	0
	.long	0
	.long	205
	.long	0
	.long	0
	.long	313
	.long	0
	.long	323
	.long	211
	.long	0
	.long	0
	.long	340
	.long	346
	.long	0
	.long	218
	.long	0
	.long	0
	.long	0
	.long	221
	.long	377
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	225
	.long	0
	.long	263
	.long	0
	.long	233
	.long	0
	.long	0
	.long	0
	.long	237
	.long	0
	.long	0
	.long	314
	.long	0
	.long	324
	.long	243
	.long	0
	.long	0
	.long	341
	.long	347
	.long	0
	.long	250
	.long	0
	.long	0
	.long	0
	.long	253
	.long	378
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	194
	.long	0
	.long	264
	.long	0
	.long	202
	.long	0
	.long	284
	.long	292
	.long	206
	.long	308
	.long	0
	.long	0
	.long	0
	.long	0
	.long	212
	.long	0
	.long	0
	.long	0
	.long	348
	.long	0
	.long	219
	.long	0
	.long	372
	.long	0
	.long	374
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	226
	.long	0
	.long	265
	.long	0
	.long	234
	.long	0
	.long	285
	.long	293
	.long	238
	.long	309
	.long	0
	.long	0
	.long	0
	.long	0
	.long	244
	.long	0
	.long	0
	.long	0
	.long	349
	.long	0
	.long	251
	.long	0
	.long	373
	.long	0
	.long	375
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	195
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	296
	.long	0
	.long	0
	.long	0
	.long	0
	.long	209
	.long	213
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	360
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	227
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	297
	.long	0
	.long	0
	.long	0
	.long	0
	.long	241
	.long	245
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	361
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	175
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	256
	.long	0
	.long	0
	.long	0
	.long	274
	.long	0
	.long	0
	.long	0
	.long	298
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	332
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	362
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	257
	.long	0
	.long	0
	.long	0
	.long	275
	.long	0
	.long	0
	.long	0
	.long	299
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	333
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	363
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	728
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	258
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	286
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	364
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	259
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	287
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	365
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	729
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	266
	.long	0
	.long	278
	.long	0
	.long	288
	.long	0
	.long	304
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	379
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	267
	.long	0
	.long	279
	.long	0
	.long	289
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	380
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	168
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	196
	.long	0
	.long	0
	.long	0
	.long	203
	.long	0
	.long	0
	.long	0
	.long	207
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	214
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	220
	.long	0
	.long	0
	.long	0
	.long	376
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	228
	.long	0
	.long	0
	.long	0
	.long	235
	.long	0
	.long	0
	.long	0
	.long	239
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	246
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	252
	.long	0
	.long	0
	.long	0
	.long	255
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	730
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	197
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	366
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	229
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	367
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	184
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	199
	.long	0
	.long	0
	.long	0
	.long	290
	.long	0
	.long	0
	.long	0
	.long	310
	.long	315
	.long	0
	.long	325
	.long	0
	.long	0
	.long	0
	.long	342
	.long	350
	.long	354
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	231
	.long	0
	.long	0
	.long	0
	.long	291
	.long	0
	.long	0
	.long	0
	.long	311
	.long	316
	.long	0
	.long	326
	.long	0
	.long	0
	.long	0
	.long	343
	.long	351
	.long	355
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	733
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	336
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	368
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	337
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	369
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	731
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	260
	.long	0
	.long	0
	.long	0
	.long	280
	.long	0
	.long	0
	.long	0
	.long	302
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	370
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	261
	.long	0
	.long	0
	.long	0
	.long	281
	.long	0
	.long	0
	.long	0
	.long	303
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	371
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	711
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	268
	.long	270
	.long	282
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	317
	.long	0
	.long	327
	.long	0
	.long	0
	.long	0
	.long	344
	.long	352
	.long	356
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	381
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	269
	.long	271
	.long	283
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	318
	.long	0
	.long	328
	.long	0
	.long	0
	.long	0
	.long	345
	.long	353
	.long	357
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	382
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	0
	.long	0
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	0
	.long	161
	.long	162
	.long	163
	.long	36
	.long	165
	.long	35
	.long	167
	.long	164
	.long	8216
	.long	8220
	.long	171
	.long	8592
	.long	8593
	.long	8594
	.long	8595
	.long	176
	.long	177
	.long	178
	.long	179
	.long	215
	.long	181
	.long	182
	.long	183
	.long	247
	.long	8217
	.long	8221
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8212
	.long	185
	.long	174
	.long	169
	.long	8482
	.long	9834
	.long	9472
	.long	9474
	.long	9585
	.long	9586
	.long	9698
	.long	9699
	.long	8539
	.long	8540
	.long	8541
	.long	8542
	.long	8486
	.long	198
	.long	208
	.long	170
	.long	294
	.long	9532
	.long	306
	.long	319
	.long	321
	.long	216
	.long	338
	.long	186
	.long	222
	.long	358
	.long	330
	.long	329
	.long	312
	.long	230
	.long	273
	.long	240
	.long	295
	.long	305
	.long	307
	.long	320
	.long	322
	.long	248
	.long	339
	.long	223
	.long	254
	.long	359
	.long	331
	.long	0
