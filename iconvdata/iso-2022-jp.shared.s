	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-JP//"
.LC1:
	.string	"ISO-2022-JP-2//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	pushq	%r12
	pushq	%rbp
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	movq	24(%rdi), %rbp
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L9
	movq	32(%rbx), %r12
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L15
	movl	$1, %ebp
.L3:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L8
	movl	$1, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869188, %rax
	movq	%rax, 72(%rbx)
	movabsq	$25769803777, %rax
	movq	%rax, 80(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %ebp
.L2:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L16
.L8:
	movl	$3, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$2, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869185, %rax
	movq	%rax, 72(%rbx)
	addq	$3, %rax
	movq	%rax, 80(%rbx)
.L5:
	movl	$1, 88(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L11
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L12
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$2, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$2, %ebp
	jmp	.L3
	.size	gconv_init, .-gconv_init
	.p2align 4,,15
	.globl	gconv_end
	.type	gconv_end, @function
gconv_end:
	movq	96(%rdi), %rdi
	jmp	free@PLT
	.size	gconv_end, .-gconv_end
	.section	.rodata.str1.1
.LC2:
	.string	"(%"
.LC3:
	.string	"#$"
.LC4:
	.string	"#~"
.LC5:
	.string	"!j"
.LC6:
	.string	"!i"
.LC7:
	.string	"!a"
.LC8:
	.string	"!b"
.LC9:
	.string	"!n"
.LC10:
	.string	"!o"
.LC11:
	.string	"!q"
.LC12:
	.string	"!r"
.LC13:
	.string	"!p"
.LC14:
	.string	"!s"
.LC15:
	.string	"!t"
.LC16:
	.string	"!w"
.LC17:
	.string	"!x"
.LC18:
	.string	"!u"
.LC19:
	.string	"!v"
.LC20:
	.string	"!P"
.LC21:
	.string	"!%"
.LC22:
	.string	"!&"
.LC23:
	.string	"(8"
.LC24:
	.string	"(7"
.LC25:
	.string	"(6"
.LC26:
	.string	"(5"
.LC27:
	.string	"(3"
.LC28:
	.string	"(/"
.LC29:
	.string	"(+"
.LC30:
	.string	"(#"
.LC31:
	.string	"(1"
.LC32:
	.string	"(-"
.LC33:
	.string	"()"
.LC34:
	.string	"('"
.LC35:
	.string	"../iconv/skeleton.c"
.LC36:
	.string	"outbufstart == NULL"
.LC37:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.section	.rodata.str1.1
.LC39:
	.string	"set == KSC5601_set"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC41:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC42:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC43:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC44:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC45:
	.string	"jis0212.h"
.LC46:
	.string	"cp[1] != '\\0'"
.LC47:
	.string	"gb2312.h"
.LC48:
	.string	"var == iso2022jp2"
.LC49:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC51:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	leaq	104(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$408, %rsp
	movq	%rax, 112(%rsp)
	leaq	48(%rsi), %rax
	movq	%rdx, 56(%rsp)
	movq	%rdi, 160(%rsp)
	movq	%r8, 80(%rsp)
	movq	%rax, 120(%rsp)
	movl	16(%rsi), %eax
	movq	%r9, 96(%rsp)
	movl	464(%rsp), %r12d
	movq	$0, 104(%rsp)
	movl	%eax, %edx
	movl	%eax, 92(%rsp)
	andl	$1, %edx
	jne	.L19
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rax
	movq	%rax, 104(%rsp)
	je	.L19
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 104(%rsp)
.L19:
	testl	%r12d, %r12d
	jne	.L1998
	movq	56(%rsp), %rax
	movq	80(%rsp), %rdi
	leaq	336(%rsp), %rdx
	movl	472(%rsp), %r9d
	movq	8(%rbp), %rbx
	movq	(%rax), %rax
	testq	%rdi, %rdi
	movq	%rax, 48(%rsp)
	movq	%rdi, %rax
	cmove	%rbp, %rax
	cmpq	$0, 96(%rsp)
	movq	(%rax), %rax
	movq	$0, 336(%rsp)
	movq	%rax, 24(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%r9d, %r9d
	movq	%rax, 128(%rsp)
	movq	160(%rsp), %rax
	movq	96(%rax), %rax
	movl	(%rax), %esi
	movl	4(%rax), %eax
	movl	%eax, 16(%rsp)
	movq	32(%rbp), %rax
	movl	%esi, 304(%rsp)
	movq	%rax, 64(%rsp)
	movl	(%rax), %eax
	movl	%eax, 40(%rsp)
	jne	.L1999
.L36:
	movq	%r13, 8(%rsp)
	movl	16(%rsp), %r13d
	leaq	.L556(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L360:
	movl	40(%rsp), %eax
	movl	%eax, %esi
	andl	$192, %eax
	andl	$56, %esi
	cmpl	$2, 304(%rsp)
	movl	%eax, 88(%rsp)
	movl	%esi, 72(%rsp)
	je	.L2000
	movq	48(%rsp), %rdx
	movq	24(%rsp), %r14
	movl	40(%rsp), %r11d
	movl	88(%rsp), %eax
	movl	72(%rsp), %r10d
	movl	$4, 36(%rsp)
	movq	%rdx, 368(%rsp)
	movq	%r14, 376(%rsp)
	andl	$1792, %r11d
	movl	%eax, 16(%rsp)
.L410:
	cmpq	%rdx, 8(%rsp)
	je	.L411
.L684:
	leaq	4(%rdx), %rax
	cmpq	%rax, 8(%rsp)
	jb	.L1166
	cmpq	%r14, %rbx
	jbe	.L1277
	cmpl	$2, %r13d
	movl	(%rdx), %ecx
	je	.L2001
.L412:
	testl	%r10d, %r10d
	jne	.L431
	cmpl	$127, %ecx
	ja	.L432
.L426:
	leaq	1(%r14), %rax
	cmpl	$2, %r13d
	movq	%rax, 376(%rsp)
	movb	%cl, (%r14)
	jne	.L435
	cmpl	$10, %ecx
	movq	376(%rsp), %r14
	jne	.L435
.L1936:
	movl	$0, 16(%rsp)
.L1935:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L441:
	movq	368(%rsp), %rax
	leaq	4(%rax), %rdx
	cmpq	%rdx, 8(%rsp)
	movq	%rdx, 368(%rsp)
	jne	.L684
	.p2align 4,,10
	.p2align 3
.L411:
	movq	56(%rsp), %rax
	movq	64(%rsp), %rdi
	movq	%rdx, (%rax)
	movl	16(%rsp), %eax
	orl	%r10d, %eax
	orl	%r11d, %eax
	cmpq	$0, 80(%rsp)
	movl	%eax, (%rdi)
	jne	.L2002
.L685:
	addl	$1, 20(%rbp)
	testb	$1, 16(%rbp)
	jne	.L2003
	cmpq	%r14, 24(%rsp)
	jnb	.L1207
	movq	104(%rsp), %r15
	movq	0(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, 344(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	472(%rsp), %eax
	leaq	344(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	pushq	%rax
	pushq	$0
	movq	112(%rsp), %r9
	movq	136(%rsp), %rsi
	movq	128(%rsp), %rdi
	call	*%r15
	movl	%eax, %r15d
	cmpl	$4, %r15d
	popq	%rax
	popq	%rdx
	je	.L689
	movq	344(%rsp), %r10
	cmpq	%r14, %r10
	jne	.L2004
.L688:
	testl	%r15d, %r15d
	jne	.L1274
.L1010:
	movq	56(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 48(%rsp)
	movq	64(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 40(%rsp)
	movl	16(%rbp), %eax
	movl	%eax, 92(%rsp)
	movq	0(%rbp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L431:
	cmpl	$24, %r10d
	je	.L2005
	cmpl	$32, %r10d
	je	.L2006
	leal	-8(%r10), %eax
	andl	$-9, %eax
	jne	.L451
	testl	$1536, %r11d
	jne	.L2007
.L448:
	movq	%rbx, %rax
	subq	%r14, %rax
	cmpq	$1, %rax
	jbe	.L1277
	leal	-162(%rcx), %eax
	cmpl	$85, %eax
	ja	.L453
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rsi
	movzbl	(%rsi), %eax
.L454:
	testb	%al, %al
	je	.L437
	movb	%al, (%r14)
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r14)
.L458:
	movq	376(%rsp), %rax
	leaq	2(%rax), %r14
	movq	%r14, 376(%rsp)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2001:
	movl	%ecx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	je	.L2008
	cmpl	$1023, %r11d
	jle	.L412
	testl	%r10d, %r10d
	je	.L2009
	cmpl	$24, %r10d
	je	.L2010
	cmpl	$32, %r10d
	je	.L443
	leal	-8(%r10), %eax
	xorl	%r11d, %r11d
	andl	$-9, %eax
	je	.L448
	cmpl	$56, %r10d
	je	.L449
	cmpl	$40, %r10d
	je	.L2011
	cmpl	$48, %r10d
	jne	.L532
	xorl	%r11d, %r11d
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L432:
	testl	%r11d, %r11d
	jne	.L1039
.L532:
	cmpl	$64, 16(%rsp)
	je	.L2012
.L1856:
	xorl	%r11d, %r11d
	cmpl	$128, 16(%rsp)
	je	.L2013
.L445:
	cmpl	$127, %ecx
	ja	.L1039
.L1038:
	movq	376(%rsp), %r14
	leaq	3(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L1938
	leaq	1(%r14), %rax
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$40, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$66, (%rax)
	movq	376(%rsp), %r14
	leaq	1(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L2014
	cmpl	$2, %r13d
	movq	%rax, 376(%rsp)
	movb	%cl, (%r14)
	movq	376(%rsp), %r14
	jne	.L1935
	cmpl	$10, %ecx
	je	.L1936
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	48(%rsp), %rax
	cmpq	%rax, 8(%rsp)
	movq	24(%rsp), %r14
	je	.L1142
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbx
	jb	.L1143
	movl	92(%rsp), %r11d
	movl	72(%rsp), %edi
	leaq	iso88597_to_ucs4(%rip), %r10
	movl	88(%rsp), %r8d
	movl	$4, 36(%rsp)
	andl	$2, %r11d
.L368:
	movzbl	(%rax), %edx
	cmpb	$27, %dl
	movl	%edx, %ecx
	movl	%edx, %r9d
	je	.L2015
	cmpl	$127, %edx
	jbe	.L2016
.L399:
	cmpq	$0, 128(%rsp)
	je	.L1165
	testl	%r11d, %r11d
	jne	.L2017
.L1165:
	movl	$6, 36(%rsp)
.L367:
	movq	56(%rsp), %rsi
	orl	%r8d, %edi
	cmpq	$0, 80(%rsp)
	movq	%rax, (%rsi)
	movq	64(%rsp), %rax
	movl	%edi, (%rax)
	je	.L685
.L2002:
	movq	80(%rsp), %rax
	movq	%r14, (%rax)
.L18:
	movl	36(%rsp), %eax
	addq	$408, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2005:
	testl	%r11d, %r11d
	jne	.L2018
.L429:
	cmpl	$165, %ecx
	je	.L1177
	cmpl	$8254, %ecx
	je	.L1178
	cmpl	$125, %ecx
	ja	.L439
	cmpl	$92, %ecx
	movl	%ecx, %eax
	je	.L439
.L440:
	leal	-33(%rax), %esi
	cmpb	$94, %sil
	jbe	.L438
.L437:
	testl	%r11d, %r11d
	jne	.L445
	cmpl	$64, 16(%rsp)
	jne	.L1856
.L2012:
	leal	-128(%rcx), %eax
	cmpl	$127, %eax
	ja	.L1195
	leaq	3(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L1196
	leaq	1(%r14), %rax
	andl	$127, %ecx
	xorl	%r11d, %r11d
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$78, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	%cl, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2013:
	cmpl	$65534, %ecx
	ja	.L1039
	cmpl	$189, %ecx
	jbe	.L1193
	movl	$890, %eax
	leaq	from_idx(%rip), %rdi
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L548:
	movzwl	10(%rsi), %eax
	movq	%rsi, %rdi
.L549:
	cmpl	%eax, %ecx
	leaq	8(%rdi), %rsi
	ja	.L548
	movzwl	8(%rdi), %eax
.L547:
	cmpl	%ecx, %eax
	jbe	.L2019
.L1195:
	xorl	%r11d, %r11d
	cmpl	$127, %ecx
	jbe	.L1038
.L1039:
	cmpl	$2, %r13d
	movl	$2, %esi
	je	.L2020
.L554:
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rax
	leal	-65281(%rcx), %edx
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leal	-128(%rcx), %r14d
	movl	%edx, 312(%rsp)
	leaq	(%rax,%rdx,2), %rax
	leal	-19968(%rcx), %edx
	movq	%rax, 296(%rsp)
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rax
	movl	%edx, 176(%rsp)
	leaq	(%rax,%rdx,2), %rax
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdx
	movq	%rax, 288(%rsp)
	leal	-12288(%rcx), %eax
	leaq	(%rdx,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdx
	movq	%rax, 280(%rsp)
	leal	-9312(%rcx), %eax
	leaq	(%rdx,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdx
	movq	%rax, 272(%rsp)
	leal	-8451(%rcx), %eax
	leaq	(%rdx,%rax,2), %rax
	leal	-8213(%rcx), %edx
	movq	%rax, 264(%rsp)
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rax
	movl	%edx, 216(%rsp)
	leaq	(%rax,%rdx,2), %rax
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdx
	movq	%rax, 256(%rsp)
	leal	-1025(%rcx), %eax
	leaq	(%rdx,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdx
	movq	%rax, 248(%rsp)
	leal	-913(%rcx), %eax
	movl	%eax, 184(%rsp)
	addq	%rax, %rax
	addq	%rax, %rdi
	addq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rax
	movq	%rdi, 240(%rsp)
	leal	-164(%rcx), %edi
	movl	%edi, 308(%rsp)
	leaq	(%rdx,%rdi,2), %rdi
	leal	-162(%rcx), %edx
	movq	%rax, 208(%rsp)
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rax
	movq	%rdi, 232(%rsp)
	movl	%edx, 168(%rsp)
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, 192(%rsp)
	leal	-65377(%rcx), %eax
	movl	%eax, 136(%rsp)
	leal	-44032(%rcx), %eax
	movl	%eax, 144(%rsp)
	leal	-63744(%rcx), %eax
	movl	%eax, 152(%rsp)
	leal	-12832(%rcx), %eax
	movl	%eax, 224(%rsp)
	leal	-9472(%rcx), %eax
	movl	%eax, 316(%rsp)
	leal	-160(%rcx), %eax
	movl	%eax, 200(%rsp)
	.p2align 4,,10
	.p2align 3
.L680:
	movl	%esi, %eax
	andl	$7, %eax
	cmpl	$5, %eax
	ja	.L220
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L556:
	.long	.L220-.L556
	.long	.L555-.L556
	.long	.L557-.L556
	.long	.L558-.L556
	.long	.L559-.L556
	.long	.L560-.L556
	.text
	.p2align 4,,10
	.p2align 3
.L1166:
	movl	$7, 36(%rsp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L435:
	movq	376(%rsp), %r14
	xorl	%r10d, %r10d
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L689:
	movl	36(%rsp), %r15d
	cmpl	$5, %r15d
	jne	.L688
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	$5, 36(%rsp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L2006:
	testl	%r11d, %r11d
	jne	.L2021
.L443:
	cmpl	$165, %ecx
	je	.L532
	xorl	%r11d, %r11d
	cmpl	$8254, %ecx
	je	.L532
.L1067:
	cmpl	$125, %ecx
	ja	.L446
	cmpl	$92, %ecx
	movl	%ecx, %eax
	je	.L446
.L447:
	leal	95(%rax), %esi
	cmpb	$62, %sil
	ja	.L437
	leaq	1(%r14), %rdx
	addl	$-128, %eax
	movl	$32, %r10d
	movq	%rdx, 376(%rsp)
	movb	%al, (%r14)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L451:
	cmpl	$56, %r10d
	je	.L2022
.L459:
	cmpl	$40, %r10d
	je	.L2023
	cmpl	$48, %r10d
	jne	.L437
	testl	$1280, %r11d
	jne	.L445
.L531:
	movq	%rbx, %rax
	subq	%r14, %rax
	movq	%rax, 136(%rsp)
	leal	-44032(%rcx), %eax
	cmpl	$11171, %eax
	jbe	.L2024
	leal	-19968(%rcx), %eax
	xorl	%edi, %edi
	cmpl	$20991, %eax
	jbe	.L1303
	leal	-63744(%rcx), %eax
	cmpl	$267, %eax
	jbe	.L1303
	movl	$988, %esi
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r15
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L2025:
	leal	-1(%rax), %esi
.L545:
	cmpl	%esi, %edi
	jg	.L437
.L538:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r15,%r8,4), %r9d
	cmpl	%r9d, %ecx
	jb	.L2025
	jbe	.L546
	leal	1(%rax), %edi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L560:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$62, 136(%rsp)
	jbe	.L2026
.L565:
	shrl	$3, %esi
	testl	%esi, %esi
	jne	.L680
	cmpq	$0, 128(%rsp)
	je	.L2027
	orl	16(%rsp), %r11d
	movq	64(%rsp), %rax
	orl	%r10d, %r11d
	movl	%r11d, (%rax)
	testb	$8, 16(%rbp)
	jne	.L682
	movl	%r11d, %eax
	movl	%r11d, %r10d
	andl	$1792, %r11d
	andl	$192, %eax
	andl	$56, %r10d
	movl	%eax, 16(%rsp)
.L683:
	testb	$2, 92(%rsp)
	movq	368(%rsp), %rdx
	movq	376(%rsp), %r14
	jne	.L2028
.L1939:
	movl	$6, 36(%rsp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L559:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$11171, 144(%rsp)
	jbe	.L2029
	xorl	%edi, %edi
	cmpl	$267, 152(%rsp)
	jbe	.L1305
	cmpl	$20991, 176(%rsp)
	jbe	.L1305
	movl	$988, %edx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r15
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L2030:
	leal	-1(%rax), %edx
.L672:
	cmpl	%edi, %edx
	jl	.L565
.L665:
	leal	(%rdi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r15,%r8,4), %r9d
	cmpl	%r9d, %ecx
	jb	.L2030
	jbe	.L673
	leal	1(%rax), %edi
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L557:
	cmpl	$165, %ecx
	je	.L1199
	cmpl	$8254, %ecx
	je	.L2031
	cmpl	$85, 168(%rsp)
	ja	.L577
	movq	192(%rsp), %rdx
	movzbl	(%rdx), %eax
.L578:
	testb	%al, %al
	je	.L582
.L2043:
	cmpl	$16, %r10d
	movzbl	1(%rdx), %ecx
	movq	376(%rsp), %r14
	je	.L583
	leaq	3(%r14), %rdx
	cmpq	%rdx, %rbx
	jb	.L1938
	leaq	1(%r14), %rdx
	movq	%rdx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$36, (%rdx)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$66, (%rdx)
	movq	376(%rsp), %r14
.L583:
	leaq	2(%r14), %rdx
	cmpq	%rdx, %rbx
	jb	.L2032
	leaq	1(%r14), %rdx
	movl	$16, %r10d
	movq	%rdx, 376(%rsp)
	movb	%al, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	%cl, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L558:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$9371, %ecx
	ja	.L593
	cmpl	$9312, %ecx
	jnb	.L594
	cmpl	$472, %ecx
	je	.L595
	ja	.L596
	cmpl	$333, %ecx
	je	.L597
	jbe	.L2033
	cmpl	$464, %ecx
	je	.L604
	jbe	.L2034
	cmpl	$468, %ecx
	je	.L608
	cmpl	$470, %ecx
	je	.L609
	cmpl	$466, %ecx
	jne	.L565
	leaq	.LC28(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L555:
	cmpl	$127, %r14d
	ja	.L561
	cmpl	$64, 16(%rsp)
	movq	376(%rsp), %r14
	leaq	3(%r14), %rax
	je	.L562
	cmpq	%rax, %rbx
	jb	.L1938
	leaq	1(%r14), %rax
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$46, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$65, (%rax)
	movq	376(%rsp), %r14
	leaq	3(%r14), %rax
.L562:
	cmpq	%rax, %rbx
	jb	.L2035
	leaq	1(%r14), %rax
	addl	$-128, %ecx
	movl	$64, 16(%rsp)
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$78, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	%cl, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L593:
	cmpl	$9792, %ecx
	je	.L623
	ja	.L624
	cmpl	$9670, %ecx
	je	.L625
	jbe	.L2036
	cmpl	$9678, %ecx
	je	.L633
	jbe	.L2037
	cmpl	$9733, %ecx
	je	.L637
	cmpl	$9734, %ecx
	je	.L638
	cmpl	$9679, %ecx
	jne	.L565
	leaq	.LC11(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L561:
	cmpl	$65534, %ecx
	ja	.L565
	cmpl	$189, %ecx
	jbe	.L1198
	movl	$890, %eax
	leaq	from_idx(%rip), %rdi
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L567:
	movzwl	10(%rdx), %eax
	movq	%rdx, %rdi
.L568:
	cmpl	%eax, %ecx
	leaq	8(%rdi), %rdx
	ja	.L567
	movzwl	8(%rdi), %eax
.L566:
	cmpl	%ecx, %eax
	ja	.L565
	movl	200(%rsp), %eax
	addl	4(%rdx), %eax
	leaq	iso88597_from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	je	.L565
	cmpl	$128, 16(%rsp)
	movq	376(%rsp), %r14
	leaq	3(%r14), %rdx
	je	.L569
	cmpq	%rdx, %rbx
	jb	.L1938
	leaq	1(%r14), %rdx
	movq	%rdx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	$46, (%rdx)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	$70, (%rdx)
	movq	376(%rsp), %r14
	leaq	3(%r14), %rdx
.L569:
	cmpq	%rdx, %rbx
	jb	.L2038
	leaq	1(%r14), %rdx
	addl	$-128, %eax
	movl	$128, 16(%rsp)
	movq	%rdx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	$78, (%rdx)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	%al, (%rdx)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2026:
	cmpl	$32, %r10d
	movq	376(%rsp), %r14
	je	.L677
	leaq	3(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L1938
	leaq	1(%r14), %rax
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$40, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$73, (%rax)
	movq	376(%rsp), %r14
.L677:
	leaq	1(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L2039
	subl	$64, %ecx
	movq	%rax, 376(%rsp)
	movl	$32, %r10d
	movb	%cl, (%r14)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	$92, %edx
.L572:
	cmpl	$24, %r10d
	movq	376(%rsp), %r14
	je	.L574
	leaq	3(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L1938
	leaq	1(%r14), %rax
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 376(%rsp)
	movb	$40, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 376(%rsp)
	movb	$74, (%rax)
	movq	376(%rsp), %r14
.L574:
	leaq	1(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L2040
	movq	%rax, 376(%rsp)
	movl	$24, %r10d
	movb	%dl, (%r14)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2029:
	xorl	%r8d, %r8d
	movl	$2349, %eax
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r15
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L2041:
	leal	-1(%rdi), %eax
.L661:
	cmpl	%eax, %r8d
	jg	.L565
.L664:
	leal	(%r8,%rax), %edx
	movl	%edx, %edi
	sarl	%edi
	movslq	%edi, %r9
	movzwl	(%r15,%r9,2), %r9d
	cmpl	%r9d, %ecx
	jb	.L2041
	jbe	.L662
	leal	1(%rdi), %r8d
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L1305:
	movl	$4887, %edx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r15
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L2042:
	leal	-1(%rax), %edx
.L668:
	cmpl	%edx, %edi
	jg	.L565
.L670:
	leal	(%rdi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r15,%r8,4), %r9d
	cmpl	%r9d, %ecx
	jb	.L2042
	jbe	.L673
	leal	1(%rax), %edi
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L577:
	cmpl	$192, 184(%rsp)
	ja	.L579
	movq	208(%rsp), %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L2043
.L582:
	cmpl	$65534, %ecx
	ja	.L565
	cmpl	$1, %r13d
	je	.L565
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edx
	cmpl	%edx, %ecx
	jbe	.L586
	.p2align 4,,10
	.p2align 3
.L587:
	addq	$6, %rax
	movzwl	2(%rax), %edx
	cmpl	%edx, %ecx
	ja	.L587
.L586:
	movzwl	(%rax), %edx
	cmpl	%edx, %ecx
	jb	.L565
	movzwl	4(%rax), %eax
	addl	%ecx, %eax
	subl	%edx, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rdx
	leaq	(%rdx,%rax,2), %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L565
	movzbl	1(%rdx), %edx
	testb	%dl, %dl
	je	.L254
	cmpl	$56, %r10d
	movq	376(%rsp), %r14
	je	.L588
	leaq	4(%r14), %rcx
	cmpq	%rcx, %rbx
	jb	.L1938
	leaq	1(%r14), %rcx
	movq	%rcx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$36, (%rcx)
	movq	376(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$40, (%rcx)
	movq	376(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$68, (%rcx)
	movq	376(%rsp), %r14
.L588:
	leaq	2(%r14), %rcx
	cmpq	%rcx, %rbx
	jb	.L2044
	leaq	1(%r14), %rcx
	movl	$56, %r10d
	movq	%rcx, 376(%rsp)
	movb	%al, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 376(%rsp)
	movb	%dl, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2031:
	movl	$126, %edx
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L596:
	cmpl	$1105, %ecx
	ja	.L611
	cmpl	$1025, %ecx
	jnb	.L612
	cmpl	$711, %ecx
	je	.L613
	ja	.L614
	cmpl	$474, %ecx
	jne	.L2045
	leaq	.LC24(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L673:
	movzbl	2(%r15,%r8,4), %ecx
	movzbl	3(%r15,%r8,4), %edi
.L663:
	cmpl	$48, %r10d
	movq	376(%rsp), %r14
	je	.L674
	leaq	4(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L1938
	leaq	1(%r14), %rax
	movq	%rax, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$36, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$40, (%rax)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	$67, (%rax)
	movq	376(%rsp), %r14
.L674:
	leaq	2(%r14), %rax
	cmpq	%rax, %rbx
	jb	.L2046
	leaq	1(%r14), %rax
	movl	$48, %r10d
	movq	%rax, 376(%rsp)
	movb	%cl, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	%dil, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L624:
	cmpl	$40864, %ecx
	jbe	.L2047
	cmpl	$65504, %ecx
	je	.L646
	jbe	.L2048
	cmpl	$65507, %ecx
	je	.L649
	cmpl	$65509, %ecx
	je	.L650
	cmpl	$65505, %ecx
	jne	.L565
	leaq	.LC5(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L1999:
	andl	$7, %eax
	je	.L36
	testq	%rdi, %rdi
	jne	.L2049
	movl	40(%rsp), %esi
	movl	%esi, %r10d
	andl	$192, %esi
	andl	$56, %r10d
	cmpl	$2, 304(%rsp)
	movl	%esi, 8(%rsp)
	je	.L2050
	movq	48(%rsp), %rsi
	cmpl	$4, %eax
	movq	%rsi, 352(%rsp)
	movq	24(%rsp), %rsi
	movq	%rsi, 360(%rsp)
	ja	.L77
	movq	64(%rsp), %rsi
	cltq
	leaq	332(%rsp), %r14
	movq	%rax, %r15
	xorl	%edx, %edx
.L78:
	movzbl	4(%rsi,%rdx), %ecx
	movb	%cl, (%r14,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L78
	movq	48(%rsp), %rcx
	subq	%rdx, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %r13
	jb	.L2051
	cmpq	%rbx, 24(%rsp)
	jnb	.L1087
	movq	48(%rsp), %rdx
	leaq	331(%rsp), %rdi
	addq	$1, %rdx
.L86:
	movq	%rdx, 352(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %r15
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	$3, %r15
	movb	%cl, (%rdi,%r15)
	ja	.L1291
	cmpq	%rsi, %r13
	ja	.L86
.L1291:
	movl	40(%rsp), %r12d
	movq	%r14, 352(%rsp)
	movl	332(%rsp), %edx
	andl	$1792, %r12d
	cmpl	$2, 16(%rsp)
	je	.L2052
.L88:
	testl	%r10d, %r10d
	je	.L2053
	cmpl	$24, %r10d
	je	.L2054
	cmpl	$32, %r10d
	je	.L2055
	leal	-8(%r10), %eax
	andl	$-16, %eax
	jne	.L114
	testl	$1536, 40(%rsp)
	jne	.L2056
.L111:
	movq	%rbx, %rax
	subq	24(%rsp), %rax
	cmpq	$1, %rax
	jbe	.L1087
	leal	-162(%rdx), %eax
	cmpl	$85, %eax
	ja	.L116
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rax,2), %rax
	movzbl	(%rax), %ecx
.L117:
	testb	%cl, %cl
	je	.L101
	movq	24(%rsp), %rsi
	movb	%cl, (%rsi)
	movzbl	1(%rax), %eax
	movb	%al, 1(%rsi)
.L121:
	addq	$2, 360(%rsp)
.L99:
	movq	352(%rsp), %rax
	leaq	4(%rax), %rdx
	cmpq	%r14, %rdx
	movq	%rdx, 352(%rsp)
	je	.L1929
.L1928:
	movq	64(%rsp), %rax
	movl	(%rax), %esi
	movl	%esi, %eax
	movl	%esi, 40(%rsp)
	andl	$7, %eax
.L90:
	subq	%r14, %rdx
	cmpq	%rax, %rdx
	jle	.L2057
	movq	56(%rsp), %rsi
	subq	%rax, %rdx
	movq	(%rsi), %rax
	addq	%rdx, %rax
	movq	%rax, 48(%rsp)
	movq	%rax, (%rsi)
	movq	360(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L2019:
	movl	4(%rsi), %eax
	leaq	iso88597_from_ucs4(%rip), %rsi
	leal	-160(%rcx,%rax), %eax
	movzbl	(%rsi,%rax), %eax
	testb	%al, %al
	je	.L1195
	leaq	3(%r14), %rcx
	cmpq	%rcx, %rbx
	jb	.L1196
	leaq	1(%r14), %rdx
	andl	$127, %eax
	xorl	%r11d, %r11d
	movq	%rdx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	$78, (%rdx)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 376(%rsp)
	movb	%al, (%rdx)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L579:
	cmpl	$65534, %ecx
	ja	.L565
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edx
	cmpl	%edx, %ecx
	jbe	.L580
	.p2align 4,,10
	.p2align 3
.L581:
	addq	$6, %rax
	movzwl	2(%rax), %edx
	cmpl	%edx, %ecx
	ja	.L581
.L580:
	movzwl	(%rax), %edx
	cmpl	%edx, %ecx
	jb	.L582
	movzwl	4(%rax), %eax
	addl	%ecx, %eax
	subl	%edx, %eax
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rdx
	leaq	(%rdx,%rax,2), %rdx
	movzbl	(%rdx), %eax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L2022:
	testl	%r11d, %r11d
	jne	.L1861
.L449:
	cmpl	$65534, %ecx
	ja	.L532
	movq	%rbx, %rdi
	xorl	%r11d, %r11d
	subq	%r14, %rdi
.L1069:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %ecx
	jbe	.L462
	.p2align 4,,10
	.p2align 3
.L463:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %ecx
	ja	.L463
.L462:
	movzwl	(%rax), %esi
	cmpl	%esi, %ecx
	jb	.L437
	movzwl	4(%rax), %eax
	addl	%ecx, %eax
	subl	%esi, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L437
	movb	%sil, (%r14)
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L254
	cmpq	$1, %rdi
	jbe	.L2058
.L1934:
	movb	%al, 1(%r14)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L453:
	leal	-913(%rcx), %eax
	cmpl	$192, %eax
	jbe	.L2059
	cmpl	$65534, %ecx
	ja	.L437
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %ecx
	jbe	.L456
	.p2align 4,,10
	.p2align 3
.L457:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %ecx
	ja	.L457
.L456:
	movzwl	(%rax), %esi
	cmpl	%esi, %ecx
	jb	.L437
	movzwl	4(%rax), %eax
	addl	%ecx, %eax
	subl	%esi, %eax
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rsi
	movzbl	(%rsi), %eax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L2036:
	cmpl	$9632, %ecx
	je	.L627
	jbe	.L2060
	cmpl	$9650, %ecx
	je	.L630
	cmpl	$9651, %ecx
	je	.L631
	cmpl	$9633, %ecx
	jne	.L565
	leaq	.LC18(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L2033:
	cmpl	$275, %ecx
	je	.L1203
	jbe	.L2061
	cmpl	$283, %ecx
	je	.L602
	cmpl	$299, %ecx
	jne	.L565
	leaq	.LC33(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L603:
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %ecx
.L1040:
	testb	%cl, %cl
	je	.L321
.L1073:
	cmpl	$40, %r10d
	movq	376(%rsp), %r14
	je	.L655
	leaq	3(%r14), %rdx
	cmpq	%rdx, %rbx
	jb	.L1938
	leaq	1(%r14), %rdx
	movq	%rdx, 376(%rsp)
	movb	$27, (%r14)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$36, (%rdx)
	movq	376(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 376(%rsp)
	movb	$65, (%rdx)
	movq	376(%rsp), %r14
.L655:
	leaq	2(%r14), %rdx
	cmpq	%rdx, %rbx
	jb	.L2062
	leaq	1(%r14), %rdx
	movl	$40, %r10d
	movq	%rdx, 376(%rsp)
	movb	%al, (%r14)
	movq	376(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 376(%rsp)
	movb	%cl, (%rax)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2023:
	cmpl	$768, %r11d
	je	.L1070
	testl	%r11d, %r11d
	jne	.L445
.L1070:
	cmpl	$9371, %ecx
	ja	.L468
	cmpl	$9312, %ecx
	jnb	.L469
	cmpl	$472, %ecx
	je	.L470
	ja	.L471
	cmpl	$333, %ecx
	je	.L472
	jbe	.L2063
	cmpl	$464, %ecx
	je	.L479
	jbe	.L2064
	cmpl	$468, %ecx
	je	.L483
	cmpl	$470, %ecx
	jne	.L2065
	leaq	.LC26(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L474:
	movzbl	(%rax), %esi
	movzbl	1(%rax), %edx
.L1037:
	testb	%dl, %dl
	je	.L321
.L1072:
	movq	%rbx, %rdx
	subq	%r14, %rdx
	cmpq	$1, %rdx
	jbe	.L530
	movb	%sil, (%r14)
	movzbl	1(%rax), %eax
	movb	%al, 1(%r14)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L2047:
	cmpl	$19968, %ecx
	jnb	.L641
	cmpl	$12585, %ecx
	ja	.L642
	cmpl	$12288, %ecx
	jnb	.L643
	cmpl	$9794, %ecx
	jne	.L565
	leaq	.LC7(%rip), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L439:
	leal	-65377(%rcx), %eax
	cmpl	$62, %eax
	ja	.L437
	leal	64(%rcx), %eax
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	368(%rsp), %rdx
	movl	$5, 36(%rsp)
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L1178:
	movl	$126, %eax
.L438:
	leaq	1(%r14), %rdx
	movl	$24, %r10d
	movq	%rdx, 376(%rsp)
	movb	%al, (%r14)
	movq	376(%rsp), %r14
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	128(%rsp), %rsi
	addq	$1, %rax
	movl	$6, 36(%rsp)
	addq	$1, (%rsi)
.L376:
	cmpq	%rax, 8(%rsp)
	je	.L367
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbx
	jnb	.L368
	movl	$5, 36(%rsp)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L1177:
	movl	$92, %eax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	96(%rsp), %rbx
	movq	%r14, 0(%rbp)
	movq	336(%rsp), %rax
	movq	8(%rsp), %r13
	addq	%rax, (%rbx)
.L687:
	cmpl	$7, 36(%rsp)
	jne	.L18
	movl	472(%rsp), %eax
	testl	%eax, %eax
	je	.L18
	movq	56(%rsp), %rax
	movq	%r13, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1012
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbp), %rsi
	je	.L1014
.L1013:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1013
.L1014:
	movq	56(%rsp), %rax
	movq	%r13, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	36(%rsp), %r15d
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L2015:
	leaq	2(%rax), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L1163
	cmpl	$2, %r13d
	movzbl	1(%rax), %ecx
	je	.L2066
	cmpb	$40, %cl
	je	.L1064
	cmpb	$36, %cl
	je	.L2067
	.p2align 4,,10
	.p2align 3
.L396:
	addq	$1, %rax
.L386:
	movl	%edx, (%r14)
	movq	%rsi, %r14
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L2008:
	andl	$127, %ecx
	leal	-65(%rcx), %edx
	cmpl	$25, %edx
	ja	.L414
	addl	$32, %ecx
.L415:
	cmpl	$1024, %r11d
	sete	%dl
	cmpl	$106, %ecx
	jne	.L1296
	testb	%dl, %dl
	je	.L1296
	movl	$1280, %r11d
.L416:
	movq	%rax, 368(%rsp)
	movq	%rax, %rdx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L662:
	movl	$-1370734243, %esi
	movl	%edx, %eax
	mull	%esi
	movl	%edi, %eax
	shrl	$7, %edx
	leal	48(%rdx), %ecx
	mull	%esi
	shrl	$6, %edx
	imull	$94, %edx, %edx
	subl	%edx, %edi
	addl	$33, %edi
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L2004:
	movq	56(%rsp), %rax
	movq	48(%rsp), %rsi
	cmpl	$2, 304(%rsp)
	movq	%rsi, (%rax)
	movq	64(%rsp), %rax
	movl	40(%rsp), %esi
	movl	%esi, (%rax)
	je	.L2068
	movl	16(%rbp), %eax
	movq	48(%rsp), %rcx
	movq	24(%rsp), %r11
	movl	40(%rsp), %r9d
	movq	%rbx, 16(%rsp)
	movl	72(%rsp), %ebx
	movl	%eax, 92(%rsp)
	movq	%rcx, 384(%rsp)
	movq	%r11, 392(%rsp)
	andl	$1792, %r9d
	movl	$4, 36(%rsp)
.L735:
	cmpq	%rcx, 8(%rsp)
	je	.L2069
	leaq	4(%rcx), %rax
	cmpq	%rax, 8(%rsp)
	jb	.L1231
	cmpq	%r11, %r10
	jbe	.L1920
	cmpl	$2, %r13d
	movl	(%rcx), %edx
	je	.L2070
.L737:
	testl	%ebx, %ebx
	jne	.L756
	cmpl	$127, %edx
	ja	.L757
.L751:
	leaq	1(%r11), %rax
	cmpl	$2, %r13d
	movq	%rax, 392(%rsp)
	movb	%dl, (%r11)
	jne	.L760
	cmpl	$10, %edx
	movq	392(%rsp), %r11
	jne	.L760
.L1947:
	movl	$0, 88(%rsp)
.L1946:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L766:
	movq	384(%rsp), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, 384(%rsp)
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L446:
	leal	-65377(%rcx), %eax
	cmpl	$62, %eax
	ja	.L437
	leal	64(%rcx), %eax
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L611:
	cmpl	$8869, %ecx
	ja	.L619
	cmpl	$8451, %ecx
	jnb	.L620
	cmpl	$38, 216(%rsp)
	movq	256(%rsp), %rdx
	ja	.L565
	.p2align 4,,10
	.p2align 3
.L601:
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L565
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L2020:
	movl	%r11d, %eax
	leaq	conversion_lists(%rip), %rdx
	sarl	$8, %eax
	cltq
	movl	(%rdx,%rax,4), %esi
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L414:
	cmpl	$1, %ecx
	jne	.L415
	movl	$1024, %r11d
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rsi
	movzbl	(%rsi), %eax
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L2009:
	cmpl	$127, %ecx
	ja	.L532
	xorl	%r11d, %r11d
	jmp	.L426
.L614:
	cmpl	$713, %ecx
	jne	.L2071
	leaq	.LC21(%rip), %rdx
	jmp	.L603
.L2034:
	cmpl	$363, %ecx
	je	.L606
	cmpl	$462, %ecx
	jne	.L565
	leaq	.LC30(%rip), %rdx
	jmp	.L603
.L2037:
	cmpl	$9671, %ecx
	je	.L635
	cmpl	$9675, %ecx
	jne	.L565
	leaq	.LC13(%rip), %rdx
	jmp	.L603
.L468:
	cmpl	$9792, %ecx
	je	.L498
	ja	.L499
	cmpl	$9670, %ecx
	je	.L500
	jbe	.L2072
	cmpl	$9678, %ecx
	je	.L508
	jbe	.L2073
	cmpl	$9733, %ecx
	je	.L512
	cmpl	$9734, %ecx
	jne	.L2074
	leaq	.LC9(%rip), %rax
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L2066:
	cmpb	$36, %cl
	je	.L2075
	cmpb	$40, %cl
	je	.L1064
	cmpb	$46, %cl
	je	.L2076
	cmpb	$78, %cl
	jne	.L396
	cmpl	$64, %r8d
	je	.L2077
	cmpl	$128, %r8d
	jne	.L399
	movzbl	2(%rax), %edx
	leal	-32(%rdx), %ecx
	cmpb	$95, %cl
	ja	.L399
	subl	$32, %edx
	movslq	%edx, %rdx
	movl	(%r10,%rdx,4), %edx
	testl	%edx, %edx
	jne	.L389
	cmpq	$0, 128(%rsp)
	je	.L1165
	testl	%r11d, %r11d
	je	.L1165
	movq	128(%rsp), %rsi
	addq	$3, %rax
	movl	$6, 36(%rsp)
	addq	$1, (%rsi)
	jmp	.L376
.L2050:
	cmpl	$4, %eax
	ja	.L39
	movq	64(%rsp), %r8
	cltq
	leaq	392(%rsp), %rdx
	movq	%rax, %rcx
	xorl	%esi, %esi
.L40:
	movzbl	4(%r8,%rsi), %edi
	movb	%dil, (%rdx,%rsi)
	addq	$1, %rsi
	cmpq	%rax, %rsi
	jne	.L40
	movq	24(%rsp), %rsi
	leaq	4(%rsi), %r11
	cmpq	%r11, %rbx
	jb	.L1087
	movq	48(%rsp), %rsi
	leaq	391(%rsp), %r9
.L41:
	addq	$1, %rsi
	movzbl	-1(%rsi), %edi
	addq	$1, %rcx
	cmpq	$3, %rcx
	setbe	%r8b
	cmpq	%rsi, %r13
	movb	%dil, (%r9,%rcx)
	seta	%dil
	testb	%dil, %r8b
	jne	.L41
	movzbl	392(%rsp), %esi
	leaq	(%rdx,%rcx), %r9
	cmpb	$27, %sil
	movl	%esi, %edi
	movl	%esi, %r8d
	je	.L2078
	cmpl	$127, %esi
	jbe	.L2079
.L1290:
	cmpq	$0, 128(%rsp)
	movl	$6, 36(%rsp)
	je	.L18
	testb	$2, 92(%rsp)
	je	.L18
	movq	128(%rsp), %rdi
	leaq	1(%rdx), %rcx
	addq	$1, (%rdi)
.L50:
	subq	%rdx, %rcx
	cmpq	%rax, %rcx
	jle	.L2080
	subq	%rax, %rcx
	addq	%rcx, 48(%rsp)
	movq	48(%rsp), %rax
	movq	56(%rsp), %rdi
	movq	%rax, (%rdi)
.L1930:
	movl	40(%rsp), %esi
	movq	64(%rsp), %rax
	andl	$-8, %esi
	movl	%esi, (%rax)
	movl	16(%rbp), %eax
	movl	%esi, 40(%rsp)
	movl	%eax, 92(%rsp)
	jmp	.L36
.L2068:
	movq	48(%rsp), %rdx
	cmpq	%rdx, 8(%rsp)
	movl	16(%rbp), %r8d
	movq	24(%rsp), %r11
	je	.L1208
	leaq	4(%r11), %rdi
	cmpq	%rdi, %r10
	jb	.L1209
	movq	%rbx, 16(%rsp)
	movq	128(%rsp), %rbx
	leaq	iso88597_to_ucs4(%rip), %r9
	movl	$4, %eax
	andl	$2, %r8d
.L693:
	movzbl	(%rdx), %ecx
	cmpb	$27, %cl
	movl	%ecx, %esi
	movl	%ecx, %r14d
	je	.L2081
	cmpl	$127, %ecx
	jbe	.L2082
.L724:
	testq	%rbx, %rbx
	je	.L1230
	testl	%r8d, %r8d
	jne	.L2083
.L1230:
	movq	16(%rsp), %rbx
	movq	%rdx, 48(%rsp)
	movl	$6, %eax
.L692:
	movq	56(%rsp), %rsi
	movq	48(%rsp), %rdi
	movl	72(%rsp), %edx
	orl	88(%rsp), %edx
	movq	%rdi, (%rsi)
	movq	64(%rsp), %rsi
	movl	%edx, (%rsi)
	jmp	.L734
.L642:
	cmpl	$9, 224(%rsp)
	ja	.L565
	addl	$69, %ecx
	movl	$34, %eax
	jmp	.L1073
.L2048:
	cmpl	$93, 312(%rsp)
	movq	296(%rsp), %rdx
	jbe	.L601
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L2060:
	cmpl	$75, 316(%rsp)
	ja	.L565
	addl	$36, %ecx
	movl	$41, %eax
	jmp	.L1073
.L1203:
	leaq	.LC2(%rip), %rdx
	movl	$40, %eax
.L599:
	movzbl	1(%rdx), %ecx
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	368(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %rsi
	pushq	136(%rsp)
	movq	72(%rsp), %rax
	movq	24(%rsp), %r8
	movq	176(%rsp), %rdi
	leaq	392(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	80(%rsp), %rsi
	movl	%eax, 52(%rsp)
	movl	(%rsi), %r11d
	movl	%r11d, %r10d
	movl	%r11d, %esi
	andl	$1792, %r11d
	andl	$192, %esi
	andl	$56, %r10d
	cmpl	$6, %eax
	movl	%esi, 32(%rsp)
	popq	%rcx
	popq	%rsi
	je	.L683
	cmpl	$5, %eax
	movq	368(%rsp), %rdx
	movq	376(%rsp), %r14
	jne	.L410
	jmp	.L411
.L2032:
	movq	368(%rsp), %rdx
	movl	$16, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L1135:
	movl	$92, %edx
.L238:
	cmpl	$24, %r10d
	movq	360(%rsp), %rax
	je	.L240
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$40, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$74, (%rax)
	movq	360(%rsp), %rax
.L240:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rbx
	jnb	.L1925
.L230:
	movq	352(%rsp), %rdx
	cmpq	%r14, %rdx
	jne	.L1928
.L1087:
	movl	$5, 36(%rsp)
	jmp	.L18
.L1064:
	movzbl	2(%rax), %ecx
	cmpb	$66, %cl
	je	.L2084
	cmpb	$74, %cl
	je	.L2085
	cmpl	$2, %r13d
	jne	.L396
	cmpb	$73, %cl
	jne	.L396
	addq	$3, %rax
	movl	$32, %edi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L756:
	cmpl	$24, %ebx
	je	.L2086
	cmpl	$32, %ebx
	je	.L2087
	leal	-8(%rbx), %eax
	andl	$-16, %eax
	jne	.L776
	testl	$1536, %r9d
	jne	.L2088
.L773:
	movq	%r10, %rax
	subq	%r11, %rax
	cmpq	$1, %rax
	jbe	.L1920
	leal	-162(%rdx), %eax
	cmpl	$85, %eax
	ja	.L778
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
.L779:
	testb	%sil, %sil
	je	.L762
.L1942:
	movb	%sil, (%r11)
	movzbl	1(%rax), %eax
	movb	%al, 1(%r11)
.L783:
	movq	392(%rsp), %rax
	leaq	2(%rax), %r11
	movq	%r11, 392(%rsp)
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L1198:
	movl	$160, %eax
	leaq	from_idx(%rip), %rdx
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L2070:
	movl	%edx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	je	.L2089
	cmpl	$1023, %r9d
	jle	.L737
	testl	%ebx, %ebx
	jne	.L750
	cmpl	$127, %edx
	ja	.L857
	xorl	%r9d, %r9d
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	24(%rsp), %r14
	movl	88(%rsp), %r8d
	movl	72(%rsp), %edi
	movq	48(%rsp), %rax
	movl	$5, 36(%rsp)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L2018:
	cmpl	$256, %r11d
	jne	.L437
	jmp	.L429
.L2035:
	movq	368(%rsp), %rdx
	movl	$64, 16(%rsp)
	movl	$5, 36(%rsp)
	jmp	.L411
.L2062:
	movq	368(%rsp), %rdx
	movl	$40, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L1193:
	movl	$160, %eax
	leaq	from_idx(%rip), %rsi
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L2016:
	testl	%edi, %edi
	je	.L396
	cmpl	$32, %edx
	jbe	.L396
	cmpl	$127, %edx
	je	.L396
	cmpl	$24, %edi
	je	.L2090
	cmpl	$32, %edi
	je	.L2091
	leal	-8(%rdi), %edx
	andl	$-9, %edx
	jne	.L398
	cmpl	$32, %r9d
	jle	.L399
	movq	8(%rsp), %rdx
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L1163
	movzbl	1(%rax), %edx
	leal	-33(%rdx), %ecx
	cmpl	$93, %ecx
	ja	.L399
	leal	-33(%r9), %edx
	imull	$94, %edx, %edx
	addl	%ecx, %edx
	cmpl	$7807, %edx
	jg	.L399
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	testw	%dx, %dx
	je	.L399
.L1960:
	leaq	2(%rax), %rcx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	368(%rsp), %rdx
	movl	$24, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2090:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edx
.L1989:
	testl	%edx, %edx
	sete	%r9b
	testb	%cl, %cl
	setne	%cl
	testb	%cl, %r9b
	jne	.L399
	cmpl	$65533, %edx
	jne	.L396
	jmp	.L399
.L757:
	testl	%r9d, %r9d
	jne	.L1051
.L857:
	cmpl	$64, 88(%rsp)
	je	.L2092
	xorl	%r9d, %r9d
	cmpl	$128, 88(%rsp)
	je	.L2093
.L770:
	cmpl	$127, %edx
	ja	.L1051
	movq	392(%rsp), %r11
	leaq	3(%r11), %rax
	cmpq	%rax, %r10
	jb	.L1949
	leaq	1(%r11), %rax
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$40, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$66, (%rax)
	movq	392(%rsp), %r11
	leaq	1(%r11), %rax
	cmpq	%rax, %r10
	jb	.L2094
	cmpl	$2, %r13d
	movq	%rax, 392(%rsp)
	movb	%dl, (%r11)
	movq	392(%rsp), %r11
	jne	.L1946
	cmpl	$10, %edx
	je	.L1947
	jmp	.L1946
.L1998:
	cmpq	$0, 80(%rsp)
	jne	.L2095
	cmpl	$1, %r12d
	movq	32(%rbp), %rbx
	jne	.L22
	movl	(%rbx), %r12d
	movq	0(%rbp), %rax
	testl	$-8, %r12d
	je	.L23
	movq	160(%rsp), %rdi
	movq	96(%rdi), %rcx
	cmpl	$2, (%rcx)
	je	.L24
	testb	$56, %r12b
	je	.L24
	leaq	3(%rax), %r13
	cmpq	%r13, 8(%rbp)
	jb	.L1087
	movl	$10267, %r15d
	movb	$66, 2(%rax)
	movw	%r15w, (%rax)
	movq	32(%rbp), %rdx
	andl	$7, (%rdx)
	testb	$1, 16(%rbp)
	jne	.L2096
	cmpq	%r13, %rax
	jnb	.L29
	movq	104(%rsp), %r15
	movq	%rax, 392(%rsp)
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	472(%rsp), %eax
	leaq	392(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	112(%rsp), %r9
	movq	136(%rsp), %rsi
	movq	128(%rsp), %rdi
	call	*%r15
	cmpl	$4, %eax
	movl	%eax, 52(%rsp)
	popq	%rbp
	popq	%r14
	je	.L29
	cmpq	%r13, 392(%rsp)
	jne	.L2097
.L31:
	movl	36(%rsp), %ebx
	testl	%ebx, %ebx
	jne	.L18
.L29:
	movq	104(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	472(%rsp), %eax
	pushq	%rax
	pushq	$1
.L1961:
	movq	112(%rsp), %r9
	movq	136(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	128(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 52(%rsp)
	popq	%r10
	popq	%r11
	jmp	.L18
.L1296:
	cmpl	$97, %ecx
	jne	.L1297
	cmpl	$1280, %r11d
	jne	.L1297
	movl	$256, %r11d
	jmp	.L416
.L2014:
	movq	368(%rsp), %rdx
	xorl	%r10d, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L1196:
	xorl	%r11d, %r11d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2046:
	movq	368(%rsp), %rdx
	movl	$48, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2091:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rdx
	addl	$-128, %ecx
	movzbl	%cl, %r9d
	movl	(%rdx,%r9,4), %edx
	jmp	.L1989
.L2021:
	cmpl	$256, %r11d
	jne	.L437
	cmpl	$165, %ecx
	je	.L445
	cmpl	$8254, %ecx
	jne	.L1067
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	368(%rsp), %rdx
	movl	$32, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2086:
	testl	%r9d, %r9d
	jne	.L2098
.L754:
	cmpl	$165, %edx
	je	.L1242
	cmpl	$8254, %edx
	je	.L1243
	cmpl	$125, %edx
	ja	.L764
	cmpl	$92, %edx
	movl	%edx, %eax
	je	.L764
.L765:
	leal	-33(%rax), %esi
	cmpb	$94, %sil
	jbe	.L763
.L762:
	testl	%r9d, %r9d
	je	.L857
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L398:
	cmpl	$56, %edi
	je	.L2099
	cmpl	$40, %edi
	je	.L2100
	cmpl	$48, %edi
	jne	.L730
	movq	8(%rsp), %rdx
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L1163
	leal	-33(%rcx), %edx
	cmpb	$92, %dl
	ja	.L399
	cmpb	$73, %cl
	je	.L399
	movzbl	1(%rax), %edx
	leal	-33(%rdx), %ecx
	cmpb	$93, %cl
	ja	.L399
	leal	-33(%r9), %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rcx,%rdx), %edx
	leaq	2(%rax), %rcx
	leal	-1410(%rdx), %r9d
	cmpl	$2349, %r9d
	ja	.L406
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rdx
	movslq	%r9d, %r9
	movzwl	(%rdx,%r9,2), %edx
	testw	%dx, %dx
	je	.L399
.L400:
	cmpl	$65533, %edx
	movq	%rcx, %rax
	jne	.L386
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L2007:
	cmpl	$56, %r10d
	jne	.L459
.L1861:
	cmpl	$256, %r11d
	jne	.L445
	movq	%rbx, %rdi
	movl	$256, %r11d
	subq	%r14, %rdi
	cmpl	$65534, %ecx
	jbe	.L1069
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L2071:
	jb	.L565
	cmpl	$56, 184(%rsp)
	movq	240(%rsp), %rdx
	jbe	.L601
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L2045:
	cmpl	$476, %ecx
	jne	.L565
	leaq	.LC23(%rip), %rdx
	jmp	.L603
.L1274:
	movq	8(%rsp), %r13
	movl	%r15d, 36(%rsp)
	jmp	.L687
.L619:
	cmpl	$8978, %ecx
	jne	.L565
	leaq	.LC20(%rip), %rdx
	jmp	.L603
.L2061:
	cmpl	$93, 308(%rsp)
	movq	232(%rsp), %rdx
	jbe	.L601
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L2092:
	leal	-128(%rdx), %eax
	cmpl	$127, %eax
	ja	.L1260
	leaq	3(%r11), %rax
	cmpq	%rax, %r10
	jb	.L1261
	leaq	1(%r11), %rax
	andl	$127, %edx
	xorl	%r9d, %r9d
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$78, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	%dl, (%rax)
	movq	392(%rsp), %r11
	jmp	.L766
.L1260:
	xorl	%r9d, %r9d
	jmp	.L770
.L602:
	leaq	.LC34(%rip), %rdx
	jmp	.L603
.L604:
	leaq	.LC29(%rip), %rdx
	jmp	.L603
.L643:
	movq	280(%rsp), %rdx
	jmp	.L601
.L609:
	leaq	.LC26(%rip), %rdx
	jmp	.L603
.L608:
	leaq	.LC27(%rip), %rdx
	jmp	.L603
.L2121:
	leaq	3(%rax), %rcx
	cmpq	%rcx, 8(%rsp)
	ja	.L373
.L1163:
	movl	$7, 36(%rsp)
	jmp	.L367
.L646:
	leaq	.LC6(%rip), %rdx
	jmp	.L603
.L627:
	leaq	.LC19(%rip), %rdx
	jmp	.L603
.L635:
	leaq	.LC14(%rip), %rdx
	jmp	.L603
.L606:
	leaq	.LC31(%rip), %rdx
	jmp	.L603
.L650:
	leaq	.LC3(%rip), %rdx
	jmp	.L603
.L649:
	leaq	.LC4(%rip), %rdx
	jmp	.L603
.L641:
	movq	288(%rsp), %rdx
	jmp	.L601
.L613:
	leaq	.LC22(%rip), %rdx
	jmp	.L603
.L612:
	movq	248(%rsp), %rdx
	jmp	.L601
.L595:
	leaq	.LC25(%rip), %rdx
	jmp	.L603
.L597:
	leaq	.LC32(%rip), %rdx
	jmp	.L603
.L623:
	leaq	.LC8(%rip), %rdx
	jmp	.L603
.L625:
	leaq	.LC15(%rip), %rdx
	jmp	.L603
.L594:
	movq	272(%rsp), %rdx
	jmp	.L601
.L620:
	movq	264(%rsp), %rdx
	jmp	.L601
.L638:
	leaq	.LC9(%rip), %rdx
	jmp	.L603
.L637:
	leaq	.LC10(%rip), %rdx
	jmp	.L603
.L633:
	leaq	.LC12(%rip), %rdx
	jmp	.L603
.L631:
	leaq	.LC16(%rip), %rdx
	jmp	.L603
.L630:
	leaq	.LC17(%rip), %rdx
	jmp	.L603
.L2024:
	xorl	%r8d, %r8d
	movl	$2349, %edi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r15
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L2101:
	leal	-1(%rsi), %edi
.L535:
	cmpl	%r8d, %edi
	jl	.L437
.L537:
	leal	(%r8,%rdi), %eax
	movl	%eax, %esi
	sarl	%esi
	movslq	%esi, %r9
	movzwl	(%r15,%r9,2), %r9d
	cmpl	%r9d, %ecx
	jb	.L2101
	jbe	.L536
	leal	1(%rsi), %r8d
	jmp	.L535
.L1231:
	movl	%ebx, 72(%rsp)
	movq	16(%rsp), %rbx
	movl	$7, %eax
	movq	%rcx, 48(%rsp)
.L736:
	movq	56(%rsp), %rsi
	movq	48(%rsp), %rdi
	movl	72(%rsp), %edx
	orl	88(%rsp), %edx
	movq	344(%rsp), %r10
	movq	%rdi, (%rsi)
	movq	64(%rsp), %rsi
	orl	%r9d, %edx
	movl	%edx, (%rsi)
.L734:
	cmpq	%r11, %r10
	jne	.L2102
	cmpq	$5, %rax
	jne	.L2103
	cmpq	24(%rsp), %r10
	jne	.L688
	subl	$1, 20(%rbp)
	jmp	.L688
.L1303:
	movl	$4887, %esi
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r15
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L2104:
	leal	-1(%rax), %esi
.L541:
	cmpl	%edi, %esi
	jl	.L437
.L543:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r15,%r8,4), %r9d
	cmpl	%r9d, %ecx
	jb	.L2104
	jbe	.L546
	leal	1(%rax), %edi
	jmp	.L541
.L760:
	movq	392(%rsp), %r11
	xorl	%ebx, %ebx
	jmp	.L766
.L22:
	movq	$0, (%rbx)
	testb	$1, 16(%rbp)
	movl	$0, 36(%rsp)
	jne	.L18
	movq	104(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	472(%rsp), %eax
	pushq	%rax
	pushq	%r12
	jmp	.L1961
.L1297:
	cmpl	$107, %ecx
	jne	.L1298
	testb	%dl, %dl
	je	.L1298
	movl	$1536, %r11d
	jmp	.L416
.L776:
	cmpl	$56, %ebx
	je	.L2105
.L784:
	cmpl	$40, %ebx
	je	.L2106
	cmpl	$48, %ebx
	jne	.L762
	testl	$1280, %r9d
	jne	.L770
.L856:
	movq	%r10, %rax
	subq	%r11, %rax
	movq	%rax, 40(%rsp)
	leal	-44032(%rdx), %eax
	cmpl	$11171, %eax
	jbe	.L2107
	leal	-19968(%rdx), %eax
	xorl	%edi, %edi
	cmpl	$20991, %eax
	jbe	.L1315
	leal	-63744(%rdx), %eax
	cmpl	$267, %eax
	jbe	.L1315
	movl	$988, %r8d
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r14
	movl	%edx, 48(%rsp)
	jmp	.L863
.L2108:
	leal	-1(%rax), %r8d
.L870:
	cmpl	%edi, %r8d
	jl	.L1945
.L863:
	leal	(%rdi,%r8), %eax
	sarl	%eax
	movslq	%eax, %rsi
	movzwl	(%r14,%rsi,4), %edx
	cmpl	%edx, 48(%rsp)
	jb	.L2108
	jbe	.L871
	leal	1(%rax), %edi
	jmp	.L870
.L2053:
	cmpl	$127, %edx
	ja	.L98
.L92:
	movq	24(%rsp), %rdi
	leaq	1(%rdi), %rax
	movq	%rax, 360(%rsp)
	movb	%dl, (%rdi)
	jmp	.L99
.L24:
	movl	%r12d, %ecx
	andl	$7, %ecx
	movl	%ecx, (%rbx)
.L23:
	testl	%edx, %edx
	je	.L29
.L1015:
	movq	%rax, 0(%rbp)
	movl	$0, 36(%rsp)
	jmp	.L18
.L2072:
	cmpl	$9632, %ecx
	je	.L502
	jbe	.L2109
	cmpl	$9650, %ecx
	je	.L505
	cmpl	$9651, %ecx
	jne	.L2110
	leaq	.LC16(%rip), %rax
	jmp	.L474
.L2052:
	movl	%edx, %ecx
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L2111
	cmpl	$1023, %r12d
	jle	.L88
	testl	%r10d, %r10d
	jne	.L91
	cmpl	$127, %edx
	jbe	.L92
.L198:
	cmpl	$64, 8(%rsp)
	je	.L2112
	xorl	%r12d, %r12d
	cmpl	$128, 8(%rsp)
	je	.L2113
.L108:
	cmpl	$127, %edx
	ja	.L1026
	movq	360(%rsp), %rax
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rbx
	jb	.L230
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$40, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$66, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rbx
	jb	.L229
.L1925:
	movq	%rcx, 360(%rsp)
	movb	%dl, (%rax)
	jmp	.L99
.L2099:
	subl	$34, %ecx
	cmpb	$75, %cl
	ja	.L399
	movq	8(%rsp), %rdx
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L1163
	movzbl	1(%rax), %edx
	leal	-33(%rdx), %ecx
	cmpb	$93, %cl
	ja	.L399
	leal	-33(%r9), %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rcx,%rdx), %ecx
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rdx
	movzwl	2(%rdx), %r9d
	cmpl	%r9d, %ecx
	jle	.L402
.L403:
	addq	$6, %rdx
	movzwl	2(%rdx), %r9d
	cmpl	%r9d, %ecx
	jg	.L403
.L402:
	movzwl	(%rdx), %r9d
	cmpl	%r9d, %ecx
	jl	.L399
	movzwl	4(%rdx), %edx
	addl	%ecx, %edx
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rcx
	subl	%r9d, %edx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	testl	%edx, %edx
	je	.L399
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1242:
	movl	$92, %eax
.L763:
	leaq	1(%r11), %rdx
	movl	$24, %ebx
	movq	%rdx, 392(%rsp)
	movb	%al, (%r11)
	movq	392(%rsp), %r11
	jmp	.L766
.L2063:
	cmpl	$275, %ecx
	je	.L1183
	jbe	.L2114
	cmpl	$283, %ecx
	jne	.L2115
	leaq	.LC34(%rip), %rax
	jmp	.L474
.L2083:
	addq	$1, (%rbx)
	addq	$1, %rdx
	movl	$6, %eax
.L701:
	cmpq	%rdx, 8(%rsp)
	je	.L2116
	leaq	4(%r11), %rdi
	cmpq	%rdi, %r10
	jnb	.L693
	movq	16(%rsp), %rbx
	movq	%rdx, 48(%rsp)
	movl	$5, %eax
	jmp	.L692
.L2028:
	movq	128(%rsp), %rax
	addq	$4, %rdx
	movl	$6, 36(%rsp)
	movq	%rdx, 368(%rsp)
	addq	$1, (%rax)
	jmp	.L410
.L1298:
	cmpl	$111, %ecx
	jne	.L1299
	cmpl	$1536, %r11d
	jne	.L1299
	movl	$512, %r11d
	jmp	.L416
.L1949:
	movq	384(%rsp), %rax
	movl	%ebx, 72(%rsp)
	movq	16(%rsp), %rbx
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L220:
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L2077:
	movzbl	2(%rax), %edx
	addq	$3, %rax
	orl	$-128, %edx
	movzbl	%dl, %edx
	jmp	.L386
.L389:
	addq	$3, %rax
	jmp	.L386
.L1005:
	movq	%r10, 40(%rsp)
	leaq	384(%rsp), %rcx
	subq	$8, %rsp
	pushq	136(%rsp)
	movq	72(%rsp), %rax
	movq	%rbp, %rsi
	movq	176(%rsp), %rdi
	movq	24(%rsp), %r8
	leaq	408(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	80(%rsp), %rbx
	movl	%eax, 52(%rsp)
	movl	(%rbx), %r9d
	movl	%r9d, %edi
	movl	%r9d, %ebx
	andl	$1792, %r9d
	andl	$192, %edi
	andl	$56, %ebx
	cmpl	$6, %eax
	movl	%edi, 104(%rsp)
	popq	%rdx
	popq	%rcx
	movq	40(%rsp), %r10
	je	.L1006
	cmpl	$5, %eax
	movq	384(%rsp), %rcx
	movq	392(%rsp), %r11
	jne	.L735
.L1920:
	movl	%ebx, 72(%rsp)
	movq	%rcx, 48(%rsp)
	movl	$5, %eax
	movq	16(%rsp), %rbx
	jmp	.L736
.L471:
	cmpl	$1105, %ecx
	ja	.L486
	cmpl	$1025, %ecx
	jnb	.L487
	cmpl	$711, %ecx
	je	.L488
	ja	.L489
	cmpl	$474, %ecx
	jne	.L2117
	leaq	.LC24(%rip), %rax
	jmp	.L474
.L2010:
	xorl	%r11d, %r11d
	jmp	.L429
.L499:
	cmpl	$40864, %ecx
	jbe	.L2118
	cmpl	$65504, %ecx
	je	.L521
	jbe	.L2119
	cmpl	$65507, %ecx
	je	.L524
	cmpl	$65509, %ecx
	je	.L525
	cmpl	$65505, %ecx
	jne	.L437
	leaq	.LC5(%rip), %rax
	jmp	.L474
.L2087:
	testl	%r9d, %r9d
	jne	.L2120
.L768:
	cmpl	$8254, %edx
	je	.L857
	xorl	%r9d, %r9d
	cmpl	$165, %edx
	je	.L857
.L1077:
	cmpl	$125, %edx
	ja	.L771
	cmpl	$92, %edx
	movl	%edx, %eax
	je	.L771
.L772:
	leal	95(%rax), %esi
	cmpb	$62, %sil
	ja	.L762
	leaq	1(%r11), %rdx
	addl	$-128, %eax
	movl	$32, %ebx
	movq	%rdx, 392(%rsp)
	movb	%al, (%r11)
	movq	392(%rsp), %r11
	jmp	.L766
.L530:
	movq	368(%rsp), %rdx
	movq	376(%rsp), %r14
	movl	$40, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2093:
	cmpl	$65534, %edx
	ja	.L1051
	cmpl	$189, %edx
	jbe	.L1258
	movl	$890, %eax
	leaq	from_idx(%rip), %rdi
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L873:
	movzwl	10(%rsi), %eax
	movq	%rsi, %rdi
.L874:
	cmpl	%eax, %edx
	leaq	8(%rdi), %rsi
	ja	.L873
	movzwl	8(%rdi), %eax
.L872:
	cmpl	%edx, %eax
	ja	.L1260
	movl	4(%rsi), %eax
	leaq	iso88597_from_ucs4(%rip), %rdi
	leal	-160(%rdx,%rax), %eax
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	je	.L1260
	leaq	3(%r11), %rdx
	cmpq	%rdx, %r10
	jb	.L1261
	leaq	1(%r11), %rdx
	andl	$127, %eax
	xorl	%r9d, %r9d
	movq	%rdx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	$78, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	%al, (%rdx)
	movq	392(%rsp), %r11
	jmp	.L766
.L546:
	cmpq	$1, 136(%rsp)
	jbe	.L1188
	movzbl	2(%r15,%r8,4), %eax
	movb	%al, (%r14)
	movzbl	3(%r15,%r8,4), %eax
	jmp	.L1934
.L2075:
	movzbl	2(%rax), %ecx
	cmpb	$40, %cl
	je	.L2121
.L372:
	cmpb	$64, %cl
	je	.L2122
	cmpb	$66, %cl
	je	.L2123
	cmpl	$2, %r13d
	jne	.L396
	cmpb	$65, %cl
	je	.L2124
	cmpb	$40, %cl
	jne	.L396
.L373:
	movzbl	3(%rax), %ecx
	cmpb	$67, %cl
	je	.L2125
	cmpb	$68, %cl
	jne	.L396
	addq	$4, %rax
	movl	$56, %edi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L2067:
	movzbl	2(%rax), %ecx
	jmp	.L372
.L1142:
	movl	88(%rsp), %r8d
	movl	72(%rsp), %edi
	movq	8(%rsp), %rax
	movl	$4, 36(%rsp)
	jmp	.L367
.L2038:
	movq	368(%rsp), %rdx
	movl	$128, 16(%rsp)
	movl	$5, 36(%rsp)
	jmp	.L411
.L2088:
	cmpl	$56, %ebx
	jne	.L784
.L1880:
	cmpl	$256, %r9d
	jne	.L770
	movq	%r10, %rsi
	movl	$256, %r9d
	subq	%r11, %rsi
	cmpl	$65534, %edx
	jbe	.L1079
.L1051:
	cmpl	$2, %r13d
	movl	$2, %ecx
	jne	.L879
	movl	%r9d, %eax
	leaq	conversion_lists(%rip), %rcx
	sarl	$8, %eax
	cltq
	movl	(%rcx,%rax,4), %ecx
.L879:
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rax
	leal	-65281(%rdx), %esi
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	movl	%esi, 280(%rsp)
	leaq	(%rax,%rsi,2), %rax
	leal	-19968(%rdx), %esi
	movq	%rax, 264(%rsp)
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rax
	movl	%esi, 152(%rsp)
	leaq	(%rax,%rsi,2), %rax
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	movq	%rax, 256(%rsp)
	leal	-12288(%rdx), %eax
	leaq	(%rsi,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	movq	%rax, 248(%rsp)
	leal	-9312(%rdx), %eax
	leaq	(%rsi,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	movq	%rax, 240(%rsp)
	leal	-8451(%rdx), %eax
	leaq	(%rsi,%rax,2), %rax
	leal	-8213(%rdx), %esi
	movq	%rax, 232(%rsp)
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rax
	movl	%esi, 288(%rsp)
	leaq	(%rax,%rsi,2), %rax
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	movq	%rax, 224(%rsp)
	leal	-1025(%rdx), %eax
	leaq	(%rsi,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rsi
	movq	%rax, 216(%rsp)
	leal	-913(%rdx), %eax
	movl	%eax, 168(%rsp)
	addq	%rax, %rax
	addq	%rax, %rdi
	addq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rax
	movq	%rdi, 208(%rsp)
	leal	-164(%rdx), %edi
	movl	%edi, 272(%rsp)
	leaq	(%rsi,%rdi,2), %rdi
	leal	-162(%rdx), %esi
	movq	%rax, 192(%rsp)
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rax
	movq	%rdi, 200(%rsp)
	movl	%esi, 144(%rsp)
	leaq	(%rax,%rsi,2), %rax
	movq	%rax, 176(%rsp)
	leal	-65377(%rdx), %eax
	movl	%eax, 40(%rsp)
	leal	-44032(%rdx), %eax
	movl	%eax, 48(%rsp)
	leal	-63744(%rdx), %eax
	movl	%eax, 136(%rsp)
	leal	-12832(%rdx), %eax
	movl	%eax, 296(%rsp)
	leal	-9472(%rdx), %eax
	movl	%eax, 308(%rsp)
	leal	-128(%rdx), %eax
	movl	%eax, 72(%rsp)
	leal	-160(%rdx), %eax
	movl	%eax, 184(%rsp)
	.p2align 4,,10
	.p2align 3
.L1003:
	movl	%ecx, %eax
	andl	$7, %eax
	cmpl	$5, %eax
	ja	.L220
	leaq	.L881(%rip), %rdi
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L881:
	.long	.L220-.L881
	.long	.L880-.L881
	.long	.L882-.L881
	.long	.L883-.L881
	.long	.L884-.L881
	.long	.L885-.L881
	.text
.L885:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$62, 40(%rsp)
	jbe	.L2126
.L890:
	shrl	$3, %ecx
	testl	%ecx, %ecx
	jne	.L1003
	cmpq	$0, 128(%rsp)
	je	.L2127
	orl	88(%rsp), %r9d
	movq	64(%rsp), %rax
	orl	%ebx, %r9d
	movl	%r9d, (%rax)
	testb	$8, 16(%rbp)
	jne	.L1005
	movl	%r9d, %eax
	movl	%r9d, %ebx
	andl	$1792, %r9d
	andl	$192, %eax
	andl	$56, %ebx
	movl	%eax, 88(%rsp)
.L1006:
	testb	$2, 92(%rsp)
	movq	384(%rsp), %rcx
	movq	392(%rsp), %r11
	jne	.L2128
	movl	%ebx, 72(%rsp)
	movq	%rcx, 48(%rsp)
	movl	$6, %eax
	movq	16(%rsp), %rbx
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L884:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$11171, 48(%rsp)
	jbe	.L2129
	xorl	%edi, %edi
	cmpl	$267, 136(%rsp)
	jbe	.L1317
	cmpl	$20991, 152(%rsp)
	jbe	.L1317
	movl	$988, %esi
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r14
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L2130:
	leal	-1(%rax), %esi
.L995:
	cmpl	%esi, %edi
	jg	.L890
.L988:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r14,%r8,4), %r11d
	cmpl	%r11d, %edx
	jb	.L2130
	jbe	.L996
	leal	1(%rax), %edi
	jmp	.L995
.L883:
	cmpl	$2, %r13d
	jne	.L658
	cmpl	$9371, %edx
	ja	.L917
	cmpl	$9312, %edx
	jnb	.L918
	cmpl	$472, %edx
	je	.L919
	ja	.L920
	cmpl	$333, %edx
	je	.L921
	jbe	.L2131
	cmpl	$464, %edx
	je	.L928
	jbe	.L2132
	cmpl	$468, %edx
	je	.L932
	cmpl	$470, %edx
	jne	.L2133
	leaq	.LC26(%rip), %rdx
.L927:
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %edx
.L1052:
	testb	%dl, %dl
	je	.L321
.L1082:
	cmpl	$40, %ebx
	movq	392(%rsp), %r11
	je	.L979
	leaq	3(%r11), %rcx
	cmpq	%rcx, %r10
	jb	.L1949
	leaq	1(%r11), %rcx
	movq	%rcx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$36, (%rcx)
	movq	392(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$65, (%rcx)
	movq	392(%rsp), %r11
.L979:
	leaq	2(%r11), %rcx
	cmpq	%rcx, %r10
	jb	.L2134
	leaq	1(%r11), %rcx
	movl	$40, %ebx
	movq	%rcx, 392(%rsp)
	movb	%al, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	%dl, (%rax)
	movq	392(%rsp), %r11
	jmp	.L766
.L882:
	cmpl	$165, %edx
	je	.L1264
	cmpl	$8254, %edx
	je	.L2135
	cmpl	$85, 144(%rsp)
	ja	.L902
	movq	176(%rsp), %rsi
	movzbl	(%rsi), %eax
.L903:
	testb	%al, %al
	je	.L907
	cmpl	$16, %ebx
	movzbl	1(%rsi), %ecx
	movq	392(%rsp), %r11
	je	.L908
	leaq	3(%r11), %rdx
	cmpq	%rdx, %r10
	jb	.L1949
	leaq	1(%r11), %rdx
	movq	%rdx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$36, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$66, (%rdx)
	movq	392(%rsp), %r11
.L908:
	leaq	2(%r11), %rdx
	cmpq	%rdx, %r10
	jb	.L2136
	leaq	1(%r11), %rdx
	movl	$16, %ebx
	movq	%rdx, 392(%rsp)
	movb	%al, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 392(%rsp)
	movb	%cl, (%rax)
	movq	392(%rsp), %r11
	jmp	.L766
.L880:
	cmpl	$127, 72(%rsp)
	ja	.L886
	cmpl	$64, 88(%rsp)
	movq	392(%rsp), %r11
	leaq	3(%r11), %rax
	je	.L887
	cmpq	%rax, %r10
	jb	.L1949
	leaq	1(%r11), %rax
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$46, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$65, (%rax)
	movq	392(%rsp), %r11
	leaq	3(%r11), %rax
.L887:
	cmpq	%rax, %r10
	jb	.L2137
	leaq	1(%r11), %rax
	addl	$-128, %edx
	movl	$64, 88(%rsp)
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$78, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	%dl, (%rax)
	movq	392(%rsp), %r11
	jmp	.L766
.L886:
	cmpl	$65534, %edx
	ja	.L890
	cmpl	$189, %edx
	jbe	.L1263
	movl	$890, %eax
	leaq	from_idx(%rip), %rdi
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L892:
	movzwl	10(%rsi), %eax
	movq	%rsi, %rdi
.L893:
	cmpl	%eax, %edx
	leaq	8(%rdi), %rsi
	ja	.L892
	movzwl	8(%rdi), %eax
.L891:
	cmpl	%eax, %edx
	jb	.L890
	movl	184(%rsp), %eax
	addl	4(%rsi), %eax
	leaq	iso88597_from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	je	.L890
	cmpl	$128, 88(%rsp)
	movq	392(%rsp), %r11
	leaq	3(%r11), %rdx
	je	.L894
	cmpq	%rdx, %r10
	jb	.L1949
	leaq	1(%r11), %rdx
	movq	%rdx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	$46, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	$70, (%rdx)
	movq	392(%rsp), %r11
	leaq	3(%r11), %rdx
.L894:
	cmpq	%rdx, %r10
	jb	.L2138
	leaq	1(%r11), %rdx
	addl	$-128, %eax
	movl	$128, 88(%rsp)
	movq	%rdx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	$78, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	%al, (%rdx)
	movq	392(%rsp), %r11
	jmp	.L766
.L917:
	cmpl	$9792, %edx
	je	.L947
	ja	.L948
	cmpl	$9670, %edx
	je	.L949
	jbe	.L2139
	cmpl	$9678, %edx
	je	.L957
	jbe	.L2140
	cmpl	$9733, %edx
	je	.L961
	cmpl	$9734, %edx
	jne	.L2141
	leaq	.LC9(%rip), %rdx
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L2126:
	cmpl	$32, %ebx
	movq	392(%rsp), %r11
	je	.L1000
	leaq	3(%r11), %rax
	cmpq	%rax, %r10
	jb	.L1949
	leaq	1(%r11), %rax
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$40, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$73, (%rax)
	movq	392(%rsp), %r11
.L1000:
	leaq	1(%r11), %rax
	cmpq	%rax, %r10
	jb	.L2142
	subl	$64, %edx
	movq	%rax, 392(%rsp)
	movl	$32, %ebx
	movb	%dl, (%r11)
	movq	392(%rsp), %r11
	jmp	.L766
.L1264:
	movl	$92, %edx
.L897:
	cmpl	$24, %ebx
	movq	392(%rsp), %r11
	je	.L899
	leaq	3(%r11), %rax
	cmpq	%rax, %r10
	jb	.L1949
	leaq	1(%r11), %rax
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$40, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$74, (%rax)
	movq	392(%rsp), %r11
.L899:
	leaq	1(%r11), %rax
	cmpq	%rax, %r10
	jb	.L2143
	movq	%rax, 392(%rsp)
	movl	$24, %ebx
	movb	%dl, (%r11)
	movq	392(%rsp), %r11
	jmp	.L766
.L2129:
	xorl	%r8d, %r8d
	movl	$2349, %edi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r14
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L2144:
	leal	-1(%rsi), %edi
.L984:
	cmpl	%r8d, %edi
	jl	.L890
.L987:
	leal	(%r8,%rdi), %eax
	movl	%eax, %esi
	sarl	%esi
	movslq	%esi, %r11
	movzwl	(%r14,%r11,2), %r11d
	cmpl	%r11d, %edx
	jb	.L2144
	jbe	.L985
	leal	1(%rsi), %r8d
	jmp	.L984
.L1317:
	movl	$4887, %esi
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r14
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L2145:
	leal	-1(%rax), %esi
.L991:
	cmpl	%esi, %edi
	jg	.L890
.L993:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r14,%r8,4), %r11d
	cmpl	%r11d, %edx
	jb	.L2145
	jbe	.L996
	leal	1(%rax), %edi
	jmp	.L991
.L902:
	cmpl	$192, 168(%rsp)
	ja	.L904
	movq	192(%rsp), %rsi
	movzbl	(%rsi), %eax
	jmp	.L903
.L907:
	cmpl	$65534, %edx
	ja	.L890
	cmpl	$1, %r13d
	je	.L890
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	jbe	.L911
.L912:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	ja	.L912
.L911:
	movzwl	(%rax), %esi
	cmpl	%esi, %edx
	jb	.L890
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%esi, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L890
	movzbl	1(%rax), %edx
	testb	%dl, %dl
	je	.L254
	cmpl	$56, %ebx
	movq	392(%rsp), %r11
	je	.L913
	leaq	4(%r11), %rax
	cmpq	%rax, %r10
	jb	.L1949
	leaq	1(%r11), %rax
	movq	%rax, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$36, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$40, (%rax)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	$68, (%rax)
	movq	392(%rsp), %r11
.L913:
	leaq	2(%r11), %rax
	cmpq	%rax, %r10
	jb	.L2146
	leaq	1(%r11), %rax
	movl	$56, %ebx
	movq	%rax, 392(%rsp)
	movb	%sil, (%r11)
	movq	392(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 392(%rsp)
	movb	%dl, (%rax)
	movq	392(%rsp), %r11
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L2135:
	movl	$126, %edx
	jmp	.L897
.L948:
	cmpl	$40864, %edx
	jbe	.L2147
	cmpl	$65504, %edx
	je	.L970
	jbe	.L2148
	cmpl	$65507, %edx
	je	.L973
	cmpl	$65509, %edx
	jne	.L2149
	leaq	.LC3(%rip), %rdx
	jmp	.L927
.L920:
	cmpl	$1105, %edx
	ja	.L935
	cmpl	$1025, %edx
	jnb	.L936
	cmpl	$711, %edx
	je	.L937
	ja	.L938
	cmpl	$474, %edx
	jne	.L2150
	leaq	.LC24(%rip), %rdx
	jmp	.L927
.L996:
	movzbl	2(%r14,%r8,4), %ecx
	movzbl	3(%r14,%r8,4), %eax
.L986:
	cmpl	$48, %ebx
	movq	392(%rsp), %r11
	je	.L997
	leaq	4(%r11), %rdx
	cmpq	%rdx, %r10
	jb	.L1949
	leaq	1(%r11), %rdx
	movq	%rdx, 392(%rsp)
	movb	$27, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$36, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$40, (%rdx)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 392(%rsp)
	movb	$67, (%rdx)
	movq	392(%rsp), %r11
.L997:
	leaq	2(%r11), %rdx
	cmpq	%rdx, %r10
	jb	.L2151
	leaq	1(%r11), %rdx
	movl	$48, %ebx
	movq	%rdx, 392(%rsp)
	movb	%cl, (%r11)
	movq	392(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 392(%rsp)
	movb	%al, (%rdx)
	movq	392(%rsp), %r11
	jmp	.L766
.L2118:
	cmpl	$19968, %ecx
	jnb	.L516
	cmpl	$12585, %ecx
	ja	.L517
	cmpl	$12288, %ecx
	jnb	.L518
	cmpl	$9794, %ecx
	leaq	.LC7(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L2084:
	addq	$3, %rax
	xorl	%edi, %edi
	jmp	.L376
.L1188:
	movl	$48, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L904:
	cmpl	$65534, %edx
	ja	.L890
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	jbe	.L905
.L906:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	ja	.L906
.L905:
	movzwl	(%rax), %esi
	cmpl	%esi, %edx
	jb	.L907
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%esi, %eax
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rsi
	movzbl	(%rsi), %eax
	jmp	.L903
.L98:
	testl	%r12d, %r12d
	je	.L198
.L1026:
	cmpl	$2, 16(%rsp)
	movl	$2, %r11d
	jne	.L219
	movl	%r12d, %eax
	leaq	conversion_lists(%rip), %rcx
	sarl	$8, %eax
	cltq
	movl	(%rcx,%rax,4), %r11d
.L219:
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rax
	leal	-65281(%rdx), %ecx
	leal	-913(%rdx), %esi
	movl	%ecx, 248(%rsp)
	movl	%esi, 36(%rsp)
	addq	%rsi, %rsi
	leaq	(%rax,%rcx,2), %rax
	leal	-19968(%rdx), %ecx
	movq	%rax, 232(%rsp)
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rax
	movl	%ecx, 216(%rsp)
	leaq	(%rax,%rcx,2), %rax
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rcx
	movq	%rax, 224(%rsp)
	leal	-12288(%rdx), %eax
	leaq	(%rcx,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rcx
	movq	%rax, 208(%rsp)
	leal	-9312(%rdx), %eax
	leaq	(%rcx,%rax,2), %rax
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rcx
	movq	%rax, 200(%rsp)
	leal	-8451(%rdx), %eax
	leaq	(%rcx,%rax,2), %rax
	leal	-8213(%rdx), %ecx
	movq	%rax, 192(%rsp)
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rax
	movl	%ecx, 256(%rsp)
	leaq	(%rax,%rcx,2), %rax
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rcx
	movq	%rax, 184(%rsp)
	leal	-1025(%rdx), %eax
	leaq	(%rcx,%rax,2), %rax
	leal	-164(%rdx), %ecx
	movq	%rax, 176(%rsp)
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rax
	movl	%ecx, 240(%rsp)
	addq	%rsi, %rax
	addq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rsi
	movq	%rax, 168(%rsp)
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rax
	leaq	(%rax,%rcx,2), %rax
	leal	-162(%rdx), %ecx
	movq	%rsi, 144(%rsp)
	movq	%rax, 152(%rsp)
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rax
	movl	%ecx, 88(%rsp)
	leaq	(%rax,%rcx,2), %rax
	movq	%rax, 136(%rsp)
	leal	-65377(%rdx), %eax
	movl	%eax, 40(%rsp)
	leal	-44032(%rdx), %eax
	movl	%eax, 48(%rsp)
	leal	-63744(%rdx), %eax
	movl	%eax, 264(%rsp)
	leal	-12832(%rdx), %eax
	movl	%eax, 272(%rsp)
	leal	-9472(%rdx), %eax
	movl	%eax, 280(%rsp)
	leal	-128(%rdx), %eax
	movl	%eax, 72(%rsp)
	leal	-160(%rdx), %eax
	movl	%eax, 288(%rsp)
	.p2align 4,,10
	.p2align 3
.L348:
	movl	%r11d, %eax
	andl	$7, %eax
	cmpl	$5, %eax
	ja	.L220
	leaq	.L222(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L222:
	.long	.L220-.L222
	.long	.L221-.L222
	.long	.L223-.L222
	.long	.L224-.L222
	.long	.L225-.L222
	.long	.L226-.L222
	.text
.L226:
	cmpl	$2, 16(%rsp)
	jne	.L326
	cmpl	$62, 40(%rsp)
	jbe	.L2152
.L231:
	shrl	$3, %r11d
	testl	%r11d, %r11d
	jne	.L348
	cmpq	$0, 128(%rsp)
	je	.L2153
	orl	8(%rsp), %r12d
	movq	64(%rsp), %rax
	orl	%r12d, %r10d
	movl	%r10d, (%rax)
	testb	$8, 16(%rbp)
	jne	.L350
.L354:
	testb	$2, 92(%rsp)
	movq	352(%rsp), %rdx
	jne	.L2154
.L1966:
	cmpq	%r14, %rdx
	jne	.L1928
.L356:
	movl	$6, 36(%rsp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	$2, 16(%rsp)
	jne	.L326
	cmpl	$11171, 48(%rsp)
	jbe	.L2155
	xorl	%esi, %esi
	cmpl	$267, 264(%rsp)
	jbe	.L1293
	cmpl	$20991, 216(%rsp)
	jbe	.L1293
	movl	$988, %ecx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L333
.L2156:
	leal	-1(%rax), %ecx
.L340:
	cmpl	%ecx, %esi
	jg	.L231
.L333:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r9,%rdi,4), %r8d
	cmpl	%r8d, %edx
	jb	.L2156
	jbe	.L341
	leal	1(%rax), %esi
	jmp	.L340
.L224:
	cmpl	$2, 16(%rsp)
	jne	.L326
	cmpl	$9371, %edx
	ja	.L260
	cmpl	$9312, %edx
	jnb	.L261
	cmpl	$472, %edx
	je	.L262
	ja	.L263
	cmpl	$333, %edx
	je	.L264
	jbe	.L2157
	cmpl	$464, %edx
	je	.L271
	jbe	.L2158
	cmpl	$468, %edx
	je	.L275
	cmpl	$470, %edx
	jne	.L2159
	leaq	.LC26(%rip), %rax
.L270:
	movzbl	(%rax), %ecx
.L1027:
	movzbl	1(%rax), %edx
	testb	%dl, %dl
	je	.L321
.L266:
	cmpl	$40, %r10d
	movq	360(%rsp), %rax
	je	.L323
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rbx
	jb	.L229
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$36, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$65, (%rax)
.L1992:
	movq	360(%rsp), %rax
.L323:
	leaq	2(%rax), %rsi
	cmpq	%rsi, %rbx
	jb	.L230
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	%cl, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	jmp	.L1925
.L221:
	cmpl	$127, 72(%rsp)
	ja	.L227
	cmpl	$64, 8(%rsp)
	movq	360(%rsp), %rax
	leaq	3(%rax), %rcx
	je	.L228
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$46, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$65, (%rax)
	movq	360(%rsp), %rax
	leaq	3(%rax), %rcx
.L228:
	cmpq	%rbx, %rcx
	ja	.L230
	leaq	1(%rax), %rcx
	addl	$-128, %edx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$78, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	%dl, (%rax)
	jmp	.L99
.L223:
	cmpl	$165, %edx
	je	.L1135
	cmpl	$8254, %edx
	je	.L2160
	cmpl	$85, 88(%rsp)
	ja	.L243
	movq	136(%rsp), %rax
	movzbl	(%rax), %ecx
.L244:
	testb	%cl, %cl
	je	.L248
	cmpl	$16, %r10d
	movzbl	1(%rax), %edx
	movq	360(%rsp), %rax
	je	.L323
	leaq	3(%rax), %rsi
	cmpq	%rsi, %rbx
	jb	.L229
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$36, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$66, (%rax)
	movq	360(%rsp), %rax
	jmp	.L323
.L486:
	cmpl	$8869, %ecx
	ja	.L494
	cmpl	$8451, %ecx
	jnb	.L495
	leal	-8213(%rcx), %eax
	cmpl	$38, %eax
	ja	.L437
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
.L527:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L437
	movzbl	1(%rax), %edx
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L536:
	cmpq	$1, 136(%rsp)
	jbe	.L1188
	movl	$-1370734243, %ecx
	mull	%ecx
	movl	%edx, %eax
	shrl	$7, %eax
	addl	$48, %eax
	movb	%al, (%r14)
	movl	%esi, %eax
	mull	%ecx
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %esi
	movl	%esi, %eax
	addl	$33, %eax
	movb	%al, 1(%r14)
	jmp	.L458
.L2105:
	testl	%r9d, %r9d
	jne	.L1880
.L774:
	cmpl	$65534, %edx
	ja	.L857
	movq	%r10, %rsi
	xorl	%r9d, %r9d
	subq	%r11, %rsi
.L1079:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %edx
	jbe	.L787
.L788:
	addq	$6, %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %edx
	ja	.L788
.L787:
	movzwl	(%rax), %edi
	cmpl	%edi, %edx
	jb	.L762
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%edi, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	movzbl	(%rax), %edi
	testb	%dil, %dil
	je	.L762
	movb	%dil, (%r11)
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L254
	cmpq	$1, %rsi
	jbe	.L2161
.L1944:
	movb	%al, 1(%r11)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	56(%rsp), %rax
	movq	%r13, %rcx
	movq	%r13, (%rax)
	movq	48(%rsp), %rax
	subq	%rax, %rcx
	addq	%rdx, %rcx
	cmpq	$4, %rcx
	ja	.L80
	addq	$1, %rax
	cmpq	%rdx, %rcx
	movq	64(%rsp), %rsi
	jbe	.L84
.L83:
	movq	%rax, 352(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rax
	movb	%dl, 4(%rsi,%r15)
	addq	$1, %r15
	cmpq	%r15, %rcx
	jne	.L83
.L84:
	movl	$7, 36(%rsp)
	jmp	.L18
.L1243:
	movl	$126, %eax
	jmp	.L763
.L2100:
	subl	$33, %r9d
	cmpl	$86, %r9d
	ja	.L399
	movq	8(%rsp), %rdx
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L1163
	movzbl	1(%rax), %edx
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L399
	imull	$94, %r9d, %r9d
	addl	%r9d, %edx
	cmpl	$8177, %edx
	jg	.L399
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	leaq	2(%rax), %rcx
	movzwl	(%r9,%rdx,2), %edx
	testw	%dx, %dx
	je	.L399
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L2085:
	addq	$3, %rax
	movl	$24, %edi
	jmp	.L376
.L227:
	cmpl	$65534, %edx
	ja	.L231
	cmpl	$189, %edx
	jbe	.L1134
	movl	$890, %eax
	leaq	from_idx(%rip), %rsi
	jmp	.L234
.L233:
	movzwl	10(%rcx), %eax
	movq	%rcx, %rsi
.L234:
	cmpl	%eax, %edx
	leaq	8(%rsi), %rcx
	ja	.L233
	movzwl	8(%rsi), %eax
.L232:
	cmpl	%eax, %edx
	jb	.L231
	movl	288(%rsp), %eax
	addl	4(%rcx), %eax
	leaq	iso88597_from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rax), %eax
	testb	%al, %al
	je	.L231
	cmpl	$128, 8(%rsp)
	movq	360(%rsp), %rdx
	leaq	3(%rdx), %rcx
	je	.L235
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$46, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$70, (%rdx)
	movq	360(%rsp), %rdx
	leaq	3(%rdx), %rcx
.L235:
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rdx), %rcx
	addl	$-128, %eax
	movq	%rcx, 360(%rsp)
	movb	$27, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$78, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	%al, (%rdx)
	jmp	.L99
.L260:
	cmpl	$9792, %edx
	je	.L290
	ja	.L291
	cmpl	$9670, %edx
	je	.L292
	jbe	.L2162
	cmpl	$9678, %edx
	je	.L300
	jbe	.L2163
	cmpl	$9733, %edx
	je	.L304
	cmpl	$9734, %edx
	jne	.L2164
	leaq	.LC9(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2076:
	movzbl	2(%rax), %ecx
	cmpb	$65, %cl
	je	.L2165
	cmpb	$70, %cl
	jne	.L396
	addq	$3, %rax
	movl	$128, %r8d
	jmp	.L376
.L778:
	leal	-913(%rdx), %eax
	cmpl	$192, %eax
	ja	.L780
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
	jmp	.L779
.L764:
	leal	-65377(%rdx), %eax
	cmpl	$62, %eax
	ja	.L762
	leal	64(%rdx), %eax
	jmp	.L765
.L2089:
	andl	$127, %edx
	leal	-65(%rdx), %ecx
	cmpl	$25, %ecx
	jbe	.L2166
	cmpl	$1, %edx
	jne	.L740
	movl	$1024, %r9d
.L741:
	movq	%rax, 384(%rsp)
	movq	%rax, %rcx
	jmp	.L735
.L2081:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jbe	.L1228
	cmpl	$2, %r13d
	movzbl	1(%rdx), %esi
	je	.L2167
	cmpb	$40, %sil
	je	.L1074
	cmpb	$36, %sil
	je	.L2168
.L721:
	addq	$1, %rdx
.L711:
	movl	%ecx, (%r11)
	movq	%rdi, %r11
	jmp	.L701
.L2139:
	cmpl	$9632, %edx
	je	.L951
	jbe	.L2169
	cmpl	$9650, %edx
	je	.L954
	cmpl	$9651, %edx
	jne	.L2170
	leaq	.LC16(%rip), %rdx
	jmp	.L927
.L2152:
	cmpl	$32, %r10d
	movq	360(%rsp), %rax
	je	.L345
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$40, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$73, (%rax)
	movq	360(%rsp), %rax
.L345:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rbx
	jb	.L230
	subl	$64, %edx
	movq	%rcx, 360(%rsp)
	movb	%dl, (%rax)
	jmp	.L99
.L985:
	movl	$-1370734243, %edi
	mull	%edi
	movl	%edx, %eax
	shrl	$7, %eax
	leal	48(%rax), %ecx
	movl	%esi, %eax
	mull	%edi
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %esi
	movl	%esi, %eax
	addl	$33, %eax
	jmp	.L986
.L1299:
	cmpl	$122, %ecx
	jne	.L1300
	testb	%dl, %dl
	je	.L1300
	movl	$1792, %r11d
	jmp	.L416
.L2122:
	addq	$3, %rax
	movl	$8, %edi
	jmp	.L376
.L2147:
	cmpl	$19968, %edx
	jnb	.L965
	cmpl	$12585, %edx
	ja	.L966
	cmpl	$12288, %edx
	jnb	.L967
	cmpl	$9794, %edx
	jne	.L890
	leaq	.LC7(%rip), %rdx
	jmp	.L927
.L2131:
	cmpl	$275, %edx
	je	.L1268
	jbe	.L2171
	cmpl	$283, %edx
	jne	.L2172
	leaq	.LC34(%rip), %rdx
	jmp	.L927
.L2106:
	testl	%r9d, %r9d
	je	.L1080
	cmpl	$768, %r9d
	jne	.L770
.L1080:
	cmpl	$9371, %edx
	ja	.L793
	cmpl	$9312, %edx
	jnb	.L794
	cmpl	$472, %edx
	je	.L795
	ja	.L796
	cmpl	$333, %edx
	je	.L797
	jbe	.L2173
	cmpl	$464, %edx
	je	.L804
	jbe	.L2174
	cmpl	$468, %edx
	je	.L808
	cmpl	$470, %edx
	jne	.L2175
	leaq	.LC26(%rip), %rax
.L799:
	movzbl	(%rax), %esi
.L1049:
	cmpb	$0, 1(%rax)
	je	.L321
	movq	%r10, %rdx
	subq	%r11, %rdx
	cmpq	$1, %rdx
	ja	.L1942
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movq	392(%rsp), %r11
	movl	$40, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2064:
	cmpl	$363, %ecx
	jne	.L2176
	leaq	.LC31(%rip), %rax
	jmp	.L474
.L489:
	cmpl	$713, %ecx
	jne	.L2177
	leaq	.LC21(%rip), %rax
	jmp	.L474
.L2054:
	testl	%r12d, %r12d
	jne	.L2178
.L95:
	cmpl	$165, %edx
	je	.L1118
	cmpl	$8254, %edx
	je	.L1119
	cmpl	$125, %edx
	ja	.L103
	cmpl	$92, %edx
	movl	%edx, %eax
	je	.L103
.L104:
	leal	-33(%rax), %ecx
	cmpb	$94, %cl
	jbe	.L102
.L101:
	testl	%r12d, %r12d
	je	.L198
	jmp	.L108
.L2069:
	movq	8(%rsp), %rdi
	movl	%ebx, 72(%rsp)
	movslq	36(%rsp), %rax
	movq	16(%rsp), %rbx
	movq	%rdi, 48(%rsp)
	jmp	.L736
.L1293:
	movl	$4887, %ecx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L338
.L2179:
	leal	-1(%rax), %ecx
.L336:
	cmpl	%ecx, %esi
	jg	.L231
.L338:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r9,%rdi,4), %r8d
	cmpl	%r8d, %edx
	jb	.L2179
	jbe	.L341
	leal	1(%rax), %esi
	jmp	.L336
.L2058:
	movq	368(%rsp), %rdx
	movq	376(%rsp), %r14
	movl	$56, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2073:
	cmpl	$9671, %ecx
	je	.L510
	cmpl	$9675, %ecx
	leaq	.LC13(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L2123:
	addq	$3, %rax
	movl	$16, %edi
	jmp	.L376
.L771:
	leal	-65377(%rdx), %eax
	cmpl	$62, %eax
	ja	.L762
	leal	64(%rdx), %eax
	jmp	.L772
.L2166:
	addl	$32, %edx
.L740:
	cmpl	$1024, %r9d
	sete	%cl
	cmpl	$106, %edx
	jne	.L1308
	testb	%cl, %cl
	je	.L1308
	movl	$1280, %r9d
	jmp	.L741
.L935:
	cmpl	$8869, %edx
	ja	.L943
	cmpl	$8451, %edx
	jnb	.L944
	cmpl	$38, 288(%rsp)
	movq	224(%rsp), %rsi
	ja	.L890
.L925:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L890
.L923:
	movzbl	1(%rsi), %edx
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L2112:
	leal	-128(%rdx), %eax
	cmpl	$127, %eax
	ja	.L1132
	movq	24(%rsp), %rdi
	leaq	3(%rdi), %rax
	cmpq	%rax, %rbx
	jb	.L1087
	leaq	1(%rdi), %rax
	andl	$127, %edx
	movq	%rax, 360(%rsp)
	movb	$27, (%rdi)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	$78, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 360(%rsp)
	movb	%dl, (%rax)
	jmp	.L99
.L2155:
	xorl	%edi, %edi
	movl	$2349, %esi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r9
	jmp	.L332
.L2180:
	leal	-1(%rcx), %esi
.L329:
	cmpl	%esi, %edi
	jg	.L231
.L332:
	leal	(%rdi,%rsi), %eax
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	movzwl	(%r9,%r8,2), %r8d
	cmpl	%r8d, %edx
	jb	.L2180
	jbe	.L330
	leal	1(%rcx), %edi
	jmp	.L329
.L1132:
	xorl	%r12d, %r12d
	jmp	.L108
.L780:
	cmpl	$65534, %edx
	ja	.L762
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%edx, %esi
	jnb	.L781
.L782:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	ja	.L782
.L781:
	movzwl	(%rax), %esi
	cmpl	%esi, %edx
	jb	.L762
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%esi, %eax
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
	jmp	.L779
.L1263:
	movl	$160, %eax
	leaq	from_idx(%rip), %rsi
	jmp	.L891
.L2098:
	cmpl	$256, %r9d
	jne	.L762
	jmp	.L754
.L2011:
	xorl	%r11d, %r11d
	jmp	.L1070
.L2055:
	testl	%r12d, %r12d
	jne	.L2181
.L106:
	cmpl	$165, %edx
	je	.L198
	xorl	%r12d, %r12d
	cmpl	$8254, %edx
	je	.L198
.L1059:
	cmpl	$125, %edx
	ja	.L109
	cmpl	$92, %edx
	movl	%edx, %eax
	je	.L109
.L110:
	leal	95(%rax), %ecx
	cmpb	$62, %cl
	ja	.L101
	movq	24(%rsp), %rdi
	addl	$-128, %eax
	leaq	1(%rdi), %rdx
	movq	%rdx, 360(%rsp)
	movb	%al, (%rdi)
	jmp	.L99
.L248:
	cmpl	$65534, %edx
	ja	.L231
	cmpl	$1, 16(%rsp)
	je	.L231
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	jbe	.L252
.L253:
	addq	$6, %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	ja	.L253
.L252:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %edx
	jb	.L231
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%ecx, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rax,2), %rax
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L231
	movzbl	1(%rax), %edx
	testb	%dl, %dl
	je	.L254
	cmpl	$56, %r10d
	je	.L1992
	movq	360(%rsp), %rax
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rbx
	jb	.L229
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$27, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$36, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$40, (%rax)
	movq	360(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 360(%rsp)
	movb	$68, (%rax)
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L243:
	cmpl	$192, 36(%rsp)
	ja	.L245
	movq	144(%rsp), %rax
	movzbl	(%rax), %ecx
	jmp	.L244
.L2177:
	jb	.L437
	leal	-913(%rcx), %eax
	cmpl	$56, %eax
	ja	.L437
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L350:
	leaq	352(%rsp), %rcx
	subq	$8, %rsp
	leaq	(%r14,%r15), %r12
	pushq	136(%rsp)
	movq	72(%rsp), %rax
	movq	%rbp, %rsi
	movq	176(%rsp), %rdi
	movq	%r12, %r8
	leaq	376(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	cmpl	$6, %eax
	movl	%eax, 52(%rsp)
	popq	%rdi
	popq	%r8
	je	.L354
	movq	352(%rsp), %rdx
	cmpq	%r14, %rdx
	jne	.L1928
	cmpl	$7, 36(%rsp)
	je	.L2182
	cmpl	$0, 36(%rsp)
	jne	.L18
.L1929:
	movq	56(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 48(%rsp)
	movq	64(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 40(%rsp)
	movl	16(%rbp), %eax
	movl	%eax, 92(%rsp)
	jmp	.L36
.L2065:
	cmpl	$466, %ecx
	jne	.L437
	leaq	.LC28(%rip), %rax
	jmp	.L474
.L938:
	cmpl	$713, %edx
	jne	.L2183
	leaq	.LC21(%rip), %rdx
	jmp	.L927
.L1300:
	cmpl	$104, %ecx
	jne	.L1301
	cmpl	$1792, %r11d
	jne	.L1301
	movl	$768, %r11d
	jmp	.L416
.L2113:
	cmpl	$65534, %edx
	ja	.L1026
	cmpl	$189, %edx
	jbe	.L1130
	movl	$890, %eax
	leaq	from_idx(%rip), %rcx
	jmp	.L216
.L215:
	movzwl	10(%rsi), %eax
	movq	%rsi, %rcx
.L216:
	cmpl	%eax, %edx
	leaq	8(%rcx), %rsi
	ja	.L215
	movzwl	8(%rcx), %eax
.L214:
	cmpl	%eax, %edx
	jb	.L1132
	movl	4(%rsi), %eax
	leaq	iso88597_from_ucs4(%rip), %rcx
	leal	-160(%rdx,%rax), %eax
	movzbl	(%rcx,%rax), %eax
	testb	%al, %al
	je	.L1132
	movq	24(%rsp), %rsi
	leaq	3(%rsi), %rdx
	cmpq	%rdx, %rbx
	jb	.L1087
	leaq	1(%rsi), %rdx
	andl	$127, %eax
	movq	%rdx, 360(%rsp)
	movb	$27, (%rsi)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$78, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	%al, (%rdx)
	jmp	.L99
.L2176:
	cmpl	$462, %ecx
	leaq	.LC30(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L291:
	cmpl	$40864, %edx
	jbe	.L2184
	cmpl	$65504, %edx
	je	.L313
	jbe	.L2185
	cmpl	$65507, %edx
	je	.L316
	cmpl	$65509, %edx
	jne	.L2186
	leaq	.LC3(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L517:
	leal	-12832(%rcx), %eax
	cmpl	$9, %eax
	ja	.L437
	addl	$69, %ecx
	movb	$34, 392(%rsp)
	movl	$34, %esi
	movb	%cl, 393(%rsp)
.L528:
	leaq	392(%rsp), %rax
	jmp	.L1072
.L2074:
	cmpl	$9679, %ecx
	jne	.L437
	leaq	.LC11(%rip), %rax
	jmp	.L474
.L263:
	cmpl	$1105, %edx
	ja	.L278
	cmpl	$1025, %edx
	jnb	.L279
	cmpl	$711, %edx
	je	.L280
	ja	.L281
	cmpl	$474, %edx
	jne	.L2187
	leaq	.LC24(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2082:
	cmpl	$0, 72(%rsp)
	je	.L721
	cmpl	$32, %ecx
	jbe	.L721
	cmpl	$127, %ecx
	je	.L721
	cmpl	$24, 72(%rsp)
	je	.L2188
	cmpl	$32, 72(%rsp)
	je	.L2189
	movl	72(%rsp), %ecx
	subl	$8, %ecx
	andl	$-16, %ecx
	jne	.L723
	cmpl	$32, %r14d
	jle	.L724
	movq	8(%rsp), %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L1228
	movzbl	1(%rdx), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L724
	leal	-33(%r14), %esi
	imull	$94, %esi, %esi
	addl	%esi, %ecx
	cmpl	$7807, %ecx
	jg	.L724
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	leaq	2(%rdx), %r14
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L724
.L725:
	cmpl	$65533, %ecx
	movq	%r14, %rdx
	jne	.L711
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L2160:
	movl	$126, %edx
	jmp	.L238
.L793:
	cmpl	$9792, %edx
	je	.L823
	ja	.L824
	cmpl	$9670, %edx
	je	.L825
	jbe	.L2190
	cmpl	$9678, %edx
	je	.L833
	jbe	.L2191
	cmpl	$9733, %edx
	je	.L837
	cmpl	$9734, %edx
	jne	.L2192
	leaq	.LC9(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	24(%rsp), %r11
	movl	$5, %eax
	jmp	.L692
.L2044:
	movq	368(%rsp), %rdx
	movl	$56, %r10d
	movl	$5, 36(%rsp)
	jmp	.L411
.L2137:
	movq	384(%rsp), %rax
	movl	%ebx, 72(%rsp)
	movl	$64, 88(%rsp)
	movq	16(%rsp), %rbx
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2117:
	cmpl	$476, %ecx
	leaq	.LC23(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L2115:
	cmpl	$299, %ecx
	leaq	.LC33(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L750:
	cmpl	$24, %ebx
	je	.L2193
	cmpl	$32, %ebx
	je	.L768
	leal	-8(%rbx), %eax
	xorl	%r9d, %r9d
	andl	$-16, %eax
	je	.L773
	cmpl	$56, %ebx
	je	.L774
	cmpl	$40, %ebx
	je	.L2194
	cmpl	$48, %ebx
	jne	.L857
	xorl	%r9d, %r9d
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L2110:
	cmpl	$9633, %ecx
	jne	.L437
	leaq	.LC18(%rip), %rax
	jmp	.L474
.L2132:
	cmpl	$363, %edx
	jne	.L2195
	leaq	.LC31(%rip), %rdx
	jmp	.L927
.L2027:
	movq	368(%rsp), %rdx
	movq	376(%rsp), %r14
	jmp	.L1939
.L1119:
	movl	$126, %eax
.L102:
	movq	24(%rsp), %rsi
	leaq	1(%rsi), %rdx
	movq	%rdx, 360(%rsp)
	movb	%al, (%rsi)
	jmp	.L99
.L512:
	leaq	.LC10(%rip), %rax
	jmp	.L474
.L508:
	leaq	.LC12(%rip), %rax
	jmp	.L474
.L524:
	leaq	.LC4(%rip), %rax
	jmp	.L474
.L2119:
	leal	-65281(%rcx), %eax
	cmpl	$93, %eax
	ja	.L437
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L521:
	leaq	.LC6(%rip), %rax
	jmp	.L474
.L483:
	leaq	.LC27(%rip), %rax
	jmp	.L474
.L470:
	leaq	.LC25(%rip), %rax
	jmp	.L474
.L469:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	leal	-9312(%rcx), %eax
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L2143:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$24, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L114:
	cmpl	$56, %r10d
	je	.L2196
.L122:
	cmpl	$40, %r10d
	je	.L2197
	cmpl	$48, %r10d
	jne	.L101
	testl	$1280, 40(%rsp)
	jne	.L108
.L197:
	leal	-44032(%rdx), %eax
	movq	%rbx, %r11
	subq	24(%rsp), %r11
	cmpl	$11171, %eax
	jbe	.L2198
	leal	-19968(%rdx), %eax
	xorl	%edi, %edi
	cmpl	$20991, %eax
	jbe	.L1292
	leal	-63744(%rdx), %eax
	cmpl	$267, %eax
	jbe	.L1292
	movl	$988, %r8d
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L2199:
	leal	-1(%rax), %r8d
.L211:
	cmpl	%r8d, %edi
	jg	.L101
.L204:
	leal	(%rdi,%r8), %eax
	sarl	%eax
	movslq	%eax, %rcx
	movzwl	(%r9,%rcx,4), %esi
	cmpl	%esi, %edx
	jb	.L2199
	jbe	.L212
	leal	1(%rax), %edi
	jmp	.L211
.L2140:
	cmpl	$9671, %edx
	je	.L959
	cmpl	$9675, %edx
	jne	.L890
	leaq	.LC13(%rip), %rdx
	jmp	.L927
.L1258:
	movl	$160, %eax
	leaq	from_idx(%rip), %rsi
	jmp	.L872
.L510:
	leaq	.LC14(%rip), %rax
	jmp	.L474
.L1308:
	cmpl	$97, %edx
	jne	.L1309
	cmpl	$1280, %r9d
	jne	.L1309
	movl	$256, %r9d
	jmp	.L741
.L505:
	leaq	.LC17(%rip), %rax
	jmp	.L474
.L498:
	leaq	.LC8(%rip), %rax
	jmp	.L474
.L2134:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$40, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L516:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rsi
	leal	-19968(%rcx), %eax
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L488:
	leaq	.LC22(%rip), %rax
	jmp	.L474
.L518:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	leal	-12288(%rcx), %eax
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L487:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	leal	-1025(%rcx), %eax
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L2114:
	leal	-164(%rcx), %eax
	cmpl	$93, %eax
	ja	.L437
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L1183:
	leaq	.LC2(%rip), %rax
	jmp	.L474
.L495:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	leal	-8451(%rcx), %eax
	leaq	(%rsi,%rax,2), %rax
	jmp	.L527
.L494:
	cmpl	$8978, %ecx
	leaq	.LC20(%rip), %rax
	je	.L474
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %r14
	movslq	%ecx, %rcx
	movl	(%r14,%rcx,4), %ecx
	testl	%ecx, %ecx
	sete	%r14b
	testb	%sil, %sil
	setne	%sil
	testb	%sil, %r14b
	jne	.L724
.L1990:
	cmpl	$65533, %ecx
	jne	.L721
	jmp	.L724
.L2109:
	leal	-9472(%rcx), %eax
	cmpl	$75, %eax
	ja	.L437
	addl	$36, %ecx
	movb	$41, 392(%rsp)
	movl	$41, %esi
	movb	%cl, 393(%rsp)
	jmp	.L528
.L502:
	leaq	.LC19(%rip), %rax
	jmp	.L474
.L341:
	movzbl	2(%r9,%rdi,4), %esi
	movzbl	3(%r9,%rdi,4), %eax
.L331:
	cmpl	$48, %r10d
	movq	360(%rsp), %rdx
	je	.L342
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %rbx
	jb	.L229
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$27, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$36, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$40, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	$67, (%rdx)
	movq	360(%rsp), %rdx
.L342:
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %rbx
	jb	.L230
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	%sil, (%rdx)
	movq	360(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 360(%rsp)
	movb	%al, (%rdx)
	jmp	.L99
.L472:
	leaq	.LC32(%rip), %rax
	jmp	.L474
.L500:
	leaq	.LC15(%rip), %rax
	jmp	.L474
.L525:
	leaq	.LC3(%rip), %rax
	jmp	.L474
.L479:
	leaq	.LC29(%rip), %rax
	jmp	.L474
.L1945:
	movl	48(%rsp), %edx
	jmp	.L762
.L2172:
	cmpl	$299, %edx
	jne	.L890
	leaq	.LC33(%rip), %rdx
	jmp	.L927
.L1261:
	movl	%ebx, 72(%rsp)
	movq	%rcx, 48(%rsp)
	xorl	%r9d, %r9d
	movq	16(%rsp), %rbx
	movl	$5, %eax
	jmp	.L736
.L2189:
	leal	-128(%rsi), %r14d
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rcx
	movzbl	%r14b, %esi
	movl	(%rcx,%rsi,4), %ecx
	testl	%ecx, %ecx
	sete	36(%rsp)
	testb	%r14b, %r14b
	setne	%sil
	testb	%sil, 36(%rsp)
	je	.L1990
	jmp	.L724
.L2162:
	cmpl	$9632, %edx
	je	.L294
	jbe	.L2200
	cmpl	$9650, %edx
	je	.L297
	cmpl	$9651, %edx
	jne	.L2201
	leaq	.LC16(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2170:
	cmpl	$9633, %edx
	jne	.L890
	leaq	.LC18(%rip), %rdx
	jmp	.L927
.L1074:
	movzbl	2(%rdx), %esi
	cmpb	$66, %sil
	je	.L2202
	cmpb	$74, %sil
	je	.L2203
	cmpl	$2, %r13d
	jne	.L721
	cmpb	$73, %sil
	jne	.L721
	addq	$3, %rdx
	movl	$32, 72(%rsp)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L2167:
	cmpb	$36, %sil
	je	.L2204
	cmpb	$40, %sil
	je	.L1074
	cmpb	$46, %sil
	je	.L2205
	cmpb	$78, %sil
	jne	.L721
	cmpl	$64, 88(%rsp)
	je	.L2206
	cmpl	$128, 88(%rsp)
	jne	.L724
	movzbl	2(%rdx), %ecx
	leal	-32(%rcx), %esi
	cmpb	$95, %sil
	ja	.L724
	subl	$32, %ecx
	movslq	%ecx, %rcx
	movl	(%r9,%rcx,4), %ecx
	testl	%ecx, %ecx
	jne	.L714
	testq	%rbx, %rbx
	je	.L1230
	testl	%r8d, %r8d
	je	.L1230
	addq	$3, %rdx
	addq	$1, (%rbx)
	movl	$6, %eax
	jmp	.L701
.L2206:
	movzbl	2(%rdx), %ecx
	addq	$3, %rdx
	orl	$-128, %ecx
	movzbl	%cl, %ecx
	jmp	.L711
.L714:
	addq	$3, %rdx
	jmp	.L711
.L2149:
	cmpl	$65505, %edx
	jne	.L890
	leaq	.LC5(%rip), %rdx
	jmp	.L927
.L2133:
	cmpl	$466, %edx
	jne	.L890
	leaq	.LC28(%rip), %rdx
	jmp	.L927
.L2142:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$32, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L966:
	cmpl	$9, 296(%rsp)
	ja	.L890
	addl	$69, %edx
	movl	$34, %eax
	jmp	.L1082
.L2094:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$0, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L658:
	leaq	__PRETTY_FUNCTION__.9463(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L2195:
	cmpl	$462, %edx
	jne	.L890
	leaq	.LC30(%rip), %rdx
	jmp	.L927
.L245:
	cmpl	$65534, %edx
	ja	.L231
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	jbe	.L246
.L247:
	addq	$6, %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	ja	.L247
.L246:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %edx
	jb	.L248
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%ecx, %eax
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rax,2), %rax
	movzbl	(%rax), %ecx
	jmp	.L244
.L2183:
	jb	.L890
	cmpl	$56, 168(%rsp)
	movq	208(%rsp), %rsi
	jbe	.L925
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L2078:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %r9
	jbe	.L44
	cmpl	$2, 16(%rsp)
	movzbl	393(%rsp), %r8d
	je	.L2207
	cmpb	$40, %r8b
	je	.L2208
	cmpb	$36, %r8b
	leaq	1(%rdx), %rcx
	movl	$27, %esi
	je	.L2209
.L51:
	movq	24(%rsp), %rax
	movq	%r11, 24(%rsp)
	movl	%esi, (%rax)
	movq	64(%rsp), %rax
	movl	(%rax), %edi
	movl	%edi, %eax
	movl	%edi, 40(%rsp)
	andl	$7, %eax
	jmp	.L50
.L2151:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$48, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2141:
	cmpl	$9679, %edx
	jne	.L890
	leaq	.LC11(%rip), %rdx
	jmp	.L927
.L2150:
	cmpl	$476, %edx
	jne	.L890
	leaq	.LC23(%rip), %rdx
	jmp	.L927
.L1118:
	movl	$92, %eax
	jmp	.L102
.L2171:
	cmpl	$93, 272(%rsp)
	movq	200(%rsp), %rsi
	jbe	.L925
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L1268:
	movl	$40, %eax
	leaq	.LC2(%rip), %rsi
	jmp	.L923
.L2157:
	cmpl	$275, %edx
	je	.L1139
	jbe	.L2210
	cmpl	$283, %edx
	jne	.L2211
	leaq	.LC34(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2111:
	leaq	4(%r14), %rdx
	movq	%rdx, 352(%rsp)
	jmp	.L90
.L2120:
	cmpl	$256, %r9d
	jne	.L762
	cmpl	$165, %edx
	je	.L770
	cmpl	$8254, %edx
	jne	.L1077
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L1301:
	cmpl	$1023, %r11d
	jg	.L1302
	cmpl	$127, %ecx
	jne	.L416
.L1302:
	xorl	%r11d, %r11d
	jmp	.L416
.L957:
	leaq	.LC12(%rip), %rdx
	jmp	.L927
.L103:
	leal	-65377(%rdx), %eax
	cmpl	$62, %eax
	ja	.L101
	leal	64(%rdx), %eax
	jmp	.L104
.L961:
	leaq	.LC10(%rip), %rdx
	jmp	.L927
.L937:
	leaq	.LC22(%rip), %rdx
	jmp	.L927
.L947:
	leaq	.LC8(%rip), %rdx
	jmp	.L927
.L954:
	leaq	.LC17(%rip), %rdx
	jmp	.L927
.L2168:
	movzbl	2(%rdx), %esi
.L697:
	cmpb	$64, %sil
	je	.L2212
	cmpb	$66, %sil
	je	.L2213
	cmpl	$2, %r13d
	jne	.L721
	cmpb	$65, %sil
	je	.L2214
	cmpb	$40, %sil
	jne	.L721
.L698:
	movzbl	3(%rdx), %esi
	cmpb	$67, %sil
	je	.L2215
	cmpb	$68, %sil
	jne	.L721
	addq	$4, %rdx
	movl	$56, 72(%rsp)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L2196:
	testl	%r12d, %r12d
	jne	.L2216
.L112:
	cmpl	$65534, %edx
	ja	.L198
	movq	%rbx, %rcx
	subq	24(%rsp), %rcx
	xorl	%r12d, %r12d
.L1061:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	jbe	.L125
.L126:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	ja	.L126
.L125:
	movzwl	(%rax), %esi
	cmpl	%esi, %edx
	jb	.L101
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%esi, %eax
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rax,2), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L101
	movq	24(%rsp), %rdi
	movb	%sil, (%rdi)
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L254
	cmpq	$1, %rcx
	jbe	.L229
	movq	24(%rsp), %rsi
	movb	%al, 1(%rsi)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L278:
	cmpl	$8869, %edx
	ja	.L286
	cmpl	$8451, %edx
	jnb	.L287
	cmpl	$38, 256(%rsp)
	movq	184(%rsp), %rax
	ja	.L231
.L268:
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L231
	jmp	.L1027
.L936:
	movq	216(%rsp), %rsi
	jmp	.L925
.L723:
	cmpl	$56, 72(%rsp)
	je	.L2217
	cmpl	$40, 72(%rsp)
	je	.L2218
	cmpl	$48, 72(%rsp)
	jne	.L730
	movq	8(%rsp), %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L1228
	leal	-33(%rsi), %ecx
	cmpb	$92, %cl
	ja	.L724
	cmpb	$73, %sil
	je	.L724
	movzbl	1(%rdx), %esi
	movl	%esi, %ecx
	subl	$33, %ecx
	cmpb	$93, %cl
	ja	.L724
	leal	-33(%r14), %ecx
	leaq	2(%rdx), %r14
	imull	$94, %ecx, %ecx
	leal	-33(%rsi,%rcx), %ecx
	leal	-1410(%rcx), %esi
	cmpl	$2349, %esi
	ja	.L731
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movzwl	(%rcx,%rsi,2), %ecx
	testw	%cx, %cx
	je	.L724
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L116:
	leal	-913(%rdx), %eax
	cmpl	$192, %eax
	ja	.L118
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rax,2), %rax
	movzbl	(%rax), %ecx
	jmp	.L117
.L970:
	leaq	.LC6(%rip), %rdx
	jmp	.L927
.L959:
	leaq	.LC14(%rip), %rdx
	jmp	.L927
.L229:
	movq	352(%rsp), %rdx
	cmpq	%r14, %rdx
	je	.L1087
.L1927:
	movq	64(%rsp), %rax
	movl	(%rax), %edi
	movl	%edi, %eax
	movl	%edi, 40(%rsp)
	andl	$7, %eax
	jmp	.L90
.L928:
	leaq	.LC29(%rip), %rdx
	jmp	.L927
.L967:
	movq	248(%rsp), %rsi
	jmp	.L925
.L965:
	movq	256(%rsp), %rsi
	jmp	.L925
.L944:
	movq	232(%rsp), %rsi
	jmp	.L925
.L943:
	cmpl	$8978, %edx
	jne	.L890
	leaq	.LC20(%rip), %rdx
	jmp	.L927
.L2136:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$16, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2124:
	addq	$3, %rax
	movl	$40, %edi
	jmp	.L376
.L2184:
	cmpl	$19968, %edx
	jnb	.L308
	cmpl	$12585, %edx
	ja	.L309
	cmpl	$12288, %edx
	jnb	.L310
	cmpl	$9794, %edx
	jne	.L231
	leaq	.LC7(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2116:
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rbx
	movq	%rdi, 48(%rsp)
	jmp	.L692
.L2193:
	xorl	%r9d, %r9d
	jmp	.L754
.L330:
	movl	$-1370734243, %edi
	mull	%edi
	movl	%edx, %eax
	shrl	$7, %eax
	leal	48(%rax), %esi
	movl	%ecx, %eax
	mull	%edi
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$33, %eax
	jmp	.L331
.L871:
	cmpq	$1, 40(%rsp)
	jbe	.L1253
	movzbl	2(%r14,%rsi,4), %eax
	movb	%al, (%r11)
	movzbl	3(%r14,%rsi,4), %eax
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L921:
	leaq	.LC32(%rip), %rdx
	jmp	.L927
.L796:
	cmpl	$1105, %edx
	ja	.L811
	cmpl	$1025, %edx
	jnb	.L812
	cmpl	$711, %edx
	je	.L813
	jbe	.L2219
	cmpl	$713, %edx
	je	.L817
	jb	.L762
	leal	-913(%rdx), %eax
	cmpl	$56, %eax
	ja	.L762
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rax
.L852:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L762
	jmp	.L1049
.L824:
	cmpl	$40864, %edx
	jbe	.L2220
	cmpl	$65504, %edx
	je	.L846
	jbe	.L2221
	cmpl	$65507, %edx
	je	.L849
	cmpl	$65509, %edx
	jne	.L2222
	leaq	.LC3(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2169:
	cmpl	$75, 308(%rsp)
	ja	.L890
	addl	$36, %edx
	movl	$41, %eax
	jmp	.L1082
.L1134:
	movl	$160, %eax
	leaq	from_idx(%rip), %rcx
	jmp	.L232
.L1315:
	movl	$4887, %r8d
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r14
	movl	%edx, 48(%rsp)
	jmp	.L868
.L2223:
	leal	-1(%rax), %r8d
.L866:
	cmpl	%edi, %r8d
	jl	.L1945
.L868:
	leal	(%rdi,%r8), %eax
	sarl	%eax
	movslq	%eax, %rsi
	movzwl	(%r14,%rsi,4), %edx
	cmpl	%edx, 48(%rsp)
	jb	.L2223
	jbe	.L871
	leal	1(%rax), %edi
	jmp	.L866
.L2107:
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rsi
	xorl	%edi, %edi
	movl	$2349, %eax
	movl	%edx, 48(%rsp)
	movq	%rsi, 72(%rsp)
	jmp	.L862
.L2224:
	leal	-1(%rsi), %eax
.L860:
	cmpl	%edi, %eax
	jl	.L1945
.L862:
	leal	(%rdi,%rax), %r14d
	movq	72(%rsp), %rdx
	movl	%r14d, %esi
	sarl	%esi
	movslq	%esi, %r8
	movzwl	(%rdx,%r8,2), %r8d
	cmpl	%r8d, 48(%rsp)
	jb	.L2224
	jbe	.L861
	leal	1(%rsi), %edi
	jmp	.L860
.L973:
	leaq	.LC4(%rip), %rdx
	jmp	.L927
.L932:
	leaq	.LC27(%rip), %rdx
	jmp	.L927
.L1309:
	cmpl	$107, %edx
	jne	.L1310
	testb	%cl, %cl
	je	.L1310
	movl	$1536, %r9d
	jmp	.L741
.L2148:
	cmpl	$93, 280(%rsp)
	movq	264(%rsp), %rsi
	jbe	.L925
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L2165:
	addq	$3, %rax
	movl	$64, %r8d
	jmp	.L376
.L949:
	leaq	.LC15(%rip), %rdx
	jmp	.L927
.L919:
	leaq	.LC25(%rip), %rdx
	jmp	.L927
.L918:
	movq	240(%rsp), %rsi
	jmp	.L925
.L951:
	leaq	.LC19(%rip), %rdx
	jmp	.L927
.L2222:
	cmpl	$65505, %edx
	jne	.L762
	leaq	.LC5(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L286:
	cmpl	$8978, %edx
	jne	.L231
	leaq	.LC20(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2220:
	cmpl	$19968, %edx
	jnb	.L841
	cmpl	$12585, %edx
	ja	.L842
	cmpl	$12288, %edx
	jnb	.L843
	cmpl	$9794, %edx
	leaq	.LC7(%rip), %rax
	je	.L799
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L2159:
	cmpl	$466, %edx
	jne	.L231
	leaq	.LC28(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2190:
	cmpl	$9632, %edx
	je	.L827
	jbe	.L2225
	cmpl	$9650, %edx
	je	.L830
	cmpl	$9651, %edx
	jne	.L2226
	leaq	.LC16(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2219:
	cmpl	$474, %edx
	jne	.L2227
	leaq	.LC24(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L811:
	cmpl	$8869, %edx
	ja	.L819
	cmpl	$8451, %edx
	jnb	.L820
	leal	-8213(%rdx), %eax
	cmpl	$38, %eax
	ja	.L762
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rax
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L2227:
	cmpl	$476, %edx
	leaq	.LC23(%rip), %rax
	je	.L799
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L2174:
	cmpl	$363, %edx
	jne	.L2228
	leaq	.LC31(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2173:
	cmpl	$275, %edx
	je	.L1248
	jbe	.L2229
	cmpl	$283, %edx
	jne	.L2230
	leaq	.LC34(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2226:
	cmpl	$9633, %edx
	jne	.L762
	leaq	.LC18(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2228:
	cmpl	$462, %edx
	leaq	.LC30(%rip), %rax
	je	.L799
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L2230:
	cmpl	$299, %edx
	leaq	.LC33(%rip), %rax
	je	.L799
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L2187:
	cmpl	$476, %edx
	jne	.L231
	leaq	.LC23(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L861:
	cmpq	$1, 40(%rsp)
	jbe	.L1253
	movl	%r14d, %eax
	movl	$188, %ecx
	cltd
	idivl	%ecx
	movl	$94, %ecx
	addl	$48, %eax
	movb	%al, (%r11)
	movl	%esi, %eax
	cltd
	idivl	%ecx
	addl	$33, %edx
	movb	%dl, 1(%r11)
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L1310:
	cmpl	$111, %edx
	jne	.L1311
	cmpl	$1536, %r9d
	jne	.L1311
	movl	$512, %r9d
	jmp	.L741
.L118:
	cmpl	$65534, %edx
	ja	.L101
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	jbe	.L119
.L120:
	addq	$6, %rax
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %edx
	ja	.L120
.L119:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %edx
	jb	.L101
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%ecx, %eax
	addq	%rax, %rax
	addq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rax
	movzbl	(%rax), %ecx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L2204:
	movzbl	2(%rdx), %esi
	cmpb	$40, %sil
	jne	.L697
	leaq	3(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	ja	.L698
.L1228:
	movq	16(%rsp), %rbx
	movq	%rdx, 48(%rsp)
	movl	$7, %eax
	jmp	.L692
.L2128:
	movq	128(%rsp), %rax
	addq	$4, %rcx
	movl	$6, 36(%rsp)
	movq	%rcx, 384(%rsp)
	addq	$1, (%rax)
	jmp	.L735
.L2056:
	cmpl	$56, %r10d
	jne	.L122
	cmpl	$256, %r12d
	jne	.L101
.L1060:
	movq	%rbx, %rcx
	subq	24(%rsp), %rcx
	cmpl	$65534, %edx
	movl	$256, %r12d
	ja	.L1026
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L2198:
	xorl	%edi, %edi
	movl	$2349, %r8d
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rcx
	jmp	.L203
.L2231:
	leal	-1(%rsi), %r8d
.L201:
	cmpl	%edi, %r8d
	jl	.L101
.L203:
	leal	(%rdi,%r8), %r9d
	movl	%r9d, %esi
	sarl	%esi
	movslq	%esi, %rax
	movzwl	(%rcx,%rax,2), %eax
	cmpl	%eax, %edx
	jb	.L2231
	jbe	.L202
	leal	1(%rsi), %edi
	jmp	.L201
.L2197:
	cmpl	$768, %r12d
	je	.L1062
	testl	%r12d, %r12d
	jne	.L101
.L1062:
	movq	%rbx, %rcx
	subq	24(%rsp), %rcx
	cmpl	$9371, %edx
	ja	.L132
	cmpl	$9312, %edx
	jnb	.L133
	cmpl	$472, %edx
	je	.L134
	ja	.L135
	cmpl	$333, %edx
	je	.L136
	jbe	.L2232
	cmpl	$464, %edx
	je	.L143
	jbe	.L2233
	cmpl	$468, %edx
	je	.L147
	cmpl	$470, %edx
	jne	.L2234
	leaq	.LC26(%rip), %rax
.L138:
	movzbl	(%rax), %esi
.L1024:
	cmpb	$0, 1(%rax)
	je	.L321
	cmpq	$1, %rcx
	jbe	.L1087
	movq	24(%rsp), %rdi
	movb	%sil, (%rdi)
	movzbl	1(%rax), %eax
	movb	%al, 1(%rdi)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L2201:
	cmpl	$9633, %edx
	jne	.L231
	leaq	.LC18(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L1208:
	movl	$4, %eax
	jmp	.L692
.L1130:
	movl	$160, %eax
	leaq	from_idx(%rip), %rsi
	jmp	.L214
.L2200:
	cmpl	$75, 280(%rsp)
	ja	.L231
	addl	$36, %edx
	movb	$41, 328(%rsp)
	movb	%dl, 329(%rsp)
.L319:
	movzbl	328(%rsp), %ecx
	leaq	328(%rsp), %rax
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L2234:
	cmpl	$466, %edx
	jne	.L101
	leaq	.LC28(%rip), %rax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L132:
	cmpl	$9792, %edx
	je	.L162
	ja	.L163
	cmpl	$9670, %edx
	je	.L164
	jbe	.L2235
	cmpl	$9678, %edx
	je	.L172
	jbe	.L2236
	cmpl	$9733, %edx
	je	.L176
	cmpl	$9734, %edx
	jne	.L2237
	leaq	.LC9(%rip), %rax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L2125:
	addq	$4, %rax
	movl	$48, %edi
	jmp	.L376
.L2185:
	cmpl	$93, 248(%rsp)
	movq	232(%rsp), %rax
	jbe	.L268
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L109:
	leal	-65377(%rdx), %eax
	cmpl	$62, %eax
	ja	.L101
	leal	64(%rdx), %eax
	jmp	.L110
.L2186:
	cmpl	$65505, %edx
	jne	.L231
	leaq	.LC5(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2178:
	cmpl	$256, %r12d
	jne	.L101
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L2218:
	subl	$33, %r14d
	cmpl	$86, %r14d
	ja	.L724
	movq	8(%rsp), %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L1228
	movzbl	1(%rdx), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L724
	imull	$94, %r14d, %r14d
	addl	%r14d, %ecx
	cmpl	$8177, %ecx
	jg	.L724
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	leaq	2(%rdx), %r14
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L724
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L2217:
	subl	$34, %esi
	cmpb	$75, %sil
	ja	.L724
	movq	8(%rsp), %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L1228
	movzbl	1(%rdx), %esi
	movl	%esi, %ecx
	subl	$33, %ecx
	cmpb	$93, %cl
	ja	.L724
	leal	-33(%r14), %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rsi,%rcx), %r14d
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rcx
.L727:
	movzwl	2(%rcx), %esi
	cmpl	%esi, %r14d
	jle	.L2238
	addq	$6, %rcx
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L309:
	cmpl	$9, 272(%rsp)
	ja	.L231
	addl	$69, %edx
	movb	$34, 328(%rsp)
	movb	%dl, 329(%rsp)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L2205:
	movzbl	2(%rdx), %esi
	cmpb	$65, %sil
	je	.L2239
	cmpb	$70, %sil
	jne	.L721
	addq	$3, %rdx
	movl	$128, 88(%rsp)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L2209:
	movzbl	394(%rsp), %edi
	leaq	3(%rdx), %rcx
	andl	$-3, %edi
	cmpb	$64, %dil
	je	.L50
.L1109:
	leaq	1(%rdx), %rcx
	jmp	.L51
.L2208:
	movzbl	394(%rsp), %esi
	leaq	3(%rdx), %rcx
	andl	$-9, %esi
	cmpb	$66, %sil
	je	.L50
.L1922:
	leaq	1(%rdx), %rcx
	movl	$27, %esi
	jmp	.L51
.L2207:
	cmpb	$36, %r8b
	je	.L2240
	cmpb	$40, %r8b
	jne	.L1907
	movzbl	394(%rsp), %ecx
	movl	%ecx, %esi
	andl	$-9, %esi
	cmpb	$66, %sil
	je	.L1286
	cmpb	$73, %cl
	jne	.L1922
.L1286:
	leaq	3(%rdx), %rcx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r9
	je	.L2241
	movq	%rcx, %rbx
	movl	40(%rsp), %esi
	subq	%rax, %rbx
	movq	48(%rsp), %rax
	andl	$-8, %esi
	addq	%rbx, %rax
	movq	56(%rsp), %rbx
	movq	%rax, (%rbx)
	movslq	%esi, %rax
	cmpq	%rax, %rcx
	jle	.L2242
	cmpq	$4, %rcx
	ja	.L2243
	movq	64(%rsp), %rax
	orl	%ecx, %esi
	movl	%esi, (%rax)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L84
.L75:
	movq	64(%rsp), %rbx
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L84
	movzbl	(%rdx,%rax), %edi
	jmp	.L75
.L2158:
	cmpl	$363, %edx
	jne	.L2244
	leaq	.LC31(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2192:
	cmpl	$9679, %edx
	jne	.L762
	leaq	.LC11(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	384(%rsp), %rax
	movl	%ebx, 72(%rsp)
	movl	$128, 88(%rsp)
	movq	16(%rsp), %rbx
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2203:
	addq	$3, %rdx
	movl	$24, 72(%rsp)
	jmp	.L701
.L2202:
	addq	$3, %rdx
	movl	$0, 72(%rsp)
	jmp	.L701
.L2175:
	cmpl	$466, %edx
	jne	.L762
	leaq	.LC28(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2237:
	cmpl	$9679, %edx
	jne	.L101
	leaq	.LC11(%rip), %rax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	$24, %r10d
	je	.L2245
	cmpl	$32, %r10d
	je	.L106
	leal	-8(%r10), %eax
	xorl	%r12d, %r12d
	andl	$-16, %eax
	je	.L111
	cmpl	$56, %r10d
	je	.L112
	cmpl	$40, %r10d
	je	.L1062
	cmpl	$48, %r10d
	jne	.L198
	xorl	%r12d, %r12d
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L2211:
	cmpl	$299, %edx
	jne	.L231
	leaq	.LC33(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2164:
	cmpl	$9679, %edx
	jne	.L231
	leaq	.LC11(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2163:
	cmpl	$9671, %edx
	jne	.L2246
	leaq	.LC14(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L281:
	cmpl	$713, %edx
	jne	.L2247
	leaq	.LC21(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2191:
	cmpl	$9671, %edx
	jne	.L2248
	leaq	.LC14(%rip), %rax
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L2246:
	cmpl	$9675, %edx
	jne	.L231
	leaq	.LC13(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2244:
	cmpl	$462, %edx
	jne	.L231
	leaq	.LC30(%rip), %rax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L2247:
	jb	.L231
	cmpl	$56, 36(%rsp)
	movq	168(%rsp), %rax
	jbe	.L268
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L2248:
	cmpl	$9675, %edx
	leaq	.LC13(%rip), %rax
	je	.L799
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L2210:
	cmpl	$93, 240(%rsp)
	movq	152(%rsp), %rax
	jbe	.L268
	jmp	.L231
.L2216:
	cmpl	$256, %r12d
	jne	.L108
	jmp	.L1060
.L279:
	movq	176(%rsp), %rax
	jmp	.L268
.L77:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L406:
	cmpl	$3853, %edx
	jle	.L407
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %r9
	subl	$3854, %edx
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edx
	testw	%dx, %dx
	je	.L399
	jmp	.L400
.L731:
	cmpl	$3853, %ecx
	jle	.L732
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rsi
	subl	$3854, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L724
	jmp	.L725
.L2239:
	addq	$3, %rdx
	movl	$64, 88(%rsp)
	jmp	.L701
.L407:
	cmpl	$1114, %edx
	jg	.L399
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edx
	testw	%dx, %dx
	je	.L399
	jmp	.L400
.L732:
	cmpl	$1114, %ecx
	jg	.L724
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L724
	jmp	.L725
.L837:
	leaq	.LC10(%rip), %rax
	jmp	.L799
.L833:
	leaq	.LC12(%rip), %rax
	jmp	.L799
.L304:
	leaq	.LC10(%rip), %rax
	jmp	.L270
.L300:
	leaq	.LC12(%rip), %rax
	jmp	.L270
.L147:
	leaq	.LC27(%rip), %rax
	jmp	.L138
.L133:
	leal	-9312(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rax
.L191:
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L101
	jmp	.L1024
.L135:
	cmpl	$1105, %edx
	ja	.L150
	cmpl	$1025, %edx
	jnb	.L151
	cmpl	$711, %edx
	je	.L152
	ja	.L153
	cmpl	$474, %edx
	je	.L154
	cmpl	$476, %edx
	leaq	.LC23(%rip), %rax
	je	.L138
	jmp	.L101
.L134:
	leaq	.LC25(%rip), %rax
	jmp	.L138
.L154:
	leaq	.LC24(%rip), %rax
	jmp	.L138
.L153:
	cmpl	$713, %edx
	je	.L156
	jb	.L101
	leal	-913(%rdx), %eax
	cmpl	$56, %eax
	ja	.L101
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rax
	jmp	.L191
.L152:
	leaq	.LC22(%rip), %rax
	jmp	.L138
.L156:
	leaq	.LC21(%rip), %rax
	jmp	.L138
.L151:
	leal	-1025(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rax
	jmp	.L191
.L150:
	cmpl	$8869, %edx
	ja	.L158
	cmpl	$8451, %edx
	jnb	.L159
	leal	-8213(%rdx), %eax
	cmpl	$38, %eax
	ja	.L101
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rax
	jmp	.L191
.L2233:
	cmpl	$363, %edx
	je	.L145
	cmpl	$462, %edx
	leaq	.LC30(%rip), %rax
	je	.L138
	jmp	.L101
.L143:
	leaq	.LC29(%rip), %rax
	jmp	.L138
.L145:
	leaq	.LC31(%rip), %rax
	jmp	.L138
.L2232:
	cmpl	$275, %edx
	je	.L1124
	jbe	.L2249
	cmpl	$283, %edx
	je	.L141
	cmpl	$299, %edx
	leaq	.LC33(%rip), %rax
	je	.L138
	jmp	.L101
.L136:
	leaq	.LC32(%rip), %rax
	jmp	.L138
.L141:
	leaq	.LC34(%rip), %rax
	jmp	.L138
.L2249:
	leal	-164(%rdx), %eax
	cmpl	$93, %eax
	ja	.L101
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rax
	jmp	.L191
.L1124:
	leaq	.LC2(%rip), %rax
	jmp	.L138
.L159:
	leal	-8451(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rax
	jmp	.L191
.L158:
	cmpl	$8978, %edx
	leaq	.LC20(%rip), %rax
	je	.L138
	jmp	.L101
.L795:
	leaq	.LC25(%rip), %rax
	jmp	.L799
.L202:
	cmpq	$1, %r11
	jbe	.L1087
	movl	%r9d, %eax
	movl	$188, %ecx
	movq	24(%rsp), %rdi
	cltd
	idivl	%ecx
	movl	$94, %ecx
	addl	$48, %eax
	movb	%al, (%rdi)
	movl	%esi, %eax
	cltd
	idivl	%ecx
	addl	$33, %edx
	movb	%dl, 1(%rdi)
	jmp	.L121
.L262:
	leaq	.LC25(%rip), %rax
	jmp	.L270
.L261:
	movq	200(%rsp), %rax
	jmp	.L268
.L297:
	leaq	.LC17(%rip), %rax
	jmp	.L270
.L2080:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L2194:
	xorl	%r9d, %r9d
	jmp	.L1080
.L2154:
	movq	128(%rsp), %rax
	addq	$4, %rdx
	movq	%rdx, 352(%rsp)
	addq	$1, (%rax)
	cmpq	%r14, %rdx
	jne	.L1927
	jmp	.L356
.L287:
	movq	192(%rsp), %rax
	jmp	.L268
.L846:
	leaq	.LC6(%rip), %rax
	jmp	.L799
.L849:
	leaq	.LC4(%rip), %rax
	jmp	.L799
.L2221:
	leal	-65281(%rdx), %eax
	cmpl	$93, %eax
	ja	.L762
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rax
	jmp	.L852
.L825:
	leaq	.LC15(%rip), %rax
	jmp	.L799
.L813:
	leaq	.LC22(%rip), %rax
	jmp	.L799
.L820:
	leal	-8451(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rax
	jmp	.L852
.L819:
	cmpl	$8978, %edx
	leaq	.LC20(%rip), %rax
	je	.L799
	jmp	.L762
.L2095:
	leaq	__PRETTY_FUNCTION__.9585(%rip), %rcx
	leaq	.LC35(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L326:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L2182:
	leaq	4(%r14), %rax
	cmpq	%rax, %r12
	je	.L2250
	movq	64(%rsp), %rax
	movq	%r15, %rbx
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	56(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r15
	jle	.L2251
	cmpq	$4, %r15
	ja	.L2252
	movq	64(%rsp), %rbx
	orl	%r15d, %eax
	testq	%r15, %r15
	movl	%eax, (%rbx)
	je	.L84
	xorl	%eax, %eax
.L365:
	movzbl	(%r14,%rax), %edx
	movq	64(%rsp), %rbx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L365
	jmp	.L84
.L2153:
	movq	352(%rsp), %rdx
	jmp	.L1966
.L2252:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC43(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L2251:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L2250:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC41(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L2103:
	leaq	__PRETTY_FUNCTION__.9585(%rip), %rcx
	leaq	.LC35(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L2102:
	leaq	__PRETTY_FUNCTION__.9585(%rip), %rcx
	leaq	.LC35(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L1253:
	movq	16(%rsp), %rbx
	movq	%rcx, 48(%rsp)
	movl	$5, %eax
	movl	$48, 72(%rsp)
	jmp	.L736
.L280:
	leaq	.LC22(%rip), %rax
	jmp	.L270
.L1139:
	movl	$40, %ecx
	movl	$37, %edx
	jmp	.L266
.L176:
	leaq	.LC10(%rip), %rax
	jmp	.L138
.L2245:
	xorl	%r12d, %r12d
	jmp	.L95
.L275:
	leaq	.LC27(%rip), %rax
	jmp	.L270
.L2243:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC43(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L2242:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L2241:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC41(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L730:
	leaq	__PRETTY_FUNCTION__.9345(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L310:
	movq	208(%rsp), %rax
	jmp	.L268
.L308:
	movq	224(%rsp), %rax
	jmp	.L268
.L2238:
	movzwl	(%rcx), %esi
	cmpl	%esi, %r14d
	jl	.L724
	movzwl	4(%rcx), %ecx
	addl	%r14d, %ecx
	leaq	2(%rdx), %r14
	subl	%esi, %ecx
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testl	%ecx, %ecx
	jne	.L725
	jmp	.L724
.L290:
	leaq	.LC8(%rip), %rax
	jmp	.L270
.L2146:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movl	$56, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L271:
	leaq	.LC29(%rip), %rax
	jmp	.L270
.L212:
	cmpq	$1, %r11
	jbe	.L1087
	movzbl	2(%r9,%rcx,4), %eax
	movq	24(%rsp), %rdi
	movb	%al, (%rdi)
	movzbl	3(%r9,%rcx,4), %eax
	movb	%al, 1(%rdi)
	jmp	.L121
.L80:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L254:
	leaq	__PRETTY_FUNCTION__.8071(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$101, %edx
	call	__assert_fail@PLT
.L2236:
	cmpl	$9671, %edx
	je	.L174
	cmpl	$9675, %edx
	leaq	.LC13(%rip), %rax
	je	.L138
	jmp	.L101
.L172:
	leaq	.LC12(%rip), %rax
	jmp	.L138
.L174:
	leaq	.LC14(%rip), %rax
	jmp	.L138
.L2235:
	cmpl	$9632, %edx
	je	.L166
	jbe	.L2253
	cmpl	$9650, %edx
	je	.L169
	cmpl	$9651, %edx
	je	.L170
	cmpl	$9633, %edx
	jne	.L101
	leaq	.LC18(%rip), %rax
	jmp	.L138
.L164:
	leaq	.LC15(%rip), %rax
	jmp	.L138
.L170:
	leaq	.LC16(%rip), %rax
	jmp	.L138
.L169:
	leaq	.LC17(%rip), %rax
	jmp	.L138
.L2253:
	leal	-9472(%rdx), %eax
	cmpl	$75, %eax
	ja	.L101
	addl	$36, %edx
	movb	$41, 326(%rsp)
	movb	%dl, 327(%rsp)
.L192:
	movzbl	326(%rsp), %esi
	leaq	326(%rsp), %rax
	jmp	.L1024
.L166:
	leaq	.LC19(%rip), %rax
	jmp	.L138
.L163:
	cmpl	$40864, %edx
	jbe	.L2254
	cmpl	$65504, %edx
	je	.L185
	jbe	.L2255
	cmpl	$65507, %edx
	je	.L188
	cmpl	$65509, %edx
	je	.L189
	cmpl	$65505, %edx
	jne	.L101
	leaq	.LC5(%rip), %rax
	jmp	.L138
.L162:
	leaq	.LC8(%rip), %rax
	jmp	.L138
.L189:
	leaq	.LC3(%rip), %rax
	jmp	.L138
.L188:
	leaq	.LC4(%rip), %rax
	jmp	.L138
.L2255:
	leal	-65281(%rdx), %eax
	cmpl	$93, %eax
	ja	.L101
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rax
	jmp	.L191
.L185:
	leaq	.LC6(%rip), %rax
	jmp	.L138
.L2254:
	cmpl	$19968, %edx
	jnb	.L180
	cmpl	$12585, %edx
	ja	.L181
	cmpl	$12288, %edx
	jnb	.L182
	cmpl	$9794, %edx
	leaq	.LC7(%rip), %rax
	je	.L138
	jmp	.L101
.L2127:
	movq	384(%rsp), %rax
	movl	%ebx, 72(%rsp)
	movq	392(%rsp), %r11
	movq	16(%rsp), %rbx
	movq	%rax, 48(%rsp)
	movl	$6, %eax
	jmp	.L736
.L2079:
	testl	%r10d, %r10d
	je	.L1109
	cmpl	$32, %esi
	jbe	.L1109
	cmpl	$127, %esi
	je	.L1109
	cmpl	$24, %r10d
	je	.L2256
	cmpl	$32, %r10d
	je	.L2257
	leal	-8(%r10), %esi
	andl	$-16, %esi
	jne	.L61
	cmpl	$32, %r8d
	jle	.L62
	cmpq	$1, %rcx
	jbe	.L44
	movzbl	393(%rsp), %ecx
	leal	-33(%rcx), %esi
	cmpl	$93, %esi
	jbe	.L2258
.L62:
	cmpq	$0, 128(%rsp)
	je	.L356
	testb	$2, 92(%rsp)
	movl	$6, 36(%rsp)
	movq	%rdx, %rcx
	je	.L18
.L1085:
	movq	128(%rsp), %rdi
	addq	$1, %rcx
	addq	$1, (%rdi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$56, %r10d
	je	.L2259
	cmpl	$40, %r10d
	je	.L2260
	cmpl	$48, %r10d
	jne	.L2261
	cmpq	$1, %rcx
	jbe	.L44
	leal	-33(%rdi), %ecx
	cmpb	$92, %cl
	ja	.L62
	cmpb	$73, %dil
	je	.L62
	movzbl	393(%rsp), %esi
	movl	%esi, %ecx
	subl	$33, %ecx
	cmpb	$93, %cl
	ja	.L62
	leal	-33(%r8), %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rsi,%rcx), %ecx
	leal	-1410(%rcx), %esi
	cmpl	$2349, %esi
	ja	.L69
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movzwl	(%rcx,%rsi,2), %esi
	testw	%si, %si
	je	.L62
.L63:
	cmpl	$65533, %esi
	je	.L1019
	leaq	2(%rdx), %rcx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L2257:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rcx
	addl	$-128, %edi
	movzbl	%dil, %esi
	movl	(%rcx,%rsi,4), %esi
	testl	%esi, %esi
	sete	%r8b
	testb	%dil, %dil
	setne	%cl
	testb	%cl, %r8b
	jne	.L1290
	cmpl	$65533, %esi
	leaq	1(%rdx), %rcx
	sete	%dil
	testb	%dil, %dil
	je	.L51
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L2256:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %esi
	testl	%esi, %esi
	sete	%r8b
	testb	%dil, %dil
	setne	%cl
	testb	%cl, %r8b
	jne	.L1289
	cmpl	$65533, %esi
	leaq	1(%rdx), %rcx
	sete	%dil
	testb	%dil, %dil
	je	.L51
.L1289:
	cmpq	$0, 128(%rsp)
	movl	$6, 36(%rsp)
	je	.L18
	testb	$2, 92(%rsp)
	je	.L18
	movq	128(%rsp), %rsi
	leaq	1(%rdx), %rcx
	addq	$1, (%rsi)
	jmp	.L50
.L1019:
	cmpq	$0, 128(%rsp)
	je	.L2262
	testb	$2, 92(%rsp)
	leaq	2(%rdx), %rcx
	je	.L50
	jmp	.L1085
.L1012:
	leaq	__PRETTY_FUNCTION__.9585(%rip), %rcx
	leaq	.LC35(%rip), %rsi
	leaq	.LC51(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L2258:
	leal	-33(%r8), %ecx
	imull	$94, %ecx, %ecx
	addl	%esi, %ecx
	cmpl	$7807, %ecx
	jg	.L62
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %esi
	testw	%si, %si
	je	.L62
	jmp	.L63
.L2262:
	leaq	2(%rdx), %rcx
	jmp	.L50
.L69:
	cmpl	$3853, %ecx
	jle	.L70
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rsi
	subl	$3854, %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %esi
	testw	%si, %si
	je	.L62
	jmp	.L63
.L2261:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L70:
	cmpl	$1114, %ecx
	jg	.L62
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %esi
	testw	%si, %si
	je	.L62
	jmp	.L63
.L2260:
	subl	$33, %r8d
	cmpl	$86, %r8d
	ja	.L62
	cmpq	$1, %rcx
	jbe	.L44
	movzbl	393(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L62
	imull	$94, %r8d, %r8d
	addl	%r8d, %ecx
	cmpl	$8177, %ecx
	jg	.L62
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %esi
	testw	%si, %si
	je	.L62
	jmp	.L63
.L2259:
	leal	-34(%rdi), %esi
	cmpb	$75, %sil
	ja	.L62
	cmpq	$1, %rcx
	jbe	.L44
	movzbl	393(%rsp), %esi
	movl	%esi, %ecx
	subl	$33, %ecx
	cmpb	$93, %cl
	ja	.L62
	leal	-33(%r8), %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rsi,%rcx), %edi
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rcx
.L65:
	movzwl	2(%rcx), %esi
	cmpl	%esi, %edi
	jle	.L2263
	addq	$6, %rcx
	jmp	.L65
.L1907:
	cmpb	$46, %r8b
	je	.L2264
	cmpb	$78, %r8b
	jne	.L1922
	cmpl	$64, 8(%rsp)
	je	.L2265
	cmpl	$128, 8(%rsp)
	jne	.L1289
	movzbl	394(%rsp), %ecx
	leal	-32(%rcx), %esi
	cmpb	$95, %sil
	ja	.L1290
	subl	$32, %ecx
	leaq	iso88597_to_ucs4(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %esi
	testl	%esi, %esi
	jne	.L1101
	cmpq	$0, 128(%rsp)
	movl	$6, 36(%rsp)
	je	.L18
	testb	$2, 92(%rsp)
	je	.L18
	movq	128(%rsp), %rdi
	leaq	3(%rdx), %rcx
	addq	$1, (%rdi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L2265:
	movzbl	394(%rsp), %esi
	leaq	3(%rdx), %rcx
	orl	$-128, %esi
	movzbl	%sil, %esi
	jmp	.L51
.L1101:
	leaq	3(%rdx), %rcx
	jmp	.L51
.L2264:
	movzbl	394(%rsp), %ecx
	cmpb	$70, %cl
	je	.L1286
	cmpb	$65, %cl
	jne	.L1922
	jmp	.L1286
.L808:
	leaq	.LC27(%rip), %rax
	jmp	.L799
.L182:
	leal	-12288(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rax
	jmp	.L191
.L181:
	leal	-12832(%rdx), %eax
	cmpl	$9, %eax
	ja	.L101
	addl	$69, %edx
	movb	$34, 326(%rsp)
	movb	%dl, 327(%rsp)
	jmp	.L192
.L180:
	leal	-19968(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rax
	jmp	.L191
.L2181:
	cmpl	$256, %r12d
	jne	.L101
	cmpl	$165, %edx
	je	.L108
	cmpl	$8254, %edx
	jne	.L1059
	jmp	.L108
.L2049:
	leaq	__PRETTY_FUNCTION__.9585(%rip), %rcx
	leaq	.LC35(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L2240:
	cmpb	$40, 394(%rsp)
	jne	.L48
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %r9
	jbe	.L44
.L48:
	movzbl	394(%rsp), %ecx
	movl	%ecx, %esi
	andl	$-3, %esi
	cmpb	$64, %sil
	je	.L1286
	cmpb	$65, %cl
	je	.L1286
	cmpb	$40, %cl
	jne	.L1922
	movzbl	395(%rsp), %edi
	leal	-67(%rdi), %ecx
	cmpb	$1, %cl
	ja	.L1922
	leaq	4(%rdx), %rcx
	jmp	.L50
.L2263:
	movzwl	(%rcx), %esi
	cmpl	%esi, %edi
	jl	.L62
	movzwl	4(%rcx), %ecx
	addl	%edi, %ecx
	subl	%esi, %ecx
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %esi
	testl	%esi, %esi
	jne	.L63
	jmp	.L62
.L797:
	leaq	.LC32(%rip), %rax
	jmp	.L799
.L830:
	leaq	.LC17(%rip), %rax
	jmp	.L799
.L2229:
	leal	-164(%rdx), %eax
	cmpl	$93, %eax
	ja	.L762
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rax
	jmp	.L852
.L1248:
	leaq	.LC2(%rip), %rax
	jmp	.L799
.L264:
	leaq	.LC32(%rip), %rax
	jmp	.L270
.L794:
	leal	-9312(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rax
	jmp	.L852
.L823:
	leaq	.LC8(%rip), %rax
	jmp	.L799
.L817:
	leaq	.LC21(%rip), %rax
	jmp	.L799
.L2225:
	leal	-9472(%rdx), %eax
	cmpl	$75, %eax
	ja	.L762
	addl	$36, %edx
	movb	$41, 330(%rsp)
	movb	%dl, 331(%rsp)
.L853:
	movzbl	330(%rsp), %esi
	leaq	330(%rsp), %rax
	jmp	.L1049
.L827:
	leaq	.LC19(%rip), %rax
	jmp	.L799
.L812:
	leal	-1025(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rax
	jmp	.L852
.L804:
	leaq	.LC29(%rip), %rax
	jmp	.L799
.L39:
	leaq	__PRETTY_FUNCTION__.9367(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L843:
	leal	-12288(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rax
	jmp	.L852
.L842:
	leal	-12832(%rdx), %eax
	cmpl	$9, %eax
	ja	.L762
	addl	$69, %edx
	movb	$34, 330(%rsp)
	movb	%dl, 331(%rsp)
	jmp	.L853
.L841:
	leal	-19968(%rdx), %eax
	addq	%rax, %rax
	addq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rax
	jmp	.L852
.L292:
	leaq	.LC15(%rip), %rax
	jmp	.L270
.L294:
	leaq	.LC19(%rip), %rax
	jmp	.L270
.L2057:
	leaq	__PRETTY_FUNCTION__.9491(%rip), %rcx
	leaq	.LC37(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L2161:
	movq	384(%rsp), %rax
	movq	16(%rsp), %rbx
	movq	392(%rsp), %r11
	movl	$56, 72(%rsp)
	movq	%rax, 48(%rsp)
	movl	$5, %eax
	jmp	.L736
.L2215:
	addq	$4, %rdx
	movl	$48, 72(%rsp)
	jmp	.L701
.L2214:
	addq	$3, %rdx
	movl	$40, 72(%rsp)
	jmp	.L701
.L2213:
	addq	$3, %rdx
	movl	$16, 72(%rsp)
	jmp	.L701
.L2212:
	addq	$3, %rdx
	movl	$8, 72(%rsp)
	jmp	.L701
.L2097:
	movl	%r12d, (%rbx)
	jmp	.L31
.L2096:
	movq	%r13, %rax
	jmp	.L1015
.L1311:
	cmpl	$122, %edx
	jne	.L1312
	testb	%cl, %cl
	je	.L1312
	movl	$1792, %r9d
	jmp	.L741
.L321:
	leaq	__PRETTY_FUNCTION__.8174(%rip), %rcx
	leaq	.LC47(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$220, %edx
	call	__assert_fail@PLT
.L1292:
	movl	$4887, %r8d
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L2266:
	leal	-1(%rax), %r8d
.L207:
	cmpl	%edi, %r8d
	jl	.L101
.L209:
	leal	(%rdi,%r8), %eax
	sarl	%eax
	movslq	%eax, %rcx
	movzwl	(%r9,%rcx,4), %esi
	cmpl	%esi, %edx
	jb	.L2266
	jbe	.L208
	leal	1(%rax), %edi
	jmp	.L207
.L313:
	leaq	.LC6(%rip), %rax
	jmp	.L270
.L208:
	cmpq	$1, %r11
	jbe	.L1087
	movzbl	2(%r9,%rcx,4), %eax
	movq	24(%rsp), %rsi
	movb	%al, (%rsi)
	movzbl	3(%r9,%rcx,4), %eax
	movb	%al, 1(%rsi)
	jmp	.L121
.L316:
	leaq	.LC4(%rip), %rax
	jmp	.L270
.L1312:
	cmpl	$104, %edx
	jne	.L1313
	cmpl	$1792, %r9d
	jne	.L1313
	movl	$768, %r9d
	jmp	.L741
.L1313:
	cmpl	$1023, %r9d
	jg	.L1314
	cmpl	$127, %edx
	jne	.L741
.L1314:
	xorl	%r9d, %r9d
	jmp	.L741
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9463, @object
	.size	__PRETTY_FUNCTION__.9463, 18
__PRETTY_FUNCTION__.9463:
	.string	"to_iso2022jp_loop"
	.align 16
	.type	__PRETTY_FUNCTION__.9345, @object
	.size	__PRETTY_FUNCTION__.9345, 20
__PRETTY_FUNCTION__.9345:
	.string	"from_iso2022jp_loop"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8174, @object
	.size	__PRETTY_FUNCTION__.8174, 15
__PRETTY_FUNCTION__.8174:
	.string	"ucs4_to_gb2312"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.8071, @object
	.size	__PRETTY_FUNCTION__.8071, 17
__PRETTY_FUNCTION__.8071:
	.string	"ucs4_to_jisx0212"
	.align 16
	.type	__PRETTY_FUNCTION__.9491, @object
	.size	__PRETTY_FUNCTION__.9491, 25
__PRETTY_FUNCTION__.9491:
	.string	"to_iso2022jp_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9367, @object
	.size	__PRETTY_FUNCTION__.9367, 27
__PRETTY_FUNCTION__.9367:
	.string	"from_iso2022jp_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9585, @object
	.size	__PRETTY_FUNCTION__.9585, 6
__PRETTY_FUNCTION__.9585:
	.string	"gconv"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	conversion_lists, @object
	.size	conversion_lists, 16
conversion_lists:
	.long	22730
	.long	22730
	.long	22156
	.long	22667
	.section	.rodata
	.align 32
	.type	iso88597_from_ucs4, @object
	.size	iso88597_from_ucs4, 115
iso88597_from_ucs4:
	.byte	-96
	.byte	0
	.byte	0
	.byte	-93
	.byte	0
	.byte	0
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	0
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	0
	.byte	0
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	0
	.byte	0
	.byte	0
	.byte	-73
	.byte	0
	.byte	0
	.byte	0
	.byte	-69
	.byte	0
	.byte	-67
	.byte	-86
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	0
	.byte	-72
	.byte	-71
	.byte	-70
	.byte	0
	.byte	-68
	.byte	0
	.byte	-66
	.byte	-65
	.byte	-64
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	-48
	.byte	-47
	.byte	0
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	-37
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-33
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	-81
	.byte	0
	.byte	0
	.byte	-95
	.byte	-94
	.byte	-92
	.byte	0
	.byte	0
	.byte	-91
	.align 32
	.type	from_idx, @object
	.size	from_idx, 48
from_idx:
	.value	160
	.value	189
	.long	0
	.value	890
	.value	890
	.long	-700
	.value	900
	.value	974
	.long	-709
	.value	8213
	.value	8217
	.long	-7947
	.value	8364
	.value	8367
	.long	-8093
	.value	-1
	.value	-1
	.long	0
	.align 32
	.type	iso88597_to_ucs4, @object
	.size	iso88597_to_ucs4, 384
iso88597_to_ucs4:
	.long	160
	.long	8216
	.long	8217
	.long	163
	.long	8364
	.long	8367
	.long	166
	.long	167
	.long	168
	.long	169
	.long	890
	.long	171
	.long	172
	.long	173
	.zero	4
	.long	8213
	.long	176
	.long	177
	.long	178
	.long	179
	.long	900
	.long	901
	.long	902
	.long	183
	.long	904
	.long	905
	.long	906
	.long	187
	.long	908
	.long	189
	.long	910
	.long	911
	.long	912
	.long	913
	.long	914
	.long	915
	.long	916
	.long	917
	.long	918
	.long	919
	.long	920
	.long	921
	.long	922
	.long	923
	.long	924
	.long	925
	.long	926
	.long	927
	.long	928
	.long	929
	.zero	4
	.long	931
	.long	932
	.long	933
	.long	934
	.long	935
	.long	936
	.long	937
	.long	938
	.long	939
	.long	940
	.long	941
	.long	942
	.long	943
	.long	944
	.long	945
	.long	946
	.long	947
	.long	948
	.long	949
	.long	950
	.long	951
	.long	952
	.long	953
	.long	954
	.long	955
	.long	956
	.long	957
	.long	958
	.long	959
	.long	960
	.long	961
	.long	962
	.long	963
	.long	964
	.long	965
	.long	966
	.long	967
	.long	968
	.long	969
	.long	970
	.long	971
	.long	972
	.long	973
	.long	974
	.zero	4
