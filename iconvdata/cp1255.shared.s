	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	testb	%sil, %sil
	js	.L2
	movzbl	%sil, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movzbl	%sil, %esi
	leaq	to_ucs4(%rip), %rax
	addl	$-128, %esi
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %eax
	leal	-1488(%rax), %edx
	cmpl	$34, %edx
	jbe	.L5
	testl	%eax, %eax
	je	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CP1255//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L11
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L14
	movabsq	$17179869188, %rdi
	movabsq	$12884901889, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC6:
	.string	"ch != 0"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC8:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC9:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC10:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC11:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC13:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r10
	subq	$184, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 48(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	movl	%r11d, %edx
	movq	%r9, 56(%rsp)
	movl	240(%rsp), %ebp
	andl	$1, %edx
	movq	%rdi, 80(%rsp)
	movq	$0, 24(%rsp)
	jne	.L16
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 24(%rsp)
	je	.L16
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L16:
	testl	%ebp, %ebp
	jne	.L356
	movq	16(%rsp), %rax
	movq	32(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	32(%r13), %r14
	movl	248(%rsp), %ebx
	movq	8(%r13), %r12
	testq	%rsi, %rsi
	movq	(%rax), %r15
	movq	%rsi, %rax
	cmove	%r13, %rax
	cmpq	$0, 56(%rsp)
	movl	(%r14), %edi
	movq	(%rax), %rbp
	movl	$0, %eax
	movq	$0, 128(%rsp)
	movl	%edi, 64(%rsp)
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 104(%rsp)
	movq	48(%rsp), %rax
	setne	115(%rsp)
	movzbl	115(%rsp), %ecx
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L31
	testb	%cl, %cl
	je	.L31
	andl	$7, %edi
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r10, %rdi
	testq	%rax, %rax
	movq	%r12, %r10
	movl	%r11d, 40(%rsp)
	movq	%r15, %r12
	movq	%rbp, %r15
	movq	%rdi, %rbp
	je	.L358
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	152(%rsp), %r11
	movq	%r12, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rbx
	movq	%r12, %rax
	movl	$4, 8(%rsp)
.L103:
	cmpq	%rax, %rbp
	je	.L104
.L124:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L229
	cmpq	%rbx, %r10
	jbe	.L238
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L105
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%rbx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L124
	.p2align 4,,10
	.p2align 3
.L104:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L359
.L125:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L360
	cmpq	%rbx, %r15
	jnb	.L241
	movq	24(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%r10, 40(%rsp)
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%rdi
	movq	40(%rsp), %r10
	je	.L129
	movq	136(%rsp), %rax
	cmpq	%rbx, %rax
	movq	%rax, 8(%rsp)
	jne	.L361
.L128:
	testl	%r11d, %r11d
	jne	.L264
.L189:
	movq	16(%rsp), %rdi
	movq	48(%rsp), %rax
	movq	0(%r13), %r15
	movq	(%rdi), %r12
	movl	(%r14), %edi
	movq	96(%rax), %rax
	movl	%edi, 64(%rsp)
	movl	16(%r13), %edi
	testq	%rax, %rax
	movl	%edi, 40(%rsp)
	jne	.L73
.L358:
	cmpq	%r12, %rbp
	je	.L220
	leaq	4(%r15), %rsi
	movq	%r12, %rcx
	movq	%r15, %rbx
	cmpq	%rsi, %r10
	jb	.L353
	movq	%rbp, %rax
	leaq	to_ucs4(%rip), %r11
	movq	%r13, %rbp
	movl	$4, 8(%rsp)
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	(%rcx), %eax
	cmpl	$127, %eax
	jbe	.L76
	addl	$-128, %eax
	movzwl	(%r11,%rax,2), %eax
	testl	%eax, %eax
	je	.L362
.L76:
	movl	(%r14), %edx
	leal	-1488(%rax), %edi
	sarl	$3, %edx
	testl	%edx, %edx
	je	.L78
	leal	-1456(%rax), %r8d
	cmpl	$20, %r8d
	ja	.L79
	leal	-1460(%rax), %r8d
	cmpl	$14, %r8d
	ja	.L79
	leaq	.L81(%rip), %r9
	movslq	(%r9,%r8,4), %r8
	addq	%r9, %r8
	jmp	*%r8
	.section	.rodata
	.align 4
	.align 4
.L81:
	.long	.L80-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L82-.L81
	.long	.L83-.L81
	.long	.L225-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L85-.L81
	.long	.L79-.L81
	.long	.L79-.L81
	.long	.L86-.L81
	.long	.L79-.L81
	.long	.L87-.L81
	.long	.L88-.L81
	.text
	.p2align 4,,10
	.p2align 3
.L105:
	leal	-160(%rdx), %esi
	cmpl	$87, %esi
	ja	.L107
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %ecx
.L108:
	testb	%cl, %cl
	je	.L113
.L109:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%rbx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	cmpl	$402, %edx
	je	.L231
	leal	-710(%rdx), %esi
	cmpl	$22, %esi
	ja	.L110
	leal	-622(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L109
	.p2align 4,,10
	.p2align 3
.L113:
	leal	-64285(%rdx), %ecx
	cmpl	$49, %ecx
	jbe	.L363
.L115:
	cmpq	$0, 104(%rsp)
	je	.L240
	testb	$8, 16(%r13)
	jne	.L364
.L122:
	testb	$2, 40(%rsp)
	jne	.L365
.L240:
	movl	$6, 8(%rsp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L229:
	movl	$7, 8(%rsp)
	jmp	.L104
.L344:
	movl	116(%rsp), %eax
	movl	120(%rsp), %edi
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	$34, %edi
	movl	%edx, (%rbx)
	movl	$0, (%r14)
	ja	.L198
	movq	%rsi, %rbx
.L98:
	sall	$3, %eax
	movl	%eax, (%r14)
.L100:
	addq	$1, %rcx
.L77:
	cmpq	%rcx, %r13
	je	.L366
.L101:
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r10
	jnb	.L75
	movq	%r13, %rax
	movq	%rbp, %r13
	movq	%rax, %rbp
.L353:
	movl	$5, 8(%rsp)
.L74:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L125
.L359:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L15:
	movl	8(%rsp), %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$34, %edi
	jbe	.L98
.L99:
	movl	%eax, (%rbx)
	addq	$4, %rbx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L82:
	cmpl	$1487, %edx
	jbe	.L335
	cmpl	$1522, %edx
	jbe	.L367
	.p2align 4,,10
	.p2align 3
.L335:
	movl	%edx, (%rbx)
	movl	$0, (%r14)
.L198:
	addq	$8, %rbx
	cmpq	%rbx, %r10
	movq	%rsi, %rbx
	jnb	.L99
	cmpq	%rcx, %r13
	jne	.L101
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r13, %rax
	movq	%rbp, %r13
	movq	%rax, %rbp
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$5, 8(%rsp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L129:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L128
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$-125, %ecx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	leal	-1456(%rdx), %esi
	cmpl	$68, %esi
	jbe	.L368
	leal	-8206(%rdx), %esi
	cmpl	$44, %esi
	ja	.L112
	leal	-8026(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L368:
	leal	-1345(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L360:
	movq	56(%rsp), %rsi
	movq	%rbx, 0(%r13)
	movq	%rbp, %r10
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L127:
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 115(%rsp)
	je	.L15
	cmpl	$7, 8(%rsp)
	jne	.L15
	movq	16(%rsp), %rax
	movq	%r10, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L192
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rcx
	je	.L194
.L193:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L193
.L194:
	movq	16(%rsp), %rax
	movq	%r10, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L241:
	movl	8(%rsp), %r11d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L362:
	cmpq	$0, 104(%rsp)
	je	.L224
	testb	$2, 40(%rsp)
	jne	.L369
.L224:
	movq	%r13, %rax
	movl	$6, 8(%rsp)
	movq	%rbp, %r13
	movq	%rax, %rbp
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$8362, %edx
	je	.L232
	cmpl	$8364, %edx
	je	.L233
	cmpl	$8482, %edx
	je	.L234
	movl	%edx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	jne	.L113
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L361:
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
	movl	64(%rsp), %eax
	movl	%eax, (%r14)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	movl	16(%r13), %eax
	je	.L370
	leaq	168(%rsp), %rdi
	movl	%eax, 40(%rsp)
	movq	%r12, 160(%rsp)
	movq	%r15, 168(%rsp)
	movq	%r15, %rbx
	movl	$4, %eax
	movq	%rdi, 64(%rsp)
.L165:
	cmpq	%r12, %rbp
	je	.L371
.L186:
	leaq	4(%r12), %rcx
	cmpq	%rcx, %rbp
	jb	.L250
	cmpq	%rbx, 8(%rsp)
	jbe	.L261
	movl	(%r12), %edx
	cmpl	$127, %edx
	ja	.L167
	leaq	1(%rbx), %rcx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rbx)
	movq	160(%rsp), %rsi
	movq	168(%rsp), %rbx
	leaq	4(%rsi), %r12
	cmpq	%r12, %rbp
	movq	%r12, 160(%rsp)
	jne	.L186
.L371:
	cltq
	movq	%rbp, %r12
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L225:
	movl	$1493, %r8d
	movl	$4, 88(%rsp)
	movl	$4, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	%r8d, %edx
	jb	.L335
	movl	88(%rsp), %r8d
	leaq	comp_table_data(%rip), %r9
	movzwl	(%r9,%r8,4), %r8d
	cmpl	%r8d, %edx
	ja	.L79
	movl	%eax, 116(%rsp)
	movl	%edi, 120(%rsp)
.L91:
	movl	96(%rsp), %r9d
	movl	88(%rsp), %eax
	leaq	comp_table_data(%rip), %r8
	addl	%r9d, %eax
	shrl	%eax
	movl	%eax, %edi
	movzwl	(%r8,%rdi,4), %r8d
	cmpl	%r8d, %edx
	je	.L92
	jnb	.L93
	cmpl	%eax, %r9d
	je	.L344
	movl	%eax, 88(%rsp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$1513, %r8d
	movl	$33, 88(%rsp)
	movl	$32, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1497, %r8d
	movl	$0, 88(%rsp)
	movl	$0, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$1488, %r8d
	movl	$28, 88(%rsp)
	movl	$5, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$1489, %r8d
	movl	$31, 88(%rsp)
	movl	$29, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1488, %r8d
	movl	$3, 88(%rsp)
	movl	$3, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$1513, %r8d
	movl	$35, 88(%rsp)
	movl	$34, 96(%rsp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L357:
	testq	%rsi, %rsi
	jne	.L372
	cmpl	$4, %edi
	movq	%r15, 160(%rsp)
	movq	%rbp, 168(%rsp)
	ja	.L33
	leaq	152(%rsp), %rcx
	movslq	%edi, %rax
	xorl	%ebx, %ebx
	movq	%rcx, 40(%rsp)
.L34:
	movzbl	4(%r14,%rbx), %edx
	movb	%dl, (%rcx,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L34
	movq	%r15, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %r10
	jb	.L373
	cmpq	%r12, %rbp
	jnb	.L218
	leaq	1(%r15), %rax
	leaq	151(%rsp), %rsi
.L42:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%dl, (%rsi,%rbx)
	ja	.L268
	cmpq	%rcx, %r10
	ja	.L42
.L268:
	movl	152(%rsp), %eax
	movq	40(%rsp), %rsi
	cmpl	$127, %eax
	movq	%rsi, 160(%rsp)
	jbe	.L374
	leal	-160(%rax), %edx
	cmpl	$87, %edx
	jbe	.L352
	cmpl	$402, %eax
	je	.L211
	leal	-710(%rax), %edx
	cmpl	$22, %edx
	ja	.L50
	leal	-622(%rax), %edx
.L352:
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L48:
	testb	%dl, %dl
	je	.L53
.L49:
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	%dl, 0(%rbp)
.L62:
	movq	160(%rsp), %rax
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 160(%rsp)
	je	.L46
.L45:
	movl	(%r14), %edx
	subq	40(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L375
	movq	16(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	168(%rsp), %rbp
	movl	%edx, 64(%rsp)
	movl	16(%r13), %r11d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r15
	movq	48(%rsp), %rax
	movl	%edx, (%r14)
	movq	96(%rax), %rax
	jmp	.L31
.L363:
	movl	$64313, %esi
	movl	$33, %r8d
	xorl	%edi, %edi
	movl	$33, %ecx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L376:
	cmpl	%ecx, %edi
	je	.L115
	movl	%ecx, %r8d
.L118:
	leal	(%r8,%rdi), %ecx
	leaq	decomp_table(%rip), %r9
	movl	%ecx, %esi
	shrl	%esi
	movzwl	(%r9,%rsi,8), %esi
.L114:
	shrl	%ecx
	cmpl	%esi, %edx
	je	.L116
	jb	.L376
	cmpl	%ecx, %edi
	je	.L377
	movl	%ecx, %edi
	jmp	.L118
.L232:
	movl	$-92, %ecx
	jmp	.L109
.L370:
	cmpq	%r12, %rbp
	movl	%eax, 116(%rsp)
	je	.L378
	leaq	4(%r15), %r9
	cmpq	%r9, 8(%rsp)
	movq	%r15, %rbx
	movl	$4, 96(%rsp)
	leaq	to_ucs4(%rip), %r8
	jb	.L379
	movq	8(%rsp), %rdi
	movl	%r11d, 40(%rsp)
	movq	%r10, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L135:
	movzbl	(%r12), %eax
	cmpl	$127, %eax
	jbe	.L138
	addl	$-128, %eax
	movzwl	(%r8,%rax,2), %eax
	testl	%eax, %eax
	je	.L380
.L138:
	movl	(%r14), %edx
	leal	-1488(%rax), %ecx
	sarl	$3, %edx
	testl	%edx, %edx
	je	.L140
	leal	-1456(%rax), %esi
	cmpl	$20, %esi
	ja	.L141
	leal	-1460(%rax), %esi
	cmpl	$14, %esi
	ja	.L141
	leaq	.L143(%rip), %r11
	movslq	(%r11,%rsi,4), %rsi
	addq	%r11, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L143:
	.long	.L142-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L144-.L143
	.long	.L145-.L143
	.long	.L246-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L147-.L143
	.long	.L141-.L143
	.long	.L141-.L143
	.long	.L148-.L143
	.long	.L141-.L143
	.long	.L149-.L143
	.long	.L150-.L143
	.text
.L348:
	movl	120(%rsp), %eax
	movl	124(%rsp), %ecx
.L141:
	cmpl	$34, %ecx
	movl	%edx, (%rbx)
	movl	$0, (%r14)
	ja	.L201
	movq	%r9, %rbx
.L160:
	sall	$3, %eax
	movl	%eax, (%r14)
.L162:
	addq	$1, %r12
.L139:
	cmpq	%r12, %rbp
	je	.L381
	leaq	4(%rbx), %r9
	cmpq	%r9, %rdi
	jnb	.L135
	movl	40(%rsp), %r11d
	movq	64(%rsp), %r10
	movl	$5, %eax
.L137:
	movq	16(%rsp), %rsi
	movq	%r12, (%rsi)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	%eax, 96(%rsp)
	je	.L382
	movl	%eax, 96(%rsp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L167:
	leal	-160(%rdx), %esi
	cmpl	$87, %esi
	ja	.L169
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %ecx
.L170:
	testb	%cl, %cl
	je	.L175
.L171:
	leaq	1(%rbx), %rdx
	movq	%rdx, 168(%rsp)
	movb	%cl, (%rbx)
	movq	160(%rsp), %rdi
	movq	168(%rsp), %rbx
	leaq	4(%rdi), %r12
	movq	%r12, 160(%rsp)
	jmp	.L165
.L382:
	movl	88(%rsp), %r8d
	leaq	comp_table_data(%rip), %r9
	movl	116(%rsp), %eax
	movl	120(%rsp), %edi
	movzwl	(%r9,%r8,4), %r9d
	cmpl	%r9d, %edx
	jne	.L79
	movq	%r8, %rdi
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	comp_table_data(%rip), %rax
	movzwl	2(%rax,%rdi,4), %eax
	leal	-64298(%rax), %edx
	cmpl	$1, %edx
	jbe	.L98
	cmpl	$64329, %eax
	je	.L98
	movl	%eax, (%rbx)
	movl	$0, (%r14)
	movq	%rsi, %rbx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L140:
	cmpl	$34, %ecx
	jbe	.L160
.L161:
	movl	%eax, (%rbx)
	addq	$4, %rbx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	pushq	112(%rsp)
	movq	32(%rsp), %rax
	movq	%r11, %r9
	movq	64(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r13, %rsi
	movq	%r11, 104(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L122
	cmpl	$5, 8(%rsp)
	jne	.L103
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$402, %edx
	je	.L252
	leal	-710(%rdx), %esi
	cmpl	$22, %esi
	jbe	.L383
	leal	-1456(%rdx), %esi
	cmpl	$68, %esi
	ja	.L173
	leal	-1345(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L170
.L365:
	movq	104(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L103
.L369:
	movq	104(%rsp), %rax
	addq	$1, %rcx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L77
.L356:
	cmpq	$0, 32(%rsp)
	jne	.L384
	cmpl	$1, %ebp
	movq	32(%r13), %rbx
	jne	.L19
	movl	(%rbx), %ebp
	movq	0(%r13), %rax
	testl	%ebp, %ebp
	je	.L20
	movq	48(%rsp), %rsi
	cmpq	$0, 96(%rsi)
	je	.L385
	movl	$0, (%rbx)
.L20:
	testl	%edx, %edx
	je	.L26
.L23:
	movq	%rax, 0(%r13)
	movl	$0, 8(%rsp)
	jmp	.L15
.L218:
	movl	$5, 8(%rsp)
	jmp	.L15
.L144:
	cmpl	$1487, %edx
	jbe	.L337
	cmpl	$1522, %edx
	jbe	.L386
.L337:
	movl	%edx, (%rbx)
	movl	$0, (%r14)
.L201:
	addq	$8, %rbx
	cmpq	%rbx, %rdi
	movq	%r9, %rbx
	jnb	.L161
	jmp	.L139
.L264:
	movq	%rbp, %r10
	movl	%r11d, 8(%rsp)
	jmp	.L127
.L250:
	movl	$7, %eax
.L166:
	movq	16(%rsp), %rdi
	movq	136(%rsp), %rsi
	movq	%r12, (%rdi)
	movq	%rsi, 8(%rsp)
.L164:
	cmpq	8(%rsp), %rbx
	jne	.L134
	cmpq	$5, %rax
	jne	.L133
	cmpq	%rbx, %r15
	jne	.L128
.L136:
	subl	$1, 20(%r13)
	jmp	.L128
.L389:
	movq	%r10, 96(%rsp)
	movl	%r11d, 88(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r13, %rsi
	pushq	112(%rsp)
	movq	32(%rsp), %rax
	movq	80(%rsp), %r9
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	160(%rsp), %r12
	movq	168(%rsp), %rbx
	movl	88(%rsp), %r11d
	movq	96(%rsp), %r10
	je	.L184
	cmpl	$5, %eax
	jne	.L165
.L261:
	movl	$5, %eax
	jmp	.L166
.L377:
	leaq	decomp_table(%rip), %rdi
	movl	%r8d, %ecx
	movzwl	(%rdi,%rcx,8), %ecx
	cmpl	%ecx, %edx
	jne	.L115
	movl	%r8d, %ecx
.L116:
	leaq	decomp_table(%rip), %rdi
	leaq	from_ucs4(%rip), %rsi
	movzwl	2(%rdi,%rcx,8), %edx
	subl	$1345, %edx
	movzbl	(%rsi,%rdx), %esi
	testb	%sil, %sil
	je	.L181
	leaq	decomp_table(%rip), %rdi
	movsbq	5(%rdi,%rcx,8), %rdx
	testb	%dl, %dl
	js	.L387
	leaq	2(%rbx), %rdi
	cmpq	%rdi, %r10
	jbe	.L238
	leaq	1(%rbx), %rax
	leaq	comb_table(%rip), %rdi
	movq	%rax, 152(%rsp)
	leaq	decomp_table(%rip), %rax
	movb	%sil, (%rbx)
	movzbl	(%rdi,%rdx), %edx
	movzbl	4(%rax,%rcx,8), %eax
	movq	152(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movzbl	(%rdi,%rax), %eax
	movq	%rsi, 152(%rsp)
	movb	%al, (%rcx)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rax)
.L121:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L103
.L19:
	movq	$0, (%rbx)
	testb	$1, 16(%r13)
	movl	$0, 8(%rsp)
	jne	.L15
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	pushq	%rax
	pushq	%rbp
.L354:
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	88(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%rbp
	popq	%r12
	jmp	.L15
.L233:
	movl	$-128, %ecx
	jmp	.L109
.L383:
	leal	-622(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L170
.L374:
	leaq	1(%rbp), %rdx
	movq	%rdx, 168(%rsp)
	movb	%al, 0(%rbp)
	movq	160(%rsp), %rax
	addq	$4, %rax
	cmpq	%rsi, %rax
	movq	%rax, 160(%rsp)
	jne	.L45
.L46:
	movq	48(%rsp), %rax
	movq	16(%rsp), %rdi
	movl	(%r14), %esi
	movl	16(%r13), %r11d
	movq	96(%rax), %rax
	movq	(%rdi), %r15
	movl	%esi, 64(%rsp)
	jmp	.L31
.L387:
	leaq	1(%rbx), %rdx
	cmpq	%rdx, %r10
	jbe	.L238
	movzbl	4(%rdi,%rcx,8), %eax
	movq	%rdx, 152(%rsp)
	movb	%sil, (%rbx)
	leaq	comb_table(%rip), %rsi
	movq	152(%rsp), %rdx
	movzbl	(%rsi,%rax), %eax
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	jmp	.L121
.L149:
	movl	$32, 88(%rsp)
	movl	$33, %r10d
	movl	$1513, %esi
.L146:
	cmpl	%esi, %edx
	jb	.L337
	leaq	comp_table_data(%rip), %r11
	movl	%r10d, %esi
	movzwl	(%r11,%rsi,4), %esi
	cmpl	%esi, %edx
	ja	.L141
	movl	88(%rsp), %r11d
	movl	%eax, 120(%rsp)
	movl	%ecx, 124(%rsp)
.L153:
	leal	(%r11,%r10), %esi
	leaq	comp_table_data(%rip), %rcx
	shrl	%esi
	movl	%esi, %eax
	movq	%rax, 88(%rsp)
	movzwl	(%rcx,%rax,4), %eax
	cmpl	%eax, %edx
	je	.L154
	jnb	.L155
	cmpl	%esi, %r11d
	je	.L348
	movl	%esi, %r10d
	jmp	.L153
.L148:
	movl	$29, 88(%rsp)
	movl	$31, %r10d
	movl	$1489, %esi
	jmp	.L146
.L142:
	movl	$0, 88(%rsp)
	xorl	%r10d, %r10d
	movl	$1497, %esi
	jmp	.L146
.L147:
	movl	$5, 88(%rsp)
	movl	$28, %r10d
	movl	$1488, %esi
	jmp	.L146
.L246:
	movl	$4, 88(%rsp)
	movl	$4, %r10d
	movl	$1493, %esi
	jmp	.L146
.L150:
	movl	$34, 88(%rsp)
	movl	$35, %r10d
	movl	$1513, %esi
	jmp	.L146
.L145:
	movl	$3, 88(%rsp)
	movl	$3, %r10d
	movl	$1488, %esi
	jmp	.L146
.L220:
	movq	%rbp, %rcx
	movq	%r15, %rbx
	movl	$4, 8(%rsp)
	jmp	.L74
.L252:
	movl	$-125, %ecx
	jmp	.L171
.L234:
	movl	$-103, %ecx
	jmp	.L109
.L367:
	movl	$2, 88(%rsp)
	movl	$1, 96(%rsp)
	movl	%eax, 116(%rsp)
	movl	%edi, 120(%rsp)
	jmp	.L91
.L175:
	leal	-64285(%rdx), %ecx
	cmpl	$49, %ecx
	jbe	.L388
.L177:
	cmpq	$0, 104(%rsp)
	je	.L262
	testb	$8, 16(%r13)
	jne	.L389
.L184:
	testb	$2, 40(%rsp)
	jne	.L390
.L262:
	movl	$6, %eax
	jmp	.L166
.L173:
	leal	-8206(%rdx), %esi
	cmpl	$44, %esi
	ja	.L174
	leal	-8026(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L170
.L155:
	cmpl	%esi, %r11d
	je	.L391
	movl	%esi, %r11d
	jmp	.L153
.L380:
	cmpq	$0, 104(%rsp)
	je	.L245
	testb	$2, 116(%rsp)
	jne	.L392
.L245:
	movl	40(%rsp), %r11d
	movq	64(%rsp), %r10
	movl	$6, %eax
	jmp	.L137
.L373:
	movq	%r10, %rdx
	movq	16(%rsp), %rax
	subq	%r15, %rdx
	addq	%rbx, %rdx
	cmpq	$4, %rdx
	movq	%r10, (%rax)
	ja	.L36
	cmpq	%rdx, %rbx
	leaq	1(%r15), %rax
	jnb	.L38
.L39:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%r14,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rdx
	jne	.L39
.L38:
	movl	$7, 8(%rsp)
	jmp	.L15
.L381:
	movl	40(%rsp), %r11d
	movq	64(%rsp), %r10
	movq	%rbp, %r12
	movslq	96(%rsp), %rax
	jmp	.L137
.L391:
	leaq	comp_table_data(%rip), %rsi
	movl	120(%rsp), %eax
	movl	124(%rsp), %ecx
	movzwl	(%rsi,%r10,4), %esi
	cmpl	%esi, %edx
	jne	.L141
	movq	%r10, 88(%rsp)
.L154:
	movq	88(%rsp), %rsi
	leaq	comp_table_data(%rip), %rax
	movzwl	2(%rax,%rsi,4), %eax
	leal	-64298(%rax), %edx
	cmpl	$1, %edx
	jbe	.L160
	cmpl	$64329, %eax
	je	.L160
	movl	%eax, (%rbx)
	movl	$0, (%r14)
	movq	%r9, %rbx
	jmp	.L162
.L174:
	cmpl	$8362, %edx
	je	.L253
	cmpl	$8364, %edx
	je	.L254
	cmpl	$8482, %edx
	je	.L255
	movl	%edx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	jne	.L175
	movq	%rcx, 160(%rsp)
	movq	%rcx, %r12
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	4(%rax), %r12
	cmpq	8(%r13), %r12
	ja	.L218
	movl	%ebp, %edx
	sarl	$3, %edx
	movl	%edx, (%rax)
	testb	$1, 16(%r13)
	movl	$0, (%rbx)
	jne	.L393
	movq	24(%rsp), %r15
	movq	%rax, 168(%rsp)
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	leaq	168(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	pushq	$0
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r15
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%r14
	popq	%r15
	je	.L26
	cmpq	168(%rsp), %r12
	jne	.L394
.L27:
	movl	8(%rsp), %r13d
	testl	%r13d, %r13d
	jne	.L15
.L26:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	pushq	%rax
	pushq	$1
	jmp	.L354
.L379:
	cmpq	%r15, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
	je	.L136
.L134:
	leaq	__PRETTY_FUNCTION__.9222(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	leal	-1456(%rax), %edx
	cmpl	$68, %edx
	ja	.L51
	leal	-1345(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L48
.L211:
	movl	$-125, %edx
	jmp	.L49
.L390:
	movq	104(%rsp), %rax
	addq	$4, %r12
	movq	%r12, 160(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L165
.L392:
	movq	104(%rsp), %rax
	addq	$1, %r12
	movl	$6, 96(%rsp)
	addq	$1, (%rax)
	jmp	.L139
.L388:
	movl	$64313, %esi
	movl	$33, %edi
	xorl	%r8d, %r8d
	movl	$33, %ecx
	jmp	.L176
.L395:
	cmpl	%ecx, %r8d
	je	.L177
	movl	%ecx, %edi
.L180:
	leal	(%r8,%rdi), %ecx
	leaq	decomp_table(%rip), %r9
	movl	%ecx, %esi
	shrl	%esi
	movzwl	(%r9,%rsi,8), %esi
.L176:
	shrl	%ecx
	cmpl	%esi, %edx
	je	.L178
	jb	.L395
	cmpl	%ecx, %r8d
	je	.L396
	movl	%ecx, %r8d
	jmp	.L180
.L253:
	movl	$-92, %ecx
	jmp	.L171
.L53:
	leal	-64285(%rax), %edx
	cmpl	$49, %edx
	jbe	.L397
.L55:
	cmpq	$0, 104(%rsp)
	je	.L219
	testb	$8, %r11b
	jne	.L398
	andl	$2, %r11d
	je	.L219
	movq	40(%rsp), %rdx
.L197:
	movq	104(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 160(%rsp)
.L65:
	cmpq	40(%rsp), %rax
	jne	.L45
.L219:
	movl	$6, 8(%rsp)
	jmp	.L15
.L396:
	leaq	decomp_table(%rip), %rsi
	movl	%edi, %ecx
	movzwl	(%rsi,%rcx,8), %ecx
	cmpl	%ecx, %edx
	jne	.L177
	movl	%edi, %ecx
.L178:
	leaq	decomp_table(%rip), %rdi
	leaq	from_ucs4(%rip), %rsi
	movzwl	2(%rdi,%rcx,8), %edx
	subl	$1345, %edx
	movzbl	(%rsi,%rdx), %esi
	testb	%sil, %sil
	je	.L181
	movsbq	5(%rdi,%rcx,8), %rdx
	testb	%dl, %dl
	js	.L399
	leaq	2(%rbx), %rdi
	cmpq	%rdi, 8(%rsp)
	jbe	.L261
	leaq	1(%rbx), %rdi
	movq	%rdi, 168(%rsp)
	movb	%sil, (%rbx)
	leaq	decomp_table(%rip), %rsi
	movzbl	4(%rsi,%rcx,8), %ecx
	movq	168(%rsp), %rsi
	leaq	1(%rsi), %rdi
	movq	%rdi, 168(%rsp)
	leaq	comb_table(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	movzbl	(%rdi,%rdx), %edx
	movb	%cl, (%rsi)
	movq	168(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 168(%rsp)
	movb	%dl, (%rcx)
.L183:
	movq	160(%rsp), %rdi
	movq	168(%rsp), %rbx
	leaq	4(%rdi), %r12
	movq	%r12, 160(%rsp)
	jmp	.L165
.L378:
	cmpq	%r15, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, %r10
	movq	%r10, (%rax)
	jne	.L134
.L133:
	leaq	__PRETTY_FUNCTION__.9222(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	leal	-8206(%rax), %edx
	cmpl	$44, %edx
	ja	.L52
	leal	-8026(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L48
.L254:
	movl	$-128, %ecx
	jmp	.L171
.L399:
	leaq	1(%rbx), %rdx
	cmpq	%rdx, 8(%rsp)
	jbe	.L261
	movq	%rdx, 168(%rsp)
	movzbl	4(%rdi,%rcx,8), %edx
	leaq	comb_table(%rip), %rdi
	movb	%sil, (%rbx)
	movq	168(%rsp), %rcx
	movzbl	(%rdi,%rdx), %edx
	leaq	1(%rcx), %rsi
	movq	%rsi, 168(%rsp)
	movb	%dl, (%rcx)
	jmp	.L183
.L397:
	movl	$64313, %edi
	movl	$33, %esi
	xorl	%ecx, %ecx
	movl	$33, %edx
	leaq	decomp_table(%rip), %r8
	jmp	.L54
.L401:
	cmpl	%edx, %ecx
	je	.L55
	movl	%edx, %esi
.L58:
	leal	(%rcx,%rsi), %edx
	movl	%edx, %edi
	shrl	%edi
	movzwl	(%r8,%rdi,8), %edi
.L54:
	shrl	%edx
	cmpl	%edi, %eax
	je	.L400
	jb	.L401
	cmpl	%edx, %ecx
	je	.L402
	movl	%edx, %ecx
	jmp	.L58
.L255:
	movl	$-103, %ecx
	jmp	.L171
.L398:
	movq	40(%rsp), %rax
	movq	%r10, 96(%rsp)
	leaq	160(%rsp), %rcx
	movl	%r11d, 88(%rsp)
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r13, %rsi
	addq	%rbx, %rax
	movq	%rax, 72(%rsp)
	pushq	112(%rsp)
	movq	%rax, %r8
	movq	64(%rsp), %rdi
	leaq	184(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movl	88(%rsp), %r11d
	movq	96(%rsp), %r10
	movq	160(%rsp), %rax
	je	.L403
	cmpq	40(%rsp), %rax
	jne	.L45
	cmpl	$7, 8(%rsp)
	jne	.L68
	movq	40(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 64(%rsp)
	je	.L404
	movl	(%r14), %eax
	movq	16(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rsi)
	cmpq	%rdx, %rbx
	jle	.L405
	cmpq	$4, %rbx
	ja	.L406
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r14)
	je	.L38
	movq	40(%rsp), %rcx
	xorl	%eax, %eax
.L72:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%r14,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L72
	jmp	.L38
.L52:
	cmpl	$8362, %eax
	je	.L212
	cmpl	$8364, %eax
	je	.L213
	cmpl	$8482, %eax
	je	.L214
	movl	%eax, %edx
	shrl	$7, %edx
	cmpl	$7168, %edx
	jne	.L53
	movq	40(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L45
.L406:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L214:
	movl	$-103, %edx
	jmp	.L49
.L213:
	movl	$-128, %edx
	jmp	.L49
.L212:
	movl	$-92, %edx
	jmp	.L49
.L33:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L192:
	leaq	__PRETTY_FUNCTION__.9222(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L384:
	leaq	__PRETTY_FUNCTION__.9222(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L402:
	leaq	decomp_table(%rip), %rcx
	movl	%esi, %edx
	movzwl	(%rcx,%rdx,8), %edx
	cmpl	%edx, %eax
	jne	.L55
	movl	%esi, %edx
.L56:
	movzwl	2(%rcx,%rdx,8), %eax
	leaq	from_ucs4(%rip), %rsi
	subl	$1345, %eax
	movzbl	(%rsi,%rax), %esi
	testb	%sil, %sil
	je	.L407
	movsbq	5(%rcx,%rdx,8), %rax
	testb	%al, %al
	js	.L408
	leaq	2(%rbp), %rdi
	cmpq	%rdi, %r12
	jbe	.L218
	leaq	1(%rbp), %rdi
	movzbl	4(%rcx,%rdx,8), %edx
	movq	%rdi, 168(%rsp)
	movb	%sil, 0(%rbp)
	movq	168(%rsp), %rsi
	leaq	1(%rsi), %rcx
	movq	%rcx, 168(%rsp)
	leaq	comb_table(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	movzbl	(%rcx,%rax), %eax
	movb	%dl, (%rsi)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 168(%rsp)
	movb	%al, (%rdx)
	jmp	.L62
.L405:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L404:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L68:
	cmpl	$0, 8(%rsp)
	jne	.L15
	movq	16(%rsp), %rsi
	movq	48(%rsp), %rax
	movl	16(%r13), %r11d
	movq	(%rsi), %r15
	movl	(%r14), %esi
	movq	96(%rax), %rax
	movl	%esi, 64(%rsp)
	jmp	.L31
.L403:
	andb	$2, %r11b
	movq	%rax, %rdx
	je	.L65
	jmp	.L197
.L408:
	leaq	1(%rbp), %rax
	cmpq	%rax, %r12
	jbe	.L218
	movq	%rax, 168(%rsp)
	movb	%sil, 0(%rbp)
	movzbl	4(%rcx,%rdx,8), %eax
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	leaq	comb_table(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	movb	%al, (%rdx)
	jmp	.L62
.L407:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L400:
	leaq	decomp_table(%rip), %rcx
	jmp	.L56
.L375:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L394:
	movl	%ebp, (%rbx)
	jmp	.L27
.L393:
	movq	%r12, %rax
	jmp	.L23
.L386:
	movl	$2, %r10d
	movl	$1, %r11d
	movl	%eax, 120(%rsp)
	movl	%ecx, 124(%rsp)
	jmp	.L153
.L372:
	leaq	__PRETTY_FUNCTION__.9222(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L36:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L181:
	leaq	__PRETTY_FUNCTION__.9122(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9122, @object
	.size	__PRETTY_FUNCTION__.9122, 10
__PRETTY_FUNCTION__.9122:
	.string	"to_cp1255"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9141, @object
	.size	__PRETTY_FUNCTION__.9141, 17
__PRETTY_FUNCTION__.9141:
	.string	"to_cp1255_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9222, @object
	.size	__PRETTY_FUNCTION__.9222, 6
__PRETTY_FUNCTION__.9222:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	decomp_table, @object
	.size	decomp_table, 272
decomp_table:
	.value	-1251
	.value	1497
	.byte	0
	.byte	-1
	.zero	2
	.value	-1249
	.value	1522
	.byte	1
	.byte	-1
	.zero	2
	.value	-1238
	.value	1513
	.byte	6
	.byte	-1
	.zero	2
	.value	-1237
	.value	1513
	.byte	7
	.byte	-1
	.zero	2
	.value	-1236
	.value	1513
	.byte	4
	.byte	6
	.zero	2
	.value	-1235
	.value	1513
	.byte	4
	.byte	7
	.zero	2
	.value	-1234
	.value	1488
	.byte	1
	.byte	-1
	.zero	2
	.value	-1233
	.value	1488
	.byte	2
	.byte	-1
	.zero	2
	.value	-1232
	.value	1488
	.byte	4
	.byte	-1
	.zero	2
	.value	-1231
	.value	1489
	.byte	4
	.byte	-1
	.zero	2
	.value	-1230
	.value	1490
	.byte	4
	.byte	-1
	.zero	2
	.value	-1229
	.value	1491
	.byte	4
	.byte	-1
	.zero	2
	.value	-1228
	.value	1492
	.byte	4
	.byte	-1
	.zero	2
	.value	-1227
	.value	1493
	.byte	4
	.byte	-1
	.zero	2
	.value	-1226
	.value	1494
	.byte	4
	.byte	-1
	.zero	2
	.value	-1224
	.value	1496
	.byte	4
	.byte	-1
	.zero	2
	.value	-1223
	.value	1497
	.byte	4
	.byte	-1
	.zero	2
	.value	-1222
	.value	1498
	.byte	4
	.byte	-1
	.zero	2
	.value	-1221
	.value	1499
	.byte	4
	.byte	-1
	.zero	2
	.value	-1220
	.value	1500
	.byte	4
	.byte	-1
	.zero	2
	.value	-1218
	.value	1502
	.byte	4
	.byte	-1
	.zero	2
	.value	-1216
	.value	1504
	.byte	4
	.byte	-1
	.zero	2
	.value	-1215
	.value	1505
	.byte	4
	.byte	-1
	.zero	2
	.value	-1213
	.value	1507
	.byte	4
	.byte	-1
	.zero	2
	.value	-1212
	.value	1508
	.byte	4
	.byte	-1
	.zero	2
	.value	-1210
	.value	1510
	.byte	4
	.byte	-1
	.zero	2
	.value	-1209
	.value	1511
	.byte	4
	.byte	-1
	.zero	2
	.value	-1208
	.value	1512
	.byte	4
	.byte	-1
	.zero	2
	.value	-1207
	.value	1513
	.byte	4
	.byte	-1
	.zero	2
	.value	-1206
	.value	1514
	.byte	4
	.byte	-1
	.zero	2
	.value	-1205
	.value	1493
	.byte	3
	.byte	-1
	.zero	2
	.value	-1204
	.value	1489
	.byte	5
	.byte	-1
	.zero	2
	.value	-1203
	.value	1499
	.byte	5
	.byte	-1
	.zero	2
	.value	-1202
	.value	1508
	.byte	5
	.byte	-1
	.zero	2
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	comb_table, @object
	.size	comb_table, 8
comb_table:
	.byte	-60
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-52
	.byte	-49
	.byte	-47
	.byte	-46
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 225
from_ucs4:
	.byte	-96
	.byte	-95
	.byte	-94
	.byte	-93
	.byte	0
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	0
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	0
	.byte	-69
	.byte	-68
	.byte	-67
	.byte	-66
	.byte	-65
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-86
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-70
	.byte	-120
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-104
	.byte	-64
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	0
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	-48
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-3
	.byte	-2
	.byte	0
	.byte	0
	.byte	0
	.byte	-106
	.byte	-105
	.byte	0
	.byte	0
	.byte	0
	.byte	-111
	.byte	-110
	.byte	-126
	.byte	0
	.byte	-109
	.byte	-108
	.byte	-124
	.byte	0
	.byte	-122
	.byte	-121
	.byte	-107
	.byte	0
	.byte	0
	.byte	0
	.byte	-123
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-119
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-117
	.byte	-101
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 144
comp_table_data:
	.value	1497
	.value	-1251
	.value	1488
	.value	-1234
	.value	1522
	.value	-1249
	.value	1488
	.value	-1233
	.value	1493
	.value	-1205
	.value	1488
	.value	-1232
	.value	1489
	.value	-1231
	.value	1490
	.value	-1230
	.value	1491
	.value	-1229
	.value	1492
	.value	-1228
	.value	1493
	.value	-1227
	.value	1494
	.value	-1226
	.value	1496
	.value	-1224
	.value	1497
	.value	-1223
	.value	1498
	.value	-1222
	.value	1499
	.value	-1221
	.value	1500
	.value	-1220
	.value	1502
	.value	-1218
	.value	1504
	.value	-1216
	.value	1505
	.value	-1215
	.value	1507
	.value	-1213
	.value	1508
	.value	-1212
	.value	1510
	.value	-1210
	.value	1511
	.value	-1209
	.value	1512
	.value	-1208
	.value	1513
	.value	-1207
	.value	1514
	.value	-1206
	.value	-1238
	.value	-1236
	.value	-1237
	.value	-1235
	.value	1489
	.value	-1204
	.value	1499
	.value	-1203
	.value	1508
	.value	-1202
	.value	1513
	.value	-1238
	.value	-1207
	.value	-1236
	.value	1513
	.value	-1237
	.value	-1207
	.value	-1235
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 256
to_ucs4:
	.value	8364
	.value	0
	.value	8218
	.value	402
	.value	8222
	.value	8230
	.value	8224
	.value	8225
	.value	710
	.value	8240
	.value	0
	.value	8249
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	8216
	.value	8217
	.value	8220
	.value	8221
	.value	8226
	.value	8211
	.value	8212
	.value	732
	.value	8482
	.value	0
	.value	8250
	.value	0
	.value	0
	.value	0
	.value	0
	.value	160
	.value	161
	.value	162
	.value	163
	.value	8362
	.value	165
	.value	166
	.value	167
	.value	168
	.value	169
	.value	215
	.value	171
	.value	172
	.value	173
	.value	174
	.value	175
	.value	176
	.value	177
	.value	178
	.value	179
	.value	180
	.value	181
	.value	182
	.value	183
	.value	184
	.value	185
	.value	247
	.value	187
	.value	188
	.value	189
	.value	190
	.value	191
	.value	1456
	.value	1457
	.value	1458
	.value	1459
	.value	1460
	.value	1461
	.value	1462
	.value	1463
	.value	1464
	.value	1465
	.value	0
	.value	1467
	.value	1468
	.value	1469
	.value	1470
	.value	1471
	.value	1472
	.value	1473
	.value	1474
	.value	1475
	.value	1520
	.value	1521
	.value	1522
	.value	1523
	.value	1524
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	1488
	.value	1489
	.value	1490
	.value	1491
	.value	1492
	.value	1493
	.value	1494
	.value	1495
	.value	1496
	.value	1497
	.value	1498
	.value	1499
	.value	1500
	.value	1501
	.value	1502
	.value	1503
	.value	1504
	.value	1505
	.value	1506
	.value	1507
	.value	1508
	.value	1509
	.value	1510
	.value	1511
	.value	1512
	.value	1513
	.value	1514
	.value	0
	.value	0
	.value	8206
	.value	8207
	.value	0
