	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	jne	.L1
	testb	%sil, %sil
	movl	$-1, %edx
	cmovne	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO_6937-2//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"\317 "
.LC2:
	.string	"\325"
.LC3:
	.string	"\340"
.LC4:
	.string	"\324"
.LC5:
	.string	"\272"
.LC6:
	.string	"\252"
.LC7:
	.string	"\271"
.LC8:
	.string	"\251"
.LC9:
	.string	"\320"
.LC10:
	.string	"../iconv/skeleton.c"
.LC11:
	.string	"outbufstart == NULL"
.LC12:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC14:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC15:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC16:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC17:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC18:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC19:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC21:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%r12), %r11d
	movq	%rsi, 64(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	testb	$1, %r11b
	movq	%r9, 56(%rsp)
	movl	224(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 24(%rsp)
	je	.L15
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L403
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	movl	232(%rsp), %r8d
	movq	8(%r12), %r13
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 56(%rsp)
	movq	(%rax), %r10
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%r8d, %r8d
	movq	%rax, 80(%rsp)
	jne	.L404
.L22:
	movq	40(%rsp), %rax
	leaq	from_ucs4(%rip), %r15
	cmpq	$0, 96(%rax)
	je	.L405
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	movq	%r10, %rbx
	movq	%r14, %rax
	movl	$4, 8(%rsp)
.L108:
	cmpq	%rax, %rbp
	je	.L109
.L139:
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rbp
	jb	.L227
	cmpq	%rbx, %r13
	jbe	.L228
	movl	(%rax), %edx
	cmpl	$382, %edx
	ja	.L406
	movl	%edx, %ecx
	cmpb	$0, (%r15,%rcx,2)
	jne	.L133
	testl	%edx, %edx
	jne	.L402
.L133:
	leaq	(%r15,%rcx,2), %rcx
	movzbl	(%rcx), %edi
.L116:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%dil, (%rbx)
	movzbl	1(%rcx), %eax
	testb	%al, %al
	je	.L136
	movq	152(%rsp), %rbx
	cmpq	%rbx, %r13
	jbe	.L407
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
.L136:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L139
	.p2align 4,,10
	.p2align 3
.L109:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L408
.L140:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L409
	cmpq	%rbx, %r10
	movq	%r10, 48(%rsp)
	jnb	.L235
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r8
	popq	%r9
	je	.L144
	movq	136(%rsp), %rax
	movq	48(%rsp), %r10
	cmpq	%rbx, %rax
	movq	%rax, 8(%rsp)
	jne	.L410
.L143:
	testl	%r11d, %r11d
	jne	.L255
.L195:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r10
	movq	(%rax), %r14
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L97
.L405:
	cmpq	%rbp, %r14
	je	.L217
	leaq	4(%r10), %rcx
	movq	%r14, %rdx
	movq	%r10, %rbx
	cmpq	%rcx, %r13
	jb	.L219
	leaq	to_ucs4(%rip), %r8
	leaq	to_ucs4_comb(%rip), %r9
	movl	$4, 8(%rsp)
	andl	$2, %r11d
	.p2align 4,,10
	.p2align 3
.L99:
	movzbl	(%rdx), %eax
	leal	-193(%rax), %edi
	movl	%eax, %esi
	cmpl	$14, %edi
	jbe	.L411
	movl	%eax, %eax
	testb	%sil, %sil
	movl	(%r8,%rax,4), %eax
	je	.L106
	testl	%eax, %eax
	je	.L412
.L106:
	addq	$1, %rdx
.L105:
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L104:
	cmpq	%rdx, %rbp
	je	.L98
.L102:
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r13
	jnb	.L99
.L219:
	movl	$5, 8(%rsp)
.L98:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	je	.L140
.L408:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L14:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	movl	$7, 8(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L144:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L143
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L228:
	movl	$5, 8(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L406:
	cmpl	$8220, %edx
	je	.L112
	jbe	.L413
	cmpl	$8542, %edx
	ja	.L120
	cmpl	$8539, %edx
	jnb	.L121
	cmpl	$8482, %edx
	je	.L122
	cmpl	$8486, %edx
	je	.L123
	cmpl	$8221, %edx
	je	.L414
.L111:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L415
	.p2align 4,,10
	.p2align 3
.L402:
	cmpq	$0, 80(%rsp)
	je	.L234
	testb	$8, 16(%r12)
	jne	.L416
.L134:
	testb	$2, %r11b
	jne	.L417
.L234:
	movl	$6, 8(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L404:
	movq	32(%r12), %rbx
	movl	(%rbx), %eax
	movl	%eax, %r15d
	andl	$7, %r15d
	je	.L22
	testq	%rdi, %rdi
	jne	.L418
	movq	40(%rsp), %rsi
	cmpq	$0, 96(%rsi)
	je	.L419
	cmpl	$4, %r15d
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	ja	.L42
	leaq	136(%rsp), %rcx
	movslq	%r15d, %r15
	xorl	%eax, %eax
	movq	%rcx, 48(%rsp)
.L43:
	movzbl	4(%rbx,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r15, %rax
	jne	.L43
	movq	%r14, %rax
	subq	%r15, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L420
	cmpq	%r13, %r10
	jnb	.L87
	leaq	1(%r14), %rax
	leaq	135(%rsp), %rsi
.L51:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %r15
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %r15
	movb	%dl, (%rsi,%r15)
	ja	.L257
	cmpq	%rcx, %rbp
	ja	.L51
.L257:
	movq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	addq	%r15, %rax
	movq	%rax, 88(%rsp)
	movl	136(%rsp), %eax
	cmpl	$382, %eax
	ja	.L421
	leaq	from_ucs4(%rip), %rdx
	movl	%eax, %ecx
	cmpb	$0, (%rdx,%rcx,2)
	jne	.L78
	testl	%eax, %eax
	je	.L78
	cmpq	$0, 80(%rsp)
	je	.L72
	testb	$8, %r11b
	jne	.L422
	andl	$2, %r11d
	je	.L72
	movq	80(%rsp), %rax
	addq	$1, (%rax)
.L388:
	movq	48(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L413:
	cmpl	$733, %edx
	movl	$5, %ecx
	je	.L114
	jbe	.L423
	cmpl	$8216, %edx
	je	.L230
	cmpl	$8217, %edx
	je	.L118
	cmpl	$8212, %edx
	jne	.L111
	leaq	.LC9(%rip), %rcx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L407:
	subq	$1, %rbx
	movq	144(%rsp), %rax
	movl	$5, 8(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L409:
	movq	56(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L142:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	cmpl	$7, 8(%rsp)
	jne	.L14
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L197
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L199
.L198:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L198
.L199:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rbp
	jbe	.L220
	movzbl	1(%rdx), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L424
	leaq	(%rdi,%rdi,2), %rsi
	cltq
	salq	$5, %rsi
	addq	%rsi, %rax
	movl	(%r9,%rax,4), %eax
	testl	%eax, %eax
	je	.L425
	addq	$2, %rdx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L235:
	movl	8(%rsp), %r11d
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L412:
	cmpq	$0, 80(%rsp)
	je	.L226
	testl	%r11d, %r11d
	jne	.L426
.L226:
	movl	$6, 8(%rsp)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L410:
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L427
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	movq	%r10, %rdx
	movl	$4, %eax
.L161:
	cmpq	%r14, %rbp
	je	.L428
.L193:
	leaq	4(%r14), %rdi
	cmpq	%rdi, %rbp
	jb	.L245
	cmpq	%rdx, 8(%rsp)
	jbe	.L246
	movl	(%r14), %ecx
	cmpl	$382, %ecx
	ja	.L429
	movl	%ecx, %esi
	cmpb	$0, (%r15,%rsi,2)
	jne	.L187
	testl	%ecx, %ecx
	jne	.L430
.L187:
	leaq	(%r15,%rsi,2), %rsi
	movzbl	(%rsi), %r8d
.L169:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%r8b, (%rdx)
	movzbl	1(%rsi), %edx
	testb	%dl, %dl
	je	.L190
	movq	152(%rsp), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L431
	leaq	1(%rcx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rcx)
.L190:
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r14
	cmpq	%r14, %rbp
	movq	%r14, 144(%rsp)
	jne	.L193
.L428:
	cltq
	movq	%rbp, %r14
	jmp	.L162
.L122:
	leaq	.LC4(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L117:
	movzbl	(%rcx), %eax
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
	jmp	.L136
.L419:
	cmpl	$4, %r15d
	ja	.L25
	movzbl	4(%rbx), %edx
	cmpl	$1, %r15d
	movb	%dl, 152(%rsp)
	movl	$1, %edx
	je	.L26
	movzbl	5(%rbx), %edx
	movb	%dl, 153(%rsp)
	movl	$2, %edx
.L26:
	leaq	4(%r10), %rcx
	cmpq	%rcx, %r13
	jb	.L87
	movzbl	(%r14), %esi
	movb	%sil, 152(%rsp,%rdx)
	movzbl	152(%rsp), %esi
	leal	-193(%rsi), %r9d
	movl	%esi, %edi
	cmpl	$14, %r9d
	jbe	.L432
	leaq	to_ucs4(%rip), %rax
	movl	%esi, %esi
	movl	(%rax,%rsi,4), %eax
	leaq	152(%rsp), %rsi
	leaq	1(%rsi), %rdx
	testl	%eax, %eax
	je	.L433
.L36:
	movl	%eax, (%r10)
.L35:
	movl	(%rbx), %eax
	subq	%rsi, %rdx
	movq	%rdx, %r11
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %r11
	jle	.L434
	movq	16(%rsp), %rdi
	subq	%rdx, %r11
	andl	$-8, %eax
	addq	%r11, %r14
	movq	%rcx, %r10
	movl	16(%r12), %r11d
	movq	%r14, (%rdi)
	movl	%eax, (%rbx)
	jmp	.L22
.L230:
	leaq	.LC8(%rip), %rcx
	jmp	.L117
.L427:
	cmpq	%rbp, %r14
	je	.L435
	leaq	4(%r10), %rcx
	andl	$2, %ebx
	cmpq	%rcx, 8(%rsp)
	movq	%r10, %rdx
	movl	$4, %edi
	leaq	to_ucs4(%rip), %r9
	jb	.L436
	movl	%edi, 48(%rsp)
	movq	8(%rsp), %rdi
	.p2align 4,,10
	.p2align 3
.L150:
	movzbl	(%r14), %eax
	leal	-193(%rax), %r8d
	movl	%eax, %esi
	cmpl	$14, %r8d
	jbe	.L437
	movl	%eax, %eax
	testb	%sil, %sil
	movl	(%r9,%rax,4), %eax
	je	.L159
	testl	%eax, %eax
	je	.L438
.L159:
	addq	$1, %r14
.L158:
	movl	%eax, (%rdx)
	movq	%rcx, %rdx
.L157:
	cmpq	%r14, %rbp
	je	.L439
.L155:
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L150
	movl	$5, %eax
.L152:
	movq	16(%rsp), %rsi
	movq	%r14, (%rsi)
	jmp	.L160
.L78:
	leaq	(%rdx,%rcx,2), %rdx
	movzbl	(%rdx), %ecx
.L59:
	leaq	1(%r10), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%r10)
	movzbl	1(%rdx), %edx
	testb	%dl, %dl
	je	.L85
	movq	152(%rsp), %rax
	cmpq	%rax, %r13
	jbe	.L440
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rax)
.L85:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L389
.L71:
	movl	(%rbx), %edx
	subq	48(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L441
	movq	16(%rsp), %rsi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r10
	movl	16(%r12), %r11d
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r14
	movl	%edx, (%rbx)
	jmp	.L22
.L440:
	subq	$1, %rax
	movq	%rax, 152(%rsp)
	movq	144(%rsp), %rax
	cmpq	48(%rsp), %rax
	jne	.L71
.L87:
	movl	$5, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L120:
	cmpl	$8592, %edx
	jb	.L111
	cmpl	$8595, %edx
	jbe	.L125
	cmpl	$9834, %edx
	leaq	.LC2(%rip), %rcx
	je	.L117
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L416:
	movq	%r10, 88(%rsp)
	movl	%r11d, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movl	48(%rsp), %r11d
	movq	88(%rsp), %r10
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L134
	cmpl	$5, 8(%rsp)
	jne	.L108
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L423:
	cmpl	$711, %edx
	movl	$-49, %edi
	leaq	.LC1(%rip), %rcx
	je	.L116
	jb	.L111
	leal	-728(%rdx), %ecx
	cmpl	$3, %ecx
	ja	.L111
.L114:
	leaq	map.9087(%rip), %rax
	movb	$32, 137(%rsp)
	movzbl	(%rax,%rcx), %edi
	leaq	136(%rsp), %rcx
	movb	%dil, 136(%rsp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L417:
	movq	80(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L108
.L426:
	movq	80(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L104
.L403:
	cmpq	$0, 32(%rsp)
	jne	.L442
	movq	32(%r12), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	movl	%eax, 24(%rsp)
	popq	%r9
	popq	%r10
	jmp	.L14
.L255:
	movl	%r11d, 8(%rsp)
	jmp	.L142
.L112:
	leaq	.LC6(%rip), %rcx
	jmp	.L117
.L125:
	leal	28(%rdx), %edi
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rcx
	movb	%dil, 136(%rsp)
	jmp	.L116
.L118:
	leaq	.LC7(%rip), %rcx
	jmp	.L117
.L121:
	leal	-127(%rdx), %edi
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rcx
	movb	%dil, 136(%rsp)
	jmp	.L116
.L414:
	leaq	.LC5(%rip), %rcx
	jmp	.L117
.L123:
	leaq	.LC3(%rip), %rcx
	jmp	.L117
.L424:
	cmpq	$0, 80(%rsp)
	je	.L226
	testl	%r11d, %r11d
	je	.L226
	movq	80(%rsp), %rax
	movq	%rsi, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L102
.L245:
	movl	$7, %eax
.L162:
	movq	16(%rsp), %rdi
	movq	136(%rsp), %rsi
	movq	%r14, (%rdi)
	movq	%rsi, 8(%rsp)
.L160:
	cmpq	8(%rsp), %rdx
	jne	.L149
	cmpq	$5, %rax
	jne	.L148
.L186:
	cmpq	%r10, %rdx
	jne	.L143
.L151:
	subl	$1, 20(%r12)
	jmp	.L143
.L415:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %rax
	jmp	.L108
.L430:
	cmpq	$0, 80(%rsp)
	je	.L253
	testb	$8, 16(%r12)
	je	.L188
	movq	%r10, 88(%rsp)
	movl	%r11d, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	movl	48(%rsp), %r11d
	movq	88(%rsp), %r10
	je	.L188
	cmpl	$5, %eax
	jne	.L161
.L246:
	movl	$5, %eax
	jmp	.L162
.L429:
	cmpl	$8220, %ecx
	je	.L165
	jbe	.L443
	cmpl	$8542, %ecx
	ja	.L173
	cmpl	$8539, %ecx
	jnb	.L174
	cmpl	$8482, %ecx
	je	.L175
	cmpl	$8486, %ecx
	je	.L176
	cmpl	$8221, %ecx
	je	.L444
.L164:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L445
	cmpq	$0, 80(%rsp)
	je	.L253
	testb	$8, 16(%r12)
	jne	.L446
.L188:
	testb	$2, %bl
	jne	.L447
.L253:
	movl	$6, %eax
	jmp	.L162
.L220:
	movl	$7, 8(%rsp)
	jmp	.L98
.L425:
	cmpq	$0, 80(%rsp)
	je	.L226
	testl	%r11d, %r11d
	je	.L226
	movq	80(%rsp), %rax
	addq	$2, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L104
.L217:
	movq	%r14, %rdx
	movq	%r10, %rbx
	movl	$4, 8(%rsp)
	jmp	.L98
.L431:
	movq	136(%rsp), %rdx
	movq	144(%rsp), %rax
	subq	$1, %rcx
	movq	16(%rsp), %rsi
	cmpq	%rcx, %rdx
	movq	%rax, (%rsi)
	je	.L186
.L149:
	leaq	__PRETTY_FUNCTION__.9209(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L443:
	cmpl	$733, %ecx
	movl	$5, %esi
	je	.L167
	jbe	.L448
	cmpl	$8216, %ecx
	je	.L248
	cmpl	$8217, %ecx
	je	.L171
	cmpl	$8212, %ecx
	jne	.L164
	leaq	.LC9(%rip), %rsi
.L170:
	movzbl	(%rsi), %ecx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	jmp	.L190
.L438:
	cmpq	$0, 80(%rsp)
	je	.L244
	testl	%ebx, %ebx
	jne	.L449
.L244:
	movl	$6, %eax
	jmp	.L152
.L437:
	leaq	1(%r14), %rsi
	cmpq	%rsi, %rbp
	jbe	.L238
	movzbl	1(%r14), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L450
	leaq	(%r8,%r8,2), %rsi
	cltq
	salq	$5, %rsi
	addq	%rsi, %rax
	leaq	to_ucs4_comb(%rip), %rsi
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %eax
	je	.L451
	addq	$2, %r14
	jmp	.L158
.L420:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r14, %rax
	addq	%r15, %rax
	cmpq	$4, %rax
	ja	.L45
	addq	$1, %r14
	cmpq	%r15, %rax
	jbe	.L49
.L48:
	movq	%r14, 144(%rsp)
	movzbl	-1(%r14), %edx
	addq	$1, %r14
	movb	%dl, 4(%rbx,%r15)
	addq	$1, %r15
	cmpq	%r15, %rax
	jne	.L48
.L49:
	movl	$7, 8(%rsp)
	jmp	.L14
.L173:
	cmpl	$8592, %ecx
	jb	.L164
	cmpl	$8595, %ecx
	jbe	.L178
	cmpl	$9834, %ecx
	leaq	.LC2(%rip), %rsi
	je	.L170
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L421:
	cmpl	$8220, %eax
	je	.L55
	jbe	.L452
	cmpl	$8542, %eax
	ja	.L63
	cmpl	$8539, %eax
	jnb	.L64
	cmpl	$8482, %eax
	je	.L65
	cmpl	$8486, %eax
	je	.L66
	cmpl	$8221, %eax
	je	.L453
.L54:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L388
	cmpq	$0, 80(%rsp)
	je	.L72
	testb	$8, %r11b
	jne	.L454
.L73:
	andb	$2, %r11b
	jne	.L455
	movq	144(%rsp), %rax
.L387:
	cmpq	48(%rsp), %rax
	jne	.L71
.L72:
	movl	$6, 8(%rsp)
	jmp	.L14
.L439:
	movslq	48(%rsp), %rax
	movq	%rbp, %r14
	jmp	.L152
.L92:
	cmpl	$0, 8(%rsp)
	jne	.L14
.L389:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r14
	jmp	.L22
.L448:
	cmpl	$711, %ecx
	movl	$-49, %r8d
	leaq	.LC1(%rip), %rsi
	je	.L169
	jb	.L164
	leal	-728(%rcx), %esi
	cmpl	$3, %esi
	ja	.L164
.L167:
	leaq	map.9087(%rip), %rcx
	movb	$32, 127(%rsp)
	movzbl	(%rcx,%rsi), %r8d
	leaq	126(%rsp), %rsi
	movb	%r8b, 126(%rsp)
	jmp	.L169
.L449:
	movq	80(%rsp), %rax
	addq	$1, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L157
.L436:
	cmpq	%r10, 8(%rsp)
	je	.L151
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L447:
	movq	80(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L161
.L178:
	leal	28(%rcx), %r8d
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rsi
	movb	%r8b, 126(%rsp)
	jmp	.L169
.L444:
	leaq	.LC5(%rip), %rsi
	jmp	.L170
.L176:
	leaq	.LC3(%rip), %rsi
	jmp	.L170
.L175:
	leaq	.LC4(%rip), %rsi
	jmp	.L170
.L174:
	leal	-127(%rcx), %r8d
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rsi
	movb	%r8b, 126(%rsp)
	jmp	.L169
.L171:
	leaq	.LC7(%rip), %rsi
	jmp	.L170
.L248:
	leaq	.LC8(%rip), %rsi
	jmp	.L170
.L165:
	leaq	.LC6(%rip), %rsi
	jmp	.L170
.L450:
	cmpq	$0, 80(%rsp)
	je	.L244
	testl	%ebx, %ebx
	je	.L244
	movq	80(%rsp), %rax
	movq	%rsi, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L155
.L452:
	cmpl	$733, %eax
	movl	$5, %edx
	je	.L57
	jbe	.L456
	cmpl	$8216, %eax
	je	.L216
	cmpl	$8217, %eax
	je	.L61
	cmpl	$8212, %eax
	jne	.L54
	leaq	.LC9(%rip), %rdx
.L60:
	movzbl	(%rdx), %eax
	leaq	1(%r10), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r10)
	jmp	.L85
.L433:
	testb	%dil, %dil
	je	.L211
.L396:
	cmpq	$0, 80(%rsp)
	movl	$6, 8(%rsp)
	je	.L14
	andl	$2, %r11d
	je	.L14
	movq	80(%rsp), %rax
	movq	%r10, %rcx
	addq	$1, (%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L432:
	leaq	152(%rsp), %rsi
	addq	$1, %rdx
	leaq	(%rsi,%rdx), %r8
	movq	%r8, 8(%rsp)
	leaq	1(%rsi), %r8
	cmpq	%r8, 8(%rsp)
	jbe	.L457
	movzbl	153(%rsp), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L458
	leaq	(%r9,%r9,2), %rdx
	cltq
	salq	$5, %rdx
	addq	%rdx, %rax
	leaq	to_ucs4_comb(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	leaq	2(%rsi), %rdx
	testl	%eax, %eax
	jne	.L36
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	1(%rsi), %rdx
	jmp	.L36
.L238:
	movl	$7, %eax
	jmp	.L152
.L55:
	leaq	.LC6(%rip), %rdx
	jmp	.L60
.L64:
	leal	-127(%rax), %ecx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdx
	movb	%cl, 126(%rsp)
	jmp	.L59
.L63:
	cmpl	$8592, %eax
	jb	.L54
	cmpl	$8595, %eax
	jbe	.L68
	cmpl	$9834, %eax
	leaq	.LC2(%rip), %rdx
	je	.L60
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L435:
	cmpq	%r10, 8(%rsp)
	jne	.L149
.L148:
	leaq	__PRETTY_FUNCTION__.9209(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rdi, 144(%rsp)
	movq	%rdi, %r14
	jmp	.L161
.L446:
	movq	%r10, 88(%rsp)
	movl	%r11d, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movl	48(%rsp), %r11d
	movq	88(%rsp), %r10
	je	.L459
	cmpl	$5, %eax
	je	.L460
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L451:
	cmpq	$0, 80(%rsp)
	je	.L244
	testl	%ebx, %ebx
	je	.L244
	movq	80(%rsp), %rax
	addq	$2, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L68:
	leal	28(%rax), %ecx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdx
	movb	%cl, 126(%rsp)
	jmp	.L59
.L457:
	leaq	2(%rsi), %rcx
	cmpq	%rcx, 8(%rsp)
	je	.L461
	movslq	%r15d, %r15
	movq	%rdx, %rcx
	andl	$-8, %eax
	subq	%r15, %rcx
	addq	%rcx, %r14
	movq	16(%rsp), %rcx
	movq	%r14, (%rcx)
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jle	.L462
	cmpq	$4, %rdx
	ja	.L463
	orl	%edx, %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L49
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L49
.L464:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L464
	jmp	.L49
.L422:
	movq	%r10, 104(%rsp)
	movl	%r11d, 100(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r14
	popq	%rdx
	movl	100(%rsp), %r11d
	movq	104(%rsp), %r10
	movq	144(%rsp), %rax
	je	.L465
	cmpl	$5, 8(%rsp)
	movq	%rax, %rdx
	je	.L466
.L75:
	cmpq	48(%rsp), %rdx
	movq	%rdx, %rax
	jne	.L71
	cmpl	$7, 8(%rsp)
	jne	.L92
	addq	$4, %rdx
	cmpq	%rdx, 88(%rsp)
	je	.L467
	movl	(%rbx), %eax
	movq	%r15, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	16(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r15
	jle	.L468
	cmpq	$4, %r15
	ja	.L469
	orl	%r15d, %eax
	testq	%r15, %r15
	movl	%eax, (%rbx)
	je	.L49
	movq	48(%rsp), %rcx
	xorl	%eax, %eax
.L96:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L96
	jmp	.L49
.L61:
	leaq	.LC7(%rip), %rdx
	jmp	.L60
.L216:
	leaq	.LC8(%rip), %rdx
	jmp	.L60
.L453:
	leaq	.LC5(%rip), %rdx
	jmp	.L60
.L66:
	leaq	.LC3(%rip), %rdx
	jmp	.L60
.L65:
	leaq	.LC4(%rip), %rdx
	jmp	.L60
.L456:
	cmpl	$711, %eax
	movl	$-49, %ecx
	leaq	.LC1(%rip), %rdx
	je	.L59
	jb	.L54
	leal	-728(%rax), %edx
	cmpl	$3, %edx
	ja	.L54
.L57:
	leaq	map.9134(%rip), %rax
	movb	$32, 127(%rsp)
	movzbl	(%rax,%rdx), %ecx
	leaq	126(%rsp), %rdx
	movb	%cl, 126(%rsp)
	jmp	.L59
.L465:
	andl	$2, %r11d
	je	.L387
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	cmpq	48(%rsp), %rax
	je	.L14
	jmp	.L71
.L466:
	cmpq	48(%rsp), %rax
	jne	.L71
	jmp	.L87
.L469:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L468:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L467:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L418:
	leaq	__PRETTY_FUNCTION__.9209(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L45:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L460:
	movq	136(%rsp), %rdx
	cmpq	152(%rsp), %rdx
	movq	144(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L186
	jmp	.L149
.L459:
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L188
.L197:
	leaq	__PRETTY_FUNCTION__.9209(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L42:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L455:
	movq	80(%rsp), %rax
	addq	$1, (%rax)
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L71
	jmp	.L72
.L434:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L441:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L442:
	leaq	__PRETTY_FUNCTION__.9209(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L458:
	cmpq	$0, 80(%rsp)
	movl	$6, 8(%rsp)
	je	.L14
	andb	$2, %r11b
	je	.L14
	movq	80(%rsp), %rax
	movq	%r10, %rcx
	movq	%r8, %rdx
	addq	$1, (%rax)
	jmp	.L35
.L454:
	movq	%r10, 104(%rsp)
	movl	%r11d, 100(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdx
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rcx
	popq	%rsi
	movl	100(%rsp), %r11d
	je	.L73
	cmpl	$5, %eax
	je	.L74
	movq	144(%rsp), %rdx
	movq	104(%rsp), %r10
	jmp	.L75
.L25:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L462:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L461:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L463:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L74:
	movq	144(%rsp), %rax
	cmpq	48(%rsp), %rax
	jne	.L71
	jmp	.L87
	.size	gconv, .-gconv
	.section	.rodata
	.type	map.9087, @object
	.size	map.9087, 6
map.9087:
	.string	"\306\307\312\316"
	.ascii	"\315"
	.set	map.9134,map.9087
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9117, @object
	.size	__PRETTY_FUNCTION__.9117, 20
__PRETTY_FUNCTION__.9117:
	.string	"to_iso6937_2_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9014, @object
	.size	__PRETTY_FUNCTION__.9014, 22
__PRETTY_FUNCTION__.9014:
	.string	"from_iso6937_2_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9209, @object
	.size	__PRETTY_FUNCTION__.9209, 6
__PRETTY_FUNCTION__.9209:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 766
from_ucs4:
	.string	""
	.string	""
	.string	"\001"
	.string	"\002"
	.string	"\003"
	.string	"\004"
	.string	"\005"
	.string	"\006"
	.string	"\007"
	.string	"\b"
	.string	"\t"
	.string	"\n"
	.string	"\013"
	.string	"\f"
	.string	"\r"
	.string	"\016"
	.string	"\017"
	.string	"\020"
	.string	"\021"
	.string	"\022"
	.string	"\023"
	.string	"\024"
	.string	"\025"
	.string	"\026"
	.string	"\027"
	.string	"\030"
	.string	"\031"
	.string	"\032"
	.string	"\033"
	.string	"\034"
	.string	"\035"
	.string	"\036"
	.string	"\037"
	.string	" "
	.string	"!"
	.string	"\""
	.string	"\246"
	.string	"\244"
	.string	"%"
	.string	"&"
	.string	"'"
	.string	"("
	.string	")"
	.string	"*"
	.string	"+"
	.string	","
	.string	"-"
	.string	"."
	.string	"/"
	.string	"0"
	.string	"1"
	.string	"2"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"6"
	.string	"7"
	.string	"8"
	.string	"9"
	.string	":"
	.string	";"
	.string	"<"
	.string	"="
	.string	">"
	.string	"?"
	.string	"@"
	.string	"A"
	.string	"B"
	.string	"C"
	.string	"D"
	.string	"E"
	.string	"F"
	.string	"G"
	.string	"H"
	.string	"I"
	.string	"J"
	.string	"K"
	.string	"L"
	.string	"M"
	.string	"N"
	.string	"O"
	.string	"P"
	.string	"Q"
	.string	"R"
	.string	"S"
	.string	"T"
	.string	"U"
	.string	"V"
	.string	"W"
	.string	"X"
	.string	"Y"
	.string	"Z"
	.string	"["
	.string	"\\"
	.string	"]"
	.string	"^"
	.string	"_"
	.string	"`"
	.string	"a"
	.string	"b"
	.string	"c"
	.string	"d"
	.string	"e"
	.string	"f"
	.string	"g"
	.string	"h"
	.string	"i"
	.string	"j"
	.string	"k"
	.string	"l"
	.string	"m"
	.string	"n"
	.string	"o"
	.string	"p"
	.string	"q"
	.string	"r"
	.string	"s"
	.string	"t"
	.string	"u"
	.string	"v"
	.string	"w"
	.string	"x"
	.string	"y"
	.string	"z"
	.string	"{"
	.string	"|"
	.string	"}"
	.string	"~"
	.string	"\177"
	.string	"\200"
	.string	"\201"
	.string	"\202"
	.string	"\203"
	.string	"\204"
	.string	"\205"
	.string	"\206"
	.string	"\207"
	.string	"\210"
	.string	"\211"
	.string	"\212"
	.string	"\213"
	.string	"\214"
	.string	"\215"
	.string	"\216"
	.string	"\217"
	.string	"\220"
	.string	"\221"
	.string	"\222"
	.string	"\223"
	.string	"\224"
	.string	"\225"
	.string	"\226"
	.string	"\227"
	.string	"\230"
	.string	"\231"
	.string	"\232"
	.string	"\233"
	.string	"\234"
	.string	"\235"
	.string	"\236"
	.string	"\237"
	.string	""
	.string	""
	.string	"\241"
	.string	"\242"
	.string	"\243"
	.string	"\250"
	.string	"\245"
	.string	""
	.string	""
	.string	"\247"
	.ascii	"\310 "
	.string	"\323"
	.string	"\343"
	.string	"\253"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\322"
	.ascii	"\305 "
	.string	"\260"
	.string	"\261"
	.string	"\262"
	.string	"\263"
	.ascii	"\302 "
	.string	"\265"
	.string	"\266"
	.string	"\267"
	.ascii	"\313 "
	.string	"\321"
	.string	"\353"
	.string	"\273"
	.string	"\274"
	.string	"\275"
	.string	"\276"
	.string	"\277"
	.ascii	"\301A"
	.ascii	"\302A"
	.ascii	"\303A"
	.ascii	"\304A"
	.ascii	"\310A"
	.ascii	"\312A"
	.string	"\341"
	.ascii	"\313C"
	.ascii	"\301E"
	.ascii	"\302E"
	.ascii	"\303E"
	.ascii	"\310E"
	.ascii	"\301I"
	.ascii	"\302I"
	.ascii	"\303I"
	.ascii	"\310I"
	.string	"\342"
	.ascii	"\304N"
	.ascii	"\301O"
	.ascii	"\302O"
	.ascii	"\303O"
	.ascii	"\304O"
	.ascii	"\310O"
	.string	"\264"
	.string	"\351"
	.ascii	"\301U"
	.ascii	"\302U"
	.ascii	"\303U"
	.ascii	"\310U"
	.ascii	"\302Y"
	.string	"\354"
	.string	"\373"
	.ascii	"\301a"
	.ascii	"\302a"
	.ascii	"\303a"
	.ascii	"\304a"
	.ascii	"\310a"
	.ascii	"\312a"
	.string	"\361"
	.ascii	"\313c"
	.ascii	"\301e"
	.ascii	"\302e"
	.ascii	"\303e"
	.ascii	"\310e"
	.ascii	"\301i"
	.ascii	"\302i"
	.ascii	"\303i"
	.ascii	"\310i"
	.string	"\363"
	.ascii	"\304n"
	.ascii	"\301o"
	.ascii	"\302o"
	.ascii	"\303o"
	.ascii	"\304o"
	.ascii	"\310o"
	.string	"\270"
	.string	"\371"
	.ascii	"\301u"
	.ascii	"\302u"
	.ascii	"\303u"
	.ascii	"\310u"
	.ascii	"\302y"
	.string	"\374"
	.ascii	"\310y"
	.ascii	"\305A"
	.ascii	"\305a"
	.ascii	"\306A"
	.ascii	"\306a"
	.ascii	"\316A"
	.ascii	"\316a"
	.ascii	"\302C"
	.ascii	"\302c"
	.ascii	"\303C"
	.ascii	"\303c"
	.ascii	"\307C"
	.ascii	"\307c"
	.ascii	"\317C"
	.ascii	"\317c"
	.ascii	"\317D"
	.ascii	"\317d"
	.string	""
	.string	""
	.string	"\362"
	.ascii	"\305E"
	.ascii	"\305e"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\307E"
	.ascii	"\307e"
	.ascii	"\316E"
	.ascii	"\316e"
	.ascii	"\317E"
	.ascii	"\317e"
	.ascii	"\303G"
	.ascii	"\303g"
	.ascii	"\306G"
	.ascii	"\306g"
	.ascii	"\307G"
	.ascii	"\307g"
	.ascii	"\313G"
	.ascii	"\313g"
	.ascii	"\303H"
	.ascii	"\303h"
	.string	"\344"
	.string	"\364"
	.ascii	"\304I"
	.ascii	"\304i"
	.ascii	"\305I"
	.ascii	"\305i"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\316I"
	.ascii	"\316i"
	.ascii	"\307I"
	.string	"\365"
	.string	"\346"
	.string	"\366"
	.ascii	"\303J"
	.ascii	"\303j"
	.ascii	"\313K"
	.ascii	"\313k"
	.string	"\360"
	.ascii	"\302L"
	.ascii	"\302l"
	.ascii	"\313L"
	.ascii	"\313l"
	.ascii	"\317L"
	.ascii	"\317l"
	.string	"\347"
	.string	"\367"
	.string	"\350"
	.string	"\370"
	.ascii	"\302N"
	.ascii	"\302n"
	.ascii	"\313N"
	.ascii	"\313n"
	.ascii	"\317N"
	.ascii	"\317n"
	.string	"\357"
	.string	"\356"
	.string	"\376"
	.ascii	"\305O"
	.ascii	"\305o"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\315O"
	.ascii	"\315o"
	.string	"\352"
	.string	"\372"
	.ascii	"\302R"
	.ascii	"\302r"
	.ascii	"\313R"
	.ascii	"\313r"
	.ascii	"\317R"
	.ascii	"\317r"
	.ascii	"\302S"
	.ascii	"\302s"
	.ascii	"\303S"
	.ascii	"\303s"
	.ascii	"\313S"
	.ascii	"\313s"
	.ascii	"\317S"
	.ascii	"\317s"
	.ascii	"\313T"
	.ascii	"\313t"
	.ascii	"\317T"
	.ascii	"\317t"
	.string	"\355"
	.string	"\375"
	.ascii	"\304U"
	.ascii	"\304u"
	.ascii	"\305U"
	.ascii	"\305u"
	.ascii	"\306U"
	.ascii	"\306u"
	.ascii	"\312U"
	.ascii	"\312u"
	.ascii	"\315U"
	.ascii	"\315u"
	.ascii	"\316U"
	.ascii	"\316u"
	.ascii	"\303W"
	.ascii	"\303w"
	.ascii	"\303Y"
	.ascii	"\303y"
	.ascii	"\310Y"
	.ascii	"\302Z"
	.ascii	"\302z"
	.ascii	"\307Z"
	.ascii	"\307z"
	.ascii	"\317Z"
	.ascii	"\317z"
	.align 32
	.type	to_ucs4_comb, @object
	.size	to_ucs4_comb, 5760
to_ucs4_comb:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	192
	.long	0
	.long	0
	.long	0
	.long	200
	.long	0
	.long	0
	.long	0
	.long	204
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	210
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	217
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	224
	.long	0
	.long	0
	.long	0
	.long	232
	.long	0
	.long	0
	.long	0
	.long	236
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	242
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	249
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	180
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	193
	.long	0
	.long	262
	.long	0
	.long	201
	.long	0
	.long	0
	.long	0
	.long	205
	.long	0
	.long	0
	.long	313
	.long	0
	.long	323
	.long	211
	.long	0
	.long	0
	.long	340
	.long	346
	.long	0
	.long	218
	.long	0
	.long	0
	.long	0
	.long	221
	.long	377
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	225
	.long	0
	.long	263
	.long	0
	.long	233
	.long	0
	.long	0
	.long	0
	.long	237
	.long	0
	.long	0
	.long	314
	.long	0
	.long	324
	.long	243
	.long	0
	.long	0
	.long	341
	.long	347
	.long	0
	.long	250
	.long	0
	.long	0
	.long	0
	.long	253
	.long	378
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	194
	.long	0
	.long	264
	.long	0
	.long	202
	.long	0
	.long	284
	.long	292
	.long	206
	.long	308
	.long	0
	.long	0
	.long	0
	.long	0
	.long	212
	.long	0
	.long	0
	.long	0
	.long	348
	.long	0
	.long	219
	.long	0
	.long	372
	.long	0
	.long	374
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	226
	.long	0
	.long	265
	.long	0
	.long	234
	.long	0
	.long	285
	.long	293
	.long	238
	.long	309
	.long	0
	.long	0
	.long	0
	.long	0
	.long	244
	.long	0
	.long	0
	.long	0
	.long	349
	.long	0
	.long	251
	.long	0
	.long	373
	.long	0
	.long	375
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	126
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	195
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	296
	.long	0
	.long	0
	.long	0
	.long	0
	.long	209
	.long	213
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	360
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	227
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	297
	.long	0
	.long	0
	.long	0
	.long	0
	.long	241
	.long	245
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	361
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	175
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	256
	.long	0
	.long	0
	.long	0
	.long	274
	.long	0
	.long	0
	.long	0
	.long	298
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	332
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	362
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	257
	.long	0
	.long	0
	.long	0
	.long	275
	.long	0
	.long	0
	.long	0
	.long	299
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	333
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	363
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	728
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	258
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	286
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	364
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	259
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	287
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	365
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	729
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	266
	.long	0
	.long	278
	.long	0
	.long	288
	.long	0
	.long	304
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	379
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	267
	.long	0
	.long	279
	.long	0
	.long	289
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	380
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	168
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	196
	.long	0
	.long	0
	.long	0
	.long	203
	.long	0
	.long	0
	.long	0
	.long	207
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	214
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	220
	.long	0
	.long	0
	.long	0
	.long	376
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	228
	.long	0
	.long	0
	.long	0
	.long	235
	.long	0
	.long	0
	.long	0
	.long	239
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	246
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	252
	.long	0
	.long	0
	.long	0
	.long	255
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	730
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	197
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	366
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	229
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	367
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	184
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	199
	.long	0
	.long	0
	.long	0
	.long	290
	.long	0
	.long	0
	.long	0
	.long	310
	.long	315
	.long	0
	.long	325
	.long	0
	.long	0
	.long	0
	.long	342
	.long	350
	.long	354
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	231
	.long	0
	.long	0
	.long	0
	.long	291
	.long	0
	.long	0
	.long	0
	.long	311
	.long	316
	.long	0
	.long	326
	.long	0
	.long	0
	.long	0
	.long	343
	.long	351
	.long	355
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	733
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	336
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	368
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	337
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	369
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	731
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	260
	.long	0
	.long	0
	.long	0
	.long	280
	.long	0
	.long	0
	.long	0
	.long	302
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	370
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	261
	.long	0
	.long	0
	.long	0
	.long	281
	.long	0
	.long	0
	.long	0
	.long	303
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	371
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	711
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	268
	.long	270
	.long	282
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	317
	.long	0
	.long	327
	.long	0
	.long	0
	.long	0
	.long	344
	.long	352
	.long	356
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	381
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	269
	.long	271
	.long	283
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	318
	.long	0
	.long	328
	.long	0
	.long	0
	.long	0
	.long	345
	.long	353
	.long	357
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	382
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	164
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	0
	.long	161
	.long	162
	.long	163
	.long	36
	.long	165
	.long	35
	.long	167
	.long	164
	.long	8216
	.long	8220
	.long	171
	.long	8592
	.long	8593
	.long	8594
	.long	8595
	.long	176
	.long	177
	.long	178
	.long	179
	.long	215
	.long	181
	.long	182
	.long	183
	.long	247
	.long	8217
	.long	8221
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8212
	.long	185
	.long	174
	.long	169
	.long	8482
	.long	9834
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8539
	.long	8540
	.long	8541
	.long	8542
	.long	8486
	.long	198
	.long	208
	.long	170
	.long	294
	.long	0
	.long	306
	.long	319
	.long	321
	.long	216
	.long	338
	.long	186
	.long	222
	.long	358
	.long	330
	.long	329
	.long	312
	.long	230
	.long	273
	.long	240
	.long	295
	.long	305
	.long	307
	.long	320
	.long	322
	.long	248
	.long	339
	.long	223
	.long	254
	.long	359
	.long	331
	.long	0
