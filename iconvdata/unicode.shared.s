	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UNICODE//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	pushq	%rbx
	movq	24(%rdi), %rsi
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	movl	$10, %ecx
	repz cmpsb
	movl	$4, %edi
	seta	%dl
	setb	%al
	cmpb	%al, %dl
	jne	.L2
	call	malloc@PLT
	testq	%rax, %rax
	je	.L6
	movl	$2, (%rax)
	movq	%rax, 96(%rbx)
	movabsq	$8589934594, %rax
	movq	%rax, 72(%rbx)
	movabsq	$17179869188, %rax
	movl	$0, 88(%rbx)
	movq	%rax, 80(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	call	malloc@PLT
	testq	%rax, %rax
	je	.L6
	movl	$1, (%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869188, %rax
	movq	%rax, 72(%rbx)
	movabsq	$8589934594, %rax
	movl	$0, 88(%rbx)
	movq	%rax, 80(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$3, %eax
	popq	%rbx
	ret
	.size	gconv_init, .-gconv_init
	.p2align 4,,15
	.globl	gconv_end
	.type	gconv_end, @function
gconv_end:
	movq	96(%rdi), %rdi
	jmp	free@PLT
	.size	gconv_end, .-gconv_end
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	addq	$48, %rsi
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%r12), %r14d
	movq	%rdi, 96(%rsp)
	addq	$104, %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 24(%rsp)
	testb	$1, %r14b
	movl	224(%rsp), %ebx
	movq	%rdi, 64(%rsp)
	movq	%rsi, 72(%rsp)
	movq	$0, 56(%rsp)
	jne	.L14
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 56(%rsp)
	je	.L14
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 56(%rsp)
.L14:
	testl	%ebx, %ebx
	jne	.L246
	movq	8(%rsp), %rax
	leaq	128(%rsp), %rdx
	movq	(%rax), %rcx
	movq	32(%rsp), %rax
	testq	%rax, %rax
	cmove	%r12, %rax
	cmpq	$0, 24(%rsp)
	movq	(%rax), %r13
	movq	8(%r12), %rax
	movq	$0, 128(%rsp)
	movq	%rax, 16(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movq	%rax, 88(%rsp)
	movq	96(%rsp), %rax
	movq	96(%rax), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	movl	%eax, 104(%rsp)
	je	.L247
	movl	24(%r12), %r10d
	testl	%r10d, %r10d
	je	.L29
.L241:
	movl	%r14d, %eax
	andl	$4, %eax
	movl	%eax, 108(%rsp)
.L23:
	movl	232(%rsp), %edi
	testl	%edi, %edi
	jne	.L31
.L243:
	movq	8(%rsp), %rax
	xorl	%r11d, %r11d
	movq	(%rax), %r15
.L32:
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 120(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 80(%rsp)
	.p2align 4,,10
	.p2align 3
.L72:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L78
	addq	(%rax), %r11
.L78:
	cmpl	$2, 104(%rsp)
	je	.L248
	movq	%r15, 144(%rsp)
	movq	%r13, 152(%rsp)
	movq	%r13, %rbx
	movq	%r15, %rax
	movl	$4, %r10d
	andl	$2, %r14d
.L87:
	cmpq	%rax, %rbp
	je	.L88
.L95:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L148
	leaq	2(%rbx), %rsi
	cmpq	%rsi, 16(%rsp)
	jb	.L149
	movl	(%rax), %ecx
	cmpl	$65535, %ecx
	ja	.L249
	leal	-55296(%rcx), %edi
	cmpl	$2047, %edi
	jbe	.L250
	movq	%rdx, %rax
	movw	%cx, (%rbx)
	movq	%rsi, 152(%rsp)
	cmpq	%rax, %rbp
	movq	%rdx, 144(%rsp)
	movq	%rsi, %rbx
	jne	.L95
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L251
.L96:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L252
	cmpq	%rbx, %r13
	movq	%r11, 48(%rsp)
	jnb	.L154
	movq	56(%rsp), %r14
	movq	(%r12), %rax
	movl	%r10d, 40(%rsp)
	movq	%r14, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	40(%rsp), %r9
	movq	96(%rsp), %rdx
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r14
	movl	%eax, %r14d
	popq	%r11
	cmpl	$4, %r14d
	popq	%rax
	movl	40(%rsp), %r10d
	je	.L100
	movq	136(%rsp), %r10
	movq	48(%rsp), %r11
	cmpq	%rbx, %r10
	jne	.L253
.L99:
	testl	%r14d, %r14d
	jne	.L169
.L129:
	movq	8(%rsp), %rax
	movq	128(%rsp), %r11
	movl	16(%r12), %r14d
	movq	(%r12), %r13
	movq	(%rax), %r15
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L248:
	cmpq	%r15, %rbp
	je	.L141
	leaq	2(%r15), %rdx
	cmpq	%rbp, %rdx
	ja	.L142
	movq	16(%rsp), %r8
	leaq	4(%r13), %rcx
	movq	%r13, %rbx
	cmpq	%rcx, %r8
	jb	.L143
	movl	108(%rsp), %esi
	movl	$4, %r10d
	andl	$2, %r14d
.L81:
	movzwl	-2(%rdx), %eax
	leaq	-2(%rdx), %rdi
	movl	%eax, %r9d
	rolw	$8, %r9w
	testl	%esi, %esi
	cmovne	%r9d, %eax
	leal	10240(%rax), %r9d
	cmpw	$2047, %r9w
	jbe	.L254
	movzwl	%ax, %eax
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L84:
	cmpq	%rbp, %rdx
	je	.L80
	leaq	2(%rdx), %rax
	cmpq	%rax, %rbp
	jb	.L144
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r8
	jb	.L145
	movq	%rax, %rdx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L254:
	cmpq	$0, 88(%rsp)
	je	.L147
	testl	%r14d, %r14d
	jne	.L255
.L147:
	movq	%rdi, %rdx
	movl	$6, %r10d
.L80:
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
.L265:
	cmpq	$0, 32(%rsp)
	je	.L96
.L251:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L13:
	addq	$168, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	cmpl	$5, %r10d
	movl	%r10d, %r14d
	jne	.L99
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$7, %r10d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$5, %r10d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L249:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L256
	cmpq	$0, 88(%rsp)
	je	.L153
	testb	$8, 16(%r12)
	jne	.L257
.L92:
	testl	%r14d, %r14d
	jne	.L258
.L153:
	movl	$6, %r10d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L250:
	cmpq	$0, 88(%rsp)
	je	.L153
	testl	%r14d, %r14d
	je	.L153
	movq	88(%rsp), %rax
	movq	%rdx, 144(%rsp)
	movl	$6, %r10d
	addq	$1, (%rax)
	movq	%rdx, %rax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L31:
	movq	32(%r12), %r11
	movl	(%r11), %ebx
	andl	$7, %ebx
	je	.L243
	cmpq	$0, 32(%rsp)
	jne	.L259
	cmpl	$2, 104(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	je	.L260
	cmpl	$4, %ebx
	movq	%rdx, 144(%rsp)
	movq	%r13, 152(%rsp)
	ja	.L49
	leaq	136(%rsp), %r15
	movslq	%ebx, %rbx
	xorl	%eax, %eax
.L50:
	movzbl	4(%r11,%rax), %ecx
	movb	%cl, (%r15,%rax)
	addq	$1, %rax
	cmpq	%rbx, %rax
	jne	.L50
	movq	%rdx, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L261
	leaq	2(%r13), %r8
	cmpq	%r8, 16(%rsp)
	jb	.L139
	leaq	1(%rdx), %rax
	leaq	135(%rsp), %rdi
.L58:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rbx
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%cl, (%rdi,%rbx)
	ja	.L171
	cmpq	%rsi, %rbp
	ja	.L58
.L171:
	movl	136(%rsp), %eax
	movq	%r15, 144(%rsp)
	cmpl	$65535, %eax
	ja	.L262
	leal	-55296(%rax), %edx
	cmpl	$2047, %edx
	jbe	.L263
	movw	%ax, 0(%r13)
	movq	%r8, 152(%rsp)
.L242:
	leaq	4(%r15), %rax
	movq	%rax, 144(%rsp)
.L62:
	subq	%r15, %rax
	movq	%rax, %r15
	movl	(%r11), %eax
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %r15
	jle	.L264
	movq	8(%rsp), %rdi
	subq	%rdx, %r15
	andl	$-8, %eax
	movq	152(%rsp), %r13
	movl	16(%r12), %r14d
	addq	(%rdi), %r15
	movq	%r15, (%rdi)
	movl	%eax, (%r11)
	movq	128(%rsp), %r11
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	movl	20(%r12), %r9d
	testl	%r9d, %r9d
	jne	.L241
	leaq	2(%r13), %rax
	cmpq	16(%rsp), %rax
	ja	.L139
	movl	%r14d, %edi
	movl	$-257, %r8d
	andl	$4, %edi
	movw	%r8w, 0(%r13)
	movq	%rax, %r13
	movl	%edi, 108(%rsp)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%rsp), %rax
	movl	$7, %r10d
	movq	%rdx, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L252:
	movq	24(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L98:
	movl	232(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L13
	cmpl	$7, %r10d
	jne	.L13
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L131
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L133
.L132:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L132
.L133:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L247:
	movl	20(%r12), %r11d
	testl	%r11d, %r11d
	jne	.L241
	leaq	2(%rcx), %rax
	cmpq	%rbp, %rax
	jbe	.L24
	xorl	%r10d, %r10d
	cmpq	%rbp, %rcx
	setne	%r10b
	leal	4(%r10,%r10,2), %r10d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L145:
	movq	8(%rsp), %rax
	movl	$5, %r10d
	movq	%rdx, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L154:
	movl	%r10d, %r14d
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r11, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	96(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	112(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	cmpl	$6, %r10d
	popq	%rax
	popq	%rdx
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movq	40(%rsp), %r11
	je	.L92
	cmpl	$5, %r10d
	jne	.L87
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L258:
	movq	88(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	jmp	.L87
.L260:
	cmpl	$4, %ebx
	ja	.L36
	movzbl	4(%r11), %eax
	movslq	%ebx, %rbx
	cmpq	$1, %rbx
	movb	%al, 152(%rsp)
	je	.L38
	movzbl	5(%r11), %eax
	movb	%al, 153(%rsp)
.L38:
	movq	%rdx, %rax
	subq	%rbx, %rax
	addq	$2, %rax
	cmpq	%rax, %rbp
	jb	.L266
	leaq	4(%r13), %rcx
	cmpq	%rcx, 16(%rsp)
	jb	.L139
	movzbl	(%rdx), %eax
	movl	108(%rsp), %edi
	movb	%al, 152(%rsp,%rbx)
	movzwl	152(%rsp), %eax
	movl	%eax, %esi
	rolw	$8, %si
	testl	%edi, %edi
	cmovne	%esi, %eax
	leal	10240(%rax), %esi
	cmpw	$2047, %si
	jbe	.L267
	movzwl	%ax, %eax
	movl	%eax, 0(%r13)
	movq	%rcx, %r13
.L47:
	movl	(%r11), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$1, %ecx
	movslq	%ecx, %rsi
	jg	.L268
	subq	%rsi, %rdx
	movq	8(%rsp), %rdi
	andl	$-8, %eax
	leaq	2(%rdx), %r15
	movl	16(%r12), %r14d
	movq	%r15, (%rdi)
	movl	%eax, (%r11)
	movq	128(%rsp), %r11
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$5, %r10d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L253:
	movq	24(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L102
	movq	(%rsi), %rax
.L102:
	addq	128(%rsp), %rax
	cmpq	%r11, %rax
	movq	8(%rsp), %rax
	je	.L269
	cmpl	$2, 104(%rsp)
	movq	%r15, (%rax)
	movl	16(%r12), %ebx
	je	.L270
	movq	%r15, 144(%rsp)
	movq	%r13, 152(%rsp)
	movq	%r13, %rdx
	movl	$4, %eax
	andl	$2, %ebx
.L118:
	cmpq	%r15, %rbp
	je	.L271
.L126:
	leaq	4(%r15), %rcx
	cmpq	%rcx, %rbp
	jb	.L161
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r10
	jb	.L164
	movl	(%r15), %esi
	cmpl	$65535, %esi
	ja	.L272
	leal	-55296(%rsi), %r8d
	cmpl	$2047, %r8d
	jbe	.L273
	movq	%rcx, %r15
	movw	%si, (%rdx)
	movq	%rdi, 152(%rsp)
	cmpq	%r15, %rbp
	movq	%rcx, 144(%rsp)
	movq	%rdi, %rdx
	jne	.L126
.L271:
	movslq	%eax, %rcx
	movq	%rbp, %r15
.L119:
	movq	8(%rsp), %rax
	movq	136(%rsp), %r10
	movq	%r15, (%rax)
.L117:
	cmpq	%rdx, %r10
	jne	.L108
	cmpq	$5, %rcx
	jne	.L107
	cmpq	%r13, %r10
	jne	.L99
.L111:
	subl	$1, 20(%r12)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rdx, 144(%rsp)
	movq	%rdx, %rax
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L142:
	movq	8(%rsp), %rax
	movq	%r15, %rdx
	movq	%r13, %rbx
	movl	$7, %r10d
	movq	%rdx, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L255:
	movq	88(%rsp), %rax
	movl	$6, %r10d
	addq	$1, (%rax)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L143:
	movq	8(%rsp), %rax
	movq	%r15, %rdx
	movl	$5, %r10d
	movq	%rdx, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L246:
	cmpq	$0, 32(%rsp)
	jne	.L274
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L13
	movq	56(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	40(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	popq	%rbx
	movl	%eax, %r10d
	popq	%rbp
	jmp	.L13
.L169:
	movl	%r14d, %r10d
	jmp	.L98
.L24:
	movzwl	(%rcx), %edx
	cmpw	$-257, %dx
	je	.L275
	cmpw	$-2, %dx
	jne	.L241
	movq	8(%rsp), %rsi
	orl	$4, %r14d
	movl	$4, 108(%rsp)
	movl	%r14d, 16(%r12)
	movq	%rax, (%rsi)
	jmp	.L23
.L269:
	subq	%r10, %rbx
	cmpl	$2, 104(%rsp)
	movq	(%rax), %rax
	je	.L276
	movq	8(%rsp), %rdi
	addq	%rbx, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rdi)
	jmp	.L99
.L276:
	movq	%rbx, %rdx
	movq	8(%rsp), %rsi
	shrq	$63, %rdx
	addq	%rdx, %rbx
	sarq	%rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L99
.L270:
	cmpq	%r15, %rbp
	je	.L245
	addq	$2, %r15
	cmpq	%r15, %rbp
	jb	.L245
	leaq	4(%r13), %rsi
	andl	$2, %ebx
	movq	%r13, %rdx
	movl	$4, %ecx
	movl	108(%rsp), %r9d
	cmpq	%rsi, %r10
	jb	.L277
.L110:
	movzwl	-2(%r15), %eax
	leaq	-2(%r15), %r8
	movl	%eax, %edi
	rolw	$8, %di
	testl	%r9d, %r9d
	cmovne	%edi, %eax
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L278
	movzwl	%ax, %eax
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
.L115:
	cmpq	%rbp, %r15
	je	.L112
	leaq	2(%r15), %rax
	cmpq	%rax, %rbp
	jb	.L157
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L158
	movq	%rax, %r15
	jmp	.L110
.L141:
	movq	%r13, %rbx
	movq	%rbp, %rdx
	movl	$4, %r10d
	jmp	.L80
.L278:
	cmpq	$0, 88(%rsp)
	je	.L160
	testl	%ebx, %ebx
	jne	.L279
.L160:
	movq	%r8, %r15
	movl	$6, %ecx
.L112:
	movq	8(%rsp), %rax
	movq	%r15, (%rax)
	jmp	.L117
.L275:
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	movl	%r14d, %eax
	andl	$4, %eax
	movl	%eax, 108(%rsp)
	jmp	.L23
.L161:
	movl	$7, %ecx
	jmp	.L119
.L281:
	movq	%r10, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	96(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	112(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r9
	cmpl	$6, %eax
	popq	%r10
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	movq	40(%rsp), %r10
	je	.L123
	cmpl	$5, %eax
	jne	.L118
.L164:
	movl	$5, %ecx
	jmp	.L119
.L272:
	shrl	$7, %esi
	cmpl	$7168, %esi
	je	.L280
	cmpq	$0, 88(%rsp)
	je	.L167
	testb	$8, 16(%r12)
	jne	.L281
.L123:
	testl	%ebx, %ebx
	jne	.L282
.L167:
	movl	$6, %ecx
	jmp	.L119
.L273:
	cmpq	$0, 88(%rsp)
	je	.L167
	testl	%ebx, %ebx
	je	.L167
	movq	88(%rsp), %rax
	movq	%rcx, 144(%rsp)
	movq	%rcx, %r15
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L118
.L261:
	movq	%rbp, %rcx
	movq	8(%rsp), %rax
	subq	%rdx, %rcx
	addq	%rbx, %rcx
	cmpq	$4, %rcx
	movq	%rbp, (%rax)
	ja	.L52
	addq	$1, %rdx
	cmpq	%rbx, %rcx
	jbe	.L54
.L55:
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %eax
	addq	$1, %rdx
	movb	%al, 4(%r11,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rcx
	jne	.L55
.L54:
	movl	$7, %r10d
	jmp	.L13
.L157:
	movl	$7, %ecx
	jmp	.L112
.L158:
	movl	$5, %ecx
	jmp	.L112
.L266:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%rdx, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L40
	subq	%rbx, %rdx
	cmpq	%rbx, %rax
	jbe	.L54
.L41:
	movzbl	(%rdx,%rbx), %ecx
	movb	%cl, 4(%r11,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L41
	jmp	.L54
.L262:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L242
	cmpq	$0, 88(%rsp)
	je	.L63
	testb	$8, %r14b
	jne	.L283
	andl	$2, %r14d
	jne	.L68
.L63:
	movl	$6, %r10d
	jmp	.L13
.L263:
	cmpq	$0, 88(%rsp)
	je	.L63
	andl	$2, %r14d
	je	.L63
	movq	88(%rsp), %rsi
	leaq	4(%r15), %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	jmp	.L62
.L245:
	cmpq	%r13, %r10
	jne	.L108
.L107:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	movq	88(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L118
.L280:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %r15
	jmp	.L118
.L277:
	cmpq	%r13, %r10
	je	.L111
.L108:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L279:
	movq	88(%rsp), %rax
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L115
.L267:
	cmpq	$0, 88(%rsp)
	je	.L63
	andl	$2, %r14d
	je	.L63
	movq	88(%rsp), %rax
	addq	$1, (%rax)
	jmp	.L47
.L284:
	andb	$2, %r14b
	je	.L67
.L68:
	movq	88(%rsp), %rax
	addq	$4, 144(%rsp)
	addq	$1, (%rax)
.L67:
	movq	144(%rsp), %rax
	cmpq	%r15, %rax
	jne	.L62
	jmp	.L63
.L283:
	leaq	(%r15,%rbx), %rax
	movq	%r11, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, 48(%rsp)
	pushq	96(%rsp)
	movq	%rax, %r8
	movq	112(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%rcx
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%rsi
	movq	48(%rsp), %r11
	je	.L284
	movq	144(%rsp), %rax
	cmpq	%r15, %rax
	jne	.L62
	cmpl	$7, %r10d
	jne	.L73
	leaq	4(%r15), %rax
	cmpq	%rax, 40(%rsp)
	je	.L285
	movl	(%r11), %eax
	movq	%rbx, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	8(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L286
	cmpq	$4, %rbx
	ja	.L287
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r11)
	je	.L54
	xorl	%eax, %eax
.L77:
	movzbl	(%r15,%rax), %edx
	movb	%dl, 4(%r11,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L77
	jmp	.L54
.L268:
	leaq	__PRETTY_FUNCTION__.9112(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L287:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L286:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L285:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L73:
	testl	%r10d, %r10d
	jne	.L13
	movq	8(%rsp), %rax
	movq	128(%rsp), %r11
	movl	16(%r12), %r14d
	movq	(%rax), %r15
	jmp	.L32
.L264:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L259:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L131:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L274:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L36:
	leaq	__PRETTY_FUNCTION__.9112(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L40:
	leaq	__PRETTY_FUNCTION__.9112(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L52:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L49:
	leaq	__PRETTY_FUNCTION__.9031(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9031, @object
	.size	__PRETTY_FUNCTION__.9031, 23
__PRETTY_FUNCTION__.9031:
	.string	"to_unicode_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9112, @object
	.size	__PRETTY_FUNCTION__.9112, 25
__PRETTY_FUNCTION__.9112:
	.string	"from_unicode_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9181, @object
	.size	__PRETTY_FUNCTION__.9181, 6
__PRETTY_FUNCTION__.9181:
	.string	"gconv"
