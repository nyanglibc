	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %esi
	movl	(%rax,%rsi,4), %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IBM1097//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$10, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L4
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	32(%rax), %rsi
	movl	$10, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L7
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r15d
	movq	%rdi, 40(%rsp)
	leaq	48(%r12), %rdi
	movq	%rdx, (%rsp)
	movq	%r8, 48(%rsp)
	movq	%r9, 16(%rsp)
	testb	$1, %r15b
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	%rdi, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L9
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 24(%rsp)
	je	.L9
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L9:
	testl	%ebx, %ebx
	jne	.L201
	movq	(%rsp), %rax
	movq	48(%rsp), %rbx
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %r14d
	testq	%rbx, %rbx
	movq	(%rax), %r13
	movq	%rbx, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r11
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, 8(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%r14d, %r14d
	movq	%rax, 80(%rsp)
	movq	40(%rsp), %rax
	setne	103(%rsp)
	movzbl	103(%rsp), %edi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L111
	testb	%dil, %dil
	je	.L111
	movq	32(%r12), %r14
	movl	(%r14), %edx
	andl	$7, %edx
	jne	.L202
.L111:
	movq	$0, 32(%rsp)
.L16:
	leaq	to_ucs4(%rip), %r14
	movq	%r11, %r15
	.p2align 4,,10
	.p2align 3
.L100:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rdi
	addq	%rdi, 32(%rsp)
.L48:
	testq	%rax, %rax
	je	.L203
	leaq	136(%rsp), %rax
	movl	16(%r12), %r10d
	movq	%r15, %rbx
	movq	%r13, 128(%rsp)
	movq	%r13, %rdi
	movl	$4, %r11d
	movq	%rax, 56(%rsp)
	leaq	128(%rsp), %rax
	movq	%r15, 136(%rsp)
	movq	%rax, 88(%rsp)
.L54:
	cmpq	%rdi, %rbp
	je	.L55
.L65:
	leaq	4(%rdi), %rax
	cmpq	%rax, %rbp
	jb	.L117
	cmpq	%rbx, 8(%rsp)
	jbe	.L118
	movl	(%rdi), %r8d
	cmpl	$65534, %r8d
	ja	.L56
	cmpl	$164, %r8d
	movl	$173, %edx
	leaq	from_idx(%rip), %rsi
	ja	.L57
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	movzwl	10(%rcx), %edx
	movq	%rcx, %rsi
.L57:
	cmpl	%edx, %r8d
	leaq	8(%rsi), %rcx
	ja	.L205
	movzwl	(%rcx), %eax
	cmpl	%eax, %r8d
	jb	.L59
	movl	%r8d, %edx
	addl	12(%rsi), %edx
.L58:
	leaq	from_ucs4(%rip), %rax
	movzbl	(%rax,%rdx), %eax
	testb	%al, %al
	jne	.L136
	testl	%r8d, %r8d
	jne	.L59
.L136:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 128(%rsp)
	jne	.L65
	.p2align 4,,10
	.p2align 3
.L55:
	cmpq	$0, 48(%rsp)
	movq	(%rsp), %rax
	movq	%rdi, (%rax)
	jne	.L206
.L66:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L207
	cmpq	%rbx, %r15
	jnb	.L122
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 56(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	32(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rdi
	movl	56(%rsp), %r11d
	je	.L70
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L208
.L69:
	testl	%r10d, %r10d
	jne	.L132
.L99:
	movq	112(%rsp), %rax
	movq	(%rsp), %rbx
	movq	(%r12), %r15
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r13
	movq	96(%rax), %rax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L56:
	shrl	$7, %r8d
	cmpl	$7168, %r8d
	je	.L209
.L59:
	cmpq	$0, 80(%rsp)
	je	.L121
	testb	$8, 16(%r12)
	jne	.L210
.L63:
	testb	$2, %r10b
	jne	.L211
.L121:
	movl	$6, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%r13, %rbp
	je	.L114
	movq	8(%rsp), %rax
	leaq	4(%r15), %rdx
	cmpq	%rax, %rdx
	ja	.L115
	subq	%r15, %rax
	subq	$4, %rax
	shrq	$2, %rax
	leaq	1(%r13,%rax), %rcx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rdx, %rbx
	movzbl	(%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %rbp
	movl	(%r14,%rdx,4), %edx
	movl	%edx, -4(%rbx)
	je	.L212
	cmpq	%rcx, %rax
	leaq	4(%rbx), %rdx
	jne	.L51
	movl	$5, %r11d
.L50:
	cmpq	$0, 48(%rsp)
	movq	(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L66
.L206:
	movq	48(%rsp), %rax
	movl	%r11d, %r9d
	movq	%rbx, (%rax)
.L8:
	addq	$152, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L69
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L204:
	movl	%r8d, %edx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$7, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$5, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L210:
	movl	%r10d, 104(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	72(%rsp), %r9
	movq	104(%rsp), %rcx
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r8
	cmpl	$6, %eax
	movl	%eax, %r11d
	popq	%r9
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rbx
	movl	104(%rsp), %r10d
	je	.L63
	cmpl	$5, %eax
	jne	.L54
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%rbp, %rcx
	movl	$4, %r11d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L211:
	movq	80(%rsp), %rax
	addq	$4, %rdi
	movl	$6, %r11d
	movq	%rdi, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L54
.L207:
	movq	%rbx, (%r12)
	movq	16(%rsp), %rbx
	movl	%r11d, %r9d
	movq	112(%rsp), %rax
	addq	%rax, (%rbx)
.L68:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 103(%rsp)
	je	.L8
	cmpl	$7, %r9d
	jne	.L8
	movq	(%rsp), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L102
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	32(%r12), %rcx
	je	.L104
.L103:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L103
.L104:
	movl	(%rcx), %edx
	movq	(%rsp), %rbx
	andl	$-8, %edx
	movq	%rbp, (%rbx)
	orl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%r11d, %r10d
	jmp	.L69
.L208:
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L72
	movq	(%rdi), %rax
.L72:
	addq	112(%rsp), %rax
	cmpq	32(%rsp), %rax
	movq	(%rsp), %rax
	je	.L213
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L214
	leaq	136(%rsp), %rsi
	leaq	128(%rsp), %rdi
	movl	16(%r12), %ebx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r15, %r8
	movl	$4, %eax
	movq	%rsi, 32(%rsp)
	movq	%rdi, 56(%rsp)
.L85:
	cmpq	%r13, %rbp
	je	.L215
	leaq	4(%r13), %rdx
	cmpq	%rdx, %rbp
	jb	.L125
	cmpq	%r8, %r11
	jbe	.L129
	movl	0(%r13), %edi
	cmpl	$65534, %edi
	ja	.L87
	cmpl	$164, %edi
	movl	$173, %edx
	leaq	from_idx(%rip), %rsi
	ja	.L88
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L217:
	movzwl	10(%rcx), %edx
	movq	%rcx, %rsi
.L88:
	cmpl	%edx, %edi
	leaq	8(%rsi), %rcx
	ja	.L217
	movzwl	(%rcx), %edx
	cmpl	%edx, %edi
	jb	.L90
	movl	%edi, %edx
	addl	12(%rsi), %edx
.L89:
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
	testb	%dl, %dl
	jne	.L137
	testl	%edi, %edi
	jne	.L90
.L137:
	leaq	1(%r8), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%r8)
	movq	128(%rsp), %rdi
	movq	136(%rsp), %r8
	leaq	4(%rdi), %r13
	movq	%r13, 128(%rsp)
	jmp	.L85
.L202:
	testq	%rbx, %rbx
	jne	.L218
	cmpl	$4, %edx
	movq	%r13, 128(%rsp)
	movq	%r11, 136(%rsp)
	ja	.L18
	leaq	120(%rsp), %r10
	movslq	%edx, %rax
	xorl	%ebx, %ebx
.L19:
	movzbl	4(%r14,%rbx), %edx
	movb	%dl, (%r10,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L19
	movq	%r13, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L219
	cmpq	8(%rsp), %r11
	movl	$5, %r9d
	jnb	.L8
	leaq	1(%r13), %rax
	leaq	119(%rsp), %rsi
.L27:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rbx
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%cl, (%rsi,%rbx)
	ja	.L134
	cmpq	%rdx, %rbp
	ja	.L27
.L134:
	movl	120(%rsp), %ecx
	movq	%r10, 128(%rsp)
	cmpl	$65534, %ecx
	ja	.L29
	cmpl	$164, %ecx
	movl	$173, %eax
	leaq	from_idx(%rip), %rsi
	ja	.L30
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movzwl	10(%rdx), %eax
	movq	%rdx, %rsi
.L30:
	cmpl	%eax, %ecx
	leaq	8(%rsi), %rdx
	ja	.L221
	movzwl	(%rdx), %eax
	cmpl	%eax, %ecx
	jb	.L32
	movl	%ecx, %eax
	addl	12(%rsi), %eax
.L31:
	leaq	from_ucs4(%rip), %rdx
	testl	%ecx, %ecx
	movzbl	(%rdx,%rax), %eax
	je	.L135
	testb	%al, %al
	jne	.L135
.L32:
	cmpq	$0, 80(%rsp)
	je	.L113
	testb	$8, %r15b
	jne	.L222
	andl	$2, %r15d
	je	.L113
	movq	%r10, %rax
.L106:
	movq	80(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rbx)
.L40:
	cmpq	%r10, %rax
	jne	.L33
.L113:
	movl	$6, %r9d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%rax, 128(%rsp)
	movq	%rax, %rdi
	jmp	.L54
.L213:
	movq	40(%rsp), %rsi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rsi)
	je	.L223
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L69
.L135:
	leaq	1(%r11), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%r11)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%r10, %rax
	movq	%rax, 128(%rsp)
	je	.L194
.L33:
	movl	(%r14), %edx
	subq	%r10, %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L224
	movq	(%rsp), %rbx
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r11
	addq	(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%rax, %r13
	movq	112(%rsp), %rax
	movl	%edx, (%r14)
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L16
.L115:
	movq	%r13, %rcx
	movq	%r15, %rbx
	movl	$5, %r11d
	jmp	.L50
.L201:
	cmpq	$0, 48(%rsp)
	jne	.L225
	movq	32(%r12), %rax
	xorl	%r9d, %r9d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L8
	movq	24(%rsp), %r14
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r14
	popq	%r15
	movl	%eax, %r9d
	popq	%rax
	jmp	.L8
.L114:
	movq	%rbp, %rcx
	movq	%r15, %rbx
	movl	$4, %r11d
	jmp	.L50
.L132:
	movl	%r10d, %r9d
	jmp	.L68
.L90:
	cmpq	$0, 80(%rsp)
	je	.L130
	testb	$8, 16(%r12)
	jne	.L226
.L94:
	testb	$2, %bl
	jne	.L227
.L130:
	movl	$6, %eax
.L86:
	movq	(%rsp), %rbx
	movq	120(%rsp), %r11
	movq	%r13, (%rbx)
.L84:
	cmpq	%r11, %r8
	jne	.L78
	cmpq	$5, %rax
	jne	.L77
	cmpq	%r8, %r15
	jne	.L69
.L80:
	subl	$1, 20(%r12)
	jmp	.L69
.L226:
	movq	%r11, 104(%rsp)
	movl	%r10d, 88(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %r8
	movl	88(%rsp), %r10d
	movq	104(%rsp), %r11
	je	.L94
	cmpl	$5, %eax
	jne	.L85
.L129:
	movl	$5, %eax
	jmp	.L86
.L214:
	cmpq	%r13, %rbp
	je	.L228
	leaq	4(%r15), %rax
	cmpq	%r11, %rax
	ja	.L229
	movq	%r11, %rdx
	subq	%r15, %rdx
	subq	$4, %rdx
	shrq	$2, %rdx
	leaq	1(%r13,%rdx), %rdx
.L81:
	movq	%rax, %r8
	movzbl	0(%r13), %eax
	addq	$1, %r13
	cmpq	%r13, %rbp
	movl	(%r14,%rax,4), %eax
	movl	%eax, -4(%r8)
	je	.L230
	cmpq	%rdx, %r13
	leaq	4(%r8), %rax
	jne	.L81
	movl	$5, %eax
.L82:
	movq	(%rsp), %rbx
	movq	%rdx, (%rbx)
	jmp	.L84
.L216:
	movl	%edi, %edx
	jmp	.L89
.L125:
	movl	$7, %eax
	jmp	.L86
.L87:
	shrl	$7, %edi
	cmpl	$7168, %edi
	jne	.L90
	movq	%rdx, 128(%rsp)
	movq	%rdx, %r13
	jmp	.L85
.L223:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L69
.L219:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L21
	cmpq	%rax, %rbx
	leaq	1(%r13), %rdx
	jnb	.L23
.L24:
	movq	%rdx, 128(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %rdx
	movb	%cl, 4(%r14,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L24
.L23:
	movl	$7, %r9d
	jmp	.L8
.L230:
	movq	%rbp, %rdx
	movl	$4, %eax
	jmp	.L82
.L227:
	movq	80(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L85
.L220:
	movl	%ecx, %eax
	jmp	.L31
.L29:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	jne	.L32
	leaq	4(%r10), %rax
	movq	%rax, 128(%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L215:
	cltq
	movq	%rbp, %r13
	jmp	.L86
.L43:
	testl	%r9d, %r9d
	jne	.L8
.L194:
	movq	112(%rsp), %rax
	movq	(%rsp), %rbx
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r13
	movq	96(%rax), %rax
	jmp	.L16
.L229:
	cmpq	%r15, %r11
	je	.L80
.L78:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	(%r10,%rbx), %rax
	movq	%r11, 88(%rsp)
	movq	%r10, 56(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rax, 40(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%r11
	movq	56(%rsp), %r10
	movq	88(%rsp), %r11
	movq	128(%rsp), %rax
	je	.L231
	cmpq	%r10, %rax
	jne	.L33
	cmpl	$7, %r9d
	jne	.L43
	leaq	4(%r10), %rax
	cmpq	%rax, 32(%rsp)
	je	.L232
	movl	(%r14), %eax
	movq	(%rsp), %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, (%rdi)
	cmpq	%rdx, %rbx
	jle	.L233
	cmpq	$4, %rbx
	ja	.L234
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r14)
	je	.L23
	xorl	%eax, %eax
.L47:
	movzbl	(%r10,%rax), %edx
	movb	%dl, 4(%r14,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L47
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r11, %r10
	cmpq	%r15, %r10
	jne	.L78
.L77:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L234:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L233:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L232:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L231:
	andb	$2, %r15b
	je	.L40
	jmp	.L106
.L224:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L21:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L102:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L225:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L18:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L218:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9074, @object
	.size	__PRETTY_FUNCTION__.9074, 14
__PRETTY_FUNCTION__.9074:
	.string	"to_gap_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9153, @object
	.size	__PRETTY_FUNCTION__.9153, 6
__PRETTY_FUNCTION__.9153:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 379
from_ucs4:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	55
	.byte	45
	.byte	46
	.byte	47
	.byte	22
	.byte	5
	.byte	37
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	60
	.byte	61
	.byte	50
	.byte	38
	.byte	24
	.byte	25
	.byte	63
	.byte	39
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	64
	.byte	90
	.byte	127
	.byte	123
	.byte	91
	.byte	108
	.byte	80
	.byte	125
	.byte	77
	.byte	93
	.byte	92
	.byte	78
	.byte	107
	.byte	96
	.byte	75
	.byte	97
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	122
	.byte	94
	.byte	76
	.byte	126
	.byte	110
	.byte	111
	.byte	124
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-70
	.byte	-32
	.byte	-69
	.byte	0
	.byte	109
	.byte	121
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-64
	.byte	79
	.byte	-48
	.byte	-95
	.byte	7
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	21
	.byte	6
	.byte	23
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	9
	.byte	10
	.byte	27
	.byte	48
	.byte	49
	.byte	26
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	8
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	4
	.byte	20
	.byte	62
	.byte	-1
	.byte	65
	.byte	0
	.byte	0
	.byte	0
	.byte	74
	.byte	-118
	.byte	95
	.byte	-54
	.byte	-117
	.byte	-65
	.byte	66
	.byte	106
	.byte	0
	.byte	0
	.byte	0
	.byte	-31
	.byte	0
	.byte	81
	.byte	68
	.byte	82
	.byte	85
	.byte	0
	.byte	0
	.byte	71
	.byte	87
	.byte	0
	.byte	99
	.byte	101
	.byte	103
	.byte	113
	.byte	115
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	-116
	.byte	-114
	.byte	-112
	.byte	-101
	.byte	0
	.byte	0
	.byte	-86
	.byte	-82
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-22
	.byte	-78
	.byte	-76
	.byte	0
	.byte	-68
	.byte	-66
	.byte	-52
	.byte	-49
	.byte	-50
	.byte	0
	.byte	0
	.byte	67
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	108
	.byte	107
	.byte	75
	.byte	92
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	84
	.byte	70
	.byte	73
	.byte	89
	.byte	0
	.byte	98
	.byte	105
	.byte	0
	.byte	112
	.byte	-128
	.byte	0
	.byte	0
	.byte	0
	.byte	-74
	.byte	0
	.byte	0
	.byte	0
	.byte	-72
	.byte	0
	.byte	-71
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-33
	.byte	81
	.byte	68
	.byte	69
	.byte	82
	.byte	83
	.byte	85
	.byte	85
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	86
	.byte	86
	.byte	71
	.byte	72
	.byte	87
	.byte	87
	.byte	88
	.byte	88
	.byte	0
	.byte	0
	.byte	99
	.byte	99
	.byte	100
	.byte	100
	.byte	101
	.byte	101
	.byte	102
	.byte	102
	.byte	103
	.byte	103
	.byte	104
	.byte	104
	.byte	113
	.byte	113
	.byte	114
	.byte	114
	.byte	115
	.byte	115
	.byte	116
	.byte	116
	.byte	117
	.byte	117
	.byte	118
	.byte	118
	.byte	119
	.byte	119
	.byte	120
	.byte	120
	.byte	-116
	.byte	-116
	.byte	-115
	.byte	-115
	.byte	-114
	.byte	-114
	.byte	-113
	.byte	-113
	.byte	-112
	.byte	-112
	.byte	-102
	.byte	-102
	.byte	-101
	.byte	-101
	.byte	-100
	.byte	-100
	.byte	-99
	.byte	-99
	.byte	-98
	.byte	-98
	.byte	-97
	.byte	-97
	.byte	-96
	.byte	-96
	.byte	-86
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-78
	.byte	-77
	.byte	-77
	.byte	-76
	.byte	-76
	.byte	-75
	.byte	-75
	.byte	0
	.byte	0
	.byte	-73
	.byte	-73
	.byte	-68
	.byte	-68
	.byte	-67
	.byte	-67
	.byte	-66
	.byte	-66
	.byte	-53
	.byte	-53
	.byte	-52
	.byte	-52
	.byte	-51
	.byte	-51
	.byte	-49
	.byte	-49
	.byte	-38
	.byte	-37
	.byte	-50
	.byte	-50
	.align 32
	.type	from_idx, @object
	.size	from_idx, 128
from_idx:
	.value	0
	.value	164
	.long	0
	.value	171
	.value	173
	.long	-6
	.value	187
	.value	187
	.long	-19
	.value	215
	.value	215
	.long	-46
	.value	1548
	.value	1548
	.long	-1378
	.value	1563
	.value	1611
	.long	-1392
	.value	1632
	.value	1645
	.long	-1412
	.value	1776
	.value	1785
	.long	-1542
	.value	-1799
	.value	-1797
	.long	-63493
	.value	-1194
	.value	-1192
	.long	-64095
	.value	-1158
	.value	-1156
	.long	-64128
	.value	-1142
	.value	-1132
	.long	-64141
	.value	-1116
	.value	-1116
	.long	-64156
	.value	-1028
	.value	-1026
	.long	-64243
	.value	-384
	.value	-274
	.long	-64884
	.value	-1
	.value	-1
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	156
	.long	9
	.long	134
	.long	127
	.long	151
	.long	141
	.long	142
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	157
	.long	133
	.long	8
	.long	135
	.long	24
	.long	25
	.long	146
	.long	143
	.long	28
	.long	29
	.long	30
	.long	31
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	10
	.long	23
	.long	27
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	5
	.long	6
	.long	7
	.long	144
	.long	145
	.long	22
	.long	147
	.long	148
	.long	149
	.long	150
	.long	4
	.long	152
	.long	153
	.long	154
	.long	155
	.long	20
	.long	21
	.long	158
	.long	26
	.long	32
	.long	160
	.long	1548
	.long	1611
	.long	65153
	.long	65154
	.long	63738
	.long	65165
	.long	65166
	.long	63739
	.long	164
	.long	46
	.long	60
	.long	40
	.long	43
	.long	124
	.long	38
	.long	65152
	.long	65155
	.long	65156
	.long	63737
	.long	65157
	.long	65163
	.long	65167
	.long	65169
	.long	64342
	.long	33
	.long	36
	.long	42
	.long	41
	.long	59
	.long	172
	.long	45
	.long	47
	.long	64344
	.long	65173
	.long	65175
	.long	65177
	.long	65179
	.long	65181
	.long	65183
	.long	64378
	.long	1563
	.long	44
	.long	37
	.long	95
	.long	62
	.long	63
	.long	64380
	.long	65185
	.long	65187
	.long	65189
	.long	65191
	.long	65193
	.long	65195
	.long	65197
	.long	65199
	.long	96
	.long	58
	.long	35
	.long	64
	.long	39
	.long	61
	.long	34
	.long	64394
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	171
	.long	187
	.long	65201
	.long	65203
	.long	65205
	.long	65207
	.long	65209
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	65211
	.long	65213
	.long	65215
	.long	65217
	.long	65219
	.long	65221
	.long	65223
	.long	126
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	65225
	.long	65226
	.long	65227
	.long	65228
	.long	65229
	.long	65230
	.long	65231
	.long	65232
	.long	65233
	.long	65235
	.long	65237
	.long	65239
	.long	64398
	.long	65243
	.long	64402
	.long	64404
	.long	91
	.long	93
	.long	65245
	.long	65247
	.long	65249
	.long	215
	.long	123
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	173
	.long	65251
	.long	65253
	.long	65255
	.long	65261
	.long	65257
	.long	125
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	65259
	.long	65260
	.long	64420
	.long	64508
	.long	64509
	.long	64510
	.long	92
	.long	1567
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	1600
	.long	1776
	.long	1777
	.long	1778
	.long	1779
	.long	1780
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	1781
	.long	1782
	.long	1783
	.long	1784
	.long	1785
	.long	159
