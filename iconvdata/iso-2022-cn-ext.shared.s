	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-CN-EXT//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$18, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$17179869185, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$18, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$17179869188, %rdx
	movabsq	$25769803777, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"#$"
.LC2:
	.string	"#~"
.LC3:
	.string	"!j"
.LC4:
	.string	"!i"
.LC5:
	.string	"!a"
.LC6:
	.string	"!b"
.LC7:
	.string	"!n"
.LC8:
	.string	"!o"
.LC9:
	.string	"!q"
.LC10:
	.string	"!r"
.LC11:
	.string	"!p"
.LC12:
	.string	"!s"
.LC13:
	.string	"!t"
.LC14:
	.string	"!w"
.LC15:
	.string	"!x"
.LC16:
	.string	"!u"
.LC17:
	.string	"!v"
.LC18:
	.string	"!P"
.LC19:
	.string	"!%"
.LC20:
	.string	"!&"
.LC21:
	.string	"(8"
.LC22:
	.string	"(7"
.LC23:
	.string	"(6"
.LC24:
	.string	"(5"
.LC25:
	.string	"(3"
.LC26:
	.string	"(/"
.LC27:
	.string	"(+"
.LC28:
	.string	"(#"
.LC29:
	.string	"(1"
.LC30:
	.string	"(-"
.LC31:
	.string	"()"
.LC32:
	.string	"('"
.LC33:
	.string	"(%"
.LC34:
	.string	"\"d"
.LC35:
	.string	"\"g"
.LC36:
	.string	"\"f"
.LC37:
	.string	"\"!"
.LC38:
	.string	"\"J"
.LC39:
	.string	"\"G"
.LC40:
	.string	"\"k"
.LC41:
	.string	"\"\""
.LC42:
	.string	"\"j"
.LC43:
	.string	"../iconv/skeleton.c"
.LC44:
	.string	"outbufstart == NULL"
.LC45:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.section	.rodata.str1.1
.LC47:
	.string	"set == CNS11643_1_set"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC49:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC50:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC51:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC52:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC53:
	.string	"gb2312.h"
.LC54:
	.string	"cp[1] != '\\0'"
.LC55:
	.string	"used >= 1 && used <= 4"
	.section	.rodata
.LC56:
	.string	")A"
	.string	""
	.string	")G)E"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"(used >> 5) >= 3 && (used >> 5) <= 7"
	.section	.rodata.str1.1
.LC58:
	.string	"+I+J+K+L+M"
.LC59:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC61:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rbx
	movq	%rcx, %rbp
	leaq	48(%rsi), %rcx
	subq	$216, %rsp
	movq	%rbx, 96(%rsp)
	movl	16(%rsi), %ebx
	movq	%rdx, 16(%rsp)
	movq	%rdi, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 88(%rsp)
	movl	%ebx, %edx
	movl	272(%rsp), %r12d
	movq	%rcx, 104(%rsp)
	andl	$1, %edx
	movl	%ebx, 80(%rsp)
	movq	$0, 56(%rsp)
	jne	.L8
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rcx
	movq	%rcx, 56(%rsp)
	je	.L8
	movq	%rcx, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 56(%rsp)
.L8:
	testl	%r12d, %r12d
	jne	.L1685
	movq	64(%rsp), %rdi
	movq	16(%rsp), %rax
	movl	280(%rsp), %r8d
	movq	8(%r13), %r15
	testq	%rdi, %rdi
	movq	%rdi, %rdx
	movq	(%rax), %rax
	cmove	%r13, %rdx
	cmpq	$0, 88(%rsp)
	movq	(%rdx), %rcx
	movl	$0, %edx
	movq	$0, 144(%rsp)
	movq	%rax, %r11
	movq	%rcx, 8(%rsp)
	leaq	144(%rsp), %rcx
	cmovne	%rcx, %rdx
	movq	32(%r13), %rcx
	testl	%r8d, %r8d
	movq	%rdx, 112(%rsp)
	movq	%rcx, 24(%rsp)
	movl	(%rcx), %ecx
	movl	%ecx, 52(%rsp)
	jne	.L1686
.L21:
	movq	8(%rsp), %r14
	movq	%r11, %r12
	.p2align 4,,10
	.p2align 3
.L379:
	movl	52(%rsp), %eax
	sarl	$3, %eax
	movzbl	%al, %ecx
	xorb	%al, %al
	movl	%eax, 32(%rsp)
	movq	72(%rsp), %rax
	movl	%ecx, 40(%rsp)
	cmpq	$0, 96(%rax)
	je	.L1687
	movq	%r12, %rax
	movq	%r12, 176(%rsp)
	movq	%r14, 184(%rsp)
	cmpq	%rax, %rbp
	movq	%r14, %rbx
	movl	32(%rsp), %ecx
	movl	40(%rsp), %edx
	movl	$4, 8(%rsp)
	je	.L1688
.L723:
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rbp
	jb	.L1689
	cmpq	%rbx, %r15
	jbe	.L1673
	movl	(%rax), %eax
	cmpl	$127, %eax
	ja	.L442
	testl	%edx, %edx
	leaq	1(%rbx), %rsi
	je	.L443
	movq	%rsi, 184(%rsp)
	movb	$15, (%rbx)
	movq	184(%rsp), %rbx
	cmpq	%rbx, %r15
	je	.L1690
	leaq	1(%rbx), %rsi
.L443:
	xorl	%edx, %edx
	cmpl	$10, %eax
	movq	%rsi, 184(%rsp)
	cmove	%edx, %ecx
	movb	%al, (%rbx)
.L445:
	movq	176(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 176(%rsp)
.L704:
	cmpq	%rax, %rbp
	movq	184(%rsp), %rbx
	jne	.L723
.L1688:
	orl	%ecx, %edx
	movq	%rbp, %rax
	sall	$3, %edx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L442:
	cmpl	$1, %edx
	je	.L446
	movl	%ecx, %r8d
	andl	$1792, %r8d
	leal	-768(%r8), %esi
	andl	$-512, %esi
	jne	.L446
	cmpl	$4, %edx
	je	.L1691
	cmpl	$9249, %eax
	ja	.L520
	cmpl	$9216, %eax
	jnb	.L521
	cmpl	$8457, %eax
	je	.L522
	jbe	.L1692
	cmpl	$8601, %eax
	ja	.L532
	cmpl	$8592, %eax
	jnb	.L533
	cmpl	$8544, %eax
	jb	.L519
	cmpl	$8553, %eax
	ja	.L1693
	subl	$53, %eax
	movl	$36, %esi
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L446:
	cmpl	$9371, %eax
	ja	.L449
	cmpl	$9312, %eax
	jnb	.L450
	cmpl	$472, %eax
	je	.L451
	ja	.L452
	cmpl	$333, %eax
	je	.L453
	jbe	.L1694
	cmpl	$464, %eax
	je	.L460
	jbe	.L1695
	cmpl	$468, %eax
	je	.L464
	cmpl	$470, %eax
	je	.L465
	cmpl	$466, %eax
	jne	.L513
	leaq	.LC26(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L449:
	cmpl	$9792, %eax
	je	.L479
	ja	.L480
	cmpl	$9670, %eax
	je	.L481
	jbe	.L1696
	cmpl	$9678, %eax
	je	.L489
	jbe	.L1697
	cmpl	$9733, %eax
	je	.L493
	cmpl	$9734, %eax
	je	.L494
	cmpl	$9679, %eax
	jne	.L513
	leaq	.LC9(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L1687:
	cmpq	%r12, %rbp
	je	.L1147
	leaq	4(%r14), %rsi
	cmpq	%rsi, %r15
	jb	.L1148
	movl	80(%rsp), %r10d
	movl	32(%rsp), %r11d
	movq	%r12, %rax
	movq	%r14, %rbx
	movl	$4, 8(%rsp)
	andl	$2, %r10d
.L387:
	movzbl	(%rax), %edx
	cmpl	$127, %edx
	movl	%edx, %edi
	movl	%edx, %r8d
	jbe	.L388
	cmpq	$0, 112(%rsp)
	je	.L1167
	testl	%r10d, %r10d
	jne	.L1698
.L1167:
	movl	$6, 8(%rsp)
.L386:
	movq	16(%rsp), %rdi
	orl	%r11d, %ecx
	sall	$3, %ecx
	cmpq	$0, 64(%rsp)
	movq	%rax, (%rdi)
	movq	24(%rsp), %rax
	movl	%ecx, (%rax)
	jne	.L1699
.L724:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L1700
	cmpq	%rbx, %r14
	jnb	.L1182
	movq	56(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%rax, 152(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %ecx
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	pushq	%rcx
	movq	%rbx, %rcx
	pushq	$0
	movq	104(%rsp), %r9
	movq	120(%rsp), %rsi
	movq	112(%rsp), %rdi
	movq	72(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rdi
	popq	%r8
	je	.L728
	movq	152(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L1701
.L727:
	testl	%r10d, %r10d
	jne	.L1220
.L1069:
	movq	16(%rsp), %rax
	movq	0(%r13), %r14
	movq	(%rax), %r12
	movq	24(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 52(%rsp)
	movl	16(%r13), %eax
	movl	%eax, 80(%rsp)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L1689:
	orl	%ecx, %edx
	movl	$7, 8(%rsp)
	sall	$3, %edx
.L440:
	movq	16(%rsp), %rdi
	cmpq	$0, 64(%rsp)
	movq	%rax, (%rdi)
	movq	24(%rsp), %rax
	movl	%edx, (%rax)
	je	.L724
.L1699:
	movq	64(%rsp), %rax
	movl	8(%rsp), %r14d
	movq	%rbx, (%rax)
.L7:
	addq	$216, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L1748:
	movq	176(%rsp), %rax
	.p2align 4,,10
	.p2align 3
.L1673:
	orl	%ecx, %edx
	movl	$5, 8(%rsp)
	sall	$3, %edx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L388:
	cmpl	$27, %edx
	je	.L1702
	cmpl	$14, %edx
	je	.L1703
	cmpl	$15, %edx
	je	.L1704
.L396:
	testl	%ecx, %ecx
	jne	.L430
	addq	$1, %rax
.L419:
	movl	%r8d, (%rbx)
	movq	%rsi, %rbx
.L389:
	cmpq	%rax, %rbp
	je	.L386
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r15
	jnb	.L387
	movl	$5, 8(%rsp)
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L728:
	movl	8(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L727
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L452:
	cmpl	$1105, %eax
	ja	.L467
	cmpl	$1025, %eax
	jnb	.L468
	cmpl	$711, %eax
	je	.L469
	ja	.L470
	cmpl	$474, %eax
	jne	.L1705
	leaq	.LC22(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L480:
	cmpl	$40864, %eax
	jbe	.L1706
	cmpl	$65504, %eax
	je	.L502
	jbe	.L1707
	cmpl	$65507, %eax
	je	.L505
	cmpl	$65509, %eax
	je	.L506
	cmpl	$65505, %eax
	jne	.L513
	leaq	.LC3(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L1691:
	cmpl	$65509, %eax
	movl	$4, %r9d
	jbe	.L1708
.L515:
	leal	-19975(%rax), %esi
	cmpl	$20893, %esi
	ja	.L564
	leal	-13312(%rax), %esi
	leaq	(%rsi,%rsi,2), %rdi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	cmpb	$2, (%rdi)
	je	.L1709
.L564:
	cmpl	$9371, %eax
	ja	.L567
	cmpl	$9312, %eax
	jnb	.L568
	cmpl	$472, %eax
	je	.L569
	ja	.L570
	cmpl	$333, %eax
	je	.L571
	jbe	.L1710
	cmpl	$464, %eax
	je	.L578
	jbe	.L1711
	cmpl	$468, %eax
	je	.L582
	cmpl	$470, %eax
	jne	.L1712
	leaq	.LC24(%rip), %rdi
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L520:
	cmpl	$13269, %eax
	ja	.L540
	cmpl	$13198, %eax
	jnb	.L541
	cmpl	$12329, %eax
	ja	.L542
	cmpl	$12288, %eax
	jnb	.L543
	cmpl	$9312, %eax
	jb	.L519
	cmpl	$9341, %eax
	ja	.L1713
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L557:
	movzbl	(%rdi), %esi
	movl	$3, %r9d
	testb	%sil, %sil
	je	.L515
	movzbl	1(%rdi), %eax
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L567:
	cmpl	$9792, %eax
	je	.L597
	ja	.L598
	cmpl	$9670, %eax
	je	.L599
	jbe	.L1714
	cmpl	$9678, %eax
	je	.L607
	jbe	.L1715
	cmpl	$9733, %eax
	je	.L611
	cmpl	$9734, %eax
	jne	.L1716
	leaq	.LC7(%rip), %rdi
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L1686:
	movl	%ecx, %edx
	andl	$7, %edx
	je	.L21
	testq	%rdi, %rdi
	jne	.L1717
	movl	52(%rsp), %r11d
	sarl	$3, %r11d
	movzbl	%r11b, %ecx
	movl	%ecx, 32(%rsp)
	movq	72(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L1718
	movq	8(%rsp), %rcx
	cmpl	$4, %edx
	movq	%rax, 160(%rsp)
	movq	%rcx, 168(%rsp)
	ja	.L78
	movq	24(%rsp), %rsi
	leaq	140(%rsp), %r12
	movslq	%edx, %r10
	xorl	%edx, %edx
.L79:
	movzbl	4(%rsi,%rdx), %ecx
	movb	%cl, (%r12,%rdx)
	addq	$1, %rdx
	cmpq	%r10, %rdx
	jne	.L79
	movq	%rax, %rdx
	subq	%r10, %rdx
	addq	$4, %rdx
	cmpq	%rdx, %rbp
	jb	.L1719
	cmpq	%r15, 8(%rsp)
	jnb	.L1120
	leaq	1(%rax), %rdx
	leaq	139(%rsp), %rdi
.L87:
	movq	%rdx, 160(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %r10
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	$3, %r10
	movb	%cl, (%rdi,%r10)
	ja	.L1233
	cmpq	%rsi, %rbp
	ja	.L87
.L1233:
	movl	140(%rsp), %edx
	movq	%r12, 160(%rsp)
	cmpl	$127, %edx
	jbe	.L1720
	cmpl	$1, 32(%rsp)
	je	.L93
	movl	%r11d, %esi
	andl	$1792, %esi
	leal	-768(%rsi), %ecx
	andl	$-512, %ecx
	jne	.L93
	cmpl	$4, 32(%rsp)
	je	.L1721
	cmpl	$9249, %edx
	ja	.L169
	cmpl	$9216, %edx
	jnb	.L170
	cmpl	$8457, %edx
	je	.L171
	ja	.L172
	cmpl	$969, %edx
	ja	.L173
	cmpl	$913, %edx
	jnb	.L174
	cmpl	$167, %edx
	jb	.L168
	cmpl	$247, %edx
	ja	.L1722
	leal	-167(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
.L206:
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	je	.L168
.L1078:
	cmpl	$3, 32(%rsp)
	movzbl	1(%rdi), %edx
	movl	$3, %eax
	movl	$768, %edi
	je	.L160
.L209:
	cmpl	%edi, %esi
	je	.L366
.L1099:
	movq	8(%rsp), %rbx
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r15
	jb	.L91
	leal	-1(%rax), %esi
	cmpl	$3, %esi
	ja	.L1723
	addl	%esi, %esi
	leaq	.LC56(%rip), %rdi
	movslq	%esi, %rsi
.L1666:
	movq	8(%rsp), %rbx
	addq	%rdi, %rsi
	leaq	1(%rbx), %rdi
	movq	%rdi, 168(%rsp)
	movb	$27, (%rbx)
	movq	168(%rsp), %rdi
	leaq	1(%rdi), %r8
	movq	%r8, 168(%rsp)
	movb	$36, (%rdi)
	movq	168(%rsp), %rdi
	movzbl	(%rsi), %r9d
	leaq	1(%rdi), %r8
	movq	%r8, 168(%rsp)
	movb	%r9b, (%rdi)
	movzbl	1(%rsi), %edi
	movq	168(%rsp), %rsi
	leaq	1(%rsi), %r8
	movq	%r8, 168(%rsp)
	movb	%dil, (%rsi)
.L364:
	subl	$96, %eax
	movq	168(%rsp), %rsi
	cmpl	$128, %eax
	ja	.L372
	leaq	2(%rsi), %rax
	cmpq	%rax, %r15
	jb	.L91
	leaq	1(%rsi), %rax
	movq	%rax, 168(%rsp)
	movb	$27, (%rsi)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$79, (%rax)
	movq	168(%rsp), %rsi
.L371:
	leaq	2(%rsi), %rax
	cmpq	%rax, %r15
	jb	.L91
.L375:
	leaq	1(%rsi), %rax
	movq	%rax, 168(%rsp)
	movb	%cl, (%rsi)
	movq	168(%rsp), %rax
.L1682:
	leaq	1(%rax), %rcx
.L1667:
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	movq	160(%rsp), %rax
	leaq	4(%rax), %rdx
	cmpq	%r12, %rdx
	movq	%rdx, 160(%rsp)
	je	.L1669
.L354:
	movq	24(%rsp), %rax
	subq	%r12, %rdx
	movl	(%rax), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rdx
	jle	.L1724
	subq	%rcx, %rdx
	movq	16(%rsp), %rcx
	movq	(%rcx), %rbx
	addq	%rdx, %rbx
	movq	%rbx, %r11
	movq	%rbx, (%rcx)
	movq	168(%rsp), %rbx
	movq	%rbx, 8(%rsp)
.L1670:
	movq	24(%rsp), %rbx
	andl	$-8, %eax
	movl	%eax, 52(%rsp)
	movl	%eax, (%rbx)
	movl	16(%r13), %eax
	movl	%eax, 80(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L1696:
	cmpl	$9632, %eax
	je	.L483
	jbe	.L1725
	cmpl	$9650, %eax
	je	.L486
	cmpl	$9651, %eax
	je	.L487
	cmpl	$9633, %eax
	jne	.L513
	leaq	.LC16(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L1694:
	cmpl	$275, %eax
	je	.L1169
	jbe	.L1726
	cmpl	$283, %eax
	je	.L458
	cmpl	$299, %eax
	leaq	.LC31(%rip), %rdi
	je	.L455
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L1706:
	cmpl	$19968, %eax
	jnb	.L497
	cmpl	$12585, %eax
	ja	.L498
	cmpl	$12288, %eax
	jnb	.L499
	cmpl	$9794, %eax
	leaq	.LC5(%rip), %rdi
	jne	.L513
	.p2align 4,,10
	.p2align 3
.L455:
	movzbl	(%rdi), %esi
	movzbl	1(%rdi), %eax
.L1082:
	testb	%al, %al
	je	.L279
.L1101:
	cmpl	$1, %edx
	je	.L1727
	movl	%ecx, %r8d
	movl	$256, %r9d
	movl	$1, %edi
	andl	$1792, %r8d
.L560:
	cmpl	%r9d, %r8d
	je	.L714
.L1104:
	leaq	4(%rbx), %r8
	cmpq	%r8, %r15
	jb	.L1728
	leal	-1(%rdi), %r8d
	cmpl	$3, %r8d
	ja	.L1054
	leaq	1(%rbx), %r10
	leaq	.LC56(%rip), %r11
	addl	%r8d, %r8d
	movslq	%r8d, %r8
	andb	$-8, %ch
	movq	%r10, 184(%rsp)
	movb	$27, (%rbx)
	addq	%r11, %r8
	movq	184(%rsp), %r10
	orl	%r9d, %ecx
	leaq	1(%r10), %r11
	movq	%r11, 184(%rsp)
	movb	$36, (%r10)
	movq	184(%rsp), %r10
	movzbl	(%r8), %r11d
	leaq	1(%r10), %rbx
	movq	%rbx, 184(%rsp)
	movb	%r11b, (%r10)
	movzbl	1(%r8), %r10d
	movq	184(%rsp), %r8
	leaq	1(%r8), %r11
	movq	%r11, 184(%rsp)
	movb	%r10b, (%r8)
.L712:
	leal	-96(%rdi), %r9d
	movq	184(%rsp), %r8
	cmpl	$128, %r9d
	ja	.L719
	leaq	2(%r8), %r9
	cmpq	%r9, %r15
	jb	.L1674
	leaq	1(%r8), %r9
	movq	%r9, 184(%rsp)
	movb	$27, (%r8)
	movq	184(%rsp), %r8
	leaq	1(%r8), %r9
	movq	%r9, 184(%rsp)
	movb	$79, (%r8)
	movq	184(%rsp), %r8
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L1692:
	cmpl	$969, %eax
	ja	.L524
	cmpl	$913, %eax
	jnb	.L525
	cmpl	$167, %eax
	jb	.L519
	cmpl	$247, %eax
	ja	.L1729
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L540:
	cmpl	$65373, %eax
	ja	.L550
	cmpl	$65281, %eax
	jnb	.L551
	cmpl	$19968, %eax
	jb	.L519
	cmpl	$40860, %eax
	jbe	.L552
	leal	-65072(%rax), %esi
	cmpl	$59, %esi
	ja	.L519
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L1703:
	movl	%r11d, %edx
	leaq	1(%rax), %rsi
	andl	$1792, %edx
	je	.L413
	cmpl	$512, %edx
	je	.L1159
	jle	.L1730
	cmpl	$768, %edx
	je	.L416
	cmpl	$1024, %edx
	jne	.L47
	movq	%rsi, %rax
	movl	$4, %ecx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L467:
	cmpl	$8869, %eax
	ja	.L475
	cmpl	$8451, %eax
	jnb	.L476
	leal	-8213(%rax), %esi
	cmpl	$38, %esi
	ja	.L513
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L508:
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L1731
.L513:
	leal	-19975(%rax), %esi
	cmpl	$20893, %esi
	jbe	.L1732
	cmpl	$65509, %eax
	jbe	.L1230
.L670:
	cmpl	$9794, %eax
	jbe	.L691
	cmpl	$40869, %eax
	ja	.L684
	cmpl	$40861, %eax
	jnb	.L685
	cmpl	$13269, %eax
	ja	.L1733
	.p2align 4,,10
	.p2align 3
.L691:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L445
	orl	%ecx, %edx
	cmpq	$0, 112(%rsp)
	leal	0(,%rdx,8), %ecx
	je	.L1734
	movq	24(%rsp), %rax
	movl	%ecx, (%rax)
	testb	$8, 16(%r13)
	jne	.L706
	sarl	$3, %ecx
	movzbl	%cl, %edx
	xorb	%cl, %cl
.L707:
	testb	$2, 80(%rsp)
	movq	176(%rsp), %rax
	jne	.L708
	orl	%ecx, %edx
	movq	184(%rsp), %rbx
	movl	$6, 8(%rsp)
	sall	$3, %edx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L1700:
	movq	%rbx, 0(%r13)
	movq	88(%rsp), %rbx
	movq	144(%rsp), %rax
	movl	8(%rsp), %r14d
	addq	%rax, (%rbx)
.L726:
	cmpl	$7, %r14d
	jne	.L7
	movl	280(%rsp), %ecx
	testl	%ecx, %ecx
	je	.L7
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L1071
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rsi
	je	.L1073
.L1072:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1072
.L1073:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L1702:
	leaq	2(%rax), %r9
	cmpq	%r9, %rbp
	movq	%r9, 80(%rsp)
	jb	.L1165
	movzbl	1(%rax), %r9d
	cmpb	$36, %r9b
	je	.L1735
	cmpb	$78, %r9b
	jne	.L398
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 120(%rsp)
	jb	.L1165
	movzbl	2(%rax), %edx
	subl	$33, %edx
	cmpl	$92, %edx
	jbe	.L1736
.L418:
	cmpq	$0, 112(%rsp)
	movq	80(%rsp), %rax
	jne	.L1684
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	112(%rsp), %rdi
	addq	$1, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rdi)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L1182:
	movl	8(%rsp), %r10d
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rbp, %r8
	subq	%rax, %r8
	cmpq	$1, %r8
	jle	.L1165
	cmpl	$1, %ecx
	je	.L1737
	cmpl	$4, %ecx
	je	.L1738
	cmpl	$3, %ecx
	jne	.L780
	subl	$33, %edx
	cmpl	$92, %edx
	jbe	.L1739
.L432:
	cmpq	$0, 112(%rsp)
	je	.L1167
.L1684:
	testl	%r10d, %r10d
	je	.L1167
.L1671:
	movq	112(%rsp), %rdi
	addq	$2, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rdi)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L570:
	cmpl	$1105, %eax
	ja	.L585
	cmpl	$1025, %eax
	jnb	.L586
	cmpl	$711, %eax
	je	.L587
	ja	.L588
	cmpl	$474, %eax
	je	.L589
	cmpl	$476, %eax
	leaq	.LC21(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L598:
	cmpl	$40864, %eax
	jbe	.L1740
	cmpl	$65504, %eax
	je	.L620
	jbe	.L1741
	cmpl	$65507, %eax
	je	.L623
	cmpl	$65509, %eax
	jne	.L1742
	leaq	.LC1(%rip), %rdi
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	16(%rsp), %rax
	movl	52(%rsp), %ebx
	movq	%r12, (%rax)
	movq	24(%rsp), %rax
	movl	%ebx, (%rax)
	movq	72(%rsp), %rax
	cmpq	$0, 96(%rax)
	movl	16(%r13), %eax
	je	.L1743
	cmpq	%r12, %rbp
	movl	%eax, 52(%rsp)
	movq	%r12, 192(%rsp)
	movq	%r14, 200(%rsp)
	movq	%r14, %rdx
	movl	$4, %ebx
	movl	40(%rsp), %esi
	movl	32(%rsp), %r8d
	je	.L1744
.L1066:
	leaq	4(%r12), %rax
	cmpq	%rax, %rbp
	jb	.L1203
	cmpq	%rdx, %r11
	jbe	.L1204
	movl	(%r12), %ecx
	cmpl	$127, %ecx
	ja	.L785
	testl	%esi, %esi
	leaq	1(%rdx), %rax
	je	.L786
	movq	%rax, 200(%rsp)
	movb	$15, (%rdx)
	movq	200(%rsp), %rdx
	cmpq	%rdx, %r11
	je	.L1745
	leaq	1(%rdx), %rax
.L786:
	xorl	%esi, %esi
	cmpl	$10, %ecx
	movq	%rax, 200(%rsp)
	cmove	%esi, %r8d
	movb	%cl, (%rdx)
.L788:
	movq	192(%rsp), %rax
	leaq	4(%rax), %r12
	movq	%r12, 192(%rsp)
.L1047:
	cmpq	%r12, %rbp
	movq	200(%rsp), %rdx
	jne	.L1066
.L1744:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movq	%rbp, %r12
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %eax
	jbe	.L516
	.p2align 4,,10
	.p2align 3
.L517:
	addq	$8, %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %eax
	ja	.L517
.L516:
	movzwl	(%rsi), %edi
	cmpl	%edi, %eax
	jb	.L1173
	movl	4(%rsi), %edi
	movq	__isoir165_from_tab@GOTPCREL(%rip), %r8
	addl	%eax, %edi
	addl	%edi, %edi
	movzbl	(%r8,%rdi), %esi
	testb	%sil, %sil
	jne	.L1746
.L1173:
	movl	$4, %r9d
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L1709:
	cmpq	$-1, %rdi
	je	.L564
.L1113:
	cmpl	$24, %edx
	movzbl	1(%rdi), %esi
	movzbl	2(%rdi), %eax
	je	.L1747
	movl	%ecx, %edi
	andl	$6144, %edi
	cmpl	$6144, %edi
	je	.L715
	leaq	1(%rbx), %rdi
	orb	$24, %ch
	movq	%rdi, 184(%rsp)
	movb	$27, (%rbx)
	movq	184(%rsp), %rdi
	leaq	1(%rdi), %r8
	movq	%r8, 184(%rsp)
	movb	$36, (%rdi)
	movq	184(%rsp), %rdi
	leaq	1(%rdi), %r8
	movq	%r8, 184(%rsp)
	movb	$42, (%rdi)
	movq	184(%rsp), %rdi
	leaq	1(%rdi), %r8
	movq	%r8, 184(%rsp)
	movb	$72, (%rdi)
.L715:
	movq	184(%rsp), %rbx
	leaq	2(%rbx), %rdi
	cmpq	%rdi, %r15
	jb	.L1748
	leaq	1(%rbx), %rdi
	movq	%rdi, 184(%rsp)
	movb	$27, (%rbx)
	movq	184(%rsp), %rdi
	leaq	1(%rdi), %r8
	movq	%r8, 184(%rsp)
	movb	$78, (%rdi)
	movl	$24, %edi
	movq	184(%rsp), %r8
.L718:
	leaq	2(%r8), %r9
	cmpq	%r9, %r15
	jb	.L1674
.L722:
	leaq	1(%r8), %rdx
	movq	%rdx, 184(%rsp)
	movb	%sil, (%r8)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 184(%rsp)
	movb	%al, (%rdx)
	movl	%edi, %edx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L470:
	cmpl	$713, %eax
	jne	.L1749
	leaq	.LC19(%rip), %rdi
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L542:
	cmpl	$12585, %eax
	ja	.L546
	cmpl	$12549, %eax
	jnb	.L547
	cmpl	$12539, %eax
	leaq	.LC20(%rip), %rdi
	jne	.L519
	.p2align 4,,10
	.p2align 3
.L528:
	movzbl	(%rdi), %esi
	movzbl	1(%rdi), %eax
.L1083:
	cmpl	$3, %edx
	movl	$768, %r9d
	movl	$3, %edi
	jne	.L560
.L1672:
	movq	184(%rsp), %r8
	movl	%edx, %edi
.L518:
	addq	$2, %rbx
	cmpq	%rbx, %r15
	jnb	.L722
.L1674:
	orl	%ecx, %edx
	movq	176(%rsp), %rax
	movq	%r8, %rbx
	sall	$3, %edx
	movl	$5, 8(%rsp)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L524:
	cmpl	$8451, %eax
	je	.L1174
	ja	.L529
	leal	-8211(%rax), %esi
	cmpl	$43, %esi
	ja	.L519
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L532:
	cmpl	$8869, %eax
	je	.L536
	ja	.L537
	leal	-8725(%rax), %esi
	cmpl	$82, %esi
	ja	.L519
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L550:
	cmpl	$65505, %eax
	je	.L554
	cmpl	$65509, %eax
	je	.L555
	cmpl	$65504, %eax
	je	.L1750
.L519:
	movl	$3, %r9d
	jmp	.L515
.L413:
	cmpq	$0, 112(%rsp)
	je	.L1161
	testl	%r10d, %r10d
	jne	.L1671
.L1161:
	movq	%rsi, %rax
	movl	$6, 8(%rsp)
	jmp	.L386
.L398:
	cmpb	$79, %r9b
	jne	.L396
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 120(%rsp)
	jb	.L1165
	movl	%r11d, %r8d
	movzbl	2(%rax), %edx
	movzbl	3(%rax), %edi
	andl	$57344, %r8d
	cmpl	$40960, %r8d
	je	.L421
	jg	.L422
	cmpl	$24576, %r8d
	je	.L423
	cmpl	$32768, %r8d
	jne	.L420
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L420
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L420
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$7297, %edx
	jg	.L420
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movl	(%rdi,%rdx,4), %r8d
	testl	%r8d, %r8d
	je	.L420
.L427:
	cmpl	$65533, %r8d
	je	.L420
	movq	120(%rsp), %rax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L1714:
	cmpl	$9632, %eax
	je	.L601
	jbe	.L1751
	cmpl	$9650, %eax
	je	.L604
	cmpl	$9651, %eax
	jne	.L1752
	leaq	.LC14(%rip), %rdi
	jmp	.L573
.L498:
	leal	-12832(%rax), %esi
	cmpl	$9, %esi
	ja	.L513
	addl	$69, %eax
	movl	$34, %esi
	jmp	.L1101
.L1710:
	cmpl	$275, %eax
	je	.L1176
	jbe	.L1753
	cmpl	$283, %eax
	jne	.L1754
	leaq	.LC32(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L573:
	movzbl	(%rdi), %esi
	movzbl	1(%rdi), %eax
.L1084:
	testb	%al, %al
	je	.L279
.L1103:
	movl	$1, %edi
	jmp	.L629
.L1740:
	cmpl	$19968, %eax
	jnb	.L615
	cmpl	$12585, %eax
	ja	.L616
	cmpl	$12288, %eax
	jnb	.L617
	cmpl	$9794, %eax
	leaq	.LC5(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L1737:
	subl	$33, %edx
	cmpl	$86, %edx
	ja	.L432
	movzbl	1(%rax), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L432
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$8177, %edx
	jg	.L432
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
	testw	%r8w, %r8w
	je	.L432
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1726:
	leal	-164(%rax), %esi
	cmpl	$93, %esi
	ja	.L513
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L475:
	cmpl	$8978, %eax
	leaq	.LC18(%rip), %rdi
	je	.L455
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L464:
	leaq	.LC25(%rip), %rdi
	jmp	.L455
.L486:
	leaq	.LC15(%rip), %rdi
	jmp	.L455
.L493:
	leaq	.LC8(%rip), %rdi
	jmp	.L455
.L505:
	leaq	.LC2(%rip), %rdi
	jmp	.L455
.L585:
	cmpl	$8869, %eax
	ja	.L593
	cmpl	$8451, %eax
	jnb	.L594
	leal	-8213(%rax), %esi
	cmpl	$38, %esi
	ja	.L630
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L626:
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L1755
.L630:
	cmpl	$4, %r9d
	je	.L674
	cmpl	$65509, %eax
	movl	$3, %r8d
	ja	.L670
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L1718:
	cmpl	$4, %edx
	ja	.L24
	movq	24(%rsp), %r8
	movslq	%edx, %r10
	leaq	136(%rsp), %rdx
	movq	%r10, %rcx
	xorl	%esi, %esi
.L25:
	movzbl	4(%r8,%rsi), %edi
	movb	%dil, (%rdx,%rsi)
	addq	$1, %rsi
	cmpq	%r10, %rsi
	jne	.L25
	movq	8(%rsp), %rdi
	leaq	4(%rdi), %r12
	cmpq	%r12, %r15
	jb	.L1120
	leaq	135(%rsp), %r9
	movq	%rax, %rsi
.L26:
	addq	$1, %rsi
	movzbl	-1(%rsi), %edi
	addq	$1, %rcx
	cmpq	$3, %rcx
	setbe	%r8b
	cmpq	%rsi, %rbp
	movb	%dil, (%r9,%rcx)
	seta	%dil
	testb	%dil, %r8b
	jne	.L26
	movzbl	136(%rsp), %r9d
	cmpl	$127, %r9d
	movb	%r9b, 40(%rsp)
	movl	%r9d, %r8d
	jbe	.L28
	cmpq	$0, 112(%rsp)
	je	.L357
	andl	$2, %ebx
	jne	.L1756
.L357:
	movl	$6, %r14d
	jmp	.L7
.L1743:
	cmpq	%r12, %rbp
	je	.L1183
	leaq	4(%r14), %rbx
	movq	%r14, %rdx
	cmpq	%rbx, %r11
	jb	.L1185
	movq	112(%rsp), %r8
	andl	$2, %eax
	movl	$4, 52(%rsp)
	movl	%eax, 8(%rsp)
.L732:
	movzbl	(%r12), %eax
	cmpl	$127, %eax
	movl	%eax, %esi
	movl	%eax, %ecx
	jbe	.L733
	testq	%r8, %r8
	je	.L1202
	movl	8(%rsp), %esi
	testl	%esi, %esi
	jne	.L1757
.L1202:
	movl	$6, %ebx
.L731:
	movq	16(%rsp), %rax
	movq	24(%rsp), %rcx
	movq	%r12, (%rax)
	movl	40(%rsp), %eax
	orl	32(%rsp), %eax
	sall	$3, %eax
	movl	%eax, (%rcx)
	jmp	.L782
.L1721:
	cmpl	$65509, %edx
	ja	.L1138
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rcx
	movzwl	2(%rcx), %esi
	cmpl	%esi, %edx
	jbe	.L164
.L165:
	addq	$8, %rcx
	movzwl	2(%rcx), %esi
	cmpl	%esi, %edx
	ja	.L165
.L164:
	movzwl	(%rcx), %esi
	cmpl	%esi, %edx
	jb	.L1138
	movl	4(%rcx), %esi
	movq	__isoir165_from_tab@GOTPCREL(%rip), %rdi
	addl	%edx, %esi
	addl	%esi, %esi
	movzbl	(%rdi,%rsi), %ecx
	testb	%cl, %cl
	je	.L1138
	movq	8(%rsp), %rax
	movzbl	1(%rsi,%rdi), %edx
	addq	$2, %rax
	cmpq	%rax, %r15
	jnb	.L167
.L1120:
	movl	$5, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L719:
	testl	%edx, %edx
	jne	.L718
	leaq	1(%r8), %r9
	cmpq	%r9, %r15
	jb	.L1758
	movq	%r9, 184(%rsp)
	movb	$14, (%r8)
	movq	184(%rsp), %r8
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L1727:
	movq	184(%rsp), %r8
	movl	$1, %edi
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L714:
	cmpl	$24, %edi
	jne	.L712
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L1732:
	leal	-13312(%rax), %esi
	leaq	(%rsi,%rsi,2), %rdi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	cmpb	$2, (%rdi)
	je	.L562
.L1230:
	movl	$1, %r8d
.L563:
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %eax
	jbe	.L671
	.p2align 4,,10
	.p2align 3
.L672:
	addq	$8, %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %eax
	ja	.L672
.L671:
	movzwl	(%rsi), %edi
	cmpl	%edi, %eax
	jb	.L673
	movl	4(%rsi), %edi
	movq	__isoir165_from_tab@GOTPCREL(%rip), %r9
	addl	%eax, %edi
	addl	%edi, %edi
	movzbl	(%r9,%rdi), %esi
	testb	%sil, %sil
	je	.L673
	movzbl	1(%rdi,%r9), %eax
	movl	$4, %edi
	.p2align 4,,10
	.p2align 3
.L629:
	cmpl	%edi, %edx
	je	.L1672
	movl	%edi, %r9d
	movl	%ecx, %r8d
	sall	$8, %r9d
	andl	$1792, %r8d
	cmpl	%r9d, %r8d
	jne	.L1104
.L709:
	testb	$-32, %dil
	je	.L714
	movl	%edi, %r9d
	movl	%ecx, %r8d
	sall	$8, %r9d
	andl	$57344, %r8d
	cmpl	%r9d, %r8d
	je	.L712
	movl	%edi, %r8d
	sarl	$5, %r8d
	subl	$3, %r8d
	cmpl	$4, %r8d
	ja	.L1059
.L716:
	leaq	1(%rbx), %r10
	leaq	.LC58(%rip), %r11
	addl	%r8d, %r8d
	movslq	%r8d, %r8
	andb	$31, %ch
	movq	%r10, 184(%rsp)
	movb	$27, (%rbx)
	addq	%r11, %r8
	movq	184(%rsp), %r10
	orl	%r9d, %ecx
	leaq	1(%r10), %r11
	movq	%r11, 184(%rsp)
	movb	$36, (%r10)
	movq	184(%rsp), %r10
	movzbl	(%r8), %r11d
	leaq	1(%r10), %rbx
	movq	%rbx, 184(%rsp)
	movb	%r11b, (%r10)
	movzbl	1(%r8), %r10d
	movq	184(%rsp), %r8
	leaq	1(%r8), %r11
	movq	%r11, 184(%rsp)
	movb	%r10b, (%r8)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L785:
	cmpl	$1, %esi
	je	.L789
	movl	%r8d, %eax
	andl	$1792, %eax
	leal	-768(%rax), %edi
	andl	$-512, %edi
	jne	.L789
	cmpl	$4, %esi
	je	.L1759
	cmpl	$9249, %ecx
	ja	.L863
	cmpl	$9216, %ecx
	jnb	.L864
	cmpl	$8457, %ecx
	je	.L865
	ja	.L866
	cmpl	$969, %ecx
	ja	.L867
	cmpl	$913, %ecx
	jnb	.L868
	cmpl	$167, %ecx
	jb	.L862
	cmpl	$247, %ecx
	ja	.L1760
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %r9
	leal	-167(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
.L900:
	movzbl	(%rdi), %r9d
	movl	$3, %r12d
	testb	%r9b, %r9b
	jne	.L1761
.L858:
	leal	-19975(%rcx), %eax
	cmpl	$20893, %eax
	ja	.L907
	leal	-13312(%rcx), %eax
	leaq	(%rax,%rax,2), %rax
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rax
	cmpb	$2, (%rax)
	je	.L1762
.L907:
	cmpl	$9371, %ecx
	ja	.L910
	cmpl	$9312, %ecx
	jnb	.L911
	cmpl	$472, %ecx
	je	.L912
	ja	.L913
	cmpl	$333, %ecx
	je	.L914
	jbe	.L1763
	cmpl	$464, %ecx
	je	.L921
	jbe	.L1764
	cmpl	$468, %ecx
	je	.L925
	cmpl	$470, %ecx
	je	.L926
	cmpl	$466, %ecx
	jne	.L973
	leaq	.LC26(%rip), %rax
.L916:
	movzbl	(%rax), %r9d
	movzbl	1(%rax), %ecx
.L1089:
	testb	%cl, %cl
	je	.L279
.L1108:
	movl	$1, %r12d
.L972:
	cmpl	%r12d, %esi
	je	.L1676
	movl	%r12d, %edi
	movl	%r8d, %eax
	sall	$8, %edi
	andl	$1792, %eax
	cmpl	%edi, %eax
	movl	%edi, 8(%rsp)
	jne	.L1109
.L1052:
	testb	$-32, %r12b
	je	.L1057
	movl	%r12d, %eax
	movl	%r8d, %edi
	sall	$8, %eax
	andl	$57344, %edi
	cmpl	%eax, %edi
	movl	%eax, 8(%rsp)
	je	.L1055
	movl	%r12d, %edi
	sarl	$5, %edi
	subl	$3, %edi
	cmpl	$4, %edi
	ja	.L1059
.L1116:
	leaq	.LC58(%rip), %rax
	addl	%edi, %edi
	andl	$-57345, %r8d
	movslq	%edi, %rdi
	orl	8(%rsp), %r8d
	addq	%rax, %rdi
	movq	%rdi, %rax
	leaq	1(%rdx), %rdi
	movq	%rax, 32(%rsp)
	movzbl	(%rax), %eax
	movq	%rdi, 200(%rsp)
	movb	$27, (%rdx)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rdi
	movq	%rdi, 200(%rsp)
	movb	$36, (%rdx)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rdi
	movq	%rdi, 200(%rsp)
	movq	32(%rsp), %rdi
	movb	%al, (%rdx)
	movq	200(%rsp), %rdx
	movzbl	1(%rdi), %eax
	leaq	1(%rdx), %rdi
	movq	%rdi, 200(%rsp)
	movb	%al, (%rdx)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L673:
	cmpl	$3, %r8d
	je	.L670
	.p2align 4,,10
	.p2align 3
.L674:
	cmpl	$9249, %eax
	ja	.L633
	cmpl	$9216, %eax
	jnb	.L634
	cmpl	$8457, %eax
	je	.L635
	ja	.L636
	cmpl	$969, %eax
	ja	.L637
	cmpl	$913, %eax
	jnb	.L638
	cmpl	$167, %eax
	jb	.L670
	cmpl	$247, %eax
	jbe	.L639
	leal	-711(%rax), %esi
	cmpl	$18, %esi
	ja	.L670
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L633:
	cmpl	$13269, %eax
	ja	.L653
	cmpl	$13198, %eax
	jnb	.L654
	cmpl	$12329, %eax
	ja	.L655
	cmpl	$12288, %eax
	jnb	.L656
	cmpl	$9312, %eax
	jb	.L670
	cmpl	$9341, %eax
	jbe	.L657
	leal	-9472(%rax), %esi
	cmpl	$322, %esi
	ja	.L670
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L675:
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L670
	movzbl	1(%rdi), %eax
	.p2align 4,,10
	.p2align 3
.L1085:
	movl	$3, %edi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L789:
	cmpl	$9371, %ecx
	ja	.L792
	cmpl	$9312, %ecx
	jnb	.L793
	cmpl	$472, %ecx
	je	.L794
	ja	.L795
	cmpl	$333, %ecx
	je	.L796
	jbe	.L1765
	cmpl	$464, %ecx
	je	.L803
	jbe	.L1766
	cmpl	$468, %ecx
	je	.L807
	cmpl	$470, %ecx
	je	.L808
	cmpl	$466, %ecx
	jne	.L856
	leaq	.LC26(%rip), %rax
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	176(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rsi
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	88(%rsp), %rdi
	leaq	200(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	40(%rsp), %rbx
	movl	%eax, 24(%rsp)
	popq	%r9
	popq	%r10
	movl	(%rbx), %esi
	movl	%eax, %ebx
	movl	%esi, %ecx
	sarl	$3, %ecx
	movzbl	%cl, %edx
	xorb	%cl, %cl
	cmpl	$6, %eax
	je	.L707
	cmpl	$5, %ebx
	movq	176(%rsp), %rax
	jne	.L704
	andl	$-8, %esi
	movq	184(%rsp), %rbx
	movl	%esi, %edx
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L684:
	cmpl	$65373, %eax
	jbe	.L691
	cmpl	$131072, %eax
	jb	.L691
	cmpl	$173782, %eax
	jbe	.L689
	leal	-194560(%rax), %esi
	cmpl	$541, %esi
	ja	.L691
	leaq	(%rsi,%rsi,2), %r8
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %r8
	movzbl	(%r8), %edi
	testb	%dil, %dil
	je	.L691
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1695:
	cmpl	$363, %eax
	je	.L462
	cmpl	$462, %eax
	leaq	.LC28(%rip), %rdi
	je	.L455
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L1697:
	cmpl	$9671, %eax
	je	.L491
	cmpl	$9675, %eax
	leaq	.LC11(%rip), %rdi
	je	.L455
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L1148:
	movl	40(%rsp), %ecx
	movl	32(%rsp), %r11d
	movq	%r14, %rbx
	movq	%r12, %rax
	movl	$5, 8(%rsp)
	jmp	.L386
.L1735:
	leaq	3(%rax), %r9
	cmpq	%r9, %rbp
	jb	.L1165
	movzbl	2(%rax), %r9d
	cmpb	$41, %r9b
	je	.L1767
	cmpb	$42, %r9b
	jne	.L394
	leaq	4(%rax), %r9
	cmpq	%r9, %rbp
	movq	%r9, 120(%rsp)
	jb	.L1165
	cmpb	$72, 3(%rax)
	jne	.L396
.L402:
	orl	$6144, %r11d
.L404:
	movq	120(%rsp), %rax
	jmp	.L389
.L562:
	cmpq	$-1, %rdi
	jne	.L1113
	jmp	.L1230
.L1685:
	cmpq	$0, 64(%rsp)
	jne	.L1768
	cmpl	$1, %r12d
	movq	32(%r13), %rbx
	jne	.L11
	movl	(%rbx), %r12d
	movq	0(%r13), %rax
	movl	%r12d, %ecx
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L12
	movq	72(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L1769
	cmpq	8(%r13), %rax
	je	.L1120
	movb	$15, (%rax)
	testb	$1, 16(%r13)
	leaq	1(%rax), %rbp
	je	.L15
	movq	88(%rsp), %rax
	addq	$1, (%rax)
	movq	32(%r13), %rax
	movl	$0, (%rax)
	movq	%rbp, %rax
.L1074:
	movq	%rax, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L7
.L1704:
	addq	$1, %rax
	xorl	%ecx, %ecx
	jmp	.L389
.L1705:
	cmpl	$476, %eax
	leaq	.LC21(%rip), %rdi
	je	.L455
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L1749:
	jb	.L513
	leal	-913(%rax), %esi
	cmpl	$56, %esi
	ja	.L513
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L792:
	cmpl	$9792, %ecx
	je	.L822
	ja	.L823
	cmpl	$9670, %ecx
	je	.L824
	jbe	.L1770
	cmpl	$9678, %ecx
	je	.L832
	jbe	.L1771
	cmpl	$9733, %ecx
	je	.L836
	cmpl	$9734, %ecx
	je	.L837
	cmpl	$9679, %ecx
	jne	.L856
	leaq	.LC9(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L798:
	movzbl	(%rax), %r9d
	movzbl	1(%rax), %ecx
.L1087:
	testb	%cl, %cl
	je	.L279
.L1106:
	cmpl	$1, %esi
	je	.L1772
	movl	%r8d, %eax
	movl	$256, 8(%rsp)
	movl	$1, %r12d
	andl	$1792, %eax
.L903:
	cmpl	8(%rsp), %eax
	je	.L1057
.L1109:
	leaq	4(%rdx), %rax
	cmpq	%rax, %r11
	jb	.L1773
	leal	-1(%r12), %eax
	cmpl	$3, %eax
	ja	.L1054
	leal	(%rax,%rax), %edi
	leaq	.LC56(%rip), %rax
	andl	$-1793, %r8d
	orl	8(%rsp), %r8d
	movslq	%edi, %rdi
	addq	%rax, %rdi
	leaq	1(%rdx), %rax
	movq	%rax, 200(%rsp)
	movb	$27, (%rdx)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$36, (%rax)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movzbl	(%rdi), %edx
	movb	%dl, (%rax)
	movq	200(%rsp), %rdx
	movzbl	1(%rdi), %eax
	leaq	1(%rdx), %rdi
	movq	%rdi, 200(%rsp)
	movb	%al, (%rdx)
.L1055:
	leal	-96(%r12), %eax
	movq	200(%rsp), %rdi
	cmpl	$128, %eax
	ja	.L1062
	leaq	2(%rdi), %rax
	cmpq	%rax, %r11
	jb	.L1677
	leaq	1(%rdi), %rax
	movq	%rax, 200(%rsp)
	movb	$27, (%rdi)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$79, (%rax)
	movq	200(%rsp), %rdi
.L1061:
	leaq	2(%rdi), %rax
	cmpq	%rax, %r11
	jb	.L1677
.L1065:
	leaq	1(%rdi), %rax
	movl	%r12d, %esi
	movq	%rax, 200(%rsp)
	movb	%r9b, (%rdi)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	%cl, (%rax)
	jmp	.L788
.L1707:
	leal	-65281(%rax), %esi
	cmpl	$93, %esi
	ja	.L513
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L636:
	cmpl	$8601, %eax
	ja	.L645
	cmpl	$8592, %eax
	jnb	.L646
	cmpl	$8544, %eax
	jb	.L670
	cmpl	$8553, %eax
	jbe	.L647
	leal	-8560(%rax), %esi
	cmpl	$9, %esi
	ja	.L670
	subl	$59, %eax
	movl	$38, %esi
	jmp	.L1085
.L1725:
	leal	-9472(%rax), %esi
	cmpl	$75, %esi
	ja	.L513
	addl	$36, %eax
	movl	$41, %esi
	jmp	.L1101
.L653:
	cmpl	$65373, %eax
	ja	.L663
	cmpl	$65281, %eax
	jnb	.L664
	cmpl	$19968, %eax
	jb	.L670
	cmpl	$40860, %eax
	jbe	.L665
	leal	-65072(%rax), %esi
	cmpl	$59, %esi
	ja	.L670
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L489:
	leaq	.LC10(%rip), %rdi
	jmp	.L455
.L491:
	leaq	.LC12(%rip), %rdi
	jmp	.L455
.L506:
	leaq	.LC1(%rip), %rdi
	jmp	.L455
.L494:
	leaq	.LC7(%rip), %rdi
	jmp	.L455
.L502:
	leaq	.LC4(%rip), %rdi
	jmp	.L455
.L497:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L487:
	leaq	.LC14(%rip), %rdi
	jmp	.L455
.L465:
	leaq	.LC24(%rip), %rdi
	jmp	.L455
.L483:
	leaq	.LC17(%rip), %rdi
	jmp	.L455
.L460:
	leaq	.LC27(%rip), %rdi
	jmp	.L455
.L468:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L479:
	leaq	.LC6(%rip), %rdi
	jmp	.L455
.L481:
	leaq	.LC13(%rip), %rdi
	jmp	.L455
.L451:
	leaq	.LC23(%rip), %rdi
	jmp	.L455
.L453:
	leaq	.LC30(%rip), %rdi
	jmp	.L455
.L450:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L458:
	leaq	.LC32(%rip), %rdi
	jmp	.L455
.L476:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L499:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L508
.L1169:
	leaq	.LC33(%rip), %rdi
	jmp	.L455
.L462:
	leaq	.LC29(%rip), %rdi
	jmp	.L455
.L469:
	leaq	.LC20(%rip), %rdi
	jmp	.L455
.L1747:
	movq	184(%rsp), %r8
	movl	$24, %edi
	jmp	.L518
.L1733:
	cmpl	$13312, %eax
	jb	.L691
	cmpl	$19967, %eax
	jbe	.L685
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %esi
	cmpb	$0, (%rdi,%rsi,2)
	jne	.L691
.L685:
	leal	-13312(%rax), %esi
	leaq	(%rsi,%rsi,2), %r8
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %r8
	movzbl	(%r8), %edi
	testb	%dil, %dil
	je	.L691
.L1614:
	subl	$3, %edi
	movzbl	1(%r8), %esi
	movzbl	2(%r8), %r8d
	cmpb	$4, %dil
	ja	.L691
	leaq	.L696(%rip), %r11
	movzbl	%dil, %edi
	movl	%r8d, %eax
	movslq	(%r11,%rdi,4), %rdi
	addq	%r11, %rdi
	jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L696:
	.long	.L695-.L696
	.long	.L697-.L696
	.long	.L698-.L696
	.long	.L699-.L696
	.long	.L700-.L696
	.text
.L1220:
	movl	%r10d, %r14d
	jmp	.L726
.L1738:
	subl	$33, %edi
	cmpb	$93, %dil
	ja	.L432
	movzbl	1(%rax), %edi
	leal	-33(%rdi), %r8d
	cmpb	$93, %r8b
	ja	.L432
	subl	$33, %edx
	imull	$94, %edx, %edx
	leal	-33(%rdx,%rdi), %edx
	movq	__isoir165_to_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
	testl	%r8d, %r8d
	je	.L432
.L1678:
	addq	$2, %rax
	cmpl	$65533, %r8d
	jne	.L419
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$7, %ebx
.L784:
	movq	16(%rsp), %rax
	movq	24(%rsp), %rcx
	movq	152(%rsp), %r11
	movq	%r12, (%rax)
	movl	40(%rsp), %eax
	orl	32(%rsp), %eax
	sall	$3, %eax
	movl	%eax, (%rcx)
.L782:
	cmpq	%r11, %rdx
	jne	.L1774
	cmpq	$5, %rbx
	jne	.L1775
	cmpq	%rdx, %r14
	jne	.L727
	subl	$1, 20(%r13)
	jmp	.L727
.L708:
	movq	112(%rsp), %rbx
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 176(%rsp)
	addq	$1, (%rbx)
	jmp	.L704
.L1713:
	leal	-9472(%rax), %esi
	cmpl	$322, %esi
	ja	.L519
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L1204:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$5, %ebx
	jmp	.L784
.L1693:
	leal	-8560(%rax), %esi
	cmpl	$9, %esi
	ja	.L519
	subl	$59, %eax
	movl	$38, %esi
	jmp	.L1083
.L1729:
	leal	-711(%rax), %esi
	cmpl	$18, %esi
	ja	.L519
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L733:
	cmpl	$27, %eax
	je	.L1776
	cmpl	$14, %eax
	je	.L1777
	cmpl	$15, %eax
	je	.L1778
.L741:
	movl	40(%rsp), %edi
	testl	%edi, %edi
	jne	.L775
	addq	$1, %r12
.L764:
	movl	%ecx, (%rdx)
	movq	%rbx, %rdx
.L734:
	cmpq	%r12, %rbp
	je	.L1779
	leaq	4(%rdx), %rbx
	cmpq	%rbx, %r11
	jnb	.L732
.L1185:
	movl	$5, %ebx
	jmp	.L731
.L1690:
	movq	176(%rsp), %rax
	leal	0(,%rcx,8), %edx
	movq	%r15, %rbx
	movl	$5, 8(%rsp)
	jmp	.L440
.L11:
	movq	$0, (%rbx)
	xorl	%r14d, %r14d
	testb	$1, 16(%r13)
	jne	.L7
	movq	56(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	pushq	%rax
	pushq	%r12
	jmp	.L1681
.L588:
	cmpl	$713, %eax
	je	.L591
	jb	.L630
	leal	-913(%rax), %esi
	cmpl	$56, %esi
	ja	.L630
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L1746:
	movzbl	1(%rdi,%r8), %eax
	movq	%rbx, %r8
	movl	$4, %edi
	jmp	.L518
.L537:
	cmpl	$8895, %eax
	leaq	.LC38(%rip), %rdi
	je	.L528
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L529:
	cmpl	$8453, %eax
	leaq	.LC41(%rip), %rdi
	je	.L528
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L655:
	cmpl	$12585, %eax
	ja	.L659
	cmpl	$12549, %eax
	jnb	.L660
	cmpl	$12539, %eax
	jne	.L670
	leaq	.LC20(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L641:
	movzbl	(%rax), %esi
	movzbl	1(%rax), %eax
	jmp	.L1085
.L645:
	cmpl	$8869, %eax
	je	.L649
	ja	.L650
	leal	-8725(%rax), %esi
	cmpl	$82, %esi
	ja	.L670
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L637:
	cmpl	$8451, %eax
	je	.L1177
	ja	.L642
	leal	-8211(%rax), %esi
	cmpl	$43, %esi
	ja	.L670
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L555:
	leaq	.LC34(%rip), %rdi
	jmp	.L528
.L394:
	cmpb	$43, %r9b
	jne	.L396
	leaq	4(%rax), %r9
	cmpq	%r9, %rbp
	movq	%r9, 120(%rsp)
	jb	.L1165
	movzbl	3(%rax), %r9d
	movb	%r9b, 80(%rsp)
	subl	$73, %r9d
	cmpb	$4, %r9b
	ja	.L396
.L403:
	cmpb	$71, 80(%rsp)
	je	.L1780
	cmpb	$69, 80(%rsp)
	je	.L1781
	cmpb	$72, 80(%rsp)
	je	.L402
	cmpb	$73, 80(%rsp)
	je	.L1782
	cmpb	$74, 80(%rsp)
	je	.L1783
	cmpb	$75, 80(%rsp)
	je	.L1784
	cmpb	$76, 80(%rsp)
	je	.L1785
	movl	%r11d, %eax
	orb	$-32, %ah
	cmpb	$77, 80(%rsp)
	cmove	%eax, %r11d
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L663:
	cmpl	$65505, %eax
	je	.L667
	cmpl	$65509, %eax
	je	.L668
	cmpl	$65504, %eax
	jne	.L670
	leaq	.LC36(%rip), %rax
	jmp	.L641
.L1720:
	movl	32(%rsp), %esi
	movq	8(%rsp), %rax
	testl	%esi, %esi
	leaq	1(%rax), %rcx
	je	.L1133
	movq	%rcx, 168(%rsp)
	movb	$15, (%rax)
	movq	168(%rsp), %rax
	cmpq	%rax, %r15
	jne	.L1682
.L91:
	movq	160(%rsp), %rdx
	cmpq	%r12, %rdx
	jne	.L354
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L795:
	cmpl	$1105, %ecx
	ja	.L810
	cmpl	$1025, %ecx
	jnb	.L811
	cmpl	$711, %ecx
	je	.L812
	jbe	.L1786
	cmpl	$713, %ecx
	je	.L816
	jb	.L856
	leal	-913(%rcx), %eax
	cmpl	$56, %eax
	ja	.L856
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
.L851:
	movzbl	(%rax), %r9d
	testb	%r9b, %r9b
	jne	.L1787
.L856:
	leal	-19975(%rcx), %eax
	cmpl	$20893, %eax
	jbe	.L1788
	cmpl	$65509, %ecx
	jbe	.L1231
.L1013:
	cmpl	$9794, %ecx
	jbe	.L1034
	cmpl	$40869, %ecx
	ja	.L1027
	cmpl	$40861, %ecx
	jnb	.L1028
	cmpl	$13269, %ecx
	ja	.L1789
.L1034:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L788
	movl	%esi, %eax
	orl	%r8d, %eax
	sall	$3, %eax
	cmpq	$0, 112(%rsp)
	je	.L1790
	movq	24(%rsp), %rbx
	movl	%eax, (%rbx)
	testb	$8, 16(%r13)
	jne	.L1049
	sarl	$3, %eax
	movl	%eax, %r8d
	movzbl	%al, %esi
	xorb	%r8b, %r8b
.L1050:
	testb	$2, 52(%rsp)
	movq	192(%rsp), %r12
	jne	.L1051
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$6, %ebx
	movq	200(%rsp), %rdx
	jmp	.L784
.L823:
	cmpl	$40864, %ecx
	jbe	.L1791
	cmpl	$65504, %ecx
	je	.L845
	jbe	.L1792
	cmpl	$65507, %ecx
	je	.L848
	cmpl	$65509, %ecx
	je	.L849
	cmpl	$65505, %ecx
	jne	.L856
	leaq	.LC3(%rip), %rax
	jmp	.L798
.L416:
	movq	%rsi, %rax
	movl	$3, %ecx
	jmp	.L389
.L422:
	cmpl	$49152, %r8d
	je	.L425
	cmpl	$57344, %r8d
	jne	.L420
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L420
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L420
	imull	$94, %edx, %edx
	addl	%edx, %edi
	cmpl	$6538, %edi
	jg	.L420
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %rdx
	movslq	%edi, %rdi
	movl	(%rdx,%rdi,4), %r8d
	testl	%r8d, %r8d
	jne	.L427
.L420:
	cmpq	$0, 112(%rsp)
	je	.L1167
	testl	%r10d, %r10d
	je	.L1167
	movq	112(%rsp), %rdi
	movq	120(%rsp), %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rdi)
	jmp	.L389
.L1769:
	movl	$0, (%rbx)
.L12:
	testl	%edx, %edx
	jne	.L1074
.L16:
	movq	56(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	pushq	%rax
	pushq	$1
.L1681:
	movq	104(%rsp), %r9
	movq	120(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	112(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	popq	%r9
	movl	%eax, %r14d
	popq	%r10
	jmp	.L7
.L863:
	cmpl	$13269, %ecx
	ja	.L883
	cmpl	$13198, %ecx
	jnb	.L884
	cmpl	$12329, %ecx
	ja	.L885
	cmpl	$12288, %ecx
	jnb	.L886
	cmpl	$9312, %ecx
	jb	.L862
	cmpl	$9341, %ecx
	ja	.L1793
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %r9
	leal	-9312(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L593:
	cmpl	$8978, %eax
	leaq	.LC18(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L1753:
	leal	-164(%rax), %esi
	cmpl	$93, %esi
	ja	.L630
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L589:
	leaq	.LC22(%rip), %rdi
	jmp	.L573
.L591:
	leaq	.LC19(%rip), %rdi
	jmp	.L573
.L582:
	leaq	.LC25(%rip), %rdi
	jmp	.L573
.L611:
	leaq	.LC8(%rip), %rdi
	jmp	.L573
.L604:
	leaq	.LC15(%rip), %rdi
	jmp	.L573
.L623:
	leaq	.LC2(%rip), %rdi
	jmp	.L573
.L689:
	leal	-131072(%rax), %esi
	leaq	(%rsi,%rsi,2), %r8
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %r8
	movzbl	(%r8), %edi
	testb	%dil, %dil
	je	.L691
	jmp	.L1614
.L617:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L1776:
	leaq	2(%r12), %rdi
	cmpq	%rdi, %rbp
	jb	.L1200
	movzbl	1(%r12), %r9d
	cmpb	$36, %r9b
	je	.L1794
	cmpb	$78, %r9b
	jne	.L743
	leaq	4(%r12), %rsi
	cmpq	%rsi, %rbp
	jb	.L1200
	movzbl	2(%r12), %eax
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1795
.L763:
	testq	%r8, %r8
	je	.L1199
	movl	8(%rsp), %ebx
	testl	%ebx, %ebx
	jne	.L1796
.L1199:
	movq	%rdi, %r12
	movl	$6, %ebx
	jmp	.L731
.L1757:
	addq	$1, %r12
	addq	$1, (%r8)
	movl	$6, 52(%rsp)
	jmp	.L734
.L699:
	cmpl	$192, %edx
	movl	$192, %edi
	je	.L702
.L701:
	movl	%edi, %r9d
	movl	%ecx, %r8d
	sall	$8, %r9d
	andl	$57344, %r8d
	cmpl	%r8d, %r9d
	je	.L712
	movl	%edi, %r8d
	sarl	$5, %r8d
	subl	$3, %r8d
	jmp	.L716
.L698:
	cmpl	$160, %edx
	movl	$160, %edi
	jne	.L701
.L702:
	movq	184(%rsp), %r8
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L695:
	cmpl	$96, %edx
	jne	.L1223
	movq	184(%rsp), %r8
	movl	$96, %edi
	jmp	.L518
.L697:
	cmpl	$128, %edx
	movl	$128, %edi
	jne	.L701
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L700:
	cmpl	$224, %edx
	movl	$224, %edi
	jne	.L701
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L1711:
	cmpl	$363, %eax
	jne	.L1797
	leaq	.LC29(%rip), %rdi
	jmp	.L573
.L1062:
	testl	%esi, %esi
	jne	.L1061
	leaq	1(%rdi), %rax
	cmpq	%rax, %r11
	jb	.L1677
	movq	%rax, 200(%rsp)
	movb	$14, (%rdi)
	movq	200(%rsp), %rdi
	jmp	.L1061
.L1715:
	cmpl	$9671, %eax
	jne	.L1798
	leaq	.LC12(%rip), %rdi
	jmp	.L573
.L546:
	cmpl	$12963, %eax
	leaq	.LC37(%rip), %rdi
	je	.L528
	jmp	.L519
.L1038:
	cmpl	$96, %esi
	movl	$96, %r12d
	jne	.L1052
.L1676:
	movq	200(%rsp), %rdi
	movl	%esi, %r12d
.L861:
	addq	$2, %rdx
	cmpq	%rdx, %r11
	jnb	.L1065
.L1677:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movq	%rdi, %rdx
	movq	192(%rsp), %r12
	movl	$5, %ebx
	jmp	.L784
.L1767:
	leaq	4(%rax), %r9
	cmpq	%r9, %rbp
	movq	%r9, 120(%rsp)
	jb	.L1165
	movzbl	3(%rax), %r9d
	movb	%r9b, 80(%rsp)
	andl	$-5, %r9d
	cmpb	$65, %r9b
	je	.L401
	cmpb	$71, 80(%rsp)
	jne	.L396
.L401:
	cmpb	$65, 80(%rsp)
	jne	.L403
	andl	$-1793, %r11d
	orl	$256, %r11d
	jmp	.L404
.L521:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L554:
	leaq	.LC35(%rip), %rdi
	jmp	.L528
.L552:
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L533:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L1174:
	leaq	.LC42(%rip), %rdi
	jmp	.L528
.L551:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L525:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L536:
	leaq	.LC39(%rip), %rdi
	jmp	.L528
.L547:
	addl	$66, %eax
	movl	$37, %esi
	jmp	.L1083
.L1750:
	leaq	.LC36(%rip), %rdi
	jmp	.L528
.L543:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L522:
	leaq	.LC40(%rip), %rdi
	jmp	.L528
.L541:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L557
.L1730:
	cmpl	$256, %edx
	jne	.L47
	movq	%rsi, %rax
	movl	$1, %ecx
	jmp	.L389
.L1159:
	movq	%rsi, %rax
	movl	$2, %ecx
	jmp	.L389
.L1739:
	movzbl	1(%rax), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L432
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$8690, %edx
	jg	.L432
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %r8d
	testw	%r8w, %r8w
	je	.L432
	jmp	.L1678
.L616:
	leal	-12832(%rax), %esi
	cmpl	$9, %esi
	ja	.L630
	addl	$69, %eax
	movl	$34, %esi
	jmp	.L1103
.L1772:
	movq	200(%rsp), %rdi
	movl	$1, %r12d
	jmp	.L861
.L1759:
	cmpl	$65509, %ecx
	movl	$4, %r12d
	ja	.L858
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %ecx
	jbe	.L859
.L860:
	addq	$8, %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %ecx
	ja	.L860
.L859:
	movzwl	(%rax), %edi
	cmpl	%edi, %ecx
	jb	.L1210
	movl	4(%rax), %edi
	addl	%ecx, %edi
	movl	%edi, %eax
	movq	__isoir165_from_tab@GOTPCREL(%rip), %rdi
	addl	%eax, %eax
	movzbl	(%rdi,%rax), %r9d
	testb	%r9b, %r9b
	je	.L1210
	movzbl	1(%rax,%rdi), %ecx
	movl	$4, %r12d
	movq	%rdx, %rdi
	jmp	.L861
.L1147:
	movl	40(%rsp), %ecx
	movl	32(%rsp), %r11d
	movq	%r14, %rbx
	movq	%rbp, %rax
	movl	$4, 8(%rsp)
	jmp	.L386
.L1763:
	cmpl	$275, %ecx
	je	.L1213
	jbe	.L1799
	cmpl	$283, %ecx
	je	.L919
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rax
	je	.L916
.L973:
	cmpl	$4, %r12d
	jne	.L974
.L1017:
	cmpl	$9249, %ecx
	jbe	.L1800
	cmpl	$13269, %ecx
	ja	.L996
	cmpl	$13198, %ecx
	jnb	.L997
	cmpl	$12329, %ecx
	ja	.L998
	cmpl	$12288, %ecx
	jnb	.L999
	cmpl	$9312, %ecx
	jb	.L1013
	cmpl	$9341, %ecx
	jbe	.L1000
	leal	-9472(%rcx), %eax
	cmpl	$322, %eax
	ja	.L1013
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
.L1018:
	movzbl	(%rax), %r9d
	testb	%r9b, %r9b
	je	.L1013
	movzbl	1(%rax), %ecx
.L1090:
	movl	$3, %r12d
	jmp	.L972
.L1716:
	cmpl	$9679, %eax
	jne	.L630
	leaq	.LC9(%rip), %rdi
	jmp	.L573
.L1741:
	leal	-65281(%rax), %esi
	cmpl	$93, %esi
	ja	.L630
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L1165:
	movl	$7, 8(%rsp)
	jmp	.L386
.L1754:
	cmpl	$299, %eax
	leaq	.LC31(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L910:
	cmpl	$9792, %ecx
	je	.L940
	ja	.L941
	cmpl	$9670, %ecx
	je	.L942
	jbe	.L1801
	cmpl	$9678, %ecx
	je	.L950
	jbe	.L1802
	cmpl	$9733, %ecx
	je	.L954
	cmpl	$9734, %ecx
	je	.L955
	cmpl	$9679, %ecx
	jne	.L973
	leaq	.LC9(%rip), %rax
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1797:
	cmpl	$462, %eax
	leaq	.LC28(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L1798:
	cmpl	$9675, %eax
	leaq	.LC11(%rip), %rdi
	je	.L573
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L1752:
	cmpl	$9633, %eax
	jne	.L630
	leaq	.LC16(%rip), %rdi
	jmp	.L573
.L1742:
	cmpl	$65505, %eax
	jne	.L630
	leaq	.LC3(%rip), %rdi
	jmp	.L573
.L1712:
	cmpl	$466, %eax
	jne	.L630
	leaq	.LC26(%rip), %rdi
	jmp	.L573
.L93:
	cmpl	$9371, %edx
	ja	.L96
	cmpl	$9312, %edx
	jnb	.L97
	cmpl	$472, %edx
	je	.L98
	ja	.L99
	cmpl	$333, %edx
	je	.L100
	jbe	.L1803
	cmpl	$464, %edx
	je	.L107
	jbe	.L1804
	cmpl	$468, %edx
	je	.L111
	cmpl	$470, %edx
	jne	.L1805
	leaq	.LC24(%rip), %rsi
.L102:
	movzbl	(%rsi), %ecx
.L1077:
	movzbl	1(%rsi), %edx
	testb	%dl, %dl
	je	.L279
	cmpl	$1, 32(%rsp)
	je	.L160
	movl	%r11d, %esi
	movl	$256, %edi
	movl	$1, %eax
	andl	$1792, %esi
	jmp	.L209
.L1751:
	leal	-9472(%rax), %esi
	cmpl	$75, %esi
	ja	.L630
	addl	$36, %eax
	movl	$41, %esi
	jmp	.L1103
.L657:
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L656:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L647:
	subl	$53, %eax
	movl	$36, %esi
	jmp	.L1085
.L646:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L668:
	leaq	.LC34(%rip), %rax
	jmp	.L641
.L667:
	leaq	.LC35(%rip), %rax
	jmp	.L641
.L654:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L634:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L649:
	leaq	.LC39(%rip), %rax
	jmp	.L641
.L665:
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L664:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L639:
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L638:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L675
.L660:
	addl	$66, %eax
	movl	$37, %esi
	jmp	.L1085
.L615:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L601:
	leaq	.LC17(%rip), %rdi
	jmp	.L573
.L607:
	leaq	.LC10(%rip), %rdi
	jmp	.L573
.L620:
	leaq	.LC4(%rip), %rdi
	jmp	.L573
.L599:
	leaq	.LC13(%rip), %rdi
	jmp	.L573
.L568:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L569:
	leaq	.LC23(%rip), %rdi
	jmp	.L573
.L571:
	leaq	.LC30(%rip), %rdi
	jmp	.L573
.L597:
	leaq	.LC6(%rip), %rdi
	jmp	.L573
.L587:
	leaq	.LC20(%rip), %rdi
	jmp	.L573
.L586:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L594:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rax), %esi
	leaq	(%rdi,%rsi,2), %rdi
	jmp	.L626
.L1176:
	leaq	.LC33(%rip), %rdi
	jmp	.L573
.L578:
	leaq	.LC27(%rip), %rdi
	jmp	.L573
.L1736:
	movzbl	3(%rax), %eax
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L418
	imull	$94, %edx, %edx
	addl	%edx, %eax
	cmpl	$7649, %eax
	jg	.L418
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %r8d
	testw	%r8w, %r8w
	je	.L418
	movq	120(%rsp), %rax
	cmpl	$65533, %r8d
	movq	%rax, 80(%rsp)
	jne	.L419
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L1770:
	cmpl	$9632, %ecx
	je	.L826
	jbe	.L1806
	cmpl	$9650, %ecx
	je	.L829
	cmpl	$9651, %ecx
	je	.L830
	cmpl	$9633, %ecx
	jne	.L856
	leaq	.LC16(%rip), %rax
	jmp	.L798
.L1728:
	orl	%ecx, %edx
	movq	176(%rsp), %rax
	movq	184(%rsp), %rbx
	sall	$3, %edx
	movl	$5, 8(%rsp)
	jmp	.L440
.L1133:
	movq	8(%rsp), %rax
	jmp	.L1667
.L28:
	cmpl	$27, %r9d
	leaq	(%rdx,%rcx), %r14
	je	.L1807
	cmpl	$14, %r9d
	je	.L1808
	cmpl	$15, %r9d
	je	.L1126
.L50:
	movl	32(%rsp), %edi
	testl	%edi, %edi
	je	.L1131
	movq	%r14, %rsi
	subq	%rdx, %rsi
	cmpq	$1, %rsi
	jle	.L32
	cmpl	$1, %edi
	je	.L1809
	cmpl	$4, 32(%rsp)
	je	.L1810
	cmpl	$3, 32(%rsp)
	jne	.L1811
	subl	$33, %r9d
	cmpl	$92, %r9d
	jbe	.L1812
.L68:
	cmpq	$0, 112(%rsp)
	je	.L357
	andl	$2, %ebx
	movq	%rdx, %rdi
	je	.L357
.L1110:
	movq	112(%rsp), %rbx
	addq	$2, %rdi
	addq	$1, (%rbx)
.L30:
	subq	%rdx, %rdi
	cmpq	%r10, %rdi
	jle	.L1813
	movq	16(%rsp), %rbx
	subq	%r10, %rdi
	addq	%rdi, %rax
	movq	%rax, %r11
	movq	%rax, (%rbx)
	movl	52(%rsp), %eax
	jmp	.L1670
.L1719:
	movq	%rbp, %rdx
	movq	16(%rsp), %rbx
	subq	%rax, %rdx
	addq	%r10, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rbx)
	ja	.L81
	addq	$1, %rax
	cmpq	%r10, %rdx
	movq	24(%rsp), %rsi
	jbe	.L85
.L84:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%rsi,%r10)
	addq	$1, %r10
	cmpq	%r10, %rdx
	jne	.L84
.L85:
	movl	$7, %r14d
	jmp	.L7
.L1788:
	leal	-13312(%rcx), %eax
	leaq	(%rax,%rax,2), %rax
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rax
	cmpb	$2, (%rax)
	je	.L905
.L1231:
	movl	$1, %r12d
.L906:
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %ecx
	jbe	.L1014
.L1015:
	addq	$8, %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %ecx
	ja	.L1015
.L1014:
	movzwl	(%rax), %edi
	cmpl	%edi, %ecx
	jb	.L1016
	movl	4(%rax), %edi
	addl	%ecx, %edi
	movl	%edi, %eax
	movq	__isoir165_from_tab@GOTPCREL(%rip), %rdi
	addl	%eax, %eax
	movzbl	(%rdi,%rax), %r9d
	testb	%r9b, %r9b
	je	.L1016
	movzbl	1(%rax,%rdi), %ecx
	movl	$4, %r12d
	jmp	.L972
.L1765:
	cmpl	$275, %ecx
	je	.L1206
	jbe	.L1814
	cmpl	$283, %ecx
	je	.L801
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L1791:
	cmpl	$19968, %ecx
	jnb	.L840
	cmpl	$12585, %ecx
	ja	.L841
	cmpl	$12288, %ecx
	jnb	.L842
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L650:
	cmpl	$8895, %eax
	jne	.L670
	leaq	.LC38(%rip), %rax
	jmp	.L641
.L1057:
	cmpl	$24, %r12d
	jne	.L1055
.L1058:
	movq	200(%rsp), %rdx
	leaq	2(%rdx), %rax
	cmpq	%rax, %r11
	jb	.L1815
	leaq	1(%rdx), %rax
	movl	$24, %r12d
	movq	%rax, 200(%rsp)
	movb	$27, (%rdx)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$78, (%rax)
	movq	200(%rsp), %rdi
	jmp	.L1061
.L642:
	cmpl	$8453, %eax
	jne	.L670
	leaq	.LC41(%rip), %rax
	jmp	.L641
.L659:
	cmpl	$12963, %eax
	jne	.L670
	leaq	.LC37(%rip), %rax
	jmp	.L641
.L775:
	movq	%rbp, %rcx
	subq	%r12, %rcx
	cmpq	$1, %rcx
	jle	.L1200
	cmpl	$1, 40(%rsp)
	je	.L1816
	cmpl	$4, 40(%rsp)
	je	.L1817
	cmpl	$3, 40(%rsp)
	jne	.L780
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1818
.L777:
	testq	%r8, %r8
	je	.L1202
	movl	8(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L1202
.L1675:
	addq	$2, %r12
	addq	$1, (%r8)
	movl	$6, 52(%rsp)
	jmp	.L734
.L635:
	leaq	.LC40(%rip), %rax
	jmp	.L641
.L866:
	cmpl	$8601, %ecx
	ja	.L875
	cmpl	$8592, %ecx
	jnb	.L876
	cmpl	$8544, %ecx
	jb	.L862
	cmpl	$8553, %ecx
	ja	.L1819
	subl	$53, %ecx
	movl	$36, %r9d
.L1088:
	cmpl	$3, %esi
	movl	$768, 8(%rsp)
	movl	$3, %r12d
	jne	.L903
	jmp	.L1676
.L96:
	cmpl	$9792, %edx
	je	.L126
	ja	.L127
	cmpl	$9670, %edx
	je	.L128
	jbe	.L1820
	cmpl	$9678, %edx
	je	.L136
	jbe	.L1821
	cmpl	$9733, %edx
	je	.L140
	cmpl	$9734, %edx
	jne	.L1822
	leaq	.LC7(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1177:
	leaq	.LC42(%rip), %rax
	jmp	.L641
.L810:
	cmpl	$8869, %ecx
	ja	.L818
	cmpl	$8451, %ecx
	jnb	.L819
	leal	-8213(%rcx), %eax
	cmpl	$38, %eax
	ja	.L856
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L423:
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L420
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L420
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$6589, %edx
	jg	.L420
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movl	(%rdi,%rdx,4), %r8d
	testl	%r8d, %r8d
	jne	.L427
	jmp	.L420
.L421:
	leal	-33(%rdx), %r8d
	cmpl	$93, %r8d
	ja	.L420
	leal	-33(%rdi), %r8d
	cmpl	$93, %r8d
	ja	.L420
	subl	$33, %edx
	imull	$94, %edx, %edx
	leal	-33(%rdx,%rdi), %edx
	cmpl	$8602, %edx
	jg	.L420
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movl	(%rdi,%rdx,4), %r8d
	testl	%r8d, %r8d
	jne	.L427
	jmp	.L420
.L1777:
	movl	32(%rsp), %ecx
	leaq	1(%r12), %rax
	andl	$1792, %ecx
	je	.L758
	cmpl	$512, %ecx
	je	.L1195
	jle	.L1823
	cmpl	$768, %ecx
	jne	.L1824
	movq	%rax, %r12
	movl	$3, 40(%rsp)
	jmp	.L734
.L425:
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L420
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L420
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$6387, %edx
	jg	.L420
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movl	(%rdi,%rdx,4), %r8d
	testl	%r8d, %r8d
	jne	.L427
	jmp	.L420
.L883:
	cmpl	$65373, %ecx
	jbe	.L1825
	cmpl	$65505, %ecx
	je	.L897
	cmpl	$65509, %ecx
	je	.L898
	cmpl	$65504, %ecx
	je	.L1826
.L862:
	movl	$3, %r12d
	jmp	.L858
.L1758:
	movq	176(%rsp), %rax
	leal	0(,%rcx,8), %edx
	movq	%r8, %rbx
	movl	$5, 8(%rsp)
	jmp	.L440
.L941:
	cmpl	$40864, %ecx
	jbe	.L1827
	cmpl	$65504, %ecx
	je	.L963
	jbe	.L1828
	cmpl	$65507, %ecx
	je	.L966
	cmpl	$65509, %ecx
	je	.L967
	cmpl	$65505, %ecx
	jne	.L973
	leaq	.LC3(%rip), %rax
	jmp	.L916
.L913:
	cmpl	$1105, %ecx
	ja	.L928
	cmpl	$1025, %ecx
	jnb	.L929
	cmpl	$711, %ecx
	je	.L930
	jbe	.L1829
	cmpl	$713, %ecx
	je	.L934
	jb	.L973
	leal	-913(%rcx), %eax
	cmpl	$56, %eax
	ja	.L973
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
.L969:
	movzbl	(%rax), %r9d
	testb	%r9b, %r9b
	je	.L973
	movzbl	1(%rax), %ecx
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1734:
	movl	%ecx, %edx
	movq	176(%rsp), %rax
	movq	184(%rsp), %rbx
	movl	$6, 8(%rsp)
	jmp	.L440
.L1761:
	movzbl	1(%rdi), %ecx
	jmp	.L1088
.L1786:
	cmpl	$474, %ecx
	jne	.L1830
	leaq	.LC22(%rip), %rax
	jmp	.L798
.L15:
	movq	56(%rsp), %r14
	movq	32(%r13), %rdx
	movq	%r14, %rdi
	movl	$0, (%rdx)
	movq	%rax, 200(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	leaq	200(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbp, %rcx
	pushq	%rax
	pushq	$0
	movq	104(%rsp), %r9
	movq	120(%rsp), %rsi
	movq	112(%rsp), %rdi
	call	*%r14
	movl	%eax, %r14d
	cmpl	$4, %r14d
	popq	%rax
	popq	%rdx
	je	.L16
	cmpq	%rbp, 200(%rsp)
	jne	.L1831
.L17:
	testl	%r14d, %r14d
	jne	.L7
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L1762:
	cmpq	$-1, %rax
	je	.L907
.L1115:
	cmpl	$24, %esi
	movzbl	1(%rax), %r9d
	movzbl	2(%rax), %ecx
	je	.L1832
	movl	%r8d, %eax
	andl	$6144, %eax
	cmpl	$6144, %eax
	je	.L1058
	leaq	1(%rdx), %rax
	orl	$6144, %r8d
	movq	%rax, 200(%rsp)
	movb	$27, (%rdx)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$36, (%rax)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$42, (%rax)
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movb	$72, (%rax)
	jmp	.L1058
.L1800:
	cmpl	$9216, %ecx
	jnb	.L977
	cmpl	$8457, %ecx
	je	.L978
	ja	.L979
	cmpl	$969, %ecx
	ja	.L980
	cmpl	$913, %ecx
	jnb	.L981
	cmpl	$167, %ecx
	jb	.L1013
	cmpl	$247, %ecx
	jbe	.L982
	leal	-711(%rcx), %eax
	cmpl	$18, %eax
	ja	.L1013
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L1027:
	cmpl	$65373, %ecx
	jbe	.L1034
	cmpl	$131072, %ecx
	jb	.L1034
	cmpl	$173782, %ecx
	jbe	.L1032
	leal	-194560(%rcx), %eax
	cmpl	$541, %eax
	ja	.L1034
	leaq	(%rax,%rax,2), %rax
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %rax
	movzbl	(%rax), %edi
	testb	%dil, %dil
	je	.L1034
.L1648:
	subl	$3, %edi
	movzbl	1(%rax), %r9d
	movzbl	2(%rax), %eax
	cmpb	$4, %dil
	ja	.L1034
	leaq	.L1039(%rip), %r12
	movzbl	%dil, %edi
	movl	%eax, %ecx
	movslq	(%r12,%rdi,4), %rax
	addq	%r12, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1039:
	.long	.L1038-.L1039
	.long	.L1040-.L1039
	.long	.L1041-.L1039
	.long	.L1042-.L1039
	.long	.L1043-.L1039
	.text
.L1771:
	cmpl	$9671, %ecx
	je	.L834
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L1766:
	cmpl	$363, %ecx
	je	.L805
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L1210:
	movl	$4, %r12d
	jmp	.L858
.L1780:
	andl	$-1793, %r11d
	orl	$768, %r11d
	jmp	.L404
.L1049:
	movq	%r11, 40(%rsp)
	movl	%r10d, 32(%rsp)
	leaq	192(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r13, %rsi
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	movq	88(%rsp), %rdi
	leaq	216(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movslq	%eax, %rbx
	movq	40(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, %r8d
	movl	%eax, 24(%rsp)
	sarl	$3, %r8d
	popq	%rdi
	movzbl	%r8b, %esi
	xorb	%r8b, %r8b
	cmpl	$6, %ebx
	popq	%r9
	movl	32(%rsp), %r10d
	movq	40(%rsp), %r11
	je	.L1050
	cmpl	$5, %ebx
	movq	192(%rsp), %r12
	jne	.L1047
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$5, %ebx
	movq	200(%rsp), %rdx
	jmp	.L784
.L1016:
	cmpl	$3, %r12d
	je	.L1013
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L974:
	cmpl	$65509, %ecx
	movl	$3, %r12d
	ja	.L1013
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1794:
	leaq	3(%r12), %rdi
	cmpq	%rdi, %rbp
	jb	.L1200
	movzbl	2(%r12), %edi
	cmpb	$41, %dil
	je	.L1833
	cmpb	$42, %dil
	jne	.L739
	leaq	4(%r12), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 80(%rsp)
	jb	.L1200
	cmpb	$72, 3(%r12)
	jne	.L741
.L747:
	orl	$6144, 32(%rsp)
.L749:
	movq	80(%rsp), %r12
	jmp	.L734
.L1807:
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r14
	jnb	.L1834
.L32:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	je	.L1835
	movq	%rcx, %rsi
	movq	16(%rsp), %rbx
	subq	%r10, %rsi
	addq	%rsi, %rax
	movq	%rax, (%rbx)
	movl	52(%rsp), %eax
	andl	$-8, %eax
	movslq	%eax, %rsi
	cmpq	%rsi, %rcx
	jle	.L1836
	cmpq	$4, %rcx
	ja	.L1837
	movq	24(%rsp), %rbx
	orl	%ecx, %eax
	movzbl	40(%rsp), %esi
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L85
.L76:
	movq	24(%rsp), %rbx
	movb	%sil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L85
	movzbl	(%rdx,%rax), %esi
	jmp	.L76
.L867:
	cmpl	$8451, %ecx
	je	.L1211
	ja	.L872
	leal	-8211(%rcx), %edi
	cmpl	$43, %edi
	ja	.L862
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L758:
	testq	%r8, %r8
	je	.L1197
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L1675
.L1197:
	movq	%rax, %r12
	movl	$6, %ebx
	jmp	.L731
.L885:
	cmpl	$12585, %ecx
	ja	.L889
	cmpl	$12549, %ecx
	jnb	.L890
	cmpl	$12539, %ecx
	leaq	.LC20(%rip), %rdi
	jne	.L862
.L871:
	movzbl	(%rdi), %r9d
	movzbl	1(%rdi), %ecx
	jmp	.L1088
.L1825:
	cmpl	$65281, %ecx
	jnb	.L894
	cmpl	$19968, %ecx
	jb	.L862
	cmpl	$40860, %ecx
	ja	.L1838
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %r9
	leal	-19968(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L99:
	cmpl	$1105, %edx
	ja	.L114
	cmpl	$1025, %edx
	jnb	.L115
	cmpl	$711, %edx
	je	.L116
	ja	.L117
	cmpl	$474, %edx
	jne	.L1839
	leaq	.LC22(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L875:
	cmpl	$8869, %ecx
	je	.L879
	ja	.L880
	leal	-8725(%rcx), %edi
	cmpl	$82, %edi
	ja	.L862
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L905:
	cmpq	$-1, %rax
	jne	.L1115
	jmp	.L1231
.L841:
	leal	-12832(%rcx), %eax
	cmpl	$9, %eax
	ja	.L856
	addl	$69, %ecx
	movl	$34, %r9d
	jmp	.L1106
.L743:
	cmpb	$79, %r9b
	jne	.L741
	leaq	4(%r12), %rax
	cmpq	%rax, %rbp
	movq	%rax, 80(%rsp)
	jb	.L1200
	movl	32(%rsp), %esi
	movzbl	2(%r12), %eax
	movzbl	3(%r12), %ecx
	andl	$57344, %esi
	cmpl	$40960, %esi
	je	.L766
	jg	.L767
	cmpl	$24576, %esi
	je	.L768
	cmpl	$32768, %esi
	jne	.L765
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L765
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L765
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$7297, %eax
	jg	.L765
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.L765
.L772:
	cmpl	$65533, %ecx
	je	.L765
	movq	80(%rsp), %r12
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L1827:
	cmpl	$19968, %ecx
	jnb	.L958
	cmpl	$12585, %ecx
	ja	.L959
	cmpl	$12288, %ecx
	jnb	.L960
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rax
	je	.L916
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L160:
	movq	8(%rsp), %rax
	addq	$2, %rax
	cmpq	%rax, %r15
	jb	.L1120
.L167:
	movq	168(%rsp), %rsi
	jmp	.L375
.L1778:
	addq	$1, %r12
	movl	$0, 40(%rsp)
	jmp	.L734
.L1830:
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L372:
	movl	32(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L371
	leaq	1(%rsi), %rax
	cmpq	%rax, %r15
	jb	.L91
	movq	%rax, 168(%rsp)
	movb	$14, (%rsi)
	movq	168(%rsp), %rsi
	jmp	.L371
.L1781:
	andl	$-1793, %r11d
	orl	$1024, %r11d
	jmp	.L404
.L1801:
	cmpl	$9632, %ecx
	je	.L944
	jbe	.L1840
	cmpl	$9650, %ecx
	je	.L947
	cmpl	$9651, %ecx
	je	.L948
	cmpl	$9633, %ecx
	jne	.L973
	leaq	.LC16(%rip), %rax
	jmp	.L916
.L127:
	cmpl	$40864, %edx
	jbe	.L1841
	cmpl	$65504, %edx
	je	.L149
	jbe	.L1842
	cmpl	$65507, %edx
	je	.L152
	cmpl	$65509, %edx
	jne	.L1843
	leaq	.LC1(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1820:
	cmpl	$9632, %edx
	je	.L130
	jbe	.L1844
	cmpl	$9650, %edx
	je	.L133
	cmpl	$9651, %edx
	jne	.L1845
	leaq	.LC14(%rip), %rsi
	jmp	.L102
.L380:
	testl	%r14d, %r14d
	jne	.L7
.L1669:
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, %r11
	movq	24(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 52(%rsp)
	movl	16(%r13), %eax
	movl	%eax, 80(%rsp)
	jmp	.L21
.L832:
	leaq	.LC10(%rip), %rax
	jmp	.L798
.L812:
	leaq	.LC20(%rip), %rax
	jmp	.L798
.L811:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L1816:
	subl	$33, %eax
	cmpl	$86, %eax
	ja	.L777
	movzbl	1(%r12), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L777
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$8177, %eax
	jg	.L777
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rcx
	cltq
	leaq	2(%r12), %rsi
	movzwl	(%rcx,%rax,2), %ecx
	testw	%cx, %cx
	je	.L777
.L1679:
	movq	%rsi, %r12
.L778:
	cmpl	$65533, %ecx
	jne	.L764
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	112(%rsp), %rbx
	leaq	1(%rdx), %rdi
	addq	$1, (%rbx)
	jmp	.L30
.L830:
	leaq	.LC14(%rip), %rax
	jmp	.L798
.L829:
	leaq	.LC15(%rip), %rax
	jmp	.L798
.L1806:
	leal	-9472(%rcx), %eax
	cmpl	$75, %eax
	ja	.L856
	addl	$36, %ecx
	movl	$41, %r9d
	jmp	.L1106
.L826:
	leaq	.LC17(%rip), %rax
	jmp	.L798
.L845:
	leaq	.LC4(%rip), %rax
	jmp	.L798
.L801:
	leaq	.LC32(%rip), %rax
	jmp	.L798
.L1814:
	leal	-164(%rcx), %eax
	cmpl	$93, %eax
	ja	.L856
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L1206:
	leaq	.LC33(%rip), %rax
	jmp	.L798
.L1138:
	movl	$4, %esi
.L163:
	leal	-19975(%rdx), %ecx
	cmpl	$20893, %ecx
	ja	.L213
	leal	-13312(%rdx), %ecx
	leaq	(%rcx,%rcx,2), %rdi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	cmpb	$2, (%rdi)
	je	.L1846
.L213:
	cmpl	$9371, %edx
	ja	.L217
	cmpl	$9312, %edx
	jnb	.L218
	cmpl	$472, %edx
	je	.L219
	ja	.L220
	cmpl	$333, %edx
	je	.L221
	jbe	.L1847
	cmpl	$464, %edx
	je	.L228
	jbe	.L1848
	cmpl	$468, %edx
	je	.L232
	cmpl	$470, %edx
	jne	.L1849
	leaq	.LC24(%rip), %rdi
.L223:
	movzbl	(%rdi), %ecx
.L1079:
	movzbl	1(%rdi), %edx
	testb	%dl, %dl
	je	.L279
	movl	$1, %eax
.L280:
	cmpl	%eax, 32(%rsp)
	je	.L215
	movl	%r11d, %edi
	movl	%eax, %esi
	andl	$1792, %edi
	sall	$8, %esi
	cmpl	%esi, %edi
	jne	.L1099
.L361:
	testb	$-32, %al
	je	.L366
	movl	%eax, %esi
	andl	$57344, %r11d
	sall	$8, %esi
	cmpl	%esi, %r11d
	je	.L364
	movl	%eax, %esi
	sarl	$5, %esi
	subl	$3, %esi
	cmpl	$4, %esi
	ja	.L1850
.L368:
	addl	%esi, %esi
	leaq	.LC58(%rip), %rdi
	movslq	%esi, %rsi
	jmp	.L1666
.L350:
	cmpl	$192, 32(%rsp)
	movl	$192, %eax
	jne	.L352
.L215:
	movq	8(%rsp), %rax
	addq	$2, %rax
	cmpq	%rax, %r15
	jb	.L91
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	$9792, %edx
	je	.L247
	ja	.L248
	cmpl	$9670, %edx
	je	.L249
	jbe	.L1851
	cmpl	$9678, %edx
	je	.L257
	jbe	.L1852
	cmpl	$9733, %edx
	je	.L261
	cmpl	$9734, %edx
	jne	.L1853
	leaq	.LC7(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L169:
	cmpl	$13269, %edx
	ja	.L189
	cmpl	$13198, %edx
	jnb	.L190
	cmpl	$12329, %edx
	ja	.L191
	cmpl	$12288, %edx
	jnb	.L192
	cmpl	$9312, %edx
	jb	.L168
	cmpl	$9341, %edx
	ja	.L1854
	leal	-9312(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L796:
	leaq	.LC30(%rip), %rax
	jmp	.L798
.L805:
	leaq	.LC29(%rip), %rax
	jmp	.L798
.L767:
	cmpl	$49152, %esi
	je	.L770
	cmpl	$57344, %esi
	jne	.L765
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L765
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L765
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$6538, %eax
	jg	.L765
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L772
.L765:
	testq	%r8, %r8
	je	.L1202
	movl	8(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L1202
	movq	80(%rsp), %r12
	addq	$1, (%r8)
	movl	$6, 52(%rsp)
	jmp	.L734
.L186:
	cmpl	$8895, %edx
	leaq	.LC38(%rip), %rdi
	je	.L177
.L168:
	movl	$3, %esi
	jmp	.L163
.L928:
	cmpl	$8869, %ecx
	ja	.L936
	cmpl	$8451, %ecx
	jnb	.L937
	leal	-8213(%rcx), %eax
	cmpl	$38, %eax
	ja	.L973
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L996:
	cmpl	$65373, %ecx
	ja	.L1006
	cmpl	$65281, %ecx
	jnb	.L1007
	cmpl	$19968, %ecx
	jb	.L1013
	cmpl	$40860, %ecx
	jbe	.L1008
	leal	-65072(%rcx), %eax
	cmpl	$59, %eax
	ja	.L1013
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L1832:
	movq	200(%rsp), %rdi
	movl	$24, %r12d
	jmp	.L861
.L840:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L1808:
	andl	$1792, %r11d
	je	.L46
	cmpl	$512, %r11d
	je	.L1126
	jle	.L1855
	cmpl	$768, %r11d
	je	.L1126
	cmpl	$1024, %r11d
	jne	.L47
.L1126:
	leaq	1(%rdx), %rdi
	jmp	.L30
.L808:
	leaq	.LC24(%rip), %rax
	jmp	.L798
.L807:
	leaq	.LC25(%rip), %rax
	jmp	.L798
.L816:
	leaq	.LC19(%rip), %rax
	jmp	.L798
.L842:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L803:
	leaq	.LC27(%rip), %rax
	jmp	.L798
.L834:
	leaq	.LC12(%rip), %rax
	jmp	.L798
.L822:
	leaq	.LC6(%rip), %rax
	jmp	.L798
.L849:
	leaq	.LC1(%rip), %rax
	jmp	.L798
.L824:
	leaq	.LC13(%rip), %rax
	jmp	.L798
.L837:
	leaq	.LC7(%rip), %rax
	jmp	.L798
.L836:
	leaq	.LC8(%rip), %rax
	jmp	.L798
.L794:
	leaq	.LC23(%rip), %rax
	jmp	.L798
.L793:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L819:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L818:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rax
	je	.L798
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L1789:
	cmpl	$13312, %ecx
	jb	.L1034
	cmpl	$19967, %ecx
	jbe	.L1028
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	cmpb	$0, (%rdi,%rax,2)
	jne	.L1034
.L1028:
	leal	-13312(%rcx), %eax
	leaq	(%rax,%rax,2), %rax
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rax
	movzbl	(%rax), %edi
	testb	%dil, %dil
	je	.L1034
	jmp	.L1648
.L848:
	leaq	.LC2(%rip), %rax
	jmp	.L798
.L1792:
	leal	-65281(%rcx), %eax
	cmpl	$93, %eax
	ja	.L856
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L851
.L979:
	cmpl	$8601, %ecx
	ja	.L988
	cmpl	$8592, %ecx
	jnb	.L989
	cmpl	$8544, %ecx
	jb	.L1013
	cmpl	$8553, %ecx
	jbe	.L990
	leal	-8560(%rcx), %eax
	cmpl	$9, %eax
	ja	.L1013
	subl	$59, %ecx
	movl	$38, %r9d
	jmp	.L1090
.L980:
	cmpl	$8451, %ecx
	je	.L1214
	ja	.L985
	leal	-8211(%rcx), %eax
	cmpl	$43, %eax
	ja	.L1013
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L988:
	cmpl	$8869, %ecx
	je	.L992
	ja	.L993
	leal	-8725(%rcx), %eax
	cmpl	$82, %eax
	ja	.L1013
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L172:
	cmpl	$8601, %edx
	ja	.L181
	cmpl	$8592, %edx
	jnb	.L182
	cmpl	$8544, %edx
	jb	.L168
	cmpl	$8553, %edx
	ja	.L1856
	subl	$53, %edx
	movb	$36, 200(%rsp)
	movb	%dl, 201(%rsp)
.L207:
	movzbl	200(%rsp), %ecx
	leaq	200(%rsp), %rdi
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1803:
	cmpl	$275, %edx
	je	.L1134
	jbe	.L1857
	cmpl	$283, %edx
	jne	.L1858
	leaq	.LC32(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1745:
	movl	%r8d, 32(%rsp)
	movq	192(%rsp), %r12
	movl	$5, %ebx
	movl	$0, 40(%rsp)
	jmp	.L784
.L880:
	cmpl	$8895, %ecx
	leaq	.LC38(%rip), %rdi
	je	.L871
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1793:
	leal	-9472(%rcx), %edi
	cmpl	$322, %edi
	ja	.L862
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1051:
	movq	112(%rsp), %rax
	addq	$4, %r12
	movl	$6, %ebx
	movq	%r12, 192(%rsp)
	addq	$1, (%rax)
	jmp	.L1047
.L1838:
	leal	-65072(%rcx), %edi
	cmpl	$59, %edi
	ja	.L862
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1824:
	cmpl	$1024, %ecx
	jne	.L47
	movq	%rax, %r12
	movl	$4, 40(%rsp)
	jmp	.L734
.L872:
	cmpl	$8453, %ecx
	leaq	.LC41(%rip), %rdi
	je	.L871
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L366:
	cmpl	$24, %eax
	jne	.L364
.L367:
	movq	168(%rsp), %rax
	leaq	2(%rax), %rsi
	cmpq	%rsi, %r15
	jb	.L91
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$27, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$78, (%rax)
	movq	168(%rsp), %rsi
	jmp	.L371
.L998:
	cmpl	$12585, %ecx
	ja	.L1002
	cmpl	$12549, %ecx
	jnb	.L1003
	cmpl	$12539, %ecx
	jne	.L1013
	leaq	.LC20(%rip), %rax
.L984:
	movzbl	(%rax), %r9d
	movzbl	1(%rax), %ecx
	jmp	.L1090
.L1131:
	leaq	1(%rdx), %rdi
.L65:
	movq	8(%rsp), %rbx
	movq	%r12, 8(%rsp)
	movl	%r8d, (%rbx)
	movq	24(%rsp), %rbx
	movl	(%rbx), %ebx
	movl	%ebx, %r10d
	movl	%ebx, 52(%rsp)
	andl	$7, %r10d
	jmp	.L30
.L1829:
	cmpl	$474, %ecx
	je	.L932
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rax
	je	.L916
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1006:
	cmpl	$65505, %ecx
	je	.L1010
	cmpl	$65509, %ecx
	je	.L1011
	cmpl	$65504, %ecx
	jne	.L1013
	leaq	.LC36(%rip), %rax
	jmp	.L984
.L1833:
	leaq	4(%r12), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 80(%rsp)
	jb	.L1200
	movzbl	3(%r12), %edi
	movl	%edi, %r9d
	andl	$-5, %r9d
	cmpb	$65, %r9b
	je	.L746
	cmpb	$71, %dil
	jne	.L741
.L746:
	cmpb	$65, %dil
	je	.L1859
.L748:
	cmpb	$71, %dil
	je	.L1860
	cmpb	$69, %dil
	je	.L1861
	cmpb	$72, %dil
	je	.L747
	cmpb	$73, %dil
	je	.L1862
	cmpb	$74, %dil
	je	.L1863
	cmpb	$75, %dil
	je	.L1864
	cmpb	$76, %dil
	je	.L1865
	cmpb	$77, %dil
	jne	.L749
	orl	$57344, 32(%rsp)
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L1817:
	subl	$33, %esi
	cmpb	$93, %sil
	ja	.L777
	movzbl	1(%r12), %ecx
	leal	-33(%rcx), %esi
	cmpb	$93, %sil
	ja	.L777
	subl	$33, %eax
	imull	$94, %eax, %eax
	leal	-33(%rax,%rcx), %eax
	movq	__isoir165_to_tab@GOTPCREL(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %ecx
	testl	%ecx, %ecx
	je	.L777
	addq	$2, %r12
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L1819:
	leal	-8560(%rcx), %edi
	cmpl	$9, %edi
	ja	.L862
	subl	$59, %ecx
	movl	$38, %r9d
	jmp	.L1088
.L114:
	cmpl	$8869, %edx
	ja	.L122
	cmpl	$8451, %edx
	jnb	.L123
	leal	-8213(%rdx), %esi
	cmpl	$38, %esi
	ja	.L161
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rsi
.L155:
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	jne	.L1077
.L161:
	leal	-19975(%rdx), %ecx
	cmpl	$20893, %ecx
	ja	.L211
	leal	-13312(%rdx), %ecx
	leaq	(%rcx,%rcx,2), %rdi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	cmpb	$2, (%rdi)
	je	.L1111
.L1665:
	movl	$1, %esi
.L1098:
	movq	__isoir165_from_idx@GOTPCREL(%rip), %rcx
	movzwl	2(%rcx), %edi
	cmpl	%edi, %edx
	jbe	.L322
.L323:
	addq	$8, %rcx
	movzwl	2(%rcx), %edi
	cmpl	%edi, %edx
	ja	.L323
.L322:
	movzwl	(%rcx), %edi
	cmpl	%edi, %edx
	jb	.L321
	movl	4(%rcx), %edi
	movq	__isoir165_from_tab@GOTPCREL(%rip), %r8
	addl	%edx, %edi
	addl	%edi, %edi
	movzbl	(%r8,%rdi), %ecx
	testb	%cl, %cl
	je	.L321
	movzbl	1(%rdi,%r8), %edx
	movl	$4, %eax
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L1764:
	cmpl	$363, %ecx
	je	.L923
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rax
	je	.L916
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1782:
	andl	$-57345, %r11d
	orl	$24576, %r11d
	jmp	.L404
.L1841:
	cmpl	$19968, %edx
	jnb	.L144
	cmpl	$12585, %edx
	ja	.L145
	cmpl	$12288, %edx
	jnb	.L146
	cmpl	$9794, %edx
	leaq	.LC5(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L220:
	cmpl	$1105, %edx
	ja	.L235
	cmpl	$1025, %edx
	jnb	.L236
	cmpl	$711, %edx
	je	.L237
	jbe	.L1866
	cmpl	$713, %edx
	je	.L241
	jb	.L281
	leal	-913(%rdx), %edi
	cmpl	$56, %edi
	ja	.L281
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
.L276:
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	jne	.L1079
.L281:
	cmpl	$4, %esi
	je	.L324
	cmpl	$65509, %edx
	jbe	.L1098
.L321:
	cmpl	$3, %esi
	jne	.L324
.L325:
	cmpl	$9794, %edx
	jbe	.L342
	cmpl	$40869, %edx
	ja	.L335
	cmpl	$40861, %edx
	jnb	.L336
	cmpl	$13269, %edx
	ja	.L1867
.L342:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L1868
	cmpq	$0, 112(%rsp)
	je	.L1869
	movl	52(%rsp), %eax
	movq	24(%rsp), %rcx
	andl	$-8, %eax
	movl	%eax, (%rcx)
	testb	$8, 16(%r13)
	jne	.L1870
.L358:
	andl	$2, %ebx
	movq	160(%rsp), %rdx
	jne	.L360
.L1668:
	cmpq	%r12, %rdx
	jne	.L354
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L324:
	cmpl	$9249, %edx
	ja	.L284
	cmpl	$9216, %edx
	jnb	.L285
	cmpl	$8457, %edx
	je	.L286
	ja	.L287
	cmpl	$969, %edx
	ja	.L288
	cmpl	$913, %edx
	jnb	.L289
	cmpl	$167, %edx
	jb	.L325
	cmpl	$247, %edx
	ja	.L1871
	leal	-167(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rsi
.L326:
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	je	.L325
.L1080:
	movzbl	1(%rsi), %edx
	movl	$3, %eax
	jmp	.L280
.L1834:
	movzbl	137(%rsp), %esi
	cmpb	$36, %sil
	je	.L1872
	cmpb	$78, %sil
	je	.L1873
	cmpb	$79, %sil
	jne	.L50
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
	andl	$57344, %r11d
	movzbl	138(%rsp), %esi
	movzbl	139(%rsp), %ecx
	cmpl	$40960, %r11d
	je	.L56
	jle	.L1874
	cmpl	$49152, %r11d
	je	.L60
	cmpl	$57344, %r11d
	jne	.L55
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L55
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L55
	imull	$94, %esi, %esi
	addl	%esi, %ecx
	cmpl	$6538, %ecx
	jg	.L55
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %r8d
	testl	%r8d, %r8d
	je	.L55
.L62:
	cmpl	$65533, %r8d
	leaq	4(%rdx), %rdi
	jne	.L65
.L55:
	cmpq	$0, 112(%rsp)
	je	.L357
	andb	$2, %bl
	je	.L357
	movq	112(%rsp), %rbx
	addq	$1, (%rbx)
.L1658:
	leaq	4(%rdx), %rdi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L1760:
	leal	-711(%rcx), %edi
	cmpl	$18, %edi
	ja	.L862
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1802:
	cmpl	$9671, %ecx
	je	.L952
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rax
	je	.L916
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L889:
	cmpl	$12963, %ecx
	leaq	.LC37(%rip), %rdi
	je	.L871
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1873:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
	movzbl	138(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$92, %ecx
	jbe	.L1875
.L51:
	cmpq	$0, 112(%rsp)
	je	.L30
	andb	$2, %bl
	je	.L30
	movq	112(%rsp), %rbx
	addq	$2, %rdi
	addq	$1, (%rbx)
	cmpq	%rdx, %rdi
	jne	.L30
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L1872:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
	movzbl	138(%rsp), %edi
	cmpb	$41, %dil
	je	.L1876
	cmpb	$42, %dil
	je	.L1877
	cmpb	$43, %dil
	jne	.L39
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
.L39:
	cmpb	$43, %dil
	jne	.L50
	movzbl	139(%rsp), %edi
	leal	-73(%rdi), %esi
	cmpb	$4, %sil
	ja	.L50
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1874:
	cmpl	$24576, %r11d
	je	.L58
	cmpl	$32768, %r11d
	jne	.L55
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L55
	leal	-33(%rcx), %edi
	cmpl	$93, %edi
	ja	.L55
	imull	$94, %esi, %ecx
	addl	%edi, %ecx
	cmpl	$7297, %ecx
	jg	.L55
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %r8d
	testl	%r8d, %r8d
	jne	.L62
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L335:
	cmpl	$65373, %edx
	jbe	.L342
	cmpl	$131072, %edx
	jb	.L342
	cmpl	$173782, %edx
	jbe	.L340
	leal	-194560(%rdx), %ecx
	cmpl	$541, %ecx
	ja	.L342
	movl	%ecx, %edi
	leaq	(%rdi,%rdi,2), %rdi
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L342
.L1577:
	subl	$3, %esi
	movzbl	1(%rdi), %ecx
	movzbl	2(%rdi), %edi
	cmpb	$4, %sil
	ja	.L342
	movl	%edi, %edx
	leaq	.L347(%rip), %rdi
	movzbl	%sil, %esi
	movslq	(%rdi,%rsi,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L347:
	.long	.L346-.L347
	.long	.L348-.L347
	.long	.L349-.L347
	.long	.L350-.L347
	.long	.L351-.L347
	.text
.L1111:
	cmpq	$-1, %rdi
	jne	.L1118
.L211:
	cmpl	$65509, %edx
	ja	.L325
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%r10, 40(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	pushq	120(%rsp)
	movq	32(%rsp), %rax
	leaq	(%r12,%r10), %r11
	movq	88(%rsp), %rdi
	movq	%r13, %rsi
	leaq	184(%rsp), %r9
	movq	%r11, %r8
	movq	%r11, 48(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r14d
	cmpl	$6, %r14d
	popq	%rax
	popq	%rdx
	je	.L358
	movq	160(%rsp), %rdx
	movq	32(%rsp), %r11
	movq	40(%rsp), %r10
	cmpq	%r12, %rdx
	jne	.L354
	cmpl	$7, %r14d
	jne	.L380
	leaq	4(%r12), %rax
	cmpq	%rax, %r11
	je	.L1878
	movq	24(%rsp), %rax
	movq	%r10, %rbx
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r10
	jle	.L1879
	cmpq	$4, %r10
	ja	.L1880
	movq	24(%rsp), %rbx
	orl	%r10d, %eax
	testq	%r10, %r10
	movl	%eax, (%rbx)
	je	.L85
	xorl	%eax, %eax
.L384:
	movzbl	(%r12,%rax), %edx
	movq	24(%rsp), %rbx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L384
	jmp	.L85
.L1869:
	movq	160(%rsp), %rdx
	cmpq	%r12, %rdx
	jne	.L354
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L1868:
	movq	160(%rsp), %rcx
	leaq	4(%rcx), %rdx
	cmpq	%r12, %rdx
	movq	%rdx, 160(%rsp)
	jne	.L354
	movq	%rax, %r11
	movq	24(%rsp), %rax
	movl	%ebx, 80(%rsp)
	movl	(%rax), %eax
	movl	%eax, 52(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L868:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %r9
	leal	-913(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L914:
	leaq	.LC30(%rip), %rax
	jmp	.L916
.L1843:
	cmpl	$65505, %edx
	jne	.L161
	leaq	.LC3(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L919:
	leaq	.LC32(%rip), %rax
	jmp	.L916
.L1799:
	leal	-164(%rcx), %eax
	cmpl	$93, %eax
	ja	.L973
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L1213:
	leaq	.LC33(%rip), %rax
	jmp	.L916
.L1805:
	cmpl	$466, %edx
	jne	.L161
	leaq	.LC26(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1822:
	cmpl	$9679, %edx
	jne	.L161
	leaq	.LC9(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1821:
	cmpl	$9671, %edx
	jne	.L1881
	leaq	.LC12(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L948:
	leaq	.LC14(%rip), %rax
	jmp	.L916
.L942:
	leaq	.LC13(%rip), %rax
	jmp	.L916
.L1871:
	leal	-711(%rdx), %esi
	cmpl	$18, %esi
	ja	.L325
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L288:
	cmpl	$8451, %edx
	je	.L1142
	ja	.L293
	leal	-8211(%rdx), %esi
	cmpl	$43, %esi
	ja	.L325
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L287:
	cmpl	$8601, %edx
	ja	.L296
	cmpl	$8592, %edx
	jnb	.L297
	cmpl	$8544, %edx
	jb	.L325
	cmpl	$8553, %edx
	ja	.L1882
	subl	$53, %edx
	movb	$36, 200(%rsp)
	movb	%dl, 201(%rsp)
.L327:
	movzbl	200(%rsp), %ecx
	leaq	200(%rsp), %rsi
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L284:
	cmpl	$13269, %edx
	ja	.L304
	cmpl	$13198, %edx
	jnb	.L305
	cmpl	$12329, %edx
	ja	.L306
	cmpl	$12288, %edx
	jnb	.L307
	cmpl	$9312, %edx
	jb	.L325
	cmpl	$9341, %edx
	ja	.L1883
	leal	-9312(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L293:
	cmpl	$8453, %edx
	jne	.L325
	leaq	.LC41(%rip), %rsi
.L292:
	movzbl	(%rsi), %ecx
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1882:
	leal	-8560(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L325
	subl	$59, %edx
	movb	$38, 200(%rsp)
	movb	%dl, 201(%rsp)
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L296:
	cmpl	$8869, %edx
	je	.L300
	ja	.L301
	leal	-8725(%rdx), %esi
	cmpl	$82, %esi
	ja	.L325
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L1883:
	leal	-9472(%rdx), %esi
	cmpl	$322, %esi
	ja	.L325
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rsi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L306:
	cmpl	$12585, %edx
	ja	.L310
	cmpl	$12549, %edx
	jnb	.L311
	cmpl	$12539, %edx
	jne	.L325
	leaq	.LC20(%rip), %rsi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L301:
	cmpl	$8895, %edx
	jne	.L325
	leaq	.LC38(%rip), %rsi
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L1785:
	andl	$-57345, %r11d
	orl	$49152, %r11d
	jmp	.L404
.L1784:
	andl	$-57345, %r11d
	orl	$40960, %r11d
	jmp	.L404
.L1826:
	leaq	.LC36(%rip), %rdi
	jmp	.L871
.L1783:
	andl	$-57345, %r11d
	orl	$32768, %r11d
	jmp	.L404
.L923:
	leaq	.LC29(%rip), %rax
	jmp	.L916
.L1195:
	movq	%rax, %r12
	movl	$2, 40(%rsp)
	jmp	.L734
.L950:
	leaq	.LC10(%rip), %rax
	jmp	.L916
.L955:
	leaq	.LC7(%rip), %rax
	jmp	.L916
.L954:
	leaq	.LC8(%rip), %rax
	jmp	.L916
.L898:
	leaq	.LC34(%rip), %rdi
	jmp	.L871
.L897:
	leaq	.LC35(%rip), %rdi
	jmp	.L871
.L890:
	addl	$66, %ecx
	movl	$37, %r9d
	jmp	.L1088
.L952:
	leaq	.LC12(%rip), %rax
	jmp	.L916
.L122:
	cmpl	$8978, %edx
	leaq	.LC18(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L117:
	cmpl	$713, %edx
	jne	.L1884
	leaq	.LC19(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L876:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %r9
	leal	-8592(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1840:
	leal	-9472(%rcx), %eax
	cmpl	$75, %eax
	ja	.L973
	addl	$36, %ecx
	movl	$41, %r9d
	jmp	.L1108
.L944:
	leaq	.LC17(%rip), %rax
	jmp	.L916
.L947:
	leaq	.LC15(%rip), %rax
	jmp	.L916
.L1881:
	cmpl	$9675, %edx
	leaq	.LC11(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L1842:
	leal	-65281(%rdx), %esi
	cmpl	$93, %esi
	ja	.L161
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rsi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L921:
	leaq	.LC27(%rip), %rax
	jmp	.L916
.L145:
	leal	-12832(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L161
	addl	$69, %edx
	movb	$34, 134(%rsp)
	movb	%dl, 135(%rsp)
.L156:
	movzbl	134(%rsp), %ecx
	leaq	134(%rsp), %rsi
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1839:
	cmpl	$476, %edx
	leaq	.LC21(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L1884:
	jb	.L161
	leal	-913(%rdx), %esi
	cmpl	$56, %esi
	ja	.L161
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rsi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L999:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L930:
	leaq	.LC20(%rip), %rax
	jmp	.L916
.L1722:
	leal	-711(%rdx), %edi
	cmpl	$18, %edi
	ja	.L168
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L929:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L934:
	leaq	.LC19(%rip), %rax
	jmp	.L916
.L191:
	cmpl	$12585, %edx
	ja	.L195
	cmpl	$12549, %edx
	jnb	.L196
	cmpl	$12539, %edx
	leaq	.LC20(%rip), %rdi
	jne	.L168
.L177:
	movzbl	(%rdi), %ecx
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L189:
	cmpl	$65373, %edx
	jbe	.L1885
	cmpl	$65505, %edx
	je	.L203
	cmpl	$65509, %edx
	je	.L204
	cmpl	$65504, %edx
	jne	.L168
	leaq	.LC36(%rip), %rdi
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	$0, 112(%rsp)
	leaq	1(%rdx), %rdi
	je	.L30
	andl	$2, %ebx
	je	.L30
	movq	112(%rsp), %rbx
	leaq	2(%rdx), %rdi
	addq	$1, (%rbx)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L1211:
	leaq	.LC42(%rip), %rdi
	jmp	.L871
.L960:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L959:
	leal	-12832(%rcx), %eax
	cmpl	$9, %eax
	ja	.L973
	addl	$69, %ecx
	movl	$34, %r9d
	jmp	.L1108
.L958:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L1815:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$5, %ebx
	movq	192(%rsp), %r12
	jmp	.L784
.L894:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %r9
	leal	-65281(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1823:
	cmpl	$256, %ecx
	jne	.L47
	movq	%rax, %r12
	movl	$1, 40(%rsp)
	jmp	.L734
.L1853:
	cmpl	$9679, %edx
	jne	.L281
	leaq	.LC9(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L1851:
	cmpl	$9632, %edx
	je	.L251
	jbe	.L1886
	cmpl	$9650, %edx
	je	.L254
	cmpl	$9651, %edx
	jne	.L1887
	leaq	.LC14(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L248:
	cmpl	$40864, %edx
	jbe	.L1888
	cmpl	$65504, %edx
	je	.L270
	jbe	.L1889
	cmpl	$65507, %edx
	je	.L273
	cmpl	$65509, %edx
	jne	.L1890
	leaq	.LC1(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L1795:
	movzbl	3(%r12), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L763
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$7649, %eax
	jg	.L763
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movq	%rsi, %r12
	movzwl	(%rcx,%rax,2), %ecx
	testw	%cx, %cx
	je	.L763
	cmpl	$65533, %ecx
	jne	.L764
	movq	%rsi, %rdi
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L1847:
	cmpl	$275, %edx
	je	.L1141
	jbe	.L1891
	cmpl	$283, %edx
	jne	.L1892
	leaq	.LC32(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L940:
	leaq	.LC6(%rip), %rax
	jmp	.L916
.L1849:
	cmpl	$466, %edx
	jne	.L281
	leaq	.LC26(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L1846:
	cmpq	$-1, %rdi
	je	.L213
.L1118:
	cmpl	$24, 32(%rsp)
	movzbl	1(%rdi), %ecx
	movzbl	2(%rdi), %edx
	je	.L215
	andl	$6144, %r11d
	cmpl	$6144, %r11d
	je	.L367
	movq	8(%rsp), %rbx
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	movb	$27, (%rbx)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$36, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$42, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$72, (%rax)
	jmp	.L367
.L990:
	subl	$53, %ecx
	movl	$36, %r9d
	jmp	.L1090
.L989:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L1804:
	cmpl	$363, %edx
	jne	.L1893
	leaq	.LC29(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1856:
	leal	-8560(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L168
	subl	$59, %edx
	movb	$38, 200(%rsp)
	movb	%dl, 201(%rsp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$8869, %edx
	je	.L185
	ja	.L186
	leal	-8725(%rdx), %edi
	cmpl	$82, %edi
	ja	.L168
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L1893:
	cmpl	$462, %edx
	leaq	.LC28(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L937:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L936:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rax
	je	.L916
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1858:
	cmpl	$299, %edx
	leaq	.LC31(%rip), %rsi
	je	.L102
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L1857:
	leal	-164(%rdx), %esi
	cmpl	$93, %esi
	ja	.L161
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rsi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L911:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L1779:
	movslq	52(%rsp), %rbx
	movq	%rbp, %r12
	jmp	.L731
.L173:
	cmpl	$8451, %edx
	je	.L1139
	ja	.L178
	leal	-8211(%rdx), %edi
	cmpl	$43, %edi
	ja	.L168
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L993:
	cmpl	$8895, %ecx
	jne	.L1013
	leaq	.LC38(%rip), %rax
	jmp	.L984
.L992:
	leaq	.LC39(%rip), %rax
	jmp	.L984
.L178:
	cmpl	$8453, %edx
	leaq	.LC41(%rip), %rdi
	je	.L177
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L1890:
	cmpl	$65505, %edx
	jne	.L281
	leaq	.LC3(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L886:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %r9
	leal	-12288(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L884:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %r9
	leal	-13198(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L977:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L985:
	cmpl	$8453, %ecx
	jne	.L1013
	leaq	.LC41(%rip), %rax
	jmp	.L984
.L879:
	leaq	.LC39(%rip), %rdi
	jmp	.L871
.L982:
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L981:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L997:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L1885:
	cmpl	$65281, %edx
	jnb	.L200
	cmpl	$19968, %edx
	jb	.L168
	cmpl	$40860, %edx
	ja	.L1894
	leal	-19968(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	%r14, %rdx
	movl	$4, %ebx
	jmp	.L731
.L1854:
	leal	-9472(%rdx), %edi
	cmpl	$322, %edi
	ja	.L168
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L1007:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L1018
.L932:
	leaq	.LC22(%rip), %rax
	jmp	.L916
.L963:
	leaq	.LC4(%rip), %rax
	jmp	.L916
.L865:
	leaq	.LC40(%rip), %rdi
	jmp	.L871
.L864:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %r9
	leal	-9216(%rcx), %edi
	leaq	(%r9,%rdi,2), %rdi
	jmp	.L900
.L1003:
	addl	$66, %ecx
	movl	$37, %r9d
	jmp	.L1090
.L1002:
	cmpl	$12963, %ecx
	jne	.L1013
	leaq	.LC37(%rip), %rax
	jmp	.L984
.L1011:
	leaq	.LC34(%rip), %rax
	jmp	.L984
.L1010:
	leaq	.LC35(%rip), %rax
	jmp	.L984
.L912:
	leaq	.LC23(%rip), %rax
	jmp	.L916
.L967:
	leaq	.LC1(%rip), %rax
	jmp	.L916
.L1892:
	cmpl	$299, %edx
	leaq	.LC31(%rip), %rdi
	je	.L223
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L1887:
	cmpl	$9633, %edx
	jne	.L281
	leaq	.LC16(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L966:
	leaq	.LC2(%rip), %rax
	jmp	.L916
.L1828:
	leal	-65281(%rcx), %eax
	cmpl	$93, %eax
	ja	.L973
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L969
.L1818:
	movzbl	1(%r12), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L777
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$8690, %eax
	jg	.L777
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	leaq	2(%r12), %rsi
	movzwl	(%rcx,%rax,2), %ecx
	testw	%cx, %cx
	je	.L777
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1845:
	cmpl	$9633, %edx
	jne	.L161
	leaq	.LC16(%rip), %rsi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L1844:
	leal	-9472(%rdx), %ecx
	cmpl	$75, %ecx
	ja	.L161
	addl	$36, %edx
	movb	$41, 134(%rsp)
	movb	%dl, 135(%rsp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L1866:
	cmpl	$474, %edx
	jne	.L1895
	leaq	.LC22(%rip), %rdi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L1773:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$5, %ebx
	movq	192(%rsp), %r12
	movq	200(%rsp), %rdx
	jmp	.L784
.L1032:
	leal	-131072(%rcx), %eax
	leaq	(%rax,%rax,2), %rax
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %rax
	movzbl	(%rax), %edi
	testb	%dil, %dil
	je	.L1034
	jmp	.L1648
.L1894:
	leal	-65072(%rdx), %edi
	cmpl	$59, %edi
	ja	.L168
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	2(%rdi), %r12
	addq	$1, (%r8)
	movl	$6, 52(%rsp)
	jmp	.L734
.L926:
	leaq	.LC24(%rip), %rax
	jmp	.L916
.L925:
	leaq	.LC25(%rip), %rax
	jmp	.L916
.L279:
	leaq	__PRETTY_FUNCTION__.8043(%rip), %rcx
	leaq	.LC53(%rip), %rsi
	leaq	.LC54(%rip), %rdi
	movl	$220, %edx
	call	__assert_fail@PLT
.L1895:
	cmpl	$476, %edx
	leaq	.LC21(%rip), %rdi
	je	.L223
	jmp	.L281
.L133:
	leaq	.LC15(%rip), %rsi
	jmp	.L102
.L130:
	leaq	.LC17(%rip), %rsi
	jmp	.L102
.L254:
	leaq	.LC15(%rip), %rdi
	jmp	.L223
.L1223:
	movl	$96, %edi
	jmp	.L709
.L1831:
	movl	%r12d, (%rbx)
	jmp	.L17
.L768:
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L765
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L765
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$6589, %eax
	jg	.L765
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L772
	jmp	.L765
.L1891:
	leal	-164(%rdx), %edi
	cmpl	$93, %edi
	ja	.L281
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	jmp	.L276
.L1141:
	leaq	.LC33(%rip), %rdi
	jmp	.L223
.L1717:
	leaq	__PRETTY_FUNCTION__.9598(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L1723:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L1850:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L241:
	leaq	.LC19(%rip), %rdi
	jmp	.L223
.L200:
	leal	-65281(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	jmp	.L206
.L1200:
	movl	$7, %ebx
	jmp	.L731
.L1071:
	leaq	__PRETTY_FUNCTION__.9598(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC61(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L770:
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L765
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L765
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$6387, %eax
	jg	.L765
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L772
	jmp	.L765
.L236:
	leal	-1025(%rdx), %edi
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	jmp	.L276
.L235:
	cmpl	$8869, %edx
	ja	.L243
	cmpl	$8451, %edx
	jnb	.L244
	leal	-8213(%rdx), %edi
	cmpl	$38, %edi
	ja	.L281
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	jmp	.L276
.L1042:
	cmpl	$192, %esi
	movl	$192, %r12d
	je	.L1680
.L1044:
	movl	%r12d, %eax
	movl	%r8d, %edi
	sall	$8, %eax
	andl	$57344, %edi
	cmpl	%edi, %eax
	movl	%eax, 8(%rsp)
	je	.L1055
	movl	%r12d, %edi
	sarl	$5, %edi
	subl	$3, %edi
	jmp	.L1116
.L1041:
	cmpl	$160, %esi
	movl	$160, %r12d
	jne	.L1044
.L1680:
	movl	%esi, %r12d
	movq	200(%rsp), %rdi
	jmp	.L861
.L244:
	leal	-8451(%rdx), %edi
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	jmp	.L276
.L243:
	cmpl	$8978, %edx
	leaq	.LC18(%rip), %rdi
	je	.L223
	jmp	.L281
.L237:
	leaq	.LC20(%rip), %rdi
	jmp	.L223
.L1043:
	cmpl	$224, %esi
	movl	$224, %r12d
	jne	.L1044
	jmp	.L1680
.L1040:
	cmpl	$128, %esi
	movl	$128, %r12d
	jne	.L1044
	jmp	.L1680
.L1139:
	leaq	.LC42(%rip), %rdi
	jmp	.L177
.L1724:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L98:
	leaq	.LC23(%rip), %rsi
	jmp	.L102
.L81:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L273:
	leaq	.LC2(%rip), %rdi
	jmp	.L223
.L1889:
	leal	-65281(%rdx), %edi
	cmpl	$93, %edi
	ja	.L281
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	jmp	.L276
.L270:
	leaq	.LC4(%rip), %rdi
	jmp	.L223
.L1888:
	cmpl	$19968, %edx
	jnb	.L265
	cmpl	$12585, %edx
	ja	.L266
	cmpl	$12288, %edx
	jnb	.L267
	cmpl	$9794, %edx
	leaq	.LC5(%rip), %rdi
	je	.L223
	jmp	.L281
.L978:
	leaq	.LC40(%rip), %rax
	jmp	.L984
.L1787:
	movzbl	1(%rax), %ecx
	jmp	.L1087
.L1855:
	cmpl	$256, %r11d
	je	.L1126
.L47:
	call	abort@PLT
.L218:
	leal	-9312(%rdx), %edi
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	jmp	.L276
.L170:
	leal	-9216(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	jmp	.L206
.L1134:
	leaq	.LC33(%rip), %rsi
	jmp	.L102
.L174:
	leal	-913(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	jmp	.L206
.L1214:
	leaq	.LC42(%rip), %rax
	jmp	.L984
.L267:
	leal	-12288(%rdx), %edi
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	jmp	.L276
.L266:
	leal	-12832(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L281
	addl	$69, %edx
	movb	$34, 136(%rsp)
	movb	%dl, 137(%rsp)
.L277:
	movzbl	136(%rsp), %ecx
	leaq	136(%rsp), %rdi
	jmp	.L1079
.L265:
	leal	-19968(%rdx), %edi
	addq	%rdi, %rdi
	addq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	jmp	.L276
.L305:
	leal	-13198(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rsi
	jmp	.L326
.L304:
	cmpl	$65373, %edx
	ja	.L314
	cmpl	$65281, %edx
	jnb	.L315
	cmpl	$19968, %edx
	jb	.L325
	cmpl	$40860, %edx
	jbe	.L316
	leal	-65072(%rdx), %esi
	cmpl	$59, %esi
	ja	.L325
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rsi
	jmp	.L326
.L307:
	leal	-12288(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rsi
	jmp	.L326
.L300:
	leaq	.LC39(%rip), %rsi
	jmp	.L292
.L316:
	leal	-19968(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rsi
	jmp	.L326
.L315:
	leal	-65281(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rsi
	jmp	.L326
.L314:
	cmpl	$65505, %edx
	je	.L318
	cmpl	$65509, %edx
	je	.L319
	cmpl	$65504, %edx
	jne	.L325
	leaq	.LC36(%rip), %rsi
	jmp	.L292
.L126:
	leaq	.LC6(%rip), %rsi
	jmp	.L102
.L1790:
	movl	%esi, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movl	$6, %ebx
	movq	192(%rsp), %r12
	movq	200(%rsp), %rdx
	jmp	.L784
.L111:
	leaq	.LC25(%rip), %rsi
	jmp	.L102
.L140:
	leaq	.LC8(%rip), %rsi
	jmp	.L102
.L319:
	leaq	.LC34(%rip), %rsi
	jmp	.L292
.L318:
	leaq	.LC35(%rip), %rsi
	jmp	.L292
.L348:
	cmpl	$128, 32(%rsp)
	movl	$128, %eax
	je	.L215
.L352:
	movl	%eax, %esi
	andl	$57344, %r11d
	sall	$8, %esi
	cmpl	%esi, %r11d
	je	.L364
	movl	%eax, %esi
	sarl	$5, %esi
	subl	$3, %esi
	jmp	.L368
.L346:
	cmpl	$96, 32(%rsp)
	movl	$96, %eax
	jne	.L361
	jmp	.L215
.L351:
	cmpl	$224, 32(%rsp)
	movl	$224, %eax
	jne	.L352
	jmp	.L215
.L340:
	leal	-131072(%rdx), %edi
	leaq	(%rdi,%rdi,2), %rdi
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L342
	jmp	.L1577
.L349:
	cmpl	$160, 32(%rsp)
	movl	$160, %eax
	jne	.L352
	jmp	.L215
.L1837:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC51(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L128:
	leaq	.LC13(%rip), %rsi
	jmp	.L102
.L1811:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L1810:
	movzbl	40(%rsp), %ecx
	subl	$33, %ecx
	cmpb	$93, %cl
	ja	.L68
	movzbl	137(%rsp), %ecx
	leal	-33(%rcx), %esi
	cmpb	$93, %sil
	ja	.L68
	leal	-33(%r9), %esi
	imull	$94, %esi, %esi
	leal	-33(%rsi,%rcx), %ecx
	movq	__isoir165_to_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testl	%r8d, %r8d
	je	.L68
.L69:
	cmpl	$65533, %r8d
	je	.L1076
	leaq	2(%rdx), %rdi
	jmp	.L65
.L185:
	leaq	.LC39(%rip), %rdi
	jmp	.L177
.L1755:
	movzbl	1(%rdi), %eax
	jmp	.L1084
.L1076:
	cmpq	$0, 112(%rsp)
	je	.L1896
	andb	$2, %bl
	leaq	2(%rdx), %rdi
	je	.L30
	jmp	.L1110
.L182:
	leal	-8592(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	jmp	.L206
.L219:
	leaq	.LC23(%rip), %rdi
	jmp	.L223
.L107:
	leaq	.LC27(%rip), %rsi
	jmp	.L102
.L100:
	leaq	.LC30(%rip), %rsi
	jmp	.L102
.L1896:
	leaq	2(%rdx), %rdi
	jmp	.L30
.L247:
	leaq	.LC6(%rip), %rdi
	jmp	.L223
.L221:
	leaq	.LC30(%rip), %rdi
	jmp	.L223
.L1848:
	cmpl	$363, %edx
	je	.L230
	cmpl	$462, %edx
	leaq	.LC28(%rip), %rdi
	je	.L223
	jmp	.L281
.L228:
	leaq	.LC27(%rip), %rdi
	jmp	.L223
.L230:
	leaq	.LC29(%rip), %rdi
	jmp	.L223
.L261:
	leaq	.LC8(%rip), %rdi
	jmp	.L223
.L249:
	leaq	.LC13(%rip), %rdi
	jmp	.L223
.L766:
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L765
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L765
	imull	$94, %eax, %eax
	addl	%ecx, %eax
	cmpl	$8602, %eax
	jg	.L765
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L772
	jmp	.L765
.L232:
	leaq	.LC25(%rip), %rdi
	jmp	.L223
.L171:
	leaq	.LC40(%rip), %rdi
	jmp	.L177
.L204:
	leaq	.LC34(%rip), %rdi
	jmp	.L177
.L203:
	leaq	.LC35(%rip), %rdi
	jmp	.L177
.L24:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L78:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L192:
	leal	-12288(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	jmp	.L206
.L196:
	addl	$66, %edx
	movb	$37, 200(%rsp)
	movb	%dl, 201(%rsp)
	jmp	.L207
.L195:
	cmpl	$12963, %edx
	leaq	.LC37(%rip), %rdi
	je	.L177
	jmp	.L168
.L146:
	leal	-12288(%rdx), %esi
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	jmp	.L155
.L144:
	leal	-19968(%rdx), %esi
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rsi
	jmp	.L155
.L739:
	cmpb	$43, %dil
	jne	.L741
	leaq	4(%r12), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 80(%rsp)
	jb	.L1200
	movzbl	3(%r12), %edi
	leal	-73(%rdi), %r9d
	cmpb	$4, %r9b
	ja	.L741
	jmp	.L748
.L97:
	leal	-9312(%rdx), %esi
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	jmp	.L155
.L1775:
	leaq	__PRETTY_FUNCTION__.9598(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC60(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L1774:
	leaq	__PRETTY_FUNCTION__.9598(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC59(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L1059:
	leaq	__PRETTY_FUNCTION__.9484(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L115:
	leal	-1025(%rdx), %esi
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	jmp	.L155
.L1813:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L1812:
	movzbl	137(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L68
	imull	$94, %r9d, %r9d
	addl	%r9d, %ecx
	cmpl	$8690, %ecx
	jg	.L68
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L68
	jmp	.L69
.L1886:
	leal	-9472(%rdx), %ecx
	cmpl	$75, %ecx
	ja	.L281
	addl	$36, %edx
	movb	$41, 136(%rsp)
	movb	%dl, 137(%rsp)
	jmp	.L277
.L251:
	leaq	.LC17(%rip), %rdi
	jmp	.L223
.L190:
	leal	-13198(%rdx), %edi
	addq	%rdi, %rdi
	addq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	jmp	.L206
.L1768:
	leaq	__PRETTY_FUNCTION__.9598(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L1852:
	cmpl	$9671, %edx
	je	.L259
	cmpl	$9675, %edx
	leaq	.LC11(%rip), %rdi
	je	.L223
	jmp	.L281
.L257:
	leaq	.LC10(%rip), %rdi
	jmp	.L223
.L259:
	leaq	.LC12(%rip), %rdi
	jmp	.L223
.L116:
	leaq	.LC20(%rip), %rsi
	jmp	.L102
.L152:
	leaq	.LC2(%rip), %rsi
	jmp	.L102
.L1731:
	movzbl	1(%rdi), %eax
	jmp	.L1082
.L149:
	leaq	.LC4(%rip), %rsi
	jmp	.L102
.L780:
	leaq	__PRETTY_FUNCTION__.9370(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L1054:
	leaq	__PRETTY_FUNCTION__.9484(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L136:
	leaq	.LC10(%rip), %rsi
	jmp	.L102
.L1865:
	movl	32(%rsp), %eax
	andb	$31, %ah
	orb	$-64, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1864:
	movl	32(%rsp), %eax
	andb	$31, %ah
	orb	$-96, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1863:
	movl	32(%rsp), %eax
	andb	$31, %ah
	orb	$-128, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1862:
	movl	32(%rsp), %eax
	andb	$31, %ah
	orb	$96, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1861:
	movl	32(%rsp), %eax
	andb	$-8, %ah
	orb	$4, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1860:
	movl	32(%rsp), %eax
	andb	$-8, %ah
	orb	$3, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L1859:
	movl	32(%rsp), %eax
	andb	$-8, %ah
	orb	$1, %ah
	movl	%eax, 32(%rsp)
	jmp	.L749
.L286:
	leaq	.LC40(%rip), %rsi
	jmp	.L292
.L285:
	leal	-9216(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	jmp	.L326
.L297:
	leal	-8592(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	jmp	.L326
.L1142:
	leaq	.LC42(%rip), %rsi
	jmp	.L292
.L1809:
	subl	$33, %r9d
	cmpl	$86, %r9d
	ja	.L68
	movzbl	137(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L68
	imull	$94, %r9d, %r9d
	addl	%r9d, %ecx
	cmpl	$8177, %ecx
	jg	.L68
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L68
	jmp	.L69
.L289:
	leal	-913(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	jmp	.L326
.L311:
	addl	$66, %edx
	movb	$37, 200(%rsp)
	movb	%dl, 201(%rsp)
	jmp	.L327
.L310:
	cmpl	$12963, %edx
	leaq	.LC37(%rip), %rsi
	je	.L292
	jmp	.L325
.L1876:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
	movzbl	139(%rsp), %esi
	movl	%esi, %edi
	andl	$-5, %edi
	cmpb	$65, %dil
	je	.L1658
	cmpb	$71, %sil
	jne	.L50
	jmp	.L1658
.L56:
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L55
	leal	-33(%rcx), %edi
	cmpl	$93, %edi
	ja	.L55
	imull	$94, %esi, %ecx
	addl	%edi, %ecx
	cmpl	$8602, %ecx
	jg	.L55
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %r8d
	testl	%r8d, %r8d
	jne	.L62
	jmp	.L55
.L1867:
	cmpl	$13312, %edx
	jb	.L342
	cmpl	$19967, %edx
	jbe	.L336
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rsi
	leal	-19968(%rdx), %ecx
	cmpb	$0, (%rsi,%rcx,2)
	jne	.L342
.L336:
	leal	-13312(%rdx), %edi
	leaq	(%rdi,%rdi,2), %rdi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L342
	jmp	.L1577
.L1875:
	movzbl	139(%rsp), %esi
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L51
	imull	$94, %ecx, %ecx
	addl	%esi, %ecx
	cmpl	$7649, %ecx
	jg	.L1130
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L1130
	cmpl	$65533, %r8d
	leaq	4(%rdx), %rdi
	jne	.L65
	jmp	.L51
.L58:
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L55
	leal	-33(%rcx), %edi
	cmpl	$93, %edi
	ja	.L55
	imull	$94, %esi, %ecx
	addl	%edi, %ecx
	cmpl	$6589, %ecx
	jg	.L55
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %r8d
	testl	%r8d, %r8d
	jne	.L62
	jmp	.L55
.L1130:
	leaq	2(%rdx), %rdi
	jmp	.L51
.L1880:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC51(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L1879:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1878:
	leaq	__PRETTY_FUNCTION__.9508(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L123:
	leal	-8451(%rdx), %esi
	addq	%rsi, %rsi
	addq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	jmp	.L155
.L360:
	movq	112(%rsp), %rax
	addq	$4, %rdx
	movq	%rdx, 160(%rsp)
	addq	$1, (%rax)
	jmp	.L1668
.L1877:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L32
	cmpb	$72, 139(%rsp)
	jne	.L50
	jmp	.L1658
.L60:
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L55
	leal	-33(%rcx), %edi
	cmpl	$93, %edi
	ja	.L55
	imull	$94, %esi, %ecx
	addl	%edi, %ecx
	cmpl	$6387, %ecx
	jg	.L55
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %r8d
	testl	%r8d, %r8d
	jne	.L62
	jmp	.L55
.L1836:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1835:
	leaq	__PRETTY_FUNCTION__.9391(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9484, @object
	.size	__PRETTY_FUNCTION__.9484, 22
__PRETTY_FUNCTION__.9484:
	.string	"to_iso2022cn_ext_loop"
	.align 16
	.type	__PRETTY_FUNCTION__.9370, @object
	.size	__PRETTY_FUNCTION__.9370, 24
__PRETTY_FUNCTION__.9370:
	.string	"from_iso2022cn_ext_loop"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8043, @object
	.size	__PRETTY_FUNCTION__.8043, 15
__PRETTY_FUNCTION__.8043:
	.string	"ucs4_to_gb2312"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.9508, @object
	.size	__PRETTY_FUNCTION__.9508, 29
__PRETTY_FUNCTION__.9508:
	.string	"to_iso2022cn_ext_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9391, @object
	.size	__PRETTY_FUNCTION__.9391, 31
__PRETTY_FUNCTION__.9391:
	.string	"from_iso2022cn_ext_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9598, @object
	.size	__PRETTY_FUNCTION__.9598, 6
__PRETTY_FUNCTION__.9598:
	.string	"gconv"
