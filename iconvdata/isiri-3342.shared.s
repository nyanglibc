	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	andl	$127, %esi
	setne	%al
	negl	%eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISIRI-3342//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L6
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	32(%rax), %rsi
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r10d
	movq	%rsi, 80(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 48(%rsp)
	testb	$1, %r10b
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L11
	movq	%rdi, %rax
	movq	144(%rdi), %rdi
	cmpq	$0, 104(%rax)
	movq	%rdi, 24(%rsp)
	je	.L11
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L11:
	testl	%ebx, %ebx
	jne	.L217
	movq	8(%rsp), %rax
	movq	48(%rsp), %rbx
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %ecx
	movq	8(%r12), %r11
	testq	%rbx, %rbx
	movq	(%rax), %r15
	movq	%rbx, %rax
	cmove	%r12, %rax
	testq	%r14, %r14
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	testl	%ecx, %ecx
	movq	%rax, 64(%rsp)
	movq	40(%rsp), %rax
	setne	103(%rsp)
	movzbl	103(%rsp), %edi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L117
	testb	%dil, %dil
	je	.L117
	movq	32(%r12), %rsi
	movl	(%rsi), %edi
	movq	%rsi, 16(%rsp)
	movl	%edi, %edx
	movl	%edi, 32(%rsp)
	andl	$7, %edx
	jne	.L218
.L117:
	movq	$0, 32(%rsp)
.L18:
	movq	%r14, 16(%rsp)
	movq	%r11, %r14
	.p2align 4,,10
	.p2align 3
.L106:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L50
	movq	(%rdi), %rsi
	addq	%rsi, 32(%rsp)
.L50:
	testq	%rax, %rax
	je	.L219
	leaq	136(%rsp), %rax
	movq	%r15, 128(%rsp)
	movq	%r13, %rbx
	movq	%r13, 136(%rsp)
	movq	%r15, %rdi
	movl	$4, %r11d
	movq	%rax, 56(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 88(%rsp)
.L58:
	cmpq	%rdi, %rbp
	je	.L59
.L69:
	leaq	4(%rdi), %rax
	cmpq	%rax, %rbp
	jb	.L125
	cmpq	%rbx, %r14
	jbe	.L126
	movl	(%rdi), %r8d
	cmpl	$65534, %r8d
	ja	.L60
	cmpl	$127, %r8d
	movl	$164, %edx
	leaq	from_idx(%rip), %rsi
	ja	.L61
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movzwl	10(%rcx), %edx
	movq	%rcx, %rsi
.L61:
	cmpl	%edx, %r8d
	leaq	8(%rsi), %rcx
	ja	.L221
	movzwl	(%rcx), %eax
	cmpl	%eax, %r8d
	jb	.L63
	movl	%r8d, %edx
	addl	12(%rsi), %edx
.L62:
	leaq	from_ucs4(%rip), %rax
	movzbl	(%rax,%rdx), %eax
	testb	%al, %al
	jne	.L146
	testl	%r8d, %r8d
	jne	.L63
.L146:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 128(%rsp)
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L59:
	cmpq	$0, 48(%rsp)
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
	jne	.L222
.L70:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L223
	cmpq	%rbx, %r13
	jnb	.L130
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 56(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%r8
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%r9
	movl	56(%rsp), %r11d
	je	.L74
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L224
.L73:
	testl	%r10d, %r10d
	jne	.L142
.L105:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rbx
	movl	16(%r12), %r10d
	movq	(%r12), %r13
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r15
	movq	96(%rax), %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L60:
	shrl	$7, %r8d
	cmpl	$7168, %r8d
	je	.L225
.L63:
	cmpq	$0, 64(%rsp)
	je	.L129
	testb	$8, 16(%r12)
	jne	.L226
.L67:
	testb	$2, %r10b
	jne	.L227
.L129:
	movl	$6, %r11d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L219:
	cmpq	%r15, %rbp
	je	.L120
	leaq	4(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L121
	movq	%r15, %rax
	movq	%r13, %rbx
	movl	$4, %r11d
	andl	$2, %r10d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %rax
	movl	%ecx, (%rbx)
	movq	%rdx, %rbx
	movq	%rax, %rsi
.L55:
	cmpq	%rax, %rbp
	je	.L52
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r14
	jb	.L122
.L53:
	movzbl	(%rax), %ecx
	leaq	to_ucs4(%rip), %r9
	movq	%rax, %rsi
	movq	%rcx, %rdi
	movl	(%r9,%rcx,4), %ecx
	testl	%ecx, %ecx
	jne	.L54
	andl	$127, %edi
	je	.L54
	cmpq	$0, 64(%rsp)
	je	.L124
	testl	%r10d, %r10d
	jne	.L228
.L124:
	movl	$6, %r11d
.L52:
	movq	8(%rsp), %rax
	movq	%rsi, (%rax)
.L229:
	cmpq	$0, 48(%rsp)
	je	.L70
.L222:
	movq	48(%rsp), %rax
	movl	%r11d, %r9d
	movq	%rbx, (%rax)
.L10:
	addq	$152, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L73
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L220:
	movl	%r8d, %edx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$7, %r11d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$5, %r11d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%rsp), %rax
	movl	$5, %r11d
	movq	%rsi, (%rax)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L226:
	movl	%r10d, 104(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	72(%rsp), %r9
	movq	104(%rsp), %rcx
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r11d
	popq	%rbx
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rbx
	movl	104(%rsp), %r10d
	je	.L67
	cmpl	$5, %eax
	jne	.L58
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L227:
	movq	64(%rsp), %rax
	addq	$4, %rdi
	movl	$6, %r11d
	movq	%rdi, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L58
.L223:
	movq	16(%rsp), %r14
	movq	%rbx, (%r12)
	movl	%r11d, %r9d
	movq	112(%rsp), %rax
	addq	%rax, (%r14)
.L72:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 103(%rsp)
	je	.L10
	cmpl	$7, %r9d
	jne	.L10
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L108
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	32(%r12), %rcx
	je	.L110
.L109:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L109
.L110:
	movl	(%rcx), %edx
	movq	8(%rsp), %rbx
	andl	$-8, %edx
	movq	%rbp, (%rbx)
	orl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L130:
	movl	%r11d, %r10d
	jmp	.L73
.L224:
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L76
	movq	(%rdi), %rax
.L76:
	addq	112(%rsp), %rax
	cmpq	32(%rsp), %rax
	movq	8(%rsp), %rax
	je	.L230
	movq	%r15, (%rax)
	movq	40(%rsp), %rax
	movl	16(%r12), %ebx
	cmpq	$0, 96(%rax)
	je	.L231
	leaq	136(%rsp), %rsi
	movq	%r15, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r13, %rdx
	movl	$4, %eax
	movq	%rsi, 32(%rsp)
	leaq	128(%rsp), %rsi
	movq	%rsi, 56(%rsp)
.L91:
	cmpq	%r15, %rbp
	je	.L232
	leaq	4(%r15), %rcx
	cmpq	%rcx, %rbp
	jb	.L135
	cmpq	%rdx, %r11
	jbe	.L139
	movl	(%r15), %r8d
	cmpl	$65534, %r8d
	ja	.L93
	cmpl	$127, %r8d
	movl	$164, %ecx
	leaq	from_idx(%rip), %rdi
	ja	.L94
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L234:
	movzwl	10(%rsi), %ecx
	movq	%rsi, %rdi
.L94:
	cmpl	%ecx, %r8d
	leaq	8(%rdi), %rsi
	ja	.L234
	movzwl	(%rsi), %ecx
	cmpl	%ecx, %r8d
	jb	.L96
	movl	%r8d, %ecx
	addl	12(%rdi), %ecx
.L95:
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L147
	testl	%r8d, %r8d
	jne	.L96
.L147:
	leaq	1(%rdx), %rsi
	movq	%rsi, 136(%rsp)
	movb	%cl, (%rdx)
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rdx
	leaq	4(%rdi), %r15
	movq	%r15, 128(%rsp)
	jmp	.L91
.L218:
	testq	%rbx, %rbx
	jne	.L235
	cmpl	$4, %edx
	movq	%r15, 128(%rsp)
	movq	%r13, 136(%rsp)
	ja	.L20
	leaq	120(%rsp), %rsi
	movq	16(%rsp), %rcx
	movslq	%edx, %rax
	xorl	%ebx, %ebx
	movq	%rsi, 32(%rsp)
.L21:
	movzbl	4(%rcx,%rbx), %edx
	movb	%dl, (%rsi,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L21
	movq	%r15, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L236
	cmpq	%r11, %r13
	movl	$5, %r9d
	jnb	.L10
	leaq	1(%r15), %rax
	leaq	119(%rsp), %rsi
.L29:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rbx
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%cl, (%rsi,%rbx)
	ja	.L144
	cmpq	%rdx, %rbp
	ja	.L29
.L144:
	movl	120(%rsp), %ecx
	movq	32(%rsp), %rax
	cmpl	$65534, %ecx
	movq	%rax, 128(%rsp)
	ja	.L31
	cmpl	$127, %ecx
	movl	$164, %eax
	leaq	from_idx(%rip), %rsi
	ja	.L32
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L238:
	movzwl	10(%rdx), %eax
	movq	%rdx, %rsi
.L32:
	cmpl	%eax, %ecx
	leaq	8(%rsi), %rdx
	ja	.L238
	movzwl	(%rdx), %eax
	cmpl	%eax, %ecx
	jb	.L34
	movl	%ecx, %eax
	addl	12(%rsi), %eax
.L33:
	leaq	from_ucs4(%rip), %rdx
	testl	%ecx, %ecx
	movzbl	(%rdx,%rax), %eax
	je	.L145
	testb	%al, %al
	jne	.L145
.L34:
	cmpq	$0, 64(%rsp)
	je	.L119
	testb	$8, %r10b
	jne	.L239
	andl	$2, %r10d
	je	.L119
	movq	32(%rsp), %rax
.L112:
	movq	64(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rbx)
.L42:
	cmpq	32(%rsp), %rax
	jne	.L35
.L119:
	movl	$6, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rax, 128(%rsp)
	movq	%rax, %rdi
	jmp	.L58
.L230:
	movq	40(%rsp), %rdi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rdi)
	je	.L240
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L73
.L145:
	leaq	1(%r13), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, 0(%r13)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	je	.L210
.L35:
	movq	16(%rsp), %rbx
	subq	32(%rsp), %rax
	movl	(%rbx), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L241
	movq	8(%rsp), %rbx
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r13
	movl	16(%r12), %r10d
	addq	(%rbx), %rax
	movq	%rax, (%rbx)
	movq	%rax, %r15
	movq	16(%rsp), %rax
	movl	%edx, (%rax)
	movq	112(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L18
.L121:
	movq	8(%rsp), %rax
	movq	%r15, %rsi
	movq	%r13, %rbx
	movl	$5, %r11d
	movq	%rsi, (%rax)
	jmp	.L229
.L228:
	movq	64(%rsp), %rdi
	leaq	1(%rax), %rax
	movl	$6, %r11d
	movq	%rax, %rsi
	addq	$1, (%rdi)
	jmp	.L55
.L217:
	cmpq	$0, 48(%rsp)
	jne	.L242
	movq	32(%r12), %rax
	xorl	%r9d, %r9d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L10
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	movq	%r14, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rsi
	movl	%eax, %r9d
	popq	%rdi
	jmp	.L10
.L142:
	movl	%r10d, %r9d
	jmp	.L72
.L96:
	cmpq	$0, 64(%rsp)
	je	.L140
	testb	$8, 16(%r12)
	jne	.L243
.L100:
	testb	$2, %bl
	jne	.L244
.L140:
	movl	$6, %eax
.L92:
	movq	8(%rsp), %rbx
	movq	120(%rsp), %r11
	movq	%r15, (%rbx)
.L90:
	cmpq	%r11, %rdx
	jne	.L82
	cmpq	$5, %rax
	jne	.L81
	cmpq	%rdx, %r13
	jne	.L73
.L84:
	subl	$1, 20(%r12)
	jmp	.L73
.L120:
	movq	%rbp, %rsi
	movq	%r13, %rbx
	movl	$4, %r11d
	jmp	.L52
.L243:
	movq	%r11, 104(%rsp)
	movl	%r10d, 88(%rsp)
	subq	$8, %rsp
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r15
	movq	136(%rsp), %rdx
	movl	88(%rsp), %r10d
	movq	104(%rsp), %r11
	je	.L100
	cmpl	$5, %eax
	jne	.L91
.L139:
	movl	$5, %eax
	jmp	.L92
.L231:
	cmpq	%r15, %rbp
	je	.L245
	leaq	4(%r13), %rcx
	cmpq	%r11, %rcx
	ja	.L246
	andl	$2, %ebx
	movq	%r13, %rdx
	movl	$4, %eax
	movq	64(%rsp), %r9
	movq	%r15, %rdi
	movl	%ebx, 32(%rsp)
	jmp	.L85
.L87:
	addq	$1, %rdi
	movl	%r8d, (%rdx)
	movq	%rcx, %rdx
	movq	%rdi, %rsi
.L88:
	cmpq	%rdi, %rbp
	je	.L247
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r11
	jb	.L132
.L85:
	movzbl	(%rdi), %r8d
	leaq	to_ucs4(%rip), %rbx
	movq	%rdi, %rsi
	movq	%r8, %r15
	movl	(%rbx,%r8,4), %r8d
	testl	%r8d, %r8d
	jne	.L87
	andl	$127, %r15d
	je	.L87
	testq	%r9, %r9
	je	.L134
	movl	32(%rsp), %edi
	testl	%edi, %edi
	jne	.L248
.L134:
	movl	$6, %eax
.L86:
	movq	8(%rsp), %rbx
	movq	%rsi, (%rbx)
	jmp	.L90
.L233:
	movl	%r8d, %ecx
	jmp	.L95
.L135:
	movl	$7, %eax
	jmp	.L92
.L93:
	shrl	$7, %r8d
	cmpl	$7168, %r8d
	jne	.L96
	movq	%rcx, 128(%rsp)
	movq	%rcx, %r15
	jmp	.L91
.L240:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L73
.L236:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r15, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L23
	cmpq	%rax, %rbx
	leaq	1(%r15), %rdx
	jnb	.L25
.L26:
	movq	%rdx, 128(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %rdx
	movq	16(%rsp), %rdi
	movb	%cl, 4(%rdi,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L26
.L25:
	movl	$7, %r9d
	jmp	.L10
.L132:
	movl	$5, %eax
	jmp	.L86
.L244:
	movq	64(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L91
.L237:
	movl	%ecx, %eax
	jmp	.L33
.L31:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	jne	.L34
	movq	32(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rbp, %rsi
	jmp	.L86
.L232:
	cltq
	movq	%rbp, %r15
	jmp	.L92
.L45:
	testl	%r9d, %r9d
	jne	.L10
.L210:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rbx
	movl	16(%r12), %r10d
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r15
	movq	96(%rax), %rax
	jmp	.L18
.L246:
	cmpq	%r13, %r11
	je	.L84
.L82:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	1(%rsi), %rdi
	addq	$1, (%r9)
	movl	$6, %eax
	movq	%rdi, %rsi
	jmp	.L88
.L239:
	movq	32(%rsp), %rax
	movq	%r11, 104(%rsp)
	leaq	128(%rsp), %rcx
	movl	%r10d, 88(%rsp)
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	addq	%rbx, %rax
	movq	%rax, 64(%rsp)
	pushq	72(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r15
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%rdx
	movl	88(%rsp), %r10d
	movq	104(%rsp), %r11
	movq	128(%rsp), %rax
	je	.L249
	cmpq	32(%rsp), %rax
	jne	.L35
	cmpl	$7, %r9d
	jne	.L45
	movq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 56(%rsp)
	je	.L250
	movq	16(%rsp), %rax
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rsi)
	cmpq	%rdx, %rbx
	jle	.L251
	cmpq	$4, %rbx
	ja	.L252
	movq	16(%rsp), %rdi
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%rdi)
	je	.L25
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L49:
	movzbl	(%rcx,%rax), %edx
	movq	16(%rsp), %rsi
	movb	%dl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L49
	jmp	.L25
.L23:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L252:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L251:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L250:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L249:
	andb	$2, %r10b
	je	.L42
	jmp	.L112
.L245:
	cmpq	%r13, %r11
	jne	.L82
.L81:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L241:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L108:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L242:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L20:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L235:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9074, @object
	.size	__PRETTY_FUNCTION__.9074, 14
__PRETTY_FUNCTION__.9074:
	.string	"to_gap_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9153, @object
	.size	__PRETTY_FUNCTION__.9153, 6
__PRETTY_FUNCTION__.9153:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 215
from_ucs4:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	64
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	95
	.byte	96
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	123
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.byte	-92
	.byte	-26
	.byte	-25
	.byte	-86
	.byte	-84
	.byte	-69
	.byte	0
	.byte	0
	.byte	0
	.byte	-65
	.byte	0
	.byte	-62
	.byte	-64
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-63
	.byte	-61
	.byte	-4
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-48
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-23
	.byte	-40
	.byte	-39
	.byte	-3
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-32
	.byte	-33
	.byte	0
	.byte	-2
	.byte	-13
	.byte	-11
	.byte	-12
	.byte	-16
	.byte	-14
	.byte	-15
	.byte	-10
	.byte	-9
	.byte	-91
	.byte	-82
	.byte	-89
	.byte	-60
	.byte	-56
	.byte	-49
	.byte	-38
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-37
	.byte	-31
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-95
	.byte	-94
	.align 32
	.type	from_idx, @object
	.size	from_idx, 128
from_idx:
	.value	0
	.value	127
	.long	0
	.value	164
	.value	164
	.long	-36
	.value	171
	.value	171
	.long	-42
	.value	187
	.value	187
	.long	-57
	.value	215
	.value	215
	.long	-84
	.value	1548
	.value	1548
	.long	-1416
	.value	1563
	.value	1618
	.long	-1430
	.value	1642
	.value	1644
	.long	-1453
	.value	1662
	.value	1662
	.long	-1470
	.value	1670
	.value	1670
	.long	-1477
	.value	1688
	.value	1688
	.long	-1494
	.value	1705
	.value	1711
	.long	-1510
	.value	1740
	.value	1740
	.long	-1538
	.value	1776
	.value	1785
	.long	-1573
	.value	8204
	.value	8205
	.long	-7991
	.value	-1
	.value	-1
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.zero	4
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	8204
	.long	8205
	.long	33
	.long	164
	.long	1642
	.long	46
	.long	1644
	.long	41
	.long	40
	.long	215
	.long	43
	.long	1548
	.long	45
	.long	1643
	.long	47
	.long	1776
	.long	1777
	.long	1778
	.long	1779
	.long	1780
	.long	1781
	.long	1782
	.long	1783
	.long	1784
	.long	1785
	.long	58
	.long	1563
	.long	60
	.long	61
	.long	62
	.long	1567
	.long	1570
	.long	1575
	.long	1569
	.long	1576
	.long	1662
	.long	1578
	.long	1579
	.long	1580
	.long	1670
	.long	1581
	.long	1582
	.long	1583
	.long	1584
	.long	1585
	.long	1586
	.long	1688
	.long	1587
	.long	1588
	.long	1589
	.long	1590
	.long	1591
	.long	1592
	.long	1593
	.long	1594
	.long	1601
	.long	1602
	.long	1705
	.long	1711
	.long	1604
	.long	1605
	.long	1606
	.long	1608
	.long	1607
	.long	1740
	.long	93
	.long	91
	.long	125
	.long	123
	.long	171
	.long	187
	.long	42
	.long	1600
	.long	124
	.long	92
	.zero	16
	.long	1614
	.long	1616
	.long	1615
	.long	1611
	.long	1613
	.long	1612
	.long	1617
	.long	1618
	.long	1571
	.long	1572
	.long	1573
	.long	1574
	.long	1577
	.long	1603
	.long	1610
	.long	127
