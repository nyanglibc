	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	jne	.L1
	testb	%sil, %sil
	movl	$-1, %edx
	cmovne	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO_6937//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$11, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$11, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"\320"
.LC2:
	.string	"\251"
.LC3:
	.string	"\271"
.LC4:
	.string	"\252"
.LC5:
	.string	"\272"
.LC6:
	.string	"\324"
.LC7:
	.string	"\340"
.LC8:
	.string	"\325"
.LC9:
	.string	"../iconv/skeleton.c"
.LC10:
	.string	"outbufstart == NULL"
.LC11:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC13:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC14:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC15:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC16:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC17:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC18:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC20:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 64(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	testb	$1, %r11b
	movq	%r9, 56(%rsp)
	movl	224(%rsp), %ebx
	movq	%rdi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L15
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L408
	movq	16(%rsp), %rax
	movq	32(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movl	232(%rsp), %edi
	movq	8(%r12), %r13
	testq	%rsi, %rsi
	movq	(%rax), %r14
	movq	%rsi, %rax
	cmove	%r12, %rax
	cmpq	$0, 56(%rsp)
	movq	(%rax), %r10
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%edi, %edi
	movq	%rax, 80(%rsp)
	jne	.L409
.L22:
	movq	40(%rsp), %rax
	leaq	from_ucs4(%rip), %r15
	cmpq	$0, 96(%rax)
	je	.L410
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	movq	%r10, %rbx
	movq	%r14, %rax
	movl	$4, 8(%rsp)
.L110:
	cmpq	%rax, %rbp
	je	.L111
.L143:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L235
	cmpq	%rbx, %r13
	jbe	.L236
	movl	(%rax), %edx
	cmpl	$382, %edx
	ja	.L411
	movl	%edx, %ecx
	cmpb	$0, (%r15,%rcx,2)
	jne	.L137
	testl	%edx, %edx
	jne	.L407
.L137:
	leaq	(%r15,%rcx,2), %rax
	movzbl	(%rax), %edx
.L129:
	leaq	1(%rbx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rbx)
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L140
.L207:
	movq	152(%rsp), %rbx
	cmpq	%rbx, %r13
	jbe	.L412
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
.L140:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L111:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L413
.L144:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L414
	cmpq	%rbx, %r10
	movq	%r10, 48(%rsp)
	jnb	.L242
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r8
	popq	%r9
	je	.L148
	movq	136(%rsp), %rax
	movq	48(%rsp), %r10
	cmpq	%rbx, %rax
	movq	%rax, 8(%rsp)
	jne	.L415
.L147:
	testl	%r11d, %r11d
	jne	.L261
.L201:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r10
	movq	(%rax), %r14
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L99
.L410:
	cmpq	%r14, %rbp
	je	.L225
	leaq	4(%r10), %rcx
	movq	%r14, %rdx
	movq	%r10, %rbx
	cmpq	%rcx, %r13
	jb	.L227
	leaq	to_ucs4(%rip), %r8
	leaq	to_ucs4_comb(%rip), %r9
	movl	$4, 8(%rsp)
	andl	$2, %r11d
	.p2align 4,,10
	.p2align 3
.L101:
	movzbl	(%rdx), %eax
	leal	-193(%rax), %edi
	movl	%eax, %esi
	cmpl	$14, %edi
	jbe	.L416
	movl	%eax, %eax
	testb	%sil, %sil
	movl	(%r8,%rax,4), %eax
	je	.L108
	testl	%eax, %eax
	je	.L417
.L108:
	addq	$1, %rdx
.L107:
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L106:
	cmpq	%rdx, %rbp
	je	.L100
.L104:
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r13
	jnb	.L101
.L227:
	movl	$5, 8(%rsp)
.L100:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	je	.L144
.L413:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L14:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$7, 8(%rsp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L148:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L147
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$5, 8(%rsp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L411:
	cmpl	$8220, %edx
	je	.L114
	jbe	.L418
	cmpl	$8542, %edx
	ja	.L122
	cmpl	$8539, %edx
	jnb	.L123
	cmpl	$8482, %edx
	je	.L124
	cmpl	$8486, %edx
	je	.L237
	cmpl	$8221, %edx
	je	.L419
.L113:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L420
	.p2align 4,,10
	.p2align 3
.L407:
	cmpq	$0, 80(%rsp)
	je	.L241
	testb	$8, 16(%r12)
	jne	.L421
.L138:
	testb	$2, %r11b
	jne	.L422
.L241:
	movl	$6, 8(%rsp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L409:
	movq	32(%r12), %r15
	movl	(%r15), %eax
	movl	%eax, %ebx
	andl	$7, %ebx
	je	.L22
	testq	%rsi, %rsi
	jne	.L423
	movq	40(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L424
	cmpl	$4, %ebx
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	ja	.L42
	leaq	136(%rsp), %rcx
	movslq	%ebx, %rbx
	xorl	%eax, %eax
	movq	%rcx, 48(%rsp)
.L43:
	movzbl	4(%r15,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rbx, %rax
	jne	.L43
	movq	%r14, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L425
	cmpq	%r13, %r10
	jnb	.L89
	leaq	1(%r14), %rax
	leaq	135(%rsp), %rsi
.L51:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%dl, (%rsi,%rbx)
	ja	.L263
	cmpq	%rcx, %rbp
	ja	.L51
.L263:
	movq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	addq	%rbx, %rax
	movq	%rax, 88(%rsp)
	movl	136(%rsp), %eax
	cmpl	$382, %eax
	ja	.L426
	leaq	from_ucs4(%rip), %rdx
	movl	%eax, %ecx
	cmpb	$0, (%rdx,%rcx,2)
	jne	.L80
	testl	%eax, %eax
	je	.L80
	cmpq	$0, 80(%rsp)
	je	.L73
	testb	$8, %r11b
	jne	.L427
	andl	$2, %r11d
	je	.L73
	movq	80(%rsp), %rax
	addq	$1, (%rax)
.L394:
	movq	48(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L418:
	cmpl	$733, %edx
	movl	$5, %esi
	je	.L116
	jbe	.L428
	cmpl	$8216, %edx
	je	.L119
	cmpl	$8217, %edx
	je	.L120
	cmpl	$8212, %edx
	jne	.L113
	leaq	.LC1(%rip), %rsi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L412:
	subq	$1, %rbx
	movq	144(%rsp), %rax
	movl	$5, 8(%rsp)
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L414:
	movq	56(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rdi)
.L146:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	cmpl	$7, 8(%rsp)
	jne	.L14
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L203
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L205
.L204:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L204
.L205:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L416:
	leaq	1(%rdx), %rsi
	cmpq	%rsi, %rbp
	jbe	.L228
	movzbl	1(%rdx), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L429
	leaq	(%rdi,%rdi,2), %rsi
	cltq
	salq	$5, %rsi
	addq	%rsi, %rax
	movl	(%r9,%rax,4), %eax
	testl	%eax, %eax
	je	.L430
	addq	$2, %rdx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L242:
	movl	8(%rsp), %r11d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L417:
	cmpq	$0, 80(%rsp)
	je	.L234
	testl	%r11d, %r11d
	jne	.L431
.L234:
	movl	$6, 8(%rsp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L415:
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L432
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	movq	%r10, %rdx
	movl	$4, %eax
.L165:
	cmpq	%r14, %rbp
	je	.L433
.L199:
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbp
	jb	.L252
	cmpq	%rdx, 8(%rsp)
	jbe	.L253
	movl	(%r14), %ecx
	cmpl	$382, %ecx
	ja	.L434
	movl	%ecx, %esi
	cmpb	$0, (%r15,%rsi,2)
	jne	.L193
	testl	%ecx, %ecx
	jne	.L435
.L193:
	leaq	(%r15,%rsi,2), %rsi
	movzbl	(%rsi), %ecx
.L184:
	leaq	1(%rdx), %rdi
	movq	%rdi, 152(%rsp)
	movb	%cl, (%rdx)
	movzbl	1(%rsi), %edx
	testb	%dl, %dl
	je	.L196
.L208:
	movq	152(%rsp), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L436
	leaq	1(%rcx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rcx)
.L196:
	movq	144(%rsp), %rdi
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r14
	cmpq	%r14, %rbp
	movq	%r14, 144(%rsp)
	jne	.L199
.L433:
	cltq
	movq	%rbp, %r14
	jmp	.L166
.L124:
	leaq	.LC6(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	(%rsi), %eax
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
	jmp	.L140
.L424:
	cmpl	$4, %ebx
	ja	.L25
	movzbl	4(%r15), %edx
	cmpl	$1, %ebx
	movb	%dl, 152(%rsp)
	movl	$1, %edx
	je	.L26
	movzbl	5(%r15), %edx
	movb	%dl, 153(%rsp)
	movl	$2, %edx
.L26:
	leaq	4(%r10), %rcx
	cmpq	%rcx, %r13
	jb	.L89
	movzbl	(%r14), %esi
	movb	%sil, 152(%rsp,%rdx)
	movzbl	152(%rsp), %esi
	leal	-193(%rsi), %r9d
	movl	%esi, %edi
	cmpl	$14, %r9d
	jbe	.L437
	leaq	to_ucs4(%rip), %rax
	movl	%esi, %esi
	movl	(%rax,%rsi,4), %eax
	leaq	152(%rsp), %rsi
	leaq	1(%rsi), %rdx
	testl	%eax, %eax
	je	.L438
.L36:
	movl	%eax, (%r10)
.L35:
	movl	(%r15), %eax
	subq	%rsi, %rdx
	movq	%rdx, %r11
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %r11
	jle	.L439
	movq	16(%rsp), %rdi
	subq	%rdx, %r11
	andl	$-8, %eax
	addq	%r11, %r14
	movq	%rcx, %r10
	movl	16(%r12), %r11d
	movq	%r14, (%rdi)
	movl	%eax, (%r15)
	jmp	.L22
.L119:
	leaq	.LC2(%rip), %rsi
	jmp	.L125
.L432:
	cmpq	%r14, %rbp
	je	.L440
	leaq	4(%r10), %rcx
	andl	$2, %ebx
	cmpq	%rcx, 8(%rsp)
	movq	%r10, %rdx
	movl	$4, %edi
	leaq	to_ucs4(%rip), %r9
	jb	.L441
	movl	%edi, 48(%rsp)
	movq	8(%rsp), %rdi
	.p2align 4,,10
	.p2align 3
.L154:
	movzbl	(%r14), %eax
	leal	-193(%rax), %r8d
	movl	%eax, %esi
	cmpl	$14, %r8d
	jbe	.L442
	movl	%eax, %eax
	testb	%sil, %sil
	movl	(%r9,%rax,4), %eax
	je	.L163
	testl	%eax, %eax
	je	.L443
.L163:
	addq	$1, %r14
.L162:
	movl	%eax, (%rdx)
	movq	%rcx, %rdx
.L161:
	cmpq	%r14, %rbp
	je	.L444
.L159:
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L154
	movl	$5, %eax
.L156:
	movq	16(%rsp), %rsi
	movq	%r14, (%rsi)
	jmp	.L164
.L80:
	leaq	(%rdx,%rcx,2), %rdx
	movzbl	(%rdx), %eax
.L70:
	leaq	1(%r10), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%r10)
	movzbl	1(%rdx), %eax
	testb	%al, %al
	je	.L87
.L206:
	movq	152(%rsp), %rdx
	cmpq	%rdx, %r13
	jbe	.L445
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
.L87:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L395
.L72:
	movl	(%r15), %edx
	subq	48(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L446
	movq	16(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r10
	movl	16(%r12), %r11d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r14
	movl	%edx, (%r15)
	jmp	.L22
.L445:
	subq	$1, %rdx
	movq	144(%rsp), %rax
	cmpq	48(%rsp), %rax
	movq	%rdx, 152(%rsp)
	jne	.L72
.L89:
	movl	$5, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L421:
	movl	%r11d, 88(%rsp)
	movq	%r10, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	48(%rsp), %r10
	movl	88(%rsp), %r11d
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L138
	cmpl	$5, 8(%rsp)
	jne	.L110
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L122:
	cmpl	$8592, %edx
	jb	.L113
	cmpl	$8595, %edx
	jbe	.L127
	cmpl	$9834, %edx
	leaq	.LC8(%rip), %rsi
	je	.L125
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L422:
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L428:
	cmpl	$711, %edx
	jne	.L447
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%rbx)
	movl	$32, %eax
	jmp	.L207
.L431:
	movq	80(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L106
.L408:
	cmpq	$0, 32(%rsp)
	jne	.L448
	movq	32(%r12), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	movl	%eax, 24(%rsp)
	popq	%r8
	popq	%r9
	jmp	.L14
.L261:
	movl	%r11d, 8(%rsp)
	jmp	.L146
.L123:
	subl	$127, %edx
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rax
	movb	%dl, 136(%rsp)
	jmp	.L129
.L447:
	jb	.L113
	leal	-728(%rdx), %esi
	cmpl	$3, %esi
	ja	.L113
.L116:
	leaq	map.9088(%rip), %rax
	movb	$32, 137(%rsp)
	movzbl	(%rax,%rsi), %edx
	leaq	136(%rsp), %rax
	movb	%dl, 136(%rsp)
	jmp	.L129
.L419:
	leaq	.LC5(%rip), %rsi
	jmp	.L125
.L237:
	leaq	.LC7(%rip), %rsi
	jmp	.L125
.L114:
	leaq	.LC4(%rip), %rsi
	jmp	.L125
.L127:
	addl	$28, %edx
	movb	$0, 137(%rsp)
	leaq	136(%rsp), %rax
	movb	%dl, 136(%rsp)
	jmp	.L129
.L120:
	leaq	.LC3(%rip), %rsi
	jmp	.L125
.L429:
	cmpq	$0, 80(%rsp)
	je	.L234
	testl	%r11d, %r11d
	je	.L234
	movq	80(%rsp), %rax
	movq	%rsi, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L104
.L252:
	movl	$7, %eax
.L166:
	movq	16(%rsp), %rdi
	movq	%r14, (%rdi)
	movq	136(%rsp), %rdi
	movq	%rdi, 8(%rsp)
.L164:
	cmpq	8(%rsp), %rdx
	jne	.L153
	cmpq	$5, %rax
	jne	.L152
.L192:
	cmpq	%r10, %rdx
	jne	.L147
.L155:
	subl	$1, 20(%r12)
	jmp	.L147
.L420:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L110
.L435:
	cmpq	$0, 80(%rsp)
	je	.L259
	testb	$8, 16(%r12)
	je	.L194
	movq	%r10, 88(%rsp)
	movl	%r11d, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	movl	48(%rsp), %r11d
	movq	88(%rsp), %r10
	je	.L194
	cmpl	$5, %eax
	jne	.L165
.L253:
	movl	$5, %eax
	jmp	.L166
.L434:
	cmpl	$8220, %ecx
	je	.L169
	jbe	.L449
	cmpl	$8542, %ecx
	ja	.L177
	cmpl	$8539, %ecx
	jnb	.L178
	cmpl	$8482, %ecx
	je	.L179
	cmpl	$8486, %ecx
	je	.L254
	cmpl	$8221, %ecx
	je	.L450
.L168:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L451
	cmpq	$0, 80(%rsp)
	je	.L259
	testb	$8, 16(%r12)
	jne	.L452
.L194:
	testb	$2, %bl
	jne	.L453
.L259:
	movl	$6, %eax
	jmp	.L166
.L228:
	movl	$7, 8(%rsp)
	jmp	.L100
.L430:
	cmpq	$0, 80(%rsp)
	je	.L234
	testl	%r11d, %r11d
	je	.L234
	movq	80(%rsp), %rax
	addq	$2, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L106
.L225:
	movq	%rbp, %rdx
	movq	%r10, %rbx
	movl	$4, 8(%rsp)
	jmp	.L100
.L436:
	movq	136(%rsp), %rdx
	movq	144(%rsp), %rax
	subq	$1, %rcx
	movq	16(%rsp), %rsi
	cmpq	%rcx, %rdx
	movq	%rax, (%rsi)
	je	.L192
.L153:
	leaq	__PRETTY_FUNCTION__.9211(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L449:
	cmpl	$733, %ecx
	movl	$5, %edi
	je	.L171
	jbe	.L454
	cmpl	$8216, %ecx
	je	.L174
	cmpl	$8217, %ecx
	je	.L175
	cmpl	$8212, %ecx
	jne	.L168
	leaq	.LC1(%rip), %rdi
.L180:
	movzbl	(%rdi), %ecx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	jmp	.L196
.L425:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r14, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L45
	addq	$1, %r14
	cmpq	%rbx, %rax
	jbe	.L49
.L48:
	movq	%r14, 144(%rsp)
	movzbl	-1(%r14), %edx
	addq	$1, %r14
	movb	%dl, 4(%r15,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L48
.L49:
	movl	$7, 8(%rsp)
	jmp	.L14
.L443:
	cmpq	$0, 80(%rsp)
	je	.L251
	testl	%ebx, %ebx
	jne	.L455
.L251:
	movl	$6, %eax
	jmp	.L156
.L442:
	leaq	1(%r14), %rsi
	cmpq	%rsi, %rbp
	jbe	.L245
	movzbl	1(%r14), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L456
	leaq	(%r8,%r8,2), %rsi
	cltq
	salq	$5, %rsi
	addq	%rsi, %rax
	leaq	to_ucs4_comb(%rip), %rsi
	movl	(%rsi,%rax,4), %eax
	testl	%eax, %eax
	je	.L457
	addq	$2, %r14
	jmp	.L162
.L177:
	cmpl	$8592, %ecx
	jb	.L168
	cmpl	$8595, %ecx
	jbe	.L182
	cmpl	$9834, %ecx
	leaq	.LC8(%rip), %rdi
	je	.L180
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L426:
	cmpl	$8220, %eax
	je	.L55
	jbe	.L458
	cmpl	$8542, %eax
	ja	.L63
	cmpl	$8539, %eax
	jnb	.L64
	cmpl	$8482, %eax
	je	.L65
	cmpl	$8486, %eax
	je	.L224
	cmpl	$8221, %eax
	je	.L459
.L54:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L394
	cmpq	$0, 80(%rsp)
	je	.L73
	testb	$8, %r11b
	jne	.L460
.L75:
	andb	$2, %r11b
	jne	.L461
	movq	144(%rsp), %rax
.L393:
	cmpq	48(%rsp), %rax
	jne	.L72
.L73:
	movl	$6, 8(%rsp)
	jmp	.L14
.L94:
	cmpl	$0, 8(%rsp)
	jne	.L14
.L395:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r14
	jmp	.L22
.L444:
	movslq	48(%rsp), %rax
	movq	%rbp, %r14
	jmp	.L156
.L455:
	movq	80(%rsp), %rax
	addq	$1, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L161
.L441:
	cmpq	%r10, 8(%rsp)
	je	.L155
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L454:
	cmpl	$711, %ecx
	jne	.L462
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-49, (%rdx)
	movl	$32, %edx
	jmp	.L208
.L453:
	movq	80(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L165
.L182:
	addl	$28, %ecx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rsi
	movb	%cl, 126(%rsp)
	jmp	.L184
.L458:
	cmpl	$733, %eax
	movl	$5, %edx
	je	.L57
	jbe	.L463
	cmpl	$8216, %eax
	je	.L60
	cmpl	$8217, %eax
	je	.L61
	cmpl	$8212, %eax
	jne	.L54
	leaq	.LC1(%rip), %rdx
.L66:
	movzbl	(%rdx), %eax
	leaq	1(%r10), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r10)
	jmp	.L87
.L169:
	leaq	.LC4(%rip), %rdi
	jmp	.L180
.L456:
	cmpq	$0, 80(%rsp)
	je	.L251
	testl	%ebx, %ebx
	je	.L251
	movq	80(%rsp), %rax
	movq	%rsi, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L159
.L450:
	leaq	.LC5(%rip), %rdi
	jmp	.L180
.L254:
	leaq	.LC7(%rip), %rdi
	jmp	.L180
.L179:
	leaq	.LC6(%rip), %rdi
	jmp	.L180
.L178:
	subl	$127, %ecx
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rsi
	movb	%cl, 126(%rsp)
	jmp	.L184
.L462:
	jb	.L168
	leal	-728(%rcx), %edi
	cmpl	$3, %edi
	ja	.L168
.L171:
	leaq	map.9088(%rip), %rcx
	movb	$32, 127(%rsp)
	leaq	126(%rsp), %rsi
	movzbl	(%rcx,%rdi), %ecx
	movb	%cl, 126(%rsp)
	jmp	.L184
.L175:
	leaq	.LC3(%rip), %rdi
	jmp	.L180
.L174:
	leaq	.LC2(%rip), %rdi
	jmp	.L180
.L438:
	testb	%dil, %dil
	je	.L220
.L402:
	cmpq	$0, 80(%rsp)
	movl	$6, 8(%rsp)
	je	.L14
	andl	$2, %r11d
	je	.L14
	movq	80(%rsp), %rax
	movq	%r10, %rcx
	addq	$1, (%rax)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	152(%rsp), %rsi
	addq	$1, %rdx
	leaq	(%rsi,%rdx), %r8
	movq	%r8, 8(%rsp)
	leaq	1(%rsi), %r8
	cmpq	%r8, 8(%rsp)
	jbe	.L464
	movzbl	153(%rsp), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L465
	leaq	(%r9,%r9,2), %rdx
	cltq
	salq	$5, %rdx
	addq	%rdx, %rax
	leaq	to_ucs4_comb(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	leaq	2(%rsi), %rdx
	testl	%eax, %eax
	jne	.L36
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	1(%rsi), %rdx
	jmp	.L36
.L245:
	movl	$7, %eax
	jmp	.L156
.L440:
	cmpq	%r10, 8(%rsp)
	jne	.L153
.L152:
	leaq	__PRETTY_FUNCTION__.9211(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r10, 88(%rsp)
	movl	%r11d, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movl	48(%rsp), %r11d
	movq	88(%rsp), %r10
	je	.L466
	cmpl	$5, %eax
	je	.L467
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L165
.L451:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r14
	jmp	.L165
.L464:
	leaq	2(%rsi), %rcx
	cmpq	%rcx, 8(%rsp)
	je	.L468
	movslq	%ebx, %rbx
	movq	%rdx, %rcx
	andl	$-8, %eax
	subq	%rbx, %rcx
	addq	%rcx, %r14
	movq	16(%rsp), %rcx
	movq	%r14, (%rcx)
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jle	.L469
	cmpq	$4, %rdx
	ja	.L470
	orl	%edx, %eax
	movl	%eax, (%r15)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L49
	movb	%dil, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L49
.L471:
	movzbl	(%rsi,%rax), %edi
	movb	%dil, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L471
	jmp	.L49
.L427:
	movl	%r11d, 108(%rsp)
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r14
	popq	%rdx
	movq	96(%rsp), %r10
	movl	108(%rsp), %r11d
	movq	144(%rsp), %rax
	je	.L472
	cmpl	$5, 8(%rsp)
	movq	%rax, %rdx
	je	.L473
.L77:
	cmpq	48(%rsp), %rdx
	movq	%rdx, %rax
	jne	.L72
	cmpl	$7, 8(%rsp)
	jne	.L94
	addq	$4, %rdx
	cmpq	%rdx, 88(%rsp)
	je	.L474
	movl	(%r15), %eax
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	16(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L475
	cmpq	$4, %rbx
	ja	.L476
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r15)
	je	.L49
	movq	48(%rsp), %rcx
	xorl	%eax, %eax
.L98:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L98
	jmp	.L49
.L64:
	subl	$127, %eax
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdx
	movb	%al, 126(%rsp)
	jmp	.L70
.L63:
	cmpl	$8592, %eax
	jb	.L54
	cmpl	$8595, %eax
	jbe	.L68
	cmpl	$9834, %eax
	leaq	.LC8(%rip), %rdx
	je	.L66
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	.LC7(%rip), %rdx
	jmp	.L66
.L65:
	leaq	.LC6(%rip), %rdx
	jmp	.L66
.L68:
	addl	$28, %eax
	movb	$0, 127(%rsp)
	leaq	126(%rsp), %rdx
	movb	%al, 126(%rsp)
	jmp	.L70
.L463:
	cmpl	$711, %eax
	jne	.L477
	leaq	1(%r10), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%r10)
	movl	$32, %eax
	jmp	.L206
.L477:
	jb	.L54
	leal	-728(%rax), %edx
	cmpl	$3, %edx
	ja	.L54
.L57:
	leaq	map.9136(%rip), %rax
	movb	$32, 127(%rsp)
	movzbl	(%rax,%rdx), %eax
	leaq	126(%rsp), %rdx
	movb	%al, 126(%rsp)
	jmp	.L70
.L457:
	cmpq	$0, 80(%rsp)
	je	.L251
	testl	%ebx, %ebx
	je	.L251
	movq	80(%rsp), %rax
	addq	$2, %r14
	movl	$6, 48(%rsp)
	addq	$1, (%rax)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L472:
	andl	$2, %r11d
	je	.L393
	movq	80(%rsp), %rdi
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	cmpq	48(%rsp), %rax
	je	.L14
	jmp	.L72
.L476:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L475:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L203:
	leaq	__PRETTY_FUNCTION__.9211(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L42:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L467:
	movq	136(%rsp), %rdx
	cmpq	152(%rsp), %rdx
	movq	144(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L192
	jmp	.L153
.L466:
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L194
.L448:
	leaq	__PRETTY_FUNCTION__.9211(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L25:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L469:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L468:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L55:
	leaq	.LC4(%rip), %rdx
	jmp	.L66
.L473:
	cmpq	48(%rsp), %rax
	jne	.L72
	jmp	.L89
.L474:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L423:
	leaq	__PRETTY_FUNCTION__.9211(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L465:
	cmpq	$0, 80(%rsp)
	movl	$6, 8(%rsp)
	je	.L14
	andb	$2, %r11b
	je	.L14
	movq	80(%rsp), %rax
	movq	%r10, %rcx
	movq	%r8, %rdx
	addq	$1, (%rax)
	jmp	.L35
.L460:
	movl	%r11d, 108(%rsp)
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdx
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rcx
	popq	%rsi
	movl	108(%rsp), %r11d
	je	.L75
	cmpl	$5, %eax
	je	.L76
	movq	144(%rsp), %rdx
	movq	96(%rsp), %r10
	jmp	.L77
.L459:
	leaq	.LC5(%rip), %rdx
	jmp	.L66
.L76:
	movq	144(%rsp), %rax
	cmpq	48(%rsp), %rax
	jne	.L72
	jmp	.L89
.L446:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L45:
	leaq	__PRETTY_FUNCTION__.9118(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L461:
	movq	80(%rsp), %rax
	addq	$1, (%rax)
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	48(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L72
	jmp	.L73
.L439:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L61:
	leaq	.LC3(%rip), %rdx
	jmp	.L66
.L60:
	leaq	.LC2(%rip), %rdx
	jmp	.L66
.L470:
	leaq	__PRETTY_FUNCTION__.9014(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata
	.type	map.9088, @object
	.size	map.9088, 6
map.9088:
	.string	"\306\307\312\316"
	.ascii	"\315"
	.set	map.9136,map.9088
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9118, @object
	.size	__PRETTY_FUNCTION__.9118, 18
__PRETTY_FUNCTION__.9118:
	.string	"to_iso6937_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9014, @object
	.size	__PRETTY_FUNCTION__.9014, 20
__PRETTY_FUNCTION__.9014:
	.string	"from_iso6937_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9211, @object
	.size	__PRETTY_FUNCTION__.9211, 6
__PRETTY_FUNCTION__.9211:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 766
from_ucs4:
	.string	""
	.string	""
	.string	"\001"
	.string	"\002"
	.string	"\003"
	.string	"\004"
	.string	"\005"
	.string	"\006"
	.string	"\007"
	.string	"\b"
	.string	"\t"
	.string	"\n"
	.string	"\013"
	.string	"\f"
	.string	"\r"
	.string	"\016"
	.string	"\017"
	.string	"\020"
	.string	"\021"
	.string	"\022"
	.string	"\023"
	.string	"\024"
	.string	"\025"
	.string	"\026"
	.string	"\027"
	.string	"\030"
	.string	"\031"
	.string	"\032"
	.string	"\033"
	.string	"\034"
	.string	"\035"
	.string	"\036"
	.string	"\037"
	.string	" "
	.string	"!"
	.string	"\""
	.string	"#"
	.string	"$"
	.string	"%"
	.string	"&"
	.string	"'"
	.string	"("
	.string	")"
	.string	"*"
	.string	"+"
	.string	","
	.string	"-"
	.string	"."
	.string	"/"
	.string	"0"
	.string	"1"
	.string	"2"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"6"
	.string	"7"
	.string	"8"
	.string	"9"
	.string	":"
	.string	";"
	.string	"<"
	.string	"="
	.string	">"
	.string	"?"
	.string	"@"
	.string	"A"
	.string	"B"
	.string	"C"
	.string	"D"
	.string	"E"
	.string	"F"
	.string	"G"
	.string	"H"
	.string	"I"
	.string	"J"
	.string	"K"
	.string	"L"
	.string	"M"
	.string	"N"
	.string	"O"
	.string	"P"
	.string	"Q"
	.string	"R"
	.string	"S"
	.string	"T"
	.string	"U"
	.string	"V"
	.string	"W"
	.string	"X"
	.string	"Y"
	.string	"Z"
	.string	"["
	.string	"\\"
	.string	"]"
	.string	"^"
	.string	"_"
	.string	"`"
	.string	"a"
	.string	"b"
	.string	"c"
	.string	"d"
	.string	"e"
	.string	"f"
	.string	"g"
	.string	"h"
	.string	"i"
	.string	"j"
	.string	"k"
	.string	"l"
	.string	"m"
	.string	"n"
	.string	"o"
	.string	"p"
	.string	"q"
	.string	"r"
	.string	"s"
	.string	"t"
	.string	"u"
	.string	"v"
	.string	"w"
	.string	"x"
	.string	"y"
	.string	"z"
	.string	"{"
	.string	"|"
	.string	"}"
	.string	"~"
	.string	"\177"
	.string	"\200"
	.string	"\201"
	.string	"\202"
	.string	"\203"
	.string	"\204"
	.string	"\205"
	.string	"\206"
	.string	"\207"
	.string	"\210"
	.string	"\211"
	.string	"\212"
	.string	"\213"
	.string	"\214"
	.string	"\215"
	.string	"\216"
	.string	"\217"
	.string	"\220"
	.string	"\221"
	.string	"\222"
	.string	"\223"
	.string	"\224"
	.string	"\225"
	.string	"\226"
	.string	"\227"
	.string	"\230"
	.string	"\231"
	.string	"\232"
	.string	"\233"
	.string	"\234"
	.string	"\235"
	.string	"\236"
	.string	"\237"
	.string	"\240"
	.string	"\241"
	.string	"\242"
	.string	"\243"
	.string	"\250"
	.string	"\245"
	.string	"\327"
	.string	"\247"
	.ascii	"\310 "
	.string	"\323"
	.string	"\343"
	.string	"\253"
	.string	"\326"
	.string	"\377"
	.string	"\322"
	.ascii	"\305 "
	.string	"\260"
	.string	"\261"
	.string	"\262"
	.string	"\263"
	.ascii	"\302 "
	.string	"\265"
	.string	"\266"
	.string	"\267"
	.ascii	"\313 "
	.string	"\321"
	.string	"\353"
	.string	"\273"
	.string	"\274"
	.string	"\275"
	.string	"\276"
	.string	"\277"
	.ascii	"\301A"
	.ascii	"\302A"
	.ascii	"\303A"
	.ascii	"\304A"
	.ascii	"\310A"
	.ascii	"\312A"
	.string	"\341"
	.ascii	"\313C"
	.ascii	"\301E"
	.ascii	"\302E"
	.ascii	"\303E"
	.ascii	"\310E"
	.ascii	"\301I"
	.ascii	"\302I"
	.ascii	"\303I"
	.ascii	"\310I"
	.string	"\342"
	.ascii	"\304N"
	.ascii	"\301O"
	.ascii	"\302O"
	.ascii	"\303O"
	.ascii	"\304O"
	.ascii	"\310O"
	.string	"\264"
	.string	"\351"
	.ascii	"\301U"
	.ascii	"\302U"
	.ascii	"\303U"
	.ascii	"\310U"
	.ascii	"\302Y"
	.string	"\354"
	.string	"\373"
	.ascii	"\301a"
	.ascii	"\302a"
	.ascii	"\303a"
	.ascii	"\304a"
	.ascii	"\310a"
	.ascii	"\312a"
	.string	"\361"
	.ascii	"\313c"
	.ascii	"\301e"
	.ascii	"\302e"
	.ascii	"\303e"
	.ascii	"\310e"
	.ascii	"\301i"
	.ascii	"\302i"
	.ascii	"\303i"
	.ascii	"\310i"
	.string	"\363"
	.ascii	"\304n"
	.ascii	"\301o"
	.ascii	"\302o"
	.ascii	"\303o"
	.ascii	"\304o"
	.ascii	"\310o"
	.string	"\270"
	.string	"\371"
	.ascii	"\301u"
	.ascii	"\302u"
	.ascii	"\303u"
	.ascii	"\310u"
	.ascii	"\302y"
	.string	"\374"
	.ascii	"\310y"
	.ascii	"\305A"
	.ascii	"\305a"
	.ascii	"\306A"
	.ascii	"\306a"
	.ascii	"\316A"
	.ascii	"\316a"
	.ascii	"\302C"
	.ascii	"\302c"
	.ascii	"\303C"
	.ascii	"\303c"
	.ascii	"\307C"
	.ascii	"\307c"
	.ascii	"\317C"
	.ascii	"\317c"
	.ascii	"\317D"
	.ascii	"\317d"
	.string	""
	.string	""
	.string	"\362"
	.ascii	"\305E"
	.ascii	"\305e"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\307E"
	.ascii	"\307e"
	.ascii	"\316E"
	.ascii	"\316e"
	.ascii	"\317E"
	.ascii	"\317e"
	.ascii	"\303G"
	.ascii	"\303g"
	.ascii	"\306G"
	.ascii	"\306g"
	.ascii	"\307G"
	.ascii	"\307g"
	.ascii	"\313G"
	.ascii	"\313g"
	.ascii	"\303H"
	.ascii	"\303h"
	.string	"\344"
	.string	"\364"
	.ascii	"\304I"
	.ascii	"\304i"
	.ascii	"\305I"
	.ascii	"\305i"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\316I"
	.ascii	"\316i"
	.ascii	"\307I"
	.string	"\365"
	.string	"\346"
	.string	"\366"
	.ascii	"\303J"
	.ascii	"\303j"
	.ascii	"\313K"
	.ascii	"\313k"
	.string	"\360"
	.ascii	"\302L"
	.ascii	"\302l"
	.ascii	"\313L"
	.ascii	"\313l"
	.ascii	"\317L"
	.ascii	"\317l"
	.string	"\347"
	.string	"\367"
	.string	"\350"
	.string	"\370"
	.ascii	"\302N"
	.ascii	"\302n"
	.ascii	"\313N"
	.ascii	"\313n"
	.ascii	"\317N"
	.ascii	"\317n"
	.string	"\357"
	.string	"\356"
	.string	"\376"
	.ascii	"\305O"
	.ascii	"\305o"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\315O"
	.ascii	"\315o"
	.string	"\352"
	.string	"\372"
	.ascii	"\302R"
	.ascii	"\302r"
	.ascii	"\313R"
	.ascii	"\313r"
	.ascii	"\317R"
	.ascii	"\317r"
	.ascii	"\302S"
	.ascii	"\302s"
	.ascii	"\303S"
	.ascii	"\303s"
	.ascii	"\313S"
	.ascii	"\313s"
	.ascii	"\317S"
	.ascii	"\317s"
	.ascii	"\313T"
	.ascii	"\313t"
	.ascii	"\317T"
	.ascii	"\317t"
	.string	"\355"
	.string	"\375"
	.ascii	"\304U"
	.ascii	"\304u"
	.ascii	"\305U"
	.ascii	"\305u"
	.ascii	"\306U"
	.ascii	"\306u"
	.ascii	"\312U"
	.ascii	"\312u"
	.ascii	"\315U"
	.ascii	"\315u"
	.ascii	"\316U"
	.ascii	"\316u"
	.ascii	"\303W"
	.ascii	"\303w"
	.ascii	"\303Y"
	.ascii	"\303y"
	.ascii	"\310Y"
	.ascii	"\302Z"
	.ascii	"\302z"
	.ascii	"\307Z"
	.ascii	"\307z"
	.ascii	"\317Z"
	.ascii	"\317z"
	.align 32
	.type	to_ucs4_comb, @object
	.size	to_ucs4_comb, 5760
to_ucs4_comb:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	192
	.long	0
	.long	0
	.long	0
	.long	200
	.long	0
	.long	0
	.long	0
	.long	204
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	210
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	217
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	224
	.long	0
	.long	0
	.long	0
	.long	232
	.long	0
	.long	0
	.long	0
	.long	236
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	242
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	249
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	180
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	193
	.long	0
	.long	262
	.long	0
	.long	201
	.long	0
	.long	0
	.long	0
	.long	205
	.long	0
	.long	0
	.long	313
	.long	0
	.long	323
	.long	211
	.long	0
	.long	0
	.long	340
	.long	346
	.long	0
	.long	218
	.long	0
	.long	0
	.long	0
	.long	221
	.long	377
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	225
	.long	0
	.long	263
	.long	0
	.long	233
	.long	0
	.long	0
	.long	0
	.long	237
	.long	0
	.long	0
	.long	314
	.long	0
	.long	324
	.long	243
	.long	0
	.long	0
	.long	341
	.long	347
	.long	0
	.long	250
	.long	0
	.long	0
	.long	0
	.long	253
	.long	378
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	194
	.long	0
	.long	264
	.long	0
	.long	202
	.long	0
	.long	284
	.long	292
	.long	206
	.long	308
	.long	0
	.long	0
	.long	0
	.long	0
	.long	212
	.long	0
	.long	0
	.long	0
	.long	348
	.long	0
	.long	219
	.long	0
	.long	372
	.long	0
	.long	374
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	226
	.long	0
	.long	265
	.long	0
	.long	234
	.long	0
	.long	285
	.long	293
	.long	238
	.long	309
	.long	0
	.long	0
	.long	0
	.long	0
	.long	244
	.long	0
	.long	0
	.long	0
	.long	349
	.long	0
	.long	251
	.long	0
	.long	373
	.long	0
	.long	375
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	195
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	296
	.long	0
	.long	0
	.long	0
	.long	0
	.long	209
	.long	213
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	360
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	227
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	297
	.long	0
	.long	0
	.long	0
	.long	0
	.long	241
	.long	245
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	361
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	175
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	256
	.long	0
	.long	0
	.long	0
	.long	274
	.long	0
	.long	0
	.long	0
	.long	298
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	332
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	362
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	257
	.long	0
	.long	0
	.long	0
	.long	275
	.long	0
	.long	0
	.long	0
	.long	299
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	333
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	363
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	728
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	258
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	286
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	364
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	259
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	287
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	365
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	729
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	266
	.long	0
	.long	278
	.long	0
	.long	288
	.long	0
	.long	304
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	379
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	267
	.long	0
	.long	279
	.long	0
	.long	289
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	380
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	168
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	196
	.long	0
	.long	0
	.long	0
	.long	203
	.long	0
	.long	0
	.long	0
	.long	207
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	214
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	220
	.long	0
	.long	0
	.long	0
	.long	376
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	228
	.long	0
	.long	0
	.long	0
	.long	235
	.long	0
	.long	0
	.long	0
	.long	239
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	246
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	252
	.long	0
	.long	0
	.long	0
	.long	255
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	730
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	197
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	366
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	229
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	367
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	184
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	199
	.long	0
	.long	0
	.long	0
	.long	290
	.long	0
	.long	0
	.long	0
	.long	310
	.long	315
	.long	0
	.long	325
	.long	0
	.long	0
	.long	0
	.long	342
	.long	350
	.long	354
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	231
	.long	0
	.long	0
	.long	0
	.long	291
	.long	0
	.long	0
	.long	0
	.long	311
	.long	316
	.long	0
	.long	326
	.long	0
	.long	0
	.long	0
	.long	343
	.long	351
	.long	355
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	733
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	336
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	368
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	337
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	369
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	731
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	260
	.long	0
	.long	0
	.long	0
	.long	280
	.long	0
	.long	0
	.long	0
	.long	302
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	370
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	261
	.long	0
	.long	0
	.long	0
	.long	281
	.long	0
	.long	0
	.long	0
	.long	303
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	371
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	711
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	268
	.long	270
	.long	282
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	317
	.long	0
	.long	327
	.long	0
	.long	0
	.long	0
	.long	344
	.long	352
	.long	356
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	381
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	269
	.long	271
	.long	283
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	318
	.long	0
	.long	328
	.long	0
	.long	0
	.long	0
	.long	345
	.long	353
	.long	357
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	382
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	160
	.long	161
	.long	162
	.long	163
	.long	0
	.long	165
	.long	0
	.long	167
	.long	164
	.long	8216
	.long	8220
	.long	171
	.long	8592
	.long	8593
	.long	8594
	.long	8595
	.long	176
	.long	177
	.long	178
	.long	179
	.long	215
	.long	181
	.long	182
	.long	183
	.long	247
	.long	8217
	.long	8221
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8212
	.long	185
	.long	174
	.long	169
	.long	8482
	.long	9834
	.long	172
	.long	166
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8539
	.long	8540
	.long	8541
	.long	8542
	.long	8486
	.long	198
	.long	208
	.long	170
	.long	294
	.long	0
	.long	306
	.long	319
	.long	321
	.long	216
	.long	338
	.long	186
	.long	222
	.long	358
	.long	330
	.long	329
	.long	312
	.long	230
	.long	273
	.long	240
	.long	295
	.long	305
	.long	307
	.long	320
	.long	322
	.long	248
	.long	339
	.long	223
	.long	254
	.long	359
	.long	331
	.long	173
