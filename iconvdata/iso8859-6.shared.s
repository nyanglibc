	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	testb	%sil, %sil
	movl	(%rax,%rdx,4), %eax
	je	.L1
	testl	%eax, %eax
	movl	$-1, %edx
	cmove	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-8859-6//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r14d
	movq	%rdi, 40(%rsp)
	leaq	48(%r12), %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 16(%rsp)
	testb	$1, %r14b
	movl	208(%rsp), %ebx
	movq	%rsi, 80(%rsp)
	movq	%rdi, 72(%rsp)
	movq	$0, 64(%rsp)
	jne	.L15
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 64(%rsp)
	je	.L15
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 64(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L216
	movq	8(%rsp), %rax
	movq	32(%rsp), %rcx
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %ebx
	movq	8(%r12), %r10
	testq	%rcx, %rcx
	movq	(%rax), %r13
	movq	%rcx, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 96(%rsp)
	movq	40(%rsp), %rax
	setne	95(%rsp)
	movzbl	95(%rsp), %esi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L113
	testb	%sil, %sil
	je	.L113
	movq	32(%r12), %rbx
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jne	.L217
.L113:
	movq	$0, 24(%rsp)
.L22:
	leaq	136(%rsp), %rsi
	movq	%rsi, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L103:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rcx
	addq	%rcx, 24(%rsp)
.L53:
	testq	%rax, %rax
	je	.L218
	leaq	128(%rsp), %rsi
	movq	%r13, 128(%rsp)
	movq	%r15, %rbx
	movq	%r15, 136(%rsp)
	movq	%r13, %rax
	movl	$4, %r11d
	andl	$2, %r14d
	movq	%rsi, 48(%rsp)
.L61:
	cmpq	%rax, %rbp
	je	.L62
.L69:
	leaq	4(%rax), %rsi
	cmpq	%rsi, %rbp
	jb	.L121
	cmpq	%rbx, %r10
	jbe	.L122
	movl	(%rax), %edx
	cmpl	$1618, %edx
	ja	.L63
	leaq	from_ucs4(%rip), %rdi
	movl	%edx, %ecx
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L64
	testl	%edx, %edx
	jne	.L63
.L64:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L69
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L219
.L70:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L220
	cmpq	%rbx, %r15
	jnb	.L125
	movq	64(%rsp), %r14
	movq	(%r12), %rax
	movl	%r11d, 56(%rsp)
	movq	%r10, 48(%rsp)
	movq	%r14, %rdi
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r14
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r14d
	popq	%rdi
	movq	48(%rsp), %r10
	movl	56(%rsp), %r11d
	je	.L74
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L221
.L73:
	testl	%r14d, %r14d
	jne	.L136
.L102:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rdi
	movl	16(%r12), %r14d
	movq	(%r12), %r15
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	(%rdi), %r13
	movq	96(%rax), %rax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L63:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L222
	cmpq	$0, 96(%rsp)
	je	.L124
	testb	$8, 16(%r12)
	jne	.L223
.L67:
	testl	%r14d, %r14d
	jne	.L224
.L124:
	movl	$6, %r11d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L218:
	cmpq	%r13, %rbp
	je	.L116
	leaq	4(%r15), %rdx
	cmpq	%rdx, %r10
	jb	.L117
	movq	%r13, %rax
	movq	%r15, %rbx
	movl	$4, %r11d
	andl	$2, %r14d
	.p2align 4,,10
	.p2align 3
.L56:
	movzbl	(%rax), %esi
	leaq	to_ucs4(%rip), %r9
	movq	%rax, %rdi
	movq	%rsi, %rcx
	movl	(%r9,%rsi,4), %esi
	testb	%cl, %cl
	je	.L57
	testl	%esi, %esi
	je	.L225
.L57:
	addq	$1, %rax
	movl	%esi, (%rbx)
	movq	%rdx, %rbx
	movq	%rax, %rdi
.L58:
	cmpq	%rax, %rbp
	je	.L55
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r10
	jnb	.L56
	movl	$5, %r11d
.L55:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
	je	.L70
.L219:
	movq	32(%rsp), %rax
	movl	%r11d, %r8d
	movq	%rbx, (%rax)
.L14:
	addq	$152, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movl	$7, %r11d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$5, %r11d
	movl	%r11d, %r14d
	jne	.L73
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$5, %r11d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L220:
	movq	16(%rsp), %rdi
	movq	%rbx, (%r12)
	movl	%r11d, %r8d
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L72:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 95(%rsp)
	je	.L14
	cmpl	$7, %r8d
	jne	.L14
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L105
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L107
.L106:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L106
.L107:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L125:
	movl	%r11d, %r14d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L225:
	cmpq	$0, 96(%rsp)
	je	.L120
	testl	%r14d, %r14d
	jne	.L226
.L120:
	movl	$6, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r10, 56(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	120(%rsp), %r9
	movq	64(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r11d
	popq	%r8
	cmpl	$6, %r11d
	popq	%r9
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movq	56(%rsp), %r10
	je	.L67
	cmpl	$5, %r11d
	jne	.L61
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rsi, 128(%rsp)
	movq	%rsi, %rax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L224:
	movq	96(%rsp), %rdi
	addq	$4, %rax
	movl	$6, %r11d
	movq	%rax, 128(%rsp)
	addq	$1, (%rdi)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L221:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L76
	movq	(%rsi), %rax
.L76:
	addq	112(%rsp), %rax
	cmpq	24(%rsp), %rax
	movq	8(%rsp), %rax
	je	.L227
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	movl	16(%r12), %ecx
	cmpq	$0, 96(%rax)
	je	.L228
	andl	$2, %ecx
	leaq	128(%rsp), %rbx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
	movl	%ecx, 24(%rsp)
.L91:
	cmpq	%r13, %rbp
	je	.L229
.L99:
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rbp
	jb	.L130
	cmpq	%rdx, %r11
	jbe	.L133
	movl	0(%r13), %ecx
	cmpl	$1618, %ecx
	ja	.L93
	leaq	from_ucs4(%rip), %r8
	movl	%ecx, %esi
	movzbl	(%r8,%rsi), %esi
	testb	%sil, %sil
	jne	.L94
	testl	%ecx, %ecx
	jne	.L93
.L94:
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%sil, (%rdx)
	movq	128(%rsp), %rcx
	movq	136(%rsp), %rdx
	leaq	4(%rcx), %r13
	cmpq	%r13, %rbp
	movq	%r13, 128(%rsp)
	jne	.L99
.L229:
	movslq	%eax, %rdi
	movq	%rbp, %r13
.L92:
	movq	8(%rsp), %rax
	movq	120(%rsp), %r11
	movq	%r13, (%rax)
.L90:
	cmpq	%r11, %rdx
	jne	.L82
	cmpq	$5, %rdi
	jne	.L81
	cmpq	%rdx, %r15
	jne	.L73
.L84:
	subl	$1, 20(%r12)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L217:
	testq	%rcx, %rcx
	jne	.L230
	cmpl	$4, %edx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	ja	.L24
	leaq	120(%rsp), %rdi
	movslq	%edx, %rdx
	xorl	%eax, %eax
	movq	%rdx, %r11
	movq	%rdi, 24(%rsp)
.L25:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L25
	movq	%r13, %rcx
	subq	%rax, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbp
	jb	.L231
	cmpq	%r10, %r15
	movl	$5, %r8d
	jnb	.L14
	leaq	1(%r13), %rax
	leaq	119(%rsp), %r8
.L33:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r11
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	$3, %r11
	movb	%cl, (%r8,%r11)
	ja	.L138
	cmpq	%rdi, %rbp
	ja	.L33
.L138:
	movq	24(%rsp), %rax
	movq	%rax, 128(%rsp)
	movl	120(%rsp), %eax
	cmpl	$1618, %eax
	ja	.L35
	leaq	from_ucs4(%rip), %rdi
	movl	%eax, %ecx
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L36
	testl	%eax, %eax
	je	.L36
.L35:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L232
	cmpq	$0, 96(%rsp)
	je	.L114
	testb	$8, %r14b
	jne	.L233
	andl	$2, %r14d
	movl	$6, %r8d
	movq	24(%rsp), %rax
	je	.L14
.L43:
	movq	96(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L44:
	cmpq	24(%rsp), %rax
	jne	.L215
.L114:
	movl	$6, %r8d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L227:
	movq	40(%rsp), %rsi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rsi)
	je	.L234
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L73
.L36:
	leaq	1(%r15), %rax
	movq	%rax, 136(%rsp)
	movb	%cl, (%r15)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	24(%rsp), %rax
	movq	%rax, 128(%rsp)
	je	.L45
.L215:
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
.L38:
	subq	24(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L235
	movq	8(%rsp), %rdi
	subq	%rdx, %rax
	andl	$-8, %esi
	movq	136(%rsp), %r15
	movl	16(%r12), %r14d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r13
	movq	112(%rsp), %rax
	movl	%esi, (%rbx)
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r13, %rdi
	movq	%r15, %rbx
	movl	$5, %r11d
	jmp	.L55
.L226:
	movq	96(%rsp), %rsi
	leaq	1(%rax), %rax
	movl	$6, %r11d
	movq	%rax, %rdi
	addq	$1, (%rsi)
	jmp	.L58
.L216:
	cmpq	$0, 32(%rsp)
	jne	.L236
	movq	32(%r12), %rax
	xorl	%r8d, %r8d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	64(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rbp
	movl	%eax, %r8d
	popq	%r12
	jmp	.L14
.L136:
	movl	%r14d, %r8d
	jmp	.L72
.L234:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L73
.L116:
	movq	%rbp, %rdi
	movq	%r15, %rbx
	movl	$4, %r11d
	jmp	.L55
.L228:
	cmpq	%r13, %rbp
	je	.L237
	leaq	4(%r15), %rax
	cmpq	%r11, %rax
	ja	.L238
	andl	$2, %ecx
	movq	%r15, %rdx
	movl	$4, %edi
	movl	%ecx, %r9d
	movq	%r13, %rcx
	.p2align 4,,10
	.p2align 3
.L85:
	movzbl	(%rcx), %esi
	leaq	to_ucs4(%rip), %r13
	movq	%rcx, %r8
	movq	%rsi, %rbx
	movl	0(%r13,%rsi,4), %esi
	testl	%esi, %esi
	jne	.L87
	testb	%bl, %bl
	jne	.L239
.L87:
	addq	$1, %rcx
	movl	%esi, (%rdx)
	movq	%rax, %rdx
	movq	%rcx, %r8
.L88:
	cmpq	%rcx, %rbp
	je	.L240
	leaq	4(%rdx), %rax
	cmpq	%rax, %r11
	jnb	.L85
	movl	$5, %edi
.L86:
	movq	8(%rsp), %rax
	movq	%r8, (%rax)
	jmp	.L90
.L93:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L241
	cmpq	$0, 96(%rsp)
	je	.L134
	testb	$8, 16(%r12)
	jne	.L242
.L97:
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jne	.L243
.L134:
	movl	$6, %edi
	jmp	.L92
.L130:
	movl	$7, %edi
	jmp	.L92
.L242:
	movq	%r11, 56(%rsp)
	movq	%r10, 48(%rsp)
	subq	$8, %rsp
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	%rbx, %rcx
	movq	120(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rsi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %rdx
	movq	48(%rsp), %r10
	movq	56(%rsp), %r11
	je	.L97
	cmpl	$5, %eax
	jne	.L91
.L133:
	movl	$5, %edi
	jmp	.L92
.L231:
	movq	%rbp, %rdx
	movq	8(%rsp), %rsi
	subq	%r13, %rdx
	addq	%rax, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rsi)
	ja	.L27
	addq	$1, %r13
	cmpq	%rax, %rdx
	jbe	.L29
.L30:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %eax
	addq	$1, %r13
	movb	%al, 4(%rbx,%r11)
	addq	$1, %r11
	cmpq	%r11, %rdx
	jne	.L30
.L29:
	movl	$7, %r8d
	jmp	.L14
.L239:
	cmpq	$0, 96(%rsp)
	je	.L129
	testl	%r9d, %r9d
	jne	.L244
.L129:
	movl	$6, %edi
	jmp	.L86
.L243:
	movq	96(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L91
.L241:
	movq	%rdi, 128(%rsp)
	movq	%rdi, %r13
	jmp	.L91
.L240:
	movq	%rbp, %r8
	jmp	.L86
.L48:
	testl	%r8d, %r8d
	jne	.L14
.L45:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rbx
	movl	16(%r12), %r14d
	movq	%rax, 24(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r13
	movq	96(%rax), %rax
	jmp	.L22
.L238:
	cmpq	%r15, %r11
	je	.L84
.L82:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	movq	96(%rsp), %rax
	leaq	1(%rcx), %rcx
	movl	$6, %edi
	movq	%rcx, %r8
	addq	$1, (%rax)
	jmp	.L88
.L232:
	movq	24(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L38
.L233:
	movq	24(%rsp), %rax
	movq	%r10, 104(%rsp)
	leaq	128(%rsp), %rcx
	movq	%r11, 56(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	addq	%r11, %rax
	movq	%rax, 56(%rsp)
	pushq	104(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r8d
	popq	%r11
	movq	56(%rsp), %r11
	movq	104(%rsp), %r10
	je	.L245
	movq	128(%rsp), %rax
	cmpq	24(%rsp), %rax
	jne	.L215
	cmpl	$7, %r8d
	jne	.L48
	movq	24(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 48(%rsp)
	je	.L246
	movl	(%rbx), %eax
	movq	8(%rsp), %rdi
	movq	%r11, %rsi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, (%rdi)
	cmpq	%rdx, %r11
	jle	.L247
	cmpq	$4, %r11
	ja	.L248
	orl	%r11d, %eax
	testq	%r11, %r11
	movl	%eax, (%rbx)
	je	.L29
	movq	24(%rsp), %rcx
	xorl	%eax, %eax
.L52:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r11
	jne	.L52
	jmp	.L29
.L237:
	cmpq	%r15, %r11
	jne	.L82
.L81:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	andl	$2, %r14d
	movq	128(%rsp), %rax
	je	.L44
	jmp	.L43
.L248:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L247:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L246:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L236:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L105:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L235:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L27:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L24:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L230:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9064, @object
	.size	__PRETTY_FUNCTION__.9064, 18
__PRETTY_FUNCTION__.9064:
	.string	"to_generic_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9138, @object
	.size	__PRETTY_FUNCTION__.9138, 6
__PRETTY_FUNCTION__.9138:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 1619
from_ucs4:
	.zero	1
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	64
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	95
	.byte	96
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	123
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	-96
	.zero	3
	.byte	-92
	.zero	8
	.byte	-83
	.zero	1374
	.byte	-84
	.zero	14
	.byte	-69
	.zero	3
	.byte	-65
	.zero	1
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	-48
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.zero	5
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-16
	.byte	-15
	.byte	-14
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.zero	4
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	160
	.zero	12
	.long	164
	.zero	28
	.long	1548
	.long	173
	.zero	52
	.long	1563
	.zero	12
	.long	1567
	.zero	4
	.long	1569
	.long	1570
	.long	1571
	.long	1572
	.long	1573
	.long	1574
	.long	1575
	.long	1576
	.long	1577
	.long	1578
	.long	1579
	.long	1580
	.long	1581
	.long	1582
	.long	1583
	.long	1584
	.long	1585
	.long	1586
	.long	1587
	.long	1588
	.long	1589
	.long	1590
	.long	1591
	.long	1592
	.long	1593
	.long	1594
	.zero	20
	.long	1600
	.long	1601
	.long	1602
	.long	1603
	.long	1604
	.long	1605
	.long	1606
	.long	1607
	.long	1608
	.long	1609
	.long	1610
	.long	1611
	.long	1612
	.long	1613
	.long	1614
	.long	1615
	.long	1616
	.long	1617
	.long	1618
	.zero	52
