	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	jne	.L1
	testb	%sil, %sil
	movl	$-1, %edx
	cmovne	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"T.61-8BIT//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$12, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$12, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 56(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	testb	$1, %r11b
	movq	%r9, 48(%rsp)
	movl	224(%rsp), %ebx
	movq	%rdi, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L15
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L341
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	movl	232(%rsp), %r9d
	movq	8(%r12), %r13
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 48(%rsp)
	movq	(%rax), %r10
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%r9d, %r9d
	movq	%rax, 80(%rsp)
	jne	.L342
.L22:
	movq	40(%rsp), %rax
	movq	%r10, %r15
	cmpq	$0, 96(%rax)
	je	.L343
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	152(%rsp), %rdi
	leaq	144(%rsp), %r10
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rbx
	movq	%r14, %rax
	movl	$4, 8(%rsp)
	movq	%rdi, 72(%rsp)
.L95:
	cmpq	%rax, %rbp
	je	.L96
.L114:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L181
	cmpq	%rbx, %r13
	jbe	.L182
	movl	(%rax), %edx
	cmpl	$382, %edx
	ja	.L344
	leaq	from_ucs4(%rip), %rdi
	movl	%edx, %ecx
	testl	%edx, %edx
	leaq	(%rdi,%rcx,2), %rcx
	movzbl	(%rcx), %esi
	je	.L108
	testb	%sil, %sil
	je	.L345
.L108:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%sil, (%rbx)
	movzbl	1(%rcx), %eax
	testb	%al, %al
	je	.L111
.L163:
	movq	152(%rsp), %rbx
	cmpq	%rbx, %r13
	jbe	.L346
	leaq	1(%rbx), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%rbx)
.L111:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L114
	.p2align 4,,10
	.p2align 3
.L96:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L347
.L115:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L348
	cmpq	%rbx, %r15
	jnb	.L187
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r8
	popq	%r9
	je	.L119
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L349
.L118:
	testl	%r11d, %r11d
	jne	.L203
.L157:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r15
	movq	(%rax), %r14
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L85
.L343:
	cmpq	%r14, %rbp
	je	.L173
	leaq	4(%r15), %rdx
	movq	%r14, %rax
	movq	%r15, %rbx
	cmpq	%r13, %rdx
	ja	.L182
	leaq	to_ucs4_comb(%rip), %r8
	movl	$4, 8(%rsp)
	andl	$2, %r11d
	.p2align 4,,10
	.p2align 3
.L87:
	movzbl	(%rax), %ecx
	leal	-193(%rcx), %esi
	movl	%ecx, %edi
	cmpl	$14, %esi
	jbe	.L350
	leaq	to_ucs4(%rip), %rsi
	movl	%ecx, %ecx
	movl	(%rsi,%rcx,4), %esi
	movl	$1, %ecx
.L91:
	testl	%esi, %esi
	jne	.L92
	testb	%dil, %dil
	jne	.L351
.L92:
	movl	%esi, (%rbx)
	addq	%rcx, %rax
	movq	%rdx, %rbx
.L93:
	cmpq	%rax, %rbp
	je	.L96
.L90:
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r13
	jnb	.L87
.L182:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movl	$5, 8(%rsp)
	movq	%rax, (%rdi)
	je	.L115
.L347:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L14:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movl	$7, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L119:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L118
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L344:
	cmpl	$8486, %edx
	je	.L98
	cmpl	$711, %edx
	je	.L99
	leal	-728(%rdx), %esi
	cmpl	$5, %esi
	ja	.L207
	cmpl	$732, %edx
	je	.L207
	leaq	map.9086(%rip), %rax
	movb	$32, 137(%rsp)
	leaq	136(%rsp), %rcx
	movzbl	(%rax,%rsi), %esi
	movb	%sil, 136(%rsp)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L342:
	movq	32(%r12), %r15
	movl	(%r15), %edx
	movl	%edx, %eax
	andl	$7, %eax
	je	.L22
	testq	%rdi, %rdi
	jne	.L352
	movq	40(%rsp), %rsi
	cmpq	$0, 96(%rsi)
	je	.L353
	cmpl	$4, %eax
	movq	%r14, 144(%rsp)
	movq	%r10, 152(%rsp)
	ja	.L42
	leaq	136(%rsp), %rcx
	cltq
	xorl	%ebx, %ebx
	movq	%rcx, 72(%rsp)
.L43:
	movzbl	4(%r15,%rbx), %edx
	movb	%dl, (%rcx,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L43
	movq	%r14, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L354
	cmpq	%r13, %r10
	jnb	.L75
	leaq	1(%r14), %rax
	leaq	135(%rsp), %rsi
.L51:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%dl, (%rsi,%rbx)
	ja	.L205
	cmpq	%rcx, %rbp
	ja	.L51
.L205:
	movq	72(%rsp), %rax
	movq	%rax, 144(%rsp)
	addq	%rbx, %rax
	movq	%rax, 88(%rsp)
	movl	136(%rsp), %eax
	cmpl	$382, %eax
	ja	.L355
	leaq	from_ucs4(%rip), %rcx
	movl	%eax, %edx
	leaq	(%rcx,%rdx,2), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L66
	testl	%eax, %eax
	je	.L66
	cmpq	$0, 80(%rsp)
	je	.L60
	testb	$8, %r11b
	jne	.L356
	andl	$2, %r11d
	je	.L60
	movq	80(%rsp), %rax
	addq	$1, (%rax)
.L336:
	movq	72(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L346:
	subq	$1, %rbx
	movq	144(%rsp), %rax
	movl	$5, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L345:
	cmpq	$0, 80(%rsp)
	je	.L186
	testb	$8, 16(%r12)
	jne	.L357
.L109:
	testb	$2, %r11b
	jne	.L358
.L186:
	movl	$6, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L348:
	movq	48(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L117:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	cmpl	$7, 8(%rsp)
	jne	.L14
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L159
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L161
.L160:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L160
.L161:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L350:
	leaq	1(%rax), %r9
	cmpq	%r9, %rbp
	jbe	.L181
	movzbl	1(%rax), %ecx
	subl	$32, %ecx
	cmpl	$95, %ecx
	ja	.L359
	leaq	(%rsi,%rsi,2), %rsi
	salq	$5, %rsi
	addq	%rsi, %rcx
	movl	(%r8,%rcx,4), %esi
	movl	$2, %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L351:
	cmpq	$0, 80(%rsp)
	je	.L186
	testl	%r11d, %r11d
	je	.L186
	movq	80(%rsp), %rsi
	addq	%rcx, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rsi)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L187:
	movl	8(%rsp), %r11d
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L349:
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L360
	leaq	152(%rsp), %rsi
	leaq	144(%rsp), %rdi
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
	movq	%rsi, 8(%rsp)
	movq	%rdi, 72(%rsp)
.L135:
	cmpq	%r14, %rbp
	je	.L361
.L155:
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbp
	jb	.L195
	cmpq	%rdx, %r10
	jbe	.L196
	movl	(%r14), %ecx
	cmpl	$382, %ecx
	ja	.L362
	leaq	from_ucs4(%rip), %rdi
	movl	%ecx, %esi
	leaq	(%rdi,%rsi,2), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L149
	testl	%ecx, %ecx
	jne	.L363
.L149:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movzbl	1(%rdi), %edx
	testb	%dl, %dl
	je	.L152
.L164:
	movq	152(%rsp), %rcx
	cmpq	%rcx, %r10
	jbe	.L364
	leaq	1(%rcx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rcx)
.L152:
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r14
	cmpq	%r14, %rbp
	movq	%r14, 144(%rsp)
	jne	.L155
.L361:
	cltq
	movq	%rbp, %r14
	jmp	.L136
.L358:
	movq	80(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L95
.L353:
	cmpl	$4, %eax
	ja	.L25
	movzbl	4(%r15), %ecx
	cmpl	$1, %eax
	movb	%cl, 152(%rsp)
	movl	$1, %ecx
	je	.L26
	movzbl	5(%r15), %ecx
	movb	%cl, 153(%rsp)
	movl	$2, %ecx
.L26:
	leaq	4(%r10), %rsi
	cmpq	%rsi, %r13
	jb	.L75
	movzbl	(%r14), %edi
	movb	%dil, 152(%rsp,%rcx)
	movzbl	152(%rsp), %edi
	leal	-193(%rdi), %ebx
	movl	%edi, %r8d
	cmpl	$14, %ebx
	jbe	.L365
	leaq	to_ucs4(%rip), %rax
	movl	%edi, %edi
	movl	(%rax,%rdi,4), %edx
	movl	$1, %eax
.L36:
	testl	%edx, %edx
	jne	.L167
	testb	%r8b, %r8b
	jne	.L366
.L167:
	leaq	152(%rsp), %rdi
	movslq	%eax, %r11
	movl	%edx, (%r10)
	movq	%rsi, %r10
	addq	%rdi, %r11
.L35:
	movl	(%r15), %eax
	subq	%rdi, %r11
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %r11
	jle	.L367
	movq	16(%rsp), %rdi
	subq	%rdx, %r11
	andl	$-8, %eax
	addq	%r11, %r14
	movl	16(%r12), %r11d
	movq	%r14, (%rdi)
	movl	%eax, (%r15)
	jmp	.L22
.L360:
	cmpq	%r14, %rbp
	je	.L368
	leaq	4(%r15), %rax
	andl	$2, %ebx
	movq	%r15, %rdx
	movl	%ebx, %r9d
	movl	$4, %esi
	leaq	to_ucs4_comb(%rip), %rbx
	cmpq	%rax, %r10
	jb	.L369
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	(%r14), %r8d
	leal	-193(%r8), %edi
	movl	%r8d, %ecx
	cmpl	$14, %edi
	jbe	.L370
	leaq	to_ucs4(%rip), %rdi
	movl	%r8d, %r8d
	movl	(%rdi,%r8,4), %r8d
	movl	$1, %edi
.L131:
	testl	%r8d, %r8d
	jne	.L132
	testb	%cl, %cl
	jne	.L371
.L132:
	movl	%r8d, (%rdx)
	addq	%rdi, %r14
	movq	%rax, %rdx
.L133:
	cmpq	%r14, %rbp
	je	.L372
.L130:
	leaq	4(%rdx), %rax
	cmpq	%rax, %r10
	jnb	.L125
	movl	$5, %eax
.L127:
	movq	16(%rsp), %rdi
	movq	%r14, (%rdi)
	jmp	.L134
.L355:
	cmpl	$8486, %eax
	je	.L54
	cmpl	$711, %eax
	je	.L55
	cmpl	$732, %eax
	leal	-728(%rax), %edx
	je	.L206
	cmpl	$5, %edx
	ja	.L206
	leaq	map.9119(%rip), %rax
	leaq	126(%rsp), %rcx
	movb	$32, 127(%rsp)
	movzbl	(%rax,%rdx), %edx
	movb	%dl, 126(%rsp)
.L66:
	leaq	1(%r10), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%r10)
	movzbl	1(%rcx), %eax
	testb	%al, %al
	je	.L73
.L162:
	movq	152(%rsp), %rdx
	cmpq	%rdx, %r13
	jbe	.L373
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
.L73:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	72(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L337
.L59:
	movl	(%r15), %edx
	subq	72(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L374
	movq	16(%rsp), %rsi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r10
	movl	16(%r12), %r11d
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r14
	movl	%edx, (%r15)
	jmp	.L22
.L373:
	subq	$1, %rdx
	movq	144(%rsp), %rax
	cmpq	72(%rsp), %rax
	movq	%rdx, 152(%rsp)
	jne	.L59
.L75:
	movl	$5, 8(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L207:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L375
	cmpq	$0, 80(%rsp)
	je	.L186
	testb	$8, 16(%r12)
	jne	.L376
.L105:
	testb	$2, %r11b
	je	.L186
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L357:
	movl	%r11d, 96(%rsp)
	subq	$8, %rsp
	movq	%r10, %rcx
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	88(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r10, 104(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	88(%rsp), %r10
	movl	96(%rsp), %r11d
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L109
.L110:
	cmpl	$5, 8(%rsp)
	jne	.L95
	jmp	.L96
.L341:
	cmpq	$0, 32(%rsp)
	jne	.L377
	movq	32(%r12), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	movl	%eax, 24(%rsp)
	popq	%r10
	popq	%r11
	jmp	.L14
.L203:
	movl	%r11d, 8(%rsp)
	jmp	.L117
.L98:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	$-32, (%rbx)
	jmp	.L111
.L99:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%rbx)
	movl	$32, %eax
	jmp	.L163
.L359:
	cmpq	$0, 80(%rsp)
	je	.L186
	testl	%r11d, %r11d
	je	.L186
	movq	80(%rsp), %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	movq	%r9, %rax
	jmp	.L90
.L195:
	movl	$7, %eax
.L136:
	movq	16(%rsp), %rsi
	movq	136(%rsp), %r10
	movq	%r14, (%rsi)
.L134:
	cmpq	%r10, %rdx
	jne	.L124
	cmpq	$5, %rax
	jne	.L123
.L148:
	cmpq	%r15, %rdx
	jne	.L118
.L126:
	subl	$1, 20(%r12)
	jmp	.L118
.L376:
	movl	%r11d, 96(%rsp)
	subq	$8, %rsp
	movq	%r10, %rcx
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	88(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r10, 104(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rbx
	popq	%rdx
	movq	88(%rsp), %r10
	movl	96(%rsp), %r11d
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	jne	.L110
	jmp	.L105
.L378:
	movq	%r10, 96(%rsp)
	movl	%r11d, 88(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	24(%rsp), %r9
	movq	88(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	movl	88(%rsp), %r11d
	movq	96(%rsp), %r10
	je	.L150
	cmpl	$5, %eax
	jne	.L135
.L196:
	movl	$5, %eax
	jmp	.L136
.L362:
	cmpl	$8486, %ecx
	je	.L138
	cmpl	$711, %ecx
	je	.L139
	leal	-728(%rcx), %edi
	cmpl	$5, %edi
	ja	.L208
	cmpl	$732, %ecx
	je	.L208
	leaq	map.9086(%rip), %rsi
	movb	$32, 127(%rsp)
	movzbl	(%rsi,%rdi), %esi
	leaq	126(%rsp), %rdi
	movb	%sil, 126(%rsp)
	jmp	.L149
.L375:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L95
.L173:
	movq	%rbp, %rax
	movq	%r15, %rbx
	movl	$4, 8(%rsp)
	jmp	.L96
.L364:
	movq	136(%rsp), %rdx
	movq	144(%rsp), %rax
	subq	$1, %rcx
	movq	16(%rsp), %rsi
	cmpq	%rcx, %rdx
	movq	%rax, (%rsi)
	je	.L148
.L124:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L80:
	cmpl	$0, 8(%rsp)
	jne	.L14
.L337:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r14
	jmp	.L22
.L363:
	cmpq	$0, 80(%rsp)
	je	.L201
	testb	$8, 16(%r12)
	jne	.L378
.L150:
	testb	$2, %bl
	jne	.L379
.L201:
	movl	$6, %eax
	jmp	.L136
.L370:
	leaq	1(%r14), %r8
	cmpq	%r8, %rbp
	movq	%r8, 8(%rsp)
	jbe	.L190
	movzbl	1(%r14), %r8d
	subl	$32, %r8d
	cmpl	$95, %r8d
	ja	.L380
	leaq	(%rdi,%rdi,2), %rdi
	salq	$5, %rdi
	addq	%rdi, %r8
	movl	$2, %edi
	movl	(%rbx,%r8,4), %r8d
	jmp	.L131
.L371:
	cmpq	$0, 80(%rsp)
	je	.L194
	testl	%r9d, %r9d
	jne	.L381
.L194:
	movl	$6, %eax
	jmp	.L127
.L354:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r14, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L45
	addq	$1, %r14
	cmpq	%rax, %rbx
	jnb	.L49
.L48:
	movq	%r14, 144(%rsp)
	movzbl	-1(%r14), %edx
	addq	$1, %r14
	movb	%dl, 4(%r15,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L48
.L49:
	movl	$7, 8(%rsp)
	jmp	.L14
.L208:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L382
	cmpq	$0, 80(%rsp)
	je	.L201
	testb	$8, 16(%r12)
	je	.L150
	movq	%r10, 96(%rsp)
	movl	%r11d, 88(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %rsi
	movq	24(%rsp), %r9
	movq	88(%rsp), %rcx
	movq	%rbp, %r8
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movl	88(%rsp), %r11d
	movq	96(%rsp), %r10
	je	.L383
	cmpl	$5, %eax
	je	.L384
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L135
.L372:
	movslq	%esi, %rax
	movq	%rbp, %r14
	jmp	.L127
.L369:
	cmpq	%r15, %r10
	je	.L126
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L379:
	movq	80(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L135
.L381:
	movq	80(%rsp), %rax
	addq	%rdi, %r14
	movl	$6, %esi
	addq	$1, (%rax)
	jmp	.L133
.L138:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-32, (%rdx)
	jmp	.L152
.L380:
	cmpq	$0, 80(%rsp)
	je	.L194
	testl	%r9d, %r9d
	je	.L194
	movq	80(%rsp), %rax
	movq	8(%rsp), %r14
	movl	$6, %esi
	addq	$1, (%rax)
	jmp	.L130
.L139:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-49, (%rdx)
	movl	$32, %edx
	jmp	.L164
.L366:
	cmpq	$0, 80(%rsp)
	je	.L60
	andl	$2, %r11d
	je	.L60
	leaq	152(%rsp), %rdi
	movslq	%eax, %r11
	movq	80(%rsp), %rax
	addq	%rdi, %r11
	addq	$1, (%rax)
	cmpq	%rdi, %r11
	jne	.L35
.L60:
	movl	$6, 8(%rsp)
	jmp	.L14
.L382:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r14
	jmp	.L135
.L190:
	movl	$7, %eax
	jmp	.L127
.L365:
	leaq	152(%rsp), %rdi
	addq	$1, %rcx
	leaq	(%rdi,%rcx), %r9
	movq	%r9, 8(%rsp)
	leaq	1(%rdi), %r9
	cmpq	%r9, 8(%rsp)
	jbe	.L385
	movzbl	153(%rsp), %eax
	subl	$32, %eax
	cmpl	$95, %eax
	ja	.L386
	movl	%ebx, %edx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$5, %rdx
	addq	%rdx, %rax
	leaq	to_ucs4_comb(%rip), %rdx
	movl	(%rdx,%rax,4), %edx
	movl	$2, %eax
	jmp	.L36
.L383:
	movq	144(%rsp), %r14
	movq	152(%rsp), %rdx
	jmp	.L150
.L368:
	movq	%r10, %r11
	cmpq	%r15, %r11
	jne	.L124
.L123:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	movl	%r11d, 108(%rsp)
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdx
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rcx
	popq	%rsi
	movq	96(%rsp), %r10
	movl	108(%rsp), %r11d
	movq	144(%rsp), %rax
	je	.L387
	cmpl	$5, 8(%rsp)
	movq	%rax, %rdx
	je	.L388
.L63:
	cmpq	72(%rsp), %rdx
	movq	%rdx, %rax
	jne	.L59
	cmpl	$7, 8(%rsp)
	jne	.L80
	addq	$4, %rdx
	cmpq	%rdx, 88(%rsp)
	je	.L389
	movl	(%r15), %eax
	movq	16(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rsi)
	cmpq	%rdx, %rbx
	jle	.L390
	cmpq	$4, %rbx
	ja	.L391
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r15)
	je	.L49
	movq	72(%rsp), %rcx
	xorl	%eax, %eax
.L84:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L84
	jmp	.L49
.L206:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L336
	cmpq	$0, 80(%rsp)
	je	.L60
	testb	$8, %r11b
	jne	.L392
.L61:
	andl	$2, %r11d
	movq	144(%rsp), %rax
	jne	.L393
.L335:
	cmpq	72(%rsp), %rax
	jne	.L59
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	2(%rdi), %rsi
	cmpq	%rsi, 8(%rsp)
	je	.L394
	cltq
	movq	%rcx, %rsi
	andl	$-8, %edx
	subq	%rax, %rsi
	movq	16(%rsp), %rax
	addq	%rsi, %r14
	movq	%r14, (%rax)
	movslq	%edx, %rax
	cmpq	%rax, %rcx
	jle	.L395
	cmpq	$4, %rcx
	ja	.L396
	orl	%ecx, %edx
	testq	%rcx, %rcx
	movl	%edx, (%r15)
	je	.L49
	xorl	%eax, %eax
	movb	%r8b, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L49
.L397:
	movzbl	(%rdi,%rax), %r8d
	movb	%r8b, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L397
	jmp	.L49
.L55:
	leaq	1(%r10), %rax
	movq	%rax, 152(%rsp)
	movb	$-49, (%r10)
	movl	$32, %eax
	jmp	.L162
.L387:
	andl	$2, %r11d
	je	.L335
	movq	80(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rsi)
	cmpq	72(%rsp), %rax
	je	.L14
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	1(%r10), %rax
	movq	%rax, 152(%rsp)
	movb	$-32, (%r10)
	jmp	.L73
.L25:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L45:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L352:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L374:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L388:
	cmpq	72(%rsp), %rax
	jne	.L59
	jmp	.L75
.L396:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L395:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L394:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L391:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L390:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L389:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L159:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L386:
	cmpq	$0, 80(%rsp)
	je	.L60
	andb	$2, %r11b
	je	.L60
	movq	80(%rsp), %rax
	movq	%r9, %r11
	addq	$1, (%rax)
	jmp	.L35
.L42:
	leaq	__PRETTY_FUNCTION__.9104(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L384:
	movq	136(%rsp), %rdx
	cmpq	152(%rsp), %rdx
	movq	144(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L148
	jmp	.L124
.L377:
	leaq	__PRETTY_FUNCTION__.9181(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L367:
	leaq	__PRETTY_FUNCTION__.9015(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L393:
	movq	80(%rsp), %rax
	addq	$1, (%rax)
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	72(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L59
	jmp	.L60
.L392:
	movl	%r11d, 108(%rsp)
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rdi
	popq	%r8
	movl	108(%rsp), %r11d
	je	.L61
	cmpl	$5, %eax
	je	.L62
	movq	144(%rsp), %rdx
	movq	96(%rsp), %r10
	jmp	.L63
.L62:
	movq	144(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L59
	jmp	.L75
	.size	gconv, .-gconv
	.section	.rodata
	.type	map.9086, @object
	.size	map.9086, 6
map.9086:
	.string	"\306\307\312\316"
	.ascii	"\315"
	.set	map.9119,map.9086
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9104, @object
	.size	__PRETTY_FUNCTION__.9104, 15
__PRETTY_FUNCTION__.9104:
	.string	"to_t_61_single"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9015, @object
	.size	__PRETTY_FUNCTION__.9015, 17
__PRETTY_FUNCTION__.9015:
	.string	"from_t_61_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9181, @object
	.size	__PRETTY_FUNCTION__.9181, 6
__PRETTY_FUNCTION__.9181:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 766
from_ucs4:
	.string	""
	.string	""
	.string	"\001"
	.string	"\002"
	.string	"\003"
	.string	"\004"
	.string	"\005"
	.string	"\006"
	.string	"\007"
	.string	"\b"
	.string	"\t"
	.string	"\n"
	.string	"\013"
	.string	"\f"
	.string	"\r"
	.string	"\016"
	.string	"\017"
	.string	"\020"
	.string	"\021"
	.string	"\022"
	.string	"\023"
	.string	"\024"
	.string	"\025"
	.string	"\026"
	.string	"\027"
	.string	"\030"
	.string	"\031"
	.string	"\032"
	.string	"\033"
	.string	"\034"
	.string	"\035"
	.string	"\036"
	.string	"\037"
	.string	" "
	.string	"!"
	.string	"\""
	.string	"\246"
	.string	"\244"
	.string	"%"
	.string	"&"
	.string	"'"
	.string	"("
	.string	")"
	.string	"*"
	.string	"+"
	.string	","
	.string	"-"
	.string	"."
	.string	"/"
	.string	"0"
	.string	"1"
	.string	"2"
	.string	"3"
	.string	"4"
	.string	"5"
	.string	"6"
	.string	"7"
	.string	"8"
	.string	"9"
	.string	":"
	.string	";"
	.string	"<"
	.string	"="
	.string	">"
	.string	"?"
	.string	"@"
	.string	"A"
	.string	"B"
	.string	"C"
	.string	"D"
	.string	"E"
	.string	"F"
	.string	"G"
	.string	"H"
	.string	"I"
	.string	"J"
	.string	"K"
	.string	"L"
	.string	"M"
	.string	"N"
	.string	"O"
	.string	"P"
	.string	"Q"
	.string	"R"
	.string	"S"
	.string	"T"
	.string	"U"
	.string	"V"
	.string	"W"
	.string	"X"
	.string	"Y"
	.string	"Z"
	.string	"["
	.string	""
	.string	""
	.string	"]"
	.string	""
	.string	""
	.string	"_"
	.string	""
	.string	""
	.string	"a"
	.string	"b"
	.string	"c"
	.string	"d"
	.string	"e"
	.string	"f"
	.string	"g"
	.string	"h"
	.string	"i"
	.string	"j"
	.string	"k"
	.string	"l"
	.string	"m"
	.string	"n"
	.string	"o"
	.string	"p"
	.string	"q"
	.string	"r"
	.string	"s"
	.string	"t"
	.string	"u"
	.string	"v"
	.string	"w"
	.string	"x"
	.string	"y"
	.string	"z"
	.string	""
	.string	""
	.string	"|"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	"\177"
	.string	"\200"
	.string	"\201"
	.string	"\202"
	.string	"\203"
	.string	"\204"
	.string	"\205"
	.string	"\206"
	.string	"\207"
	.string	"\210"
	.string	"\211"
	.string	"\212"
	.string	"\213"
	.string	"\214"
	.string	"\215"
	.string	"\216"
	.string	"\217"
	.string	"\220"
	.string	"\221"
	.string	"\222"
	.string	"\223"
	.string	"\224"
	.string	"\225"
	.string	"\226"
	.string	"\227"
	.string	"\230"
	.string	"\231"
	.string	"\232"
	.string	"\233"
	.string	"\234"
	.string	"\235"
	.string	"\236"
	.string	"\237"
	.string	""
	.string	""
	.string	"\241"
	.string	"\242"
	.string	"\243"
	.string	"\250"
	.string	"\245"
	.string	""
	.string	""
	.string	"\247"
	.ascii	"\310 "
	.string	""
	.string	""
	.string	"\343"
	.string	"\253"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\305 "
	.string	"\260"
	.string	"\261"
	.string	"\262"
	.string	"\263"
	.ascii	"\302 "
	.string	"\265"
	.string	"\266"
	.string	"\267"
	.ascii	"\313 "
	.string	""
	.string	""
	.string	"\353"
	.string	"\273"
	.string	"\274"
	.string	"\275"
	.string	"\276"
	.string	"\277"
	.ascii	"\301A"
	.ascii	"\302A"
	.ascii	"\303A"
	.ascii	"\304A"
	.ascii	"\310A"
	.ascii	"\312A"
	.string	"\341"
	.ascii	"\313C"
	.ascii	"\301E"
	.ascii	"\302E"
	.ascii	"\303E"
	.ascii	"\310E"
	.ascii	"\301I"
	.ascii	"\302I"
	.ascii	"\303I"
	.ascii	"\310I"
	.string	"\342"
	.ascii	"\304N"
	.ascii	"\301O"
	.ascii	"\302O"
	.ascii	"\303O"
	.ascii	"\304O"
	.ascii	"\310O"
	.string	"\264"
	.string	"\351"
	.ascii	"\301U"
	.ascii	"\302U"
	.ascii	"\303U"
	.ascii	"\310U"
	.ascii	"\302Y"
	.string	"\354"
	.string	"\373"
	.ascii	"\301a"
	.ascii	"\302a"
	.ascii	"\303a"
	.ascii	"\304a"
	.ascii	"\310a"
	.ascii	"\312a"
	.string	"\361"
	.ascii	"\313c"
	.ascii	"\301e"
	.ascii	"\302e"
	.ascii	"\303e"
	.ascii	"\310e"
	.ascii	"\301i"
	.ascii	"\302i"
	.ascii	"\303i"
	.ascii	"\310i"
	.string	"\363"
	.ascii	"\304n"
	.ascii	"\301o"
	.ascii	"\302o"
	.ascii	"\303o"
	.ascii	"\304o"
	.ascii	"\310o"
	.string	"\270"
	.string	"\371"
	.ascii	"\301u"
	.ascii	"\302u"
	.ascii	"\303u"
	.ascii	"\310u"
	.ascii	"\302y"
	.string	"\374"
	.ascii	"\310y"
	.ascii	"\305A"
	.ascii	"\305a"
	.ascii	"\306A"
	.ascii	"\306a"
	.ascii	"\316A"
	.ascii	"\316a"
	.ascii	"\302C"
	.ascii	"\302c"
	.ascii	"\303C"
	.ascii	"\303c"
	.ascii	"\307C"
	.ascii	"\307c"
	.ascii	"\317C"
	.ascii	"\317c"
	.ascii	"\317D"
	.ascii	"\317d"
	.string	""
	.string	""
	.string	"\362"
	.ascii	"\305E"
	.ascii	"\305e"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\307E"
	.ascii	"\307e"
	.ascii	"\316E"
	.ascii	"\316e"
	.ascii	"\317E"
	.ascii	"\317e"
	.ascii	"\303G"
	.ascii	"\303g"
	.ascii	"\306G"
	.ascii	"\306g"
	.ascii	"\307G"
	.ascii	"\307g"
	.ascii	"\313G"
	.ascii	"\313g"
	.ascii	"\303H"
	.ascii	"\303h"
	.string	"\344"
	.string	"\364"
	.ascii	"\304I"
	.ascii	"\304i"
	.ascii	"\305I"
	.ascii	"\305i"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\316I"
	.ascii	"\316i"
	.ascii	"\307I"
	.string	"\365"
	.string	"\346"
	.string	"\366"
	.ascii	"\303J"
	.ascii	"\303j"
	.ascii	"\313K"
	.ascii	"\313k"
	.string	"\360"
	.ascii	"\302L"
	.ascii	"\302l"
	.ascii	"\313L"
	.ascii	"\313l"
	.ascii	"\317L"
	.ascii	"\317l"
	.string	"\347"
	.string	"\367"
	.string	"\350"
	.string	"\370"
	.ascii	"\302N"
	.ascii	"\302n"
	.ascii	"\313N"
	.ascii	"\313n"
	.ascii	"\317N"
	.ascii	"\317n"
	.string	"\357"
	.string	"\356"
	.string	"\376"
	.ascii	"\305O"
	.ascii	"\305o"
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\315O"
	.ascii	"\315o"
	.string	"\352"
	.string	"\372"
	.ascii	"\302R"
	.ascii	"\302r"
	.ascii	"\313R"
	.ascii	"\313r"
	.ascii	"\317R"
	.ascii	"\317r"
	.ascii	"\302S"
	.ascii	"\302s"
	.ascii	"\303S"
	.ascii	"\303s"
	.ascii	"\313S"
	.ascii	"\313s"
	.ascii	"\317S"
	.ascii	"\317s"
	.ascii	"\313T"
	.ascii	"\313t"
	.ascii	"\317T"
	.ascii	"\317t"
	.string	"\355"
	.string	"\375"
	.ascii	"\304U"
	.ascii	"\304u"
	.ascii	"\305U"
	.ascii	"\305u"
	.ascii	"\306U"
	.ascii	"\306u"
	.ascii	"\312U"
	.ascii	"\312u"
	.ascii	"\315U"
	.ascii	"\315u"
	.ascii	"\316U"
	.ascii	"\316u"
	.ascii	"\303W"
	.ascii	"\303w"
	.ascii	"\303Y"
	.ascii	"\303y"
	.ascii	"\310Y"
	.ascii	"\302Z"
	.ascii	"\302z"
	.ascii	"\307Z"
	.ascii	"\307z"
	.ascii	"\317Z"
	.ascii	"\317z"
	.align 32
	.type	to_ucs4_comb, @object
	.size	to_ucs4_comb, 5760
to_ucs4_comb:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	192
	.long	0
	.long	0
	.long	0
	.long	200
	.long	0
	.long	0
	.long	0
	.long	204
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	210
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	217
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	224
	.long	0
	.long	0
	.long	0
	.long	232
	.long	0
	.long	0
	.long	0
	.long	236
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	242
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	249
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	180
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	193
	.long	0
	.long	262
	.long	0
	.long	201
	.long	0
	.long	0
	.long	0
	.long	205
	.long	0
	.long	0
	.long	313
	.long	0
	.long	323
	.long	211
	.long	0
	.long	0
	.long	340
	.long	346
	.long	0
	.long	218
	.long	0
	.long	0
	.long	0
	.long	221
	.long	377
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	225
	.long	0
	.long	263
	.long	0
	.long	233
	.long	0
	.long	0
	.long	0
	.long	237
	.long	0
	.long	0
	.long	314
	.long	0
	.long	324
	.long	243
	.long	0
	.long	0
	.long	341
	.long	347
	.long	0
	.long	250
	.long	0
	.long	0
	.long	0
	.long	253
	.long	378
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	194
	.long	0
	.long	264
	.long	0
	.long	202
	.long	0
	.long	284
	.long	292
	.long	206
	.long	308
	.long	0
	.long	0
	.long	0
	.long	0
	.long	212
	.long	0
	.long	0
	.long	0
	.long	348
	.long	0
	.long	219
	.long	0
	.long	372
	.long	0
	.long	374
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	226
	.long	0
	.long	265
	.long	0
	.long	234
	.long	0
	.long	285
	.long	293
	.long	238
	.long	309
	.long	0
	.long	0
	.long	0
	.long	0
	.long	244
	.long	0
	.long	0
	.long	0
	.long	349
	.long	0
	.long	251
	.long	0
	.long	373
	.long	0
	.long	375
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	195
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	296
	.long	0
	.long	0
	.long	0
	.long	0
	.long	209
	.long	213
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	360
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	227
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	297
	.long	0
	.long	0
	.long	0
	.long	0
	.long	241
	.long	245
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	361
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	175
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	256
	.long	0
	.long	0
	.long	0
	.long	274
	.long	0
	.long	0
	.long	0
	.long	298
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	332
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	362
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	257
	.long	0
	.long	0
	.long	0
	.long	275
	.long	0
	.long	0
	.long	0
	.long	299
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	333
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	363
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	728
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	258
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	286
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	364
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	259
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	287
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	365
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	729
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	266
	.long	0
	.long	278
	.long	0
	.long	288
	.long	0
	.long	304
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	379
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	267
	.long	0
	.long	279
	.long	0
	.long	289
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	380
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	168
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	196
	.long	0
	.long	0
	.long	0
	.long	203
	.long	0
	.long	0
	.long	0
	.long	207
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	214
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	220
	.long	0
	.long	0
	.long	0
	.long	376
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	228
	.long	0
	.long	0
	.long	0
	.long	235
	.long	0
	.long	0
	.long	0
	.long	239
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	246
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	252
	.long	0
	.long	0
	.long	0
	.long	255
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	730
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	197
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	366
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	229
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	367
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	184
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	199
	.long	0
	.long	0
	.long	0
	.long	290
	.long	0
	.long	0
	.long	0
	.long	310
	.long	315
	.long	0
	.long	325
	.long	0
	.long	0
	.long	0
	.long	342
	.long	350
	.long	354
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	231
	.long	0
	.long	0
	.long	0
	.long	291
	.long	0
	.long	0
	.long	0
	.long	311
	.long	316
	.long	0
	.long	326
	.long	0
	.long	0
	.long	0
	.long	343
	.long	351
	.long	355
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	380
	.long	733
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	336
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	368
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	337
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	369
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	731
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	260
	.long	0
	.long	0
	.long	0
	.long	280
	.long	0
	.long	0
	.long	0
	.long	302
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	370
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	261
	.long	0
	.long	0
	.long	0
	.long	281
	.long	0
	.long	0
	.long	0
	.long	303
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	371
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	711
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	268
	.long	270
	.long	282
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	317
	.long	0
	.long	327
	.long	0
	.long	0
	.long	0
	.long	344
	.long	352
	.long	356
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	381
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	269
	.long	271
	.long	283
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	318
	.long	0
	.long	328
	.long	0
	.long	0
	.long	0
	.long	345
	.long	353
	.long	357
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	382
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	0
	.long	0
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	0
	.long	93
	.long	0
	.long	95
	.long	0
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	0
	.long	124
	.long	0
	.long	0
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	0
	.long	161
	.long	162
	.long	163
	.long	36
	.long	165
	.long	35
	.long	167
	.long	164
	.long	0
	.long	0
	.long	171
	.long	0
	.long	0
	.long	0
	.long	0
	.long	176
	.long	177
	.long	178
	.long	179
	.long	215
	.long	181
	.long	182
	.long	183
	.long	247
	.long	0
	.long	0
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	8486
	.long	198
	.long	208
	.long	170
	.long	294
	.long	0
	.long	306
	.long	319
	.long	321
	.long	216
	.long	338
	.long	186
	.long	222
	.long	358
	.long	330
	.long	329
	.long	312
	.long	230
	.long	273
	.long	240
	.long	295
	.long	305
	.long	307
	.long	320
	.long	322
	.long	248
	.long	339
	.long	223
	.long	254
	.long	359
	.long	331
	.long	0
