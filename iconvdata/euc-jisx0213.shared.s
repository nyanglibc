	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	movzbl	%sil, %eax
	testb	%sil, %sil
	movl	$-1, %edx
	cmovs	%edx, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EUC-JISX0213//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$15, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L6
	movabsq	$12884901889, %rdx
	movabsq	$34359738372, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	32(%rax), %rsi
	movl	$15, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$17179869188, %rdi
	movabsq	$12884901889, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"(jch & 0x8000) == 0"
.LC11:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC13:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rsi, %rbx
	subq	$184, %rsp
	movl	16(%rsi), %r10d
	movq	%rdi, 48(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	movl	%r10d, %edx
	movq	%r9, 64(%rsp)
	movl	240(%rsp), %ecx
	andl	$1, %edx
	movq	%rdi, 80(%rsp)
	movq	$0, 24(%rsp)
	jne	.L11
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L11
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L11:
	testl	%ecx, %ecx
	jne	.L429
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	movq	32(%rbx), %r12
	movl	248(%rsp), %r13d
	testq	%rdi, %rdi
	movq	(%rax), %r15
	movq	%rdi, %rax
	cmove	%rbx, %rax
	cmpq	$0, 64(%rsp)
	movq	(%rax), %r11
	movq	8(%rbx), %rax
	movq	$0, 128(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%r13d, %r13d
	movq	%rax, 88(%rsp)
	movl	(%r12), %eax
	movl	%eax, 40(%rsp)
	jne	.L430
.L28:
	leaq	152(%rsp), %rax
	leaq	comp_table_data(%rip), %r13
	movq	%rax, 96(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 104(%rsp)
	movq	%r11, %rax
	movq	%r15, %r11
	movq	%rax, %r15
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L431
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r11, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %r14
	movq	%r11, %rdx
	movl	$4, 8(%rsp)
.L128:
	cmpq	%rdx, %rbp
	je	.L129
.L148:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rbp
	jb	.L244
	cmpq	%r14, (%rsp)
	jbe	.L289
	movl	(%r12), %ecx
	movl	(%rdx), %eax
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L130
	cmpl	$741, %eax
	movl	%ecx, %edi
	je	.L246
	cmpl	$745, %eax
	je	.L247
	cmpl	$768, %eax
	je	.L248
	cmpl	$769, %eax
	je	.L249
	cmpl	$12442, %eax
	je	.L432
.L132:
	leaq	1(%r14), %rax
	cmpq	%rax, (%rsp)
	jbe	.L289
	movq	%rax, 152(%rsp)
	movzbl	%ch, %eax
	movb	%al, (%r14)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	%cl, (%rax)
	movq	144(%rsp), %rdx
	movl	$0, (%r12)
	movq	152(%rsp), %r14
	cmpq	%rdx, %rbp
	jne	.L148
	.p2align 4,,10
	.p2align 3
.L129:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L433
.L149:
	addl	$1, 20(%rbx)
	testb	$1, 16(%rbx)
	jne	.L434
	cmpq	%r14, %r15
	movq	%r11, 56(%rsp)
	jnb	.L256
	movq	24(%rsp), %rdi
	movq	(%rbx), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	pushq	%rdi
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rsi
	popq	%rdi
	je	.L153
	movq	136(%rsp), %rax
	movq	56(%rsp), %r11
	cmpq	%r14, %rax
	movq	%rax, 8(%rsp)
	jne	.L435
.L152:
	testl	%r10d, %r10d
	jne	.L287
.L204:
	movq	16(%rsp), %rax
	movl	16(%rbx), %r10d
	movq	(%rbx), %r15
	movq	(%rax), %r11
	movl	(%r12), %eax
	movl	%eax, 40(%rsp)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L106
.L431:
	cmpq	%r11, %rbp
	je	.L228
	leaq	4(%r15), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L229
	movl	40(%rsp), %eax
	andl	$2, %r10d
	movq	%r11, %rdx
	movq	%r15, %r14
	movl	$4, 8(%rsp)
	sarl	$3, %eax
	testl	%eax, %eax
	jne	.L109
	movzbl	(%rdx), %eax
	cmpl	$127, %eax
	ja	.L110
.L436:
	addq	$1, %rdx
.L109:
	movl	%eax, (%r14)
	movq	%rcx, %r14
.L125:
	cmpq	%rdx, %rbp
	je	.L129
.L126:
	leaq	4(%r14), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L289
	movl	(%r12), %eax
	sarl	$3, %eax
	testl	%eax, %eax
	jne	.L109
	movzbl	(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L436
.L110:
	leal	-161(%rax), %esi
	cmpl	$93, %esi
	setbe	%r8b
	cmpl	$142, %eax
	sete	%dil
	orb	%dil, %r8b
	jne	.L294
	cmpl	$143, %eax
	jne	.L111
.L294:
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 56(%rsp)
	jbe	.L244
	movzbl	1(%rdx), %r8d
	leal	-161(%r8), %r9d
	cmpl	$93, %r9d
	ja	.L121
	cmpl	$142, %eax
	je	.L437
	cmpl	$143, %eax
	leaq	2(%rdx), %r9
	je	.L438
	leal	-128(%r8), %eax
	movzbl	%al, %eax
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L121
	imull	$94, %esi, %esi
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rdi
	addl	%eax, %esi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rsi,2), %eax
	movzbl	%ah, %esi
	movzbl	%al, %eax
	addl	(%rdi,%rsi,4), %eax
	cmpl	$65533, %eax
	je	.L121
.L122:
	testl	%eax, %eax
	je	.L121
	cmpl	$127, %eax
	ja	.L241
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rdx
	leal	-1(%rax), %esi
	movzwl	2(%rdx,%rsi,4), %eax
	movzwl	(%rdx,%rsi,4), %edx
	movl	%edx, (%r14)
	leaq	8(%r14), %rdx
	cmpq	%rdx, (%rsp)
	jnb	.L439
	sall	$3, %eax
	movq	%r9, %rdx
	movq	%rcx, %r14
	movl	%eax, (%r12)
	movl	$5, 8(%rsp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L130:
	cmpl	$127, %eax
	ja	.L136
	leaq	1(%r14), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r14)
.L137:
	movq	144(%rsp), %rax
	movq	152(%rsp), %r14
	leaq	4(%rax), %rdx
	movq	%rdx, 144(%rsp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L136:
	leal	-65377(%rax), %ecx
	cmpl	$62, %ecx
	ja	.L138
	leaq	1(%r14), %rcx
	cmpq	%rcx, (%rsp)
	jbe	.L289
	movq	%rcx, 152(%rsp)
	movb	$-114, (%r14)
	addl	$64, %eax
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L289:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movl	$5, 8(%rsp)
	movq	%rdx, (%rax)
	je	.L149
.L433:
	movq	32(%rsp), %rax
	movq	%r14, (%rax)
.L10:
	movl	8(%rsp), %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$7, 8(%rsp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L153:
	movl	8(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L152
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L138:
	cmpl	$173759, %eax
	ja	.L139
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%eax, %ecx
	shrl	$6, %ecx
	movswl	(%rdi,%rcx,2), %ecx
	testl	%ecx, %ecx
	js	.L139
	movl	%eax, %edi
	sall	$6, %ecx
	andl	$63, %edi
	addl	%ecx, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rcx
	movzwl	(%rcx,%rdi,2), %ecx
	testl	%ecx, %ecx
	movl	%ecx, %edi
	je	.L139
	testb	$-128, %cl
	je	.L144
	testw	%cx, %cx
	js	.L199
	sall	$3, %ecx
	orl	$263168, %ecx
	movl	%ecx, (%r12)
.L417:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L139:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L417
	cmpq	$0, 88(%rsp)
	je	.L253
	testb	$8, 16(%rbx)
	jne	.L440
.L142:
	testb	$2, %r10b
	jne	.L441
.L253:
	movl	$6, 8(%rsp)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$-21532, %esi
	movl	$1, %r8d
	xorl	%eax, %eax
.L131:
	addl	%eax, %r8d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L442:
	addl	$1, %eax
	cmpl	%eax, %r8d
	je	.L132
	movl	%eax, %esi
	movzwl	0(%r13,%rsi,4), %esi
.L134:
	cmpw	%si, %di
	jne	.L442
	leaq	1(%r14), %rcx
	cmpq	%rcx, (%rsp)
	jbe	.L289
	movzwl	2(%r13,%rax,4), %edx
	movq	%rcx, 152(%rsp)
	movl	%edx, %eax
	shrw	$8, %ax
	movb	%al, (%r14)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rax)
	movq	144(%rsp), %rax
	movl	$0, (%r12)
	movq	152(%rsp), %r14
	leaq	4(%rax), %rdx
	movq	%rdx, 144(%rsp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L247:
	movl	$-21536, %esi
	movl	$1, %r8d
	movl	$1, %eax
	jmp	.L131
.L120:
	leal	-238(%r8), %esi
	subl	$135, %r8d
	cmpl	$16, %esi
	jbe	.L118
	.p2align 4,,10
	.p2align 3
.L121:
	cmpq	$0, 88(%rsp)
	je	.L253
	testl	%r10d, %r10d
	je	.L253
	movq	88(%rsp), %rax
	movq	56(%rsp), %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$-22052, %esi
	movl	$5, %r8d
	movl	$2, %eax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L111:
	cmpq	$0, 88(%rsp)
	je	.L253
	testl	%r10d, %r10d
	je	.L253
	movq	88(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L430:
	andl	$7, %eax
	je	.L28
	testq	%rdi, %rdi
	jne	.L443
	movq	48(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L444
	cmpl	$4, %eax
	movq	%r15, 160(%rsp)
	movq	%r11, 168(%rsp)
	ja	.L61
	leaq	152(%rsp), %r14
	movslq	%eax, %r13
	xorl	%edx, %edx
.L62:
	movzbl	4(%r12,%rdx), %eax
	movb	%al, (%r14,%rdx)
	addq	$1, %rdx
	cmpq	%r13, %rdx
	jne	.L62
	movq	%r15, %rax
	subq	%r13, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L445
	cmpq	(%rsp), %r11
	jnb	.L215
	leaq	1(%r15), %rax
	leaq	151(%rsp), %rdi
.L70:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %r13
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %r13
	movb	%dl, (%rdi,%r13)
	ja	.L293
	cmpq	%rsi, %rbp
	ja	.L70
.L293:
	movl	(%r12), %esi
	movq	%r14, 160(%rsp)
	movl	152(%rsp), %eax
	movl	%esi, %r8d
	sarl	$3, %r8d
	testl	%r8d, %r8d
	je	.L72
	cmpl	$741, %eax
	movl	%r8d, %edi
	je	.L222
	cmpl	$745, %eax
	je	.L223
	cmpl	$768, %eax
	je	.L224
	cmpl	$769, %eax
	je	.L225
	cmpl	$12442, %eax
	je	.L446
.L74:
	leaq	1(%r11), %rax
	cmpq	%rax, (%rsp)
	jbe	.L215
	movq	%rax, 168(%rsp)
	movl	%r8d, %eax
	movzbl	%ah, %eax
	movb	%al, (%r11)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%r8b, (%rax)
	xorl	%edx, %edx
	movq	160(%rsp), %rax
	movl	$0, (%r12)
	cmpq	%r14, %rax
	jne	.L77
	movq	16(%rsp), %rax
	movl	$0, 40(%rsp)
	movl	16(%rbx), %r10d
	movq	(%rax), %r15
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$-21576, %esi
	movl	$4, %r8d
	movl	$7, %eax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L434:
	movq	64(%rsp), %rdi
	movq	%r14, (%rbx)
	movq	128(%rsp), %rax
	addq	%rax, (%rdi)
.L151:
	movl	248(%rsp), %eax
	testl	%eax, %eax
	je	.L10
	cmpl	$7, 8(%rsp)
	jne	.L10
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L206
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbx), %rcx
	je	.L208
.L207:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L207
.L208:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L432:
	movl	$-23381, %esi
	movl	$14, %r8d
	movl	$11, %eax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L256:
	movl	8(%rsp), %r10d
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L437:
	cmpl	$223, %r8d
	ja	.L121
	leal	65216(%r8), %eax
	addq	$2, %rdx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L144:
	testw	%cx, %cx
	js	.L447
	leaq	1(%r14), %rax
	cmpq	%rax, (%rsp)
	jbe	.L289
.L147:
	shrl	$8, %ecx
	movq	%rax, 152(%rsp)
	orl	$-128, %ecx
	movb	%cl, (%r14)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 152(%rsp)
	movl	%edi, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L435:
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	movl	40(%rsp), %eax
	movl	%eax, (%r12)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	movl	16(%rbx), %eax
	je	.L448
	movl	%eax, 40(%rsp)
	leaq	168(%rsp), %rax
	movq	%r11, 160(%rsp)
	movq	%r15, 168(%rsp)
	movq	%r15, %rdx
	movl	$4, %r8d
	movq	%rax, 56(%rsp)
	leaq	160(%rsp), %rax
	movl	$-21532, %r14d
	movq	%rax, 112(%rsp)
.L182:
	cmpq	%rbp, %r11
	je	.L183
.L202:
	leaq	4(%r11), %rsi
	cmpq	%rsi, %rbp
	jb	.L273
	cmpq	%rdx, 8(%rsp)
	jbe	.L290
	movl	(%r12), %eax
	movl	(%r11), %ecx
	sarl	$3, %eax
	testl	%eax, %eax
	je	.L184
	cmpl	$741, %ecx
	movl	%eax, %r9d
	je	.L275
	cmpl	$745, %ecx
	je	.L276
	cmpl	$768, %ecx
	je	.L277
	cmpl	$769, %ecx
	je	.L278
	cmpl	$12442, %ecx
	je	.L449
.L186:
	leaq	1(%rdx), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L290
	movq	%rcx, 168(%rsp)
	movb	%ah, (%rdx)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	movb	%al, (%rdx)
	movq	160(%rsp), %r11
	movl	$0, (%r12)
	movq	168(%rsp), %rdx
	cmpq	%rbp, %r11
	jne	.L202
.L183:
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	movq	136(%rsp), %rax
	movq	%rax, 8(%rsp)
.L181:
	cmpq	8(%rsp), %rdx
	jne	.L158
	cmpq	$5, %r8
	jne	.L157
.L179:
	cmpq	%r15, %rdx
	jne	.L152
.L160:
	subl	$1, 20(%rbx)
	jmp	.L152
.L438:
	cmpq	%r9, %rbp
	jbe	.L244
	movzbl	2(%rdx), %eax
	leal	384(%r8), %esi
	addl	$-128, %eax
	cmpl	$545, %esi
	movzbl	%al, %eax
	je	.L237
	leal	-163(%r8), %edi
	cmpl	$2, %edi
	ja	.L119
	subl	$68, %r8d
.L118:
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L121
	imull	$94, %r8d, %r8d
	leaq	3(%rdx), %r9
	leal	(%r8,%rax), %esi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rsi,2), %eax
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rsi
	movzbl	%ah, %edi
	movzbl	%al, %eax
	addl	(%rsi,%rdi,4), %eax
	cmpl	$65533, %eax
	jne	.L122
	jmp	.L121
.L441:
	movq	88(%rsp), %rax
	addq	$4, %rdx
	movl	$6, 8(%rsp)
	movq	%rdx, 144(%rsp)
	addq	$1, (%rax)
	jmp	.L128
.L447:
	leaq	2(%r14), %rax
	cmpq	%rax, (%rsp)
	jbe	.L289
	leaq	1(%r14), %rax
	movq	%rax, 152(%rsp)
	movb	$-113, (%r14)
	movq	152(%rsp), %r14
	leaq	1(%r14), %rax
	jmp	.L147
.L444:
	cmpl	$4, %eax
	ja	.L31
	leaq	168(%rsp), %rdx
	cltq
	xorl	%ecx, %ecx
.L32:
	movzbl	4(%r12,%rcx), %esi
	movb	%sil, (%rdx,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L32
	leaq	4(%r11), %rdi
	cmpq	%rdi, (%rsp)
	jb	.L215
	movq	%r15, %r8
.L33:
	addq	$1, %r8
	movzbl	-1(%r8), %r9d
	leaq	1(%rcx), %rsi
	cmpq	$2, %rsi
	movb	%r9b, 168(%rsp,%rcx)
	movl	$2, %ecx
	ja	.L291
	cmpq	%r8, %rbp
	ja	.L33
.L291:
	movl	40(%rsp), %ecx
	sarl	$3, %ecx
	testl	%ecx, %ecx
	jne	.L36
	movzbl	168(%rsp), %r8d
	cmpl	$127, %r8d
	movb	%r8b, 96(%rsp)
	jbe	.L219
	leal	-161(%r8), %ecx
	cmpl	$93, %ecx
	movl	%ecx, 8(%rsp)
	setbe	%r13b
	cmpl	$142, %r8d
	sete	%cl
	orb	%cl, %r13b
	jne	.L292
	cmpl	$143, %r8d
	je	.L292
.L38:
	cmpq	$0, 88(%rsp)
	je	.L87
	andl	$2, %r10d
	jne	.L450
.L87:
	movl	$6, 8(%rsp)
	jmp	.L10
.L448:
	cmpq	%r11, %rbp
	je	.L451
	andl	$2, %eax
	leaq	4(%r15), %r14
	cmpq	%r14, 8(%rsp)
	movq	%r15, %rdx
	movl	$4, 112(%rsp)
	movl	%eax, %r8d
	jb	.L452
	movl	40(%rsp), %eax
	movl	%r10d, 56(%rsp)
	movq	88(%rsp), %r10
	sarl	$3, %eax
	testl	%eax, %eax
	jne	.L162
.L454:
	movzbl	(%r11), %eax
	cmpl	$127, %eax
	ja	.L163
	addq	$1, %r11
.L162:
	movl	%eax, (%rdx)
	movq	%r14, %rdx
.L178:
	cmpq	%rbp, %r11
	je	.L453
.L180:
	leaq	4(%rdx), %r14
	cmpq	%r14, 8(%rsp)
	jb	.L258
	movl	(%r12), %eax
	sarl	$3, %eax
	testl	%eax, %eax
	je	.L454
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$5, 8(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$127, %ecx
	ja	.L190
	leaq	1(%rdx), %rax
	movq	%rax, 168(%rsp)
	movb	%cl, (%rdx)
.L191:
	movq	160(%rsp), %rax
	movq	168(%rsp), %rdx
	leaq	4(%rax), %r11
	movq	%r11, 160(%rsp)
	jmp	.L182
.L440:
	movq	%r11, 112(%rsp)
	movl	%r10d, 56(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	%rbx, %rsi
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	144(%rsp), %rdx
	movq	152(%rsp), %r14
	movl	56(%rsp), %r10d
	movq	112(%rsp), %r11
	je	.L142
	cmpl	$5, 8(%rsp)
	jne	.L128
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r15, %r14
	movq	%r11, %rdx
	movl	$5, 8(%rsp)
	jmp	.L129
.L190:
	leal	-65377(%rcx), %eax
	cmpl	$62, %eax
	ja	.L192
	leaq	1(%rdx), %rax
	cmpq	%rax, 8(%rsp)
	jbe	.L290
	movq	%rax, 168(%rsp)
	movb	$-114, (%rdx)
	addl	$64, %ecx
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%cl, (%rax)
	jmp	.L191
.L461:
	movl	%r10d, 124(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%rbx, %rsi
	movq	72(%rsp), %r9
	movq	128(%rsp), %rcx
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movslq	%eax, %r8
	popq	%rdx
	cmpl	$6, %r8d
	popq	%rcx
	movq	160(%rsp), %r11
	movq	168(%rsp), %rdx
	movl	124(%rsp), %r10d
	je	.L196
	cmpl	$5, %r8d
	jne	.L182
.L290:
	movl	$5, %r8d
	jmp	.L183
.L429:
	cmpq	$0, 32(%rsp)
	jne	.L455
	cmpl	$1, %ecx
	movq	32(%rbx), %rbp
	jne	.L14
	movl	0(%rbp), %r12d
	movq	(%rbx), %rax
	testl	%r12d, %r12d
	je	.L15
	movq	48(%rsp), %rdi
	movq	8(%rbx), %rdx
	cmpq	$0, 96(%rdi)
	je	.L456
	leaq	2(%rax), %r13
	cmpq	%r13, %rdx
	jb	.L215
	movl	%r12d, %edx
	sarl	$3, %edx
	movb	%dh, (%rax)
	movb	%dl, 1(%rax)
	movq	32(%rbx), %rdx
	movl	$0, (%rdx)
.L18:
	testb	$1, 16(%rbx)
	jne	.L457
	cmpq	%rax, %r13
	jbe	.L21
	movq	24(%rsp), %rbx
	movq	%rax, 168(%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	leaq	168(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%rsi
	popq	%rdi
	je	.L21
	cmpq	%r13, 168(%rsp)
	jne	.L458
.L23:
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L10
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L163:
	leal	-161(%rax), %edi
	cmpl	$93, %edi
	setbe	%sil
	cmpl	$142, %eax
	sete	%cl
	orb	%cl, %sil
	jne	.L295
	cmpl	$143, %eax
	je	.L295
	testq	%r10, %r10
	je	.L272
	testl	%r8d, %r8d
	jne	.L459
.L272:
	movl	56(%rsp), %r10d
	movl	$6, %r8d
.L161:
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	jmp	.L181
.L287:
	movl	%r10d, 8(%rsp)
	jmp	.L151
.L237:
	movl	$94, %r8d
	jmp	.L118
.L119:
	cmpl	$552, %esi
	je	.L238
	leal	-172(%r8), %esi
	cmpl	$3, %esi
	ja	.L120
	subl	$73, %r8d
	jmp	.L118
.L192:
	cmpl	$173759, %ecx
	ja	.L193
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%ecx, %eax
	shrl	$6, %eax
	movswl	(%rdi,%rax,2), %eax
	testl	%eax, %eax
	js	.L193
	movl	%ecx, %edi
	sall	$6, %eax
	andl	$63, %edi
	addl	%eax, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rdi,2), %eax
	testl	%eax, %eax
	movl	%eax, %edi
	je	.L193
	testb	$-128, %al
	je	.L198
	testw	%ax, %ax
	js	.L199
	sall	$3, %eax
	orl	$263168, %eax
	movl	%eax, (%r12)
.L418:
	movq	%rsi, 160(%rsp)
	movq	%rsi, %r11
	jmp	.L182
.L273:
	movl	$7, %r8d
	jmp	.L183
.L14:
	movq	$0, 0(%rbp)
	testb	$1, 16(%rbx)
	movl	$0, 8(%rsp)
	jne	.L10
	movq	24(%rsp), %rbx
	movl	%ecx, (%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	pushq	%rax
	movl	8(%rsp), %ecx
	pushq	%rcx
	xorl	%ecx, %ecx
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r14
	popq	%r15
	jmp	.L10
.L72:
	cmpl	$127, %eax
	ja	.L81
	leaq	1(%r11), %rdx
	movq	%rdx, 168(%rsp)
	movb	%al, (%r11)
.L82:
	movq	160(%rsp), %rax
	addq	$4, %rax
	cmpq	%r14, %rax
	movq	%rax, 160(%rsp)
	je	.L416
.L415:
	movl	(%r12), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
.L77:
	subq	%r14, %rax
	cmpq	%rdx, %rax
	jle	.L460
	movq	16(%rsp), %rdi
	subq	%rdx, %rax
	movq	168(%rsp), %r11
	movl	16(%rbx), %r10d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r15
	movl	%ecx, %eax
	andl	$-8, %eax
	movl	%eax, (%r12)
	movl	%eax, 40(%rsp)
	jmp	.L28
.L241:
	movq	%r9, %rdx
	jmp	.L109
.L15:
	testl	%edx, %edx
	jne	.L209
.L21:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	$1
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%rax
	popq	%rdx
	jmp	.L10
.L193:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L418
	cmpq	$0, 88(%rsp)
	je	.L283
	testb	$8, 16(%rbx)
	jne	.L461
.L196:
	testb	$2, 40(%rsp)
	jne	.L462
.L283:
	movl	$6, %r8d
	jmp	.L183
.L295:
	leaq	1(%r11), %rcx
	cmpq	%rcx, %rbp
	movq	%rcx, 40(%rsp)
	jbe	.L264
	movzbl	1(%r11), %ecx
	leal	-161(%rcx), %r9d
	cmpl	$93, %r9d
	ja	.L174
	cmpl	$142, %eax
	je	.L463
	cmpl	$143, %eax
	leaq	2(%r11), %r9
	je	.L464
	leal	-128(%rcx), %eax
	movzbl	%al, %eax
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L174
	imull	$94, %edi, %edi
	leal	(%rdi,%rax), %ecx
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rcx
	movzbl	%ah, %esi
	movzbl	%al, %eax
	addl	(%rcx,%rsi,4), %eax
	cmpl	$65533, %eax
	je	.L174
.L175:
	testl	%eax, %eax
	je	.L174
	cmpl	$127, %eax
	ja	.L269
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rcx
	subl	$1, %eax
	movzwl	2(%rcx,%rax,4), %esi
	movzwl	(%rcx,%rax,4), %eax
	movl	%eax, (%rdx)
	leaq	8(%rdx), %rax
	cmpq	%rax, 8(%rsp)
	jnb	.L465
	sall	$3, %esi
	cmpq	%r14, 8(%rsp)
	movq	16(%rsp), %rax
	movl	%esi, (%r12)
	movl	56(%rsp), %r10d
	movq	%r9, (%rax)
	jne	.L158
	movq	8(%rsp), %rdx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L238:
	movl	$98, %r8d
	jmp	.L118
.L275:
	movl	%r14d, %esi
	movl	$1, %edi
	xorl	%ecx, %ecx
.L185:
	addl	%ecx, %edi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L466:
	addl	$1, %ecx
	cmpl	%edi, %ecx
	je	.L186
	movl	%ecx, %esi
	movzwl	0(%r13,%rsi,4), %esi
.L188:
	cmpw	%r9w, %si
	jne	.L466
	leaq	1(%rdx), %rax
	cmpq	%rax, 8(%rsp)
	jbe	.L290
	movzwl	2(%r13,%rcx,4), %ecx
	movq	%rax, 168(%rsp)
	movl	%ecx, %eax
	shrw	$8, %ax
	movb	%al, (%rdx)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%cl, (%rax)
	movq	160(%rsp), %rax
	movl	$0, (%r12)
	movq	168(%rsp), %rdx
	leaq	4(%rax), %r11
	movq	%r11, 160(%rsp)
	jmp	.L182
.L276:
	movl	$-21536, %esi
	movl	$1, %edi
	movl	$1, %ecx
	jmp	.L185
.L464:
	cmpq	%r9, %rbp
	jbe	.L264
	movzbl	2(%r11), %eax
	leal	-128(%rax), %edi
	leal	384(%rcx), %eax
	cmpl	$545, %eax
	movzbl	%dil, %edi
	je	.L265
	leal	-163(%rcx), %esi
	cmpl	$2, %esi
	ja	.L172
	subl	$68, %ecx
.L171:
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L174
	imull	$94, %ecx, %ecx
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	leaq	3(%r11), %r9
	addl	%edi, %ecx
	movzwl	(%rax,%rcx,2), %eax
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rcx
	movzbl	%ah, %esi
	movzbl	%al, %eax
	addl	(%rcx,%rsi,4), %eax
	cmpl	$65533, %eax
	jne	.L175
.L174:
	testq	%r10, %r10
	je	.L272
	testl	%r8d, %r8d
	je	.L272
	addq	$1, (%r10)
	movq	40(%rsp), %r11
	movl	$6, 112(%rsp)
	jmp	.L180
.L277:
	movl	$-22052, %esi
	movl	$5, %edi
	movl	$2, %ecx
	jmp	.L185
.L228:
	movq	%r15, %r14
	movq	%rbp, %rdx
	movl	$4, 8(%rsp)
	jmp	.L129
.L457:
	movq	%r13, %rax
.L209:
	movq	%rax, (%rbx)
	movl	$0, 8(%rsp)
	jmp	.L10
.L278:
	movl	$-21576, %esi
	movl	$4, %edi
	movl	$7, %ecx
	jmp	.L185
.L101:
	cmpl	$0, 8(%rsp)
	jne	.L10
.L416:
	movq	16(%rsp), %rax
	movl	16(%rbx), %r10d
	movq	(%rax), %r15
	movl	(%r12), %eax
	movl	%eax, 40(%rsp)
	jmp	.L28
.L81:
	leal	-65377(%rax), %edx
	cmpl	$62, %edx
	ja	.L83
	leaq	1(%r11), %rdx
	cmpq	%rdx, (%rsp)
	jbe	.L215
	movq	%rdx, 168(%rsp)
	movb	$-114, (%r11)
	addl	$64, %eax
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	movb	%al, (%rdx)
	jmp	.L82
.L445:
	movq	%rbp, %rdx
	movq	16(%rsp), %rax
	subq	%r15, %rdx
	addq	%r13, %rdx
	cmpq	$4, %rdx
	movq	%rbp, (%rax)
	ja	.L64
	cmpq	%r13, %rdx
	leaq	1(%r15), %rax
	jbe	.L68
.L67:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%r12,%r13)
	addq	$1, %r13
	cmpq	%r13, %rdx
	jne	.L67
.L68:
	movl	$7, 8(%rsp)
	jmp	.L10
.L449:
	movl	$-23381, %esi
	movl	$14, %edi
	movl	$11, %ecx
	jmp	.L185
.L219:
	leaq	1(%rdx), %rsi
.L37:
	movl	%r8d, (%r11)
.L410:
	movl	(%r12), %ecx
	movq	%rdi, %r11
	movl	%ecx, %eax
	movl	%ecx, 40(%rsp)
	andl	$7, %eax
.L43:
	subq	%rdx, %rsi
	cmpq	%rax, %rsi
	jle	.L467
	subq	%rax, %rsi
	movq	16(%rsp), %rax
	andl	$-8, 40(%rsp)
	addq	%rsi, %r15
	movl	16(%rbx), %r10d
	movq	%r15, (%rax)
	movl	40(%rsp), %eax
	movl	%eax, (%r12)
	jmp	.L28
.L463:
	cmpl	$223, %ecx
	ja	.L174
	leal	65216(%rcx), %eax
	addq	$2, %r11
	jmp	.L162
.L198:
	testw	%ax, %ax
	js	.L468
	leaq	1(%rdx), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L290
.L201:
	shrl	$8, %eax
	movq	%rcx, 168(%rsp)
	orl	$-128, %edi
	orl	$-128, %eax
	movb	%al, (%rdx)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%dil, (%rax)
	jmp	.L191
.L258:
	movl	56(%rsp), %r10d
	movl	$5, %r8d
	jmp	.L161
.L222:
	movl	$-21532, %edx
	movl	$1, %esi
	xorl	%eax, %eax
.L73:
	addl	%eax, %esi
	leaq	comp_table_data(%rip), %r9
	jmp	.L76
.L469:
	addl	$1, %eax
	cmpl	%esi, %eax
	je	.L74
	movl	%eax, %edx
	movzwl	(%r9,%rdx,4), %edx
.L76:
	cmpw	%dx, %di
	jne	.L469
	leaq	1(%r11), %rdx
	cmpq	%rdx, (%rsp)
	jbe	.L215
	leaq	comp_table_data(%rip), %rsi
	movq	%rdx, 168(%rsp)
	movzwl	2(%rsi,%rax,4), %esi
	movl	%esi, %eax
	shrw	$8, %ax
	movb	%al, (%r11)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%sil, (%rax)
	xorl	%edx, %edx
	movq	160(%rsp), %rax
	movl	$0, (%r12)
	addq	$4, %rax
	cmpq	%r14, %rax
	movq	%rax, 160(%rsp)
	jne	.L77
	jmp	.L416
.L292:
	leaq	(%rdx,%rsi), %r14
	leaq	1(%rdx), %r13
	cmpq	%r13, %r14
	jbe	.L40
	movzbl	169(%rsp), %ecx
	leal	-161(%rcx), %r9d
	movb	%cl, 56(%rsp)
	cmpl	$93, %r9d
	ja	.L414
	cmpl	$142, %r8d
	je	.L470
	cmpl	$143, %r8d
	je	.L471
	movzbl	56(%rsp), %ecx
	addl	$-128, %ecx
	movzbl	%cl, %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L38
	imull	$94, 8(%rsp), %esi
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %r8
	addl	%ecx, %esi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rcx
	movzwl	(%rcx,%rsi,2), %ecx
	movzbl	%ch, %esi
	movzbl	%cl, %ecx
	addl	(%r8,%rsi,4), %ecx
	cmpl	$65533, %ecx
	je	.L38
	leaq	2(%rdx), %rsi
.L51:
	testl	%ecx, %ecx
	je	.L38
	cmpl	$127, %ecx
	ja	.L53
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %r8
	subl	$1, %ecx
	movzwl	2(%r8,%rcx,4), %eax
	movzwl	(%r8,%rcx,4), %ecx
	movl	%ecx, (%r11)
	leaq	8(%r11), %rcx
	cmpq	%rcx, (%rsp)
	jnb	.L472
	sall	$3, %eax
	movq	%rdi, %r11
	movl	%eax, 40(%rsp)
	movl	%eax, (%r12)
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	4(%rax), %r13
	cmpq	%rdx, %r13
	ja	.L215
	movl	%r12d, %edx
	sarl	$3, %edx
	movl	%edx, (%rax)
	movl	$0, 0(%rbp)
	jmp	.L18
.L439:
	movl	%eax, 4(%r14)
	movq	%rdx, %r14
	movq	%r9, %rdx
	jmp	.L125
.L459:
	addq	$1, %r11
	addq	$1, (%r10)
	movl	$6, 112(%rsp)
	jmp	.L178
.L223:
	movl	$-21536, %edx
	movl	$1, %esi
	movl	$1, %eax
	jmp	.L73
.L83:
	cmpl	$173759, %eax
	ja	.L84
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rcx
	movl	%eax, %edx
	shrl	$6, %edx
	movswl	(%rcx,%rdx,2), %edx
	testl	%edx, %edx
	js	.L84
	movl	%eax, %ecx
	sall	$6, %edx
	andl	$63, %ecx
	addl	%edx, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	testl	%edx, %edx
	movl	%edx, %edi
	je	.L84
	testb	$-128, %dl
	je	.L93
	testw	%dx, %dx
	js	.L473
	sall	$3, %edx
	leaq	4(%r14), %rax
	movl	%edx, %ecx
	xorl	%edx, %edx
	orl	$263168, %ecx
	movl	%ecx, (%r12)
	movq	%rax, 160(%rsp)
	jmp	.L77
.L224:
	movl	$-22052, %edx
	movl	$5, %esi
	movl	$2, %eax
	jmp	.L73
.L462:
	movq	88(%rsp), %rax
	addq	$4, %r11
	movl	$6, %r8d
	movq	%r11, 160(%rsp)
	addq	$1, (%rax)
	jmp	.L182
.L452:
	cmpq	%r15, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	je	.L160
.L158:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	2(%rdx), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L290
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	movb	$-113, (%rdx)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	jmp	.L201
.L36:
	movl	%ecx, (%r11)
	movl	(%r12), %eax
	movl	16(%rbx), %r10d
	movl	%eax, 40(%rsp)
	jmp	.L28
.L450:
	movq	88(%rsp), %rdi
	leaq	1(%rdx), %rsi
	addq	$1, (%rdi)
	jmp	.L43
.L264:
	movl	56(%rsp), %r10d
	movl	$7, %r8d
	jmp	.L161
.L225:
	movl	$-21576, %edx
	movl	$4, %esi
	movl	$7, %eax
	jmp	.L73
.L84:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L474
	cmpq	$0, 88(%rsp)
	je	.L87
	testb	$8, %r10b
	jne	.L475
	andl	$2, %r10d
	je	.L87
.L92:
	movq	88(%rsp), %rax
	addq	$4, 160(%rsp)
	addq	$1, (%rax)
.L91:
	movq	160(%rsp), %rax
	cmpq	%r14, %rax
	jne	.L415
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L451:
	movq	8(%rsp), %r10
	movq	16(%rsp), %rax
	cmpq	%r15, %r10
	movq	%rbp, (%rax)
	jne	.L158
.L157:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	movl	56(%rsp), %r10d
	movslq	112(%rsp), %r8
	jmp	.L161
.L172:
	cmpl	$552, %eax
	je	.L266
	leal	-172(%rcx), %eax
	cmpl	$3, %eax
	ja	.L173
	subl	$73, %ecx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L265:
	movl	$94, %ecx
	jmp	.L171
.L269:
	movq	%r9, %r11
	jmp	.L162
.L93:
	testw	%dx, %dx
	js	.L476
	leaq	1(%r11), %rsi
	cmpq	%rsi, (%rsp)
	jbe	.L215
	movq	%r11, %rcx
.L96:
	shrl	$8, %edx
	movq	%rsi, 168(%rsp)
	orl	$-128, %edx
	movb	%dl, (%rcx)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 168(%rsp)
	movl	%edi, %eax
	orl	$-128, %eax
	movb	%al, (%rdx)
	jmp	.L82
.L446:
	movl	$-23381, %edx
	movl	$14, %esi
	movl	$11, %eax
	jmp	.L73
.L471:
	leaq	2(%rdx), %r8
	cmpq	%r8, %r14
	jbe	.L40
	movzbl	170(%rsp), %esi
	leal	384(%rcx), %r8d
	addl	$-128, %esi
	cmpl	$545, %r8d
	movzbl	%sil, %esi
	je	.L220
	leal	-163(%rcx), %r9d
	cmpl	$2, %r9d
	ja	.L48
	subl	$68, %ecx
.L47:
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L38
	imull	$94, %ecx, %ecx
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %r8
	addl	%ecx, %esi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rcx
	movzwl	(%rcx,%rsi,2), %ecx
	movzbl	%ch, %esi
	movzbl	%cl, %ecx
	addl	(%r8,%rsi,4), %ecx
	cmpl	$65533, %ecx
	je	.L38
	leaq	3(%rdx), %rsi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L470:
	cmpl	$223, %ecx
	leal	65216(%rcx), %r8d
	leaq	2(%rdx), %rsi
	jbe	.L37
.L414:
	cmpq	$0, 88(%rsp)
	je	.L87
	andb	$2, %r10b
	je	.L87
	movq	88(%rsp), %rdi
	movq	%r13, %rsi
	addq	$1, (%rdi)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %r14
	je	.L477
	movq	%rsi, %rbx
	subq	%rax, %rbx
	movq	%rbx, %rax
	movq	16(%rsp), %rbx
	addq	%r15, %rax
	movq	%rax, (%rbx)
	movl	40(%rsp), %eax
	andl	$-8, %eax
	movslq	%eax, %rcx
	cmpq	%rcx, %rsi
	jle	.L478
	cmpq	$4, %rsi
	ja	.L479
	orl	%esi, %eax
	testq	%rsi, %rsi
	movl	%eax, (%r12)
	je	.L68
	xorl	%eax, %eax
	movzbl	96(%rsp), %r9d
	jmp	.L60
.L480:
	movzbl	(%rdx,%rax), %r9d
.L60:
	movb	%r9b, 4(%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L480
	jmp	.L68
.L460:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L479:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L478:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L477:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L467:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L473:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L455:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L476:
	leaq	2(%r11), %rax
	cmpq	%rax, (%rsp)
	jbe	.L215
	leaq	1(%r11), %rax
	movq	%rax, 168(%rsp)
	movb	$-113, (%r11)
	movq	168(%rsp), %rcx
	leaq	1(%rcx), %rsi
	jmp	.L96
.L64:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L31:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L48:
	cmpl	$552, %r8d
	je	.L221
	leal	-172(%rcx), %r8d
	cmpl	$3, %r8d
	ja	.L49
	subl	$73, %ecx
	jmp	.L47
.L220:
	movl	$94, %ecx
	jmp	.L47
.L49:
	leal	-238(%rcx), %r8d
	cmpl	$16, %r8d
	ja	.L38
	subl	$135, %ecx
	jmp	.L47
.L221:
	movl	$98, %ecx
	jmp	.L47
.L199:
	leaq	__PRETTY_FUNCTION__.9123(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L443:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L472:
	movl	%eax, 4(%r11)
	movl	(%r12), %edi
	movq	%rcx, %r11
	movl	%edi, %eax
	movl	%edi, 40(%rsp)
	andl	$7, %eax
	jmp	.L43
.L53:
	movl	%ecx, (%r11)
	jmp	.L410
.L458:
	movl	%r12d, 0(%rbp)
	jmp	.L23
.L465:
	movl	%esi, 4(%rdx)
	movq	%r9, %r11
	movq	%rax, %rdx
	jmp	.L178
.L475:
	leaq	(%r14,%r13), %rax
	movq	%r11, 96(%rsp)
	movl	%r10d, 56(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%rax, 48(%rsp)
	pushq	96(%rsp)
	movq	%rax, %r8
	movq	64(%rsp), %rdi
	movq	%rbx, %rsi
	leaq	184(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movl	56(%rsp), %r10d
	movq	96(%rsp), %r11
	je	.L481
	movq	160(%rsp), %rax
	cmpq	%r14, %rax
	jne	.L415
	cmpl	$7, 8(%rsp)
	jne	.L101
	leaq	4(%r14), %rax
	cmpq	%rax, 40(%rsp)
	je	.L482
	movl	(%r12), %eax
	movq	%r13, %rbx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r13
	jle	.L483
	cmpq	$4, %r13
	ja	.L484
	orl	%r13d, %eax
	testq	%r13, %r13
	movl	%eax, (%r12)
	je	.L68
	xorl	%eax, %eax
.L105:
	movzbl	(%r14,%rax), %edx
	movb	%dl, 4(%r12,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L105
	jmp	.L68
.L474:
	leaq	4(%r14), %rax
	movslq	%esi, %rdx
	movl	%esi, %ecx
	movq	%rax, 160(%rsp)
	jmp	.L77
.L484:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L483:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L482:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L481:
	andb	$2, %r10b
	je	.L91
	jmp	.L92
.L206:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L61:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L173:
	leal	-238(%rcx), %eax
	cmpl	$16, %eax
	ja	.L174
	subl	$135, %ecx
	jmp	.L171
.L266:
	movl	$98, %ecx
	jmp	.L171
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9123, @object
	.size	__PRETTY_FUNCTION__.9123, 16
__PRETTY_FUNCTION__.9123:
	.string	"to_euc_jisx0213"
	.align 16
	.type	__PRETTY_FUNCTION__.9142, @object
	.size	__PRETTY_FUNCTION__.9142, 23
__PRETTY_FUNCTION__.9142:
	.string	"to_euc_jisx0213_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9040, @object
	.size	__PRETTY_FUNCTION__.9040, 25
__PRETTY_FUNCTION__.9040:
	.string	"from_euc_jisx0213_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9223, @object
	.size	__PRETTY_FUNCTION__.9223, 6
__PRETTY_FUNCTION__.9223:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 100
comp_table_data:
	.value	-21532
	.value	-21531
	.value	-21536
	.value	-21530
	.value	-22052
	.value	-21564
	.value	-21576
	.value	-21560
	.value	-21577
	.value	-21558
	.value	-21584
	.value	-21556
	.value	-21565
	.value	-21554
	.value	-21576
	.value	-21559
	.value	-21577
	.value	-21557
	.value	-21584
	.value	-21555
	.value	-21565
	.value	-21553
	.value	-23381
	.value	-23305
	.value	-23379
	.value	-23304
	.value	-23377
	.value	-23303
	.value	-23375
	.value	-23302
	.value	-23373
	.value	-23301
	.value	-23125
	.value	-23049
	.value	-23123
	.value	-23048
	.value	-23121
	.value	-23047
	.value	-23119
	.value	-23046
	.value	-23117
	.value	-23045
	.value	-23109
	.value	-23044
	.value	-23100
	.value	-23043
	.value	-23096
	.value	-23042
	.value	-22795
	.value	-22792
