	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	testb	%sil, %sil
	js	.L2
	cmpb	$92, %sil
	je	.L4
	movzbl	%sil, %eax
	cmpb	$126, %sil
	movl	$8254, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leal	95(%rsi), %edx
	movzbl	%sil, %esi
	movl	$-1, %eax
	addl	$65216, %esi
	cmpb	$63, %dl
	cmovb	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$165, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"SHIFT_JISX0213//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$17, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$8589934593, %rdx
	movabsq	$34359738372, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	32(%rax), %rsi
	movl	$17, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L12
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"(jch & 0x8000) == 0"
.LC11:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC13:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	subq	$184, %rsp
	movq	%rdi, 56(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rbx), %rdi
	movq	%r8, 32(%rsp)
	movq	%r9, 64(%rsp)
	movl	240(%rsp), %eax
	movq	%rdi, 80(%rsp)
	movl	16(%rbx), %edi
	movq	$0, 24(%rsp)
	movl	%edi, %ecx
	movl	%edi, 48(%rsp)
	andl	$1, %ecx
	jne	.L14
	cmpq	$0, 104(%rsi)
	movq	144(%rsi), %rdx
	movq	%rdx, 24(%rsp)
	je	.L14
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 24(%rsp)
.L14:
	testl	%eax, %eax
	jne	.L463
	movq	16(%rsp), %rdi
	movq	32(%rbx), %rbp
	leaq	128(%rsp), %rcx
	movl	248(%rsp), %esi
	movq	8(%rbx), %r10
	movq	(%rdi), %r11
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	movq	%rdi, %rdx
	movl	0(%rbp), %edi
	cmove	%rbx, %rdx
	cmpq	$0, 64(%rsp)
	movq	(%rdx), %r15
	movl	$0, %edx
	movq	$0, 128(%rsp)
	movl	%edi, 40(%rsp)
	cmovne	%rcx, %rdx
	testl	%esi, %esi
	movq	%rdx, 88(%rsp)
	jne	.L31
.L450:
	movq	56(%rsp), %rax
	movq	96(%rax), %rdx
.L32:
	leaq	152(%rsp), %rax
	testq	%rdx, %rdx
	movq	%r15, (%rsp)
	leaq	comp_table_data(%rip), %r12
	movq	%r10, %r15
	movq	%r11, %r10
	movq	%rax, 96(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 104(%rsp)
	je	.L464
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%rsp), %rax
	movq	%r10, 144(%rsp)
	movq	%r10, %rdx
	movl	$4, 8(%rsp)
	movl	$-31100, %r11d
	movq	%rax, 152(%rsp)
	movq	%rax, %r13
.L154:
	cmpq	%rdx, %r14
	je	.L155
.L184:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jb	.L299
	cmpq	%r13, %r15
	jbe	.L338
	movl	0(%rbp), %ecx
	movl	(%rdx), %eax
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L156
	cmpl	$741, %eax
	movl	%ecx, %edi
	je	.L301
	cmpl	$745, %eax
	je	.L302
	cmpl	$768, %eax
	je	.L303
	cmpl	$769, %eax
	je	.L304
	cmpl	$12442, %eax
	je	.L465
.L158:
	leaq	1(%r13), %rax
	cmpq	%rax, %r15
	jbe	.L338
	movq	%rax, 152(%rsp)
	movzbl	%ch, %eax
	movb	%al, 0(%r13)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	%cl, (%rax)
	movq	144(%rsp), %rdx
	movl	$0, 0(%rbp)
	movq	152(%rsp), %r13
	cmpq	%rdx, %r14
	jne	.L184
	.p2align 4,,10
	.p2align 3
.L155:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L466
.L185:
	addl	$1, 20(%rbx)
	testb	$1, 16(%rbx)
	jne	.L467
	cmpq	%r13, (%rsp)
	movq	%r10, 48(%rsp)
	jnb	.L309
	movq	24(%rsp), %rdi
	movq	(%rbx), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rdi
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r9
	popq	%r10
	je	.L189
	movq	136(%rsp), %rax
	movq	48(%rsp), %r10
	cmpq	%r13, %rax
	movq	%rax, 8(%rsp)
	jne	.L468
.L188:
	testl	%r11d, %r11d
	jne	.L335
.L257:
	movq	56(%rsp), %rax
	movq	96(%rax), %rdx
	movq	16(%rsp), %rax
	movq	(%rax), %r10
	movl	0(%rbp), %eax
	testq	%rdx, %rdx
	movl	%eax, 40(%rsp)
	movl	16(%rbx), %eax
	movl	%eax, 48(%rsp)
	movq	(%rbx), %rax
	movq	%rax, (%rsp)
	jne	.L125
.L464:
	cmpq	%r10, %r14
	movq	(%rsp), %r13
	je	.L286
	leaq	4(%r13), %rcx
	cmpq	%rcx, %r15
	jb	.L287
	movl	40(%rsp), %eax
	movl	48(%rsp), %edi
	movq	%r10, %rdx
	movl	$4, 8(%rsp)
	movl	$8254, %r11d
	sarl	$3, %eax
	andl	$2, %edi
	testl	%eax, %eax
	movl	%edi, 48(%rsp)
	jne	.L128
	movzbl	(%rdx), %eax
	cmpl	$127, %eax
	ja	.L129
.L470:
	cmpl	$92, %eax
	je	.L289
	cmpl	$126, %eax
	cmove	%r11d, %eax
.L452:
	addq	$1, %rdx
.L128:
	movl	%eax, 0(%r13)
.L151:
	cmpq	%rdx, %r14
	je	.L469
	leaq	4(%rcx), %rsi
	cmpq	%rsi, %r15
	jb	.L288
.L483:
	movl	0(%rbp), %eax
	movq	%rcx, %r13
	movq	%rsi, %rcx
	sarl	$3, %eax
	testl	%eax, %eax
	jne	.L128
	movzbl	(%rdx), %eax
	cmpl	$127, %eax
	jbe	.L470
.L129:
	leal	-161(%rax), %esi
	cmpl	$62, %esi
	ja	.L131
	addl	$65216, %eax
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L156:
	cmpl	$127, %eax
	ja	.L162
	leaq	1(%r13), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, 0(%r13)
.L163:
	movq	144(%rsp), %rax
	movq	152(%rsp), %r13
	leaq	4(%rax), %rdx
	movq	%rdx, 144(%rsp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	$165, %eax
	je	.L471
	cmpl	$8254, %eax
	je	.L472
	leal	-65377(%rax), %ecx
	cmpl	$62, %ecx
	ja	.L166
	leaq	1(%r13), %rdx
	addl	$64, %eax
	movq	%rdx, 152(%rsp)
	movb	%al, 0(%r13)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L338:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movl	$5, 8(%rsp)
	movq	%rdx, (%rax)
	je	.L185
.L466:
	movq	32(%rsp), %rax
	movq	%r13, (%rax)
.L13:
	movl	8(%rsp), %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r14
	ja	.L473
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$7, 8(%rsp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L189:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L188
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L131:
	leal	-224(%rax), %edi
	leal	-129(%rax), %esi
	cmpl	$28, %edi
	jbe	.L346
	cmpl	$30, %esi
	jbe	.L346
	cmpq	$0, 88(%rsp)
	je	.L307
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jne	.L474
.L307:
	movl	$6, 8(%rsp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L301:
	movl	%r11d, %esi
	movl	$1, %r8d
	xorl	%eax, %eax
.L157:
	addl	%eax, %r8d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L475:
	addl	$1, %eax
	cmpl	%r8d, %eax
	je	.L158
	movl	%eax, %esi
	movzwl	(%r12,%rsi,4), %esi
.L160:
	cmpw	%si, %di
	jne	.L475
	leaq	1(%r13), %rcx
	cmpq	%rcx, %r15
	jbe	.L338
	movzwl	2(%r12,%rax,4), %edx
	movq	%rcx, 152(%rsp)
	movl	%edx, %eax
	shrw	$8, %ax
	movb	%al, 0(%r13)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rax)
	movq	144(%rsp), %rax
	movl	$0, 0(%rbp)
	movq	152(%rsp), %r13
	leaq	4(%rax), %rdx
	movq	%rdx, 144(%rsp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	1(%r13), %rax
	movq	%rax, 152(%rsp)
	movb	$92, 0(%r13)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$-31104, %esi
	movl	$1, %r8d
	movl	$1, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$-31365, %esi
	movl	$5, %r8d
	movl	$2, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	1(%r13), %rax
	movq	%rax, 152(%rsp)
	movb	$126, 0(%r13)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$173759, %eax
	jbe	.L476
.L167:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L453
	cmpq	$0, 88(%rsp)
	je	.L307
	testb	$8, 16(%rbx)
	jne	.L477
.L170:
	testb	$2, 48(%rsp)
	je	.L307
	movq	88(%rsp), %rax
	addq	$4, %rdx
	movl	$6, 8(%rsp)
	movq	%rdx, 144(%rsp)
	addq	$1, (%rax)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L473:
	movzbl	1(%rdx), %r8d
	movl	%r8d, %r9d
	subl	$64, %r9d
	cmpb	$-68, %r9b
	ja	.L149
	cmpl	$127, %r8d
	je	.L149
	leal	-193(%rax), %r9d
	cmpl	$223, %eax
	leal	-64(%r8), %eax
	cmova	%r9d, %esi
	addl	%esi, %esi
	cmpl	$127, %r8d
	jbe	.L139
	leal	-65(%r8), %eax
	cmpl	$93, %eax
	jbe	.L139
	leal	-159(%r8), %eax
	addl	$1, %esi
.L139:
	cmpl	$93, %esi
	jbe	.L140
	cmpl	$102, %esi
	jbe	.L141
	leal	230(%rsi), %r8d
	leal	519(%rsi), %r9d
	cmpl	$93, %r8d
	jbe	.L478
.L142:
	leal	-258(%r8), %esi
	cmpl	$2, %esi
	ja	.L147
	leal	-163(%r8), %esi
	imull	$94, %esi, %esi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L31:
	movl	40(%rsp), %r12d
	andl	$7, %r12d
	je	.L450
	cmpq	$0, 32(%rsp)
	jne	.L479
	movq	56(%rsp), %rdi
	movq	96(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L480
	cmpl	$4, %r12d
	movq	%r11, 160(%rsp)
	movq	%r15, 168(%rsp)
	ja	.L71
	leaq	152(%rsp), %r13
	movslq	%r12d, %r12
	xorl	%edx, %edx
.L72:
	movzbl	4(%rbp,%rdx), %ecx
	movb	%cl, 0(%r13,%rdx)
	addq	$1, %rdx
	cmpq	%r12, %rdx
	jne	.L72
	movq	%r11, %rdx
	subq	%r12, %rdx
	addq	$4, %rdx
	cmpq	%rdx, %r14
	jb	.L481
	cmpq	%r10, %r15
	jnb	.L271
	leaq	1(%r11), %rdx
	leaq	151(%rsp), %rdi
.L80:
	movq	%rdx, 160(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %r12
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	$3, %r12
	movb	%cl, (%rdi,%r12)
	ja	.L344
	cmpq	%rsi, %r14
	ja	.L80
.L344:
	movl	0(%rbp), %esi
	movq	%r13, 160(%rsp)
	movl	152(%rsp), %edx
	movl	%esi, %r8d
	sarl	$3, %r8d
	testl	%r8d, %r8d
	je	.L82
	cmpl	$741, %edx
	movl	%r8d, %edi
	je	.L280
	cmpl	$745, %edx
	je	.L281
	cmpl	$768, %edx
	je	.L282
	cmpl	$769, %edx
	je	.L283
	cmpl	$12442, %edx
	je	.L482
.L84:
	leaq	1(%r15), %rdx
	cmpq	%rdx, %r10
	jbe	.L271
	movl	%r8d, %ecx
	movq	%rdx, 168(%rsp)
	movzbl	%ch, %ecx
	movb	%cl, (%r15)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	movb	%r8b, (%rdx)
	xorl	%ecx, %ecx
	movq	160(%rsp), %rdx
	movl	$0, 0(%rbp)
	cmpq	%r13, %rdx
	jne	.L87
	movq	56(%rsp), %rax
	movl	$0, 40(%rsp)
	movq	96(%rax), %rdx
	movq	16(%rsp), %rax
	movq	(%rax), %r11
	movl	16(%rbx), %eax
	movl	%eax, 48(%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L476:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%eax, %ecx
	shrl	$6, %ecx
	movswl	(%rdi,%rcx,2), %ecx
	testl	%ecx, %ecx
	js	.L167
	movl	%eax, %edi
	sall	$6, %ecx
	andl	$63, %edi
	addl	%ecx, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rcx
	movzwl	(%rcx,%rdi,2), %r8d
	testl	%r8d, %r8d
	movl	%r8d, %edi
	je	.L167
	shrl	$8, %r8d
	movl	%edi, %ecx
	leal	-33(%r8), %eax
	andl	$127, %ecx
	cmpl	$93, %eax
	jbe	.L172
	cmpl	$204, %eax
	jbe	.L173
	leal	-135(%r8), %eax
.L172:
	leal	-33(%rcx), %r8d
	addl	$61, %ecx
	testb	$1, %al
	cmove	%r8d, %ecx
	shrl	%eax
	leal	129(%rax), %r9d
	leal	193(%rax), %r8d
	cmpl	$30, %eax
	cmovbe	%r9d, %r8d
	xorl	%eax, %eax
	cmpl	$62, %ecx
	seta	%al
	testb	$-128, %dil
	leal	64(%rcx,%rax), %eax
	je	.L182
	testw	%di, %di
	js	.L254
	sall	$8, %r8d
	orl	%eax, %r8d
	leal	0(,%r8,8), %eax
	movl	%eax, 0(%rbp)
.L453:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %rdx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$-31145, %esi
	movl	$4, %r8d
	movl	$7, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L149:
	cmpq	$0, 88(%rsp)
	je	.L307
	movl	48(%rsp), %eax
	testl	%eax, %eax
	je	.L307
	movq	88(%rsp), %rax
	movq	%r13, %rcx
	movq	%rdi, %rdx
	leaq	4(%rcx), %rsi
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	cmpq	%rsi, %r15
	jnb	.L483
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rcx, %r13
	movl	$5, 8(%rsp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L467:
	movq	64(%rsp), %rdi
	movq	%r13, (%rbx)
	movq	128(%rsp), %rax
	addq	%rax, (%rdi)
.L187:
	movl	248(%rsp), %eax
	testl	%eax, %eax
	je	.L13
	cmpl	$7, 8(%rsp)
	jne	.L13
	movq	16(%rsp), %rax
	movq	%r14, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L259
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbx), %rcx
	je	.L261
.L260:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L260
.L261:
	movq	16(%rsp), %rax
	movq	%r14, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$-32087, %esi
	movl	$14, %r8d
	movl	$11, %eax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L309:
	movl	8(%rsp), %r11d
	jmp	.L188
.L468:
	movq	16(%rsp), %rax
	movq	%r10, (%rax)
	movl	40(%rsp), %eax
	movl	%eax, 0(%rbp)
	movq	56(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L484
	movl	16(%rbx), %eax
	leaq	168(%rsp), %rdi
	movq	%r10, 160(%rsp)
	movl	$4, %r13d
	movq	%rdi, 48(%rsp)
	leaq	160(%rsp), %rdi
	movl	%eax, 40(%rsp)
	movq	(%rsp), %rax
	movq	%rdi, 112(%rsp)
	movq	%rax, 168(%rsp)
.L225:
	cmpq	%r10, %r14
	je	.L485
.L255:
	leaq	4(%r10), %rsi
	cmpq	%rsi, %r14
	jb	.L323
	cmpq	%rax, 8(%rsp)
	jbe	.L340
	movl	0(%rbp), %ecx
	movl	(%r10), %edx
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L227
	cmpl	$741, %edx
	movl	%ecx, %edi
	je	.L325
	cmpl	$745, %edx
	je	.L326
	cmpl	$768, %edx
	je	.L327
	cmpl	$769, %edx
	je	.L328
	cmpl	$12442, %edx
	je	.L486
.L229:
	leaq	1(%rax), %rdx
	cmpq	%rdx, 8(%rsp)
	jbe	.L340
	movq	%rdx, 168(%rsp)
	movb	%ch, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%cl, (%rax)
	movq	160(%rsp), %r10
	movl	$0, 0(%rbp)
	movq	168(%rsp), %rax
	cmpq	%r10, %r14
	jne	.L255
.L485:
	movq	%r14, %r10
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$165, %eax
	jmp	.L452
.L140:
	imull	$94, %esi, %esi
.L143:
	addl	%eax, %esi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %r8
	movzwl	(%rax,%rsi,2), %eax
	movzbl	%ah, %esi
	movzbl	%al, %eax
	addl	(%r8,%rsi,4), %eax
	je	.L149
	cmpl	$65533, %eax
	je	.L149
	addq	$2, %rdx
	cmpl	$127, %eax
	ja	.L128
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rsi
	leal	-1(%rax), %edi
	movzwl	2(%rsi,%rdi,4), %eax
	movzwl	(%rsi,%rdi,4), %esi
	movl	%esi, 0(%r13)
	leaq	8(%r13), %rsi
	cmpq	%rsi, %r15
	jnb	.L487
	sall	$3, %eax
	movq	%rcx, %r13
	movl	$5, 8(%rsp)
	movl	%eax, 0(%rbp)
	jmp	.L155
.L474:
	movq	88(%rsp), %rax
	addq	$1, %rdx
	movq	%r13, %rcx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L151
.L480:
	cmpl	$4, %r12d
	ja	.L36
	movzbl	4(%rbp), %eax
	cmpl	$1, %r12d
	movl	$1, %ecx
	movb	%al, 168(%rsp)
	je	.L37
	movzbl	5(%rbp), %eax
	movl	$2, %ecx
	movb	%al, 169(%rsp)
.L37:
	leaq	4(%r15), %rsi
	cmpq	%rsi, %r10
	jb	.L271
	movzbl	(%r11), %eax
	movb	%al, 168(%rsp,%rcx)
	movl	40(%rsp), %eax
	sarl	$3, %eax
	testl	%eax, %eax
	jne	.L40
	movzbl	168(%rsp), %eax
	cmpl	$127, %eax
	movl	%eax, %r13d
	ja	.L41
	cmpl	$92, %eax
	je	.L275
	cmpl	$126, %eax
	je	.L276
.L456:
	leaq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
.L42:
	movl	%eax, (%r15)
	movq	%rsi, %r15
.L50:
	movl	0(%rbp), %eax
	subq	%rdx, %rcx
	movl	%eax, %edx
	andl	$7, %edx
	cmpq	%rdx, %rcx
	jle	.L488
	movq	16(%rsp), %rdi
	subq	%rdx, %rcx
	andl	$-8, %eax
	addq	%rcx, %r11
	movl	%eax, 40(%rsp)
	movq	%r11, (%rdi)
	movl	%eax, 0(%rbp)
	movq	56(%rsp), %rax
	movq	96(%rax), %rdx
	movl	16(%rbx), %eax
	movl	%eax, 48(%rsp)
	jmp	.L32
.L484:
	cmpq	%r10, %r14
	movl	16(%rbx), %edx
	je	.L489
	movq	(%rsp), %rax
	andl	$2, %edx
	movl	$4, %r13d
	movl	%edx, 48(%rsp)
	movl	40(%rsp), %edx
	leaq	4(%rax), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L490
.L195:
	sarl	$3, %edx
	testl	%edx, %edx
	jne	.L198
	movzbl	(%r10), %edx
	cmpl	$127, %edx
	ja	.L199
	cmpl	$92, %edx
	je	.L312
	cmpl	$126, %edx
	movl	$8254, %edi
	cmove	%edi, %edx
.L454:
	addq	$1, %r10
.L198:
	movl	%edx, (%rax)
.L221:
	cmpq	%r14, %r10
	je	.L491
.L223:
	leaq	4(%rsi), %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L311
	movq	%rsi, %rax
	movl	0(%rbp), %edx
	movq	%rcx, %rsi
	jmp	.L195
.L271:
	movl	$5, 8(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L227:
	cmpl	$127, %edx
	ja	.L233
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
.L234:
	movq	160(%rsp), %rax
	leaq	4(%rax), %r10
	movq	168(%rsp), %rax
	movq	%r10, 160(%rsp)
	jmp	.L225
.L287:
	movq	(%rsp), %r13
	movq	%r10, %rdx
	movl	$5, 8(%rsp)
	jmp	.L155
.L182:
	leaq	1(%r13), %rcx
	cmpq	%rcx, %r15
	jbe	.L338
	movq	%rcx, 152(%rsp)
	movb	%r8b, 0(%r13)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	jmp	.L163
.L233:
	cmpl	$165, %edx
	je	.L492
	cmpl	$8254, %edx
	je	.L493
	leal	-65377(%rdx), %ecx
	cmpl	$62, %ecx
	jbe	.L494
	cmpl	$173759, %edx
	ja	.L238
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%edx, %ecx
	shrl	$6, %ecx
	movswl	(%rdi,%rcx,2), %ecx
	testl	%ecx, %ecx
	js	.L238
	movl	%edx, %edi
	sall	$6, %ecx
	andl	$63, %edi
	addl	%ecx, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rcx
	movzwl	(%rcx,%rdi,2), %edi
	testl	%edi, %edi
	movl	%edi, %ecx
	je	.L238
	shrl	$8, %edi
	movl	%ecx, %r8d
	leal	-33(%rdi), %edx
	andl	$127, %r8d
	cmpl	$93, %edx
	jbe	.L243
	cmpl	$204, %edx
	jbe	.L244
	leal	-135(%rdi), %edx
.L243:
	leal	-33(%r8), %r9d
	leal	61(%r8), %edi
	testb	$1, %dl
	cmove	%r9d, %edi
	shrl	%edx
	leal	129(%rdx), %r9d
	leal	193(%rdx), %r8d
	cmpl	$30, %edx
	movl	%r9d, %edx
	cmova	%r8d, %edx
	xorl	%r8d, %r8d
	cmpl	$62, %edi
	seta	%r8b
	testb	$-128, %cl
	leal	64(%rdi,%r8), %r8d
	je	.L253
	testw	%cx, %cx
	js	.L254
	sall	$8, %edx
	orl	%r8d, %edx
	sall	$3, %edx
	movl	%edx, 0(%rbp)
.L455:
	movq	%rsi, 160(%rsp)
	movq	%rsi, %r10
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L463:
	cmpq	$0, 32(%rsp)
	jne	.L495
	cmpl	$1, %eax
	movq	32(%rbx), %rbp
	jne	.L17
	movl	0(%rbp), %r12d
	movq	(%rbx), %rax
	testl	%r12d, %r12d
	je	.L18
	movq	56(%rsp), %rdi
	movq	8(%rbx), %rdx
	cmpq	$0, 96(%rdi)
	je	.L496
	leaq	2(%rax), %r13
	cmpq	%r13, %rdx
	jb	.L271
	movl	%r12d, %edx
	sarl	$3, %edx
	movb	%dh, (%rax)
	movb	%dl, 1(%rax)
	movq	32(%rbx), %rdx
	movl	$0, (%rdx)
.L21:
	testb	$1, 16(%rbx)
	jne	.L497
	cmpq	%r13, %rax
	jnb	.L24
	movq	24(%rsp), %rbx
	movq	%rax, 168(%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	leaq	168(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%rbx
	popq	%r14
	je	.L24
	cmpq	%r13, 168(%rsp)
	jne	.L498
.L26:
	movl	8(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L13
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L199:
	leal	-161(%rdx), %ecx
	cmpl	$62, %ecx
	ja	.L201
	addl	$65216, %edx
	jmp	.L454
.L141:
	cmpl	$98, %esi
	ja	.L348
	cmpl	$95, %esi
	leal	162(%rsi), %r8d
	sete	%r9b
	testb	%r9b, %r9b
	jne	.L348
.L146:
	leal	289(%r8), %r9d
	movl	$8836, %esi
	cmpl	$545, %r9d
	je	.L143
	jmp	.L142
.L469:
	movq	%rcx, %r13
	jmp	.L155
.L506:
	movl	%r11d, 120(%rsp)
	subq	$8, %rsp
	movq	%r14, %r8
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%rbx, %rsi
	movq	64(%rsp), %r9
	movq	128(%rsp), %rcx
	movq	72(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movslq	%eax, %r13
	popq	%rdx
	cmpl	$6, %r13d
	popq	%rcx
	movq	160(%rsp), %r10
	movq	168(%rsp), %rax
	movl	120(%rsp), %r11d
	je	.L241
	cmpl	$5, %r13d
	jne	.L225
.L340:
	movl	$5, %r13d
.L226:
	movq	16(%rsp), %rdi
	movq	%r10, (%rdi)
	movq	136(%rsp), %rdi
	movq	%rdi, 8(%rsp)
.L224:
	cmpq	8(%rsp), %rax
	jne	.L194
	cmpq	$5, %r13
	jne	.L193
.L222:
	cmpq	%rax, (%rsp)
	jne	.L188
.L196:
	subl	$1, 20(%rbx)
	jmp	.L188
.L335:
	movl	%r11d, 8(%rsp)
	jmp	.L187
.L477:
	movq	%r10, 120(%rsp)
	movl	%r11d, 112(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%r14, %r8
	movq	112(%rsp), %r9
	movq	120(%rsp), %rcx
	movq	%rbx, %rsi
	movq	72(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r11
	popq	%r13
	movq	144(%rsp), %rdx
	movq	152(%rsp), %r13
	movl	112(%rsp), %r11d
	movq	120(%rsp), %r10
	je	.L170
	cmpl	$5, 8(%rsp)
	jne	.L154
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L173:
	cmpl	$138, %eax
	ja	.L349
	cmpl	$135, %eax
	leal	-67(%r8), %eax
	sete	%r9b
	testb	%r9b, %r9b
	je	.L172
.L349:
	leal	-73(%r8), %eax
	jmp	.L172
.L147:
	cmpl	$552, %r9d
	je	.L294
	leal	-267(%r8), %esi
	cmpl	$3, %esi
	ja	.L148
	leal	-168(%r8), %esi
	imull	$94, %esi, %esi
	jmp	.L143
.L323:
	movl	$7, %r13d
	jmp	.L226
.L201:
	leal	-224(%rdx), %edi
	leal	-129(%rdx), %ecx
	cmpl	$28, %edi
	jbe	.L350
	cmpl	$30, %ecx
	jbe	.L350
	cmpq	$0, 88(%rsp)
	je	.L322
	movl	48(%rsp), %esi
	testl	%esi, %esi
	jne	.L499
.L322:
	movl	$6, %r13d
.L197:
	movq	16(%rsp), %rdi
	movq	%r10, (%rdi)
	jmp	.L224
.L17:
	movq	$0, 0(%rbp)
	testb	$1, 16(%rbx)
	movl	$0, 8(%rsp)
	jne	.L13
	movq	24(%rsp), %rbx
	movl	%eax, (%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	movl	8(%rsp), %eax
	pushq	%rax
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%rdi
	popq	%r8
	jmp	.L13
.L82:
	cmpl	$127, %edx
	ja	.L91
	leaq	1(%r15), %rax
	movq	%rax, 168(%rsp)
	movb	%dl, (%r15)
.L92:
	movq	160(%rsp), %rax
	leaq	4(%rax), %rdx
	cmpq	%r13, %rdx
	movq	%rdx, 160(%rsp)
	je	.L451
.L449:
	movl	0(%rbp), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
.L87:
	subq	%r13, %rdx
	cmpq	%rcx, %rdx
	jle	.L500
	movq	16(%rsp), %rdi
	subq	%rcx, %rdx
	andl	$-8, %eax
	movq	168(%rsp), %r15
	movl	%eax, 40(%rsp)
	addq	(%rdi), %rdx
	movq	%rdx, (%rdi)
	movl	%eax, 0(%rbp)
	movq	%rdx, %r11
	movq	56(%rsp), %rdi
	movl	16(%rbx), %eax
	movq	96(%rdi), %rdx
	movl	%eax, 48(%rsp)
	jmp	.L32
.L18:
	testl	%ecx, %ecx
	jne	.L262
.L24:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	$1
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r9
	popq	%r10
	jmp	.L13
.L348:
	leal	168(%rsi), %r8d
	jmp	.L146
.L494:
	leaq	1(%rax), %rcx
	addl	$64, %edx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	jmp	.L234
.L294:
	movl	$9212, %esi
	jmp	.L143
.L350:
	leaq	1(%r10), %rdi
	cmpq	%rdi, %r14
	jbe	.L314
	movzbl	1(%r10), %r8d
	movl	%r8d, %r9d
	subl	$64, %r9d
	cmpb	$-68, %r9b
	ja	.L219
	cmpl	$127, %r8d
	je	.L219
	leal	-193(%rdx), %r9d
	cmpl	$223, %edx
	cmova	%r9d, %ecx
	cmpl	$127, %r8d
	leal	(%rcx,%rcx), %edx
	leal	-64(%r8), %ecx
	jbe	.L209
	leal	-65(%r8), %ecx
	cmpl	$93, %ecx
	jbe	.L209
	leal	-159(%r8), %ecx
	addl	$1, %edx
.L209:
	cmpl	$93, %edx
	jbe	.L210
	cmpl	$102, %edx
	jbe	.L211
	leal	230(%rdx), %r8d
	leal	519(%rdx), %r9d
	cmpl	$93, %r8d
	jbe	.L501
.L212:
	leal	-258(%r8), %edx
	cmpl	$2, %edx
	ja	.L217
	leal	-163(%r8), %edx
	imull	$94, %edx, %edx
.L213:
	addl	%edx, %ecx
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rdx
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %r8
	movzwl	(%rdx,%rcx,2), %edx
	movzbl	%dh, %ecx
	movzbl	%dl, %edx
	addl	(%r8,%rcx,4), %edx
	je	.L219
	cmpl	$65533, %edx
	je	.L219
	addq	$2, %r10
	cmpl	$127, %edx
	ja	.L198
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rcx
	subl	$1, %edx
	movzwl	2(%rcx,%rdx,4), %edi
	movzwl	(%rcx,%rdx,4), %edx
	movl	%edx, (%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, 8(%rsp)
	jnb	.L502
	sall	$3, %edi
	cmpq	%rsi, 8(%rsp)
	movq	16(%rsp), %rax
	movl	%edi, 0(%rbp)
	movq	%r10, (%rax)
	jne	.L194
	movq	8(%rsp), %rax
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$-31100, %esi
	movl	$1, %r8d
	xorl	%edx, %edx
.L228:
	addl	%edx, %r8d
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L503:
	addl	$1, %edx
	cmpl	%r8d, %edx
	je	.L229
	movl	%edx, %esi
	movzwl	(%r12,%rsi,4), %esi
.L231:
	cmpw	%si, %di
	jne	.L503
	leaq	1(%rax), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L340
	movzwl	2(%r12,%rdx,4), %edx
	movq	%rcx, 168(%rsp)
	movl	%edx, %ecx
	shrw	$8, %cx
	movb	%cl, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	movq	160(%rsp), %rax
	movl	$0, 0(%rbp)
	leaq	4(%rax), %r10
	movq	168(%rsp), %rax
	movq	%r10, 160(%rsp)
	jmp	.L225
.L492:
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$92, (%rax)
	jmp	.L234
.L326:
	movl	$-31104, %esi
	movl	$1, %r8d
	movl	$1, %edx
	jmp	.L228
.L327:
	movl	$-31365, %esi
	movl	$5, %r8d
	movl	$2, %edx
	jmp	.L228
.L286:
	movq	%r14, %rdx
	movl	$4, 8(%rsp)
	jmp	.L155
.L493:
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$126, (%rax)
	jmp	.L234
.L497:
	movq	%r13, %rax
.L262:
	movq	%rax, (%rbx)
	movl	$0, 8(%rsp)
	jmp	.L13
.L328:
	movl	$-31145, %esi
	movl	$4, %r8d
	movl	$7, %edx
	jmp	.L228
.L148:
	leal	-333(%r8), %esi
	cmpl	$16, %esi
	ja	.L149
	leal	-230(%r8), %esi
	imull	$94, %esi, %esi
	jmp	.L143
.L219:
	cmpq	$0, 88(%rsp)
	je	.L322
	movl	48(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L322
	movq	88(%rsp), %rcx
	movq	%rax, %rsi
	movq	%rdi, %r10
	movl	$6, %r13d
	addq	$1, (%rcx)
	jmp	.L223
.L311:
	movq	%rsi, %rax
	movl	$5, %r13d
	jmp	.L197
.L120:
	cmpl	$0, 8(%rsp)
	jne	.L13
.L451:
	movq	56(%rsp), %rax
	movq	96(%rax), %rdx
	movq	16(%rsp), %rax
	movq	(%rax), %r11
	movl	0(%rbp), %eax
	movl	%eax, 40(%rsp)
	movl	16(%rbx), %eax
	movl	%eax, 48(%rsp)
	jmp	.L32
.L91:
	cmpl	$165, %edx
	je	.L504
	cmpl	$8254, %edx
	je	.L505
	leal	-65377(%rdx), %eax
	cmpl	$62, %eax
	ja	.L95
	leaq	1(%r15), %rax
	addl	$64, %edx
	movq	%rax, 168(%rsp)
	movb	%dl, (%r15)
	jmp	.L92
.L486:
	movl	$-32087, %esi
	movl	$14, %r8d
	movl	$11, %edx
	jmp	.L228
.L41:
	leal	-161(%rax), %edx
	cmpl	$62, %edx
	ja	.L43
	addl	$65216, %eax
	jmp	.L456
.L481:
	movq	%r14, %rdx
	movq	16(%rsp), %rax
	subq	%r11, %rdx
	addq	%r12, %rdx
	cmpq	$4, %rdx
	movq	%r14, (%rax)
	ja	.L74
	cmpq	%r12, %rdx
	leaq	1(%r11), %rax
	jbe	.L78
.L77:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%rbp,%r12)
	addq	$1, %r12
	cmpq	%r12, %rdx
	jne	.L77
.L78:
	movl	$7, 8(%rsp)
	jmp	.L13
.L238:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L455
	cmpq	$0, 88(%rsp)
	je	.L332
	testb	$8, 16(%rbx)
	jne	.L506
.L241:
	testb	$2, 40(%rsp)
	jne	.L507
.L332:
	movl	$6, %r13d
	jmp	.L226
.L280:
	movl	$-31100, %ecx
	movl	$1, %esi
	xorl	%edx, %edx
.L83:
	addl	%edx, %esi
	leaq	comp_table_data(%rip), %r9
	jmp	.L86
.L508:
	addl	$1, %edx
	cmpl	%esi, %edx
	je	.L84
	movl	%edx, %ecx
	movzwl	(%r9,%rcx,4), %ecx
.L86:
	cmpw	%cx, %di
	jne	.L508
	leaq	1(%r15), %rcx
	cmpq	%rcx, %r10
	jbe	.L271
	leaq	comp_table_data(%rip), %rsi
	movq	%rcx, 168(%rsp)
	movzwl	2(%rsi,%rdx,4), %esi
	movl	%esi, %edx
	shrw	$8, %dx
	movb	%dl, (%r15)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	movb	%sil, (%rdx)
	xorl	%ecx, %ecx
	movq	160(%rsp), %rdi
	movl	$0, 0(%rbp)
	leaq	4(%rdi), %rdx
	cmpq	%r13, %rdx
	movq	%rdx, 160(%rsp)
	jne	.L87
	jmp	.L451
.L281:
	movl	$-31104, %ecx
	movl	$1, %esi
	movl	$1, %edx
	jmp	.L83
.L210:
	imull	$94, %edx, %edx
	jmp	.L213
.L312:
	movl	$165, %edx
	jmp	.L454
.L499:
	movq	88(%rsp), %rdi
	addq	$1, %r10
	movq	%rax, %rsi
	movl	$6, %r13d
	addq	$1, (%rdi)
	jmp	.L221
.L487:
	movl	%eax, 4(%r13)
	movq	%rsi, %rcx
	jmp	.L151
.L253:
	leaq	1(%rax), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L340
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%r8b, (%rax)
	jmp	.L234
.L496:
	leaq	4(%rax), %r13
	cmpq	%rdx, %r13
	ja	.L271
	movl	%r12d, %edx
	sarl	$3, %edx
	movl	%edx, (%rax)
	movl	$0, 0(%rbp)
	jmp	.L21
.L490:
	movq	(%rsp), %rdi
	cmpq	%rdi, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r10, (%rax)
	je	.L196
.L194:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	1(%r15), %rax
	movq	%rax, 168(%rsp)
	movb	$92, (%r15)
	jmp	.L92
.L43:
	leal	-224(%rax), %edx
	leal	-129(%rax), %r9d
	cmpl	$28, %edx
	jbe	.L341
	cmpl	$30, %r9d
	jbe	.L341
	cmpq	$0, 88(%rsp)
	je	.L99
	testb	$2, 48(%rsp)
	jne	.L509
.L99:
	movl	$6, 8(%rsp)
	jmp	.L13
.L341:
	leaq	168(%rsp), %rdx
	addq	$1, %rcx
	leaq	(%rdx,%rcx), %rdi
	leaq	1(%rdx), %r8
	cmpq	%r8, %rdi
	jbe	.L46
	movzbl	169(%rsp), %edi
	movl	%edi, %ecx
	subl	$64, %ecx
	cmpb	$-68, %cl
	ja	.L62
	cmpl	$127, %edi
	je	.L62
	leal	-193(%rax), %ecx
	cmpl	$223, %eax
	cmova	%ecx, %r9d
	leal	-64(%rdi), %ecx
	addl	%r9d, %r9d
	cmpl	$127, %edi
	jbe	.L53
	leal	-65(%rdi), %ecx
	cmpl	$93, %ecx
	jbe	.L53
	leal	-159(%rdi), %ecx
	addl	$1, %r9d
.L53:
	cmpl	$93, %r9d
	jbe	.L54
	cmpl	$102, %r9d
	jbe	.L55
	leal	230(%r9), %eax
	addl	$519, %r9d
	cmpl	$93, %eax
	jbe	.L510
.L56:
	leal	-258(%rax), %edi
	cmpl	$2, %edi
	ja	.L60
	leal	-163(%rax), %r9d
.L54:
	imull	$94, %r9d, %r9d
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	addl	%r9d, %ecx
	movzwl	(%rax,%rcx,2), %eax
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rcx
	movzbl	%ah, %edi
	movzbl	%al, %eax
	addl	(%rcx,%rdi,4), %eax
	je	.L62
	cmpl	$65533, %eax
	je	.L62
	cmpl	$127, %eax
	ja	.L279
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rcx
	leal	-1(%rax), %edi
	movzwl	2(%rcx,%rdi,4), %eax
	movzwl	(%rcx,%rdi,4), %ecx
	movl	%ecx, (%r15)
	leaq	8(%r15), %rcx
	cmpq	%rcx, %r10
	jnb	.L511
	sall	$3, %eax
	movq	%rsi, %r15
	leaq	2(%rdx), %rcx
	movl	%eax, 0(%rbp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L505:
	leaq	1(%r15), %rax
	movq	%rax, 168(%rsp)
	movb	$126, (%r15)
	jmp	.L92
.L95:
	cmpl	$173759, %edx
	ja	.L96
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rcx
	movl	%edx, %eax
	shrl	$6, %eax
	movswl	(%rcx,%rax,2), %eax
	testl	%eax, %eax
	js	.L96
	movl	%edx, %ecx
	sall	$6, %eax
	andl	$63, %ecx
	addl	%eax, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rcx,2), %eax
	testl	%eax, %eax
	movl	%eax, %ecx
	je	.L96
	shrl	$8, %eax
	movl	%ecx, %edi
	movl	%eax, %edx
	leal	-33(%rax), %eax
	andl	$127, %edi
	leal	-33(%rdi), %esi
	cmpl	$93, %eax
	jbe	.L105
	cmpl	$204, %eax
	jbe	.L106
	leal	-135(%rdx), %eax
.L105:
	addl	$61, %edi
	testb	$1, %al
	cmovne	%edi, %esi
	shrl	%eax
	leal	129(%rax), %edi
	leal	193(%rax), %edx
	cmpl	$30, %eax
	movl	%edi, %eax
	cmova	%edx, %eax
	xorl	%edx, %edx
	cmpl	$62, %esi
	seta	%dl
	testb	$-128, %cl
	leal	64(%rsi,%rdx), %edx
	je	.L114
	testw	%cx, %cx
	js	.L512
	sall	$8, %eax
	xorl	%ecx, %ecx
	orl	%edx, %eax
	leaq	4(%r13), %rdx
	sall	$3, %eax
	movl	%eax, 0(%rbp)
	movq	%rdx, 160(%rsp)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%eax, (%r15)
	movl	0(%rbp), %eax
	movl	%eax, 40(%rsp)
	movl	16(%rbx), %eax
	movl	%eax, 48(%rsp)
	jmp	.L32
.L211:
	cmpl	$98, %edx
	ja	.L352
	cmpl	$95, %edx
	leal	162(%rdx), %r8d
	sete	%r9b
	testb	%r9b, %r9b
	jne	.L352
.L216:
	leal	289(%r8), %r9d
	movl	$8836, %edx
	cmpl	$545, %r9d
	je	.L213
	jmp	.L212
.L491:
	movq	%rsi, %rax
	jmp	.L197
.L283:
	movl	$-31145, %ecx
	movl	$4, %esi
	movl	$7, %edx
	jmp	.L83
.L282:
	movl	$-31365, %ecx
	movl	$5, %esi
	movl	$2, %edx
	jmp	.L83
.L507:
	movq	88(%rsp), %rdi
	addq	$4, %r10
	movl	$6, %r13d
	movq	%r10, 160(%rsp)
	addq	$1, (%rdi)
	jmp	.L225
.L244:
	cmpl	$138, %edx
	ja	.L353
	cmpl	$135, %edx
	leal	-67(%rdi), %edx
	sete	%r9b
	testb	%r9b, %r9b
	je	.L243
.L353:
	leal	-73(%rdi), %edx
	jmp	.L243
.L96:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L513
	cmpq	$0, 88(%rsp)
	je	.L99
	testb	$8, 48(%rsp)
	jne	.L514
	testb	$2, 48(%rsp)
	je	.L99
.L104:
	movq	88(%rsp), %rax
	addq	$4, 160(%rsp)
	addq	$1, (%rax)
.L103:
	movq	160(%rsp), %rdx
	cmpq	%r13, %rdx
	jne	.L449
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	2(%rdx), %rax
	cmpq	%rax, %rdi
	je	.L515
	movslq	%r12d, %r12
	movq	%rcx, %rax
	movq	16(%rsp), %rbx
	subq	%r12, %rax
	addq	%r11, %rax
	movq	%rax, (%rbx)
	movl	40(%rsp), %eax
	andl	$-8, %eax
	movslq	%eax, %rsi
	cmpq	%rsi, %rcx
	jle	.L516
	cmpq	$4, %rcx
	ja	.L517
	orl	%ecx, %eax
	testq	%rcx, %rcx
	movl	%eax, 0(%rbp)
	je	.L78
	xorl	%eax, %eax
	jmp	.L70
.L518:
	movzbl	(%rdx,%rax), %r13d
.L70:
	movb	%r13b, 4(%rbp,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L518
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$-32087, %ecx
	movl	$14, %esi
	movl	$11, %edx
	jmp	.L83
.L352:
	leal	168(%rdx), %r8d
	jmp	.L216
.L314:
	movl	$7, %r13d
	jmp	.L197
.L62:
	cmpq	$0, 88(%rsp)
	je	.L99
	testb	$2, 48(%rsp)
	je	.L99
	movq	88(%rsp), %rax
	leaq	1(%rdx), %rcx
	addq	$1, (%rax)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L489:
	movq	(%rsp), %r15
	cmpq	%r15, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r14, (%rax)
	jne	.L194
.L193:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	cmpl	$552, %r9d
	je	.L317
	leal	-267(%r8), %edx
	cmpl	$3, %edx
	ja	.L218
	leal	-168(%r8), %edx
	imull	$94, %edx, %edx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	168(%rsp), %rdx
	movl	$8254, %eax
	leaq	1(%rdx), %rcx
	jmp	.L42
.L275:
	leaq	168(%rsp), %rdx
	movl	$165, %eax
	leaq	1(%rdx), %rcx
	jmp	.L42
.L501:
	imull	$94, %r8d, %edx
	jmp	.L213
.L71:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L60:
	cmpl	$552, %r9d
	je	.L278
	leal	-267(%rax), %edi
	cmpl	$3, %edi
	ja	.L61
	leal	-168(%rax), %r9d
	jmp	.L54
.L510:
	movl	%eax, %r9d
	jmp	.L54
.L55:
	cmpl	$98, %r9d
	ja	.L343
	cmpl	$95, %r9d
	leal	162(%r9), %eax
	sete	%dil
	testb	%dil, %dil
	jne	.L343
.L59:
	leal	289(%rax), %r9d
	cmpl	$545, %r9d
	jne	.L56
	movl	$94, %r9d
	jmp	.L54
.L61:
	leal	-333(%rax), %edi
	cmpl	$16, %edi
	ja	.L62
	leal	-230(%rax), %r9d
	jmp	.L54
.L278:
	movl	$98, %r9d
	jmp	.L54
.L343:
	leal	168(%r9), %eax
	jmp	.L59
.L509:
	movq	88(%rsp), %rax
	leaq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	addq	$1, (%rax)
	jmp	.L50
.L74:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L254:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L478:
	imull	$94, %r8d, %esi
	jmp	.L143
.L36:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L106:
	cmpl	$138, %eax
	ja	.L345
	cmpl	$135, %eax
	leal	-67(%rdx), %eax
	sete	%r8b
	testb	%r8b, %r8b
	je	.L105
.L345:
	leal	-73(%rdx), %eax
	jmp	.L105
.L514:
	leaq	0(%r13,%r12), %rax
	movq	%r10, 40(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	movq	%r11, %rdx
	movq	%rbx, %rsi
	movq	%rax, 8(%rsp)
	pushq	96(%rsp)
	movq	%rax, %r8
	movq	72(%rsp), %rdi
	leaq	184(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	movq	40(%rsp), %r10
	je	.L519
	movq	160(%rsp), %rdx
	cmpq	%r13, %rdx
	jne	.L449
	cmpl	$7, 8(%rsp)
	jne	.L120
	leaq	4(%r13), %rax
	cmpq	%rax, (%rsp)
	je	.L520
	movl	0(%rbp), %eax
	movq	%r12, %rbx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r12
	jle	.L521
	cmpq	$4, %r12
	ja	.L522
	orl	%r12d, %eax
	testq	%r12, %r12
	movl	%eax, 0(%rbp)
	je	.L78
	xorl	%eax, %eax
.L124:
	movzbl	0(%r13,%rax), %edx
	movb	%dl, 4(%rbp,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L124
	jmp	.L78
.L513:
	leaq	4(%r13), %rdx
	movslq	%esi, %rcx
	movl	%esi, %eax
	movq	%rdx, 160(%rsp)
	jmp	.L87
.L522:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L521:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L520:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L519:
	testb	$2, 48(%rsp)
	je	.L103
	jmp	.L104
.L479:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L488:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L259:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L500:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L495:
	leaq	__PRETTY_FUNCTION__.9223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L517:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L516:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L515:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L498:
	movl	%r12d, 0(%rbp)
	jmp	.L26
.L502:
	movl	%edi, 4(%rax)
	movq	%rdx, %rsi
	jmp	.L221
.L512:
	leaq	__PRETTY_FUNCTION__.9140(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L114:
	leaq	1(%r15), %rcx
	cmpq	%rcx, %r10
	jbe	.L271
	movq	%rcx, 168(%rsp)
	movb	%al, (%r15)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	jmp	.L92
.L511:
	movl	%eax, 4(%r15)
	movq	%rcx, %r15
	leaq	2(%rdx), %rcx
	jmp	.L50
.L279:
	leaq	2(%rdx), %rcx
	jmp	.L42
.L218:
	leal	-333(%r8), %edx
	cmpl	$16, %edx
	ja	.L219
	leal	-230(%r8), %edx
	imull	$94, %edx, %edx
	jmp	.L213
.L317:
	movl	$9212, %edx
	jmp	.L213
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9121, @object
	.size	__PRETTY_FUNCTION__.9121, 18
__PRETTY_FUNCTION__.9121:
	.string	"to_shift_jisx0213"
	.align 16
	.type	__PRETTY_FUNCTION__.9140, @object
	.size	__PRETTY_FUNCTION__.9140, 25
__PRETTY_FUNCTION__.9140:
	.string	"to_shift_jisx0213_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9038, @object
	.size	__PRETTY_FUNCTION__.9038, 27
__PRETTY_FUNCTION__.9038:
	.string	"from_shift_jisx0213_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9223, @object
	.size	__PRETTY_FUNCTION__.9223, 6
__PRETTY_FUNCTION__.9223:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 100
comp_table_data:
	.value	-31100
	.value	-31099
	.value	-31104
	.value	-31098
	.value	-31365
	.value	-31133
	.value	-31145
	.value	-31129
	.value	-31146
	.value	-31127
	.value	-31153
	.value	-31125
	.value	-31134
	.value	-31123
	.value	-31145
	.value	-31128
	.value	-31146
	.value	-31126
	.value	-31153
	.value	-31124
	.value	-31134
	.value	-31122
	.value	-32087
	.value	-32011
	.value	-32085
	.value	-32010
	.value	-32083
	.value	-32009
	.value	-32081
	.value	-32008
	.value	-32079
	.value	-32007
	.value	-31926
	.value	-31849
	.value	-31924
	.value	-31848
	.value	-31922
	.value	-31847
	.value	-31920
	.value	-31846
	.value	-31918
	.value	-31845
	.value	-31910
	.value	-31844
	.value	-31901
	.value	-31843
	.value	-31897
	.value	-31842
	.value	-31757
	.value	-31754
