	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-KR//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$17179869185, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$17179869188, %rdx
	movabsq	$17179869185, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.section	.rodata.str1.1
.LC5:
	.string	"set == KSC5601_set"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC10:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC11:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC13:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rdi, 64(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	movl	%r11d, %edx
	movq	%r9, 56(%rsp)
	movl	224(%rsp), %r13d
	andl	$1, %edx
	movq	%rdi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L8
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L8
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L8:
	testl	%r13d, %r13d
	jne	.L430
	movq	32(%rsp), %rax
	leaq	128(%rsp), %rdx
	movq	8(%r12), %r13
	testq	%rax, %rax
	cmove	%r12, %rax
	cmpq	$0, 56(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	movq	%rax, 80(%rsp)
	movq	32(%r12), %rax
	movq	%rax, (%rsp)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L29
	movl	24(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L29
	movl	20(%r12), %edi
	testl	%edi, %edi
	jne	.L29
	leaq	4(%r15), %rax
	cmpq	%rax, %r13
	jb	.L241
	movl	$1126769691, (%r15)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L29:
	movl	232(%rsp), %esi
	testl	%esi, %esi
	jne	.L31
.L420:
	movq	8(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r14
.L32:
	leaq	152(%rsp), %rax
	movq	%rax, 88(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 16(%rsp)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L431
	movq	%r14, %rax
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	cmpq	%rax, %rbp
	movq	%r15, %rbx
	movl	16(%rsp), %edi
	movl	$4, %r10d
	je	.L129
.L158:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L260
	cmpq	%rbx, %r13
	jbe	.L261
	movl	(%rax), %ecx
	cmpl	$127, %ecx
	ja	.L130
	testl	%edi, %edi
	leaq	1(%rbx), %rax
	je	.L131
	movq	%rax, 152(%rsp)
	movb	$15, (%rbx)
	movq	152(%rsp), %rbx
	cmpq	%rbx, %r13
	je	.L432
	leaq	1(%rbx), %rax
.L131:
	movq	%rax, 152(%rsp)
	xorl	%edi, %edi
	movb	%cl, (%rbx)
.L133:
	movq	144(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
.L157:
	cmpq	%rax, %rbp
	movq	152(%rsp), %rbx
	jne	.L158
	.p2align 4,,10
	.p2align 3
.L129:
	movq	8(%rsp), %rcx
	cmpq	$0, 32(%rsp)
	movq	%rax, (%rcx)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	jne	.L433
.L159:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L434
	cmpq	%rbx, %r15
	jnb	.L263
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r10d, 48(%rsp)
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%r8
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r9
	movl	48(%rsp), %r10d
	je	.L163
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L435
.L162:
	testl	%r11d, %r11d
	jne	.L278
.L220:
	movq	8(%rsp), %rax
	movq	(%r12), %r15
	movl	16(%r12), %r11d
	movq	(%rax), %r14
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L130:
	leal	-44032(%rcx), %eax
	cmpl	$11171, %eax
	jbe	.L436
	leal	-19968(%rcx), %eax
	xorl	%esi, %esi
	cmpl	$20991, %eax
	jbe	.L292
	leal	-63744(%rcx), %eax
	cmpl	$267, %eax
	jbe	.L292
	movl	$988, %edx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L437:
	leal	-1(%rax), %edx
	cmpl	%edx, %esi
	jg	.L140
.L141:
	leal	(%rsi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r9,%r8,4), %ebx
	cmpl	%ebx, %ecx
	jb	.L437
	jbe	.L149
	leal	1(%rax), %esi
	cmpl	%edx, %esi
	jle	.L141
	.p2align 4,,10
	.p2align 3
.L140:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L133
	cmpq	$0, 80(%rsp)
	je	.L151
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	testb	$8, 16(%r12)
	jne	.L438
.L152:
	testb	$2, %r11b
	movq	144(%rsp), %rax
	jne	.L154
.L422:
	movq	152(%rsp), %rbx
	movl	$6, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L436:
	xorl	%r9d, %r9d
	movl	$2349, %r8d
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rbx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L439:
	leal	-1(%rsi), %r8d
.L136:
	cmpl	%r9d, %r8d
	jl	.L140
.L139:
	leal	(%r9,%r8), %edx
	movl	%edx, %esi
	sarl	%esi
	movslq	%esi, %rax
	movzwl	(%rbx,%rax,2), %eax
	cmpl	%eax, %ecx
	jb	.L439
	jbe	.L137
	leal	1(%rsi), %r9d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$4887, %edx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L440:
	leal	-1(%rax), %edx
.L144:
	cmpl	%edx, %esi
	jg	.L140
.L146:
	leal	(%rsi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r9,%r8,4), %ebx
	cmpl	%ebx, %ecx
	jb	.L440
	jbe	.L149
	leal	1(%rax), %esi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L431:
	cmpq	%r14, %rbp
	je	.L249
	leaq	4(%r15), %rsi
	cmpq	%rsi, %r13
	jb	.L250
	movl	16(%rsp), %edi
	movq	%r14, %rdx
	movq	%r15, %rbx
	movl	$4, %r10d
	andl	$2, %r11d
.L112:
	movzbl	(%rdx), %ecx
	cmpl	$127, %ecx
	movl	%ecx, %eax
	movl	%ecx, %r8d
	ja	.L122
	cmpb	$27, %cl
	je	.L441
	cmpb	$14, %cl
	je	.L442
	cmpb	$15, %cl
	je	.L443
.L116:
	testl	%edi, %edi
	jne	.L119
	addq	$1, %rdx
.L120:
	movl	%r8d, (%rbx)
	movq	%rsi, %rbx
.L114:
	cmpq	%rdx, %rbp
	je	.L111
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r13
	jnb	.L112
	movl	$5, %r10d
.L111:
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	.p2align 4,,10
	.p2align 3
.L446:
	cmpq	$0, 32(%rsp)
	je	.L159
.L433:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L7:
	addq	$168, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	cmpl	$8, %edi
	jne	.L180
	movq	%rbp, %r8
	subq	%rdx, %r8
	cmpq	$1, %r8
	jbe	.L257
	leal	-33(%rax), %r8d
	cmpb	$92, %r8b
	ja	.L122
	cmpb	$73, %al
	jne	.L444
.L122:
	cmpq	$0, 80(%rsp)
	je	.L259
	testl	%r11d, %r11d
	jne	.L445
.L259:
	movq	8(%rsp), %rax
	movl	$6, %r10d
	movq	%rdx, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L163:
	cmpl	$5, %r10d
	movl	%r10d, %r11d
	jne	.L162
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$7, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L261:
	movl	$5, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L149:
	movzbl	2(%r9,%r8,4), %eax
	movb	%al, 136(%rsp)
	movzbl	3(%r9,%r8,4), %eax
	movb	%al, 137(%rsp)
.L138:
	cmpl	$8, %edi
	movq	152(%rsp), %rbx
	je	.L155
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	$14, (%rbx)
	movq	152(%rsp), %rbx
.L155:
	leaq	2(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L447
	leaq	1(%rbx), %rax
	movl	$8, %edi
	movq	%rax, 152(%rsp)
	movzbl	136(%rsp), %eax
	movb	%al, (%rbx)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	137(%rsp), %edx
	movb	%dl, (%rax)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L257:
	movq	8(%rsp), %rax
	movl	$7, %r10d
	movq	%rdx, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L31:
	movq	32(%r12), %rbx
	movl	(%rbx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	je	.L420
	cmpq	$0, 32(%rsp)
	jne	.L448
	movq	8(%rsp), %rdi
	movl	16(%r12), %r11d
	movq	(%rdi), %rdx
	movq	(%rsp), %rdi
	movl	%r11d, 88(%rsp)
	movl	(%rdi), %edi
	movl	%edi, 48(%rsp)
	movq	40(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L449
	cmpl	$4, %ecx
	movq	%rdx, 144(%rsp)
	movq	%r15, 152(%rsp)
	ja	.L58
	leaq	136(%rsp), %rsi
	movslq	%ecx, %r14
	xorl	%eax, %eax
	movq	%rsi, 16(%rsp)
.L59:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%r14, %rax
	jne	.L59
	movq	%rdx, %rax
	subq	%r14, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L450
	cmpq	%r15, %r13
	jbe	.L241
	leaq	1(%rdx), %rax
	leaq	135(%rsp), %rdi
.L67:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r14
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %r14
	movb	%cl, (%rdi,%r14)
	ja	.L290
	cmpq	%rsi, %rbp
	ja	.L67
.L290:
	movl	136(%rsp), %r10d
	movq	16(%rsp), %rax
	cmpl	$127, %r10d
	movq	%rax, 144(%rsp)
	ja	.L69
	movl	48(%rsp), %ecx
	leaq	1(%r15), %rdx
	testl	%ecx, %ecx
	je	.L247
	movq	%rdx, 152(%rsp)
	movb	$15, (%r15)
	movq	152(%rsp), %rax
	cmpq	%rax, %r13
	je	.L71
	leaq	1(%rax), %rdx
.L70:
	movq	%rdx, 152(%rsp)
	movb	%r10b, (%rax)
.L72:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	16(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L420
.L89:
	movl	(%rbx), %edx
	subq	16(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L451
	movq	8(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r15
	movl	16(%r12), %r11d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r14
	movl	%edx, (%rbx)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$-1370734243, %eax
	mull	%edx
	movl	$-1370734243, %eax
	shrl	$7, %edx
	addl	$48, %edx
	movb	%dl, 136(%rsp)
	mull	%esi
	shrl	$6, %edx
	imull	$94, %edx, %edx
	subl	%edx, %esi
	addl	$33, %esi
	movb	%sil, 137(%rsp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	2(%rdx), %r9
	cmpq	%r9, %rbp
	jb	.L257
	cmpb	$36, 1(%rdx)
	jne	.L116
	leaq	3(%rdx), %r9
	cmpq	%r9, %rbp
	jb	.L257
	cmpb	$41, 2(%rdx)
	jne	.L116
	leaq	4(%rdx), %r9
	cmpq	%r9, %rbp
	jb	.L257
	cmpb	$67, 3(%rdx)
	jne	.L116
	movq	%r9, %rdx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L434:
	movq	56(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rdi)
.L161:
	cmpl	$7, %r10d
	jne	.L7
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L7
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L222
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L224
.L223:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L223
.L224:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L447:
	movq	144(%rsp), %rax
	movl	$8, %edi
	movl	$5, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L442:
	addq	$1, %rdx
	movl	$8, %edi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L263:
	movl	%r10d, %r11d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L443:
	addq	$1, %rdx
	xorl	%edi, %edi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L445:
	movq	80(%rsp), %rax
	addq	$1, %rdx
	movl	$6, %r10d
	addq	$1, (%rax)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L444:
	movzbl	1(%rdx), %eax
	leal	-33(%rax), %r8d
	cmpb	$93, %r8b
	ja	.L122
	subl	$33, %ecx
	imull	$94, %ecx, %ecx
	leal	-33(%rcx,%rax), %eax
	leal	-1410(%rax), %ecx
	cmpl	$2349, %ecx
	ja	.L123
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rax
	movslq	%ecx, %rcx
	movzwl	(%rax,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L122
.L124:
	addq	$2, %rdx
	cmpl	$65533, %r8d
	jne	.L120
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L154:
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 144(%rsp)
	addq	$1, (%rcx)
	jmp	.L157
.L449:
	cmpl	$4, %ecx
	ja	.L36
	movslq	%ecx, %r14
	leaq	152(%rsp), %rcx
	xorl	%esi, %esi
	movq	%r14, %r10
.L37:
	movzbl	4(%rbx,%rsi), %edi
	movb	%dil, (%rcx,%rsi)
	addq	$1, %rsi
	cmpq	%r14, %rsi
	jne	.L37
	leaq	4(%r15), %rdi
	cmpq	%rdi, %r13
	movq	%rdi, 16(%rsp)
	jb	.L241
	leaq	151(%rsp), %r9
	movq	%rdx, %rsi
.L38:
	addq	$1, %rsi
	movzbl	-1(%rsi), %edi
	addq	$1, %r10
	cmpq	$3, %r10
	setbe	%r8b
	cmpq	%rsi, %rbp
	movb	%dil, (%r9,%r10)
	seta	%dil
	testb	%dil, %r8b
	jne	.L38
	movzbl	152(%rsp), %r9d
	leaq	(%rcx,%r10), %rdi
	movq	%rdi, 88(%rsp)
	cmpl	$127, %r9d
	movl	%r9d, %edi
	movl	%r9d, %r8d
	ja	.L452
	cmpb	$27, %r9b
	je	.L453
	leal	-14(%r9), %esi
	cmpb	$1, %sil
	jbe	.L454
.L44:
	movl	48(%rsp), %esi
	testl	%esi, %esi
	je	.L245
	cmpl	$8, %esi
	jne	.L455
	cmpq	$1, %r10
	jbe	.L43
	leal	-33(%rdi), %esi
	cmpb	$92, %sil
	ja	.L48
	cmpb	$73, %dil
	jne	.L456
.L48:
	cmpq	$0, 80(%rsp)
	je	.L101
	andl	$2, %r11d
	je	.L101
	movq	%rcx, %rsi
.L237:
	movq	80(%rsp), %rdi
	addq	$1, %rsi
	addq	$1, (%rdi)
.L41:
	subq	%rcx, %rsi
	cmpq	%r14, %rsi
	jle	.L457
	subq	%r14, %rsi
	movq	8(%rsp), %rdi
	andl	$-8, %eax
	leaq	(%rdx,%rsi), %r14
	movl	16(%r12), %r11d
	movq	%r14, (%rdi)
	movl	%eax, (%rbx)
	jmp	.L32
.L71:
	movq	144(%rsp), %rax
.L419:
	cmpq	16(%rsp), %rax
	jne	.L89
	.p2align 4,,10
	.p2align 3
.L241:
	movl	$5, %r10d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L432:
	movq	144(%rsp), %rax
	movq	%r13, %rbx
	xorl	%edi, %edi
	movl	$5, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L435:
	movq	8(%rsp), %rax
	movl	16(%rsp), %edi
	movq	%r14, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L458
	movl	16(%r12), %eax
	cmpq	%r14, %rbp
	movq	%r14, 144(%rsp)
	movq	%r15, 152(%rsp)
	movl	$4, %ebx
	movl	16(%rsp), %ecx
	movl	%eax, 48(%rsp)
	movq	%r15, %rax
	je	.L459
.L217:
	leaq	4(%r14), %rdx
	cmpq	%rdx, %rbp
	jb	.L274
	cmpq	%rax, %r10
	jbe	.L275
	movl	(%r14), %edx
	cmpl	$127, %edx
	ja	.L189
	testl	%ecx, %ecx
	leaq	1(%rax), %rsi
	je	.L190
	movq	%rsi, 152(%rsp)
	movb	$15, (%rax)
	movq	152(%rsp), %rax
	cmpq	%rax, %r10
	je	.L460
	leaq	1(%rax), %rsi
.L190:
	movq	%rsi, 152(%rsp)
	xorl	%ecx, %ecx
	movb	%dl, (%rax)
.L192:
	movq	144(%rsp), %rax
	leaq	4(%rax), %r14
	movq	%r14, 144(%rsp)
.L216:
	cmpq	%r14, %rbp
	movq	152(%rsp), %rax
	jne	.L217
.L459:
	movl	%ecx, 16(%rsp)
	movq	%rbp, %r14
	movslq	%ebx, %rcx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	leal	-44032(%rdx), %eax
	cmpl	$11171, %eax
	jbe	.L461
	leal	-19968(%rdx), %eax
	xorl	%edi, %edi
	cmpl	$20991, %eax
	jbe	.L293
	leal	-63744(%rdx), %eax
	cmpl	$267, %eax
	jbe	.L293
	movl	$988, %esi
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L462:
	leal	-1(%rax), %esi
.L207:
	cmpl	%esi, %edi
	jg	.L199
.L200:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r9,%r8,4), %r14d
	cmpl	%r14d, %edx
	jb	.L462
	jbe	.L208
	leal	1(%rax), %edi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L199:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L192
	cmpq	$0, 80(%rsp)
	je	.L210
	movq	(%rsp), %rax
	movl	%ecx, (%rax)
	testb	$8, 16(%r12)
	jne	.L463
.L211:
	testb	$2, 48(%rsp)
	movq	144(%rsp), %r14
	jne	.L213
	movq	152(%rsp), %rax
	movl	%ecx, 16(%rsp)
	movl	$6, %ecx
	.p2align 4,,10
	.p2align 3
.L188:
	movq	8(%rsp), %rdi
	movl	16(%rsp), %ebx
	movq	136(%rsp), %r10
	movq	%r14, (%rdi)
	movq	(%rsp), %rdi
	movl	%ebx, (%rdi)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L250:
	movq	8(%rsp), %rax
	movq	%r14, %rdx
	movl	16(%rsp), %edi
	movq	%r15, %rbx
	movl	$5, %r10d
	movq	%rdx, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	jmp	.L446
.L430:
	cmpq	$0, 32(%rsp)
	jne	.L464
	cmpl	$1, %r13d
	movq	32(%r12), %rbx
	jne	.L11
	movq	40(%rsp), %rdi
	movq	(%r12), %rax
	cmpq	$0, 96(%rdi)
	je	.L12
	movl	24(%r12), %ecx
	movq	8(%r12), %rsi
	testl	%ecx, %ecx
	jne	.L13
	movl	20(%r12), %r15d
	testl	%r15d, %r15d
	jne	.L13
	leaq	4(%rax), %r13
	cmpq	%r13, %rsi
	jb	.L241
	movl	$1126769691, (%rax)
	movq	32(%r12), %rdi
	movq	%r13, %rdx
	movl	(%rbx), %ebp
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L416
	movq	40(%rsp), %r11
	cmpq	$0, 96(%r11)
	je	.L465
.L18:
	cmpq	%rdx, %rsi
	je	.L241
	movb	$15, (%rdx)
	leaq	1(%rdx), %r13
	movq	32(%r12), %rdx
	movl	$0, (%rdx)
.L416:
	movl	16(%r12), %r11d
	andl	$1, %r11d
.L17:
	testl	%r11d, %r11d
	jne	.L236
	cmpq	%r13, %rax
	jnb	.L22
	movq	24(%rsp), %r15
	movq	%rax, 152(%rsp)
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%r12
	popq	%r14
	je	.L22
	cmpq	%r13, 152(%rsp)
	jne	.L466
.L24:
	testl	%r10d, %r10d
	jne	.L7
.L22:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	$1
.L424:
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	80(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	popq	%r9
	movl	%eax, %r10d
	popq	%r11
	jmp	.L7
.L438:
	movl	%r11d, 48(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	104(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	movq	16(%rsp), %rax
	cmpl	$6, %r10d
	popq	%r11
	popq	%rbx
	movl	(%rax), %edi
	movl	48(%rsp), %r11d
	je	.L152
	cmpl	$5, %r10d
	movq	144(%rsp), %rax
	jne	.L157
	movq	152(%rsp), %rbx
	jmp	.L129
.L461:
	xorl	%r8d, %r8d
	movl	$2349, %edi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r14
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L467:
	leal	-1(%rsi), %edi
.L195:
	cmpl	%edi, %r8d
	jg	.L199
.L198:
	leal	(%r8,%rdi), %eax
	movl	%eax, %esi
	sarl	%esi
	movslq	%esi, %r9
	movzwl	(%r14,%r9,2), %r9d
	cmpl	%r9d, %edx
	jb	.L467
	jbe	.L196
	leal	1(%rsi), %r8d
	jmp	.L195
.L293:
	movl	$4887, %esi
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L468:
	leal	-1(%rax), %esi
.L203:
	cmpl	%esi, %edi
	jg	.L199
.L205:
	leal	(%rdi,%rsi), %eax
	sarl	%eax
	movslq	%eax, %r8
	movzwl	(%r9,%r8,4), %r14d
	cmpl	%r14d, %edx
	jb	.L468
	jbe	.L208
	leal	1(%rax), %edi
	jmp	.L203
.L458:
	cmpq	%r14, %rbp
	movl	16(%r12), %ebx
	je	.L469
	leaq	4(%r15), %rdi
	andl	$2, %ebx
	movq	%r15, %rax
	movl	$4, %ecx
	cmpq	%rdi, %r10
	jb	.L470
	movl	16(%rsp), %r9d
	movl	%ebx, 16(%rsp)
.L169:
	movzbl	(%r14), %esi
	cmpl	$127, %esi
	movl	%esi, %edx
	movl	%esi, %r8d
	ja	.L181
	cmpb	$27, %sil
	je	.L471
	cmpb	$14, %sil
	je	.L472
	cmpb	$15, %sil
	je	.L473
.L175:
	testl	%r9d, %r9d
	jne	.L178
	addq	$1, %r14
.L179:
	movl	%r8d, (%rax)
	movq	%rdi, %rax
.L173:
	cmpq	%r14, %rbp
	je	.L474
	leaq	4(%rax), %rdi
	cmpq	%rdi, %r10
	jnb	.L169
	movl	%r9d, 16(%rsp)
	movl	$5, %ecx
	jmp	.L171
.L178:
	cmpl	$8, %r9d
	jne	.L180
	movq	%rbp, %r8
	subq	%r14, %r8
	cmpq	$1, %r8
	jbe	.L271
	leal	-33(%rdx), %r8d
	cmpb	$92, %r8b
	ja	.L181
	cmpb	$73, %dl
	jne	.L475
.L181:
	cmpq	$0, 80(%rsp)
	je	.L273
	movl	16(%rsp), %edi
	testl	%edi, %edi
	jne	.L476
.L273:
	movl	%r9d, 16(%rsp)
	movl	$6, %ecx
.L171:
	movq	8(%rsp), %rdi
	movl	16(%rsp), %ebx
	movq	%r14, (%rdi)
	movq	(%rsp), %rdi
	movl	%ebx, (%rdi)
.L186:
	cmpq	%r10, %rax
	jne	.L168
	cmpq	$5, %rcx
	jne	.L167
	cmpq	%rax, %r15
	jne	.L162
.L170:
	subl	$1, 20(%r12)
	jmp	.L162
.L278:
	movl	%r11d, %r10d
	jmp	.L161
.L11:
	movq	$0, (%rbx)
	xorl	%r10d, %r10d
	testb	$1, 16(%r12)
	jne	.L7
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	%r13
	jmp	.L424
.L69:
	leal	-44032(%r10), %eax
	cmpl	$11171, %eax
	ja	.L73
	xorl	%r8d, %r8d
	movl	$2349, %esi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rdi
	jmp	.L78
.L477:
	leal	-1(%rcx), %esi
.L75:
	cmpl	%r8d, %esi
	jl	.L79
.L78:
	leal	(%r8,%rsi), %eax
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %r9
	movzwl	(%rdi,%r9,2), %r9d
	cmpl	%r9d, %r10d
	jb	.L477
	jbe	.L76
	leal	1(%rcx), %r8d
	jmp	.L75
.L271:
	movl	%r9d, 16(%rsp)
	movl	$7, %ecx
	jmp	.L171
.L12:
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L478
.L233:
	testl	%edx, %edx
	je	.L22
	movq	%rax, %r13
.L236:
	movq	%r13, (%r12)
	xorl	%r10d, %r10d
	jmp	.L7
.L151:
	movq	144(%rsp), %rax
	jmp	.L422
.L274:
	movl	%ecx, 16(%rsp)
	movl	$7, %ecx
	jmp	.L188
.L275:
	movl	%ecx, 16(%rsp)
	movl	$5, %ecx
	jmp	.L188
.L208:
	movzbl	2(%r9,%r8,4), %eax
	movb	%al, 126(%rsp)
	movzbl	3(%r9,%r8,4), %eax
	movb	%al, 127(%rsp)
.L197:
	cmpl	$8, %ecx
	movq	152(%rsp), %rax
	je	.L214
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$14, (%rax)
	movq	152(%rsp), %rax
.L214:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r10
	jb	.L479
	leaq	1(%rax), %rdx
	movl	$8, %ecx
	movq	%rdx, 152(%rsp)
	movzbl	126(%rsp), %edx
	movb	%dl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	127(%rsp), %edx
	movb	%dl, (%rax)
	jmp	.L192
.L13:
	movl	(%rbx), %ebp
	testl	%ebp, %ebp
	je	.L233
	movq	%rax, %rdx
	jmp	.L18
.L249:
	movq	%r15, %rbx
	movl	16(%rsp), %edi
	movq	%rbp, %rdx
	movl	$4, %r10d
	jmp	.L111
.L79:
	shrl	$7, %r10d
	cmpl	$7168, %r10d
	je	.L480
	cmpq	$0, 80(%rsp)
	je	.L91
	movq	(%rsp), %rax
	movl	48(%rsp), %edi
	movl	%edi, (%rax)
	testb	$8, 16(%r12)
	jne	.L481
.L92:
	testb	$2, 88(%rsp)
	movq	144(%rsp), %rax
	jne	.L482
	cmpq	16(%rsp), %rax
	jne	.L89
.L101:
	movl	$6, %r10d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L196:
	movl	$-1370734243, %edi
	mull	%edi
	movl	%edx, %eax
	shrl	$7, %eax
	addl	$48, %eax
	movb	%al, 126(%rsp)
	movl	%edi, %eax
	mull	%esi
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %esi
	movl	%esi, %eax
	addl	$33, %eax
	movb	%al, 127(%rsp)
	jmp	.L197
.L123:
	cmpl	$3853, %eax
	jle	.L125
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rcx
	subl	$3854, %eax
	cltq
	movzwl	(%rcx,%rax,2), %r8d
	testw	%r8w, %r8w
	je	.L122
	jmp	.L124
.L471:
	leaq	2(%r14), %rbx
	cmpq	%rbx, %rbp
	jb	.L271
	cmpb	$36, 1(%r14)
	jne	.L175
	leaq	3(%r14), %rbx
	cmpq	%rbx, %rbp
	jb	.L271
	cmpb	$41, 2(%r14)
	jne	.L175
	leaq	4(%r14), %rbx
	cmpq	%rbx, %rbp
	jb	.L271
	cmpb	$67, 3(%r14)
	jne	.L175
	movq	%rbx, %r14
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L472:
	addq	$1, %r14
	movl	$8, %r9d
	jmp	.L173
.L479:
	movq	144(%rsp), %r14
	movl	$5, %ecx
	movl	$8, 16(%rsp)
	jmp	.L188
.L247:
	movq	%r15, %rax
	jmp	.L70
.L73:
	leal	-19968(%r10), %eax
	xorl	%esi, %esi
	cmpl	$20991, %eax
	jbe	.L291
	leal	-63744(%r10), %eax
	cmpl	$267, %eax
	jbe	.L291
	movl	$988, %ecx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L80
.L483:
	leal	-1(%rax), %ecx
.L87:
	cmpl	%esi, %ecx
	jl	.L79
.L80:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r9,%rdi,4), %r8d
	cmpl	%r8d, %r10d
	jb	.L483
	jbe	.L88
	leal	1(%rax), %esi
	jmp	.L87
.L473:
	addq	$1, %r14
	xorl	%r9d, %r9d
	jmp	.L173
.L450:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%rdx, %rax
	addq	%r14, %rax
	cmpq	$4, %rax
	ja	.L61
	addq	$1, %rdx
	cmpq	%r14, %rax
	jbe	.L65
.L64:
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %rdx
	movb	%cl, 4(%rbx,%r14)
	addq	$1, %r14
	cmpq	%r14, %rax
	jne	.L64
.L65:
	movl	$7, %r10d
	jmp	.L7
.L245:
	leaq	1(%rcx), %rsi
.L46:
	movl	%r8d, (%r15)
	movl	(%rbx), %eax
	movq	16(%rsp), %r15
	movl	%eax, %r14d
	andl	$7, %r14d
	jmp	.L41
.L125:
	cmpl	$1114, %eax
	jg	.L122
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %r8d
	testw	%r8w, %r8w
	jne	.L124
	jmp	.L122
.L476:
	movq	80(%rsp), %rdi
	addq	$1, %r14
	movl	$6, %ecx
	addq	$1, (%rdi)
	jmp	.L173
.L460:
	movq	144(%rsp), %r14
	movl	$0, 16(%rsp)
	movl	$5, %ecx
	jmp	.L188
.L475:
	movzbl	1(%r14), %edx
	leal	-33(%rdx), %r8d
	cmpb	$93, %r8b
	ja	.L181
	subl	$33, %esi
	imull	$94, %esi, %esi
	leal	-33(%rsi,%rdx), %edx
	leal	-1410(%rdx), %esi
	cmpl	$2349, %esi
	ja	.L182
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rdx
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %r8d
	testw	%r8w, %r8w
	je	.L181
.L183:
	addq	$2, %r14
	cmpl	$65533, %r8d
	jne	.L179
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L291:
	movl	$4887, %ecx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L85
.L484:
	leal	-1(%rax), %ecx
.L83:
	cmpl	%esi, %ecx
	jl	.L79
.L85:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r9,%rdi,4), %r8d
	cmpl	%r8d, %r10d
	jb	.L484
	jbe	.L88
	leal	1(%rax), %esi
	jmp	.L83
.L478:
	movl	%ecx, %ebp
	movq	%rax, %r13
	movq	%rbx, %rdi
.L232:
	andl	$7, %ecx
	andl	$1, %r11d
	movl	%ecx, (%rdi)
	jmp	.L17
.L470:
	movq	8(%rsp), %rax
	movl	16(%rsp), %edi
	cmpq	%r15, %r10
	movq	%r14, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	je	.L170
.L168:
	leaq	__PRETTY_FUNCTION__.9267(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	1(%rcx), %rsi
	jmp	.L41
.L76:
	movl	$-1370734243, %esi
	mull	%esi
	movl	%edx, %eax
	shrl	$7, %eax
	addl	$48, %eax
	movb	%al, 126(%rsp)
	movl	%ecx, %eax
	mull	%esi
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$33, %eax
	movb	%al, 127(%rsp)
.L77:
	cmpl	$8, 48(%rsp)
	movq	152(%rsp), %rax
	je	.L97
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$14, (%rax)
	movq	152(%rsp), %rax
.L97:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r13
	jb	.L71
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	126(%rsp), %edx
	movb	%dl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	127(%rsp), %edx
	movb	%dl, (%rax)
	jmp	.L72
.L463:
	movq	%r10, 104(%rsp)
	movl	%r11d, 16(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %rcx
	movq	104(%rsp), %r9
	movq	%rbp, %r8
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %ebx
	movq	16(%rsp), %rax
	cmpl	$6, %ebx
	popq	%rdx
	popq	%rsi
	movl	(%rax), %ecx
	movl	16(%rsp), %r11d
	movq	104(%rsp), %r10
	je	.L211
	cmpl	$5, %ebx
	movq	144(%rsp), %r14
	jne	.L216
	movl	%ecx, 16(%rsp)
	movq	152(%rsp), %rax
	movl	$5, %ecx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	2(%r9,%rdi,4), %eax
	movb	%al, 126(%rsp)
	movzbl	3(%r9,%rdi,4), %eax
	movb	%al, 127(%rsp)
	jmp	.L77
.L474:
	movl	%r9d, 16(%rsp)
	movq	%rbp, %r14
	jmp	.L171
.L213:
	movq	80(%rsp), %rax
	addq	$4, %r14
	movl	$6, %ebx
	movq	%r14, 144(%rsp)
	addq	$1, (%rax)
	jmp	.L216
.L452:
	cmpq	$0, 80(%rsp)
	movl	$6, %r10d
	je	.L7
	andl	$2, %r11d
	je	.L7
	movq	80(%rsp), %rdi
	leaq	1(%rcx), %rsi
	addq	$1, (%rdi)
	jmp	.L41
.L453:
	leaq	2(%rcx), %rsi
	cmpq	%rsi, 88(%rsp)
	jb	.L43
	cmpb	$36, 153(%rsp)
	jne	.L44
	leaq	3(%rcx), %rsi
	cmpq	%rsi, 88(%rsp)
	jb	.L43
	movzbl	154(%rsp), %esi
	movb	%sil, 96(%rsp)
	leaq	4(%rcx), %rsi
	cmpq	%rsi, 88(%rsp)
	jnb	.L289
	cmpb	$41, 96(%rsp)
	je	.L43
.L289:
	cmpb	$41, 96(%rsp)
	jne	.L44
	cmpb	$67, 155(%rsp)
	leaq	4(%rcx), %rsi
	jne	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L456:
	movzbl	153(%rsp), %edi
	leal	-33(%rdi), %esi
	cmpb	$93, %sil
	ja	.L48
	leal	-33(%r9), %esi
	imull	$94, %esi, %esi
	leal	-33(%rsi,%rdi), %edi
	leal	-1410(%rdi), %esi
	cmpl	$2349, %esi
	ja	.L49
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %r8d
	testw	%r8w, %r8w
	je	.L48
.L50:
	cmpl	$65533, %r8d
	je	.L225
	leaq	2(%rcx), %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L469:
	movq	8(%rsp), %rax
	cmpq	%r15, %r10
	movq	%rbp, (%rax)
	movq	(%rsp), %rax
	movl	%edi, (%rax)
	jne	.L168
.L167:
	leaq	__PRETTY_FUNCTION__.9267(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	4(%rcx), %rsi
	cmpq	%rsi, 88(%rsp)
	je	.L485
	movq	%r10, %rsi
	andl	$-8, %eax
	subq	%r14, %rsi
	movq	%rsi, %r14
	movq	8(%rsp), %rsi
	addq	%rdx, %r14
	movslq	%eax, %rdx
	cmpq	%rdx, %r10
	movq	%r14, (%rsi)
	jle	.L486
	cmpq	$4, %r10
	ja	.L487
	orl	%r10d, %eax
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	testq	%r10, %r10
	je	.L65
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	je	.L65
.L488:
	movzbl	(%rcx,%rax), %edi
	movb	%dil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L488
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L182:
	cmpl	$3853, %edx
	jle	.L184
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rsi
	subl	$3854, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %r8d
	testw	%r8w, %r8w
	je	.L181
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	__PRETTY_FUNCTION__.9085(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L91:
	movq	144(%rsp), %rax
	cmpq	16(%rsp), %rax
	jne	.L89
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L480:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	16(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L89
	movq	%rdx, %r14
	jmp	.L32
.L36:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L451:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L222:
	leaq	__PRETTY_FUNCTION__.9267(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L465:
	movl	16(%r12), %r11d
	jmp	.L232
.L448:
	leaq	__PRETTY_FUNCTION__.9267(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L184:
	cmpl	$1114, %edx
	jg	.L181
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %r8d
	testw	%r8w, %r8w
	jne	.L183
	jmp	.L181
.L61:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L457:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L482:
	movq	80(%rsp), %rdi
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	cmpq	16(%rsp), %rax
	jne	.L89
	jmp	.L101
.L481:
	movq	16(%rsp), %rax
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	leaq	(%rax,%r14), %r11
	movq	24(%rsp), %rax
	leaq	168(%rsp), %r9
	movq	%r11, %r8
	movq	%r11, 64(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	cmpl	$6, %r10d
	popq	%rax
	popq	%rdx
	je	.L92
	cmpl	$5, %r10d
	movq	144(%rsp), %rax
	movq	48(%rsp), %r11
	je	.L419
	cmpq	16(%rsp), %rax
	jne	.L89
	cmpl	$7, %r10d
	je	.L489
	testl	%r10d, %r10d
	je	.L420
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L489:
	movq	16(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, %r11
	je	.L490
	movl	(%rbx), %eax
	movq	%r14, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r14
	jle	.L491
	cmpq	$4, %r14
	ja	.L492
	orl	%r14d, %eax
	testq	%r14, %r14
	movl	%eax, (%rbx)
	je	.L65
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
.L109:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L109
	jmp	.L65
.L225:
	cmpq	$0, 80(%rsp)
	je	.L493
	andb	$2, %r11b
	leaq	2(%rcx), %rsi
	je	.L41
	jmp	.L237
.L49:
	cmpl	$3853, %edi
	jle	.L51
	leal	-3854(%rdi), %esi
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %r8d
	testw	%r8w, %r8w
	je	.L48
	jmp	.L50
.L491:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L492:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L493:
	leaq	2(%rcx), %rsi
	jmp	.L41
.L490:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L51:
	cmpl	$1114, %edi
	jg	.L48
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%edi, %rdi
	movzwl	(%rsi,%rdi,2), %r8d
	testw	%r8w, %r8w
	je	.L48
	jmp	.L50
.L58:
	leaq	__PRETTY_FUNCTION__.9191(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L466:
	movl	%ebp, (%rbx)
	jmp	.L24
.L464:
	leaq	__PRETTY_FUNCTION__.9267(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L455:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L210:
	movl	%ecx, 16(%rsp)
	movq	144(%rsp), %r14
	movl	$6, %ecx
	movq	152(%rsp), %rax
	jmp	.L188
.L487:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L486:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L485:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9085, @object
	.size	__PRETTY_FUNCTION__.9085, 20
__PRETTY_FUNCTION__.9085:
	.string	"from_iso2022kr_loop"
	.align 16
	.type	__PRETTY_FUNCTION__.9191, @object
	.size	__PRETTY_FUNCTION__.9191, 25
__PRETTY_FUNCTION__.9191:
	.string	"to_iso2022kr_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9105, @object
	.size	__PRETTY_FUNCTION__.9105, 27
__PRETTY_FUNCTION__.9105:
	.string	"from_iso2022kr_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9267, @object
	.size	__PRETTY_FUNCTION__.9267, 6
__PRETTY_FUNCTION__.9267:
	.string	"gconv"
