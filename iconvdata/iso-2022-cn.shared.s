	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-CN//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$17179869185, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$14, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$17179869188, %rdx
	movabsq	$25769803777, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"#$"
.LC2:
	.string	"#~"
.LC3:
	.string	"!j"
.LC4:
	.string	"!i"
.LC5:
	.string	"!a"
.LC6:
	.string	"!b"
.LC7:
	.string	"!n"
.LC8:
	.string	"!o"
.LC9:
	.string	"!q"
.LC10:
	.string	"!r"
.LC11:
	.string	"!p"
.LC12:
	.string	"!s"
.LC13:
	.string	"!t"
.LC14:
	.string	"!w"
.LC15:
	.string	"!x"
.LC16:
	.string	"!u"
.LC17:
	.string	"!v"
.LC18:
	.string	"!P"
.LC19:
	.string	"!%"
.LC20:
	.string	"!&"
.LC21:
	.string	"(8"
.LC22:
	.string	"(7"
.LC23:
	.string	"(6"
.LC24:
	.string	"(5"
.LC25:
	.string	"(3"
.LC26:
	.string	"(/"
.LC27:
	.string	"(+"
.LC28:
	.string	"(#"
.LC29:
	.string	"(1"
.LC30:
	.string	"(-"
.LC31:
	.string	"()"
.LC32:
	.string	"('"
.LC33:
	.string	"(%"
.LC34:
	.string	"\"d"
.LC35:
	.string	"\"g"
.LC36:
	.string	"\"f"
.LC37:
	.string	"\"!"
.LC38:
	.string	"\"J"
.LC39:
	.string	"\"G"
.LC40:
	.string	"\"k"
.LC41:
	.string	"\"\""
.LC42:
	.string	"\"j"
.LC43:
	.string	"../iconv/skeleton.c"
.LC44:
	.string	"outbufstart == NULL"
.LC45:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.section	.rodata.str1.1
.LC47:
	.string	"set == CNS11643_1_set"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC49:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC50:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC51:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC52:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC53:
	.string	"gb2312.h"
.LC54:
	.string	"cp[1] != '\\0'"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"(used >> 3) >= 1 && (used >> 3) <= 3"
	.section	.rodata.str1.1
.LC56:
	.string	")A)G*H"
.LC57:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC59:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	addq	$48, %rsi
	subq	$200, %rsp
	movl	16(%rbp), %ebx
	movq	%rdx, 8(%rsp)
	movq	%rdi, 56(%rsp)
	addq	$104, %rdi
	movq	%r8, 48(%rsp)
	movq	%r9, 64(%rsp)
	movl	%ebx, %edx
	movl	256(%rsp), %r12d
	movq	%rdi, 80(%rsp)
	andl	$1, %edx
	movq	%rsi, 88(%rsp)
	movl	%ebx, %r14d
	movq	$0, 72(%rsp)
	jne	.L8
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 72(%rsp)
	je	.L8
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 72(%rsp)
.L8:
	testl	%r12d, %r12d
	jne	.L1332
	movq	48(%rsp), %rsi
	movq	8(%rsp), %rax
	leaq	128(%rsp), %rcx
	movq	32(%rbp), %r11
	movl	264(%rsp), %r8d
	movq	8(%rbp), %r10
	testq	%rsi, %rsi
	movq	%rsi, %rdx
	movq	(%rax), %rax
	cmove	%rbp, %rdx
	cmpq	$0, 64(%rsp)
	movl	(%r11), %edi
	movq	(%rdx), %r13
	movl	$0, %edx
	movq	$0, 128(%rsp)
	movq	%rax, %r12
	movl	%edi, 24(%rsp)
	cmovne	%rcx, %rdx
	testl	%r8d, %r8d
	movq	%rdx, 96(%rsp)
	jne	.L1333
.L22:
	movq	%r13, (%rsp)
	movq	%r11, 40(%rsp)
	movq	%r10, %r13
	.p2align 4,,10
	.p2align 3
.L309:
	movl	24(%rsp), %eax
	movl	%eax, %edi
	andl	$224, %eax
	movl	%eax, 36(%rsp)
	movq	56(%rsp), %rax
	andl	$24, %edi
	movl	%edi, 16(%rsp)
	cmpq	$0, 96(%rax)
	je	.L1334
	movq	(%rsp), %rax
	movq	%r12, 160(%rsp)
	leaq	.LC36(%rip), %r10
	movl	36(%rsp), %esi
	movl	16(%rsp), %edx
	movl	$4, 32(%rsp)
	movq	%rax, 168(%rsp)
	movq	%rax, %rbx
	movq	%r12, %rax
	cmpq	%rax, %r15
	je	.L1335
.L581:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r15
	jb	.L1336
	cmpq	%rbx, %r13
	jbe	.L1319
	movl	(%rax), %eax
	cmpl	$127, %eax
	ja	.L343
	testl	%edx, %edx
	leaq	1(%rbx), %rcx
	je	.L344
	movq	%rcx, 168(%rsp)
	movb	$15, (%rbx)
	movq	168(%rsp), %rbx
	cmpq	%rbx, %r13
	je	.L1337
	leaq	1(%rbx), %rcx
.L344:
	xorl	%edx, %edx
	cmpl	$10, %eax
	movq	%rcx, 168(%rsp)
	cmove	%edx, %esi
	movb	%al, (%rbx)
.L346:
	movq	160(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 160(%rsp)
.L580:
	cmpq	%rax, %r15
	movq	168(%rsp), %rbx
	jne	.L581
.L1335:
	orl	%edx, %esi
	movq	%r15, %rax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L343:
	cmpl	$8, %edx
	je	.L348
	testb	$64, %sil
	je	.L348
	cmpl	$9249, %eax
	ja	.L416
	cmpl	$9216, %eax
	jnb	.L417
	cmpl	$8457, %eax
	je	.L418
	jbe	.L1338
	cmpl	$8601, %eax
	ja	.L428
	cmpl	$8592, %eax
	jnb	.L429
	cmpl	$8544, %eax
	jb	.L458
	cmpl	$8553, %eax
	ja	.L1339
	subl	$53, %eax
	movl	$36, %ecx
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L348:
	cmpl	$9371, %eax
	ja	.L351
	cmpl	$9312, %eax
	jnb	.L352
	cmpl	$472, %eax
	je	.L504
	ja	.L354
	cmpl	$333, %eax
	je	.L506
	jbe	.L1340
	cmpl	$464, %eax
	je	.L513
	jbe	.L1341
	cmpl	$468, %eax
	je	.L517
	cmpl	$470, %eax
	je	.L518
	cmpl	$466, %eax
	jne	.L414
.L519:
	leaq	.LC26(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	160(%rsp), %rax
.L1319:
	orl	%edx, %esi
	movl	$5, 32(%rsp)
.L341:
	movq	8(%rsp), %rdi
	cmpq	$0, 48(%rsp)
	movq	%rax, (%rdi)
	movq	40(%rsp), %rax
	movl	%esi, (%rax)
	jne	.L1342
.L582:
	addl	$1, 20(%rbp)
	testb	$1, 16(%rbp)
	jne	.L1343
	cmpq	%rbx, (%rsp)
	jnb	.L948
	movq	72(%rsp), %r14
	movq	0(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r14
	cmpl	$4, %eax
	movl	%eax, %r14d
	popq	%rsi
	popq	%rdi
	je	.L586
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L1344
.L585:
	testl	%r14d, %r14d
	jne	.L977
.L855:
	movq	8(%rsp), %rax
	movl	16(%rbp), %r14d
	movq	(%rax), %r12
	movq	40(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	movq	0(%rbp), %rax
	movq	%rax, (%rsp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L351:
	cmpl	$9792, %eax
	je	.L532
	ja	.L382
	cmpl	$9670, %eax
	je	.L534
	jbe	.L1345
	cmpl	$9678, %eax
	je	.L542
	jbe	.L1346
	cmpl	$9733, %eax
	je	.L546
	cmpl	$9734, %eax
	je	.L547
	cmpl	$9679, %eax
	jne	.L414
.L548:
	leaq	.LC9(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L1334:
	cmpq	%r12, %r15
	je	.L923
	movq	(%rsp), %rbx
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r13
	jb	.L924
	movl	%r14d, %r9d
	movl	36(%rsp), %r8d
	movl	%edi, %esi
	andl	$2, %r9d
	movq	%r12, %rdx
	movl	$4, 32(%rsp)
	movl	$32, %r14d
	movl	%r9d, %r11d
.L317:
	movzbl	(%rdx), %eax
	cmpl	$126, %eax
	movl	%eax, %edi
	ja	.L334
	cmpb	$27, %al
	je	.L1347
	cmpb	$14, %al
	je	.L1348
	cmpb	$15, %al
	je	.L1349
.L324:
	testl	%esi, %esi
	jne	.L332
	addq	$1, %rdx
.L331:
	movl	%edi, (%rbx)
	movq	%rcx, %rbx
.L319:
	cmpq	%rdx, %r15
	je	.L316
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r13
	jnb	.L317
	movl	$5, 32(%rsp)
.L316:
	movq	8(%rsp), %rax
	orl	%r8d, %esi
	cmpq	$0, 48(%rsp)
	movq	%rdx, (%rax)
	movq	40(%rsp), %rax
	movl	%esi, (%rax)
	je	.L582
.L1342:
	movq	48(%rsp), %rax
	movq	%rbx, (%rax)
.L7:
	movl	32(%rsp), %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	cmpl	$8, %esi
	je	.L1350
	cmpl	$16, %esi
	jne	.L609
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1351
.L334:
	cmpq	$0, 96(%rsp)
	je	.L940
	testl	%r11d, %r11d
	jne	.L1352
.L940:
	movl	$6, 32(%rsp)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L586:
	movl	32(%rsp), %r14d
	cmpl	$5, %r14d
	jne	.L585
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L1336:
	orl	%edx, %esi
	movl	$7, 32(%rsp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L416:
	cmpl	$13269, %eax
	ja	.L436
	cmpl	$13198, %eax
	jnb	.L437
	cmpl	$12329, %eax
	ja	.L438
	cmpl	$12288, %eax
	jnb	.L439
	cmpl	$9312, %eax
	jb	.L458
	cmpl	$9341, %eax
	ja	.L1353
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	.p2align 4,,10
	.p2align 3
.L453:
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	jne	.L1354
.L458:
	leal	-19975(%rax), %ecx
	cmpl	$20893, %ecx
	jbe	.L1355
.L887:
	cmpl	$9371, %eax
	ja	.L502
	cmpl	$9312, %eax
	jnb	.L503
	cmpl	$472, %eax
	je	.L504
	ja	.L505
	cmpl	$333, %eax
	je	.L506
	jbe	.L1356
	cmpl	$464, %eax
	je	.L513
	jbe	.L1357
	cmpl	$468, %eax
	je	.L517
	cmpl	$470, %eax
	je	.L518
	cmpl	$466, %eax
	jne	.L500
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L354:
	cmpl	$1105, %eax
	ja	.L369
	cmpl	$1025, %eax
	jnb	.L370
	cmpl	$711, %eax
	je	.L522
	ja	.L372
	cmpl	$474, %eax
	je	.L524
	cmpl	$476, %eax
	leaq	.LC21(%rip), %rcx
	je	.L508
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L382:
	cmpl	$40864, %eax
	jbe	.L1358
	cmpl	$65504, %eax
	je	.L555
	jbe	.L1359
	cmpl	$65507, %eax
	je	.L558
	cmpl	$65509, %eax
	je	.L559
	cmpl	$65505, %eax
	jne	.L414
.L560:
	leaq	.LC3(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L1350:
	subl	$33, %eax
	cmpl	$86, %eax
	ja	.L334
	movq	%r15, %rdi
	subq	%rdx, %rdi
	cmpq	$1, %rdi
	jbe	.L938
	movzbl	1(%rdx), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L334
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$8177, %eax
	jg	.L334
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rdi
	cltq
	movzwl	(%rdi,%rax,2), %edi
	testw	%di, %di
	je	.L334
.L1322:
	addq	$2, %rdx
	cmpl	$65533, %edi
	jne	.L331
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L1333:
	movl	%edi, %edx
	andl	$7, %edx
	je	.L22
	testq	%rsi, %rsi
	jne	.L1360
	movl	24(%rsp), %edi
	andl	$24, %edi
	movl	%edi, 32(%rsp)
	movq	56(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L1361
	cmpl	$4, %edx
	movq	%rax, 144(%rsp)
	movq	%r13, 152(%rsp)
	ja	.L54
	movslq	%edx, %rsi
	leaq	124(%rsp), %r14
	xorl	%edx, %edx
	movq	%rsi, (%rsp)
.L55:
	movzbl	4(%r11,%rdx), %ecx
	movb	%cl, (%r14,%rdx)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L55
	movq	%rax, %rdx
	subq	(%rsp), %rdx
	addq	$4, %rdx
	cmpq	%rdx, %r15
	jb	.L1362
	cmpq	%r10, %r13
	jnb	.L907
	movq	(%rsp), %rcx
	leaq	1(%rax), %rdx
	leaq	123(%rsp), %rdi
.L63:
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %esi
	addq	$1, %rcx
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	$3, %rcx
	movb	%sil, (%rdi,%rcx)
	ja	.L987
	cmpq	%r8, %r15
	ja	.L63
.L987:
	movl	124(%rsp), %edx
	movq	%rcx, (%rsp)
	movq	%r14, 144(%rsp)
	cmpl	$127, %edx
	jbe	.L1363
	cmpl	$8, 32(%rsp)
	je	.L69
	testb	$64, 24(%rsp)
	je	.L69
	cmpl	$9249, %edx
	ja	.L138
	cmpl	$9216, %edx
	jnb	.L139
	cmpl	$8457, %edx
	je	.L182
	ja	.L141
	cmpl	$969, %edx
	jbe	.L1364
	cmpl	$8451, %edx
	je	.L921
	ja	.L147
	leal	-8211(%rdx), %ecx
	cmpl	$43, %ecx
	ja	.L178
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rcx
.L175:
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	jne	.L865
.L178:
	leal	-19975(%rdx), %ecx
	cmpl	$20893, %ecx
	ja	.L880
	leal	-13312(%rdx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rcx
	cmpb	$2, (%rcx)
	je	.L1365
.L880:
	cmpl	$9371, %edx
	ja	.L222
	cmpl	$9312, %edx
	jnb	.L223
	cmpl	$472, %edx
	je	.L224
	ja	.L225
	cmpl	$333, %edx
	je	.L226
	jbe	.L1366
	cmpl	$464, %edx
	je	.L233
	jbe	.L1367
	cmpl	$468, %edx
	je	.L237
	cmpl	$470, %edx
	jne	.L1368
	leaq	.LC24(%rip), %rcx
.L228:
	movzbl	(%rcx), %esi
.L866:
	movzbl	1(%rcx), %eax
	testb	%al, %al
	je	.L284
	movb	%sil, 116(%rsp)
	movb	%al, 117(%rsp)
	movl	$8, %esi
.L135:
	cmpl	32(%rsp), %esi
	movq	152(%rsp), %rax
	je	.L294
	movl	24(%rsp), %edi
	movl	%esi, %ecx
	movl	$16, %edx
	sarl	$3, %ecx
	sall	%cl, %edx
	andl	$224, %edi
	testl	%edx, %edi
	jne	.L295
	leaq	4(%rax), %rdx
	cmpq	%rdx, %r10
	jb	.L67
	subl	$1, %ecx
	cmpl	$2, %ecx
	ja	.L1369
	leal	(%rcx,%rcx), %edx
	leaq	.LC56(%rip), %rcx
	movslq	%edx, %rdx
	addq	%rcx, %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	$27, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	$36, (%rax)
	movq	152(%rsp), %rax
	movzbl	(%rdx), %ecx
	movzbl	1(%rdx), %edx
	leaq	1(%rax), %rdi
	movq	%rdi, 152(%rsp)
	movb	%cl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rax)
	movq	152(%rsp), %rax
.L295:
	cmpl	$24, %esi
	je	.L1370
	movl	32(%rsp), %ebx
	testl	%ebx, %ebx
	jne	.L294
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r10
	jb	.L67
	movq	%rdx, 152(%rsp)
	movb	$14, (%rax)
	movq	152(%rsp), %rax
.L294:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r10
	jb	.L67
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	116(%rsp), %edx
	movb	%dl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	117(%rsp), %edx
	movb	%dl, (%rax)
.L68:
	movq	144(%rsp), %rax
	leaq	4(%rax), %rdx
	cmpq	%r14, %rdx
	movq	%rdx, 144(%rsp)
	je	.L1316
.L286:
	movl	(%r11), %eax
	subq	%r14, %rdx
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rdx
	jle	.L1371
	movq	8(%rsp), %rdi
	subq	%rcx, %rdx
	andl	$-8, %eax
	movq	152(%rsp), %r13
	movl	%eax, 24(%rsp)
	movl	16(%rbp), %r14d
	addq	(%rdi), %rdx
	movq	%rdx, (%rdi)
	movq	%rdx, %r12
	movl	%eax, (%r11)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L1345:
	cmpl	$9632, %eax
	je	.L536
	jbe	.L1372
	cmpl	$9650, %eax
	je	.L539
	cmpl	$9651, %eax
	je	.L540
	cmpl	$9633, %eax
	jne	.L414
.L541:
	leaq	.LC16(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L1338:
	cmpl	$969, %eax
	ja	.L420
	cmpl	$913, %eax
	jnb	.L421
	cmpl	$167, %eax
	jb	.L458
	cmpl	$247, %eax
	ja	.L1373
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L436:
	cmpl	$65373, %eax
	ja	.L446
	cmpl	$65281, %eax
	jnb	.L447
	cmpl	$19968, %eax
	jb	.L458
	cmpl	$40860, %eax
	ja	.L1374
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L1340:
	cmpl	$275, %eax
	je	.L946
	jbe	.L1375
	cmpl	$283, %eax
	je	.L511
	cmpl	$299, %eax
	leaq	.LC31(%rip), %rcx
	jne	.L414
	.p2align 4,,10
	.p2align 3
.L508:
	movzbl	(%rcx), %edi
	movzbl	1(%rcx), %eax
.L870:
	testb	%al, %al
	je	.L284
.L888:
	movb	%dil, 120(%rsp)
	movb	%al, 121(%rsp)
	movl	$8, %edi
.L413:
	cmpl	%edx, %edi
	movq	168(%rsp), %rbx
	je	.L457
	movl	%edi, %ecx
	movl	$16, %eax
	sarl	$3, %ecx
	sall	%cl, %eax
	testl	%esi, %eax
	jne	.L571
	leaq	4(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L1318
	subl	$1, %ecx
	cmpl	$2, %ecx
	ja	.L844
	addl	%ecx, %ecx
	movslq	%ecx, %rax
	leaq	.LC56(%rip), %rcx
	addq	%rcx, %rax
	leaq	1(%rbx), %rcx
	cmpl	$8, %edi
	movq	%rcx, 168(%rsp)
	movb	$27, (%rbx)
	movq	168(%rsp), %rcx
	leaq	1(%rcx), %r8
	movq	%r8, 168(%rsp)
	movb	$36, (%rcx)
	movq	168(%rsp), %rcx
	movzbl	(%rax), %r8d
	leaq	1(%rcx), %r9
	movq	%r9, 168(%rsp)
	movb	%r8b, (%rcx)
	movzbl	1(%rax), %ecx
	movq	168(%rsp), %rax
	leaq	1(%rax), %r8
	movq	%r8, 168(%rsp)
	movb	%cl, (%rax)
	je	.L1376
	cmpl	$16, %edi
	movq	168(%rsp), %rbx
	je	.L1377
	orb	$-128, %sil
.L571:
	cmpl	$24, %edi
	jne	.L456
	leaq	2(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L1318
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	movb	$27, (%rbx)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	$78, (%rax)
	movq	168(%rsp), %rbx
.L577:
	leaq	2(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L1318
.L579:
	leaq	1(%rbx), %rax
	movq	%rax, 168(%rsp)
	movzbl	120(%rsp), %eax
	movb	%al, (%rbx)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movzbl	121(%rsp), %edx
	movb	%dl, (%rax)
	movl	%edi, %edx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L1358:
	cmpl	$19968, %eax
	jnb	.L399
	cmpl	$12585, %eax
	ja	.L400
	cmpl	$12288, %eax
	jnb	.L401
	cmpl	$9794, %eax
	leaq	.LC5(%rip), %rcx
	je	.L508
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L1347:
	leaq	2(%rdx), %r9
	cmpq	%r9, %r15
	movq	%r9, %r10
	jb	.L938
	movzbl	1(%rdx), %r9d
	cmpb	$36, %r9b
	je	.L1378
	cmpb	$78, %r9b
	jne	.L324
	leaq	4(%rdx), %r9
	cmpq	%r9, %r15
	jb	.L938
	movzbl	2(%rdx), %eax
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1379
.L330:
	cmpq	$0, 96(%rsp)
	leaq	-2(%r10), %rdx
	je	.L940
	testl	%r11d, %r11d
	je	.L940
	movq	96(%rsp), %rax
	movq	%r10, %rdx
	movl	$6, 32(%rsp)
	addq	$1, (%rax)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L1343:
	movq	64(%rsp), %rsi
	movq	%rbx, 0(%rbp)
	movq	128(%rsp), %rax
	addq	%rax, (%rsi)
.L584:
	movl	264(%rsp), %eax
	testl	%eax, %eax
	je	.L7
	cmpl	$7, 32(%rsp)
	jne	.L7
	movq	8(%rsp), %rax
	movq	%r15, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L857
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbp), %rsi
	je	.L859
.L858:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L858
.L859:
	movq	8(%rsp), %rax
	movq	%r15, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L1348:
	xorl	%esi, %esi
	addq	$1, %rdx
	cmpl	$64, %r8d
	sete	%sil
	leal	8(,%rsi,8), %esi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L948:
	movl	32(%rsp), %r14d
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L1349:
	addq	$1, %rdx
	xorl	%esi, %esi
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L1351:
	movq	%r15, %rdi
	subq	%rdx, %rdi
	cmpq	$1, %rdi
	jbe	.L938
	movzbl	1(%rdx), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L334
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$8690, %eax
	jg	.L334
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rdi
	cltq
	movzwl	(%rdi,%rax,2), %edi
	testw	%di, %di
	je	.L334
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L938:
	movl	$7, 32(%rsp)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	8(%rsp), %rax
	movl	24(%rsp), %edi
	movq	%r12, (%rax)
	movq	40(%rsp), %rax
	movl	%edi, (%rax)
	movq	56(%rsp), %rax
	movl	16(%rbp), %r11d
	cmpq	$0, 96(%rax)
	je	.L1380
	movq	(%rsp), %rax
	cmpq	%r12, %r15
	movq	%r12, 176(%rsp)
	leaq	.LC36(%rip), %rbx
	movl	36(%rsp), %edi
	movl	16(%rsp), %esi
	movq	%rax, 184(%rsp)
	movq	%rax, %rdx
	movl	$4, %eax
	je	.L1381
.L852:
	leaq	4(%r12), %rcx
	cmpq	%rcx, %r15
	jb	.L967
	cmpq	%rdx, %r10
	jbe	.L968
	movl	(%r12), %ecx
	cmpl	$127, %ecx
	ja	.L614
	testl	%esi, %esi
	leaq	1(%rdx), %r8
	je	.L615
	movq	%r8, 184(%rsp)
	movb	$15, (%rdx)
	movq	184(%rsp), %rdx
	cmpq	%rdx, %r10
	je	.L1382
	leaq	1(%rdx), %r8
.L615:
	xorl	%esi, %esi
	cmpl	$10, %ecx
	movq	%r8, 184(%rsp)
	cmove	%esi, %edi
	movb	%cl, (%rdx)
.L617:
	movq	176(%rsp), %rdx
	leaq	4(%rdx), %r12
	movq	%r12, 176(%rsp)
.L851:
	cmpq	%r12, %r15
	movq	184(%rsp), %rdx
	jne	.L852
.L1381:
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movslq	%eax, %rcx
	movq	%r15, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	160(%rsp), %rax
	movq	%r13, %rbx
	movl	$5, 32(%rsp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	96(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 32(%rsp)
	addq	$1, (%rax)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L369:
	cmpl	$8869, %eax
	ja	.L377
	cmpl	$8451, %eax
	jnb	.L378
	leal	-8213(%rax), %ecx
	cmpl	$38, %ecx
	ja	.L414
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	.p2align 4,,10
	.p2align 3
.L410:
	movzbl	(%rcx), %edi
	testb	%dil, %dil
	jne	.L1330
.L414:
	leal	-19975(%rax), %ecx
	cmpl	$20893, %ecx
	ja	.L885
	leal	-13312(%rax), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rcx
	cmpb	$2, (%rcx)
	je	.L1383
.L885:
	cmpl	$9249, %eax
	ja	.L460
	cmpl	$9216, %eax
	jnb	.L461
	cmpl	$8457, %eax
	je	.L462
	ja	.L463
	cmpl	$969, %eax
	ja	.L464
	cmpl	$913, %eax
	jnb	.L465
	cmpl	$167, %eax
	jb	.L500
	cmpl	$247, %eax
	ja	.L1384
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	.p2align 4,,10
	.p2align 3
.L497:
	movzbl	(%rdi), %ecx
	testb	%cl, %cl
	jne	.L1385
.L500:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L346
	orl	%edx, %esi
	cmpq	$0, 96(%rsp)
	je	.L566
	movq	40(%rsp), %rax
	movl	%esi, (%rax)
	testb	$8, 16(%rbp)
	jne	.L567
	movl	%esi, %edx
	andl	$224, %esi
	andl	$24, %edx
.L568:
	testb	$2, %r14b
	movq	160(%rsp), %rax
	jne	.L570
	movq	168(%rsp), %rbx
	orl	%edx, %esi
	movl	$6, 32(%rsp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L438:
	cmpl	$12585, %eax
	ja	.L442
	cmpl	$12549, %eax
	jnb	.L443
	cmpl	$12539, %eax
	leaq	.LC20(%rip), %rdi
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L424:
	movzbl	(%rdi), %ecx
	movzbl	1(%rdi), %eax
.L868:
	cmpl	$16, %edx
	movb	%cl, 120(%rsp)
	movb	%al, 121(%rsp)
	movq	168(%rsp), %rbx
	movl	$16, %edi
	je	.L457
.L456:
	testl	%edx, %edx
	jne	.L577
	leaq	1(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L1386
	movq	%rax, 168(%rsp)
	movb	$14, (%rbx)
	movq	168(%rsp), %rbx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L428:
	cmpl	$8869, %eax
	je	.L432
	ja	.L433
	leal	-8725(%rax), %ecx
	cmpl	$82, %ecx
	ja	.L458
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L420:
	cmpl	$8451, %eax
	je	.L943
	ja	.L425
	leal	-8211(%rax), %ecx
	cmpl	$43, %ecx
	ja	.L458
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L446:
	cmpl	$65505, %eax
	je	.L450
	cmpl	$65509, %eax
	je	.L451
	cmpl	$65504, %eax
	jne	.L458
	movq	%r10, %rdi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L372:
	cmpl	$713, %eax
	je	.L526
	jb	.L414
	leal	-913(%rax), %ecx
	cmpl	$56, %ecx
	ja	.L414
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L1341:
	cmpl	$363, %eax
	je	.L515
	cmpl	$462, %eax
	leaq	.LC28(%rip), %rcx
	je	.L508
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L1346:
	cmpl	$9671, %eax
	je	.L544
	cmpl	$9675, %eax
	leaq	.LC11(%rip), %rcx
	je	.L508
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L400:
	leal	-12832(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L414
.L1270:
	addl	$69, %eax
	movl	$34, %edi
	jmp	.L888
.L1361:
	cmpl	$4, %edx
	ja	.L25
	movslq	%edx, %r14
	leaq	184(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rcx
.L26:
	movzbl	4(%r11,%rsi), %edi
	movb	%dil, (%rdx,%rsi)
	addq	$1, %rsi
	cmpq	%r14, %rsi
	jne	.L26
	leaq	4(%r13), %rsi
	cmpq	%rsi, %r10
	movq	%rsi, (%rsp)
	jb	.L907
	leaq	183(%rsp), %r9
	movq	%rax, %rsi
.L27:
	addq	$1, %rsi
	movzbl	-1(%rsi), %edi
	addq	$1, %rcx
	cmpq	$3, %rcx
	setbe	%r8b
	cmpq	%rsi, %r15
	movb	%dil, (%r9,%rcx)
	seta	%dil
	testb	%dil, %r8b
	jne	.L27
	movzbl	184(%rsp), %r9d
	leaq	(%rdx,%rcx), %r12
	cmpl	$126, %r9d
	movl	%r9d, %edi
	movl	%r9d, %r8d
	ja	.L1387
	cmpb	$27, %r9b
	je	.L1388
	leal	-14(%r9), %esi
	cmpb	$1, %sil
	jbe	.L1389
.L39:
	movl	32(%rsp), %esi
	testl	%esi, %esi
	je	.L916
	cmpl	$8, %esi
	je	.L1390
	cmpl	$16, 32(%rsp)
	jne	.L1391
	subl	$33, %r9d
	cmpl	$92, %r9d
	jbe	.L1392
.L45:
	cmpq	$0, 96(%rsp)
	je	.L293
	andl	$2, %ebx
	je	.L293
	movq	%rdx, %rcx
.L896:
	addq	$1, %rcx
.L1307:
	movq	96(%rsp), %rdi
	addq	$1, (%rdi)
.L31:
	subq	%rdx, %rcx
	cmpq	%r14, %rcx
	jle	.L1393
	subq	%r14, %rcx
	movl	16(%rbp), %r14d
	leaq	(%rax,%rcx), %r12
	movq	8(%rsp), %rax
	movq	%r12, (%rax)
	movl	24(%rsp), %eax
	andl	$-8, %eax
	movl	%eax, (%r11)
	movl	%eax, 24(%rsp)
	jmp	.L22
.L1372:
	leal	-9472(%rax), %ecx
	cmpl	$75, %ecx
	ja	.L414
.L1269:
	addl	$36, %eax
	movl	$41, %edi
	jmp	.L888
.L1359:
	leal	-65281(%rax), %ecx
	cmpl	$93, %ecx
	ja	.L414
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L946:
	leaq	.LC33(%rip), %rcx
	jmp	.L508
.L1380:
	cmpq	%r12, %r15
	movq	(%rsp), %rdx
	je	.L949
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L950
	movl	16(%rsp), %r8d
	movl	$4, %ecx
	andl	$2, %r11d
.L590:
	movzbl	(%r12), %eax
	cmpl	$126, %eax
	movl	%eax, %edi
	ja	.L607
	cmpb	$27, %al
	je	.L1394
	cmpb	$14, %al
	je	.L1395
	cmpb	$15, %al
	je	.L1396
.L597:
	testl	%r8d, %r8d
	jne	.L605
	addq	$1, %r12
.L604:
	movl	%edi, (%rdx)
	movq	%rsi, %rdx
.L592:
	cmpq	%r12, %r15
	je	.L1397
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jnb	.L590
	movl	%r8d, 16(%rsp)
	movl	$5, %ecx
	jmp	.L589
.L67:
	movq	144(%rsp), %rdx
.L1314:
	cmpq	%r14, %rdx
	jne	.L286
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$5, 32(%rsp)
	jmp	.L7
.L517:
	leaq	.LC25(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L457:
	leaq	2(%rbx), %rax
	cmpq	%rax, %r13
	jb	.L1318
	movl	%edx, %edi
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L1383:
	cmpq	$-1, %rcx
	je	.L885
.L904:
	movzbl	1(%rcx), %eax
	movl	$24, %edi
	movb	%al, 120(%rsp)
	movzbl	2(%rcx), %eax
	movb	%al, 121(%rsp)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L502:
	cmpl	$9792, %eax
	je	.L532
	ja	.L533
	cmpl	$9670, %eax
	je	.L534
	jbe	.L1398
	cmpl	$9678, %eax
	je	.L542
	jbe	.L1399
	cmpl	$9733, %eax
	je	.L546
	cmpl	$9734, %eax
	je	.L547
	cmpl	$9679, %eax
	jne	.L500
	jmp	.L548
.L546:
	leaq	.LC8(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L1376:
	andl	$128, %esi
	movq	168(%rsp), %rbx
	orl	$32, %esi
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L1377:
	andl	$128, %esi
	orl	$64, %esi
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L460:
	cmpl	$13269, %eax
	ja	.L480
	cmpl	$13198, %eax
	jnb	.L481
	cmpl	$12329, %eax
	ja	.L482
	cmpl	$12288, %eax
	jnb	.L483
	cmpl	$9312, %eax
	jb	.L500
	cmpl	$9341, %eax
	ja	.L1400
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L614:
	cmpl	$8, %esi
	je	.L619
	testb	$64, %dil
	je	.L619
	cmpl	$9249, %ecx
	ja	.L687
	cmpl	$9216, %ecx
	jnb	.L688
	cmpl	$8457, %ecx
	je	.L689
	ja	.L690
	cmpl	$969, %ecx
	jbe	.L1401
	cmpl	$8451, %ecx
	je	.L971
	ja	.L696
	leal	-8211(%rcx), %edx
	cmpl	$43, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
.L724:
	movzbl	(%rdx), %r8d
	testb	%r8b, %r8b
	jne	.L1402
.L729:
	leal	-19975(%rcx), %edx
	cmpl	$20893, %edx
	jbe	.L1403
.L893:
	cmpl	$9371, %ecx
	ja	.L773
	cmpl	$9312, %ecx
	jnb	.L774
	cmpl	$472, %ecx
	je	.L775
	ja	.L776
	cmpl	$333, %ecx
	je	.L777
	jbe	.L1404
	cmpl	$464, %ecx
	je	.L784
	jbe	.L1405
	cmpl	$468, %ecx
	je	.L788
	cmpl	$470, %ecx
	je	.L789
	cmpl	$466, %ecx
	je	.L790
.L771:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L617
	movl	%esi, %eax
	orl	%edi, %eax
	cmpq	$0, 96(%rsp)
	je	.L837
	movq	40(%rsp), %rsi
	movl	%eax, (%rsi)
	testb	$8, 16(%rbp)
	jne	.L838
	movl	%eax, %esi
	andl	$224, %eax
	andl	$24, %esi
	movl	%eax, %edi
.L839:
	testb	$2, %r11b
	movq	176(%rsp), %r12
	jne	.L841
	movq	184(%rsp), %rdx
	movl	%edi, 36(%rsp)
	movl	$6, %ecx
	movl	%esi, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L613:
	movq	8(%rsp), %rax
	movq	40(%rsp), %rdi
	movq	136(%rsp), %r10
	movq	%r12, (%rax)
	movl	16(%rsp), %eax
	orl	36(%rsp), %eax
	movl	%eax, (%rdi)
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L505:
	cmpl	$1105, %eax
	ja	.L520
	cmpl	$1025, %eax
	jnb	.L521
	cmpl	$711, %eax
	je	.L522
	ja	.L523
	cmpl	$474, %eax
	je	.L524
	cmpl	$476, %eax
	leaq	.LC21(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L533:
	cmpl	$40864, %eax
	jbe	.L1406
	cmpl	$65504, %eax
	je	.L555
	jbe	.L1407
	cmpl	$65507, %eax
	je	.L558
	cmpl	$65509, %eax
	je	.L559
	cmpl	$65505, %eax
	jne	.L500
	jmp	.L560
.L558:
	leaq	.LC2(%rip), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L619:
	cmpl	$9371, %ecx
	ja	.L622
	cmpl	$9312, %ecx
	jnb	.L623
	cmpl	$472, %ecx
	je	.L775
	ja	.L625
	cmpl	$333, %ecx
	je	.L777
	jbe	.L1408
	cmpl	$464, %ecx
	je	.L784
	jbe	.L1409
	cmpl	$468, %ecx
	je	.L788
	cmpl	$470, %ecx
	je	.L789
	cmpl	$466, %ecx
	jne	.L685
.L790:
	leaq	.LC26(%rip), %rdx
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L1378:
	leaq	3(%rdx), %r9
	cmpq	%r9, %r15
	jb	.L938
	movzbl	2(%rdx), %r9d
	cmpb	$41, %r9b
	je	.L1410
	cmpb	$42, %r9b
	jne	.L324
	leaq	4(%rdx), %r9
	cmpq	%r9, %r15
	jb	.L938
	cmpb	$72, 3(%rdx)
	jne	.L324
.L328:
	movq	%r9, %rdx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L924:
	movl	36(%rsp), %r8d
	movq	(%rsp), %rbx
	movq	%r12, %rdx
	movl	16(%rsp), %esi
	movl	$5, 32(%rsp)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L1332:
	cmpq	$0, 48(%rsp)
	jne	.L1411
	cmpl	$1, %r12d
	movq	32(%rbp), %rbx
	jne	.L11
	movl	(%rbx), %r13d
	movq	0(%rbp), %rax
	testl	%r13d, %r13d
	je	.L12
	movq	56(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L1412
	cmpq	8(%rbp), %rax
	je	.L907
	movb	$15, (%rax)
	testb	$1, 16(%rbp)
	leaq	1(%rax), %r12
	movq	32(%rbp), %rdx
	movl	$0, (%rdx)
	jne	.L1413
	movq	72(%rsp), %r14
	movq	%rax, 184(%rsp)
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	leaq	184(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r14
	cmpl	$4, %eax
	movl	%eax, 48(%rsp)
	popq	%rbp
	popq	%r14
	je	.L17
	cmpq	%r12, 184(%rsp)
	jne	.L1414
.L18:
	movl	32(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L7
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L463:
	cmpl	$8601, %eax
	ja	.L472
	cmpl	$8592, %eax
	jnb	.L473
	cmpl	$8544, %eax
	jb	.L500
	cmpl	$8553, %eax
	ja	.L1415
	subl	$53, %eax
	movl	$36, %ecx
	.p2align 4,,10
	.p2align 3
.L869:
	movb	%cl, 120(%rsp)
	movb	%al, 121(%rsp)
	movl	$16, %edi
	jmp	.L413
.L480:
	cmpl	$65373, %eax
	ja	.L490
	cmpl	$65281, %eax
	jnb	.L491
	cmpl	$19968, %eax
	jb	.L500
	cmpl	$40860, %eax
	ja	.L1416
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L1398:
	cmpl	$9632, %eax
	je	.L536
	jbe	.L1417
	cmpl	$9650, %eax
	je	.L539
	cmpl	$9651, %eax
	je	.L540
	cmpl	$9633, %eax
	jne	.L500
	jmp	.L541
.L539:
	leaq	.LC15(%rip), %rcx
	jmp	.L508
.L1386:
	movq	160(%rsp), %rax
	movl	$5, 32(%rsp)
	jmp	.L341
.L605:
	cmpl	$8, %r8d
	je	.L1418
	cmpl	$16, %r8d
	jne	.L609
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1419
.L607:
	cmpq	$0, 96(%rsp)
	je	.L966
	testl	%r11d, %r11d
	jne	.L1420
.L966:
	movl	%r8d, 16(%rsp)
	movl	$6, %ecx
.L589:
	movq	8(%rsp), %rax
	movq	40(%rsp), %rsi
	movq	%r12, (%rax)
	movl	16(%rsp), %eax
	orl	36(%rsp), %eax
	movl	%eax, (%rsi)
.L611:
	cmpq	%r10, %rdx
	jne	.L1421
	cmpq	$5, %rcx
	jne	.L1422
	cmpq	%rdx, (%rsp)
	jne	.L585
	subl	$1, 20(%rbp)
	jmp	.L585
.L555:
	leaq	.LC4(%rip), %rcx
	jmp	.L508
.L511:
	leaq	.LC32(%rip), %rcx
	jmp	.L508
.L518:
	leaq	.LC24(%rip), %rcx
	jmp	.L508
.L534:
	leaq	.LC13(%rip), %rcx
	jmp	.L508
.L506:
	leaq	.LC30(%rip), %rcx
	jmp	.L508
.L513:
	leaq	.LC27(%rip), %rcx
	jmp	.L508
.L504:
	leaq	.LC23(%rip), %rcx
	jmp	.L508
.L532:
	leaq	.LC6(%rip), %rcx
	jmp	.L508
.L540:
	leaq	.LC14(%rip), %rcx
	jmp	.L508
.L547:
	leaq	.LC7(%rip), %rcx
	jmp	.L508
.L515:
	leaq	.LC29(%rip), %rcx
	jmp	.L508
.L542:
	leaq	.LC10(%rip), %rcx
	jmp	.L508
.L526:
	leaq	.LC19(%rip), %rcx
	jmp	.L508
.L536:
	leaq	.LC17(%rip), %rcx
	jmp	.L508
.L559:
	leaq	.LC1(%rip), %rcx
	jmp	.L508
.L522:
	leaq	.LC20(%rip), %rcx
	jmp	.L508
.L524:
	leaq	.LC22(%rip), %rcx
	jmp	.L508
.L544:
	leaq	.LC12(%rip), %rcx
	jmp	.L508
.L1356:
	cmpl	$275, %eax
	je	.L946
	jbe	.L1423
	cmpl	$283, %eax
	je	.L511
	cmpl	$299, %eax
	leaq	.LC31(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1406:
	cmpl	$19968, %eax
	jnb	.L550
	cmpl	$12585, %eax
	ja	.L551
	cmpl	$12288, %eax
	jnb	.L552
	cmpl	$9794, %eax
	leaq	.LC5(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1353:
	leal	-9472(%rax), %ecx
	cmpl	$322, %ecx
	ja	.L458
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L377:
	cmpl	$8978, %eax
	leaq	.LC18(%rip), %rcx
	je	.L508
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L622:
	cmpl	$9792, %ecx
	je	.L803
	ja	.L653
	cmpl	$9670, %ecx
	je	.L805
	jbe	.L1424
	cmpl	$9678, %ecx
	je	.L813
	jbe	.L1425
	cmpl	$9733, %ecx
	je	.L817
	cmpl	$9734, %ecx
	je	.L818
	cmpl	$9679, %ecx
	jne	.L685
.L819:
	leaq	.LC9(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L779:
	movzbl	(%rdx), %r8d
	movzbl	1(%rdx), %ecx
.L874:
	testb	%cl, %cl
	je	.L284
.L894:
	movb	%r8b, 122(%rsp)
	movb	%cl, 123(%rsp)
	movl	$8, %r8d
.L684:
	cmpl	%esi, %r8d
	movq	184(%rsp), %rdx
	je	.L728
	movl	%r8d, %ecx
	movl	$16, %r9d
	sarl	$3, %ecx
	sall	%cl, %r9d
	testl	%edi, %r9d
	jne	.L842
	leaq	4(%rdx), %r9
	cmpq	%r9, %r10
	jb	.L1321
	subl	$1, %ecx
	cmpl	$2, %ecx
	ja	.L844
	addl	%ecx, %ecx
	leaq	.LC56(%rip), %r9
	movslq	%ecx, %rcx
	addq	%r9, %rcx
	leaq	1(%rdx), %r9
	cmpl	$8, %r8d
	movq	%r9, 184(%rsp)
	movb	$27, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %r9
	movq	%r9, 184(%rsp)
	movb	$36, (%rdx)
	movq	184(%rsp), %rdx
	movzbl	(%rcx), %r9d
	movzbl	1(%rcx), %ecx
	leaq	1(%rdx), %r12
	movq	%r12, 184(%rsp)
	movb	%r9b, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %r9
	movq	%r9, 184(%rsp)
	movb	%cl, (%rdx)
	je	.L1426
	cmpl	$16, %r8d
	movq	184(%rsp), %rdx
	je	.L1427
	orb	$-128, %dil
.L842:
	cmpl	$24, %r8d
	jne	.L727
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r10
	jb	.L1321
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	$27, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	$78, (%rdx)
	movq	184(%rsp), %rdx
.L848:
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r10
	jb	.L1321
.L850:
	leaq	1(%rdx), %rcx
	movl	%r8d, %esi
	movq	%rcx, 184(%rsp)
	movzbl	122(%rsp), %ecx
	movb	%cl, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movzbl	123(%rsp), %ecx
	movb	%cl, (%rdx)
	jmp	.L617
.L1375:
	leal	-164(%rax), %ecx
	cmpl	$93, %ecx
	ja	.L414
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L1373:
	leal	-711(%rax), %ecx
	cmpl	$18, %ecx
	ja	.L458
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L1339:
	leal	-8560(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L458
	subl	$59, %eax
	movl	$38, %ecx
	jmp	.L868
.L977:
	movl	%r14d, 32(%rsp)
	jmp	.L584
.L1374:
	leal	-65072(%rax), %ecx
	cmpl	$59, %ecx
	ja	.L458
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L352:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L378:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L401:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L399:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L370:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L410
.L520:
	cmpl	$8869, %eax
	ja	.L528
	cmpl	$8451, %eax
	jnb	.L529
	leal	-8213(%rax), %ecx
	cmpl	$38, %ecx
	ja	.L500
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L442:
	cmpl	$12963, %eax
	leaq	.LC37(%rip), %rdi
	je	.L424
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L425:
	cmpl	$8453, %eax
	leaq	.LC41(%rip), %rdi
	je	.L424
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L433:
	cmpl	$8895, %eax
	leaq	.LC38(%rip), %rdi
	je	.L424
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L472:
	cmpl	$8869, %eax
	je	.L476
	ja	.L477
	leal	-8725(%rax), %ecx
	cmpl	$82, %ecx
	ja	.L500
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L464:
	cmpl	$8451, %eax
	je	.L945
	ja	.L469
	leal	-8211(%rax), %ecx
	cmpl	$43, %ecx
	ja	.L500
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L490:
	cmpl	$65505, %eax
	je	.L494
	cmpl	$65509, %eax
	je	.L495
	cmpl	$65504, %eax
	jne	.L500
	movq	%r10, %rdi
	.p2align 4,,10
	.p2align 3
.L468:
	movzbl	(%rdi), %ecx
	movzbl	1(%rdi), %eax
	jmp	.L869
.L523:
	cmpl	$713, %eax
	je	.L526
	jb	.L500
	leal	-913(%rax), %ecx
	cmpl	$56, %ecx
	ja	.L500
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	.p2align 4,,10
	.p2align 3
.L561:
	movzbl	(%rcx), %edi
	testb	%dil, %dil
	je	.L500
.L1330:
	movzbl	1(%rcx), %eax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1363:
	movl	32(%rsp), %esi
	leaq	1(%r13), %rax
	testl	%esi, %esi
	je	.L918
	movq	%rax, 152(%rsp)
	movb	$15, 0(%r13)
	movq	152(%rsp), %rcx
	cmpq	%rcx, %r10
	je	.L67
	leaq	1(%rcx), %rax
.L66:
	movq	%rax, 152(%rsp)
	movb	%dl, (%rcx)
	jmp	.L68
.L687:
	cmpl	$13269, %ecx
	ja	.L707
	cmpl	$13198, %ecx
	jnb	.L708
	cmpl	$12329, %ecx
	jbe	.L1428
	cmpl	$12585, %ecx
	ja	.L713
	cmpl	$12549, %ecx
	jnb	.L714
	cmpl	$12539, %ecx
	leaq	.LC20(%rip), %rdx
	jne	.L729
.L695:
	movzbl	(%rdx), %r8d
	movzbl	1(%rdx), %ecx
.L872:
	cmpl	$16, %esi
	movb	%r8b, 122(%rsp)
	movb	%cl, 123(%rsp)
	movq	184(%rsp), %rdx
	movl	$16, %r8d
	je	.L728
.L727:
	testl	%esi, %esi
	jne	.L848
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %r10
	jb	.L1321
	movq	%rcx, 184(%rsp)
	movb	$14, (%rdx)
	movq	184(%rsp), %rdx
	jmp	.L848
.L625:
	cmpl	$1105, %ecx
	ja	.L640
	cmpl	$1025, %ecx
	jnb	.L641
	cmpl	$711, %ecx
	je	.L793
	jbe	.L1429
	cmpl	$713, %ecx
	je	.L797
	jb	.L685
	leal	-913(%rcx), %edx
	cmpl	$56, %edx
	ja	.L685
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
.L681:
	movzbl	(%rdx), %r8d
	testb	%r8b, %r8b
	jne	.L1331
.L685:
	leal	-19975(%rcx), %edx
	cmpl	$20893, %edx
	ja	.L891
	leal	-13312(%rcx), %edx
	leaq	(%rdx,%rdx,2), %rdx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdx
	cmpb	$2, (%rdx)
	je	.L1430
.L891:
	cmpl	$9249, %ecx
	ja	.L731
	cmpl	$9216, %ecx
	jnb	.L732
	cmpl	$8457, %ecx
	je	.L733
	ja	.L734
	cmpl	$969, %ecx
	jbe	.L1431
	cmpl	$8451, %ecx
	je	.L973
	ja	.L740
	leal	-8211(%rcx), %edx
	cmpl	$43, %edx
	ja	.L771
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
.L768:
	movzbl	(%rdx), %r8d
	testb	%r8b, %r8b
	je	.L771
	movzbl	1(%rdx), %ecx
.L873:
	movb	%r8b, 122(%rsp)
	movb	%cl, 123(%rsp)
	movl	$16, %r8d
	jmp	.L684
.L653:
	cmpl	$40864, %ecx
	jbe	.L1432
	cmpl	$65504, %ecx
	je	.L826
	jbe	.L1433
	cmpl	$65507, %ecx
	je	.L829
	cmpl	$65509, %ecx
	je	.L830
	cmpl	$65505, %ecx
	jne	.L685
.L831:
	leaq	.LC3(%rip), %rdx
	jmp	.L779
.L1412:
	movl	$0, (%rbx)
.L12:
	testl	%edx, %edx
	jne	.L860
.L17:
	movq	72(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	pushq	%rax
	pushq	$1
.L1323:
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	96(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 48(%rsp)
	popq	%r9
	popq	%r10
	jmp	.L7
.L551:
	leal	-12832(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L1270
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1423:
	leal	-164(%rax), %ecx
	cmpl	$93, %ecx
	ja	.L500
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L528:
	cmpl	$8978, %eax
	leaq	.LC18(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	%r15, %rdi
	subq	%r12, %rdi
	cmpq	$1, %rdi
	jbe	.L964
	movzbl	1(%r12), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L607
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$8690, %eax
	jg	.L607
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rdi
	cltq
	movzwl	(%rdi,%rax,2), %edi
	testw	%di, %di
	je	.L607
	addq	$2, %r12
.L608:
	cmpl	$65533, %edi
	jne	.L604
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L483:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L728:
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r10
	jb	.L1321
	movl	%esi, %r8d
	jmp	.L850
.L11:
	movq	$0, (%rbx)
	testb	$1, 16(%rbp)
	movl	$0, 32(%rsp)
	jne	.L7
	movq	72(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	pushq	%rax
	pushq	%r12
	jmp	.L1323
.L482:
	cmpl	$12585, %eax
	ja	.L486
	cmpl	$12549, %eax
	jnb	.L487
	cmpl	$12539, %eax
	leaq	.LC20(%rip), %rdi
	je	.L468
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L443:
	addl	$66, %eax
	movl	$37, %ecx
	jmp	.L868
.L439:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L967:
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movl	$7, %ecx
	jmp	.L613
.L451:
	leaq	.LC34(%rip), %rdi
	jmp	.L424
.L450:
	leaq	.LC35(%rip), %rdi
	jmp	.L424
.L447:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L943:
	leaq	.LC42(%rip), %rdi
	jmp	.L424
.L432:
	leaq	.LC39(%rip), %rdi
	jmp	.L424
.L421:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L429:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L418:
	leaq	.LC40(%rip), %rdi
	jmp	.L424
.L417:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L437:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L453
.L968:
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movl	$5, %ecx
	jmp	.L613
.L1410:
	leaq	4(%rdx), %r9
	cmpq	%r9, %r15
	jb	.L938
	movzbl	3(%rdx), %r10d
	cmpb	$65, %r10b
	je	.L327
	cmpb	$71, %r10b
	jne	.L324
.L327:
	cmpb	$65, %r10b
	movl	%r14d, %r8d
	movl	$64, %eax
	cmovne	%eax, %r8d
	jmp	.L328
.L1357:
	cmpl	$363, %eax
	je	.L515
	cmpl	$462, %eax
	leaq	.LC28(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1399:
	cmpl	$9671, %eax
	je	.L544
	cmpl	$9675, %eax
	leaq	.LC11(%rip), %rcx
	je	.L508
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%r10, 104(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	%r15, %r8
	movq	72(%rsp), %rdi
	movq	%rbp, %rsi
	leaq	184(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	56(%rsp), %rsi
	movl	%eax, 48(%rsp)
	movl	%eax, %edi
	popq	%r8
	popq	%r9
	movl	(%rsi), %ecx
	movq	104(%rsp), %r10
	movl	%ecx, %edx
	movl	%ecx, %esi
	andl	$24, %edx
	andl	$224, %esi
	cmpl	$6, %eax
	je	.L568
	cmpl	$5, %edi
	movq	160(%rsp), %rax
	jne	.L580
	movl	%ecx, %esi
	movq	168(%rsp), %rbx
	andl	$248, %esi
	jmp	.L341
.L1321:
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movl	$5, %ecx
	movq	176(%rsp), %r12
	jmp	.L613
.L1379:
	movzbl	3(%rdx), %edx
	subl	$33, %edx
	cmpl	$93, %edx
	ja	.L330
	imull	$94, %eax, %eax
	addl	%edx, %eax
	cmpl	$7649, %eax
	jg	.L330
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %edi
	testw	%di, %di
	je	.L330
	cmpl	$65533, %edi
	movq	%r9, %rdx
	jne	.L331
	movq	%r9, %r10
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L1418:
	subl	$33, %eax
	cmpl	$86, %eax
	ja	.L607
	movq	%r15, %rdi
	subq	%r12, %rdi
	cmpq	$1, %rdi
	jbe	.L964
	movzbl	1(%r12), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L607
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$8177, %eax
	jg	.L607
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rdi
	cltq
	leaq	2(%r12), %r9
	movzwl	(%rdi,%rax,2), %edi
	testw	%di, %di
	je	.L607
	movq	%r9, %r12
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L1430:
	cmpq	$-1, %rdx
	je	.L891
.L905:
	movzbl	1(%rdx), %ecx
	movzbl	2(%rdx), %edx
	movl	$24, %r8d
	movb	%cl, 122(%rsp)
	movb	%dl, 123(%rsp)
	jmp	.L684
.L923:
	movl	36(%rsp), %r8d
	movq	(%rsp), %rbx
	movq	%r15, %rdx
	movl	16(%rsp), %esi
	movl	$4, 32(%rsp)
	jmp	.L316
.L69:
	cmpl	$9371, %edx
	ja	.L72
	cmpl	$9312, %edx
	jnb	.L73
	cmpl	$472, %edx
	je	.L74
	ja	.L75
	cmpl	$333, %edx
	je	.L76
	jbe	.L1434
	cmpl	$464, %edx
	je	.L83
	jbe	.L1435
	cmpl	$468, %edx
	je	.L87
	cmpl	$470, %edx
	je	.L88
	cmpl	$466, %edx
	jne	.L136
	leaq	.LC26(%rip), %rcx
.L78:
	movzbl	(%rcx), %esi
	movzbl	1(%rcx), %edx
.L863:
	testb	%dl, %dl
	je	.L284
.L134:
	movb	%sil, 116(%rsp)
	movb	%dl, 117(%rsp)
	movl	$8, %esi
	jmp	.L135
.L1413:
	movq	%r12, %rax
.L860:
	movq	%rax, 0(%rbp)
	movl	$0, 32(%rsp)
	jmp	.L7
.L1400:
	leal	-9472(%rax), %ecx
	cmpl	$322, %ecx
	ja	.L500
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L1354:
	movzbl	1(%rdi), %eax
	jmp	.L868
.L1394:
	leaq	2(%r12), %r9
	cmpq	%r9, %r15
	jb	.L964
	movzbl	1(%r12), %ebx
	cmpb	$36, %bl
	je	.L1436
	cmpb	$78, %bl
	jne	.L597
	leaq	4(%r12), %rax
	cmpq	%rax, %r15
	movq	%rax, %rbx
	jb	.L964
	movzbl	2(%r12), %eax
	subl	$33, %eax
	cmpl	$92, %eax
	jbe	.L1437
.L603:
	cmpq	$0, 96(%rsp)
	leaq	-2(%r9), %r12
	je	.L966
	testl	%r11d, %r11d
	je	.L966
	movq	96(%rsp), %rax
	movq	%r9, %r12
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L1415:
	leal	-8560(%rax), %ecx
	cmpl	$9, %ecx
	ja	.L500
	subl	$59, %eax
	movl	$38, %ecx
	jmp	.L869
.L1384:
	leal	-711(%rax), %ecx
	cmpl	$18, %ecx
	ja	.L500
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L1407:
	leal	-65281(%rax), %ecx
	cmpl	$93, %ecx
	ja	.L500
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L1426:
	andl	$128, %edi
	movq	184(%rsp), %rdx
	orl	$32, %edi
	jmp	.L727
.L1416:
	leal	-65072(%rax), %ecx
	cmpl	$59, %ecx
	ja	.L500
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L1417:
	leal	-9472(%rax), %ecx
	cmpl	$75, %ecx
	jbe	.L1269
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L773:
	cmpl	$9792, %ecx
	je	.L803
	ja	.L804
	cmpl	$9670, %ecx
	je	.L805
	jbe	.L1438
	cmpl	$9678, %ecx
	je	.L813
	jbe	.L1439
	cmpl	$9733, %ecx
	je	.L817
	cmpl	$9734, %ecx
	je	.L818
	cmpl	$9679, %ecx
	jne	.L771
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r13, %rcx
	jmp	.L66
.L1395:
	xorl	%r8d, %r8d
	addq	$1, %r12
	cmpl	$64, 36(%rsp)
	sete	%r8b
	leal	8(,%r8,8), %r8d
	jmp	.L592
.L529:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L503:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L521:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L552:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L550:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rcx
	jmp	.L561
.L477:
	cmpl	$8895, %eax
	leaq	.LC38(%rip), %rdi
	je	.L468
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$12963, %eax
	leaq	.LC37(%rip), %rdi
	je	.L468
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	8(%rsp), %rdi
	movq	%r15, %rdx
	subq	%rax, %rdx
	movq	%r15, (%rdi)
	movq	(%rsp), %rdi
	addq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L57
	addq	$1, %rax
	cmpq	%rdi, %rdx
	movq	(%rsp), %rcx
	jbe	.L59
.L60:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %esi
	addq	$1, %rax
	movb	%sil, 4(%r11,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	jne	.L60
.L59:
	movl	$7, 32(%rsp)
	jmp	.L7
.L469:
	cmpl	$8453, %eax
	leaq	.LC41(%rip), %rdi
	je	.L468
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L1396:
	addq	$1, %r12
	xorl	%r8d, %r8d
	jmp	.L592
.L1424:
	cmpl	$9632, %ecx
	je	.L807
	jbe	.L1440
	cmpl	$9650, %ecx
	je	.L810
	cmpl	$9651, %ecx
	je	.L811
	cmpl	$9633, %ecx
	jne	.L685
.L812:
	leaq	.LC16(%rip), %rdx
	jmp	.L779
.L690:
	cmpl	$8601, %ecx
	jbe	.L1441
	cmpl	$8869, %ecx
	je	.L703
	ja	.L704
	leal	-8725(%rcx), %edx
	cmpl	$82, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L461:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L462:
	leaq	.LC40(%rip), %rdi
	jmp	.L468
.L476:
	leaq	.LC39(%rip), %rdi
	jmp	.L468
.L487:
	addl	$66, %eax
	movl	$37, %ecx
	jmp	.L869
.L465:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L473:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L495:
	leaq	.LC34(%rip), %rdi
	jmp	.L468
.L494:
	leaq	.LC35(%rip), %rdi
	jmp	.L468
.L491:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L707:
	cmpl	$65373, %ecx
	ja	.L717
	cmpl	$65281, %ecx
	jnb	.L718
	cmpl	$19968, %ecx
	jb	.L729
	cmpl	$40860, %ecx
	ja	.L1442
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %r8
	leal	-19968(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L481:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rax), %ecx
	leaq	(%rdi,%rcx,2), %rdi
	jmp	.L497
.L945:
	leaq	.LC42(%rip), %rdi
	jmp	.L468
.L1385:
	movzbl	1(%rdi), %eax
	jmp	.L869
.L964:
	movl	%r8d, 16(%rsp)
	movl	$7, %ecx
	jmp	.L589
.L1427:
	andl	$128, %edi
	orl	$64, %edi
	jmp	.L727
.L1432:
	cmpl	$19968, %ecx
	jnb	.L670
	cmpl	$12585, %ecx
	ja	.L671
	cmpl	$12288, %ecx
	jnb	.L672
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$1105, %edx
	ja	.L90
	cmpl	$1025, %edx
	jnb	.L91
	cmpl	$711, %edx
	je	.L92
	ja	.L93
	cmpl	$474, %edx
	je	.L94
	cmpl	$476, %edx
	leaq	.LC21(%rip), %rcx
	je	.L78
.L136:
	leal	-19975(%rdx), %ecx
	cmpl	$20893, %ecx
	ja	.L882
	leal	-13312(%rdx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rcx
	cmpb	$2, (%rcx)
	jne	.L882
	cmpq	$-1, %rcx
	je	.L882
.L903:
	movzbl	1(%rcx), %eax
	movl	$24, %esi
	movb	%al, 116(%rsp)
	movzbl	2(%rcx), %eax
	movb	%al, 117(%rsp)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L1408:
	cmpl	$275, %ecx
	je	.L974
	jbe	.L1443
	cmpl	$283, %ecx
	je	.L782
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$9792, %edx
	je	.L102
	ja	.L103
	cmpl	$9670, %edx
	je	.L104
	jbe	.L1444
	cmpl	$9678, %edx
	je	.L112
	jbe	.L1445
	cmpl	$9733, %edx
	je	.L116
	cmpl	$9734, %edx
	je	.L117
	cmpl	$9679, %edx
	jne	.L136
	leaq	.LC9(%rip), %rcx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L916:
	leaq	1(%rdx), %rcx
.L43:
	movl	%r8d, 0(%r13)
	movl	(%r11), %esi
	movq	(%rsp), %r13
	movl	%esi, %r14d
	movl	%esi, 24(%rsp)
	andl	$7, %r14d
	jmp	.L31
.L731:
	cmpl	$13269, %ecx
	ja	.L751
	cmpl	$13198, %ecx
	jnb	.L752
	cmpl	$12329, %ecx
	jbe	.L1446
	cmpl	$12585, %ecx
	ja	.L757
	cmpl	$12549, %ecx
	jnb	.L758
	cmpl	$12539, %ecx
	leaq	.LC20(%rip), %rdx
	jne	.L771
.L739:
	movzbl	(%rdx), %r8d
	movzbl	1(%rdx), %ecx
	jmp	.L873
.L1382:
	movl	%edi, 36(%rsp)
	movq	176(%rsp), %r12
	movl	$5, %ecx
	movl	$0, 16(%rsp)
	jmp	.L613
.L570:
	movq	96(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 32(%rsp)
	movq	%rax, 160(%rsp)
	addq	$1, (%rdi)
	jmp	.L580
.L1420:
	movq	96(%rsp), %rax
	addq	$1, %r12
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L592
.L640:
	cmpl	$8869, %ecx
	ja	.L648
	cmpl	$8451, %ecx
	jnb	.L649
	leal	-8213(%rcx), %edx
	cmpl	$38, %edx
	ja	.L685
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L1428:
	cmpl	$12288, %ecx
	jnb	.L710
	cmpl	$9312, %ecx
	jb	.L729
	cmpl	$9341, %ecx
	ja	.L1447
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-9312(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L804:
	cmpl	$40864, %ecx
	jbe	.L1448
	cmpl	$65504, %ecx
	je	.L826
	jbe	.L1449
	cmpl	$65507, %ecx
	je	.L829
	cmpl	$65509, %ecx
	je	.L830
	cmpl	$65505, %ecx
	jne	.L771
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L1401:
	cmpl	$913, %ecx
	jnb	.L692
	cmpl	$167, %ecx
	jb	.L729
	cmpl	$247, %ecx
	ja	.L1450
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leal	-167(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1441:
	cmpl	$8592, %ecx
	jnb	.L700
	cmpl	$8544, %ecx
	jb	.L729
	cmpl	$8553, %ecx
	ja	.L1451
	subl	$53, %ecx
	movl	$36, %r8d
	jmp	.L872
.L138:
	cmpl	$13269, %edx
	ja	.L158
	cmpl	$13198, %edx
	jnb	.L159
	cmpl	$12329, %edx
	jbe	.L1452
	cmpl	$12585, %edx
	ja	.L164
	cmpl	$12549, %edx
	jnb	.L207
	cmpl	$12539, %edx
	leaq	.LC20(%rip), %rcx
	jne	.L178
.L188:
	movzbl	(%rcx), %esi
.L865:
	movb	%sil, 116(%rsp)
	movzbl	1(%rcx), %eax
	movl	$16, %esi
	movb	%al, 117(%rsp)
	jmp	.L135
.L776:
	cmpl	$1105, %ecx
	ja	.L791
	cmpl	$1025, %ecx
	jnb	.L792
	cmpl	$711, %ecx
	je	.L793
	jbe	.L1453
	cmpl	$713, %ecx
	je	.L797
	jb	.L771
	leal	-913(%rcx), %edx
	cmpl	$56, %edx
	ja	.L771
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
.L832:
	movzbl	(%rdx), %r8d
	testb	%r8b, %r8b
	je	.L771
.L1331:
	movzbl	1(%rdx), %ecx
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L1429:
	cmpl	$474, %ecx
	je	.L795
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1370:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r10
	jb	.L67
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$27, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$78, (%rax)
	movq	152(%rsp), %rax
	jmp	.L294
.L717:
	cmpl	$65505, %ecx
	je	.L721
	cmpl	$65509, %ecx
	je	.L722
	cmpl	$65504, %ecx
	jne	.L729
	movq	%rbx, %rdx
	jmp	.L695
.L1425:
	cmpl	$9671, %ecx
	je	.L815
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1436:
	leaq	3(%r12), %r9
	cmpq	%r9, %r15
	jb	.L964
	movzbl	2(%r12), %r9d
	cmpb	$41, %r9b
	je	.L1454
	cmpb	$42, %r9b
	jne	.L597
	leaq	4(%r12), %r9
	cmpq	%r9, %r15
	jb	.L964
	cmpb	$72, 3(%r12)
	jne	.L597
.L601:
	movq	%r9, %r12
	jmp	.L592
.L950:
	movq	(%rsp), %rdx
	movl	$5, %ecx
	jmp	.L589
.L310:
	cmpl	$0, 32(%rsp)
	jne	.L7
.L1316:
	movq	8(%rsp), %rax
	movl	16(%rbp), %r14d
	movq	(%rax), %r12
	movl	(%r11), %eax
	movl	%eax, 24(%rsp)
	jmp	.L22
.L1409:
	cmpl	$363, %ecx
	je	.L786
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1402:
	movzbl	1(%rdx), %ecx
	jmp	.L872
.L751:
	cmpl	$65373, %ecx
	jbe	.L1455
	cmpl	$65505, %ecx
	je	.L765
	cmpl	$65509, %ecx
	je	.L766
	cmpl	$65504, %ecx
	jne	.L771
	movq	%rbx, %rdx
	jmp	.L739
.L103:
	cmpl	$40864, %edx
	jbe	.L1456
	cmpl	$65504, %edx
	je	.L125
	jbe	.L1457
	cmpl	$65507, %edx
	je	.L128
	cmpl	$65509, %edx
	je	.L129
	cmpl	$65505, %edx
	jne	.L136
	leaq	.LC3(%rip), %rcx
	jmp	.L78
.L1389:
	leaq	1(%rdx), %rcx
	jmp	.L31
.L734:
	cmpl	$8601, %ecx
	jbe	.L1458
	cmpl	$8869, %ecx
	je	.L747
	ja	.L748
	leal	-8725(%rcx), %edx
	cmpl	$82, %edx
	ja	.L771
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L803:
	leaq	.LC6(%rip), %rdx
	jmp	.L779
.L795:
	leaq	.LC22(%rip), %rdx
	jmp	.L779
.L1397:
	movl	%r8d, 16(%rsp)
	movq	%r15, %r12
	jmp	.L589
.L1447:
	leal	-9472(%rcx), %edx
	cmpl	$322, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1451:
	leal	-8560(%rcx), %edx
	cmpl	$9, %edx
	ja	.L729
	subl	$59, %ecx
	movl	$38, %r8d
	jmp	.L872
.L815:
	leaq	.LC12(%rip), %rdx
	jmp	.L779
.L797:
	leaq	.LC19(%rip), %rdx
	jmp	.L779
.L793:
	leaq	.LC20(%rip), %rdx
	jmp	.L779
.L1438:
	cmpl	$9632, %ecx
	je	.L807
	jbe	.L1459
	cmpl	$9650, %ecx
	je	.L810
	cmpl	$9651, %ecx
	je	.L811
	cmpl	$9633, %ecx
	jne	.L771
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	.LC13(%rip), %rdx
	jmp	.L779
.L784:
	leaq	.LC27(%rip), %rdx
	jmp	.L779
.L1390:
	subl	$33, %r9d
	cmpl	$86, %r9d
	ja	.L45
	cmpq	$1, %rcx
	jbe	.L33
	movzbl	185(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L45
	imull	$94, %r9d, %r9d
	addl	%r9d, %ecx
	cmpl	$8177, %ecx
	jg	.L45
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L45
.L46:
	cmpl	$65533, %r8d
	je	.L862
	leaq	2(%rdx), %rcx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L1448:
	cmpl	$19968, %ecx
	jnb	.L821
	cmpl	$12585, %ecx
	ja	.L822
	cmpl	$12288, %ecx
	jnb	.L823
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	.LC14(%rip), %rdx
	jmp	.L779
.L807:
	leaq	.LC17(%rip), %rdx
	jmp	.L779
.L826:
	leaq	.LC4(%rip), %rdx
	jmp	.L779
.L829:
	leaq	.LC2(%rip), %rdx
	jmp	.L779
.L830:
	leaq	.LC1(%rip), %rdx
	jmp	.L779
.L671:
	leal	-12832(%rcx), %edx
	cmpl	$9, %edx
	ja	.L685
.L1297:
	addl	$69, %ecx
	movl	$34, %r8d
	jmp	.L894
.L782:
	leaq	.LC32(%rip), %rdx
	jmp	.L779
.L974:
	leaq	.LC33(%rip), %rdx
	jmp	.L779
.L1355:
	leal	-13312(%rax), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rcx
	cmpb	$2, (%rcx)
	jne	.L887
	cmpq	$-1, %rcx
	jne	.L904
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L1404:
	cmpl	$275, %ecx
	je	.L974
	jbe	.L1460
	cmpl	$283, %ecx
	je	.L782
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L777:
	leaq	.LC30(%rip), %rdx
	jmp	.L779
.L818:
	leaq	.LC7(%rip), %rdx
	jmp	.L779
.L817:
	leaq	.LC8(%rip), %rdx
	jmp	.L779
.L813:
	leaq	.LC10(%rip), %rdx
	jmp	.L779
.L222:
	cmpl	$9792, %edx
	je	.L252
	ja	.L253
	cmpl	$9670, %edx
	je	.L254
	jbe	.L1461
	cmpl	$9678, %edx
	je	.L262
	jbe	.L1462
	cmpl	$9733, %edx
	je	.L266
	cmpl	$9734, %edx
	jne	.L1463
	leaq	.LC7(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L1450:
	leal	-711(%rcx), %edx
	cmpl	$18, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L810:
	leaq	.LC15(%rip), %rdx
	jmp	.L779
.L775:
	leaq	.LC23(%rip), %rdx
	jmp	.L779
.L789:
	leaq	.LC24(%rip), %rdx
	jmp	.L779
.L788:
	leaq	.LC25(%rip), %rdx
	jmp	.L779
.L786:
	leaq	.LC29(%rip), %rdx
	jmp	.L779
.L882:
	cmpl	$9249, %edx
	ja	.L180
	cmpl	$9216, %edx
	jnb	.L181
	cmpl	$8457, %edx
	je	.L182
	ja	.L183
	cmpl	$969, %edx
	jbe	.L1464
	cmpl	$8451, %edx
	je	.L921
	ja	.L189
	leal	-8211(%rdx), %ecx
	cmpl	$43, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rcx
.L217:
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	jne	.L865
.L220:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L1465
	cmpq	$0, 96(%rsp)
	je	.L288
	movl	24(%rsp), %eax
	andl	$248, %eax
	movl	%eax, (%r11)
	testb	$8, 16(%rbp)
	jne	.L1466
.L289:
	andl	$2, %ebx
	movq	144(%rsp), %rdx
	jne	.L1467
.L1315:
	cmpq	%r14, %rdx
	jne	.L286
.L293:
	movl	$6, 32(%rsp)
	jmp	.L7
.L1440:
	leal	-9472(%rcx), %edx
	cmpl	$75, %edx
	ja	.L685
.L1296:
	addl	$36, %ecx
	movl	$41, %r8d
	jmp	.L894
.L1443:
	leal	-164(%rcx), %edx
	cmpl	$93, %edx
	ja	.L685
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L623:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leal	-9312(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L1455:
	cmpl	$65281, %ecx
	jnb	.L762
	cmpl	$19968, %ecx
	jb	.L771
	cmpl	$40860, %ecx
	ja	.L1468
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %r8
	leal	-19968(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L1433:
	leal	-65281(%rcx), %edx
	cmpl	$93, %edx
	ja	.L685
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L672:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-12288(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L1458:
	cmpl	$8592, %ecx
	jnb	.L744
	cmpl	$8544, %ecx
	jb	.L771
	cmpl	$8553, %ecx
	ja	.L1469
	subl	$53, %ecx
	movl	$36, %r8d
	jmp	.L873
.L704:
	cmpl	$8895, %ecx
	leaq	.LC38(%rip), %rdx
	je	.L695
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	$65373, %edx
	jbe	.L1470
	cmpl	$65505, %edx
	je	.L214
	cmpl	$65509, %edx
	je	.L215
	cmpl	$65504, %edx
	jne	.L178
.L216:
	leaq	.LC36(%rip), %rcx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L791:
	cmpl	$8869, %ecx
	ja	.L799
	cmpl	$8451, %ecx
	jnb	.L800
	leal	-8213(%rcx), %edx
	cmpl	$38, %edx
	ja	.L771
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L141:
	cmpl	$8601, %edx
	jbe	.L1471
	cmpl	$8869, %edx
	je	.L196
	ja	.L155
	leal	-8725(%rdx), %ecx
	cmpl	$82, %ecx
	ja	.L178
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L670:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-19968(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L641:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-1025(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L1388:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %r12
	jb	.L33
	movzbl	185(%rsp), %esi
	cmpb	$36, %sil
	je	.L1472
	cmpb	$78, %sil
	jne	.L39
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r12
	jb	.L33
	movzbl	186(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$92, %ecx
	jbe	.L1473
.L41:
	cmpq	$0, 96(%rsp)
	je	.L293
	andl	$2, %ebx
	movl	$6, 32(%rsp)
	leaq	2(%rdx), %rcx
	je	.L7
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1387:
	cmpq	$0, 96(%rsp)
	movl	$6, 32(%rsp)
	je	.L7
	andl	$2, %ebx
	je	.L7
	movq	96(%rsp), %rdi
	leaq	1(%rdx), %rcx
	addq	$1, (%rdi)
	jmp	.L31
.L1431:
	cmpl	$913, %ecx
	jnb	.L736
	cmpl	$167, %ecx
	jb	.L771
	cmpl	$247, %ecx
	ja	.L1474
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leal	-167(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L696:
	cmpl	$8453, %ecx
	leaq	.LC41(%rip), %rdx
	je	.L695
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L1442:
	leal	-65072(%rcx), %edx
	cmpl	$59, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1444:
	cmpl	$9632, %edx
	je	.L106
	jbe	.L1475
	cmpl	$9650, %edx
	je	.L109
	cmpl	$9651, %edx
	je	.L110
	cmpl	$9633, %edx
	jne	.L136
	leaq	.LC16(%rip), %rcx
	jmp	.L78
.L1446:
	cmpl	$12288, %ecx
	jnb	.L754
	cmpl	$9312, %ecx
	jb	.L771
	cmpl	$9341, %ecx
	ja	.L1476
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-9312(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L649:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8451(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L681
.L648:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rdx
	je	.L779
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L1474:
	leal	-711(%rcx), %edx
	cmpl	$18, %edx
	ja	.L771
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L689:
	leaq	.LC40(%rip), %rdx
	jmp	.L695
.L688:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-9216(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L566:
	movq	168(%rsp), %rbx
	movq	160(%rsp), %rax
	movl	$6, 32(%rsp)
	jmp	.L341
.L180:
	cmpl	$13269, %edx
	ja	.L200
	cmpl	$13198, %edx
	jnb	.L201
	cmpl	$12329, %edx
	jbe	.L1477
	cmpl	$12585, %edx
	ja	.L206
	cmpl	$12549, %edx
	jnb	.L207
	cmpl	$12539, %edx
	leaq	.LC20(%rip), %rcx
	je	.L188
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$40864, %edx
	jbe	.L1478
	cmpl	$65504, %edx
	je	.L275
	jbe	.L1479
	cmpl	$65507, %edx
	je	.L278
	cmpl	$65509, %edx
	jne	.L1480
	leaq	.LC1(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L971:
	leaq	.LC42(%rip), %rdx
	jmp	.L695
.L1439:
	cmpl	$9671, %ecx
	je	.L815
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L710:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %r8
	leal	-12288(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1452:
	cmpl	$12288, %edx
	jnb	.L161
	cmpl	$9312, %edx
	jb	.L178
	cmpl	$9341, %edx
	ja	.L1481
	leal	-9312(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L1405:
	cmpl	$363, %ecx
	je	.L786
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	.LC39(%rip), %rdx
	jmp	.L695
.L1470:
	cmpl	$65281, %edx
	jnb	.L169
	cmpl	$19968, %edx
	jb	.L178
	cmpl	$40860, %edx
	ja	.L1482
	leal	-19968(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L225:
	cmpl	$1105, %edx
	ja	.L240
	cmpl	$1025, %edx
	jnb	.L241
	cmpl	$711, %edx
	je	.L242
	jbe	.L1483
	cmpl	$713, %edx
	je	.L246
	jb	.L220
	leal	-913(%rdx), %ecx
	cmpl	$56, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rcx
.L281:
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	je	.L220
	jmp	.L866
.L1392:
	cmpq	$1, %rcx
	jbe	.L33
	movzbl	185(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	ja	.L45
	imull	$94, %r9d, %r9d
	addl	%r9d, %ecx
	cmpl	$8690, %ecx
	jg	.L45
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L45
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L1456:
	cmpl	$19968, %edx
	jnb	.L120
	cmpl	$12585, %edx
	ja	.L121
	cmpl	$12288, %edx
	jnb	.L122
	cmpl	$9794, %edx
	leaq	.LC5(%rip), %rcx
	je	.L78
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L1434:
	cmpl	$275, %edx
	je	.L919
	jbe	.L1484
	cmpl	$283, %edx
	je	.L81
	cmpl	$299, %edx
	leaq	.LC31(%rip), %rcx
	je	.L78
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L722:
	leaq	.LC34(%rip), %rdx
	jmp	.L695
.L1471:
	cmpl	$8592, %edx
	jnb	.L151
	cmpl	$8544, %edx
	jb	.L178
	cmpl	$8553, %edx
	ja	.L1485
.L194:
	subl	$53, %edx
	movb	$36, 184(%rsp)
	movb	%dl, 185(%rsp)
.L218:
	movzbl	184(%rsp), %esi
	leaq	184(%rsp), %rcx
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L700:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8592(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1454:
	leaq	4(%r12), %r9
	cmpq	%r9, %r15
	jb	.L964
	movzbl	3(%r12), %ebx
	cmpb	$65, %bl
	je	.L600
	cmpb	$71, %bl
	jne	.L597
.L600:
	cmpb	$65, %bl
	movl	$32, %eax
	movl	$64, %edi
	cmovne	%edi, %eax
	movl	%eax, 36(%rsp)
	jmp	.L601
.L1469:
	leal	-8560(%rcx), %edx
	cmpl	$9, %edx
	ja	.L771
	subl	$59, %ecx
	movl	$38, %r8d
	jmp	.L873
.L718:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %r8
	leal	-65281(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L1468:
	leal	-65072(%rcx), %edx
	cmpl	$59, %edx
	ja	.L771
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L692:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-913(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L721:
	leaq	.LC35(%rip), %rdx
	jmp	.L695
.L90:
	cmpl	$8869, %edx
	ja	.L98
	cmpl	$8451, %edx
	jnb	.L99
	leal	-8213(%rdx), %ecx
	cmpl	$38, %ecx
	ja	.L136
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rcx,2), %rcx
.L131:
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	je	.L136
	movzbl	1(%rcx), %edx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L708:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %r8
	leal	-13198(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L724
.L714:
	addl	$66, %ecx
	movl	$37, %r8d
	jmp	.L872
.L713:
	cmpl	$12963, %ecx
	leaq	.LC37(%rip), %rdx
	je	.L695
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L1476:
	leal	-9472(%rcx), %edx
	cmpl	$322, %edx
	ja	.L771
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L1364:
	cmpl	$913, %edx
	jnb	.L143
	cmpl	$167, %edx
	jb	.L178
	cmpl	$247, %edx
	ja	.L1486
	leal	-167(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L1453:
	cmpl	$474, %ecx
	je	.L795
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L792:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-1025(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L1368:
	cmpl	$466, %edx
	jne	.L220
	leaq	.LC26(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L754:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %r8
	leal	-12288(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L800:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8451(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L799:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rdx
	je	.L779
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L1485:
	leal	-8560(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L178
.L1231:
	subl	$59, %edx
	movb	$38, 184(%rsp)
	movb	%dl, 185(%rsp)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L1486:
	leal	-711(%rdx), %ecx
	cmpl	$18, %ecx
	ja	.L178
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	.LC14(%rip), %rcx
	jmp	.L78
.L109:
	leaq	.LC15(%rip), %rcx
	jmp	.L78
.L1475:
	leal	-9472(%rdx), %ecx
	cmpl	$75, %ecx
	ja	.L136
	addl	$36, %edx
	movl	$41, %esi
	jmp	.L134
.L106:
	leaq	.LC17(%rip), %rcx
	jmp	.L78
.L93:
	cmpl	$713, %edx
	je	.L96
	jb	.L136
	leal	-913(%rdx), %ecx
	cmpl	$56, %ecx
	ja	.L136
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L92:
	leaq	.LC20(%rip), %rcx
	jmp	.L78
.L96:
	leaq	.LC19(%rip), %rcx
	jmp	.L78
.L91:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	leal	-1025(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L1483:
	cmpl	$474, %edx
	jne	.L1487
	leaq	.LC22(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L240:
	cmpl	$8869, %edx
	ja	.L248
	cmpl	$8451, %edx
	jnb	.L249
	leal	-8213(%rdx), %ecx
	cmpl	$38, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rcx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L732:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-9216(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L129:
	leaq	.LC1(%rip), %rcx
	jmp	.L78
.L1487:
	cmpl	$476, %edx
	leaq	.LC21(%rip), %rcx
	je	.L228
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L99:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	leal	-8451(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L98:
	cmpl	$8978, %edx
	leaq	.LC18(%rip), %rcx
	je	.L78
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	.LC4(%rip), %rcx
	jmp	.L78
.L128:
	leaq	.LC2(%rip), %rcx
	jmp	.L78
.L1457:
	leal	-65281(%rdx), %ecx
	cmpl	$93, %ecx
	ja	.L136
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L774:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leal	-9312(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L284:
	leaq	__PRETTY_FUNCTION__.8043(%rip), %rcx
	leaq	.LC53(%rip), %rsi
	leaq	.LC54(%rip), %rdi
	movl	$220, %edx
	call	__assert_fail@PLT
.L81:
	leaq	.LC32(%rip), %rcx
	jmp	.L78
.L1484:
	leal	-164(%rdx), %ecx
	cmpl	$93, %ecx
	ja	.L136
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rsi
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L919:
	leaq	.LC33(%rip), %rcx
	jmp	.L78
.L1414:
	movl	%r13d, (%rbx)
	jmp	.L18
.L1480:
	cmpl	$65505, %edx
	jne	.L220
	leaq	.LC3(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L147:
	cmpl	$8453, %edx
	leaq	.LC41(%rip), %rcx
	je	.L188
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L1403:
	leal	-13312(%rcx), %edx
	leaq	(%rdx,%rdx,2), %rdx
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdx
	cmpb	$2, (%rdx)
	jne	.L893
	cmpq	$-1, %rdx
	jne	.L905
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L762:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %r8
	leal	-65281(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L838:
	movq	%r10, 16(%rsp)
	movl	%r11d, 32(%rsp)
	leaq	176(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %rsi
	movq	%r15, %r8
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	72(%rsp), %rdi
	leaq	200(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	56(%rsp), %rsi
	popq	%rdx
	popq	%rcx
	movl	(%rsi), %edi
	movl	32(%rsp), %r11d
	movq	16(%rsp), %r10
	movl	%edi, %esi
	andl	$224, %edi
	andl	$24, %esi
	cmpl	$6, %eax
	je	.L839
	cmpl	$5, %eax
	movq	176(%rsp), %r12
	jne	.L851
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movl	$5, %ecx
	movq	184(%rsp), %rdx
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L740:
	cmpl	$8453, %ecx
	leaq	.LC41(%rip), %rdx
	je	.L739
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L1449:
	leal	-65281(%rcx), %edx
	cmpl	$93, %edx
	ja	.L771
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L1481:
	leal	-9472(%rdx), %ecx
	cmpl	$322, %ecx
	ja	.L178
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L1435:
	cmpl	$363, %edx
	je	.L85
	cmpl	$462, %edx
	leaq	.LC28(%rip), %rcx
	je	.L78
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	.LC27(%rip), %rcx
	jmp	.L78
.L85:
	leaq	.LC29(%rip), %rcx
	jmp	.L78
.L1478:
	cmpl	$19968, %edx
	jnb	.L270
	cmpl	$12585, %edx
	ja	.L271
	cmpl	$12288, %edx
	jnb	.L272
	cmpl	$9794, %edx
	leaq	.LC5(%rip), %rcx
	je	.L228
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L949:
	movl	$4, %ecx
	jmp	.L589
.L1437:
	movzbl	3(%r12), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L603
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$7649, %eax
	jg	.L603
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rdi
	cltq
	movq	%rbx, %r12
	movzwl	(%rdi,%rax,2), %edi
	testw	%di, %di
	je	.L603
	cmpl	$65533, %edi
	jne	.L604
	movq	%rbx, %r9
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L765:
	leaq	.LC35(%rip), %rdx
	jmp	.L739
.L747:
	leaq	.LC39(%rip), %rdx
	jmp	.L739
.L733:
	leaq	.LC40(%rip), %rdx
	jmp	.L739
.L748:
	cmpl	$8895, %ecx
	leaq	.LC38(%rip), %rdx
	je	.L739
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L744:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8592(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L1460:
	leal	-164(%rcx), %edx
	cmpl	$93, %edx
	ja	.L771
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L73:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	leal	-9312(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L155:
	cmpl	$8895, %edx
	leaq	.LC38(%rip), %rcx
	je	.L188
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC30(%rip), %rcx
	jmp	.L78
.L122:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	leal	-12288(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L121:
	leal	-12832(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L136
	addl	$69, %edx
	movl	$34, %esi
	jmp	.L134
.L120:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rsi
	leal	-19968(%rdx), %ecx
	leaq	(%rsi,%rcx,2), %rcx
	jmp	.L131
.L1366:
	cmpl	$275, %edx
	je	.L922
	jbe	.L1488
	cmpl	$283, %edx
	jne	.L1489
	leaq	.LC32(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L822:
	leal	-12832(%rcx), %edx
	cmpl	$9, %edx
	jbe	.L1297
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L1482:
	leal	-65072(%rdx), %ecx
	cmpl	$59, %ecx
	ja	.L178
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rcx
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L821:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-19968(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L1489:
	cmpl	$299, %edx
	leaq	.LC31(%rip), %rcx
	je	.L228
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L1464:
	cmpl	$913, %edx
	jnb	.L185
	cmpl	$167, %edx
	jb	.L220
	cmpl	$247, %edx
	ja	.L1490
	leal	-167(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L183:
	cmpl	$8601, %edx
	jbe	.L1491
	cmpl	$8869, %edx
	je	.L196
	ja	.L197
	leal	-8725(%rdx), %ecx
	cmpl	$82, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L1490:
	leal	-711(%rdx), %ecx
	cmpl	$18, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L1491:
	cmpl	$8592, %edx
	jnb	.L193
	cmpl	$8544, %edx
	jb	.L220
	cmpl	$8553, %edx
	jbe	.L194
	leal	-8560(%rdx), %ecx
	cmpl	$9, %ecx
	jbe	.L1231
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L758:
	addl	$66, %ecx
	movl	$37, %r8d
	jmp	.L873
.L1459:
	leal	-9472(%rcx), %edx
	cmpl	$75, %edx
	jbe	.L1296
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	.LC23(%rip), %rcx
	jmp	.L78
.L1472:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, %r12
	jb	.L33
	movzbl	186(%rsp), %esi
	cmpb	$41, %sil
	movb	%sil, 16(%rsp)
	je	.L1492
	cmpb	$42, 16(%rsp)
	jne	.L38
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r12
	jb	.L33
.L38:
	cmpb	$42, 16(%rsp)
	jne	.L39
	cmpb	$72, 187(%rsp)
	jne	.L39
.L914:
	leaq	4(%rdx), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	.LC13(%rip), %rcx
	jmp	.L78
.L757:
	cmpl	$12963, %ecx
	leaq	.LC37(%rip), %rdx
	je	.L739
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L752:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %r8
	leal	-13198(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L94:
	leaq	.LC22(%rip), %rcx
	jmp	.L78
.L1461:
	cmpl	$9632, %edx
	je	.L256
	jbe	.L1493
	cmpl	$9650, %edx
	je	.L259
	cmpl	$9651, %edx
	jne	.L1494
	leaq	.LC14(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L736:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-913(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L768
.L823:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-12288(%rcx), %edx
	leaq	(%r8,%rdx,2), %rdx
	jmp	.L832
.L973:
	leaq	.LC42(%rip), %rdx
	jmp	.L739
.L88:
	leaq	.LC24(%rip), %rcx
	jmp	.L78
.L87:
	leaq	.LC25(%rip), %rcx
	jmp	.L78
.L1463:
	cmpl	$9679, %edx
	jne	.L220
	leaq	.LC9(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L1477:
	cmpl	$12288, %edx
	jnb	.L203
	cmpl	$9312, %edx
	jb	.L220
	cmpl	$9341, %edx
	ja	.L1495
	leal	-9312(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L200:
	cmpl	$65373, %edx
	jbe	.L1496
	cmpl	$65505, %edx
	je	.L214
	cmpl	$65509, %edx
	je	.L215
	cmpl	$65504, %edx
	jne	.L220
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r12
	je	.L1497
	movq	%rcx, %rsi
	subq	%r14, %rsi
	addq	%rsi, %rax
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	movl	24(%rsp), %eax
	andl	$-8, %eax
	movslq	%eax, %rsi
	cmpq	%rsi, %rcx
	jle	.L1498
	cmpq	$4, %rcx
	ja	.L1499
	orl	%ecx, %eax
	movl	%eax, (%r11)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L59
	movb	%dil, 4(%r11,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L59
.L1500:
	movzbl	(%rdx,%rax), %edi
	movb	%dil, 4(%r11,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L1500
	jmp	.L59
.L1495:
	leal	-9472(%rdx), %ecx
	cmpl	$322, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L1494:
	cmpl	$9633, %edx
	jne	.L220
	leaq	.LC16(%rip), %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	.LC7(%rip), %rcx
	jmp	.L78
.L116:
	leaq	.LC8(%rip), %rcx
	jmp	.L78
.L1445:
	cmpl	$9671, %edx
	je	.L114
	cmpl	$9675, %edx
	leaq	.LC11(%rip), %rcx
	je	.L78
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	.LC10(%rip), %rcx
	jmp	.L78
.L114:
	leaq	.LC12(%rip), %rcx
	jmp	.L78
.L102:
	leaq	.LC6(%rip), %rcx
	jmp	.L78
.L766:
	leaq	.LC34(%rip), %rdx
	jmp	.L739
.L1496:
	cmpl	$65281, %edx
	jnb	.L211
	cmpl	$19968, %edx
	jb	.L220
	cmpl	$40860, %edx
	ja	.L1501
	leal	-19968(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rcx
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L1501:
	leal	-65072(%rdx), %ecx
	cmpl	$59, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rcx
	jmp	.L217
.L211:
	leal	-65281(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rcx
	jmp	.L217
.L862:
	cmpq	$0, 96(%rsp)
	je	.L981
	andb	$2, %bl
	leaq	2(%rdx), %rcx
	je	.L31
	jmp	.L896
.L266:
	leaq	.LC8(%rip), %rcx
	jmp	.L228
.L206:
	cmpl	$12963, %edx
	leaq	.LC37(%rip), %rcx
	je	.L188
	jmp	.L220
.L981:
	leaq	2(%rdx), %rcx
	jmp	.L31
.L201:
	leal	-13198(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rcx
	jmp	.L217
.L259:
	leaq	.LC15(%rip), %rcx
	jmp	.L228
.L1493:
	leal	-9472(%rdx), %ecx
	cmpl	$75, %ecx
	ja	.L220
	addl	$36, %edx
	movb	$41, 118(%rsp)
	movb	%dl, 119(%rsp)
.L282:
	movzbl	118(%rsp), %esi
	leaq	118(%rsp), %rcx
	jmp	.L866
.L256:
	leaq	.LC17(%rip), %rcx
	jmp	.L228
.L1393:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L1411:
	leaq	__PRETTY_FUNCTION__.9405(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L1422:
	leaq	__PRETTY_FUNCTION__.9405(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC58(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L1421:
	leaq	__PRETTY_FUNCTION__.9405(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L1492:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r12
	jb	.L33
	movzbl	187(%rsp), %esi
	cmpb	$65, %sil
	je	.L914
	cmpb	$71, %sil
	jne	.L39
	jmp	.L914
.L1473:
	movzbl	187(%rsp), %esi
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L41
	imull	$94, %ecx, %ecx
	addl	%esi, %ecx
	cmpl	$7649, %ecx
	jg	.L41
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r8d
	testw	%r8w, %r8w
	je	.L41
	cmpl	$65533, %r8d
	je	.L42
	leaq	4(%rdx), %rcx
	jmp	.L43
.L185:
	leal	-913(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rcx
	jmp	.L217
.L197:
	cmpl	$8895, %edx
	leaq	.LC38(%rip), %rcx
	je	.L188
	jmp	.L220
.L42:
	cmpq	$0, 96(%rsp)
	je	.L981
	andb	$2, %bl
	leaq	2(%rdx), %rcx
	je	.L31
	leaq	4(%rdx), %rcx
	jmp	.L1307
.L1488:
	leal	-164(%rdx), %ecx
	cmpl	$93, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rcx
	jmp	.L281
.L922:
	leaq	.LC33(%rip), %rcx
	jmp	.L228
.L54:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L193:
	leal	-8592(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rcx
	jmp	.L217
.L226:
	leaq	.LC30(%rip), %rcx
	jmp	.L228
.L169:
	leal	-65281(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rcx
	jmp	.L175
.L1367:
	cmpl	$363, %edx
	je	.L235
	cmpl	$462, %edx
	leaq	.LC28(%rip), %rcx
	je	.L228
	jmp	.L220
.L233:
	leaq	.LC27(%rip), %rcx
	jmp	.L228
.L235:
	leaq	.LC29(%rip), %rcx
	jmp	.L228
.L271:
	leal	-12832(%rdx), %ecx
	cmpl	$9, %ecx
	ja	.L220
	addl	$69, %edx
	movb	$34, 118(%rsp)
	movb	%dl, 119(%rsp)
	jmp	.L282
.L270:
	leal	-19968(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rcx
	jmp	.L281
.L272:
	leal	-12288(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rcx
	jmp	.L281
.L841:
	movq	96(%rsp), %rax
	addq	$4, %r12
	movq	%r12, 176(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L851
.L181:
	leal	-9216(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rcx
	jmp	.L217
.L252:
	leaq	.LC6(%rip), %rcx
	jmp	.L228
.L139:
	leal	-9216(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rcx
	jmp	.L175
.L1371:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L1466:
	movq	(%rsp), %rax
	movq	%r10, 24(%rsp)
	leaq	144(%rsp), %rcx
	movq	%r11, 16(%rsp)
	subq	$8, %rsp
	movq	%rbp, %rsi
	pushq	104(%rsp)
	movq	72(%rsp), %rdi
	leaq	(%r14,%rax), %r12
	movq	24(%rsp), %rax
	leaq	168(%rsp), %r9
	movq	%r12, %r8
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 48(%rsp)
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	movq	16(%rsp), %r11
	movq	24(%rsp), %r10
	je	.L289
	cmpl	$5, %eax
	movq	144(%rsp), %rdx
	je	.L1314
	cmpq	%r14, %rdx
	jne	.L286
	cmpl	$7, 32(%rsp)
	jne	.L310
	leaq	4(%r14), %rax
	cmpq	%rax, %r12
	je	.L1502
	movl	(%r11), %eax
	movq	(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %rdi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jle	.L1503
	cmpq	$4, (%rsp)
	ja	.L1504
	movq	(%rsp), %rsi
	orl	%esi, %eax
	testq	%rsi, %rsi
	movl	%eax, (%r11)
	je	.L59
	xorl	%eax, %eax
.L314:
	movzbl	(%r14,%rax), %edx
	movb	%dl, 4(%r11,%rax)
	addq	$1, %rax
	cmpq	%rax, (%rsp)
	jne	.L314
	jmp	.L59
.L288:
	movq	144(%rsp), %rdx
	jmp	.L1315
.L1504:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC51(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L1503:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1502:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L1465:
	movq	144(%rsp), %rdi
	leaq	4(%rdi), %rdx
	cmpq	%r14, %rdx
	movq	%rdx, 144(%rsp)
	jne	.L286
	movq	%rax, %r12
	movl	(%r11), %eax
	movl	%ebx, %r14d
	movl	%eax, 24(%rsp)
	jmp	.L22
.L189:
	cmpl	$8453, %edx
	leaq	.LC41(%rip), %rcx
	je	.L188
	jmp	.L220
.L254:
	leaq	.LC13(%rip), %rcx
	jmp	.L228
.L182:
	leaq	.LC40(%rip), %rcx
	jmp	.L188
.L1462:
	cmpl	$9671, %edx
	je	.L264
	cmpl	$9675, %edx
	leaq	.LC11(%rip), %rcx
	je	.L228
	jmp	.L220
.L262:
	leaq	.LC10(%rip), %rcx
	jmp	.L228
.L264:
	leaq	.LC12(%rip), %rcx
	jmp	.L228
.L203:
	leal	-12288(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rcx
	jmp	.L217
.L1499:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC51(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L1498:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1497:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L275:
	leaq	.LC4(%rip), %rcx
	jmp	.L228
.L196:
	leaq	.LC39(%rip), %rcx
	jmp	.L188
.L278:
	leaq	.LC2(%rip), %rcx
	jmp	.L228
.L1479:
	leal	-65281(%rdx), %ecx
	cmpl	$93, %ecx
	ja	.L220
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rcx
	jmp	.L281
.L837:
	movl	%edi, 36(%rsp)
	movl	%esi, 16(%rsp)
	movl	$6, %ecx
	movq	184(%rsp), %rdx
	movq	176(%rsp), %r12
	jmp	.L613
.L1467:
	movq	96(%rsp), %rax
	addq	$4, %rdx
	movq	%rdx, 144(%rsp)
	addq	$1, (%rax)
	cmpq	%r14, %rdx
	jne	.L286
	jmp	.L293
.L1391:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L161:
	leal	-12288(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rcx
	jmp	.L175
.L223:
	leal	-9312(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rcx
	jmp	.L281
.L1365:
	cmpq	$-1, %rcx
	jne	.L903
	jmp	.L880
.L1369:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L214:
	leaq	.LC35(%rip), %rcx
	jmp	.L188
.L215:
	leaq	.LC34(%rip), %rcx
	jmp	.L188
.L844:
	leaq	__PRETTY_FUNCTION__.9305(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC55(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L609:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L1360:
	leaq	__PRETTY_FUNCTION__.9405(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L57:
	leaq	__PRETTY_FUNCTION__.9327(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L857:
	leaq	__PRETTY_FUNCTION__.9405(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC59(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L237:
	leaq	.LC25(%rip), %rcx
	jmp	.L228
.L921:
	leaq	.LC42(%rip), %rcx
	jmp	.L188
.L151:
	leal	-8592(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rcx
	jmp	.L175
.L143:
	leal	-913(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rcx
	jmp	.L175
.L246:
	leaq	.LC19(%rip), %rcx
	jmp	.L228
.L242:
	leaq	.LC20(%rip), %rcx
	jmp	.L228
.L249:
	leal	-8451(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rcx
	jmp	.L281
.L248:
	cmpl	$8978, %edx
	leaq	.LC18(%rip), %rcx
	je	.L228
	jmp	.L220
.L241:
	leal	-1025(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rcx
	jmp	.L281
.L224:
	leaq	.LC23(%rip), %rcx
	jmp	.L228
.L25:
	leaq	__PRETTY_FUNCTION__.9237(%rip), %rcx
	leaq	.LC45(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L159:
	leal	-13198(%rdx), %ecx
	addq	%rcx, %rcx
	addq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rcx
	jmp	.L175
.L164:
	cmpl	$12963, %edx
	leaq	.LC37(%rip), %rcx
	je	.L188
	jmp	.L178
.L207:
	addl	$66, %edx
	movb	$37, 184(%rsp)
	movb	%dl, 185(%rsp)
	jmp	.L218
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9305, @object
	.size	__PRETTY_FUNCTION__.9305, 18
__PRETTY_FUNCTION__.9305:
	.string	"to_iso2022cn_loop"
	.align 16
	.type	__PRETTY_FUNCTION__.9216, @object
	.size	__PRETTY_FUNCTION__.9216, 20
__PRETTY_FUNCTION__.9216:
	.string	"from_iso2022cn_loop"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8043, @object
	.size	__PRETTY_FUNCTION__.8043, 15
__PRETTY_FUNCTION__.8043:
	.string	"ucs4_to_gb2312"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.9327, @object
	.size	__PRETTY_FUNCTION__.9327, 25
__PRETTY_FUNCTION__.9327:
	.string	"to_iso2022cn_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9237, @object
	.size	__PRETTY_FUNCTION__.9237, 27
__PRETTY_FUNCTION__.9237:
	.string	"from_iso2022cn_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9405, @object
	.size	__PRETTY_FUNCTION__.9405, 6
__PRETTY_FUNCTION__.9405:
	.string	"gconv"
