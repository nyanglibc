	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	testb	%sil, %sil
	movzbl	%sil, %eax
	js	.L2
.L3:
	leal	-65(%rax), %edx
	cmpl	$367, %edx
	movl	$-1, %edx
	cmovbe	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$-128, %eax
	leaq	to_ucs4(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	testl	%eax, %eax
	jne	.L3
	movl	$-1, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CP1258//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L8
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L11
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r10
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 48(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 32(%rsp)
	movl	%r11d, %edx
	movq	%r9, 56(%rsp)
	movl	224(%rsp), %r12d
	andl	$1, %edx
	movq	%rdi, 80(%rsp)
	movq	$0, 24(%rsp)
	jne	.L13
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L13
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L13:
	testl	%r12d, %r12d
	jne	.L336
	movq	16(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	128(%rsp), %rdx
	movq	32(%rbx), %r15
	movl	232(%rsp), %ebp
	movq	8(%rbx), %r14
	testq	%rdi, %rdi
	movq	(%rax), %r12
	movq	%rdi, %rax
	cmove	%rbx, %rax
	cmpq	$0, 56(%rsp)
	movl	(%r15), %esi
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 128(%rsp)
	movl	%esi, 64(%rsp)
	cmovne	%rdx, %rax
	testl	%ebp, %ebp
	movq	%rax, 96(%rsp)
	movq	48(%rsp), %rax
	setne	123(%rsp)
	movzbl	123(%rsp), %ecx
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L28
	testb	%cl, %cl
	je	.L28
	andl	$7, %esi
	jne	.L337
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%r10, %rdi
	testq	%rax, %rax
	movq	%r14, %r10
	movl	%r11d, 40(%rsp)
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rdi, %r12
	je	.L338
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	152(%rsp), %rdi
	leaq	144(%rsp), %r11
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rbp
	movq	%r13, %rax
	movl	$4, 8(%rsp)
	movq	%rdi, 88(%rsp)
.L95:
	cmpq	%rax, %r12
	je	.L96
.L116:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r12
	jb	.L214
	cmpq	%rbp, %r10
	jbe	.L221
	movl	(%rax), %edx
	leal	-160(%rdx), %esi
	cmpl	$34, %esi
	jbe	.L248
	cmpl	$127, %edx
	jbe	.L248
	leal	-196(%rdx), %esi
	cmpl	$77, %esi
	ja	.L100
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %ecx
.L101:
	testb	%cl, %cl
	je	.L108
.L107:
	leaq	1(%rbp), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, 0(%rbp)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbp
	addq	$4, %rax
	cmpq	%rax, %r12
	movq	%rax, 144(%rsp)
	jne	.L116
	.p2align 4,,10
	.p2align 3
.L96:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L339
.L117:
	addl	$1, 20(%rbx)
	testb	$1, 16(%rbx)
	jne	.L340
	cmpq	%rbp, %r14
	jnb	.L224
	movq	24(%rsp), %rdi
	movq	(%rbx), %rax
	movq	%r10, 40(%rsp)
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbp, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%rdi
	movq	40(%rsp), %r10
	je	.L121
	movq	136(%rsp), %rax
	cmpq	%rbp, %rax
	movq	%rax, 8(%rsp)
	jne	.L341
.L120:
	testl	%r11d, %r11d
	jne	.L242
.L176:
	movq	16(%rsp), %rdi
	movq	48(%rsp), %rax
	movq	(%rbx), %r14
	movq	(%rdi), %r13
	movl	(%r15), %edi
	movq	96(%rax), %rax
	movl	%edi, 64(%rsp)
	movl	16(%rbx), %edi
	testq	%rax, %rax
	movl	%edi, 40(%rsp)
	jne	.L70
.L338:
	cmpq	%r13, %r12
	je	.L205
	leaq	4(%r14), %rsi
	movq	%r13, %rcx
	movq	%r14, %rbp
	cmpq	%rsi, %r10
	jb	.L331
	leaq	to_ucs4(%rip), %r11
	movq	%rbx, 88(%rsp)
	movl	$4, 8(%rsp)
	movq	%r13, %rbx
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	(%rcx), %eax
	cmpl	$127, %eax
	jbe	.L73
	addl	$-128, %eax
	movzwl	(%r11,%rax,2), %eax
	testl	%eax, %eax
	je	.L342
.L73:
	movl	(%r15), %edx
	leal	-65(%rax), %edi
	sarl	$3, %edx
	testl	%edx, %edx
	je	.L75
	leal	-768(%rax), %r8d
	cmpl	$63, %r8d
	ja	.L76
	cmpl	$35, %r8d
	ja	.L77
	leaq	.L79(%rip), %r9
	movslq	(%r9,%r8,4), %r8
	addq	%r9, %r8
	jmp	*%r8
	.section	.rodata
	.align 4
	.align 4
.L79:
	.long	.L78-.L79
	.long	.L80-.L79
	.long	.L77-.L79
	.long	.L81-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L82-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L210-.L79
	.text
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	1(%rbp), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, 0(%rbp)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L100:
	leal	-338(%rdx), %esi
	cmpl	$94, %esi
	ja	.L102
	leal	-260(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L107
.L108:
	leal	-192(%rdx), %ecx
	cmpl	$7982, %ecx
	jbe	.L343
.L110:
	cmpq	$0, 96(%rsp)
	je	.L223
	testb	$8, 16(%rbx)
	jne	.L344
.L114:
	testb	$2, 40(%rsp)
	jne	.L345
.L223:
	movl	$6, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L102:
	leal	-710(%rdx), %esi
	cmpl	$22, %esi
	ja	.L103
	leal	-537(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L214:
	movl	$7, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$367, %edi
	ja	.L91
.L90:
	sall	$3, %eax
	movl	%eax, (%r15)
.L92:
	addq	$1, %rcx
.L74:
	cmpq	%rcx, %r12
	je	.L346
.L93:
	leaq	4(%rbp), %rsi
	cmpq	%rsi, %r10
	jnb	.L72
	movq	%rbx, %r13
	movq	88(%rsp), %rbx
.L331:
	movl	$5, 8(%rsp)
.L71:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L117
.L339:
	movq	32(%rsp), %rax
	movq	%rbp, (%rax)
.L12:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$64, %edx
	ja	.L84
	.p2align 4,,10
	.p2align 3
.L311:
	movl	%edx, 0(%rbp)
	movl	$0, (%r15)
.L185:
	addq	$8, %rbp
	cmpq	%rbp, %r10
	movq	%rsi, %rbp
	jb	.L74
.L91:
	movl	%eax, 0(%rbp)
	addq	$4, %rbp
	jmp	.L92
.L319:
	movl	104(%rsp), %eax
	movl	112(%rsp), %edi
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$367, %edi
	movl	%edx, 0(%rbp)
	movl	$0, (%r15)
	ja	.L185
	movq	%rsi, %rbp
	jmp	.L90
.L359:
	leaq	decomp_table(%rip), %rsi
	movl	%r8d, %ecx
	movzwl	(%rsi,%rcx,4), %ecx
	cmpl	%ecx, %edx
	jne	.L110
	movl	%r8d, %ecx
.L111:
	leaq	1(%rbp), %rdx
	cmpq	%rdx, %r10
	ja	.L347
	.p2align 4,,10
	.p2align 3
.L221:
	movl	$5, 8(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L121:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L120
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L103:
	leal	-768(%rdx), %esi
	cmpl	$35, %esi
	jbe	.L348
	leal	-832(%rdx), %esi
	cmpl	$1, %esi
	jbe	.L349
	leal	-8211(%rdx), %esi
	cmpl	$39, %esi
	jbe	.L350
	cmpl	$8363, %edx
	je	.L216
	cmpl	$8364, %edx
	je	.L217
	cmpl	$8482, %edx
	je	.L218
	movl	%edx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	jne	.L108
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$197, %r9d
	movl	$148, 104(%rsp)
.L83:
	cmpl	$64, %edx
	jbe	.L311
	leaq	comp_table_data(%rip), %r13
	movl	%r9d, %r8d
	movzwl	0(%r13,%r8,4), %r8d
	cmpl	%r8d, %edx
	ja	.L76
	movl	104(%rsp), %r13d
	movl	%edi, 112(%rsp)
	movl	%eax, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L86:
	leal	0(%r13,%r9), %eax
	leaq	comp_table_data(%rip), %r8
	shrl	%eax
	movl	%eax, %edi
	movzwl	(%r8,%rdi,4), %r8d
	cmpl	%r8d, %edx
	je	.L87
	jnb	.L88
	cmpl	%eax, %r13d
	je	.L319
	movl	%eax, %r9d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$147, %r9d
	movl	$124, 104(%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$123, %r9d
	movl	$90, 104(%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$30, %r9d
	movl	$0, 104(%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L348:
	leal	-572(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	%eax, %r13d
	je	.L351
	movl	%eax, %r13d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L340:
	movq	56(%rsp), %rcx
	movq	%rbp, (%rbx)
	movq	%r12, %r10
	movq	128(%rsp), %rax
	addq	%rax, (%rcx)
.L119:
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 123(%rsp)
	je	.L12
	cmpl	$7, 8(%rsp)
	jne	.L12
	movq	16(%rsp), %rax
	movq	%r10, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L179
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbx), %rcx
	je	.L181
.L180:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L180
.L181:
	movq	16(%rsp), %rax
	movq	%r10, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L342:
	cmpq	$0, 96(%rsp)
	je	.L209
	testb	$2, 40(%rsp)
	jne	.L352
.L209:
	movq	%rbx, %r13
	movl	$6, 8(%rsp)
	movq	88(%rsp), %rbx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L224:
	movl	8(%rsp), %r11d
	jmp	.L120
.L351:
	leaq	comp_table_data(%rip), %r8
	movl	104(%rsp), %eax
	movl	112(%rsp), %edi
	movzwl	(%r8,%r9,4), %r8d
	cmpl	%r8d, %edx
	jne	.L76
	movq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	comp_table_data(%rip), %rax
	addq	$1, %rcx
	cmpq	%rcx, %r12
	movzwl	2(%rax,%rdi,4), %eax
	movl	%eax, 0(%rbp)
	movl	$0, (%r15)
	movq	%rsi, %rbp
	jne	.L93
	.p2align 4,,10
	.p2align 3
.L346:
	movq	%rbx, %r13
	movq	88(%rsp), %rbx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L341:
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	movl	64(%rsp), %eax
	movl	%eax, (%r15)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L353
	leaq	152(%rsp), %rdi
	leaq	144(%rsp), %rsi
	movl	16(%rbx), %ebp
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%rdi, 40(%rsp)
	movq	%rsi, 64(%rsp)
.L153:
	cmpq	%r13, %r12
	je	.L354
.L174:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %r12
	jb	.L230
	cmpq	%rdx, 8(%rsp)
	jbe	.L239
	movl	0(%r13), %ecx
	leal	-160(%rcx), %edi
	cmpl	$34, %edi
	jbe	.L249
	cmpl	$127, %ecx
	jbe	.L249
	leal	-196(%rcx), %edi
	cmpl	$77, %edi
	ja	.L158
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rdi), %esi
.L159:
	testb	%sil, %sil
	je	.L166
.L165:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r13
	cmpq	%r13, %r12
	movq	%r13, 144(%rsp)
	jne	.L174
.L354:
	cltq
	movq	%r12, %r13
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	$432, %edx
	ja	.L311
	movl	$89, %r9d
	movl	$31, %r13d
	movl	%eax, 104(%rsp)
	movl	%edi, 112(%rsp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L337:
	testq	%rdi, %rdi
	jne	.L355
	cmpl	$4, %esi
	movq	%r12, 144(%rsp)
	movq	%r13, 152(%rsp)
	ja	.L30
	leaq	136(%rsp), %rcx
	movslq	%esi, %rax
	xorl	%ebp, %ebp
	movq	%rcx, 40(%rsp)
.L31:
	movzbl	4(%r15,%rbp), %edx
	movb	%dl, (%rcx,%rbp)
	addq	$1, %rbp
	cmpq	%rbp, %rax
	jne	.L31
	movq	%r12, %rax
	subq	%rbp, %rax
	addq	$4, %rax
	cmpq	%rax, %r10
	jb	.L356
	cmpq	%r14, %r13
	jnb	.L203
	leaq	1(%r12), %rax
	leaq	135(%rsp), %rsi
.L39:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbp
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbp
	movb	%dl, (%rsi,%rbp)
	ja	.L246
	cmpq	%rcx, %r10
	ja	.L39
.L246:
	movq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	movl	136(%rsp), %eax
	leal	-160(%rax), %edx
	cmpl	$34, %edx
	jbe	.L247
	cmpl	$127, %eax
	jbe	.L247
	leal	-196(%rax), %edx
	cmpl	$77, %edx
	jbe	.L329
	leal	-338(%rax), %edx
	cmpl	$94, %edx
	ja	.L47
	leal	-260(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L46:
	testb	%dl, %dl
	je	.L53
.L52:
	leaq	1(%r13), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, 0(%r13)
.L330:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L44
.L43:
	movl	(%r15), %edx
	subq	40(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L357
	subq	%rcx, %rax
	movq	16(%rsp), %rcx
	andl	$-8, %edx
	movq	152(%rsp), %r13
	movl	%edx, 64(%rsp)
	movl	16(%rbx), %r11d
	addq	(%rcx), %rax
	movq	%rax, (%rcx)
	movq	%rax, %r12
	movq	48(%rsp), %rax
	movl	%edx, (%r15)
	movq	96(%rax), %rax
	jmp	.L28
.L350:
	leal	-7979(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L101
.L343:
	movl	$7808, %esi
	movl	$198, %r8d
	xorl	%edi, %edi
	movl	$198, %ecx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L358:
	cmpl	%ecx, %edi
	je	.L110
	movl	%ecx, %r8d
.L113:
	leal	(%r8,%rdi), %ecx
	leaq	decomp_table(%rip), %r9
	movl	%ecx, %esi
	shrl	%esi
	movzwl	(%r9,%rsi,4), %esi
.L109:
	shrl	%ecx
	cmpl	%esi, %edx
	je	.L111
	jb	.L358
	cmpl	%ecx, %edi
	je	.L359
	movl	%ecx, %edi
	jmp	.L113
.L353:
	movl	16(%rbx), %eax
	cmpq	%r13, %r12
	movl	%eax, 104(%rsp)
	je	.L360
	leaq	4(%r14), %r9
	cmpq	%r9, 8(%rsp)
	movq	%r14, %rbp
	movl	$4, 64(%rsp)
	jb	.L361
	movq	8(%rsp), %r8
	movq	%r10, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	0(%r13), %eax
	cmpl	$127, %eax
	jbe	.L130
	leaq	to_ucs4(%rip), %rsi
	addl	$-128, %eax
	movzwl	(%rsi,%rax,2), %eax
	testl	%eax, %eax
	je	.L362
.L130:
	movl	(%r15), %edx
	leal	-65(%rax), %ecx
	sarl	$3, %edx
	testl	%edx, %edx
	je	.L134
	leal	-768(%rax), %esi
	cmpl	$63, %esi
	ja	.L135
	cmpl	$35, %esi
	ja	.L77
	leaq	.L137(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L137:
	.long	.L136-.L137
	.long	.L138-.L137
	.long	.L77-.L137
	.long	.L139-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L140-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L77-.L137
	.long	.L226-.L137
	.text
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
.L332:
	movb	%cl, (%rdx)
	movq	144(%rsp), %rcx
	movq	152(%rsp), %rdx
	leaq	4(%rcx), %r13
	movq	%r13, 144(%rsp)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L134:
	cmpl	$367, %ecx
	ja	.L149
.L148:
	sall	$3, %eax
	movl	%eax, (%r15)
.L150:
	addq	$1, %r13
.L133:
	cmpq	%r13, %r12
	je	.L363
	leaq	4(%rbp), %r9
	cmpq	%r9, %r8
	jnb	.L127
	movq	8(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	40(%rsp), %r10
	cmpq	%rdx, %rbp
	movq	%r13, (%rax)
	je	.L129
.L126:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	cmpl	$64, %edx
	jbe	.L312
	cmpl	$432, %edx
	jbe	.L364
.L312:
	movl	%edx, 0(%rbp)
	movl	$0, (%r15)
.L188:
	addq	$8, %rbp
	cmpq	%rbp, %r8
	movq	%r9, %rbp
	jb	.L133
.L149:
	movl	%eax, 0(%rbp)
	addq	$4, %rbp
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%r10, 112(%rsp)
	subq	$8, %rsp
	movq	%r12, %r8
	pushq	104(%rsp)
	movq	32(%rsp), %rax
	movq	%r11, %rcx
	movq	104(%rsp), %r9
	movq	64(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r11, 120(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	104(%rsp), %r11
	movq	112(%rsp), %r10
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbp
	je	.L114
	cmpl	$5, 8(%rsp)
	jne	.L95
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L158:
	leal	-338(%rcx), %edi
	cmpl	$94, %edi
	ja	.L160
	leal	-260(%rcx), %esi
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	jmp	.L159
.L345:
	movq	96(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L95
.L349:
	leal	-636(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L101
.L352:
	movq	96(%rsp), %rax
	addq	$1, %rcx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L74
.L324:
	movl	112(%rsp), %eax
	movl	124(%rsp), %ecx
.L135:
	cmpl	$367, %ecx
	movl	%edx, 0(%rbp)
	movl	$0, (%r15)
	ja	.L188
	movq	%r9, %rbp
	jmp	.L148
.L336:
	cmpq	$0, 32(%rsp)
	jne	.L365
	cmpl	$1, %r12d
	movq	32(%rbx), %rbp
	jne	.L16
	movl	0(%rbp), %r13d
	movq	(%rbx), %rax
	testl	%r13d, %r13d
	je	.L17
	movq	48(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L366
	movl	$0, 0(%rbp)
.L17:
	testl	%edx, %edx
	je	.L23
.L20:
	movq	%rax, (%rbx)
	movl	$0, 8(%rsp)
	jmp	.L12
.L203:
	movl	$5, 8(%rsp)
	jmp	.L12
.L242:
	movq	%r12, %r10
	movl	%r11d, 8(%rsp)
	jmp	.L119
.L160:
	leal	-710(%rcx), %edi
	cmpl	$22, %edi
	jbe	.L367
	leal	-768(%rcx), %edi
	cmpl	$35, %edi
	ja	.L162
	leal	-572(%rcx), %esi
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	jmp	.L159
.L230:
	movl	$7, %eax
.L154:
	movq	16(%rsp), %rcx
	movq	%r13, (%rcx)
	movq	136(%rsp), %r13
.L152:
	cmpq	%r13, %rdx
	jne	.L126
	cmpq	$5, %rax
	jne	.L125
.L129:
	cmpq	%r14, %rdx
	jne	.L120
.L128:
	subl	$1, 20(%rbx)
	jmp	.L120
.L369:
	movq	%r10, 104(%rsp)
	movl	%r11d, 88(%rsp)
	subq	$8, %rsp
	pushq	104(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %r8
	movq	56(%rsp), %r9
	movq	80(%rsp), %rcx
	movq	%rbx, %rsi
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r13
	movq	152(%rsp), %rdx
	movl	88(%rsp), %r11d
	movq	104(%rsp), %r10
	je	.L172
	cmpl	$5, %eax
	jne	.L153
.L239:
	movl	$5, %eax
	jmp	.L154
.L16:
	movq	$0, 0(%rbp)
	testb	$1, 16(%rbx)
	movl	$0, 8(%rsp)
	jne	.L12
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	%r12
.L335:
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	88(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r12
	popq	%r13
	jmp	.L12
.L347:
	leaq	decomp_table(%rip), %rsi
	movq	%rdx, 152(%rsp)
	movzbl	2(%rsi,%rcx,4), %eax
	movb	%al, 0(%rbp)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	3(%rsi,%rcx,4), %edx
	movb	%dl, (%rax)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L95
.L367:
	leal	-537(%rcx), %esi
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	jmp	.L159
.L217:
	movl	$-128, %ecx
	jmp	.L107
.L247:
	leaq	1(%r13), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, 0(%r13)
	jmp	.L330
.L139:
	movl	$90, 88(%rsp)
	movl	$123, %r10d
.L141:
	cmpl	$64, %edx
	jbe	.L312
	leaq	comp_table_data(%rip), %rdi
	movl	%r10d, %esi
	movzwl	(%rdi,%rsi,4), %esi
	cmpl	%esi, %edx
	ja	.L135
	movl	88(%rsp), %edi
	movl	%eax, 112(%rsp)
	movl	%ecx, 124(%rsp)
.L144:
	leal	(%rdi,%r10), %esi
	leaq	comp_table_data(%rip), %rcx
	shrl	%esi
	movl	%esi, %eax
	movq	%rax, 88(%rsp)
	movzwl	(%rcx,%rax,4), %eax
	cmpl	%eax, %edx
	je	.L145
	jnb	.L146
	cmpl	%esi, %edi
	je	.L324
	movl	%esi, %r10d
	jmp	.L144
.L136:
	movl	$0, 88(%rsp)
	movl	$30, %r10d
	jmp	.L141
.L226:
	movl	$148, 88(%rsp)
	movl	$197, %r10d
	jmp	.L141
.L140:
	movl	$124, 88(%rsp)
	movl	$147, %r10d
	jmp	.L141
.L205:
	movq	%r12, %rcx
	movq	%r14, %rbp
	movl	$4, 8(%rsp)
	jmp	.L71
.L216:
	movl	$-2, %ecx
	jmp	.L107
.L166:
	leal	-192(%rcx), %esi
	cmpl	$7982, %esi
	jbe	.L368
.L168:
	cmpq	$0, 96(%rsp)
	je	.L240
	testb	$8, 16(%rbx)
	jne	.L369
.L172:
	testb	$2, %bpl
	jne	.L370
.L240:
	movl	$6, %eax
	jmp	.L154
.L146:
	cmpl	%esi, %edi
	je	.L371
	movl	%esi, %edi
	jmp	.L144
.L379:
	leal	-636(%rax), %edx
.L329:
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L46
.L362:
	cmpq	$0, 96(%rsp)
	je	.L334
	testb	$2, 104(%rsp)
	jne	.L132
.L334:
	cmpq	8(%rsp), %rbp
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	jne	.L126
.L125:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r10, %rdx
	movq	16(%rsp), %rax
	subq	%r12, %rdx
	addq	%rbp, %rdx
	cmpq	$4, %rdx
	movq	%r10, (%rax)
	ja	.L33
	cmpq	%rdx, %rbp
	leaq	1(%r12), %rax
	jnb	.L35
.L36:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%r15,%rbp)
	addq	$1, %rbp
	cmpq	%rbp, %rdx
	jne	.L36
.L35:
	movl	$7, 8(%rsp)
	jmp	.L12
.L162:
	leal	-832(%rcx), %edi
	cmpl	$1, %edi
	jbe	.L372
	leal	-8211(%rcx), %edi
	cmpl	$39, %edi
	ja	.L164
	leal	-7979(%rcx), %esi
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	jmp	.L159
.L218:
	movl	$-103, %ecx
	jmp	.L107
.L371:
	leaq	comp_table_data(%rip), %rsi
	movl	%r10d, %edi
	movl	112(%rsp), %eax
	movl	124(%rsp), %ecx
	movzwl	(%rsi,%rdi,4), %esi
	cmpl	%esi, %edx
	jne	.L135
	movq	%rdi, 88(%rsp)
.L145:
	movq	88(%rsp), %rcx
	leaq	comp_table_data(%rip), %rax
	addq	$1, %r13
	movzwl	2(%rax,%rcx,4), %eax
	movl	%eax, 0(%rbp)
	movl	$0, (%r15)
	movq	%r9, %rbp
	jmp	.L133
.L363:
	movq	16(%rsp), %rcx
	movq	40(%rsp), %r10
	movq	%rbp, %rdx
	movslq	64(%rsp), %rax
	movq	8(%rsp), %r13
	movq	%r12, (%rcx)
	jmp	.L152
.L366:
	leaq	4(%rax), %r12
	cmpq	8(%rbx), %r12
	ja	.L203
	movl	%r13d, %edx
	sarl	$3, %edx
	movl	%edx, (%rax)
	testb	$1, 16(%rbx)
	movl	$0, 0(%rbp)
	jne	.L373
	movq	24(%rsp), %rbx
	movq	%rax, 152(%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	pushq	$0
	movq	72(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%r15
	popq	%rdx
	je	.L23
	cmpq	152(%rsp), %r12
	jne	.L374
.L24:
	movl	8(%rsp), %r14d
	testl	%r14d, %r14d
	jne	.L12
.L23:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	$1
	jmp	.L335
.L47:
	leal	-710(%rax), %edx
	cmpl	$22, %edx
	ja	.L48
	leal	-537(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L46
.L361:
	cmpq	%r14, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	je	.L128
	jmp	.L126
.L65:
	cmpl	$0, 8(%rsp)
	jne	.L12
.L44:
	movq	48(%rsp), %rax
	movq	16(%rsp), %rcx
	movl	(%r15), %esi
	movl	16(%rbx), %r11d
	movq	96(%rax), %rax
	movq	(%rcx), %r12
	movl	%esi, 64(%rsp)
	jmp	.L28
.L132:
	movq	96(%rsp), %rax
	addq	$1, %r13
	movl	$6, 64(%rsp)
	addq	$1, (%rax)
	jmp	.L133
.L164:
	cmpl	$8363, %ecx
	je	.L232
	cmpl	$8364, %ecx
	je	.L233
	cmpl	$8482, %ecx
	je	.L234
	movl	%ecx, %edi
	shrl	$7, %edi
	cmpl	$7168, %edi
	jne	.L166
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r13
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L372:
	leal	-636(%rcx), %esi
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rsi), %esi
	jmp	.L159
.L370:
	movq	96(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L153
.L368:
	movl	$7808, %edi
	movl	$198, %r8d
	xorl	%r9d, %r9d
	movl	$198, %esi
	movl	%eax, 88(%rsp)
	jmp	.L167
.L375:
	cmpl	%esi, %r9d
	je	.L168
	movl	%esi, %r8d
.L171:
	leal	(%r9,%r8), %esi
	leaq	decomp_table(%rip), %rax
	movl	%esi, %edi
	shrl	%edi
	movzwl	(%rax,%rdi,4), %edi
.L167:
	shrl	%esi
	cmpl	%edi, %ecx
	je	.L325
	jb	.L375
	cmpl	%esi, %r9d
	je	.L376
	movl	%esi, %r9d
	jmp	.L171
.L53:
	leal	-192(%rax), %edx
	cmpl	$7982, %edx
	jbe	.L377
.L55:
	cmpq	$0, 96(%rsp)
	je	.L204
	testb	$8, %r11b
	jne	.L378
	andl	$2, %r11d
	je	.L204
	movq	40(%rsp), %rdx
.L184:
	movq	96(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 144(%rsp)
.L62:
	cmpq	40(%rsp), %rax
	jne	.L43
.L204:
	movl	$6, 8(%rsp)
	jmp	.L12
.L48:
	leal	-768(%rax), %edx
	cmpl	$35, %edx
	ja	.L49
	leal	-572(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L46
.L325:
	movl	88(%rsp), %eax
.L169:
	leaq	1(%rdx), %rcx
	cmpq	%rcx, 8(%rsp)
	jbe	.L239
	leaq	decomp_table(%rip), %rdi
	movq	%rcx, 152(%rsp)
	movzbl	2(%rdi,%rsi,4), %ecx
	movb	%cl, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movzbl	3(%rdi,%rsi,4), %ecx
	jmp	.L332
.L49:
	leal	-832(%rax), %edx
	cmpl	$1, %edx
	jbe	.L379
	leal	-8211(%rax), %edx
	cmpl	$39, %edx
	ja	.L51
	leal	-7979(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L46
.L364:
	movl	$89, %r10d
	movl	$31, %edi
	movl	%eax, 112(%rsp)
	movl	%ecx, 124(%rsp)
	jmp	.L144
.L378:
	movq	40(%rsp), %rax
	movq	%r10, 104(%rsp)
	leaq	144(%rsp), %rcx
	movl	%r11d, 88(%rsp)
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%rbx, %rsi
	addq	%rbp, %rax
	movq	%rax, 72(%rsp)
	pushq	104(%rsp)
	movq	%rax, %r8
	movq	64(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movl	88(%rsp), %r11d
	movq	104(%rsp), %r10
	movq	144(%rsp), %rax
	je	.L380
	cmpq	40(%rsp), %rax
	jne	.L43
	cmpl	$7, 8(%rsp)
	jne	.L65
	movq	40(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 64(%rsp)
	je	.L381
	movl	(%r15), %eax
	movq	%rbp, %rbx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbp
	jle	.L382
	cmpq	$4, %rbp
	ja	.L383
	orl	%ebp, %eax
	testq	%rbp, %rbp
	movl	%eax, (%r15)
	je	.L35
	movq	40(%rsp), %rcx
	xorl	%eax, %eax
.L69:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbp
	jne	.L69
	jmp	.L35
.L377:
	movl	$7808, %edi
	movl	$198, %ecx
	xorl	%esi, %esi
	movl	$198, %edx
	leaq	decomp_table(%rip), %r8
	jmp	.L54
.L384:
	cmpl	%edx, %esi
	je	.L55
	movl	%edx, %ecx
.L58:
	leal	(%rsi,%rcx), %edx
	movl	%edx, %edi
	shrl	%edi
	movzwl	(%r8,%rdi,4), %edi
.L54:
	shrl	%edx
	cmpl	%edi, %eax
	je	.L56
	jb	.L384
	cmpl	%edx, %esi
	je	.L385
	movl	%edx, %esi
	jmp	.L58
.L360:
	cmpq	%r14, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
	je	.L125
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	decomp_table(%rip), %rdi
	movl	%r8d, %esi
	movl	88(%rsp), %eax
	movzwl	(%rdi,%rsi,4), %esi
	cmpl	%esi, %ecx
	jne	.L168
	movl	%r8d, %esi
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$8363, %eax
	je	.L198
	cmpl	$8364, %eax
	je	.L199
	cmpl	$8482, %eax
	je	.L200
	movl	%eax, %edx
	shrl	$7, %edx
	cmpl	$7168, %edx
	jne	.L53
	movq	40(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L77:
	call	abort@PLT
.L233:
	movl	$-128, %esi
	jmp	.L165
.L232:
	movl	$-2, %esi
	jmp	.L165
.L234:
	movl	$-103, %esi
	jmp	.L165
.L355:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L385:
	leaq	decomp_table(%rip), %rsi
	movl	%ecx, %edx
	movzwl	(%rsi,%rdx,4), %edx
	cmpl	%edx, %eax
	jne	.L55
	movl	%ecx, %edx
.L56:
	leaq	1(%r13), %rax
	cmpq	%rax, %r14
	jbe	.L203
	leaq	decomp_table(%rip), %rcx
	movq	%rax, 152(%rsp)
	movzbl	2(%rcx,%rdx,4), %eax
	movzbl	3(%rcx,%rdx,4), %edx
	movb	%al, 0(%r13)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rax)
	jmp	.L330
.L200:
	movl	$-103, %edx
	jmp	.L52
.L199:
	movl	$-128, %edx
	jmp	.L52
.L198:
	movl	$-2, %edx
	jmp	.L52
.L382:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L381:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L380:
	andb	$2, %r11b
	movq	%rax, %rdx
	je	.L62
	jmp	.L184
.L357:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L383:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L33:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L365:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L374:
	movl	%r13d, 0(%rbp)
	jmp	.L24
.L373:
	movq	%r12, %rax
	jmp	.L20
.L179:
	leaq	__PRETTY_FUNCTION__.9216(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L30:
	leaq	__PRETTY_FUNCTION__.9135(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9135, @object
	.size	__PRETTY_FUNCTION__.9135, 17
__PRETTY_FUNCTION__.9135:
	.string	"to_cp1258_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9216, @object
	.size	__PRETTY_FUNCTION__.9216, 6
__PRETTY_FUNCTION__.9216:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	decomp_table, @object
	.size	decomp_table, 796
decomp_table:
	.value	192
	.byte	65
	.byte	-52
	.value	193
	.byte	65
	.byte	-20
	.value	195
	.byte	65
	.byte	-34
	.value	200
	.byte	69
	.byte	-52
	.value	201
	.byte	69
	.byte	-20
	.value	204
	.byte	73
	.byte	-52
	.value	205
	.byte	73
	.byte	-20
	.value	209
	.byte	78
	.byte	-34
	.value	210
	.byte	79
	.byte	-52
	.value	211
	.byte	79
	.byte	-20
	.value	213
	.byte	79
	.byte	-34
	.value	217
	.byte	85
	.byte	-52
	.value	218
	.byte	85
	.byte	-20
	.value	221
	.byte	89
	.byte	-20
	.value	224
	.byte	97
	.byte	-52
	.value	225
	.byte	97
	.byte	-20
	.value	227
	.byte	97
	.byte	-34
	.value	232
	.byte	101
	.byte	-52
	.value	233
	.byte	101
	.byte	-20
	.value	236
	.byte	105
	.byte	-52
	.value	237
	.byte	105
	.byte	-20
	.value	241
	.byte	110
	.byte	-34
	.value	242
	.byte	111
	.byte	-52
	.value	243
	.byte	111
	.byte	-20
	.value	245
	.byte	111
	.byte	-34
	.value	249
	.byte	117
	.byte	-52
	.value	250
	.byte	117
	.byte	-20
	.value	253
	.byte	121
	.byte	-20
	.value	262
	.byte	67
	.byte	-20
	.value	263
	.byte	99
	.byte	-20
	.value	296
	.byte	73
	.byte	-34
	.value	297
	.byte	105
	.byte	-34
	.value	313
	.byte	76
	.byte	-20
	.value	314
	.byte	108
	.byte	-20
	.value	323
	.byte	78
	.byte	-20
	.value	324
	.byte	110
	.byte	-20
	.value	340
	.byte	82
	.byte	-20
	.value	341
	.byte	114
	.byte	-20
	.value	346
	.byte	83
	.byte	-20
	.value	347
	.byte	115
	.byte	-20
	.value	360
	.byte	85
	.byte	-34
	.value	361
	.byte	117
	.byte	-34
	.value	377
	.byte	90
	.byte	-20
	.value	378
	.byte	122
	.byte	-20
	.value	471
	.byte	-36
	.byte	-20
	.value	472
	.byte	-4
	.byte	-20
	.value	475
	.byte	-36
	.byte	-52
	.value	476
	.byte	-4
	.byte	-52
	.value	500
	.byte	71
	.byte	-20
	.value	501
	.byte	103
	.byte	-20
	.value	504
	.byte	78
	.byte	-52
	.value	505
	.byte	110
	.byte	-52
	.value	506
	.byte	-59
	.byte	-20
	.value	507
	.byte	-27
	.byte	-20
	.value	508
	.byte	-58
	.byte	-20
	.value	509
	.byte	-26
	.byte	-20
	.value	510
	.byte	-40
	.byte	-20
	.value	511
	.byte	-8
	.byte	-20
	.value	901
	.byte	-88
	.byte	-20
	.value	7684
	.byte	66
	.byte	-14
	.value	7685
	.byte	98
	.byte	-14
	.value	7688
	.byte	-57
	.byte	-20
	.value	7689
	.byte	-25
	.byte	-20
	.value	7692
	.byte	68
	.byte	-14
	.value	7693
	.byte	100
	.byte	-14
	.value	7716
	.byte	72
	.byte	-14
	.value	7717
	.byte	104
	.byte	-14
	.value	7726
	.byte	-49
	.byte	-20
	.value	7727
	.byte	-17
	.byte	-20
	.value	7728
	.byte	75
	.byte	-20
	.value	7729
	.byte	107
	.byte	-20
	.value	7730
	.byte	75
	.byte	-14
	.value	7731
	.byte	107
	.byte	-14
	.value	7734
	.byte	76
	.byte	-14
	.value	7735
	.byte	108
	.byte	-14
	.value	7742
	.byte	77
	.byte	-20
	.value	7743
	.byte	109
	.byte	-20
	.value	7746
	.byte	77
	.byte	-14
	.value	7747
	.byte	109
	.byte	-14
	.value	7750
	.byte	78
	.byte	-14
	.value	7751
	.byte	110
	.byte	-14
	.value	7756
	.byte	-45
	.byte	-34
	.value	7757
	.byte	-13
	.byte	-34
	.value	7758
	.byte	-42
	.byte	-34
	.value	7759
	.byte	-10
	.byte	-34
	.value	7764
	.byte	80
	.byte	-20
	.value	7765
	.byte	112
	.byte	-20
	.value	7770
	.byte	82
	.byte	-14
	.value	7771
	.byte	114
	.byte	-14
	.value	7778
	.byte	83
	.byte	-14
	.value	7779
	.byte	115
	.byte	-14
	.value	7788
	.byte	84
	.byte	-14
	.value	7789
	.byte	116
	.byte	-14
	.value	7800
	.byte	-38
	.byte	-34
	.value	7801
	.byte	-6
	.byte	-34
	.value	7804
	.byte	86
	.byte	-34
	.value	7805
	.byte	118
	.byte	-34
	.value	7806
	.byte	86
	.byte	-14
	.value	7807
	.byte	118
	.byte	-14
	.value	7808
	.byte	87
	.byte	-52
	.value	7809
	.byte	119
	.byte	-52
	.value	7810
	.byte	87
	.byte	-20
	.value	7811
	.byte	119
	.byte	-20
	.value	7816
	.byte	87
	.byte	-14
	.value	7817
	.byte	119
	.byte	-14
	.value	7826
	.byte	90
	.byte	-14
	.value	7827
	.byte	122
	.byte	-14
	.value	7840
	.byte	65
	.byte	-14
	.value	7841
	.byte	97
	.byte	-14
	.value	7842
	.byte	65
	.byte	-46
	.value	7843
	.byte	97
	.byte	-46
	.value	7844
	.byte	-62
	.byte	-20
	.value	7845
	.byte	-30
	.byte	-20
	.value	7846
	.byte	-62
	.byte	-52
	.value	7847
	.byte	-30
	.byte	-52
	.value	7848
	.byte	-62
	.byte	-46
	.value	7849
	.byte	-30
	.byte	-46
	.value	7850
	.byte	-62
	.byte	-34
	.value	7851
	.byte	-30
	.byte	-34
	.value	7852
	.byte	-62
	.byte	-14
	.value	7853
	.byte	-30
	.byte	-14
	.value	7854
	.byte	-61
	.byte	-20
	.value	7855
	.byte	-29
	.byte	-20
	.value	7856
	.byte	-61
	.byte	-52
	.value	7857
	.byte	-29
	.byte	-52
	.value	7858
	.byte	-61
	.byte	-46
	.value	7859
	.byte	-29
	.byte	-46
	.value	7860
	.byte	-61
	.byte	-34
	.value	7861
	.byte	-29
	.byte	-34
	.value	7862
	.byte	-61
	.byte	-14
	.value	7863
	.byte	-29
	.byte	-14
	.value	7864
	.byte	69
	.byte	-14
	.value	7865
	.byte	101
	.byte	-14
	.value	7866
	.byte	69
	.byte	-46
	.value	7867
	.byte	101
	.byte	-46
	.value	7868
	.byte	69
	.byte	-34
	.value	7869
	.byte	101
	.byte	-34
	.value	7870
	.byte	-54
	.byte	-20
	.value	7871
	.byte	-22
	.byte	-20
	.value	7872
	.byte	-54
	.byte	-52
	.value	7873
	.byte	-22
	.byte	-52
	.value	7874
	.byte	-54
	.byte	-46
	.value	7875
	.byte	-22
	.byte	-46
	.value	7876
	.byte	-54
	.byte	-34
	.value	7877
	.byte	-22
	.byte	-34
	.value	7878
	.byte	-54
	.byte	-14
	.value	7879
	.byte	-22
	.byte	-14
	.value	7880
	.byte	73
	.byte	-46
	.value	7881
	.byte	105
	.byte	-46
	.value	7882
	.byte	73
	.byte	-14
	.value	7883
	.byte	105
	.byte	-14
	.value	7884
	.byte	79
	.byte	-14
	.value	7885
	.byte	111
	.byte	-14
	.value	7886
	.byte	79
	.byte	-46
	.value	7887
	.byte	111
	.byte	-46
	.value	7888
	.byte	-44
	.byte	-20
	.value	7889
	.byte	-12
	.byte	-20
	.value	7890
	.byte	-44
	.byte	-52
	.value	7891
	.byte	-12
	.byte	-52
	.value	7892
	.byte	-44
	.byte	-46
	.value	7893
	.byte	-12
	.byte	-46
	.value	7894
	.byte	-44
	.byte	-34
	.value	7895
	.byte	-12
	.byte	-34
	.value	7896
	.byte	-44
	.byte	-14
	.value	7897
	.byte	-12
	.byte	-14
	.value	7898
	.byte	-43
	.byte	-20
	.value	7899
	.byte	-11
	.byte	-20
	.value	7900
	.byte	-43
	.byte	-52
	.value	7901
	.byte	-11
	.byte	-52
	.value	7902
	.byte	-43
	.byte	-46
	.value	7903
	.byte	-11
	.byte	-46
	.value	7904
	.byte	-43
	.byte	-34
	.value	7905
	.byte	-11
	.byte	-34
	.value	7906
	.byte	-43
	.byte	-14
	.value	7907
	.byte	-11
	.byte	-14
	.value	7908
	.byte	85
	.byte	-14
	.value	7909
	.byte	117
	.byte	-14
	.value	7910
	.byte	85
	.byte	-46
	.value	7911
	.byte	117
	.byte	-46
	.value	7912
	.byte	-35
	.byte	-20
	.value	7913
	.byte	-3
	.byte	-20
	.value	7914
	.byte	-35
	.byte	-52
	.value	7915
	.byte	-3
	.byte	-52
	.value	7916
	.byte	-35
	.byte	-46
	.value	7917
	.byte	-3
	.byte	-46
	.value	7918
	.byte	-35
	.byte	-34
	.value	7919
	.byte	-3
	.byte	-34
	.value	7920
	.byte	-35
	.byte	-14
	.value	7921
	.byte	-3
	.byte	-14
	.value	7922
	.byte	89
	.byte	-52
	.value	7923
	.byte	121
	.byte	-52
	.value	7924
	.byte	89
	.byte	-14
	.value	7925
	.byte	121
	.byte	-14
	.value	7926
	.byte	89
	.byte	-46
	.value	7927
	.byte	121
	.byte	-46
	.value	7928
	.byte	89
	.byte	-34
	.value	7929
	.byte	121
	.byte	-34
	.value	8173
	.byte	-88
	.byte	-52
	.value	8174
	.byte	-88
	.byte	-20
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 272
from_ucs4:
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	0
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	0
	.byte	-47
	.byte	0
	.byte	-45
	.byte	-44
	.byte	0
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	-37
	.byte	-36
	.byte	0
	.byte	0
	.byte	-33
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	0
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	0
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	0
	.byte	-15
	.byte	0
	.byte	-13
	.byte	-12
	.byte	0
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	0
	.byte	0
	.byte	-1
	.byte	0
	.byte	0
	.byte	-61
	.byte	-29
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-48
	.byte	-16
	.byte	-116
	.byte	-100
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-97
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-125
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-43
	.byte	-11
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-35
	.byte	-3
	.byte	-120
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-104
	.byte	-52
	.byte	-20
	.byte	0
	.byte	-34
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-46
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-14
	.byte	-106
	.byte	-105
	.byte	0
	.byte	0
	.byte	0
	.byte	-111
	.byte	-110
	.byte	-126
	.byte	0
	.byte	-109
	.byte	-108
	.byte	-124
	.byte	0
	.byte	-122
	.byte	-121
	.byte	-107
	.byte	0
	.byte	0
	.byte	0
	.byte	-123
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-119
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-117
	.byte	-101
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 792
comp_table_data:
	.value	65
	.value	192
	.value	69
	.value	200
	.value	73
	.value	204
	.value	78
	.value	504
	.value	79
	.value	210
	.value	85
	.value	217
	.value	87
	.value	7808
	.value	89
	.value	7922
	.value	97
	.value	224
	.value	101
	.value	232
	.value	105
	.value	236
	.value	110
	.value	505
	.value	111
	.value	242
	.value	117
	.value	249
	.value	119
	.value	7809
	.value	121
	.value	7923
	.value	168
	.value	8173
	.value	194
	.value	7846
	.value	202
	.value	7872
	.value	212
	.value	7890
	.value	220
	.value	475
	.value	226
	.value	7847
	.value	234
	.value	7873
	.value	244
	.value	7891
	.value	252
	.value	476
	.value	258
	.value	7856
	.value	259
	.value	7857
	.value	416
	.value	7900
	.value	417
	.value	7901
	.value	431
	.value	7914
	.value	432
	.value	7915
	.value	65
	.value	193
	.value	67
	.value	262
	.value	69
	.value	201
	.value	71
	.value	500
	.value	73
	.value	205
	.value	75
	.value	7728
	.value	76
	.value	313
	.value	77
	.value	7742
	.value	78
	.value	323
	.value	79
	.value	211
	.value	80
	.value	7764
	.value	82
	.value	340
	.value	83
	.value	346
	.value	85
	.value	218
	.value	87
	.value	7810
	.value	89
	.value	221
	.value	90
	.value	377
	.value	97
	.value	225
	.value	99
	.value	263
	.value	101
	.value	233
	.value	103
	.value	501
	.value	105
	.value	237
	.value	107
	.value	7729
	.value	108
	.value	314
	.value	109
	.value	7743
	.value	110
	.value	324
	.value	111
	.value	243
	.value	112
	.value	7765
	.value	114
	.value	341
	.value	115
	.value	347
	.value	117
	.value	250
	.value	119
	.value	7811
	.value	121
	.value	253
	.value	122
	.value	378
	.value	168
	.value	901
	.value	194
	.value	7844
	.value	197
	.value	506
	.value	198
	.value	508
	.value	199
	.value	7688
	.value	202
	.value	7870
	.value	207
	.value	7726
	.value	212
	.value	7888
	.value	216
	.value	510
	.value	220
	.value	471
	.value	226
	.value	7845
	.value	229
	.value	507
	.value	230
	.value	509
	.value	231
	.value	7689
	.value	234
	.value	7871
	.value	239
	.value	7727
	.value	244
	.value	7889
	.value	248
	.value	511
	.value	252
	.value	472
	.value	258
	.value	7854
	.value	259
	.value	7855
	.value	416
	.value	7898
	.value	417
	.value	7899
	.value	431
	.value	7912
	.value	432
	.value	7913
	.value	65
	.value	195
	.value	69
	.value	7868
	.value	73
	.value	296
	.value	78
	.value	209
	.value	79
	.value	213
	.value	85
	.value	360
	.value	86
	.value	7804
	.value	89
	.value	7928
	.value	97
	.value	227
	.value	101
	.value	7869
	.value	105
	.value	297
	.value	110
	.value	241
	.value	111
	.value	245
	.value	117
	.value	361
	.value	118
	.value	7805
	.value	121
	.value	7929
	.value	194
	.value	7850
	.value	202
	.value	7876
	.value	211
	.value	7756
	.value	212
	.value	7894
	.value	214
	.value	7758
	.value	218
	.value	7800
	.value	226
	.value	7851
	.value	234
	.value	7877
	.value	243
	.value	7757
	.value	244
	.value	7895
	.value	246
	.value	7759
	.value	250
	.value	7801
	.value	258
	.value	7860
	.value	259
	.value	7861
	.value	416
	.value	7904
	.value	417
	.value	7905
	.value	431
	.value	7918
	.value	432
	.value	7919
	.value	65
	.value	7842
	.value	69
	.value	7866
	.value	73
	.value	7880
	.value	79
	.value	7886
	.value	85
	.value	7910
	.value	89
	.value	7926
	.value	97
	.value	7843
	.value	101
	.value	7867
	.value	105
	.value	7881
	.value	111
	.value	7887
	.value	117
	.value	7911
	.value	121
	.value	7927
	.value	194
	.value	7848
	.value	202
	.value	7874
	.value	212
	.value	7892
	.value	226
	.value	7849
	.value	234
	.value	7875
	.value	244
	.value	7893
	.value	258
	.value	7858
	.value	259
	.value	7859
	.value	416
	.value	7902
	.value	417
	.value	7903
	.value	431
	.value	7916
	.value	432
	.value	7917
	.value	65
	.value	7840
	.value	66
	.value	7684
	.value	68
	.value	7692
	.value	69
	.value	7864
	.value	72
	.value	7716
	.value	73
	.value	7882
	.value	75
	.value	7730
	.value	76
	.value	7734
	.value	77
	.value	7746
	.value	78
	.value	7750
	.value	79
	.value	7884
	.value	82
	.value	7770
	.value	83
	.value	7778
	.value	84
	.value	7788
	.value	85
	.value	7908
	.value	86
	.value	7806
	.value	87
	.value	7816
	.value	89
	.value	7924
	.value	90
	.value	7826
	.value	97
	.value	7841
	.value	98
	.value	7685
	.value	100
	.value	7693
	.value	101
	.value	7865
	.value	104
	.value	7717
	.value	105
	.value	7883
	.value	107
	.value	7731
	.value	108
	.value	7735
	.value	109
	.value	7747
	.value	110
	.value	7751
	.value	111
	.value	7885
	.value	114
	.value	7771
	.value	115
	.value	7779
	.value	116
	.value	7789
	.value	117
	.value	7909
	.value	118
	.value	7807
	.value	119
	.value	7817
	.value	121
	.value	7925
	.value	122
	.value	7827
	.value	194
	.value	7852
	.value	202
	.value	7878
	.value	212
	.value	7896
	.value	226
	.value	7853
	.value	234
	.value	7879
	.value	244
	.value	7897
	.value	258
	.value	7862
	.value	259
	.value	7863
	.value	416
	.value	7906
	.value	417
	.value	7907
	.value	431
	.value	7920
	.value	432
	.value	7921
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 256
to_ucs4:
	.value	8364
	.value	0
	.value	8218
	.value	402
	.value	8222
	.value	8230
	.value	8224
	.value	8225
	.value	710
	.value	8240
	.value	0
	.value	8249
	.value	338
	.value	0
	.value	0
	.value	0
	.value	0
	.value	8216
	.value	8217
	.value	8220
	.value	8221
	.value	8226
	.value	8211
	.value	8212
	.value	732
	.value	8482
	.value	0
	.value	8250
	.value	339
	.value	0
	.value	0
	.value	376
	.value	160
	.value	161
	.value	162
	.value	163
	.value	164
	.value	165
	.value	166
	.value	167
	.value	168
	.value	169
	.value	170
	.value	171
	.value	172
	.value	173
	.value	174
	.value	175
	.value	176
	.value	177
	.value	178
	.value	179
	.value	180
	.value	181
	.value	182
	.value	183
	.value	184
	.value	185
	.value	186
	.value	187
	.value	188
	.value	189
	.value	190
	.value	191
	.value	192
	.value	193
	.value	194
	.value	258
	.value	196
	.value	197
	.value	198
	.value	199
	.value	200
	.value	201
	.value	202
	.value	203
	.value	768
	.value	205
	.value	206
	.value	207
	.value	272
	.value	209
	.value	777
	.value	211
	.value	212
	.value	416
	.value	214
	.value	215
	.value	216
	.value	217
	.value	218
	.value	219
	.value	220
	.value	431
	.value	771
	.value	223
	.value	224
	.value	225
	.value	226
	.value	259
	.value	228
	.value	229
	.value	230
	.value	231
	.value	232
	.value	233
	.value	234
	.value	235
	.value	769
	.value	237
	.value	238
	.value	239
	.value	273
	.value	241
	.value	803
	.value	243
	.value	244
	.value	417
	.value	246
	.value	247
	.value	248
	.value	249
	.value	250
	.value	251
	.value	252
	.value	432
	.value	8363
	.value	255
