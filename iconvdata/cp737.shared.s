	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	testb	%sil, %sil
	movl	(%rax,%rdx,4), %eax
	je	.L1
	testl	%eax, %eax
	movl	$-1, %edx
	cmove	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"CP737//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r10d
	movq	%rsi, 80(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 48(%rsp)
	testb	$1, %r10b
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	movq	%rdi, %rax
	movq	144(%rdi), %rdi
	cmpq	$0, 104(%rax)
	movq	%rdi, 24(%rsp)
	je	.L15
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L227
	movq	8(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %ecx
	movq	8(%r12), %r11
	testq	%rdi, %rdi
	movq	(%rax), %r15
	movq	%rdi, %rax
	cmove	%r12, %rax
	testq	%r14, %r14
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	testl	%ecx, %ecx
	movq	%rax, 64(%rsp)
	movq	40(%rsp), %rax
	setne	103(%rsp)
	movzbl	103(%rsp), %esi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L121
	testb	%sil, %sil
	je	.L121
	movq	32(%r12), %rbx
	movl	(%rbx), %edx
	andl	$7, %edx
	jne	.L228
.L121:
	movq	$0, 32(%rsp)
.L22:
	movq	%r14, 16(%rsp)
	movq	%r11, %r14
	.p2align 4,,10
	.p2align 3
.L110:
	movq	16(%rsp), %rbx
	testq	%rbx, %rbx
	je	.L54
	movq	(%rbx), %rbx
	addq	%rbx, 32(%rsp)
.L54:
	testq	%rax, %rax
	je	.L229
	leaq	136(%rsp), %rax
	movq	%r15, 128(%rsp)
	movq	%r13, %rbx
	movq	%r13, 136(%rsp)
	movq	%r15, %rdi
	movl	$4, %r11d
	movq	%rax, 56(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 88(%rsp)
.L62:
	cmpq	%rdi, %rbp
	je	.L63
.L73:
	leaq	4(%rdi), %rax
	cmpq	%rax, %rbp
	jb	.L129
	cmpq	%rbx, %r14
	jbe	.L130
	movl	(%rdi), %r8d
	cmpl	$65534, %r8d
	ja	.L64
	cmpl	$127, %r8d
	movl	$160, %edx
	leaq	from_idx(%rip), %rsi
	ja	.L65
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movzwl	10(%rcx), %edx
	movq	%rcx, %rsi
.L65:
	cmpl	%edx, %r8d
	leaq	8(%rsi), %rcx
	ja	.L231
	movzwl	(%rcx), %eax
	cmpl	%eax, %r8d
	jb	.L67
	movl	%r8d, %edx
	addl	12(%rsi), %edx
.L66:
	leaq	from_ucs4(%rip), %rax
	movzbl	(%rax,%rdx), %eax
	testb	%al, %al
	jne	.L150
	testl	%r8d, %r8d
	jne	.L67
.L150:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 128(%rsp)
	jne	.L73
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$0, 48(%rsp)
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
	jne	.L232
.L74:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L233
	cmpq	%rbx, %r13
	jnb	.L134
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 56(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%r8
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%r9
	movl	56(%rsp), %r11d
	je	.L78
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L234
.L77:
	testl	%r10d, %r10d
	jne	.L146
.L109:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rdi
	movl	16(%r12), %r10d
	movq	(%r12), %r13
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rdi), %r15
	movq	96(%rax), %rax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L64:
	shrl	$7, %r8d
	cmpl	$7168, %r8d
	je	.L235
.L67:
	cmpq	$0, 64(%rsp)
	je	.L133
	testb	$8, 16(%r12)
	jne	.L236
.L71:
	testb	$2, %r10b
	jne	.L237
.L133:
	cmpq	$0, 48(%rsp)
	movq	8(%rsp), %rax
	movl	$6, %r11d
	movq	%rdi, (%rax)
	je	.L74
.L232:
	movq	48(%rsp), %rax
	movl	%r11d, %r9d
	movq	%rbx, (%rax)
.L14:
	addq	$152, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	cmpq	%r15, %rbp
	je	.L124
	leaq	4(%r13), %rdx
	cmpq	%rdx, %r14
	jb	.L125
	movq	%r15, %rax
	movq	%r13, %rbx
	movl	$4, %r11d
	andl	$2, %r10d
	.p2align 4,,10
	.p2align 3
.L57:
	movzbl	(%rax), %ecx
	leaq	to_ucs4(%rip), %r9
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movl	(%r9,%rcx,4), %ecx
	testl	%ecx, %ecx
	jne	.L58
	testb	%sil, %sil
	jne	.L238
.L58:
	addq	$1, %rax
	movl	%ecx, (%rbx)
	movq	%rdx, %rbx
	movq	%rax, %rdi
.L59:
	cmpq	%rax, %rbp
	je	.L63
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r14
	jnb	.L57
.L130:
	movl	$5, %r11d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L77
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L230:
	movl	%r8d, %edx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$7, %r11d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L236:
	movl	%r10d, 104(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	72(%rsp), %r9
	movq	104(%rsp), %rcx
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r11d
	popq	%rbx
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rbx
	movl	104(%rsp), %r10d
	je	.L71
	cmpl	$5, %eax
	jne	.L62
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L237:
	movq	64(%rsp), %rax
	addq	$4, %rdi
	movl	$6, %r11d
	movq	%rdi, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L62
.L233:
	movq	16(%rsp), %r14
	movq	%rbx, (%r12)
	movl	%r11d, %r9d
	movq	112(%rsp), %rax
	addq	%rax, (%r14)
.L76:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 103(%rsp)
	je	.L14
	cmpl	$7, %r9d
	jne	.L14
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L112
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	32(%r12), %rcx
	je	.L114
.L113:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L113
.L114:
	movl	(%rcx), %edx
	movq	8(%rsp), %rbx
	andl	$-8, %edx
	movq	%rbp, (%rbx)
	orl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%r11d, %r10d
	jmp	.L77
.L238:
	cmpq	$0, 64(%rsp)
	je	.L133
	testl	%r10d, %r10d
	je	.L133
	movq	64(%rsp), %rsi
	leaq	1(%rax), %rax
	movl	$6, %r11d
	movq	%rax, %rdi
	addq	$1, (%rsi)
	jmp	.L59
.L234:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L80
	movq	(%rsi), %rax
.L80:
	addq	112(%rsp), %rax
	cmpq	32(%rsp), %rax
	movq	8(%rsp), %rax
	je	.L239
	movq	%r15, (%rax)
	movq	40(%rsp), %rax
	movl	16(%r12), %ebx
	cmpq	$0, 96(%rax)
	je	.L240
	leaq	136(%rsp), %rdi
	movq	%r15, 128(%rsp)
	movq	%r13, 136(%rsp)
	movq	%r13, %rdx
	movl	$4, %eax
	movq	%rdi, 32(%rsp)
	leaq	128(%rsp), %rdi
	movq	%rdi, 56(%rsp)
.L95:
	cmpq	%r15, %rbp
	je	.L241
	leaq	4(%r15), %rcx
	cmpq	%rcx, %rbp
	jb	.L139
	cmpq	%rdx, %r11
	jbe	.L143
	movl	(%r15), %r8d
	cmpl	$65534, %r8d
	ja	.L97
	cmpl	$127, %r8d
	movl	$160, %ecx
	leaq	from_idx(%rip), %rdi
	ja	.L98
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L243:
	movzwl	10(%rsi), %ecx
	movq	%rsi, %rdi
.L98:
	cmpl	%ecx, %r8d
	leaq	8(%rdi), %rsi
	ja	.L243
	movzwl	(%rsi), %ecx
	cmpl	%ecx, %r8d
	jb	.L100
	movl	%r8d, %ecx
	addl	12(%rdi), %ecx
.L99:
	leaq	from_ucs4(%rip), %rdi
	testl	%r8d, %r8d
	movzbl	(%rdi,%rcx), %ecx
	je	.L151
	testb	%cl, %cl
	je	.L100
.L151:
	leaq	1(%rdx), %rsi
	movq	%rsi, 136(%rsp)
	movb	%cl, (%rdx)
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rdx
	leaq	4(%rdi), %r15
	movq	%r15, 128(%rsp)
	jmp	.L95
.L228:
	testq	%rdi, %rdi
	jne	.L244
	cmpl	$4, %edx
	movq	%r15, 128(%rsp)
	movq	%r13, 136(%rsp)
	ja	.L24
	leaq	120(%rsp), %rsi
	movslq	%edx, %rcx
	xorl	%eax, %eax
	movq	%rcx, 16(%rsp)
	movq	%rsi, 32(%rsp)
.L25:
	movzbl	4(%rbx,%rax), %edx
	movb	%dl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L25
	movq	%r15, %rax
	subq	16(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L245
	cmpq	%r11, %r13
	movl	$5, %r9d
	jnb	.L14
	movq	16(%rsp), %rdx
	leaq	1(%r15), %rax
	leaq	119(%rsp), %rsi
.L33:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %edi
	addq	$1, %rdx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rdx
	movb	%dil, (%rsi,%rdx)
	ja	.L148
	cmpq	%rcx, %rbp
	ja	.L33
.L148:
	movl	120(%rsp), %ecx
	movq	32(%rsp), %rax
	movq	%rdx, 16(%rsp)
	cmpl	$65534, %ecx
	movq	%rax, 128(%rsp)
	ja	.L35
	cmpl	$127, %ecx
	movl	$160, %eax
	leaq	from_idx(%rip), %rsi
	ja	.L36
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L247:
	movzwl	10(%rdx), %eax
	movq	%rdx, %rsi
.L36:
	cmpl	%eax, %ecx
	leaq	8(%rsi), %rdx
	ja	.L247
	movzwl	(%rdx), %eax
	cmpl	%eax, %ecx
	jb	.L38
	movl	%ecx, %eax
	addl	12(%rsi), %eax
.L37:
	leaq	from_ucs4(%rip), %rdx
	testl	%ecx, %ecx
	movzbl	(%rdx,%rax), %eax
	je	.L149
	testb	%al, %al
	jne	.L149
.L38:
	cmpq	$0, 64(%rsp)
	je	.L123
	testb	$8, %r10b
	jne	.L248
	andl	$2, %r10d
	je	.L123
	movq	32(%rsp), %rax
.L116:
	movq	64(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L46:
	cmpq	32(%rsp), %rax
	jne	.L39
.L123:
	movl	$6, %r9d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%rax, 128(%rsp)
	movq	%rax, %rdi
	jmp	.L62
.L239:
	movq	40(%rsp), %rdi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rdi)
	je	.L249
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L77
.L149:
	leaq	1(%r13), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, 0(%r13)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	je	.L220
.L39:
	movl	(%rbx), %edx
	subq	32(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L250
	movq	8(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r13
	movl	16(%r12), %r10d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r15
	movq	112(%rsp), %rax
	movl	%edx, (%rbx)
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L22
.L125:
	movq	%r15, %rdi
	movq	%r13, %rbx
	movl	$5, %r11d
	jmp	.L63
.L227:
	cmpq	$0, 48(%rsp)
	jne	.L251
	movq	32(%r12), %rax
	xorl	%r9d, %r9d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	movq	%r14, %r9
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rsi
	movl	%eax, %r9d
	popq	%rdi
	jmp	.L14
.L146:
	movl	%r10d, %r9d
	jmp	.L76
.L100:
	cmpq	$0, 64(%rsp)
	je	.L144
	testb	$8, 16(%r12)
	jne	.L252
.L104:
	testb	$2, %bl
	jne	.L253
.L144:
	movl	$6, %eax
.L96:
	movq	8(%rsp), %rbx
	movq	120(%rsp), %r11
	movq	%r15, (%rbx)
.L94:
	cmpq	%r11, %rdx
	jne	.L86
	cmpq	$5, %rax
	jne	.L85
	cmpq	%rdx, %r13
	jne	.L77
.L88:
	subl	$1, 20(%r12)
	jmp	.L77
.L124:
	movq	%rbp, %rdi
	movq	%r13, %rbx
	movl	$4, %r11d
	jmp	.L63
.L252:
	movq	%r11, 104(%rsp)
	movl	%r10d, 88(%rsp)
	subq	$8, %rsp
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r15
	movq	136(%rsp), %rdx
	movl	88(%rsp), %r10d
	movq	104(%rsp), %r11
	je	.L104
	cmpl	$5, %eax
	jne	.L95
.L143:
	movl	$5, %eax
	jmp	.L96
.L240:
	cmpq	%r15, %rbp
	je	.L254
	leaq	4(%r13), %rcx
	cmpq	%r11, %rcx
	ja	.L255
	movq	64(%rsp), %r9
	andl	$2, %ebx
	movq	%r13, %rdx
	movl	$4, %eax
	movq	%r15, %rdi
	movl	%ebx, 32(%rsp)
.L89:
	movzbl	(%rdi), %r8d
	leaq	to_ucs4(%rip), %rbx
	movq	%rdi, %rsi
	movq	%r8, %r15
	movl	(%rbx,%r8,4), %r8d
	testl	%r8d, %r8d
	jne	.L91
	testb	%r15b, %r15b
	jne	.L256
.L91:
	addq	$1, %rdi
	movl	%r8d, (%rdx)
	movq	%rcx, %rdx
	movq	%rdi, %rsi
.L92:
	cmpq	%rdi, %rbp
	je	.L257
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r11
	jnb	.L89
	movl	$5, %eax
.L90:
	movq	8(%rsp), %rbx
	movq	%rsi, (%rbx)
	jmp	.L94
.L242:
	movl	%r8d, %ecx
	jmp	.L99
.L139:
	movl	$7, %eax
	jmp	.L96
.L97:
	shrl	$7, %r8d
	cmpl	$7168, %r8d
	jne	.L100
	movq	%rcx, 128(%rsp)
	movq	%rcx, %r15
	jmp	.L95
.L249:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L77
.L245:
	movq	8(%rsp), %rax
	movq	16(%rsp), %rsi
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r15, %rax
	addq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L27
	cmpq	%rsi, %rax
	leaq	1(%r15), %rdx
	movq	16(%rsp), %rcx
	jbe	.L29
.L30:
	movq	%rdx, 128(%rsp)
	movzbl	-1(%rdx), %esi
	addq	$1, %rdx
	movb	%sil, 4(%rbx,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %rax
	jne	.L30
.L29:
	movl	$7, %r9d
	jmp	.L14
.L253:
	movq	64(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L95
.L257:
	movq	%rbp, %rsi
	jmp	.L90
.L246:
	movl	%ecx, %eax
	jmp	.L37
.L35:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	jne	.L38
	movq	32(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L241:
	cltq
	movq	%rbp, %r15
	jmp	.L96
.L256:
	testq	%r9, %r9
	je	.L138
	movl	32(%rsp), %edi
	testl	%edi, %edi
	jne	.L258
.L138:
	movl	$6, %eax
	jmp	.L90
.L258:
	leaq	1(%rsi), %rdi
	addq	$1, (%r9)
	movl	$6, %eax
	movq	%rdi, %rsi
	jmp	.L92
.L248:
	movq	32(%rsp), %rax
	addq	16(%rsp), %rax
	leaq	128(%rsp), %rcx
	movq	%r11, 104(%rsp)
	movl	%r10d, 88(%rsp)
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, 64(%rsp)
	pushq	72(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r15
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%rdx
	movl	88(%rsp), %r10d
	movq	104(%rsp), %r11
	movq	128(%rsp), %rax
	je	.L259
	cmpq	32(%rsp), %rax
	jne	.L39
	cmpl	$7, %r9d
	jne	.L49
	movq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 56(%rsp)
	je	.L260
	movl	(%rbx), %eax
	movq	16(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %rdi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jle	.L261
	cmpq	$4, 16(%rsp)
	ja	.L262
	movq	16(%rsp), %rsi
	orl	%esi, %eax
	testq	%rsi, %rsi
	movl	%eax, (%rbx)
	je	.L29
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L53:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 16(%rsp)
	jne	.L53
	jmp	.L29
.L49:
	testl	%r9d, %r9d
	jne	.L14
.L220:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rbx
	movl	16(%r12), %r10d
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r15
	movq	96(%rax), %rax
	jmp	.L22
.L255:
	cmpq	%r13, %r11
	je	.L88
.L86:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
.L250:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L262:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L261:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L260:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L259:
	andb	$2, %r10b
	je	.L46
	jmp	.L116
.L27:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L254:
	cmpq	%r13, %r11
	jne	.L86
.L85:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L112:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L251:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L24:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L244:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9074, @object
	.size	__PRETTY_FUNCTION__.9074, 14
__PRETTY_FUNCTION__.9074:
	.string	"to_gap_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9153, @object
	.size	__PRETTY_FUNCTION__.9153, 6
__PRETTY_FUNCTION__.9153:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 291
from_ucs4:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	64
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	95
	.byte	96
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	123
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.byte	-1
	.byte	-8
	.byte	-15
	.byte	-3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-6
	.byte	-10
	.byte	-22
	.byte	0
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	0
	.byte	-18
	.byte	0
	.byte	-17
	.byte	-16
	.byte	0
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	0
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-12
	.byte	-11
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-27
	.byte	0
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	-96
	.byte	-95
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-86
	.byte	-87
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.byte	-32
	.byte	-28
	.byte	-24
	.byte	-26
	.byte	-25
	.byte	-23
	.byte	-4
	.byte	-7
	.byte	-5
	.byte	-9
	.byte	-13
	.byte	-14
	.byte	-60
	.byte	0
	.byte	-77
	.byte	-38
	.byte	0
	.byte	0
	.byte	0
	.byte	-65
	.byte	0
	.byte	0
	.byte	0
	.byte	-64
	.byte	0
	.byte	0
	.byte	0
	.byte	-39
	.byte	0
	.byte	0
	.byte	0
	.byte	-61
	.byte	-76
	.byte	-62
	.byte	-63
	.byte	-59
	.byte	-51
	.byte	-70
	.byte	-43
	.byte	-42
	.byte	-55
	.byte	-72
	.byte	-73
	.byte	-69
	.byte	-44
	.byte	-45
	.byte	-56
	.byte	-66
	.byte	-67
	.byte	-68
	.byte	-58
	.byte	-57
	.byte	-52
	.byte	-75
	.byte	-74
	.byte	-71
	.byte	-47
	.byte	-46
	.byte	-53
	.byte	-49
	.byte	-48
	.byte	-54
	.byte	-40
	.byte	-41
	.byte	-50
	.byte	-33
	.byte	0
	.byte	0
	.byte	0
	.byte	-36
	.byte	0
	.byte	0
	.byte	0
	.byte	-37
	.byte	0
	.byte	0
	.byte	0
	.byte	-35
	.byte	0
	.byte	0
	.byte	0
	.byte	-34
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-2
	.align 32
	.type	from_idx, @object
	.size	from_idx, 152
from_idx:
	.value	0
	.value	127
	.long	0
	.value	160
	.value	160
	.long	-32
	.value	176
	.value	183
	.long	-47
	.value	247
	.value	247
	.long	-110
	.value	902
	.value	974
	.long	-764
	.value	8319
	.value	8319
	.long	-8108
	.value	8729
	.value	8730
	.long	-8517
	.value	8776
	.value	8776
	.long	-8562
	.value	8804
	.value	8805
	.long	-8589
	.value	9472
	.value	9474
	.long	-9255
	.value	9484
	.value	9500
	.long	-9264
	.value	9508
	.value	9508
	.long	-9271
	.value	9516
	.value	9516
	.long	-9278
	.value	9524
	.value	9524
	.long	-9285
	.value	9532
	.value	9532
	.long	-9292
	.value	9552
	.value	9580
	.long	-9311
	.value	9600
	.value	9619
	.long	-9330
	.value	9632
	.value	9632
	.long	-9342
	.value	-1
	.value	-1
	.long	0
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	913
	.long	914
	.long	915
	.long	916
	.long	917
	.long	918
	.long	919
	.long	920
	.long	921
	.long	922
	.long	923
	.long	924
	.long	925
	.long	926
	.long	927
	.long	928
	.long	929
	.long	931
	.long	932
	.long	933
	.long	934
	.long	935
	.long	936
	.long	937
	.long	945
	.long	946
	.long	947
	.long	948
	.long	949
	.long	950
	.long	951
	.long	952
	.long	953
	.long	954
	.long	955
	.long	956
	.long	957
	.long	958
	.long	959
	.long	960
	.long	961
	.long	963
	.long	962
	.long	964
	.long	965
	.long	966
	.long	967
	.long	968
	.long	9617
	.long	9618
	.long	9619
	.long	9474
	.long	9508
	.long	9569
	.long	9570
	.long	9558
	.long	9557
	.long	9571
	.long	9553
	.long	9559
	.long	9565
	.long	9564
	.long	9563
	.long	9488
	.long	9492
	.long	9524
	.long	9516
	.long	9500
	.long	9472
	.long	9532
	.long	9566
	.long	9567
	.long	9562
	.long	9556
	.long	9577
	.long	9574
	.long	9568
	.long	9552
	.long	9580
	.long	9575
	.long	9576
	.long	9572
	.long	9573
	.long	9561
	.long	9560
	.long	9554
	.long	9555
	.long	9579
	.long	9578
	.long	9496
	.long	9484
	.long	9608
	.long	9604
	.long	9612
	.long	9616
	.long	9600
	.long	969
	.long	940
	.long	941
	.long	942
	.long	970
	.long	943
	.long	972
	.long	973
	.long	971
	.long	974
	.long	902
	.long	904
	.long	905
	.long	906
	.long	908
	.long	910
	.long	911
	.long	177
	.long	8805
	.long	8804
	.long	938
	.long	939
	.long	247
	.long	8776
	.long	176
	.long	8729
	.long	183
	.long	8730
	.long	8319
	.long	178
	.long	9632
	.long	160
