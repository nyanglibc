	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	cmpb	$23, %sil
	ja	.L2
	leaq	map_from_tcvn_low(%rip), %rax
	movzbl	%sil, %esi
	movzwl	(%rax,%rsi,2), %eax
.L3:
	leal	-65(%rax), %edx
	cmpl	$367, %edx
	movl	$-1, %edx
	cmovbe	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testb	%sil, %sil
	movzbl	%sil, %eax
	jns	.L3
	addl	$-128, %eax
	leaq	map_from_tcvn_high(%rip), %rdx
	cltq
	movzwl	(%rdx,%rax,2), %eax
	jmp	.L3
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TCVN5712-1//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	32(%rax), %rsi
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L12
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r11
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	leaq	104(%rdi), %rsi
	subq	$168, %rsp
	movl	16(%rbp), %ebx
	movq	%rdx, 16(%rsp)
	movq	%rsi, 80(%rsp)
	leaq	48(%rbp), %rsi
	movq	%rdi, 40(%rsp)
	movq	%r8, 24(%rsp)
	movl	%ebx, %edx
	movq	%r9, 64(%rsp)
	movl	224(%rsp), %r12d
	andl	$1, %edx
	movq	%rsi, 88(%rsp)
	movq	$0, 72(%rsp)
	jne	.L14
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rcx
	movq	%rcx, 72(%rsp)
	je	.L14
	movq	%rcx, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 72(%rsp)
.L14:
	testl	%r12d, %r12d
	jne	.L322
	movq	16(%rsp), %rax
	movq	24(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	32(%rbp), %r15
	movl	232(%rsp), %r12d
	movq	8(%rbp), %r14
	testq	%rsi, %rsi
	movq	(%rax), %r10
	movq	%rsi, %rax
	cmove	%rbp, %rax
	cmpq	$0, 64(%rsp)
	movl	(%r15), %ecx
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 128(%rsp)
	movl	%ecx, 48(%rsp)
	cmovne	%rdx, %rax
	testl	%r12d, %r12d
	movq	%rax, 104(%rsp)
	movq	40(%rsp), %rax
	setne	103(%rsp)
	movzbl	103(%rsp), %edi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L29
	testb	%dil, %dil
	je	.L29
	andl	$7, %ecx
	movslq	%ecx, %r12
	jne	.L323
	.p2align 4,,10
	.p2align 3
.L29:
	testq	%rax, %rax
	movq	%r13, 8(%rsp)
	movq	%r11, %r13
	movq	%r10, %r11
	movq	%r14, %r10
	je	.L324
	.p2align 4,,10
	.p2align 3
.L70:
	movq	8(%rsp), %rax
	leaq	152(%rsp), %rdi
	leaq	144(%rsp), %rsi
	movl	16(%rbp), %r12d
	movq	%r11, 144(%rsp)
	movl	$4, %r14d
	movq	%rdi, 32(%rsp)
	movq	%rsi, 56(%rsp)
	movq	%rax, 152(%rsp)
	movq	%rax, %rbx
	movq	%r11, %rax
.L96:
	cmpq	%rax, %r13
	je	.L97
.L116:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %r13
	jb	.L207
	cmpq	%rbx, %r10
	jbe	.L211
	movl	(%rax), %edx
	testl	%edx, %edx
	sete	%dil
	cmpl	$160, %edx
	sete	%sil
	orb	%sil, %dil
	jne	.L229
	leal	-24(%rdx), %esi
	cmpl	$103, %esi
	jbe	.L229
	cmpl	$16, %edx
	ja	.L101
	leal	-1(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
.L102:
	testb	%cl, %cl
	je	.L108
.L328:
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%rbx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %r13
	movq	%rax, 144(%rsp)
	jne	.L116
	.p2align 4,,10
	.p2align 3
.L97:
	cmpq	$0, 24(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L325
.L117:
	addl	$1, 20(%rbp)
	testb	$1, 16(%rbp)
	jne	.L326
	cmpq	%rbx, 8(%rsp)
	movq	%r11, 56(%rsp)
	jnb	.L214
	movq	72(%rsp), %r12
	movq	0(%rbp), %rax
	movq	%r10, 32(%rsp)
	movq	%r12, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r12
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r12d
	popq	%rdi
	movq	32(%rsp), %r10
	je	.L121
	movq	136(%rsp), %r14
	movq	56(%rsp), %r11
	cmpq	%rbx, %r14
	jne	.L327
.L120:
	testl	%r12d, %r12d
	jne	.L223
.L174:
	movq	40(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	0(%rbp), %rsi
	movq	96(%rax), %rax
	movq	(%rdi), %r11
	movl	(%r15), %edi
	movq	%rsi, 8(%rsp)
	testq	%rax, %rax
	movl	%edi, 48(%rsp)
	jne	.L70
.L324:
	cmpq	%r11, %r13
	je	.L200
	movq	8(%rsp), %rbx
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r10
	jb	.L201
	movl	48(%rsp), %edx
	leaq	map_from_tcvn_low(%rip), %r12
	leaq	.L79(%rip), %r9
	movq	%rbp, 32(%rsp)
	movq	%r11, %rcx
	movq	%r11, %r14
	movq	%r13, %rbp
	.p2align 4,,10
	.p2align 3
.L72:
	movzbl	(%rcx), %eax
	cmpl	$23, %eax
	ja	.L73
	movzwl	(%r12,%rax,2), %eax
.L74:
	sarl	$3, %edx
	leal	-65(%rax), %edi
	testl	%edx, %edx
	je	.L75
.L332:
	leal	-768(%rax), %r8d
	cmpl	$63, %r8d
	ja	.L76
	cmpl	$35, %r8d
	ja	.L77
	movslq	(%r9,%r8,4), %r8
	addq	%r9, %r8
	jmp	*%r8
	.section	.rodata
	.align 4
	.align 4
.L79:
	.long	.L78-.L79
	.long	.L80-.L79
	.long	.L77-.L79
	.long	.L81-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L82-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L77-.L79
	.long	.L203-.L79
	.text
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	1(%rbx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dl, (%rbx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	leal	-192(%rdx), %esi
	cmpl	$105, %esi
	ja	.L103
	leal	-176(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L328
.L108:
	leal	-209(%rdx), %ecx
	cmpl	$7618, %ecx
	jbe	.L329
.L110:
	cmpq	$0, 104(%rsp)
	je	.L213
	testb	$8, 16(%rbp)
	jne	.L330
.L114:
	testb	$2, %r12b
	jne	.L331
.L213:
	movl	$6, %r14d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L207:
	movl	$7, %r14d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$127, %eax
	jbe	.L74
	leaq	map_from_tcvn_high(%rip), %rdi
	addl	$-128, %eax
	sarl	$3, %edx
	testl	%edx, %edx
	movzwl	(%rdi,%rax,2), %eax
	leal	-65(%rax), %edi
	jne	.L332
.L75:
	cmpl	$367, %edi
	ja	.L92
.L91:
	sall	$3, %eax
	movl	%eax, (%r15)
.L93:
	addq	$1, %rcx
.L90:
	cmpq	%rcx, %rbp
	je	.L333
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r10
	jb	.L202
	movl	(%r15), %edx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$64, %edx
	ja	.L84
	.p2align 4,,10
	.p2align 3
.L297:
	movl	%edx, (%rbx)
	movl	$0, (%r15)
.L183:
	addq	$8, %rbx
	cmpq	%rbx, %r10
	movq	%rsi, %rbx
	jb	.L90
.L92:
	movl	%eax, (%rbx)
	addq	$4, %rbx
	jmp	.L93
.L305:
	movl	56(%rsp), %eax
	movl	112(%rsp), %edi
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$367, %edi
	movl	%edx, (%rbx)
	movl	$0, (%r15)
	ja	.L183
	movq	%rsi, %rbx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L211:
	movl	$5, %r14d
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L121:
	cmpl	$5, %r14d
	movl	%r14d, %r12d
	jne	.L120
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L103:
	leal	-360(%rdx), %esi
	cmpl	$1, %esi
	jbe	.L334
	leal	-416(%rdx), %esi
	cmpl	$16, %esi
	ja	.L105
	leal	-292(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rbp, %r13
	movq	32(%rsp), %rbp
	movq	%r14, %r11
	movl	$5, %r14d
.L71:
	cmpq	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L117
.L325:
	movq	24(%rsp), %rax
	movl	%r14d, %r9d
	movq	%rbx, (%rax)
.L13:
	addq	$168, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$136, 56(%rsp)
	movl	$185, %r11d
.L83:
	cmpl	$64, %edx
	jbe	.L297
	leaq	comp_table_data(%rip), %r13
	movl	%r11d, %r8d
	movzwl	0(%r13,%r8,4), %r8d
	cmpl	%r8d, %edx
	ja	.L76
	movl	56(%rsp), %r13d
	movl	%edi, 112(%rsp)
	movl	%eax, 56(%rsp)
	.p2align 4,,10
	.p2align 3
.L86:
	leal	0(%r13,%r11), %eax
	leaq	comp_table_data(%rip), %r8
	shrl	%eax
	movl	%eax, %edi
	movzwl	(%r8,%rdi,4), %r8d
	cmpl	%r8d, %edx
	je	.L87
	jnb	.L88
	cmpl	%eax, %r13d
	je	.L305
	movl	%eax, %r11d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$112, 56(%rsp)
	movl	$135, %r11d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$0, 56(%rsp)
	movl	$27, %r11d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$78, 56(%rsp)
	movl	$111, %r11d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L105:
	leal	-768(%rdx), %esi
	cmpl	$35, %esi
	jbe	.L335
	leal	-7840(%rdx), %esi
	cmpl	$89, %esi
	ja	.L107
	leal	-7663(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L88:
	cmpl	%eax, %r13d
	je	.L336
	movl	%eax, %r13d
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L326:
	movq	64(%rsp), %rcx
	movq	%rbx, 0(%rbp)
	movl	%r14d, %r9d
	movq	128(%rsp), %rax
	movq	%r13, %r11
	addq	%rax, (%rcx)
.L119:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 103(%rsp)
	je	.L13
	cmpl	$7, %r9d
	jne	.L13
	movq	16(%rsp), %rax
	movq	%r11, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L177
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbp), %rcx
	je	.L179
.L178:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L178
.L179:
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r14, %r11
	movq	%rbp, %r13
	movl	$4, %r14d
	movq	32(%rsp), %rbp
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L214:
	movl	%r14d, %r12d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L334:
	leal	-238(%rdx), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L102
.L336:
	leaq	comp_table_data(%rip), %r8
	movl	56(%rsp), %eax
	movl	112(%rsp), %edi
	movzwl	(%r8,%r11,4), %r8d
	cmpl	%r8d, %edx
	jne	.L76
	movq	%r11, %rdi
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	comp_table_data(%rip), %rax
	addq	$1, %rcx
	movzwl	2(%rax,%rdi,4), %eax
	movl	%eax, (%rbx)
	movl	$0, (%r15)
	movq	%rsi, %rbx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L327:
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	movl	48(%rsp), %eax
	movl	%eax, (%r15)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L337
	movq	8(%rsp), %rax
	movl	16(%rbp), %ebx
	movq	%r11, 144(%rsp)
	movq	%rax, 152(%rsp)
	movq	%rax, %rdx
	leaq	152(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 48(%rsp)
.L152:
	cmpq	%r11, %r13
	je	.L317
.L173:
	leaq	4(%r11), %rcx
	cmpq	%rcx, %r13
	jb	.L317
	cmpq	%rdx, %r14
	jbe	.L154
	movl	(%r11), %eax
	testl	%eax, %eax
	sete	%dil
	cmpl	$160, %eax
	sete	%sil
	orb	%sil, %dil
	jne	.L230
	leal	-24(%rax), %esi
	cmpl	$103, %esi
	jbe	.L230
	cmpl	$16, %eax
	ja	.L158
	leal	-1(%rax), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
.L159:
	testb	%cl, %cl
	je	.L165
	leaq	1(%rdx), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%rdx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rdx
	leaq	4(%rax), %r11
	cmpq	%r11, %r13
	movq	%r11, 144(%rsp)
	jne	.L173
.L317:
	cmpq	136(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	%r11, (%rax)
	je	.L125
.L126:
	leaq	__PRETTY_FUNCTION__.9186(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	$432, %edx
	ja	.L297
	movl	$77, %r11d
	movl	$28, %r13d
	movl	%eax, 56(%rsp)
	movl	%edi, 112(%rsp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L323:
	testq	%rsi, %rsi
	jne	.L338
	cmpl	$4, %r12d
	movq	%r10, 144(%rsp)
	movq	%r13, 152(%rsp)
	ja	.L31
	leaq	136(%rsp), %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%rsp)
.L32:
	movzbl	4(%r15,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r12, %rax
	jne	.L32
	movq	%r10, %rax
	subq	%r12, %rax
	addq	$4, %rax
	cmpq	%rax, %r11
	jb	.L339
	cmpq	%r14, %r13
	jnb	.L198
	leaq	1(%r10), %rax
	leaq	135(%rsp), %rsi
.L40:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %r12
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %r12
	movb	%dl, (%rsi,%r12)
	ja	.L227
	cmpq	%rcx, %r11
	ja	.L40
.L227:
	movq	8(%rsp), %rax
	movq	%rax, 144(%rsp)
	movl	136(%rsp), %eax
	testl	%eax, %eax
	sete	%cl
	cmpl	$160, %eax
	sete	%dl
	orb	%dl, %cl
	jne	.L228
	leal	-24(%rax), %edx
	cmpl	$103, %edx
	jbe	.L228
	cmpl	$16, %eax
	ja	.L46
	leal	-1(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
.L47:
	testb	%dl, %dl
	je	.L53
	leaq	1(%r13), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, 0(%r13)
.L320:
	movq	144(%rsp), %rax
	addq	$4, %rax
	cmpq	8(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L340
.L44:
	movl	(%r15), %edx
	subq	8(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L341
	movq	16(%rsp), %rsi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r13
	movl	%edx, 48(%rsp)
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r10
	movq	40(%rsp), %rax
	movl	%edx, (%r15)
	movq	96(%rax), %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L335:
	leal	-627(%rdx), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L102
.L331:
	movq	104(%rsp), %rcx
	addq	$4, %rax
	movl	$6, %r14d
	movq	%rax, 144(%rsp)
	addq	$1, (%rcx)
	jmp	.L96
.L337:
	cmpq	%r11, %r13
	je	.L342
	movq	8(%rsp), %rbx
	leaq	4(%rbx), %r9
	cmpq	%r9, %r14
	jb	.L343
	movzbl	(%r11), %eax
	movq	%r10, 32(%rsp)
	movl	48(%rsp), %edx
	cmpl	$23, %eax
	ja	.L131
	.p2align 4,,10
	.p2align 3
.L346:
	leaq	map_from_tcvn_low(%rip), %rdi
	movzwl	(%rdi,%rax,2), %eax
.L132:
	sarl	$3, %edx
	leal	-65(%rax), %ecx
	testl	%edx, %edx
	je	.L133
	leal	-768(%rax), %esi
	cmpl	$63, %esi
	ja	.L134
	cmpl	$35, %esi
	ja	.L77
	leaq	.L136(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L136:
	.long	.L135-.L136
	.long	.L137-.L136
	.long	.L77-.L136
	.long	.L138-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L139-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L77-.L136
	.long	.L216-.L136
	.text
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rdx
	leaq	4(%rax), %r11
	movq	%r11, 144(%rsp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L133:
	cmpl	$367, %ecx
	ja	.L149
.L148:
	sall	$3, %eax
	movl	%eax, (%r15)
.L150:
	addq	$1, %r11
.L147:
	cmpq	%r11, %r13
	je	.L344
	leaq	4(%rbx), %r9
	cmpq	%r9, %r14
	jb	.L345
	movzbl	(%r11), %eax
	movl	(%r15), %edx
	cmpl	$23, %eax
	jbe	.L346
.L131:
	cmpl	$127, %eax
	jbe	.L132
	leaq	map_from_tcvn_high(%rip), %rcx
	addl	$-128, %eax
	movzwl	(%rcx,%rax,2), %eax
	jmp	.L132
.L137:
	cmpl	$64, %edx
	jbe	.L298
	cmpl	$432, %edx
	jbe	.L347
.L298:
	movl	%edx, (%rbx)
	movl	$0, (%r15)
.L186:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	movq	%r9, %rbx
	jb	.L147
.L149:
	movl	%eax, (%rbx)
	addq	$4, %rbx
	jmp	.L150
.L201:
	movq	%r11, %rcx
	movq	8(%rsp), %rbx
	movl	$5, %r14d
	jmp	.L71
.L310:
	movl	48(%rsp), %eax
.L134:
	cmpl	$367, %ecx
	movl	%edx, (%rbx)
	movl	$0, (%r15)
	ja	.L186
	movq	%r9, %rbx
	jmp	.L148
.L330:
	movq	%r11, 120(%rsp)
	movq	%r10, 112(%rsp)
	subq	$8, %rsp
	pushq	112(%rsp)
	movq	32(%rsp), %rax
	movq	%r13, %r8
	movq	48(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	%rbp, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r14d
	popq	%r8
	cmpl	$6, %r14d
	popq	%r9
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movq	112(%rsp), %r10
	movq	120(%rsp), %r11
	je	.L114
	cmpl	$5, %r14d
	jne	.L96
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L322:
	cmpq	$0, 24(%rsp)
	jne	.L348
	cmpl	$1, %r12d
	movq	32(%rbp), %rbx
	jne	.L17
	movl	(%rbx), %r12d
	movq	0(%rbp), %rax
	testl	%r12d, %r12d
	je	.L18
	movq	40(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L349
	movl	$0, (%rbx)
.L18:
	testl	%edx, %edx
	je	.L24
.L21:
	movq	%rax, 0(%rbp)
	xorl	%r9d, %r9d
	jmp	.L13
.L158:
	leal	-192(%rax), %esi
	cmpl	$105, %esi
	jbe	.L350
	leal	-360(%rax), %esi
	cmpl	$1, %esi
	jbe	.L351
	leal	-416(%rax), %esi
	cmpl	$16, %esi
	ja	.L162
	leal	-292(%rax), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L159
.L198:
	movl	$5, %r9d
	jmp	.L13
.L107:
	movl	%edx, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	jne	.L108
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L96
.L223:
	movq	%r13, %r11
	movl	%r12d, %r9d
	jmp	.L119
.L200:
	movq	%r13, %rcx
	movq	8(%rsp), %rbx
	movl	$4, %r14d
	jmp	.L71
.L329:
	movl	$7731, %esi
	movl	$55, %r8d
	xorl	%edi, %edi
	movl	$55, %ecx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L352:
	cmpl	%ecx, %edi
	je	.L110
	movl	%ecx, %r8d
.L113:
	leal	(%r8,%rdi), %ecx
	leaq	decomp_table(%rip), %r9
	movl	%ecx, %esi
	shrl	%esi
	movzwl	(%r9,%rsi,4), %esi
.L109:
	shrl	%ecx
	cmpl	%esi, %edx
	je	.L111
	jb	.L352
	cmpl	%ecx, %edi
	je	.L353
	movl	%ecx, %edi
	jmp	.L113
.L356:
	movq	%r10, 56(%rsp)
	subq	$8, %rsp
	movq	%r13, %r8
	pushq	112(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %rsi
	movq	48(%rsp), %r9
	movq	64(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r11
	movq	152(%rsp), %rdx
	movq	56(%rsp), %r10
	je	.L171
	cmpl	$5, %eax
	jne	.L152
.L154:
	movq	136(%rsp), %r14
	movq	16(%rsp), %rax
	cmpq	%r14, %rdx
	movq	%r11, (%rax)
	jne	.L126
.L130:
	cmpq	8(%rsp), %r14
	jne	.L120
.L128:
	subl	$1, 20(%rbp)
	jmp	.L120
.L17:
	movq	$0, (%rbx)
	xorl	%r9d, %r9d
	testb	$1, 16(%rbp)
	jne	.L13
	movq	72(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	%r12
.L321:
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	96(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	popq	%r13
	movl	%eax, %r9d
	popq	%r14
	jmp	.L13
.L350:
	leal	-176(%rax), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L159
.L228:
	leaq	1(%r13), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, 0(%r13)
	jmp	.L320
.L135:
	xorl	%r10d, %r10d
	movl	$27, %esi
.L140:
	cmpl	$64, %edx
	jbe	.L298
	leaq	comp_table_data(%rip), %r8
	movl	%esi, %edi
	movzwl	(%r8,%rdi,4), %edi
	cmpl	%edi, %edx
	ja	.L134
	movl	%eax, 48(%rsp)
.L143:
	leal	(%r10,%rsi), %edi
	leaq	comp_table_data(%rip), %rax
	shrl	%edi
	movl	%edi, %edi
	movzwl	(%rax,%rdi,4), %eax
	movq	%rdi, %r8
	cmpl	%eax, %edx
	je	.L144
	jnb	.L145
	cmpl	%edi, %r10d
	je	.L310
	movl	%edi, %esi
	jmp	.L143
.L216:
	movl	$136, %r10d
	movl	$185, %esi
	jmp	.L140
.L138:
	movl	$78, %r10d
	movl	$111, %esi
	jmp	.L140
.L139:
	movl	$112, %r10d
	movl	$135, %esi
	jmp	.L140
.L353:
	leaq	decomp_table(%rip), %rsi
	movl	%r8d, %ecx
	movzwl	(%rsi,%rcx,4), %ecx
	cmpl	%ecx, %edx
	jne	.L110
	movl	%r8d, %ecx
.L111:
	leaq	1(%rbx), %rdx
	cmpq	%rdx, %r10
	jbe	.L211
	leaq	decomp_table(%rip), %rdi
	movq	%rdx, 152(%rsp)
	movzbl	2(%rdi,%rcx,4), %eax
	movb	%al, (%rbx)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	3(%rdi,%rcx,4), %edx
	movb	%dl, (%rax)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L96
.L345:
	movq	16(%rsp), %rax
	cmpq	%r14, %rbx
	movq	32(%rsp), %r10
	movq	%r11, (%rax)
	je	.L130
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	%edi, %r10d
	je	.L354
	movl	%r8d, %r10d
	jmp	.L143
.L165:
	leal	-209(%rax), %ecx
	cmpl	$7618, %ecx
	jbe	.L355
.L167:
	cmpq	$0, 104(%rsp)
	je	.L317
	testb	$8, 16(%rbp)
	jne	.L356
.L171:
	testb	$2, %bl
	je	.L317
	movq	104(%rsp), %rax
	addq	$4, %r11
	movq	%r11, 144(%rsp)
	addq	$1, (%rax)
	jmp	.L152
.L344:
	movq	16(%rsp), %rax
	cmpq	%r14, %rbx
	movq	%r13, (%rax)
	jne	.L126
.L125:
	leaq	__PRETTY_FUNCTION__.9186(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%r11, %rdx
	movq	16(%rsp), %rax
	subq	%r10, %rdx
	addq	%r12, %rdx
	cmpq	$4, %rdx
	movq	%r11, (%rax)
	ja	.L34
	cmpq	%r12, %rdx
	leaq	1(%r10), %rax
	jbe	.L36
.L37:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%r15,%r12)
	addq	$1, %r12
	cmpq	%r12, %rdx
	jne	.L37
.L36:
	movl	$7, %r9d
	jmp	.L13
.L351:
	leal	-238(%rax), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L159
.L354:
	leaq	comp_table_data(%rip), %rdi
	movl	48(%rsp), %eax
	movzwl	(%rdi,%rsi,4), %edi
	cmpl	%edi, %edx
	jne	.L134
	movq	%rsi, %rdi
.L144:
	leaq	comp_table_data(%rip), %rax
	addq	$1, %r11
	movzwl	2(%rax,%rdi,4), %eax
	movl	%eax, (%rbx)
	movl	$0, (%r15)
	movq	%r9, %rbx
	jmp	.L147
.L162:
	leal	-768(%rax), %esi
	cmpl	$35, %esi
	ja	.L163
	leal	-627(%rax), %ecx
	leaq	from_ucs4(%rip), %rsi
	movzbl	(%rsi,%rcx), %ecx
	jmp	.L159
.L46:
	leal	-192(%rax), %edx
	cmpl	$105, %edx
	ja	.L48
	leal	-176(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L47
.L349:
	leaq	4(%rax), %r14
	cmpq	8(%rbp), %r14
	ja	.L198
	movl	%r12d, %edx
	sarl	$3, %edx
	movl	%edx, (%rax)
	testb	$1, 16(%rbp)
	movl	$0, (%rbx)
	jne	.L357
	movq	72(%rsp), %r15
	movq	%rax, 152(%rsp)
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	104(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	cmpl	$4, %eax
	movl	%eax, %r9d
	popq	%r15
	popq	%rdx
	je	.L24
	cmpq	152(%rsp), %r14
	jne	.L358
.L25:
	testl	%r9d, %r9d
	jne	.L13
.L24:
	movq	72(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	pushq	%rax
	pushq	$1
	jmp	.L321
.L340:
	movq	16(%rsp), %rcx
	movq	40(%rsp), %rax
	movq	(%rcx), %r10
	movl	(%r15), %ecx
	movq	96(%rax), %rax
	movl	%ecx, 48(%rsp)
	jmp	.L29
.L163:
	leal	-7840(%rax), %esi
	cmpl	$89, %esi
	ja	.L164
	leal	-7663(%rax), %ecx
	leaq	from_ucs4(%rip), %rdi
	movzbl	(%rdi,%rcx), %ecx
	jmp	.L159
.L48:
	leal	-360(%rax), %edx
	cmpl	$1, %edx
	jbe	.L359
	leal	-416(%rax), %edx
	cmpl	$16, %edx
	ja	.L50
	leal	-292(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L47
.L343:
	movq	16(%rsp), %rax
	cmpq	%rbx, %r14
	movq	%r11, (%rax)
	je	.L128
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L347:
	movl	$77, %esi
	movl	$28, %r10d
	movl	%eax, 48(%rsp)
	jmp	.L143
.L164:
	movl	%eax, %esi
	shrl	$7, %esi
	cmpl	$7168, %esi
	jne	.L165
	movq	%rcx, 144(%rsp)
	movq	%rcx, %r11
	jmp	.L152
.L53:
	leal	-209(%rax), %edx
	cmpl	$7618, %edx
	jbe	.L360
.L55:
	cmpq	$0, 104(%rsp)
	je	.L199
	testb	$8, %bl
	jne	.L361
	andl	$2, %ebx
	je	.L199
	movq	8(%rsp), %rdx
.L182:
	movq	104(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 144(%rsp)
.L62:
	cmpq	8(%rsp), %rax
	jne	.L44
.L199:
	movl	$6, %r9d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r13, %r11
	movq	8(%rsp), %r13
	movq	16(%rsp), %rax
	cmpq	%r13, %r14
	movq	%r11, (%rax)
	je	.L125
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L359:
	leal	-238(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L47
.L355:
	movl	$7731, %esi
	movl	$55, %edi
	xorl	%r8d, %r8d
	movl	$55, %ecx
	jmp	.L166
.L362:
	cmpl	%ecx, %r8d
	je	.L167
	movl	%ecx, %edi
.L170:
	leal	(%rdi,%r8), %ecx
	leaq	decomp_table(%rip), %r9
	movl	%ecx, %esi
	shrl	%esi
	movzwl	(%r9,%rsi,4), %esi
.L166:
	shrl	%ecx
	cmpl	%esi, %eax
	je	.L168
	jb	.L362
	cmpl	%ecx, %r8d
	je	.L363
	movl	%ecx, %r8d
	jmp	.L170
.L363:
	leaq	decomp_table(%rip), %rsi
	movl	%edi, %ecx
	movzwl	(%rsi,%rcx,4), %ecx
	cmpl	%ecx, %eax
	jne	.L167
	movl	%edi, %ecx
.L168:
	leaq	1(%rdx), %rax
	cmpq	%rax, %r14
	jbe	.L154
	leaq	decomp_table(%rip), %rdi
	movq	%rax, 152(%rsp)
	movzbl	2(%rdi,%rcx,4), %eax
	movb	%al, (%rdx)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movzbl	3(%rdi,%rcx,4), %edx
	movb	%dl, (%rax)
	movq	144(%rsp), %rax
	movq	152(%rsp), %rdx
	leaq	4(%rax), %r11
	movq	%r11, 144(%rsp)
	jmp	.L152
.L360:
	movl	$7731, %edi
	movl	$55, %ecx
	xorl	%esi, %esi
	movl	$55, %edx
	leaq	decomp_table(%rip), %r8
	jmp	.L54
.L364:
	cmpl	%edx, %esi
	je	.L55
	movl	%edx, %ecx
.L58:
	leal	(%rcx,%rsi), %edx
	movl	%edx, %edi
	shrl	%edi
	movzwl	(%r8,%rdi,4), %edi
.L54:
	shrl	%edx
	cmpl	%edi, %eax
	je	.L56
	jb	.L364
	cmpl	%edx, %esi
	je	.L365
	movl	%edx, %esi
	jmp	.L58
.L77:
	call	abort@PLT
.L50:
	leal	-768(%rax), %edx
	cmpl	$35, %edx
	ja	.L51
	leal	-627(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L47
.L51:
	leal	-7840(%rax), %edx
	cmpl	$89, %edx
	ja	.L52
	leal	-7663(%rax), %edx
	leaq	from_ucs4(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	jmp	.L47
.L341:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L52:
	movl	%eax, %edx
	shrl	$7, %edx
	cmpl	$7168, %edx
	jne	.L53
	movq	8(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L44
.L348:
	leaq	__PRETTY_FUNCTION__.9186(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L361:
	movq	8(%rsp), %rax
	movq	%r11, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r10, %rdx
	movq	%rbp, %rsi
	addq	%r12, %rax
	movq	%rax, 40(%rsp)
	pushq	112(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%r11
	movq	48(%rsp), %r11
	movq	144(%rsp), %rax
	je	.L366
	cmpq	8(%rsp), %rax
	jne	.L44
	cmpl	$7, %r9d
	jne	.L65
	movq	8(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 32(%rsp)
	je	.L367
	movl	(%r15), %eax
	movq	%r12, %rcx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	16(%rsp), %rcx
	addq	%rdx, (%rcx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r12
	jle	.L368
	cmpq	$4, %r12
	ja	.L369
	orl	%r12d, %eax
	testq	%r12, %r12
	movl	%eax, (%r15)
	je	.L36
	movq	8(%rsp), %rcx
	xorl	%eax, %eax
.L69:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L69
	jmp	.L36
.L365:
	leaq	decomp_table(%rip), %rsi
	movl	%ecx, %edx
	movzwl	(%rsi,%rdx,4), %edx
	cmpl	%edx, %eax
	jne	.L55
	movl	%ecx, %edx
.L56:
	leaq	1(%r13), %rax
	cmpq	%rax, %r14
	jbe	.L198
	leaq	decomp_table(%rip), %rcx
	movq	%rax, 152(%rsp)
	movzbl	2(%rcx,%rdx,4), %eax
	movzbl	3(%rcx,%rdx,4), %edx
	movb	%al, 0(%r13)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rax)
	jmp	.L320
.L31:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L177:
	leaq	__PRETTY_FUNCTION__.9186(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L358:
	movl	%r12d, (%rbx)
	jmp	.L25
.L357:
	movq	%r14, %rax
	jmp	.L21
.L338:
	leaq	__PRETTY_FUNCTION__.9186(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L34:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L369:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L368:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L367:
	leaq	__PRETTY_FUNCTION__.9105(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L65:
	testl	%r9d, %r9d
	jne	.L13
	movq	40(%rsp), %rax
	movq	16(%rsp), %rsi
	movl	(%r15), %ecx
	movq	96(%rax), %rax
	movq	(%rsi), %r10
	movl	%ecx, 48(%rsp)
	jmp	.L29
.L366:
	andb	$2, %bl
	movq	%rax, %rdx
	je	.L62
	jmp	.L182
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9105, @object
	.size	__PRETTY_FUNCTION__.9105, 21
__PRETTY_FUNCTION__.9105:
	.string	"to_tcvn5712_1_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9186, @object
	.size	__PRETTY_FUNCTION__.9186, 6
__PRETTY_FUNCTION__.9186:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	decomp_table, @object
	.size	decomp_table, 224
decomp_table:
	.value	209
	.byte	78
	.byte	-78
	.value	241
	.byte	110
	.byte	-78
	.value	262
	.byte	67
	.byte	-77
	.value	263
	.byte	99
	.byte	-77
	.value	313
	.byte	76
	.byte	-77
	.value	314
	.byte	108
	.byte	-77
	.value	323
	.byte	78
	.byte	-77
	.value	324
	.byte	110
	.byte	-77
	.value	340
	.byte	82
	.byte	-77
	.value	341
	.byte	114
	.byte	-77
	.value	346
	.byte	83
	.byte	-77
	.value	347
	.byte	115
	.byte	-77
	.value	377
	.byte	90
	.byte	-77
	.value	378
	.byte	122
	.byte	-77
	.value	500
	.byte	71
	.byte	-77
	.value	501
	.byte	103
	.byte	-77
	.value	504
	.byte	78
	.byte	-80
	.value	505
	.byte	110
	.byte	-80
	.value	7684
	.byte	66
	.byte	-76
	.value	7685
	.byte	98
	.byte	-76
	.value	7692
	.byte	68
	.byte	-76
	.value	7693
	.byte	100
	.byte	-76
	.value	7716
	.byte	72
	.byte	-76
	.value	7717
	.byte	104
	.byte	-76
	.value	7728
	.byte	75
	.byte	-77
	.value	7729
	.byte	107
	.byte	-77
	.value	7730
	.byte	75
	.byte	-76
	.value	7731
	.byte	107
	.byte	-76
	.value	7734
	.byte	76
	.byte	-76
	.value	7735
	.byte	108
	.byte	-76
	.value	7742
	.byte	77
	.byte	-77
	.value	7743
	.byte	109
	.byte	-77
	.value	7746
	.byte	77
	.byte	-76
	.value	7747
	.byte	109
	.byte	-76
	.value	7750
	.byte	78
	.byte	-76
	.value	7751
	.byte	110
	.byte	-76
	.value	7764
	.byte	80
	.byte	-77
	.value	7765
	.byte	112
	.byte	-77
	.value	7770
	.byte	82
	.byte	-76
	.value	7771
	.byte	114
	.byte	-76
	.value	7778
	.byte	83
	.byte	-76
	.value	7779
	.byte	115
	.byte	-76
	.value	7788
	.byte	84
	.byte	-76
	.value	7789
	.byte	116
	.byte	-76
	.value	7804
	.byte	86
	.byte	-78
	.value	7805
	.byte	118
	.byte	-78
	.value	7806
	.byte	86
	.byte	-76
	.value	7807
	.byte	118
	.byte	-76
	.value	7808
	.byte	87
	.byte	-80
	.value	7809
	.byte	119
	.byte	-80
	.value	7810
	.byte	87
	.byte	-77
	.value	7811
	.byte	119
	.byte	-77
	.value	7816
	.byte	87
	.byte	-76
	.value	7817
	.byte	119
	.byte	-76
	.value	7826
	.byte	90
	.byte	-76
	.value	7827
	.byte	122
	.byte	-76
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 267
from_ucs4:
	.byte	0
	.byte	0
	.byte	3
	.byte	0
	.byte	0
	.byte	0
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	-128
	.byte	-125
	.byte	-94
	.byte	-126
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-121
	.byte	-118
	.byte	-93
	.byte	0
	.byte	-115
	.byte	-112
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-110
	.byte	-107
	.byte	-92
	.byte	-108
	.byte	0
	.byte	0
	.byte	0
	.byte	-99
	.byte	1
	.byte	0
	.byte	0
	.byte	22
	.byte	0
	.byte	0
	.byte	-75
	.byte	-72
	.byte	-87
	.byte	-73
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-52
	.byte	-48
	.byte	-86
	.byte	0
	.byte	-41
	.byte	-35
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-33
	.byte	-29
	.byte	-85
	.byte	-30
	.byte	0
	.byte	0
	.byte	0
	.byte	-17
	.byte	-13
	.byte	0
	.byte	0
	.byte	-3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-95
	.byte	-88
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-89
	.byte	-82
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-113
	.byte	-36
	.byte	-97
	.byte	-14
	.byte	-91
	.byte	-84
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-90
	.byte	-83
	.byte	-80
	.byte	-77
	.byte	0
	.byte	-78
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-79
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-76
	.byte	-124
	.byte	-71
	.byte	-127
	.byte	-74
	.byte	-60
	.byte	-54
	.byte	-63
	.byte	-57
	.byte	-62
	.byte	-56
	.byte	-61
	.byte	-55
	.byte	-122
	.byte	-53
	.byte	-64
	.byte	-66
	.byte	-81
	.byte	-69
	.byte	-70
	.byte	-68
	.byte	-65
	.byte	-67
	.byte	-123
	.byte	-58
	.byte	-117
	.byte	-47
	.byte	-120
	.byte	-50
	.byte	-119
	.byte	-49
	.byte	-38
	.byte	-43
	.byte	-59
	.byte	-46
	.byte	-51
	.byte	-45
	.byte	-39
	.byte	-44
	.byte	-116
	.byte	-42
	.byte	-114
	.byte	-40
	.byte	-111
	.byte	-34
	.byte	-106
	.byte	-28
	.byte	-109
	.byte	-31
	.byte	-1
	.byte	-24
	.byte	-37
	.byte	-27
	.byte	-32
	.byte	-26
	.byte	-16
	.byte	-25
	.byte	-105
	.byte	-23
	.byte	-101
	.byte	-19
	.byte	-104
	.byte	-22
	.byte	-103
	.byte	-21
	.byte	-102
	.byte	-20
	.byte	-100
	.byte	-18
	.byte	2
	.byte	-12
	.byte	-98
	.byte	-15
	.byte	17
	.byte	-8
	.byte	4
	.byte	-11
	.byte	5
	.byte	-10
	.byte	6
	.byte	-9
	.byte	18
	.byte	-7
	.byte	19
	.byte	-6
	.byte	23
	.byte	-2
	.byte	20
	.byte	-5
	.byte	21
	.byte	-4
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 744
comp_table_data:
	.value	65
	.value	192
	.value	69
	.value	200
	.value	73
	.value	204
	.value	78
	.value	504
	.value	79
	.value	210
	.value	85
	.value	217
	.value	87
	.value	7808
	.value	89
	.value	7922
	.value	97
	.value	224
	.value	101
	.value	232
	.value	105
	.value	236
	.value	110
	.value	505
	.value	111
	.value	242
	.value	117
	.value	249
	.value	119
	.value	7809
	.value	121
	.value	7923
	.value	194
	.value	7846
	.value	202
	.value	7872
	.value	212
	.value	7890
	.value	226
	.value	7847
	.value	234
	.value	7873
	.value	244
	.value	7891
	.value	258
	.value	7856
	.value	259
	.value	7857
	.value	416
	.value	7900
	.value	417
	.value	7901
	.value	431
	.value	7914
	.value	432
	.value	7915
	.value	65
	.value	193
	.value	67
	.value	262
	.value	69
	.value	201
	.value	71
	.value	500
	.value	73
	.value	205
	.value	75
	.value	7728
	.value	76
	.value	313
	.value	77
	.value	7742
	.value	78
	.value	323
	.value	79
	.value	211
	.value	80
	.value	7764
	.value	82
	.value	340
	.value	83
	.value	346
	.value	85
	.value	218
	.value	87
	.value	7810
	.value	89
	.value	221
	.value	90
	.value	377
	.value	97
	.value	225
	.value	99
	.value	263
	.value	101
	.value	233
	.value	103
	.value	501
	.value	105
	.value	237
	.value	107
	.value	7729
	.value	108
	.value	314
	.value	109
	.value	7743
	.value	110
	.value	324
	.value	111
	.value	243
	.value	112
	.value	7765
	.value	114
	.value	341
	.value	115
	.value	347
	.value	117
	.value	250
	.value	119
	.value	7811
	.value	121
	.value	253
	.value	122
	.value	378
	.value	194
	.value	7844
	.value	202
	.value	7870
	.value	212
	.value	7888
	.value	213
	.value	7756
	.value	226
	.value	7845
	.value	234
	.value	7871
	.value	244
	.value	7889
	.value	245
	.value	7757
	.value	258
	.value	7854
	.value	259
	.value	7855
	.value	360
	.value	7800
	.value	361
	.value	7801
	.value	416
	.value	7898
	.value	417
	.value	7899
	.value	431
	.value	7912
	.value	432
	.value	7913
	.value	65
	.value	195
	.value	69
	.value	7868
	.value	73
	.value	296
	.value	78
	.value	209
	.value	79
	.value	213
	.value	85
	.value	360
	.value	86
	.value	7804
	.value	89
	.value	7928
	.value	97
	.value	227
	.value	101
	.value	7869
	.value	105
	.value	297
	.value	110
	.value	241
	.value	111
	.value	245
	.value	117
	.value	361
	.value	118
	.value	7805
	.value	121
	.value	7929
	.value	194
	.value	7850
	.value	202
	.value	7876
	.value	211
	.value	7756
	.value	212
	.value	7894
	.value	214
	.value	7758
	.value	218
	.value	7800
	.value	226
	.value	7851
	.value	234
	.value	7877
	.value	243
	.value	7757
	.value	244
	.value	7895
	.value	246
	.value	7759
	.value	250
	.value	7801
	.value	258
	.value	7860
	.value	259
	.value	7861
	.value	416
	.value	7904
	.value	417
	.value	7905
	.value	431
	.value	7918
	.value	432
	.value	7919
	.value	65
	.value	7842
	.value	69
	.value	7866
	.value	73
	.value	7880
	.value	79
	.value	7886
	.value	85
	.value	7910
	.value	89
	.value	7926
	.value	97
	.value	7843
	.value	101
	.value	7867
	.value	105
	.value	7881
	.value	111
	.value	7887
	.value	117
	.value	7911
	.value	121
	.value	7927
	.value	194
	.value	7848
	.value	202
	.value	7874
	.value	212
	.value	7892
	.value	226
	.value	7849
	.value	234
	.value	7875
	.value	244
	.value	7893
	.value	258
	.value	7858
	.value	259
	.value	7859
	.value	416
	.value	7902
	.value	417
	.value	7903
	.value	431
	.value	7916
	.value	432
	.value	7917
	.value	65
	.value	7840
	.value	66
	.value	7684
	.value	68
	.value	7692
	.value	69
	.value	7864
	.value	72
	.value	7716
	.value	73
	.value	7882
	.value	75
	.value	7730
	.value	76
	.value	7734
	.value	77
	.value	7746
	.value	78
	.value	7750
	.value	79
	.value	7884
	.value	82
	.value	7770
	.value	83
	.value	7778
	.value	84
	.value	7788
	.value	85
	.value	7908
	.value	86
	.value	7806
	.value	87
	.value	7816
	.value	89
	.value	7924
	.value	90
	.value	7826
	.value	97
	.value	7841
	.value	98
	.value	7685
	.value	100
	.value	7693
	.value	101
	.value	7865
	.value	104
	.value	7717
	.value	105
	.value	7883
	.value	107
	.value	7731
	.value	108
	.value	7735
	.value	109
	.value	7747
	.value	110
	.value	7751
	.value	111
	.value	7885
	.value	114
	.value	7771
	.value	115
	.value	7779
	.value	116
	.value	7789
	.value	117
	.value	7909
	.value	118
	.value	7807
	.value	119
	.value	7817
	.value	121
	.value	7925
	.value	122
	.value	7827
	.value	194
	.value	7852
	.value	202
	.value	7878
	.value	212
	.value	7896
	.value	226
	.value	7853
	.value	234
	.value	7879
	.value	244
	.value	7897
	.value	258
	.value	7862
	.value	259
	.value	7863
	.value	416
	.value	7906
	.value	417
	.value	7907
	.value	431
	.value	7920
	.value	432
	.value	7921
	.align 32
	.type	map_from_tcvn_high, @object
	.size	map_from_tcvn_high, 256
map_from_tcvn_high:
	.value	192
	.value	7842
	.value	195
	.value	193
	.value	7840
	.value	7862
	.value	7852
	.value	200
	.value	7866
	.value	7868
	.value	201
	.value	7864
	.value	7878
	.value	204
	.value	7880
	.value	296
	.value	205
	.value	7882
	.value	210
	.value	7886
	.value	213
	.value	211
	.value	7884
	.value	7896
	.value	7900
	.value	7902
	.value	7904
	.value	7898
	.value	7906
	.value	217
	.value	7910
	.value	360
	.value	160
	.value	258
	.value	194
	.value	202
	.value	212
	.value	416
	.value	431
	.value	272
	.value	259
	.value	226
	.value	234
	.value	244
	.value	417
	.value	432
	.value	273
	.value	7856
	.value	768
	.value	777
	.value	771
	.value	769
	.value	803
	.value	224
	.value	7843
	.value	227
	.value	225
	.value	7841
	.value	7858
	.value	7857
	.value	7859
	.value	7861
	.value	7855
	.value	7860
	.value	7854
	.value	7846
	.value	7848
	.value	7850
	.value	7844
	.value	7872
	.value	7863
	.value	7847
	.value	7849
	.value	7851
	.value	7845
	.value	7853
	.value	232
	.value	7874
	.value	7867
	.value	7869
	.value	233
	.value	7865
	.value	7873
	.value	7875
	.value	7877
	.value	7871
	.value	7879
	.value	236
	.value	7881
	.value	7876
	.value	7870
	.value	7890
	.value	297
	.value	237
	.value	7883
	.value	242
	.value	7892
	.value	7887
	.value	245
	.value	243
	.value	7885
	.value	7891
	.value	7893
	.value	7895
	.value	7889
	.value	7897
	.value	7901
	.value	7903
	.value	7905
	.value	7899
	.value	7907
	.value	249
	.value	7894
	.value	7911
	.value	361
	.value	250
	.value	7909
	.value	7915
	.value	7917
	.value	7919
	.value	7913
	.value	7921
	.value	7923
	.value	7927
	.value	7929
	.value	253
	.value	7925
	.value	7888
	.align 32
	.type	map_from_tcvn_low, @object
	.size	map_from_tcvn_low, 48
map_from_tcvn_low:
	.value	0
	.value	218
	.value	7908
	.value	3
	.value	7914
	.value	7916
	.value	7918
	.value	7
	.value	8
	.value	9
	.value	10
	.value	11
	.value	12
	.value	13
	.value	14
	.value	15
	.value	16
	.value	7912
	.value	7920
	.value	7922
	.value	7926
	.value	7928
	.value	221
	.value	7924
