	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %esi
	movl	(%rax,%rsi,4), %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IBM273//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L4
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L7
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%rsi), %r10d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, (%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 48(%rsp)
	testb	$1, %r10b
	movq	%r9, 16(%rsp)
	movl	208(%rsp), %ebx
	movq	%rdi, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L9
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L9
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L9:
	testl	%ebx, %ebx
	jne	.L189
	movq	(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %ebx
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r14
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, 8(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 80(%rsp)
	movq	40(%rsp), %rax
	setne	95(%rsp)
	movzbl	95(%rsp), %esi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L108
	testb	%sil, %sil
	je	.L108
	movq	32(%r12), %r15
	movl	(%r15), %edx
	andl	$7, %edx
	jne	.L190
.L108:
	movq	$0, 32(%rsp)
.L16:
	leaq	136(%rsp), %rsi
	leaq	to_ucs4(%rip), %r15
	movq	%rsi, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L97:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	(%rdi), %rdi
	addq	%rdi, 32(%rsp)
.L47:
	testq	%rax, %rax
	je	.L191
	movl	16(%r12), %r10d
	leaq	128(%rsp), %rsi
	movq	%r14, %rbx
	movq	%r13, 128(%rsp)
	movq	%r13, %rax
	movl	$4, %r11d
	movq	%r14, 136(%rsp)
	movq	%rsi, 56(%rsp)
	andl	$2, %r10d
.L53:
	cmpq	%rax, %rbp
	je	.L54
.L63:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L113
	cmpq	%rbx, 8(%rsp)
	jbe	.L114
	movl	(%rax), %edx
	cmpl	$65534, %edx
	ja	.L55
	cmpl	$255, %edx
	jbe	.L57
	cmpl	$65534, %edx
	jbe	.L58
.L57:
	leaq	from_ucs4(%rip), %rdi
	movl	%edx, %ecx
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L130
	testl	%edx, %edx
	jne	.L58
.L130:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	$0, 48(%rsp)
	movq	(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L192
.L64:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L193
	cmpq	%rbx, %r14
	jnb	.L117
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 56(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %esi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rsi
	pushq	$0
	movq	32(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rdi
	movl	56(%rsp), %r11d
	je	.L68
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L194
.L67:
	testl	%r10d, %r10d
	jne	.L126
.L96:
	movq	112(%rsp), %rax
	movq	(%rsp), %rsi
	movq	(%r12), %r14
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rsi), %r13
	movq	96(%rax), %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L55:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L195
.L58:
	cmpq	$0, 80(%rsp)
	je	.L116
	testb	$8, 16(%r12)
	jne	.L196
.L61:
	testl	%r10d, %r10d
	jne	.L197
.L116:
	movl	$6, %r11d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L191:
	cmpq	%r13, %rbp
	je	.L110
	movq	8(%rsp), %rax
	leaq	4(%r14), %rdx
	cmpq	%rax, %rdx
	ja	.L111
	subq	%r14, %rax
	subq	$4, %rax
	shrq	$2, %rax
	leaq	1(%r13,%rax), %rcx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rdx, %rbx
	movzbl	(%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %rbp
	movl	(%r15,%rdx,4), %edx
	movl	%edx, -4(%rbx)
	je	.L198
	cmpq	%rcx, %rax
	leaq	4(%rbx), %rdx
	jne	.L50
	movl	$5, %r11d
.L49:
	cmpq	$0, 48(%rsp)
	movq	(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L64
.L192:
	movq	48(%rsp), %rax
	movl	%r11d, %r8d
	movq	%rbx, (%rax)
.L8:
	addq	$152, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L67
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L113:
	movl	$7, %r11d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$5, %r11d
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L196:
	movl	%r10d, 104(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r11d
	popq	%r8
	cmpl	$6, %r11d
	popq	%r9
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movl	104(%rsp), %r10d
	je	.L61
	cmpl	$5, %r11d
	jne	.L53
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%rbp, %rcx
	movl	$4, %r11d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L197:
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movl	$6, %r11d
	movq	%rax, 128(%rsp)
	addq	$1, (%rcx)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L193:
	movq	16(%rsp), %rsi
	movq	%rbx, (%r12)
	movl	%r11d, %r8d
	movq	112(%rsp), %rax
	addq	%rax, (%rsi)
.L66:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 95(%rsp)
	je	.L8
	cmpl	$7, %r8d
	jne	.L8
	movq	(%rsp), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L99
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	32(%r12), %rcx
	je	.L101
.L100:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L100
.L101:
	movl	(%rcx), %edx
	movq	(%rsp), %rsi
	andl	$-8, %edx
	movq	%rbp, (%rsi)
	orl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%r11d, %r10d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L194:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L70
	movq	(%rsi), %rax
.L70:
	addq	112(%rsp), %rax
	cmpq	32(%rsp), %rax
	movq	(%rsp), %rax
	je	.L199
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L200
	movl	16(%r12), %edi
	leaq	128(%rsp), %rbx
	movq	%r13, 128(%rsp)
	movq	%r14, 136(%rsp)
	movq	%r14, %rdx
	movl	$4, %eax
	andl	$2, %edi
	movl	%edi, 32(%rsp)
.L83:
	cmpq	%r13, %rbp
	je	.L201
.L93:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rbp
	jb	.L120
	cmpq	%rdx, %r11
	jbe	.L123
	movl	0(%r13), %ecx
	cmpl	$65534, %ecx
	ja	.L85
	cmpl	$255, %ecx
	jbe	.L87
	cmpl	$65534, %ecx
	jbe	.L88
.L87:
	leaq	from_ucs4(%rip), %rdi
	movl	%ecx, %esi
	movzbl	(%rdi,%rsi), %esi
	testb	%sil, %sil
	jne	.L131
	testl	%ecx, %ecx
	jne	.L88
.L131:
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%sil, (%rdx)
	movq	128(%rsp), %rcx
	movq	136(%rsp), %rdx
	leaq	4(%rcx), %r13
	cmpq	%r13, %rbp
	movq	%r13, 128(%rsp)
	jne	.L93
.L201:
	cltq
	movq	%rbp, %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L190:
	testq	%rdi, %rdi
	jne	.L202
	cmpl	$4, %edx
	movq	%r13, 128(%rsp)
	movq	%r14, 136(%rsp)
	ja	.L18
	leaq	120(%rsp), %r11
	movslq	%edx, %rax
	xorl	%ebx, %ebx
.L19:
	movzbl	4(%r15,%rbx), %edx
	movb	%dl, (%r11,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L19
	movq	%r13, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L203
	cmpq	8(%rsp), %r14
	movl	$5, %r8d
	jnb	.L8
	leaq	1(%r13), %rax
	leaq	119(%rsp), %rsi
.L27:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rbx
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%cl, (%rsi,%rbx)
	ja	.L128
	cmpq	%rdx, %rbp
	ja	.L27
.L128:
	movl	120(%rsp), %eax
	movq	%r11, 128(%rsp)
	cmpl	$65534, %eax
	ja	.L29
	cmpl	$255, %eax
	jbe	.L31
	cmpl	$65534, %eax
	jbe	.L32
.L31:
	leaq	from_ucs4(%rip), %rcx
	movl	%eax, %edx
	testl	%eax, %eax
	movzbl	(%rcx,%rdx), %edx
	je	.L129
	testb	%dl, %dl
	jne	.L129
.L32:
	cmpq	$0, 80(%rsp)
	je	.L109
	testb	$8, %r10b
	jne	.L204
	andl	$2, %r10d
	je	.L109
	movq	%r11, %rax
.L103:
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rcx)
.L39:
	cmpq	%r11, %rax
	jne	.L33
.L109:
	movl	$6, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rcx, 128(%rsp)
	movq	%rcx, %rax
	jmp	.L53
.L199:
	movq	40(%rsp), %rdi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rdi)
	je	.L205
	movq	(%rsp), %rsi
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L67
.L129:
	leaq	1(%r14), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%r14)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%r11, %rax
	movq	%rax, 128(%rsp)
	je	.L206
.L33:
	movl	(%r15), %edx
	subq	%r11, %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L207
	movq	(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r14
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r13
	movq	112(%rsp), %rax
	movl	%edx, (%r15)
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r13, %rcx
	movq	%r14, %rbx
	movl	$5, %r11d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L189:
	cmpq	$0, 48(%rsp)
	jne	.L208
	movq	32(%r12), %rax
	xorl	%r8d, %r8d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L8
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r15
	popq	%rbp
	movl	%eax, %r8d
	popq	%r12
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rbp, %rcx
	movq	%r14, %rbx
	movl	$4, %r11d
	jmp	.L49
.L126:
	movl	%r10d, %r8d
	jmp	.L66
.L88:
	cmpq	$0, 80(%rsp)
	je	.L124
	testb	$8, 16(%r12)
	jne	.L209
.L91:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	jne	.L210
.L124:
	movl	$6, %eax
.L84:
	movq	(%rsp), %rdi
	movq	120(%rsp), %r11
	movq	%r13, (%rdi)
.L82:
	cmpq	%r11, %rdx
	jne	.L76
	cmpq	$5, %rax
	jne	.L75
	cmpq	%rdx, %r14
	jne	.L67
.L78:
	subl	$1, 20(%r12)
	jmp	.L67
.L209:
	movq	%r11, 104(%rsp)
	movl	%r10d, 56(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	16(%rsp), %rax
	movq	%rbx, %rcx
	movq	112(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rsi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %rdx
	movl	56(%rsp), %r10d
	movq	104(%rsp), %r11
	je	.L91
	cmpl	$5, %eax
	jne	.L83
.L123:
	movl	$5, %eax
	jmp	.L84
.L200:
	cmpq	%r13, %rbp
	je	.L211
	leaq	4(%r14), %rax
	cmpq	%r11, %rax
	ja	.L212
	movq	%r11, %rdx
	subq	%r14, %rdx
	subq	$4, %rdx
	shrq	$2, %rdx
	leaq	1(%r13,%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rax, %rdx
	movzbl	0(%r13), %eax
	addq	$1, %r13
	cmpq	%r13, %rbp
	movl	(%r15,%rax,4), %eax
	movl	%eax, -4(%rdx)
	je	.L213
	cmpq	%rcx, %r13
	leaq	4(%rdx), %rax
	jne	.L79
	movl	$5, %eax
.L80:
	movq	(%rsp), %rdi
	movq	%rcx, (%rdi)
	jmp	.L82
.L120:
	movl	$7, %eax
	jmp	.L84
.L205:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	movq	(%rsp), %rcx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rcx)
	jmp	.L67
.L85:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	jne	.L88
	movq	%rsi, 128(%rsp)
	movq	%rsi, %r13
	jmp	.L83
.L203:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L21
	addq	$1, %r13
	cmpq	%rax, %rbx
	jnb	.L23
.L24:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %edx
	addq	$1, %r13
	movb	%dl, 4(%r15,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L24
.L23:
	movl	$7, %r8d
	jmp	.L8
.L213:
	movq	%rbp, %rcx
	movl	$4, %eax
	jmp	.L80
.L210:
	movq	80(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L83
.L29:
	shrl	$7, %eax
	cmpl	$7168, %eax
	jne	.L32
	leaq	4(%r11), %rax
	movq	%rax, 128(%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L212:
	cmpq	%r14, %r11
	je	.L78
.L76:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	cmpq	%r14, %r11
	jne	.L76
.L75:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	(%r11,%rbx), %rax
	movl	%r10d, 96(%rsp)
	movq	%r11, 56(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rax, 40(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r8d
	popq	%r11
	movq	56(%rsp), %r11
	movl	96(%rsp), %r10d
	movq	128(%rsp), %rax
	je	.L214
	cmpq	%r11, %rax
	jne	.L33
	cmpl	$7, %r8d
	jne	.L42
	leaq	4(%r11), %rax
	cmpq	%rax, 32(%rsp)
	je	.L215
	movl	(%r15), %eax
	movq	%rbx, %rcx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	(%rsp), %rcx
	addq	%rdx, (%rcx)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L216
	cmpq	$4, %rbx
	ja	.L217
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r15)
	je	.L23
	xorl	%eax, %eax
.L46:
	movzbl	(%r11,%rax), %edx
	movb	%dl, 4(%r15,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L46
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L206:
	movq	112(%rsp), %rax
	movq	(%rsp), %rcx
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rcx), %r13
	movq	96(%rax), %rax
	jmp	.L16
.L21:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L99:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L208:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L18:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L202:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L207:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L217:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L216:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L215:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L42:
	testl	%r8d, %r8d
	jne	.L8
	movq	112(%rsp), %rax
	movq	(%rsp), %rsi
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rsi), %r13
	movq	96(%rax), %rax
	jmp	.L16
.L214:
	andb	$2, %r10b
	je	.L39
	jmp	.L103
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9074, @object
	.size	__PRETTY_FUNCTION__.9074, 14
__PRETTY_FUNCTION__.9074:
	.string	"to_gap_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9153, @object
	.size	__PRETTY_FUNCTION__.9153, 6
__PRETTY_FUNCTION__.9153:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 256
from_ucs4:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	55
	.byte	45
	.byte	46
	.byte	47
	.byte	22
	.byte	5
	.byte	37
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	60
	.byte	61
	.byte	50
	.byte	38
	.byte	24
	.byte	25
	.byte	63
	.byte	39
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	64
	.byte	79
	.byte	127
	.byte	123
	.byte	91
	.byte	108
	.byte	80
	.byte	125
	.byte	77
	.byte	93
	.byte	92
	.byte	78
	.byte	107
	.byte	96
	.byte	75
	.byte	97
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	122
	.byte	94
	.byte	76
	.byte	126
	.byte	110
	.byte	111
	.byte	-75
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	99
	.byte	-20
	.byte	-4
	.byte	95
	.byte	109
	.byte	121
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	67
	.byte	-69
	.byte	-36
	.byte	89
	.byte	7
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	21
	.byte	6
	.byte	23
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	9
	.byte	10
	.byte	27
	.byte	48
	.byte	49
	.byte	26
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	8
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	4
	.byte	20
	.byte	62
	.byte	-1
	.byte	65
	.byte	-86
	.byte	-80
	.byte	-79
	.byte	-97
	.byte	-78
	.byte	-52
	.byte	124
	.byte	-67
	.byte	-76
	.byte	-102
	.byte	-118
	.byte	-70
	.byte	-54
	.byte	-81
	.byte	-68
	.byte	-112
	.byte	-113
	.byte	-22
	.byte	-6
	.byte	-66
	.byte	-96
	.byte	-74
	.byte	-77
	.byte	-99
	.byte	-38
	.byte	-101
	.byte	-117
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-85
	.byte	100
	.byte	101
	.byte	98
	.byte	102
	.byte	74
	.byte	103
	.byte	-98
	.byte	104
	.byte	116
	.byte	113
	.byte	114
	.byte	115
	.byte	120
	.byte	117
	.byte	118
	.byte	119
	.byte	-84
	.byte	105
	.byte	-19
	.byte	-18
	.byte	-21
	.byte	-17
	.byte	-32
	.byte	-65
	.byte	-128
	.byte	-3
	.byte	-2
	.byte	-5
	.byte	90
	.byte	-83
	.byte	-82
	.byte	-95
	.byte	68
	.byte	69
	.byte	66
	.byte	70
	.byte	-64
	.byte	71
	.byte	-100
	.byte	72
	.byte	84
	.byte	81
	.byte	82
	.byte	83
	.byte	88
	.byte	85
	.byte	86
	.byte	87
	.byte	-116
	.byte	73
	.byte	-51
	.byte	-50
	.byte	-53
	.byte	-49
	.byte	106
	.byte	-31
	.byte	112
	.byte	-35
	.byte	-34
	.byte	-37
	.byte	-48
	.byte	-115
	.byte	-114
	.byte	-33
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.zero	4
	.long	1
	.long	2
	.long	3
	.long	156
	.long	9
	.long	134
	.long	127
	.long	151
	.long	141
	.long	142
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	157
	.long	133
	.long	8
	.long	135
	.long	24
	.long	25
	.long	146
	.long	143
	.long	28
	.long	29
	.long	30
	.long	31
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	10
	.long	23
	.long	27
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	5
	.long	6
	.long	7
	.long	144
	.long	145
	.long	22
	.long	147
	.long	148
	.long	149
	.long	150
	.long	4
	.long	152
	.long	153
	.long	154
	.long	155
	.long	20
	.long	21
	.long	158
	.long	26
	.long	32
	.long	160
	.long	226
	.long	123
	.long	224
	.long	225
	.long	227
	.long	229
	.long	231
	.long	241
	.long	196
	.long	46
	.long	60
	.long	40
	.long	43
	.long	33
	.long	38
	.long	233
	.long	234
	.long	235
	.long	232
	.long	237
	.long	238
	.long	239
	.long	236
	.long	126
	.long	220
	.long	36
	.long	42
	.long	41
	.long	59
	.long	94
	.long	45
	.long	47
	.long	194
	.long	91
	.long	192
	.long	193
	.long	195
	.long	197
	.long	199
	.long	209
	.long	246
	.long	44
	.long	37
	.long	95
	.long	62
	.long	63
	.long	248
	.long	201
	.long	202
	.long	203
	.long	200
	.long	205
	.long	206
	.long	207
	.long	204
	.long	96
	.long	58
	.long	35
	.long	167
	.long	39
	.long	61
	.long	34
	.long	216
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	171
	.long	187
	.long	240
	.long	253
	.long	254
	.long	177
	.long	176
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	170
	.long	186
	.long	230
	.long	184
	.long	198
	.long	164
	.long	181
	.long	223
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	161
	.long	191
	.long	208
	.long	221
	.long	222
	.long	174
	.long	162
	.long	163
	.long	165
	.long	183
	.long	169
	.long	64
	.long	182
	.long	188
	.long	189
	.long	190
	.long	172
	.long	124
	.long	175
	.long	168
	.long	180
	.long	215
	.long	228
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	173
	.long	244
	.long	166
	.long	242
	.long	243
	.long	245
	.long	252
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	185
	.long	251
	.long	125
	.long	249
	.long	250
	.long	255
	.long	214
	.long	247
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	178
	.long	212
	.long	92
	.long	210
	.long	211
	.long	213
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	179
	.long	219
	.long	93
	.long	217
	.long	218
	.long	159
