	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"IBM1008//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$10, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$4294967297, %rdx
	movq	$0, 96(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdx, 80(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$10, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdx, 80(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$120, %rsp
	movq	%rdi, 64(%rsp)
	addq	$104, %rdi
	testb	$1, 16(%rsi)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%rdx, (%rsp)
	movq	%r8, 40(%rsp)
	movq	%r9, 8(%rsp)
	movl	176(%rsp), %ebx
	movq	%rdi, 80(%rsp)
	movq	$0, 16(%rsp)
	jne	.L8
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 16(%rsp)
	je	.L8
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L8:
	testl	%ebx, %ebx
	jne	.L75
	movq	40(%rsp), %rax
	movq	8(%r13), %r12
	leaq	__from_ibm1008_to_ibm420(%rip), %r11
	leaq	__from_ibm420_to_ibm1008(%rip), %r14
	testq	%rax, %rax
	cmove	%r13, %rax
	movq	(%rax), %r10
	leaq	104(%rsp), %rax
	movq	%rax, 88(%rsp)
.L44:
	movq	(%rsp), %rax
	movq	$0, 24(%rsp)
	movq	(%rax), %r15
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L14
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)
.L14:
	movq	64(%rsp), %rax
	movq	%r10, %rbx
	cmpq	$0, 96(%rax)
	movq	%r15, %rax
	je	.L15
	cmpq	%rbp, %r15
	je	.L67
	cmpq	%r10, %r12
	jbe	.L68
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	(%rax), %edx
	addq	$1, %rax
	addq	$1, %rbx
	cmpq	%rax, %rbp
	movzbl	(%r14,%rdx), %edx
	movb	%dl, -1(%rbx)
	je	.L67
	cmpq	%rbx, %r12
	jne	.L17
	movq	%r12, %rbx
.L68:
	movl	$5, 36(%rsp)
.L16:
	cmpq	$0, 40(%rsp)
	movq	(%rsp), %rcx
	movq	%rax, (%rcx)
	jne	.L76
.L23:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L77
	cmpq	%rbx, %r10
	movq	%r11, 48(%rsp)
	movq	%r10, 56(%rsp)
	jnb	.L7
	movq	16(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%rax, 104(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	184(%rsp), %esi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rsi
	pushq	$0
	movq	24(%rsp), %r9
	movq	104(%rsp), %rdx
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	popq	%rdx
	cmpl	$4, %eax
	popq	%rcx
	movq	48(%rsp), %r11
	je	.L25
	movq	104(%rsp), %rdx
	movq	56(%rsp), %r10
	cmpq	%rbx, %rdx
	jne	.L78
.L29:
	testl	%eax, %eax
	jne	.L56
.L43:
	movq	0(%r13), %r10
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$4, 36(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	%rbp, %r15
	je	.L65
	cmpq	%r10, %r12
	jbe	.L66
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	(%rax), %edx
	addq	$1, %rax
	addq	$1, %rbx
	cmpq	%rax, %rbp
	movzbl	(%r11,%rdx), %edx
	movb	%dl, -1(%rbx)
	je	.L65
	cmpq	%rbx, %r12
	jne	.L19
	movq	%r12, %rbx
.L66:
	movl	$5, 36(%rsp)
.L18:
	cmpq	$0, 40(%rsp)
	movq	(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L23
.L76:
	movq	40(%rsp), %rax
	movq	%rbx, (%rax)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$5, 36(%rsp)
	je	.L43
	movl	$4, 36(%rsp)
.L7:
	movl	36(%rsp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$4, 36(%rsp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rbx, 0(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L78:
	movq	8(%rsp), %rdi
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rcx
.L27:
	cmpq	%rcx, 24(%rsp)
	je	.L79
	movq	(%rsp), %rcx
	movq	%r15, (%rcx)
	movq	64(%rsp), %rcx
	cmpq	$0, 96(%rcx)
	je	.L30
	cmpq	%rbp, %r15
	je	.L72
	movq	%rdx, %rsi
	movq	%r10, %rcx
	subq	%r10, %rsi
	addq	%r15, %rsi
	cmpq	%r10, %rdx
	jbe	.L70
	.p2align 4,,10
	.p2align 3
.L40:
	movzbl	(%r15), %edx
	addq	$1, %r15
	addq	$1, %rcx
	cmpq	%r15, %rbp
	movzbl	(%r14,%rdx), %edx
	movb	%dl, -1(%rcx)
	je	.L74
	cmpq	%r15, %rsi
	jne	.L40
	movq	104(%rsp), %rdi
	movq	(%rsp), %rbx
	cmpq	%rcx, %rdi
	movq	%rsi, (%rbx)
	jne	.L33
.L38:
	cmpq	%rdi, %r10
	jne	.L29
.L35:
	subl	$1, 20(%r13)
	jmp	.L29
.L79:
	movq	(%rsp), %rcx
	subq	%rdx, %rbx
	subq	%rbx, (%rcx)
	jmp	.L29
.L75:
	cmpq	$0, 40(%rsp)
	jne	.L80
	movq	32(%r13), %rax
	movl	$0, 36(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r13)
	jne	.L7
	movq	16(%rsp), %r14
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	184(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	24(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r14
	movl	%eax, 52(%rsp)
	popq	%rsi
	popq	%rdi
	jmp	.L7
.L74:
	cmpq	%rcx, 104(%rsp)
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	jne	.L33
.L32:
	leaq	__PRETTY_FUNCTION__.9099(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rbp, %r15
	je	.L72
	cmpq	%r10, %rdx
	movq	%r10, %rcx
	jbe	.L70
	.p2align 4,,10
	.p2align 3
.L37:
	movzbl	(%r15), %esi
	addq	$1, %r15
	addq	$1, %rcx
	cmpq	%r15, %rbp
	movzbl	(%r11,%rsi), %esi
	movb	%sil, -1(%rcx)
	je	.L74
	cmpq	%rcx, %rdx
	jne	.L37
	movq	104(%rsp), %rdi
	movq	(%rsp), %rcx
	cmpq	%rdx, %rdi
	movq	%r15, (%rcx)
	je	.L38
.L33:
	leaq	__PRETTY_FUNCTION__.9099(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	je	.L35
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L72:
	cmpq	%r10, %rdx
	je	.L32
	jmp	.L33
.L56:
	movl	%eax, 36(%rsp)
	jmp	.L7
.L80:
	leaq	__PRETTY_FUNCTION__.9099(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9099, @object
	.size	__PRETTY_FUNCTION__.9099, 6
__PRETTY_FUNCTION__.9099:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	__from_ibm420_to_ibm1008, @object
	.size	__from_ibm420_to_ibm1008, 256
__from_ibm420_to_ibm1008:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	-100
	.byte	9
	.byte	-122
	.byte	127
	.byte	-105
	.byte	-115
	.byte	-114
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	-99
	.byte	-123
	.byte	8
	.byte	-121
	.byte	24
	.byte	25
	.byte	-110
	.byte	-113
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	10
	.byte	23
	.byte	27
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	5
	.byte	6
	.byte	7
	.byte	-112
	.byte	-111
	.byte	22
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	4
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-101
	.byte	20
	.byte	21
	.byte	-98
	.byte	26
	.byte	32
	.byte	-96
	.byte	-91
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-86
	.byte	-85
	.byte	-82
	.byte	-81
	.byte	-94
	.byte	46
	.byte	60
	.byte	40
	.byte	43
	.byte	124
	.byte	38
	.byte	-70
	.byte	-69
	.byte	91
	.byte	92
	.byte	-68
	.byte	-67
	.byte	-66
	.byte	-65
	.byte	-64
	.byte	33
	.byte	36
	.byte	42
	.byte	41
	.byte	59
	.byte	-84
	.byte	45
	.byte	47
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-90
	.byte	44
	.byte	37
	.byte	95
	.byte	62
	.byte	63
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.byte	-48
	.byte	-47
	.byte	-95
	.byte	58
	.byte	35
	.byte	64
	.byte	39
	.byte	61
	.byte	34
	.byte	-46
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	-37
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-33
	.byte	-32
	.byte	-31
	.byte	-9
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	93
	.byte	94
	.byte	-18
	.byte	-17
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-93
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	-83
	.byte	-10
	.byte	96
	.byte	-8
	.byte	123
	.byte	-7
	.byte	-92
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	-80
	.byte	-41
	.byte	125
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	-79
	.byte	-78
	.byte	126
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	-1
	.byte	-74
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-97
	.align 32
	.type	__from_ibm1008_to_ibm420, @object
	.size	__from_ibm1008_to_ibm420, 256
__from_ibm1008_to_ibm420:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	55
	.byte	45
	.byte	46
	.byte	47
	.byte	22
	.byte	5
	.byte	37
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	60
	.byte	61
	.byte	50
	.byte	38
	.byte	24
	.byte	25
	.byte	63
	.byte	39
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	64
	.byte	90
	.byte	127
	.byte	123
	.byte	91
	.byte	108
	.byte	80
	.byte	125
	.byte	77
	.byte	93
	.byte	92
	.byte	78
	.byte	107
	.byte	96
	.byte	75
	.byte	97
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	122
	.byte	94
	.byte	76
	.byte	126
	.byte	110
	.byte	111
	.byte	124
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	83
	.byte	84
	.byte	-74
	.byte	-73
	.byte	109
	.byte	-52
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-50
	.byte	79
	.byte	-31
	.byte	-20
	.byte	7
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	21
	.byte	6
	.byte	23
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	9
	.byte	10
	.byte	27
	.byte	48
	.byte	49
	.byte	26
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	8
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	4
	.byte	20
	.byte	62
	.byte	-1
	.byte	65
	.byte	121
	.byte	74
	.byte	-64
	.byte	-48
	.byte	66
	.byte	106
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	95
	.byte	-54
	.byte	72
	.byte	73
	.byte	-33
	.byte	-22
	.byte	-21
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-5
	.byte	-4
	.byte	-3
	.byte	-2
	.byte	81
	.byte	82
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	-128
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-32
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	-96
	.byte	-86
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-72
	.byte	-71
	.byte	-70
	.byte	-69
	.byte	-68
	.byte	-67
	.byte	-66
	.byte	-65
	.byte	-53
	.byte	-95
	.byte	-51
	.byte	-49
	.byte	-38
	.byte	-37
	.byte	-36
	.byte	-35
	.byte	-34
	.byte	-6
