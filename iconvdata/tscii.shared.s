	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TSCII//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$8589934593, %rdx
	movabsq	$68719476740, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$17179869188, %rdx
	movabsq	$12884901889, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC7:
	.string	"last == 0xc38a"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC9:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC10:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC11:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC13:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	addq	$48, %rsi
	subq	$168, %rsp
	movl	16(%r13), %ebx
	movq	%rdi, 56(%rsp)
	addq	$104, %rdi
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 64(%rsp)
	movl	%ebx, %ecx
	movl	224(%rsp), %edx
	movq	%rdi, 72(%rsp)
	andl	$1, %ecx
	movq	%rsi, 80(%rsp)
	movl	%ebx, 48(%rsp)
	movq	$0, 24(%rsp)
	jne	.L8
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rsi
	movq	%rsi, 24(%rsp)
	je	.L8
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L8:
	testl	%edx, %edx
	jne	.L585
	movq	16(%rsp), %rax
	leaq	112(%rsp), %rcx
	movq	32(%r13), %rbp
	movl	232(%rsp), %edi
	movq	8(%r13), %r14
	movq	(%rax), %r11
	movq	32(%rsp), %rax
	testq	%rax, %rax
	cmove	%r13, %rax
	cmpq	$0, 64(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rcx, %rax
	testl	%edi, %edi
	movq	%rax, 88(%rsp)
	movl	0(%rbp), %eax
	movl	%eax, 40(%rsp)
	jne	.L29
.L553:
	movq	56(%rsp), %rax
	movq	96(%rax), %rax
.L30:
	movq	%r11, %rdi
	testq	%rax, %rax
	movq	%r15, %r11
	movq	%rdi, %r15
	je	.L586
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	consonant_with_u(%rip), %r10
	movq	%r15, 128(%rsp)
	movq	%r11, 136(%rsp)
	movq	%r11, %rbx
	movq	%r15, %rax
	movl	$4, 8(%rsp)
.L176:
	cmpq	%rax, %r12
	je	.L177
.L224:
	leaq	4(%rax), %rsi
	cmpq	%rsi, %r12
	jb	.L358
	cmpq	%rbx, %r14
	jbe	.L368
	movl	0(%rbp), %edx
	movl	(%rax), %ecx
	sarl	$3, %edx
	testl	%edx, %edx
	je	.L178
	leal	-184(%rdx), %edi
	cmpl	$17, %edi
	ja	.L179
	cmpl	$3009, %ecx
	je	.L587
	cmpl	$3010, %ecx
	je	.L588
	cmpl	$3014, %ecx
	je	.L589
	cmpl	$3015, %ecx
	je	.L590
	cmpl	$3016, %ecx
	je	.L591
	cmpl	$3018, %ecx
	je	.L592
	cmpl	$3019, %ecx
	je	.L593
	cmpl	$3020, %ecx
	je	.L594
	cmpl	$3021, %ecx
	je	.L595
	cmpl	$188, %edx
	jne	.L192
	leal	-3007(%rcx), %esi
	leaq	1(%rbx), %rax
	cmpl	$1, %esi
	ja	.L193
	addl	$11, %ecx
	movq	%rax, 136(%rsp)
	movb	%cl, (%rbx)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L178:
	cmpl	$127, %ecx
	ja	.L206
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
.L207:
	cmpq	%rax, %r12
	movq	%rax, 128(%rsp)
	jne	.L224
	.p2align 4,,10
	.p2align 3
.L177:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L596
.L225:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L597
	cmpq	%rbx, %r11
	movq	%r11, 48(%rsp)
	jnb	.L374
	movq	24(%rsp), %rdi
	movq	0(%r13), %rax
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	movl	%eax, %r10d
	cmpl	$4, %r10d
	popq	%r11
	popq	%rax
	je	.L229
	movq	120(%rsp), %rax
	movq	48(%rsp), %r11
	cmpq	%rbx, %rax
	movq	%rax, 8(%rsp)
	jne	.L598
.L228:
	testl	%r10d, %r10d
	jne	.L407
.L316:
	movq	56(%rsp), %rax
	movq	16(%rsp), %rdi
	movl	16(%r13), %esi
	movq	0(%r13), %r11
	movq	96(%rax), %rax
	movq	(%rdi), %r15
	movl	0(%rbp), %edi
	movl	%esi, 48(%rsp)
	testq	%rax, %rax
	movl	%edi, 40(%rsp)
	jne	.L146
.L586:
	cmpq	%r15, %r12
	je	.L352
	leaq	4(%r11), %rsi
	cmpq	%rsi, %r14
	jb	.L353
	movl	40(%rsp), %edi
	movl	48(%rsp), %eax
	movq	%r15, %rdx
	movq	%r11, %rbx
	movl	$4, 8(%rsp)
	leaq	tscii_to_ucs4(%rip), %r9
	leaq	tscii_next_state(%rip), %r8
	movl	%edi, %ecx
	andl	$2, %eax
	sarl	$8, %ecx
	movl	%eax, 48(%rsp)
	movzbl	(%rdx), %eax
	testl	%ecx, %ecx
	je	.L149
.L607:
	cmpl	$3021, %ecx
	je	.L599
	leal	-3014(%rcx), %r10d
	cmpl	$2, %r10d
	ja	.L151
	cmpl	$161, %eax
	sete	%r10b
	cmpl	$3014, %ecx
	jne	.L413
	testb	%r10b, %r10b
	jne	.L153
.L413:
	cmpl	$3015, %ecx
	je	.L600
.L155:
	leal	-184(%rax), %r10d
	cmpl	$17, %r10d
	ja	.L151
	andl	$8, %edi
	je	.L601
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	4(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L157:
	movl	%ecx, -4(%rax)
	movq	%rax, %rbx
	movl	0(%rbp), %eax
	sarl	$4, %eax
	andl	$15, %eax
	movl	(%r8,%rax,4), %eax
	testl	%eax, %eax
	movl	%eax, 0(%rbp)
	je	.L152
	leaq	4(%rbx), %rax
	cmpq	%rax, %r14
	jnb	.L157
.L554:
	movl	$5, 8(%rsp)
.L147:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	je	.L225
.L596:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L7:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	leal	-131(%rdx), %edi
	cmpl	$3, %edi
	ja	.L194
	cmpl	$132, %edx
	jbe	.L195
	leal	-3009(%rcx), %edi
	cmpl	$1, %edi
	jbe	.L602
.L195:
	cmpl	$3021, %ecx
	je	.L603
.L192:
	movl	%edx, %esi
	shrl	$8, %esi
	testl	%esi, %esi
	jne	.L204
.L555:
	leaq	1(%rbx), %rax
.L193:
	movq	%rax, 136(%rsp)
	movb	%dl, (%rbx)
.L205:
	movl	$0, 0(%rbp)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L206:
	leal	-2944(%rcx), %edx
	cmpl	$127, %edx
	ja	.L208
	leaq	ucs4_to_tscii(%rip), %rdi
	movzbl	(%rdi,%rdx), %edx
	testb	%dl, %dl
	je	.L209
	leal	72(%rdx), %eax
	cmpb	$17, %al
	jbe	.L414
	leal	125(%rdx), %eax
	cmpb	$3, %al
	jbe	.L414
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
.L636:
	leaq	8(%rbx), %rax
	addq	$1, %rdx
	movl	$2965, (%rbx)
	cmpq	%rax, %r14
	jb	.L604
	leaq	12(%rbx), %rcx
	movl	$3021, 4(%rbx)
	cmpq	%rcx, %r14
	jb	.L605
	leaq	16(%rbx), %rax
	movl	$2999, 8(%rbx)
	cmpq	%rax, %r14
	jb	.L606
	movl	$3021, 12(%rbx)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L152:
	cmpq	%rdx, %r12
	je	.L147
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r14
	jb	.L554
	movl	0(%rbp), %edi
	movzbl	(%rdx), %eax
	movl	%edi, %ecx
	sarl	$8, %ecx
	testl	%ecx, %ecx
	jne	.L607
.L149:
	cmpl	$127, %eax
	ja	.L158
	movl	%eax, (%rbx)
	addq	$1, %rdx
	movq	%rsi, %rbx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L194:
	cmpl	$236, %edx
	je	.L608
	cmpl	$138, %edx
	je	.L609
	cmpl	$135, %edx
	je	.L610
	cmpl	$50058, %edx
	jne	.L293
	cmpl	$3008, %ecx
	movl	$195, %esi
	je	.L611
.L204:
	leaq	2(%rbx), %rcx
	cmpq	%rcx, %r14
	jnb	.L612
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$5, 8(%rsp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L208:
	cmpl	$169, %ecx
	je	.L613
	leal	-8216(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L614
	leal	-8220(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L615
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L616
	cmpq	$0, 88(%rsp)
	je	.L373
	testb	$8, 16(%r13)
	jne	.L617
.L222:
	testb	$2, 48(%rsp)
	je	.L373
	movq	88(%rsp), %rdi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 128(%rsp)
	addq	$1, (%rdi)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L358:
	movl	$7, 8(%rsp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L229:
	movl	8(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L228
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L158:
	leal	-128(%rax), %ecx
	movzwl	(%r9,%rcx,4), %edi
	testl	%edi, %edi
	je	.L159
	movzwl	2(%r9,%rcx,4), %ecx
	addq	$1, %rdx
	movl	%edi, (%rbx)
	testl	%ecx, %ecx
	je	.L355
	leaq	8(%rbx), %rax
	cmpq	%rax, %r14
	jb	.L618
	movl	%ecx, 4(%rbx)
	movq	%rax, %rbx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L209:
	leal	-3018(%rcx), %edx
	cmpl	$2, %edx
	ja	.L212
	leaq	2(%rbx), %rdx
	cmpq	%rdx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	cmpl	$3018, %ecx
	movq	%rax, 136(%rsp)
	je	.L213
	cmpl	$3020, %ecx
	movb	$-89, (%rbx)
	sete	%al
	leal	-95(%rax,%rax,8), %eax
.L214:
	movq	136(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L599:
	andl	$8, %edi
	je	.L151
	leal	-164(%rax), %edi
	cmpl	$1, %edi
	ja	.L151
	addl	$2845, %eax
	addq	$1, %rdx
	movl	%eax, (%rbx)
	movl	$0, 0(%rbp)
	movq	%rsi, %rbx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	2(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-90, (%rbx)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
.L557:
	movq	128(%rsp), %rax
	movl	$0, 0(%rbp)
	movq	136(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L29:
	movl	40(%rsp), %r10d
	andl	$7, %r10d
	je	.L553
	cmpq	$0, 32(%rsp)
	jne	.L619
	movq	56(%rsp), %rax
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L620
	cmpl	$4, %r10d
	movq	%r11, 144(%rsp)
	movq	%r15, 152(%rsp)
	ja	.L60
	leaq	136(%rsp), %rsi
	movslq	%r10d, %r10
	xorl	%eax, %eax
	movq	%rsi, 40(%rsp)
.L61:
	movzbl	4(%rbp,%rax), %ecx
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%r10, %rax
	jne	.L61
	movq	%r11, %rax
	subq	%r10, %rax
	addq	$4, %rax
	cmpq	%rax, %r12
	jb	.L621
	cmpq	%r14, %r15
	jnb	.L79
	leaq	1(%r11), %rax
	leaq	135(%rsp), %rdi
.L69:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r10
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %r10
	movb	%cl, (%rdi,%r10)
	ja	.L411
	cmpq	%rsi, %r12
	ja	.L69
.L411:
	movl	0(%rbp), %esi
	movq	40(%rsp), %rax
	movl	%esi, %ecx
	movq	%rax, 144(%rsp)
	movl	136(%rsp), %eax
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L71
	leal	-184(%rcx), %esi
	cmpl	$17, %esi
	ja	.L72
	cmpl	$3009, %eax
	je	.L622
	cmpl	$3010, %eax
	je	.L623
	cmpl	$3014, %eax
	je	.L624
	cmpl	$3015, %eax
	je	.L625
	cmpl	$3016, %eax
	je	.L626
	cmpl	$3018, %eax
	je	.L627
	cmpl	$3019, %eax
	je	.L628
	cmpl	$3020, %eax
	je	.L629
	cmpl	$3021, %eax
	je	.L630
	cmpl	$188, %ecx
	je	.L631
.L95:
	movl	%ecx, %esi
	shrl	$8, %esi
	testl	%esi, %esi
	jne	.L111
.L549:
	leaq	1(%r15), %rsi
.L96:
	movq	%rsi, 152(%rsp)
	movb	%cl, (%r15)
.L113:
	movq	144(%rsp), %rax
	xorl	%ecx, %ecx
	cmpq	40(%rsp), %rax
	movl	$0, 0(%rbp)
	jne	.L74
	movq	56(%rsp), %rax
	movq	16(%rsp), %rdi
	movl	16(%r13), %esi
	movl	$0, 40(%rsp)
	movq	96(%rax), %rax
	movq	(%rdi), %r11
	movl	%esi, 48(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L159:
	leal	-166(%rax), %ecx
	cmpl	$2, %ecx
	jbe	.L632
	leal	-138(%rax), %ecx
	cmpl	$1, %ecx
	jbe	.L633
	cmpl	$130, %eax
	je	.L634
	cmpl	$135, %eax
	je	.L635
	cmpl	$140, %eax
	je	.L636
	cmpq	$0, 88(%rsp)
	je	.L357
	movl	48(%rsp), %r10d
	testl	%r10d, %r10d
	jne	.L637
.L357:
	movl	$6, 8(%rsp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L600:
	cmpl	$170, %eax
	je	.L153
	testb	%r10b, %r10b
	je	.L155
	.p2align 4,,10
	.p2align 3
.L153:
	cmpl	$161, %eax
	setne	%al
	addq	$1, %rdx
	movzbl	%al, %eax
	leal	4(%rcx,%rax), %eax
	movl	%eax, (%rbx)
	movl	$0, 0(%rbp)
	movq	%rsi, %rbx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L212:
	cmpq	$0, 88(%rsp)
	je	.L373
	testb	$8, 16(%r13)
	jne	.L638
.L215:
	testb	$2, 48(%rsp)
	jne	.L639
.L373:
	movl	$6, 8(%rsp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L591:
	leaq	2(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-88, (%rbx)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L414:
	sall	$3, %edx
	movq	%rsi, %rax
	movl	%edx, 0(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movzbl	(%r10,%rdi), %eax
	movb	%al, (%rbx)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L608:
	cmpl	$2999, %ecx
	jne	.L555
	movl	$1080, 0(%rbp)
	movq	%rsi, %rax
	movq	%rsi, 128(%rsp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L613:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-87, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	leaq	consonant_with_uu(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	movb	%al, (%rbx)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L597:
	movq	64(%rsp), %rdi
	movq	%rbx, 0(%r13)
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L227:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L7
	cmpl	$7, 8(%rsp)
	jne	.L7
	movq	16(%rsp), %rax
	movq	%r12, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L318
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rsi
	je	.L320
.L319:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L319
.L320:
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L374:
	movl	8(%rsp), %r10d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	2(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-89, (%rbx)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	1(%rbx), %rax
	addl	$121, %ecx
	movq	%rax, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L609:
	cmpl	$2992, %ecx
	jne	.L555
	movl	$400464, 0(%rbp)
	movq	%rsi, %rax
	movq	%rsi, 128(%rsp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L603:
	cmpl	$133, %edx
	je	.L196
	leaq	1(%rbx), %rax
	addl	$5, %edx
	movq	%rax, 136(%rsp)
	movb	%dl, (%rbx)
	movq	128(%rsp), %rax
	movl	$0, 0(%rbp)
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L598:
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	movl	40(%rsp), %eax
	movl	%eax, 0(%rbp)
	movq	56(%rsp), %rax
	cmpq	$0, 96(%rax)
	movl	16(%r13), %eax
	je	.L640
	leaq	consonant_with_u(%rip), %rbx
	movl	%eax, 40(%rsp)
	movq	%r15, 144(%rsp)
	movq	%r11, 152(%rsp)
	movq	%r11, %rdx
	movl	$4, %eax
.L266:
	cmpq	%r15, %r12
	je	.L641
	leaq	4(%r15), %rdi
	cmpq	%rdi, %r12
	jb	.L389
	cmpq	%rdx, 8(%rsp)
	jbe	.L404
	movl	0(%rbp), %ecx
	movl	(%r15), %esi
	sarl	$3, %ecx
	testl	%ecx, %ecx
	je	.L268
	leal	-184(%rcx), %r8d
	cmpl	$17, %r8d
	ja	.L269
	cmpl	$3009, %esi
	je	.L642
	cmpl	$3010, %esi
	je	.L643
	cmpl	$3014, %esi
	je	.L644
	cmpl	$3015, %esi
	je	.L645
	cmpl	$3016, %esi
	je	.L646
	cmpl	$3018, %esi
	je	.L647
	cmpl	$3019, %esi
	je	.L648
	cmpl	$3020, %esi
	je	.L649
	cmpl	$3021, %esi
	je	.L650
	cmpl	$188, %ecx
	je	.L651
.L282:
	movl	%ecx, %edi
	shrl	$8, %edi
	testl	%edi, %edi
	jne	.L294
.L558:
	leaq	1(%rdx), %rdi
.L283:
	movq	%rdi, 152(%rsp)
	movb	%cl, (%rdx)
.L295:
	movl	$0, 0(%rbp)
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	1(%rbx), %rax
	addl	$119, %ecx
	movq	%rax, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L601:
	addl	$-128, %eax
	addq	$1, %rdx
	movzwl	(%r9,%rax,4), %eax
	movl	%eax, (%rbx)
	movq	%rsi, %rbx
	orl	$8, 0(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L610:
	cmpl	$3021, %ecx
	leaq	1(%rbx), %rax
	jne	.L193
	movq	%rax, 136(%rsp)
	movb	$-116, (%rbx)
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L612:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%rbx)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 136(%rsp)
	movb	%sil, (%rax)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L602:
	leaq	1(%rbx), %rax
	addl	$5, %edx
	movq	%rax, 136(%rsp)
	movb	%dl, (%rbx)
	movl	$0, 0(%rbp)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	jmp	.L176
.L593:
	leaq	3(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-89, (%rbx)
.L581:
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 136(%rsp)
	movb	$-95, (%rax)
	jmp	.L557
.L638:
	movq	%r11, 104(%rsp)
	movq	%r10, 96(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %r8
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	72(%rsp), %rdi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rsi
	popq	%rdi
	movq	96(%rsp), %r10
	movq	104(%rsp), %r11
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	je	.L215
.L223:
	cmpl	$5, 8(%rsp)
	jne	.L176
	jmp	.L177
.L213:
	movb	$-90, (%rbx)
	movl	$-95, %eax
	jmp	.L214
.L617:
	movq	%r11, 104(%rsp)
	movq	%r10, 96(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %r8
	movq	%r13, %rsi
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	72(%rsp), %rdi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	movq	96(%rsp), %r10
	movq	104(%rsp), %r11
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	jne	.L223
	jmp	.L222
.L634:
	leaq	8(%rbx), %rax
	addq	$1, %rdx
	movl	$3000, (%rbx)
	cmpq	%rax, %r14
	jb	.L652
	leaq	12(%rbx), %rcx
	movl	$3021, 4(%rbx)
	cmpq	%rcx, %r14
	jb	.L653
	leaq	16(%rbx), %rax
	movl	$2992, 8(%rbx)
	cmpq	%rax, %r14
	jb	.L654
	movl	$3008, 12(%rbx)
	movq	%rax, %rbx
	jmp	.L152
.L639:
	movq	88(%rsp), %rsi
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
	jmp	.L176
.L196:
	movl	$1104, 0(%rbp)
	movq	%rsi, %rax
	jmp	.L207
.L620:
	cmpl	$4, %r10d
	ja	.L34
	movzbl	4(%rbp), %ecx
	cmpl	$1, %r10d
	movb	%cl, 152(%rsp)
	movl	$1, %ecx
	je	.L35
	movzbl	5(%rbp), %ecx
	movb	%cl, 153(%rsp)
	movl	$2, %ecx
.L35:
	leaq	4(%r15), %rdi
	cmpq	%rdi, %r14
	jb	.L79
	movzbl	(%r11), %esi
	movb	%sil, 152(%rsp,%rcx)
	movl	40(%rsp), %esi
	movzbl	152(%rsp), %ecx
	sarl	$8, %esi
	testl	%esi, %esi
	je	.L37
	cmpl	$3021, %esi
	je	.L655
	leal	-3014(%rsi), %r8d
	cmpl	$2, %r8d
	ja	.L39
	cmpl	$161, %ecx
	sete	%r8b
	cmpl	$3014, %esi
	jne	.L410
	testb	%r8b, %r8b
	je	.L410
.L41:
	xorl	%eax, %eax
	cmpl	$161, %ecx
	setne	%al
	leal	4(%rsi,%rax), %eax
	movl	%eax, (%r15)
	movl	16(%r13), %ebx
	movq	%rdi, %r15
.L40:
	movq	16(%rsp), %rax
	addq	$1, %r11
	andl	$-8, %edx
	movl	%edx, 40(%rsp)
	movl	%ebx, 48(%rsp)
	movq	%r11, (%rax)
	movq	56(%rsp), %rax
	movl	%edx, 0(%rbp)
	movq	96(%rax), %rax
	jmp	.L30
.L640:
	cmpq	%r15, %r12
	je	.L656
	andl	$2, %eax
	leaq	4(%r11), %rsi
	cmpq	%rsi, 8(%rsp)
	movq	%r11, %rdx
	movl	$4, %ebx
	leaq	tscii_to_ucs4(%rip), %r8
	movl	%eax, 96(%rsp)
	leaq	tscii_next_state(%rip), %rdi
	jb	.L657
	movq	8(%rsp), %r9
	movl	%r10d, 48(%rsp)
	movl	40(%rsp), %r10d
	movl	%ebx, 40(%rsp)
.L235:
	movl	%r10d, %eax
	movzbl	(%r15), %ecx
	sarl	$8, %eax
	testl	%eax, %eax
	je	.L238
	cmpl	$3021, %eax
	je	.L658
	leal	-3014(%rax), %ebx
	cmpl	$2, %ebx
	ja	.L240
	cmpl	$161, %ecx
	sete	%bl
	cmpl	$3014, %eax
	jne	.L415
	testb	%bl, %bl
	je	.L415
.L242:
	cmpl	$161, %ecx
	setne	%cl
	addq	$1, %r15
	movzbl	%cl, %ecx
	leal	4(%rax,%rcx), %eax
	movl	%eax, (%rdx)
	movl	$0, 0(%rbp)
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L241:
	cmpq	%r15, %r12
	je	.L659
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r9
	jb	.L376
	movl	0(%rbp), %r10d
	jmp	.L235
.L39:
	movq	%r14, %rdx
	leaq	tscii_next_state(%rip), %rdi
	movq	%r15, %rcx
	subq	%r15, %rdx
	subq	$4, %rdx
	shrq	$2, %rdx
	leaq	4(%r15,%rdx,4), %r8
.L47:
	movl	%esi, (%rcx)
	movl	0(%rbp), %edx
	addq	$4, %rcx
	sarl	$4, %edx
	andl	$15, %edx
	movl	(%rdi,%rdx,4), %edx
	testl	%edx, %edx
	movl	%edx, 0(%rbp)
	je	.L551
	cmpq	%r8, %rcx
	jne	.L47
.L79:
	movl	$5, 8(%rsp)
	jmp	.L7
.L240:
	leaq	4(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L246:
	movl	%eax, -4(%rcx)
	movq	%rcx, %rdx
	movl	0(%rbp), %ecx
	sarl	$4, %ecx
	andl	$15, %ecx
	movl	(%rdi,%rcx,4), %ecx
	testl	%ecx, %ecx
	movl	%ecx, 0(%rbp)
	je	.L241
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r9
	jnb	.L246
.L376:
	movl	48(%rsp), %r10d
	movl	$5, %ebx
.L237:
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L268:
	cmpl	$127, %esi
	ja	.L296
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movq	144(%rsp), %rdi
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r15
.L297:
	movq	%r15, 144(%rsp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	3(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-90, (%rbx)
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L632:
	addl	$2848, %eax
	addq	$1, %rdx
	sall	$8, %eax
	movl	%eax, 0(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rsi, %rbx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r11, %rbx
	movq	%r15, %rdx
	movl	$5, 8(%rsp)
	jmp	.L147
.L269:
	leal	-131(%rcx), %r8d
	cmpl	$3, %r8d
	ja	.L284
	cmpl	$132, %ecx
	jbe	.L285
	leal	-3009(%rsi), %r8d
	cmpl	$1, %r8d
	ja	.L285
	leaq	1(%rdx), %rsi
	addl	$5, %ecx
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movl	$0, 0(%rbp)
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	jmp	.L266
.L285:
	cmpl	$3021, %esi
	jne	.L282
	cmpl	$133, %ecx
	je	.L286
	leaq	1(%rdx), %rsi
	addl	$5, %ecx
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	144(%rsp), %rsi
	movl	$0, 0(%rbp)
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r15
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L296:
	leal	-2944(%rsi), %ecx
	cmpl	$127, %ecx
	ja	.L298
	leaq	ucs4_to_tscii(%rip), %r9
	movzbl	(%r9,%rcx), %ecx
	testb	%cl, %cl
	jne	.L660
	leal	-3018(%rsi), %ecx
	cmpl	$2, %ecx
	ja	.L302
	leaq	2(%rdx), %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rcx
	cmpl	$3018, %esi
	movq	%rcx, 152(%rsp)
	je	.L303
	cmpl	$3020, %esi
	movb	$-89, (%rdx)
	sete	%dl
	leal	-95(%rdx,%rdx,8), %edx
.L304:
	movq	152(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%dl, (%rcx)
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r15
	jmp	.L297
.L585:
	cmpq	$0, 32(%rsp)
	jne	.L661
	cmpl	$1, %edx
	movq	32(%r13), %rbx
	jne	.L11
	movl	(%rbx), %ebp
	movq	0(%r13), %rax
	testl	%ebp, %ebp
	je	.L662
	movq	56(%rsp), %rdi
	movq	8(%r13), %rcx
	cmpq	$0, 96(%rdi)
	je	.L663
	movl	%ebp, %esi
	sarl	$3, %esi
	movl	%esi, %edi
	shrl	$8, %edi
	testl	%edi, %edi
	jne	.L664
	cmpq	%rcx, %rax
	jnb	.L79
	movb	%sil, (%rax)
	testb	$1, 16(%r13)
	leaq	1(%rax), %r12
	movq	32(%r13), %rdx
	movl	$0, (%rdx)
	jne	.L409
.L22:
	movq	24(%rsp), %r15
	movq	%rax, 152(%rsp)
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r15
	cmpl	$4, %eax
	movl	%eax, 24(%rsp)
	popq	%r13
	popq	%r14
	je	.L14
	cmpq	%r12, 152(%rsp)
	jne	.L665
.L25:
	movl	8(%rsp), %ebx
	testl	%ebx, %ebx
	jne	.L7
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L238:
	cmpl	$127, %ecx
	jbe	.L666
	leal	-128(%rcx), %ebx
	movzwl	(%r8,%rbx,4), %eax
	testl	%eax, %eax
	je	.L248
	movzwl	2(%r8,%rbx,4), %ecx
	addq	$1, %r15
	movl	%eax, (%rdx)
	testl	%ecx, %ecx
	je	.L377
	leaq	8(%rdx), %rax
	cmpq	%rax, %r9
	jb	.L667
	movl	%ecx, 4(%rdx)
	movq	%rax, %rdx
	jmp	.L241
.L611:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-126, (%rbx)
	jmp	.L557
.L407:
	movl	%r10d, 8(%rsp)
	jmp	.L227
.L594:
	leaq	3(%rbx), %rcx
	cmpq	%rcx, %r14
	jb	.L368
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	$-89, (%rbx)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 136(%rsp)
	movb	$-86, (%rax)
	jmp	.L557
.L616:
	movq	%rsi, 128(%rsp)
	movq	%rsi, %rax
	jmp	.L176
.L633:
	addl	$2862, %eax
	addq	$1, %rdx
	movl	%eax, (%rbx)
	movl	$773384, 0(%rbp)
	movq	%rsi, %rbx
	jmp	.L152
.L676:
	movl	%r10d, 96(%rsp)
	movq	%r11, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %r8
	movq	%r13, %rsi
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	72(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	movq	48(%rsp), %r11
	movl	96(%rsp), %r10d
	je	.L312
	cmpl	$5, %eax
	jne	.L266
.L404:
	movl	$5, %ebx
.L267:
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	movq	120(%rsp), %rax
	movq	%rax, 8(%rsp)
.L265:
	cmpq	8(%rsp), %rdx
	jne	.L234
	cmpq	$5, %rbx
	jne	.L233
.L250:
	cmpq	%r11, %rdx
	jne	.L228
.L236:
	subl	$1, 20(%r13)
	jmp	.L228
.L284:
	cmpl	$236, %ecx
	je	.L668
	cmpl	$138, %ecx
	je	.L669
	cmpl	$135, %ecx
	je	.L670
	cmpl	$50058, %ecx
	jne	.L293
	cmpl	$3008, %esi
	movl	$195, %edi
	je	.L671
.L294:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%dil, (%rdx)
	jmp	.L295
.L298:
	cmpl	$169, %esi
	je	.L672
	leal	-8216(%rsi), %ecx
	cmpl	$1, %ecx
	jbe	.L673
	leal	-8220(%rsi), %ecx
	cmpl	$1, %ecx
	jbe	.L674
	shrl	$7, %esi
	cmpl	$7168, %esi
	je	.L675
	cmpq	$0, 88(%rsp)
	je	.L405
	testb	$8, 16(%r13)
	jne	.L676
.L312:
	testb	$2, 40(%rsp)
	jne	.L677
.L405:
	movl	$6, %ebx
	jmp	.L267
.L389:
	movl	$7, %ebx
	jmp	.L267
.L635:
	leaq	8(%rbx), %rax
	addq	$1, %rdx
	movl	$2965, (%rbx)
	cmpq	%rax, %r14
	jb	.L678
	leaq	12(%rbx), %rcx
	movl	$3021, 4(%rbx)
	cmpq	%rcx, %r14
	jb	.L679
	movl	$2999, 8(%rbx)
	movq	%rcx, %rbx
	jmp	.L152
.L666:
	movl	%ecx, (%rdx)
	addq	$1, %r15
	movq	%rsi, %rdx
	jmp	.L241
.L71:
	cmpl	$127, %eax
	ja	.L115
	leaq	1(%r15), %rdx
	movq	%rdx, 152(%rsp)
	movb	%al, (%r15)
	movq	144(%rsp), %rax
.L116:
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	je	.L137
.L550:
	movl	0(%rbp), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
.L74:
	subq	40(%rsp), %rax
	cmpq	%rcx, %rax
	jle	.L680
	movq	16(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	152(%rsp), %r15
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r11
	movq	56(%rsp), %rax
	movl	%edx, 0(%rbp)
	movq	96(%rax), %rax
.L551:
	movl	16(%r13), %esi
	movl	%edx, 40(%rsp)
	movl	%esi, 48(%rsp)
	jmp	.L30
.L660:
	leal	72(%rcx), %esi
	cmpb	$17, %sil
	jbe	.L416
	leal	125(%rcx), %esi
	cmpb	$3, %sil
	jbe	.L416
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	144(%rsp), %rdi
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r15
	jmp	.L297
.L644:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-90, (%rdx)
.L560:
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
.L562:
	movl	$0, 0(%rbp)
.L564:
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r15
	movq	%r15, 144(%rsp)
	jmp	.L266
.L662:
	testl	%ecx, %ecx
	jne	.L13
.L14:
	movq	24(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	$1
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r10
	popq	%r11
	jmp	.L7
.L637:
	movq	88(%rsp), %rax
	addq	$1, %rdx
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	jmp	.L152
.L672:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-87, (%rdx)
	movq	144(%rsp), %rdi
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r15
	jmp	.L297
.L668:
	cmpl	$2999, %esi
	jne	.L558
	movl	$1080, 0(%rbp)
	movq	%rdi, %r15
	movq	%rdi, 144(%rsp)
	jmp	.L266
.L678:
	movl	$773392, 0(%rbp)
	movq	%rsi, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L11:
	movq	$0, (%rbx)
	testb	$1, 16(%r13)
	movl	$0, 8(%rsp)
	jne	.L7
	movq	24(%rsp), %rbx
	movl	%edx, 8(%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	movl	16(%rsp), %edx
	pushq	%rdx
	xorl	%edx, %edx
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r8
	popq	%r9
	jmp	.L7
.L415:
	cmpl	$3015, %eax
	je	.L681
.L244:
	leal	-184(%rcx), %ebx
	cmpl	$17, %ebx
	ja	.L240
	andl	$8, %r10d
	jne	.L240
	leal	-128(%rcx), %eax
	addq	$1, %r15
	movzwl	(%r8,%rax,4), %eax
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
	orl	$8, 0(%rbp)
	jmp	.L241
.L618:
	movl	%ecx, %eax
	movq	%rsi, %rbx
	sall	$8, %eax
	movl	%eax, 0(%rbp)
	jmp	.L554
.L658:
	andl	$8, %r10d
	je	.L240
	leal	-164(%rcx), %ebx
	cmpl	$1, %ebx
	ja	.L240
	addl	$2845, %ecx
	addq	$1, %r15
	movl	%ecx, (%rdx)
	movl	$0, 0(%rbp)
	movq	%rsi, %rdx
	jmp	.L241
.L352:
	movq	%r11, %rbx
	movq	%r12, %rdx
	movl	$4, 8(%rsp)
	jmp	.L147
.L595:
	cmpl	$184, %edx
	je	.L190
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	leaq	consonant_with_virama(%rip), %rax
	movzbl	(%rax,%rdi), %eax
	movb	%al, (%rbx)
	movq	128(%rsp), %rax
	movl	$0, 0(%rbp)
	movq	136(%rsp), %rbx
	addq	$4, %rax
	jmp	.L207
.L642:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movzbl	(%rbx,%r8), %ecx
.L559:
	movb	%cl, (%rdx)
.L563:
	movq	144(%rsp), %rdi
	movl	$0, 0(%rbp)
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r15
	movq	%r15, 144(%rsp)
	jmp	.L266
.L409:
	movq	%r12, %rax
.L13:
	movq	%rax, 0(%r13)
	movl	$0, 8(%rsp)
	jmp	.L7
.L248:
	leal	-166(%rcx), %eax
	cmpl	$2, %eax
	jbe	.L682
	leal	-138(%rcx), %eax
	cmpl	$1, %eax
	jbe	.L683
	cmpl	$130, %ecx
	je	.L684
	cmpl	$135, %ecx
	je	.L685
	cmpl	$140, %ecx
	je	.L686
	cmpq	$0, 88(%rsp)
	je	.L388
	movl	96(%rsp), %r10d
	testl	%r10d, %r10d
	jne	.L687
.L388:
	movl	48(%rsp), %r10d
	movl	$6, %ebx
	jmp	.L237
.L643:
	leaq	1(%rdx), %rcx
	leaq	consonant_with_uu(%rip), %rsi
	movq	%rcx, 152(%rsp)
	movzbl	(%rsi,%r8), %ecx
	jmp	.L559
.L115:
	movq	40(%rsp), %rdi
	leal	-2944(%rax), %edx
	addq	%r10, %rdi
	cmpl	$127, %edx
	movq	%rdi, 48(%rsp)
	ja	.L117
	leaq	ucs4_to_tscii(%rip), %rcx
	movzbl	(%rcx,%rdx), %edx
	testb	%dl, %dl
	je	.L118
	leal	72(%rdx), %eax
	cmpb	$17, %al
	jbe	.L412
	leal	125(%rdx), %eax
	cmpb	$3, %al
	jbe	.L412
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%r15)
	movq	144(%rsp), %rax
	jmp	.L116
.L72:
	leal	-131(%rcx), %esi
	cmpl	$3, %esi
	ja	.L98
	cmpl	$132, %ecx
	jbe	.L99
	leal	-3009(%rax), %esi
	cmpl	$1, %esi
	jbe	.L688
.L99:
	cmpl	$3021, %eax
	jne	.L95
	cmpl	$133, %ecx
	je	.L101
	leaq	1(%r15), %rax
	addl	$5, %ecx
	movq	%rax, 152(%rsp)
	movb	%cl, (%r15)
.L565:
	movq	144(%rsp), %rax
	movl	$0, 0(%rbp)
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L550
.L552:
	movq	16(%rsp), %rsi
	movq	56(%rsp), %rax
	movl	0(%rbp), %edi
	movq	(%rsi), %r11
	movl	16(%r13), %esi
	movq	96(%rax), %rax
	movl	%edi, 40(%rsp)
	movl	%esi, 48(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-89, (%rdx)
	jmp	.L560
.L37:
	cmpl	$127, %ecx
	ja	.L48
	movl	%ecx, (%r15)
	movl	0(%rbp), %edx
	movq	%rdi, %r15
	movl	%edx, %eax
	andl	$7, %eax
.L45:
	testq	%rax, %rax
	jne	.L58
.L548:
	movl	16(%r13), %ebx
	jmp	.L40
.L621:
	movq	%r12, %rdx
	movq	16(%rsp), %rax
	subq	%r11, %rdx
	addq	%r10, %rdx
	cmpq	$4, %rdx
	movq	%r12, (%rax)
	ja	.L63
	cmpq	%r10, %rdx
	leaq	1(%r11), %rax
	jbe	.L65
.L66:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%rbp,%r10)
	addq	$1, %r10
	cmpq	%r10, %rdx
	jne	.L66
.L65:
	movl	$7, 8(%rsp)
	jmp	.L7
.L302:
	cmpq	$0, 88(%rsp)
	je	.L405
	testb	$8, 16(%r13)
	je	.L312
	movl	%r10d, 96(%rsp)
	movq	%r11, 48(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %r8
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	72(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movq	48(%rsp), %r11
	movl	96(%rsp), %r10d
	je	.L689
	cmpl	$5, %eax
	je	.L307
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	jmp	.L266
.L416:
	sall	$3, %ecx
	movq	%rdi, %r15
	movl	%ecx, 0(%rbp)
	jmp	.L297
.L669:
	cmpl	$2992, %esi
	jne	.L558
	movl	$400464, 0(%rbp)
	movq	%rdi, %r15
	movq	%rdi, 144(%rsp)
	jmp	.L266
.L673:
	leaq	1(%rdx), %rcx
	addl	$121, %esi
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movq	144(%rsp), %rsi
	movq	152(%rsp), %rdx
	leaq	4(%rsi), %r15
	jmp	.L297
.L641:
	movslq	%eax, %rbx
	movq	%r12, %r15
	jmp	.L267
.L646:
	leaq	2(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-88, (%rdx)
	jmp	.L560
.L681:
	cmpl	$170, %ecx
	je	.L242
	testb	%bl, %bl
	je	.L244
	jmp	.L242
.L190:
	movl	$1888, 0(%rbp)
	movq	%rsi, %rax
	jmp	.L207
.L670:
	cmpl	$3021, %esi
	leaq	1(%rdx), %rdi
	jne	.L283
	movq	%rdi, 152(%rsp)
	movb	$-116, (%rdx)
	jmp	.L563
.L98:
	cmpl	$236, %ecx
	je	.L690
	cmpl	$138, %ecx
	je	.L691
	cmpl	$135, %ecx
	je	.L692
	cmpl	$50058, %ecx
	jne	.L693
	cmpl	$3008, %eax
	movl	$195, %esi
	je	.L694
.L111:
	leaq	2(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rax)
	jmp	.L113
.L674:
	leaq	1(%rdx), %rcx
	addl	$119, %esi
	movq	%rcx, 152(%rsp)
	movb	%sil, (%rdx)
	movq	144(%rsp), %rdi
	movq	152(%rsp), %rdx
	leaq	4(%rdi), %r15
	jmp	.L297
.L48:
	leaq	tscii_to_ucs4(%rip), %rdx
	leal	-128(%rcx), %esi
	movzwl	(%rdx,%rsi,4), %eax
	testl	%eax, %eax
	je	.L49
	movzwl	2(%rdx,%rsi,4), %edx
	movl	%eax, (%r15)
	testl	%edx, %edx
	je	.L50
	leaq	8(%r15), %rcx
	cmpq	%rcx, %r14
	jb	.L695
	movl	%edx, 4(%r15)
	movl	0(%rbp), %edx
	movq	%rcx, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L45
.L117:
	cmpl	$169, %eax
	je	.L696
	leal	-8216(%rax), %edx
	cmpl	$1, %edx
	jbe	.L697
	leal	-8220(%rax), %edx
	cmpl	$1, %edx
	jbe	.L698
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L699
	cmpq	$0, 88(%rsp)
	je	.L322
	testb	$8, %bl
	jne	.L700
	andl	$2, %ebx
	je	.L322
	movq	40(%rsp), %rdx
.L323:
	movq	88(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 144(%rsp)
.L129:
	cmpq	40(%rsp), %rax
	jne	.L550
.L322:
	movl	$6, 8(%rsp)
	jmp	.L7
.L647:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-90, (%rdx)
.L582:
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-95, (%rdx)
	jmp	.L562
.L663:
	movq	%rax, %r12
	leaq	tscii_next_state(%rip), %rsi
	jmp	.L18
.L16:
	movl	(%rbx), %edx
	sarl	$8, %edx
	movl	%edx, -4(%r12)
	movl	(%rbx), %edx
	sarl	$4, %edx
	andl	$15, %edx
	movl	(%rsi,%rdx,4), %edx
	testl	%edx, %edx
	movl	%edx, (%rbx)
	je	.L701
.L18:
	addq	$4, %r12
	cmpq	%rcx, %r12
	jbe	.L16
	jmp	.L79
.L677:
	movq	88(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L266
.L653:
	movl	$765984, 0(%rbp)
	movq	%rax, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L682:
	leal	2848(%rcx), %eax
	addq	$1, %r15
	sall	$8, %eax
	movl	%eax, 0(%rbp)
	jmp	.L241
.L377:
	movq	%rsi, %rdx
	jmp	.L241
.L652:
	movl	$773440, 0(%rbp)
	movq	%rsi, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L655:
	testb	$8, 40(%rsp)
	je	.L39
	leal	-164(%rcx), %r8d
	cmpl	$1, %r8d
	ja	.L39
	addl	$2845, %ecx
	movl	%ecx, (%r15)
	movq	%rdi, %r15
	movl	16(%r13), %ebx
	jmp	.L40
.L657:
	cmpq	%r11, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	je	.L236
.L234:
	leaq	__PRETTY_FUNCTION__.9218(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L654:
	movl	$770048, 0(%rbp)
	movq	%rcx, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L701:
	testb	$1, 16(%r13)
	jne	.L409
	cmpq	%rax, %r12
	jbe	.L14
	jmp	.L22
.L303:
	movb	$-90, (%rdx)
	movl	$-95, %edx
	jmp	.L304
.L679:
	movl	$767744, 0(%rbp)
	movq	%rax, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L118:
	leal	-3018(%rax), %edx
	cmpl	$2, %edx
	ja	.L122
	leaq	2(%r15), %rdx
	cmpq	%rdx, %r14
	jb	.L79
	leaq	1(%r15), %rdx
	cmpl	$3018, %eax
	movq	%rdx, 152(%rsp)
	je	.L123
	cmpl	$3020, %eax
	movb	$-89, (%r15)
	sete	%al
	leal	-95(%rax,%rax,8), %eax
.L124:
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	%al, (%rdx)
	movq	144(%rsp), %rax
	jmp	.L116
.L648:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-89, (%rdx)
	jmp	.L582
.L410:
	cmpl	$3015, %esi
	je	.L702
.L43:
	leal	-184(%rcx), %edx
	cmpl	$17, %edx
	ja	.L39
	testb	$8, 40(%rsp)
	jne	.L39
	leal	-128(%rcx), %edx
	leaq	tscii_to_ucs4(%rip), %rax
	movzwl	(%rax,%rdx,4), %eax
	movl	%eax, (%r15)
	movl	0(%rbp), %eax
	movq	%rdi, %r15
	movl	%eax, %edx
	andl	$7, %eax
	orl	$8, %edx
	movl	%edx, 0(%rbp)
	jmp	.L45
.L675:
	movq	%rdi, 144(%rsp)
	movq	%rdi, %r15
	jmp	.L266
.L604:
	movl	$773456, 0(%rbp)
	movq	%rsi, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L49:
	leal	-166(%rcx), %eax
	leal	2848(%rcx), %edx
	sall	$8, %edx
	cmpl	$2, %eax
	jbe	.L40
	leal	-138(%rcx), %eax
	cmpl	$1, %eax
	jbe	.L703
	cmpl	$130, %ecx
	je	.L704
	cmpl	$135, %ecx
	je	.L705
	cmpl	$140, %ecx
	je	.L706
	cmpq	$0, 88(%rsp)
	je	.L322
	andb	$2, %bl
	je	.L322
	movq	88(%rsp), %rax
	addq	$1, (%rax)
.L58:
	leaq	__PRETTY_FUNCTION__.9049(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	leaq	consonant_with_u(%rip), %rax
	movzbl	(%rax,%rsi), %eax
	movb	%al, (%r15)
.L577:
	movq	144(%rsp), %rax
	xorl	%ecx, %ecx
	movl	$0, 0(%rbp)
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L74
	jmp	.L552
.L624:
	leaq	2(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-90, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
	jmp	.L577
.L684:
	leaq	8(%rdx), %rax
	addq	$1, %r15
	movl	$3000, (%rdx)
	cmpq	%rax, %r9
	jb	.L707
	leaq	12(%rdx), %rcx
	movl	$3021, 4(%rdx)
	cmpq	%rcx, %r9
	jb	.L708
	leaq	16(%rdx), %rax
	movl	$2992, 8(%rdx)
	cmpq	%rax, %r9
	jb	.L709
	movl	$3008, 12(%rdx)
	movq	%rax, %rdx
	jmp	.L241
.L625:
	leaq	2(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-89, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
.L575:
	movq	144(%rsp), %rax
	xorl	%ecx, %ecx
	movl	$0, 0(%rbp)
	addq	$4, %rax
	cmpq	40(%rsp), %rax
	movq	%rax, 144(%rsp)
	jne	.L74
.L137:
	movq	16(%rsp), %rdi
	movq	56(%rsp), %rax
	movl	0(%rbp), %esi
	movq	(%rdi), %r11
	movl	16(%r13), %edi
	movq	96(%rax), %rax
	movl	%esi, 40(%rsp)
	movl	%edi, 48(%rsp)
	jmp	.L30
.L649:
	leaq	3(%rdx), %rsi
	cmpq	%rsi, 8(%rsp)
	jb	.L404
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	$-89, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-86, (%rdx)
	jmp	.L562
.L671:
	leaq	1(%rdx), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-126, (%rdx)
	jmp	.L562
.L286:
	movl	$1104, 0(%rbp)
	movq	%rdi, %r15
	jmp	.L297
.L412:
	movzbl	%dl, %eax
	sall	$3, %eax
	movl	%eax, 0(%rbp)
	movq	40(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L550
.L696:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-87, (%r15)
	movq	144(%rsp), %rax
	jmp	.L116
.L683:
	addl	$2862, %ecx
	addq	$1, %r15
	movl	%ecx, (%rdx)
	movl	$773384, 0(%rbp)
	movq	%rsi, %rdx
	jmp	.L241
.L605:
	movl	$767792, 0(%rbp)
	movq	%rax, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L606:
	movl	$773376, 0(%rbp)
	movq	%rcx, %rbx
	movl	$5, 8(%rsp)
	jmp	.L147
.L659:
	movl	48(%rsp), %r10d
	movslq	40(%rsp), %rbx
	movq	%r12, %r15
	jmp	.L237
.L691:
	cmpl	$2992, %eax
	jne	.L549
	movq	40(%rsp), %rax
	movl	$400464, 0(%rbp)
	xorl	%ecx, %ecx
	movl	$400464, %edx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L690:
	cmpl	$2999, %eax
	jne	.L549
	movq	40(%rsp), %rax
	movl	$1080, 0(%rbp)
	xorl	%ecx, %ecx
	movl	$1080, %edx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L74
.L623:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	leaq	consonant_with_uu(%rip), %rax
	movzbl	(%rax,%rsi), %eax
	movb	%al, (%r15)
	jmp	.L575
.L685:
	leaq	8(%rdx), %rax
	addq	$1, %r15
	movl	$2965, (%rdx)
	cmpq	%rax, %r9
	jb	.L710
	leaq	12(%rdx), %rcx
	movl	$3021, 4(%rdx)
	cmpq	%rcx, %r9
	jb	.L711
	movl	$2999, 8(%rdx)
	movq	%rcx, %rdx
	jmp	.L241
.L122:
	cmpq	$0, 88(%rsp)
	je	.L322
	testb	$8, %bl
	jne	.L712
	andl	$2, %ebx
	je	.L322
.L131:
	movq	88(%rsp), %rax
	addq	$4, 144(%rsp)
	addq	$1, (%rax)
	movq	144(%rsp), %rax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	1(%r15), %rdx
	addl	$121, %eax
	movq	%rdx, 152(%rsp)
	movb	%al, (%r15)
	movq	144(%rsp), %rax
	jmp	.L116
.L123:
	movb	$-90, (%r15)
	movl	$-95, %eax
	jmp	.L124
.L686:
	leaq	8(%rdx), %rax
	addq	$1, %r15
	movl	$2965, (%rdx)
	cmpq	%rax, %r9
	jb	.L713
	leaq	12(%rdx), %rcx
	movl	$3021, 4(%rdx)
	cmpq	%rcx, %r9
	jb	.L714
	leaq	16(%rdx), %rax
	movl	$2999, 8(%rdx)
	cmpq	%rax, %r9
	jb	.L715
	movl	$3021, 12(%rdx)
	movq	%rax, %rdx
	jmp	.L241
.L698:
	leaq	1(%r15), %rdx
	addl	$119, %eax
	movq	%rdx, 152(%rsp)
	movb	%al, (%r15)
	movq	144(%rsp), %rax
	jmp	.L116
.L703:
	addl	$2862, %ecx
	movl	$773384, %edx
	movl	%ecx, (%r15)
	movq	%rdi, %r15
	movl	16(%r13), %ebx
	jmp	.L40
.L50:
	movl	0(%rbp), %edx
	movq	%rdi, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L45
.L656:
	cmpq	%r11, 8(%rsp)
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
	jne	.L234
.L233:
	leaq	__PRETTY_FUNCTION__.9218(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.L79
	movb	%sil, (%rax)
	movb	%dil, 1(%rax)
	movq	%rdx, %r12
	testb	$1, 16(%r13)
	movq	32(%r13), %rcx
	movl	$0, (%rcx)
	jne	.L325
	cmpq	%rdx, %rax
	jnb	.L14
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	2(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-88, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
	jmp	.L577
.L692:
	cmpl	$3021, %eax
	leaq	1(%r15), %rsi
	jne	.L96
	movq	%rsi, 152(%rsp)
	movb	$-116, (%r15)
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L687:
	movq	88(%rsp), %rax
	addq	$1, %r15
	movl	$6, 40(%rsp)
	addq	$1, (%rax)
	jmp	.L241
.L689:
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	jmp	.L312
.L704:
	leaq	8(%r15), %rdx
	movl	$3000, (%r15)
	cmpq	%rdx, %r14
	jb	.L329
	leaq	12(%r15), %rax
	movl	$3021, 4(%r15)
	cmpq	%rax, %r14
	jb	.L330
	leaq	16(%r15), %rcx
	movl	$2992, 8(%r15)
	cmpq	%rcx, %r14
	jb	.L331
	movl	$3008, 12(%r15)
	movl	0(%rbp), %edx
	movq	%rcx, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	1(%r15), %rax
	addl	$5, %ecx
	movq	%rax, 152(%rsp)
	movb	%cl, (%r15)
	xorl	%ecx, %ecx
	movq	144(%rsp), %rax
	cmpq	40(%rsp), %rax
	movl	$0, 0(%rbp)
	jne	.L74
	movq	56(%rsp), %rax
	movq	16(%rsp), %rsi
	movl	16(%r13), %edi
	movl	$0, 40(%rsp)
	movq	96(%rax), %rax
	movq	(%rsi), %r11
	movl	%edi, 48(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L702:
	cmpl	$170, %ecx
	je	.L41
	testb	%r8b, %r8b
	je	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	3(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-89, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-95, (%rax)
	jmp	.L577
.L627:
	leaq	3(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-90, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-95, (%rax)
	jmp	.L575
.L650:
	cmpl	$184, %ecx
	je	.L280
	leaq	1(%rdx), %rcx
	leaq	consonant_with_virama(%rip), %rdi
	movq	%rcx, 152(%rsp)
	movzbl	(%rdi,%r8), %ecx
	movb	%cl, (%rdx)
	movl	$0, 0(%rbp)
	jmp	.L564
.L667:
	sall	$8, %ecx
	cmpq	%rsi, 8(%rsp)
	movq	16(%rsp), %rax
	movl	%ecx, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	jne	.L234
.L386:
	movq	8(%rsp), %rdx
	jmp	.L250
.L651:
	leal	-3007(%rsi), %r8d
	leaq	1(%rdx), %rdi
	cmpl	$1, %r8d
	ja	.L283
	addl	$11, %esi
	movq	%rdi, 152(%rsp)
	movb	%sil, (%rdx)
	jmp	.L563
.L706:
	leaq	8(%r15), %rdx
	movl	$2965, (%r15)
	cmpq	%rdx, %r14
	jb	.L334
	leaq	12(%r15), %rax
	movl	$3021, 4(%r15)
	cmpq	%rax, %r14
	jb	.L335
	leaq	16(%r15), %rcx
	movl	$2999, 8(%r15)
	cmpq	%rcx, %r14
	jb	.L336
	movl	$3021, 12(%r15)
	movl	0(%rbp), %edx
	movq	%rcx, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L45
.L318:
	leaq	__PRETTY_FUNCTION__.9218(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L280:
	movl	$1888, 0(%rbp)
	jmp	.L564
.L336:
	movq	%rax, %r15
	movl	$773376, %edx
	jmp	.L548
.L335:
	movq	%rdx, %r15
	movl	16(%r13), %ebx
	movl	$767792, %edx
	jmp	.L40
.L334:
	movq	%rdi, %r15
	movl	$773456, %edx
	movl	16(%r13), %ebx
	jmp	.L40
.L694:
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-126, (%r15)
	jmp	.L577
.L693:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L700:
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	64(%rsp), %r8
	movq	%r11, %rdx
	movq	72(%rsp), %rdi
	movq	%r13, %rsi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r11
	popq	%rdx
	movq	96(%rsp), %r10
	je	.L716
.L127:
	movq	144(%rsp), %rax
	cmpq	40(%rsp), %rax
	jne	.L550
	cmpl	$7, 8(%rsp)
	jne	.L141
	movq	40(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 48(%rsp)
	je	.L717
	movl	0(%rbp), %eax
	movq	%r10, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	16(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r10
	jle	.L718
	cmpq	$4, %r10
	ja	.L719
	orl	%r10d, %eax
	testq	%r10, %r10
	movl	%eax, 0(%rbp)
	je	.L65
	movq	40(%rsp), %rcx
	xorl	%eax, %eax
.L145:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbp,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L145
	jmp	.L65
.L325:
	movq	%rdx, %rax
	jmp	.L13
.L719:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L718:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L717:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L141:
	cmpl	$0, 8(%rsp)
	je	.L552
	jmp	.L7
.L716:
	movq	144(%rsp), %rax
	andb	$2, %bl
	movq	%rax, %rdx
	je	.L129
	jmp	.L323
.L709:
	cmpq	%rcx, 8(%rsp)
	movq	16(%rsp), %rax
	movl	$770048, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	je	.L386
	jmp	.L234
.L331:
	movq	%rax, %r15
	movl	$770048, %edx
	movl	16(%r13), %ebx
	jmp	.L40
.L330:
	movq	%rdx, %r15
	movl	16(%r13), %ebx
	movl	$765984, %edx
	jmp	.L40
.L329:
	movq	%rdi, %r15
	movl	$773440, %edx
	movl	16(%r13), %ebx
	jmp	.L40
.L307:
	movq	120(%rsp), %rdx
	cmpq	152(%rsp), %rdx
	movq	144(%rsp), %rax
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	je	.L250
	jmp	.L234
.L705:
	leaq	8(%r15), %rax
	movl	$2965, (%r15)
	cmpq	%rax, %r14
	jb	.L332
	leaq	12(%r15), %rcx
	movl	$3021, 4(%r15)
	cmpq	%rcx, %r14
	jb	.L333
	movl	$2999, 8(%r15)
	movl	0(%rbp), %edx
	movq	%rcx, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L45
.L60:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L680:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L333:
	movq	%rax, %r15
	movl	$767744, %edx
	movl	16(%r13), %ebx
	jmp	.L40
.L332:
	movq	%rdi, %r15
	movl	$773392, %edx
	movl	16(%r13), %ebx
	jmp	.L40
.L34:
	leaq	__PRETTY_FUNCTION__.9049(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L661:
	leaq	__PRETTY_FUNCTION__.9218(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L708:
	cmpq	%rax, 8(%rsp)
	movq	16(%rsp), %rdi
	movl	$765984, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rdi)
	je	.L386
	jmp	.L234
.L707:
	cmpq	%rsi, 8(%rsp)
	movq	16(%rsp), %rax
	movl	$773440, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	je	.L386
	jmp	.L234
.L712:
	movq	%r10, 96(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	64(%rsp), %r8
	movq	%r13, %rsi
	movq	72(%rsp), %rdi
	movq	%r11, %rdx
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%rcx
	popq	%rsi
	movq	96(%rsp), %r10
	jne	.L127
	andb	$2, %bl
	jne	.L131
	movq	144(%rsp), %rax
	jmp	.L129
.L715:
	cmpq	%rcx, 8(%rsp)
	movq	16(%rsp), %rax
	movl	$773376, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	je	.L386
	jmp	.L234
.L714:
	cmpq	%rax, 8(%rsp)
	movq	16(%rsp), %rdi
	movl	$767792, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rdi)
	je	.L386
	jmp	.L234
.L713:
	cmpq	%rsi, 8(%rsp)
	movq	16(%rsp), %rax
	movl	$773456, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	je	.L386
	jmp	.L234
.L711:
	cmpq	%rax, 8(%rsp)
	movq	16(%rsp), %rdi
	movl	$767744, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rdi)
	je	.L386
	jmp	.L234
.L710:
	cmpq	%rsi, 8(%rsp)
	movq	16(%rsp), %rax
	movl	$773392, 0(%rbp)
	movl	48(%rsp), %r10d
	movq	%r15, (%rax)
	je	.L386
	jmp	.L234
.L101:
	movq	144(%rsp), %rax
	movl	$1104, 0(%rbp)
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L550
.L695:
	sall	$8, %edx
	movq	%rdi, %r15
	movl	16(%r13), %ebx
	jmp	.L40
.L665:
	movl	%ebp, (%rbx)
	jmp	.L25
.L631:
	leal	-3007(%rax), %edi
	leaq	1(%r15), %rsi
	cmpl	$1, %edi
	ja	.L96
	addl	$11, %eax
	movq	%rsi, 152(%rsp)
	movb	%al, (%r15)
	jmp	.L575
.L619:
	leaq	__PRETTY_FUNCTION__.9218(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L293:
	leaq	__PRETTY_FUNCTION__.9122(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L699:
	movq	40(%rsp), %rax
	movslq	%esi, %rcx
	movl	%esi, %edx
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L74
.L63:
	leaq	__PRETTY_FUNCTION__.9142(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L630:
	cmpl	$184, %ecx
	je	.L92
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	leaq	consonant_with_virama(%rip), %rax
	movzbl	(%rax,%rsi), %eax
	movb	%al, (%r15)
	jmp	.L565
.L629:
	leaq	3(%r15), %rax
	cmpq	%rax, %r14
	jb	.L79
	leaq	1(%r15), %rax
	movq	%rax, 152(%rsp)
	movb	$-89, (%r15)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 152(%rsp)
	movb	%cl, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 152(%rsp)
	movb	$-86, (%rax)
	jmp	.L575
.L92:
	movq	144(%rsp), %rax
	movl	$1888, 0(%rbp)
	addq	$4, %rax
	movq	%rax, 144(%rsp)
	jmp	.L550
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9122, @object
	.size	__PRETTY_FUNCTION__.9122, 9
__PRETTY_FUNCTION__.9122:
	.string	"to_tscii"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9142, @object
	.size	__PRETTY_FUNCTION__.9142, 16
__PRETTY_FUNCTION__.9142:
	.string	"to_tscii_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9049, @object
	.size	__PRETTY_FUNCTION__.9049, 18
__PRETTY_FUNCTION__.9049:
	.string	"from_tscii_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9218, @object
	.size	__PRETTY_FUNCTION__.9218, 6
__PRETTY_FUNCTION__.9218:
	.string	"gconv"
	.section	.rodata
	.align 16
	.type	consonant_with_virama, @object
	.size	consonant_with_virama, 18
consonant_with_virama:
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.byte	-16
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-4
	.byte	-3
	.align 16
	.type	consonant_with_uu, @object
	.size	consonant_with_uu, 18
consonant_with_uu:
	.byte	-36
	.byte	-101
	.byte	-35
	.byte	-100
	.byte	-34
	.byte	-33
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.align 16
	.type	consonant_with_u, @object
	.size	consonant_with_u, 18
consonant_with_u:
	.byte	-52
	.byte	-103
	.byte	-51
	.byte	-102
	.byte	-50
	.byte	-49
	.byte	-48
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	-37
	.align 32
	.type	ucs4_to_tscii, @object
	.size	ucs4_to_tscii, 128
ucs4_to_tscii:
	.byte	0
	.byte	0
	.byte	0
	.byte	-73
	.byte	0
	.byte	-85
	.byte	-84
	.byte	-2
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	0
	.byte	0
	.byte	0
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	0
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	-72
	.byte	0
	.byte	0
	.byte	0
	.byte	-71
	.byte	-70
	.byte	0
	.byte	-125
	.byte	0
	.byte	-69
	.byte	-68
	.byte	0
	.byte	0
	.byte	0
	.byte	-67
	.byte	-66
	.byte	0
	.byte	0
	.byte	0
	.byte	-65
	.byte	-55
	.byte	-64
	.byte	0
	.byte	0
	.byte	0
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-56
	.byte	-60
	.byte	-57
	.byte	-58
	.byte	-59
	.byte	0
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-95
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	0
	.byte	0
	.byte	0
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-86
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-128
	.byte	-127
	.byte	-115
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.align 16
	.type	tscii_next_state, @object
	.size	tscii_next_state, 24
tscii_next_state:
	.long	0
	.long	767744
	.long	770048
	.long	773376
	.long	765984
	.long	767792
	.align 32
	.type	tscii_to_ucs4, @object
	.size	tscii_to_ucs4, 512
tscii_to_ucs4:
	.value	3046
	.value	0
	.value	3047
	.value	0
	.value	0
	.value	0
	.value	2972
	.value	0
	.value	2999
	.value	0
	.value	3000
	.value	0
	.value	3001
	.value	0
	.value	0
	.value	0
	.value	2972
	.value	3021
	.value	2999
	.value	3021
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	3048
	.value	0
	.value	3049
	.value	0
	.value	3050
	.value	0
	.value	3051
	.value	0
	.value	8216
	.value	0
	.value	8217
	.value	0
	.value	8220
	.value	0
	.value	8221
	.value	0
	.value	3052
	.value	0
	.value	3053
	.value	0
	.value	3054
	.value	0
	.value	3055
	.value	0
	.value	2969
	.value	3009
	.value	2974
	.value	3009
	.value	2969
	.value	3010
	.value	2974
	.value	3010
	.value	3056
	.value	0
	.value	3057
	.value	0
	.value	3058
	.value	0
	.value	0
	.value	0
	.value	3006
	.value	0
	.value	3007
	.value	0
	.value	3008
	.value	0
	.value	3009
	.value	0
	.value	3010
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	0
	.value	169
	.value	0
	.value	3031
	.value	0
	.value	2949
	.value	0
	.value	2950
	.value	0
	.value	2951
	.value	0
	.value	2952
	.value	0
	.value	2953
	.value	0
	.value	2954
	.value	0
	.value	2958
	.value	0
	.value	2959
	.value	0
	.value	2960
	.value	0
	.value	2962
	.value	0
	.value	2963
	.value	0
	.value	2964
	.value	0
	.value	2947
	.value	0
	.value	2965
	.value	0
	.value	2969
	.value	0
	.value	2970
	.value	0
	.value	2974
	.value	0
	.value	2975
	.value	0
	.value	2979
	.value	0
	.value	2980
	.value	0
	.value	2984
	.value	0
	.value	2986
	.value	0
	.value	2990
	.value	0
	.value	2991
	.value	0
	.value	2992
	.value	0
	.value	2994
	.value	0
	.value	2997
	.value	0
	.value	2996
	.value	0
	.value	2995
	.value	0
	.value	2993
	.value	0
	.value	2985
	.value	0
	.value	2975
	.value	3007
	.value	2975
	.value	3008
	.value	2965
	.value	3009
	.value	2970
	.value	3009
	.value	2975
	.value	3009
	.value	2979
	.value	3009
	.value	2980
	.value	3009
	.value	2984
	.value	3009
	.value	2986
	.value	3009
	.value	2990
	.value	3009
	.value	2991
	.value	3009
	.value	2992
	.value	3009
	.value	2994
	.value	3009
	.value	2997
	.value	3009
	.value	2996
	.value	3009
	.value	2995
	.value	3009
	.value	2993
	.value	3009
	.value	2985
	.value	3009
	.value	2965
	.value	3010
	.value	2970
	.value	3010
	.value	2975
	.value	3010
	.value	2979
	.value	3010
	.value	2980
	.value	3010
	.value	2984
	.value	3010
	.value	2986
	.value	3010
	.value	2990
	.value	3010
	.value	2991
	.value	3010
	.value	2992
	.value	3010
	.value	2994
	.value	3010
	.value	2997
	.value	3010
	.value	2996
	.value	3010
	.value	2995
	.value	3010
	.value	2993
	.value	3010
	.value	2985
	.value	3010
	.value	2965
	.value	3021
	.value	2969
	.value	3021
	.value	2970
	.value	3021
	.value	2974
	.value	3021
	.value	2975
	.value	3021
	.value	2979
	.value	3021
	.value	2980
	.value	3021
	.value	2984
	.value	3021
	.value	2986
	.value	3021
	.value	2990
	.value	3021
	.value	2991
	.value	3021
	.value	2992
	.value	3021
	.value	2994
	.value	3021
	.value	2997
	.value	3021
	.value	2996
	.value	3021
	.value	2995
	.value	3021
	.value	2993
	.value	3021
	.value	2985
	.value	3021
	.value	2951
	.value	0
	.value	0
	.value	0
