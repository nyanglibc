	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-16//"
.LC1:
	.string	"UTF-16BE//"
.LC2:
	.string	"UTF-16LE//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	pushq	%r12
	pushq	%rbp
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	movq	24(%rdi), %rbp
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L9
	movq	32(%rbx), %r12
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L17
	movl	$1, %ebp
.L3:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L8
	movl	$1, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869188, %rax
	movq	%rax, 72(%rbx)
	subq	$2, %rax
	movq	%rax, 80(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %ebp
.L2:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L6
.L8:
	movl	$3, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$2, (%rax)
	movl	%ebp, 4(%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869186, %rax
	movq	%rax, 72(%rbx)
	addq	$2, %rax
	movq	%rax, 80(%rbx)
.L5:
	movl	$0, 88(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L11
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L12
	leaq	.LC2(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L13
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L14
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$3, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$3, %ebp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$2, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$2, %ebp
	jmp	.L3
	.size	gconv_init, .-gconv_init
	.p2align 4,,15
	.globl	gconv_end
	.type	gconv_end, @function
gconv_end:
	movq	96(%rdi), %rdi
	jmp	free@PLT
	.size	gconv_end, .-gconv_end
	.section	.rodata.str1.1
.LC3:
	.string	"../iconv/skeleton.c"
.LC4:
	.string	"outbufstart == NULL"
.LC5:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC7:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC8:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC9:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC10:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC11:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC12:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC14:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%r12), %r11d
	movq	%rdi, 96(%rsp)
	leaq	48(%r12), %rdi
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 56(%rsp)
	testb	$1, %r11b
	movl	224(%rsp), %ebx
	movq	%rsi, 64(%rsp)
	movq	%rdi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L20
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 24(%rsp)
	je	.L20
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L20:
	testl	%ebx, %ebx
	jne	.L377
	movq	16(%rsp), %rax
	leaq	128(%rsp), %rdx
	movl	20(%r12), %r9d
	movq	8(%r12), %r10
	movq	(%rax), %rcx
	movq	32(%rsp), %rax
	testq	%rax, %rax
	cmove	%r12, %rax
	cmpq	$0, 56(%rsp)
	movq	(%rax), %r14
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%r9d, %r9d
	movq	%rax, 88(%rsp)
	movq	96(%rsp), %rax
	movq	96(%rax), %rax
	movl	(%rax), %esi
	movl	%esi, 52(%rsp)
	je	.L27
.L332:
	movl	%r11d, %r15d
	andl	$4, %r15d
.L28:
	movl	232(%rsp), %esi
	testl	%esi, %esi
	jne	.L38
.L335:
	movq	16(%rsp), %rax
	movq	(%rax), %r13
.L39:
	leaq	152(%rsp), %rax
	cmpl	$2, 52(%rsp)
	movq	%r10, 8(%rsp)
	movq	%rax, 104(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 112(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 80(%rsp)
	je	.L378
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rbx
	movq	%r13, %rax
	movl	$4, %r10d
	andl	$2, %r11d
.L116:
	cmpq	%rax, %rbp
	je	.L117
.L131:
	leaq	4(%rax), %rdi
	cmpq	%rdi, %rbp
	jb	.L208
	leaq	2(%rbx), %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L217
	movl	(%rax), %edx
	leal	-55296(%rdx), %esi
	cmpl	$2047, %esi
	jbe	.L379
	testl	%r15d, %r15d
	je	.L120
	cmpl	$65535, %edx
	ja	.L380
	rolw	$8, %dx
	movw	%dx, (%rbx)
	movq	%rcx, %rbx
.L126:
	movq	%rdi, %rax
	movq	%rbx, 152(%rsp)
	movq	%rdi, 144(%rsp)
	cmpq	%rax, %rbp
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L117:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rcx
	movq	%rax, (%rcx)
	jne	.L381
.L132:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L382
	cmpq	%rbx, %r14
	jnb	.L218
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r10d, 40(%rsp)
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	72(%rsp), %r9
	movq	96(%rsp), %rdx
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%r8
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r9
	movl	40(%rsp), %r10d
	je	.L136
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L383
.L135:
	testl	%r11d, %r11d
	jne	.L244
.L174:
	cmpl	$2, 52(%rsp)
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r14
	movq	(%rax), %r13
	jne	.L103
.L378:
	cmpq	%r13, %rbp
	je	.L193
	leaq	2(%r13), %rcx
	cmpq	%rcx, %rbp
	jb	.L194
	movq	8(%rsp), %r9
	leaq	4(%r14), %rsi
	movq	%r14, %rbx
	movq	%r13, %rdx
	cmpq	%rsi, %r9
	jb	.L197
	movl	$4, %r10d
	andl	$2, %r11d
.L105:
	testl	%r15d, %r15d
	movzwl	(%rdx), %eax
	je	.L106
	rolw	$8, %ax
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L107
	movzwl	%ax, %eax
	movq	%rcx, %rdx
	movl	%eax, (%rbx)
	movq	%rsi, %rbx
.L108:
	cmpq	%rbp, %rdx
	je	.L104
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %rbp
	jb	.L205
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r9
	jnb	.L105
.L197:
	movl	$5, %r10d
.L104:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	je	.L132
.L381:
	movq	32(%rsp), %rax
	movl	%r10d, %r8d
	movq	%rbx, (%rax)
.L19:
	addq	$168, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	cmpl	$65535, %edx
	ja	.L384
	movw	%dx, (%rbx)
	movq	%rcx, %rbx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L106:
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L111
	movl	%eax, (%rbx)
	movq	%rcx, %rdx
	movq	%rsi, %rbx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L136:
	cmpl	$5, %r10d
	movl	%r10d, %r11d
	jne	.L135
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L208:
	movl	$7, %r10d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$5, %r10d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L379:
	cmpq	$0, 88(%rsp)
	je	.L216
	testl	%r11d, %r11d
	jne	.L385
.L216:
	movl	$6, %r10d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$7, %r10d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L38:
	movq	32(%r12), %rbx
	movl	(%rbx), %ecx
	movl	%ecx, %esi
	andl	$7, %esi
	je	.L335
	cmpq	$0, 32(%rsp)
	jne	.L386
	cmpl	$2, 52(%rsp)
	movq	16(%rsp), %rax
	movq	(%rax), %rdx
	je	.L387
	cmpl	$4, %esi
	movq	%rdx, 144(%rsp)
	movq	%r14, 152(%rsp)
	ja	.L66
	movslq	%esi, %r13
	leaq	136(%rsp), %rsi
	xorl	%eax, %eax
	movq	%rsi, 8(%rsp)
.L67:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%r13, %rax
	jne	.L67
	movq	%rdx, %rax
	subq	%r13, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L388
	leaq	2(%r14), %r8
	cmpq	%r8, %r10
	jb	.L182
	leaq	1(%rdx), %rax
	leaq	135(%rsp), %rdi
.L74:
	movq	%rax, 144(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r13
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	$3, %r13
	movb	%cl, (%rdi,%r13)
	ja	.L247
	cmpq	%rsi, %rbp
	ja	.L74
.L247:
	movq	8(%rsp), %rax
	movq	%rax, 144(%rsp)
	addq	%r13, %rax
	movq	%rax, 40(%rsp)
	movl	136(%rsp), %eax
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L389
	testl	%r15d, %r15d
	jne	.L390
	cmpl	$65535, %eax
	ja	.L391
	movw	%ax, (%r14)
.L89:
	movq	8(%rsp), %rax
	movq	%r8, 152(%rsp)
	addq	$4, %rax
	movq	%rax, 144(%rsp)
.L78:
	movl	(%rbx), %edx
	subq	8(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L392
	subq	%rcx, %rax
	movq	16(%rsp), %rcx
	andl	$-8, %edx
	movq	152(%rsp), %r14
	movl	16(%r12), %r11d
	addq	(%rcx), %rax
	movq	%rax, (%rcx)
	movq	%rax, %r13
	movl	%edx, (%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L382:
	movq	56(%rsp), %rcx
	movq	%rbx, (%r12)
	movl	%r10d, %r8d
	movq	128(%rsp), %rax
	addq	%rax, (%rcx)
.L134:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L19
	cmpl	$7, %r8d
	jne	.L19
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L176
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L178
.L177:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L177
.L178:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L380:
	cmpl	$1114111, %edx
	ja	.L376
	leaq	4(%rbx), %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L217
	movl	%edx, %eax
	andw	$1023, %dx
	shrl	$10, %eax
	subw	$9216, %dx
	subw	$10304, %ax
	rolw	$8, %dx
	rolw	$8, %ax
	movw	%dx, 2(%rbx)
	movw	%ax, (%rbx)
	movq	%rcx, %rbx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L384:
	cmpl	$1114111, %edx
	ja	.L376
	leaq	4(%rbx), %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L217
	movl	%edx, %eax
	andw	$1023, %dx
	shrl	$10, %eax
	subw	$9216, %dx
	subw	$10304, %ax
	movw	%dx, 2(%rbx)
	movw	%ax, (%rbx)
	movq	%rcx, %rbx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L218:
	movl	%r10d, %r11d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L385:
	movq	88(%rsp), %rax
	movq	%rdi, 144(%rsp)
	movl	$6, %r10d
	addq	$1, (%rax)
	movq	%rdi, %rax
	jmp	.L116
.L387:
	cmpl	$4, %esi
	ja	.L43
	movslq	%esi, %rsi
	xorl	%r8d, %r8d
	leaq	152(%rsp), %rdi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%rax, %r8
.L44:
	movzbl	4(%rbx,%r8), %eax
	movb	%al, (%rdi,%r8)
	leaq	1(%r8), %rax
	cmpq	%rax, %rsi
	jne	.L183
	movq	%rdx, %r9
	subq	%rax, %r9
	addq	$2, %r9
	cmpq	%r9, %rbp
	jb	.L393
	leaq	4(%r14), %r8
	cmpq	%r8, %r10
	movq	%r8, 8(%rsp)
	jb	.L182
	leaq	151(%rsp), %r9
	movq	%rdx, %r8
.L49:
	addq	$1, %r8
	movzbl	-1(%r8), %r13d
	addq	$1, %rax
	cmpq	$3, %rax
	movb	%r13b, (%r9,%rax)
	ja	.L246
	cmpq	%r8, %rbp
	ja	.L49
.L246:
	testl	%r15d, %r15d
	leaq	(%rdi,%rax), %r13
	movzwl	152(%rsp), %r9d
	je	.L51
	rolw	$8, %r9w
	leal	10240(%r9), %r8d
	cmpw	$2047, %r8w
	jbe	.L52
	movzwl	%r9w, %r9d
	leaq	2(%rdi), %r8
	movl	%r9d, (%r14)
.L53:
	movl	(%rbx), %eax
	subq	%rdi, %r8
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %r8
	jle	.L394
	subq	%rcx, %r8
	movq	16(%rsp), %rcx
	andl	$-8, %eax
	leaq	(%rdx,%r8), %r13
	movl	16(%r12), %r11d
	movq	8(%rsp), %r14
	movq	%r13, (%rcx)
	movl	%eax, (%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$5, %r8d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	$2, 52(%rsp)
	movq	16(%rsp), %rax
	movl	16(%r12), %edi
	movq	%r13, (%rax)
	je	.L395
	andl	$2, %edi
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rdx
	movl	$4, %eax
	movl	%edi, %ebx
.L156:
	cmpq	%r13, %rbp
	je	.L396
.L172:
	leaq	4(%r13), %r8
	cmpq	%r8, %rbp
	jb	.L232
	leaq	2(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L242
	movl	0(%r13), %ecx
	leal	-55296(%rcx), %edi
	cmpl	$2047, %edi
	jbe	.L397
	testl	%r15d, %r15d
	je	.L160
	cmpl	$65535, %ecx
	ja	.L398
	rolw	$8, %cx
	movw	%cx, (%rdx)
	movq	%rsi, %rdx
.L167:
	movq	%r8, %r13
	movq	%rdx, 152(%rsp)
	movq	%r8, 144(%rsp)
	cmpq	%r13, %rbp
	jne	.L172
.L396:
	movslq	%eax, %rbx
	movq	%rbp, %r13
	.p2align 4,,10
	.p2align 3
.L157:
	movq	16(%rsp), %rax
	movq	136(%rsp), %r10
	movq	%r13, (%rax)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	$65535, %ecx
	ja	.L399
	movw	%cx, (%rdx)
	movq	%rsi, %rdx
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r14, %rbx
	movq	%r13, %rdx
	movl	$7, %r10d
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L111:
	cmpw	$-9217, %ax
	ja	.L352
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 40(%rsp)
	jb	.L205
	movzwl	2(%rdx), %edi
	leal	9216(%rdi), %r8d
	cmpw	$1023, %r8w
	ja	.L352
	subl	$55232, %eax
	movq	40(%rsp), %rdx
	sall	$10, %eax
	leal	-56320(%rax,%rdi), %eax
	movl	%eax, (%rbx)
	movq	%rsi, %rbx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L107:
	cmpw	$-9217, %ax
	ja	.L352
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 40(%rsp)
	jb	.L205
	movzwl	2(%rdx), %edi
	rolw	$8, %di
	leal	9216(%rdi), %r8d
	cmpw	$1023, %r8w
	ja	.L352
	movzwl	%ax, %eax
	movzwl	%di, %edi
	movq	40(%rsp), %rdx
	subl	$55232, %eax
	sall	$10, %eax
	leal	-56320(%rax,%rdi), %eax
	movl	%eax, (%rbx)
	movq	%rsi, %rbx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L377:
	cmpq	$0, 32(%rsp)
	jne	.L400
	movq	32(%r12), %rax
	xorl	%r8d, %r8d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L19
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	72(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r15
	popq	%r10
	movl	%eax, %r8d
	popq	%r11
	jmp	.L19
.L27:
	movl	4(%rax), %eax
	cmpl	$1, %eax
	je	.L401
	cmpl	$3, %eax
	jne	.L332
	orl	$4, %r11d
	movl	$4, %r15d
	movl	%r11d, 16(%r12)
	jmp	.L28
.L244:
	movl	%r11d, %r8d
	jmp	.L134
.L376:
	cmpq	$0, 88(%rsp)
	je	.L216
	testb	$8, 16(%r12)
	jne	.L402
.L129:
	testl	%r11d, %r11d
	je	.L216
	movq	88(%rsp), %rcx
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 144(%rsp)
	addq	$1, (%rcx)
	jmp	.L116
.L395:
	cmpq	%r13, %rbp
	je	.L343
	leaq	2(%r13), %rcx
	cmpq	%rbp, %rcx
	ja	.L343
	leaq	4(%r14), %rsi
	andl	$2, %edi
	movq	%r14, %rdx
	movl	$4, %ebx
	movl	%edi, %r9d
	cmpq	%rsi, %r10
	jb	.L403
.L143:
	testl	%r15d, %r15d
	movzwl	0(%r13), %eax
	je	.L146
	rolw	$8, %ax
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L147
	movzwl	%ax, %eax
	movq	%rcx, %r13
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
.L148:
	cmpq	%r13, %rbp
	je	.L404
	leaq	2(%r13), %rcx
	cmpq	%rcx, %rbp
	jb	.L229
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jnb	.L143
	movl	$5, %ebx
.L145:
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
.L155:
	cmpq	%rdx, %r10
	jne	.L141
	cmpq	$5, %rbx
	jne	.L140
.L166:
	cmpq	%r14, %r10
	jne	.L135
.L144:
	subl	$1, 20(%r12)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L146:
	leal	10240(%rax), %edi
	cmpw	$2047, %di
	jbe	.L151
	movl	%eax, (%rdx)
	movq	%rcx, %r13
	movq	%rsi, %rdx
	jmp	.L148
.L352:
	cmpq	$0, 88(%rsp)
	je	.L207
	testl	%r11d, %r11d
	jne	.L405
.L207:
	movl	$6, %r10d
	jmp	.L104
.L390:
	cmpl	$65535, %eax
	ja	.L406
	rolw	$8, %ax
	movw	%ax, (%r14)
	jmp	.L89
.L402:
	movl	%r11d, 40(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %rsi
	movq	120(%rsp), %r9
	movq	128(%rsp), %rcx
	movq	112(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r11
	cmpl	$6, %r10d
	popq	%rbx
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movl	40(%rsp), %r11d
	je	.L129
	cmpl	$5, %r10d
	jne	.L116
	jmp	.L117
.L405:
	movq	88(%rsp), %rax
	movq	%rcx, %rdx
	movl	$6, %r10d
	addq	$1, (%rax)
	jmp	.L108
.L232:
	movl	$7, %ebx
	jmp	.L157
.L414:
	movq	%r10, 120(%rsp)
	movl	%r11d, 40(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %r8
	movq	120(%rsp), %r9
	movq	128(%rsp), %rcx
	movq	%r12, %rsi
	movq	112(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r13
	movq	152(%rsp), %rdx
	movl	40(%rsp), %r11d
	movq	120(%rsp), %r10
	je	.L170
	cmpl	$5, %eax
	jne	.L156
.L242:
	movl	$5, %ebx
	jmp	.L157
.L397:
	cmpq	$0, 88(%rsp)
	je	.L241
	testl	%ebx, %ebx
	jne	.L407
.L241:
	movl	$6, %ebx
	jmp	.L157
.L401:
	cmpl	$2, 52(%rsp)
	je	.L408
	movl	24(%r12), %r8d
	testl	%r8d, %r8d
	jne	.L332
	leaq	2(%r14), %rax
	cmpq	%r10, %rax
	ja	.L182
	movl	$-257, %edi
	movl	%r11d, %r15d
	movw	%di, (%r14)
	andl	$4, %r15d
	movq	%rax, %r14
	jmp	.L28
.L193:
	movq	%r14, %rbx
	movq	%rbp, %rdx
	movl	$4, %r10d
	jmp	.L104
.L229:
	movl	$7, %ebx
	jmp	.L145
.L399:
	cmpl	$1114111, %ecx
	ja	.L409
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L242
	movl	%ecx, %edi
	andw	$1023, %cx
	shrl	$10, %edi
	subw	$9216, %cx
	subw	$10304, %di
	movw	%cx, 2(%rdx)
	movw	%di, (%rdx)
	movq	%rsi, %rdx
	jmp	.L167
.L398:
	cmpl	$1114111, %ecx
	ja	.L410
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L242
	movl	%ecx, %edi
	andw	$1023, %cx
	shrl	$10, %edi
	subw	$9216, %cx
	subw	$10304, %di
	rolw	$8, %cx
	rolw	$8, %di
	movw	%cx, 2(%rdx)
	movw	%di, (%rdx)
	movq	%rsi, %rdx
	jmp	.L167
.L51:
	leal	10240(%r9), %r8d
	cmpw	$2047, %r8w
	jbe	.L58
	movl	%r9d, (%r14)
	leaq	2(%rdi), %r8
	jmp	.L53
.L388:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%rdx, %rax
	addq	%r13, %rax
	cmpq	$4, %rax
	ja	.L69
	cmpq	%r13, %rax
	jbe	.L71
.L70:
	addq	$1, %rdx
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %ecx
	movb	%cl, 4(%rbx,%r13)
	addq	$1, %r13
	cmpq	%r13, %rax
	jne	.L70
.L71:
	movl	$7, %r8d
	jmp	.L19
.L407:
	movq	88(%rsp), %rax
	movq	%r8, 144(%rsp)
	movq	%r8, %r13
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L156
.L408:
	leaq	2(%rcx), %rax
	cmpq	%rbp, %rax
	jbe	.L31
	xorl	%eax, %eax
	cmpq	%rbp, %rcx
	setne	%al
	leal	4(%rax,%rax,2), %eax
	movl	%eax, %r8d
	jmp	.L19
.L343:
	cmpq	%r14, %r10
	jne	.L141
.L140:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rbp, %rsi
	movq	16(%rsp), %rcx
	subq	%rdx, %rsi
	addq	%rax, %rsi
	cmpq	$4, %rsi
	movq	%rbp, (%rcx)
	ja	.L46
	movq	%r8, %rcx
	notq	%rcx
	addq	%rcx, %rdx
	cmpq	%rsi, %rax
	jnb	.L71
.L47:
	movzbl	(%rdx,%rax), %ecx
	movb	%cl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L47
	jmp	.L71
.L389:
	cmpq	$0, 88(%rsp)
	je	.L77
	andl	$2, %r11d
	jne	.L411
.L77:
	movl	$6, %r8d
	jmp	.L19
.L151:
	cmpw	$-9217, %ax
	ja	.L359
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 40(%rsp)
	jb	.L229
	movzwl	2(%r13), %edi
	leal	9216(%rdi), %r8d
	cmpw	$1023, %r8w
	ja	.L359
	subl	$55232, %eax
	movq	40(%rsp), %r13
	sall	$10, %eax
	leal	-56320(%rax,%rdi), %eax
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
	jmp	.L148
.L147:
	cmpw	$-9217, %ax
	ja	.L359
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 40(%rsp)
	jb	.L229
	movzwl	2(%r13), %edi
	rolw	$8, %di
	leal	9216(%rdi), %r8d
	cmpw	$1023, %r8w
	ja	.L359
	movzwl	%ax, %eax
	movzwl	%di, %edi
	movq	40(%rsp), %r13
	subl	$55232, %eax
	sall	$10, %eax
	leal	-56320(%rax,%rdi), %eax
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
	jmp	.L148
.L403:
	cmpq	%r14, %r10
	je	.L144
.L141:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	movzwl	(%rcx), %edx
	cmpw	$-257, %dx
	je	.L412
	cmpw	$-2, %dx
	jne	.L332
	movq	16(%rsp), %rcx
	orl	$4, %r11d
	movl	$4, %r15d
	movl	%r11d, 16(%r12)
	movq	%rax, (%rcx)
	jmp	.L28
.L404:
	movq	%rbp, %r13
	jmp	.L145
.L359:
	cmpq	$0, 88(%rsp)
	je	.L231
	testl	%r9d, %r9d
	jne	.L413
.L231:
	movl	$6, %ebx
	jmp	.L145
.L406:
	cmpl	$1114111, %eax
	ja	.L366
	leaq	4(%r14), %r8
	cmpq	%r8, %r10
	jb	.L182
	movl	%eax, %edx
	andw	$1023, %ax
	shrl	$10, %edx
	subw	$9216, %ax
	subw	$10304, %dx
	rolw	$8, %ax
	rolw	$8, %dx
	movw	%ax, 2(%r14)
	movw	%dx, (%r14)
	jmp	.L89
.L391:
	cmpl	$1114111, %eax
	ja	.L366
	leaq	4(%r14), %r8
	cmpq	%r8, %r10
	jb	.L182
	movl	%eax, %edx
	andw	$1023, %ax
	shrl	$10, %edx
	subw	$9216, %ax
	subw	$10304, %dx
	movw	%ax, 2(%r14)
	movw	%dx, (%r14)
	jmp	.L89
.L409:
	cmpq	$0, 88(%rsp)
	je	.L241
	testb	$8, 16(%r12)
	jne	.L414
.L170:
	testl	%ebx, %ebx
	je	.L241
	movq	88(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L411:
	movq	8(%rsp), %rax
	movq	88(%rsp), %rcx
	addq	$4, %rax
	addq	$1, (%rcx)
	movq	%rax, 144(%rsp)
	jmp	.L78
.L410:
	cmpq	$0, 88(%rsp)
	je	.L241
	testb	$8, 16(%r12)
	je	.L170
	movq	%r10, 120(%rsp)
	movl	%r11d, 40(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, %rsi
	movq	120(%rsp), %r9
	movq	128(%rsp), %rcx
	movq	%rbp, %r8
	movq	112(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movl	40(%rsp), %r11d
	movq	120(%rsp), %r10
	je	.L415
	cmpl	$5, %eax
	je	.L165
	movq	144(%rsp), %r13
	movq	152(%rsp), %rdx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L412:
	movq	16(%rsp), %rcx
	movl	%r11d, %r15d
	andl	$4, %r15d
	movq	%rax, (%rcx)
	jmp	.L28
.L52:
	cmpw	$-9217, %r9w
	ja	.L368
	leaq	4(%rdi), %r8
	cmpq	%r8, %r13
	jb	.L56
	movzwl	154(%rsp), %ecx
	rolw	$8, %cx
	leal	9216(%rcx), %eax
	cmpw	$1023, %ax
	ja	.L368
	movzwl	%r9w, %eax
	movzwl	%cx, %ecx
	subl	$55232, %eax
	sall	$10, %eax
	leal	-56320(%rax,%rcx), %eax
	movl	%eax, (%r14)
	jmp	.L53
.L56:
	leaq	4(%rdi), %r8
	cmpq	%r8, %r13
	je	.L416
	movq	%rax, %r8
	andl	$-8, %ecx
	subq	%rsi, %r8
	movq	16(%rsp), %rsi
	addq	%r8, %rdx
	movq	%rdx, (%rsi)
	movslq	%ecx, %rdx
	cmpq	%rdx, %rax
	jle	.L417
	cmpq	$4, %rax
	ja	.L418
	orl	%eax, %ecx
	testq	%rax, %rax
	movl	%ecx, (%rbx)
	je	.L71
	xorl	%edx, %edx
.L65:
	movzbl	(%rdi,%rdx), %ecx
	movb	%cl, 4(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L65
	jmp	.L71
.L58:
	cmpw	$-9217, %r9w
	ja	.L368
	leaq	4(%rdi), %r8
	cmpq	%r8, %r13
	jb	.L56
	movzwl	154(%rsp), %ecx
	leal	9216(%rcx), %eax
	cmpw	$1023, %ax
	ja	.L368
	movzwl	%r9w, %eax
	subl	$55232, %eax
	sall	$10, %eax
	leal	-56320(%rax,%rcx), %eax
	movl	%eax, (%r14)
	jmp	.L53
.L368:
	cmpq	$0, 88(%rsp)
	movl	$6, %r8d
	je	.L19
	andb	$2, %r11b
	je	.L19
	movq	88(%rsp), %rax
	movq	%r14, 8(%rsp)
	leaq	2(%rdi), %r8
	addq	$1, (%rax)
	jmp	.L53
.L392:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L366:
	cmpq	$0, 88(%rsp)
	je	.L77
	testb	$8, %r11b
	jne	.L419
	andb	$2, %r11b
	je	.L77
.L94:
	movq	88(%rsp), %rax
	addq	$4, 144(%rsp)
	addq	$1, (%rax)
.L85:
	movq	144(%rsp), %rax
	cmpq	8(%rsp), %rax
	jne	.L78
	jmp	.L77
.L165:
	movq	136(%rsp), %r10
	cmpq	%r10, 152(%rsp)
	movq	144(%rsp), %rax
	movq	16(%rsp), %rcx
	movq	%rax, (%rcx)
	je	.L166
	jmp	.L141
.L415:
	movq	144(%rsp), %r13
	movq	152(%rsp), %rdx
	jmp	.L170
.L419:
	movl	%r11d, 104(%rsp)
	movq	%r10, 80(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	pushq	96(%rsp)
	movq	56(%rsp), %r8
	movq	112(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	movl	%eax, %r8d
	popq	%rcx
	movq	80(%rsp), %r10
	movl	104(%rsp), %r11d
	je	.L420
	movq	144(%rsp), %rax
	cmpq	8(%rsp), %rax
	jne	.L78
	cmpl	$7, %r8d
	jne	.L98
	movq	8(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 40(%rsp)
	je	.L421
	movl	(%rbx), %eax
	movq	%r13, %rcx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	16(%rsp), %rcx
	addq	%rdx, (%rcx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r13
	jle	.L422
	cmpq	$4, %r13
	ja	.L423
	orl	%r13d, %eax
	testq	%r13, %r13
	movl	%eax, (%rbx)
	je	.L71
	movq	8(%rsp), %rcx
	xorl	%eax, %eax
.L102:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L102
	jmp	.L71
.L394:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L66:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L386:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L46:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L423:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L422:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L421:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L98:
	testl	%r8d, %r8d
	jne	.L19
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r13
	jmp	.L39
.L420:
	andb	$2, %r11b
	je	.L85
	jmp	.L94
.L413:
	movq	88(%rsp), %rax
	movq	%rcx, %r13
	movl	$6, %ebx
	addq	$1, (%rax)
	jmp	.L148
.L69:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L176:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L400:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L43:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L418:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L417:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L416:
	leaq	__PRETTY_FUNCTION__.9121(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9038, @object
	.size	__PRETTY_FUNCTION__.9038, 21
__PRETTY_FUNCTION__.9038:
	.string	"to_utf16_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9121, @object
	.size	__PRETTY_FUNCTION__.9121, 23
__PRETTY_FUNCTION__.9121:
	.string	"from_utf16_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9192, @object
	.size	__PRETTY_FUNCTION__.9192, 6
__PRETTY_FUNCTION__.9192:
	.string	"gconv"
