	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leal	112(%rsi), %eax
	cmpb	$15, %al
	jbe	.L5
	cmpb	$-115, %sil
	movl	$-1, %eax
	ja	.L1
.L5:
	movzbl	%sil, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EUC-JP//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$12884901889, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L12
	movabsq	$17179869188, %rdi
	movabsq	$12884901889, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"./jis0212.h"
.LC11:
	.string	"cp[1] != '\\0'"
.LC12:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC14:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	addq	$48, %rsi
	movq	%rcx, %rbp
	subq	$216, %rsp
	movl	16(%r12), %r10d
	movq	%rdi, 40(%rsp)
	addq	$104, %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 24(%rsp)
	movq	%r9, 48(%rsp)
	testb	$1, %r10b
	movl	272(%rsp), %ebx
	movq	%rdi, 56(%rsp)
	movq	%rsi, 64(%rsp)
	movq	$0, 16(%rsp)
	jne	.L14
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 16(%rsp)
	je	.L14
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L14:
	testl	%ebx, %ebx
	jne	.L423
	movq	8(%rsp), %rax
	movq	24(%rsp), %rcx
	leaq	144(%rsp), %rdx
	movl	280(%rsp), %r9d
	movq	8(%r12), %r11
	testq	%rcx, %rcx
	movq	(%rax), %r13
	movq	%rcx, %rax
	cmove	%r12, %rax
	cmpq	$0, 48(%rsp)
	movq	(%rax), %r14
	movl	$0, %eax
	movq	$0, 144(%rsp)
	cmovne	%rdx, %rax
	testl	%r9d, %r9d
	movq	%rax, 80(%rsp)
	jne	.L424
.L21:
	leaq	184(%rsp), %rax
	movq	%r14, %r15
	movq	%r13, %r14
	movq	%r11, %r13
	movq	%rax, 88(%rsp)
	leaq	176(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 72(%rsp)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L425
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r14, 176(%rsp)
	movq	%r15, 184(%rsp)
	movq	%r15, %rbx
	movq	%r14, %rax
	movl	$4, %r11d
	andl	$2, %r10d
.L118:
	cmpq	%rax, %rbp
	je	.L119
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L246
	cmpq	%rbx, %r13
	jbe	.L248
	movl	(%rax), %edx
	leaq	1(%rbx), %rdi
	leal	-144(%rdx), %esi
	cmpl	$15, %esi
	jbe	.L284
	cmpl	$141, %edx
	jbe	.L284
	cmpl	$165, %edx
	je	.L426
	cmpl	$8254, %edx
	je	.L427
	cmpq	%rdi, %r13
	jbe	.L248
	leal	-65377(%rdx), %esi
	cmpl	$62, %esi
	jbe	.L125
	leal	-162(%rdx), %esi
	cmpl	$85, %esi
	ja	.L428
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L135
.L136:
	cmpl	$65534, %edx
	jbe	.L134
.L130:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L429
	cmpq	$0, 80(%rsp)
	je	.L252
	testb	$8, 16(%r12)
	jne	.L430
.L143:
	testl	%r10d, %r10d
	jne	.L431
.L252:
	movl	$6, %r11d
	.p2align 4,,10
	.p2align 3
.L119:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L432
.L146:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L433
	cmpq	%rbx, %r15
	jnb	.L253
	movq	16(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 32(%rsp)
	movq	%rax, 152(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %edi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	64(%rsp), %r9
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	movl	%eax, %r10d
	popq	%r11
	cmpl	$4, %r10d
	popq	%rax
	movl	32(%rsp), %r11d
	je	.L150
	movq	152(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L434
.L149:
	testl	%r10d, %r10d
	jne	.L276
.L205:
	movq	8(%rsp), %rax
	movl	16(%r12), %r10d
	movq	(%r12), %r15
	movq	(%rax), %r14
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L98
.L425:
	cmpq	%r14, %rbp
	je	.L231
	leaq	4(%r15), %rcx
	movq	%r14, %rdx
	movq	%r15, %rbx
	cmpq	%rcx, %r13
	jb	.L233
	andl	$2, %r10d
	movl	$4, %r11d
	movl	%r10d, 32(%rsp)
.L100:
	movzbl	(%rdx), %eax
	leal	-144(%rax), %edi
	movl	%eax, %esi
	cmpl	$15, %edi
	jbe	.L282
	cmpl	$141, %eax
	jbe	.L282
	cmpl	$255, %eax
	je	.L435
	leaq	1(%rdx), %r9
	cmpq	%r9, %rbp
	jbe	.L243
	movzbl	1(%rdx), %edi
	cmpl	$160, %edi
	movl	%edi, %r10d
	jle	.L112
	cmpl	$142, %eax
	je	.L436
	cmpl	$143, %eax
	je	.L437
	cmpb	$-96, %al
	ja	.L438
.L112:
	cmpq	$0, 80(%rsp)
	je	.L245
	movl	32(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L439
.L245:
	movl	$6, %r11d
.L99:
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
.L440:
	cmpq	$0, 24(%rsp)
	je	.L146
.L432:
	movq	24(%rsp), %rax
	movl	%r11d, %r15d
	movq	%rbx, (%rax)
.L13:
	addq	$216, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%rdi, 184(%rsp)
	movb	%dl, (%rbx)
	movq	184(%rsp), %rbx
.L122:
	movq	176(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 176(%rsp)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rdi, 184(%rsp)
	movb	$92, (%rbx)
	movq	184(%rsp), %rbx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L125:
	addl	$64, %edx
	movb	%dl, 1(%rbx)
	movq	184(%rsp), %rdx
	movb	$-114, (%rdx)
	movq	184(%rsp), %rax
	leaq	2(%rax), %rbx
	movq	%rbx, 184(%rsp)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$5, %r11d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L246:
	movl	$7, %r11d
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$1, %rdx
.L103:
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L105:
	cmpq	%rdx, %rbp
	je	.L99
.L116:
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r13
	jnb	.L100
.L233:
	movq	8(%rsp), %rax
	movl	$5, %r11d
	movq	%rdx, (%rax)
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%rdi, 184(%rsp)
	movb	$126, (%rbx)
	movq	184(%rsp), %rbx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L149
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L435:
	cmpq	$0, 80(%rsp)
	je	.L245
	movl	32(%rsp), %esi
	testl	%esi, %esi
	je	.L245
	movq	80(%rsp), %rax
	addq	$1, %rdx
	movl	$6, %r11d
	addq	$1, (%rax)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L424:
	movq	32(%r12), %rbx
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L21
	testq	%rcx, %rcx
	jne	.L441
	movq	40(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L442
	cmpl	$4, %edx
	movq	%r13, 160(%rsp)
	movq	%r14, 168(%rsp)
	ja	.L48
	leaq	140(%rsp), %rsi
	movslq	%edx, %rcx
	xorl	%eax, %eax
	movq	%rcx, 32(%rsp)
	movq	%rsi, 72(%rsp)
.L49:
	movzbl	4(%rbx,%rax), %edx
	movb	%dl, (%rsi,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L49
	movq	%r13, %rax
	subq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L443
	cmpq	%r11, %r14
	jnb	.L64
	movq	32(%rsp), %rdx
	leaq	1(%r13), %rax
	leaq	139(%rsp), %rsi
.L57:
	movq	%rax, 160(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rdx
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	$3, %rdx
	movb	%cl, (%rsi,%rdx)
	ja	.L280
	cmpq	%rdi, %rbp
	ja	.L57
.L280:
	movq	72(%rsp), %rax
	movq	%rdx, 32(%rsp)
	leaq	1(%r14), %rdx
	movq	%rax, 160(%rsp)
	movl	140(%rsp), %eax
	leal	-144(%rax), %ecx
	cmpl	$15, %ecx
	jbe	.L281
	cmpl	$141, %eax
	jbe	.L281
	cmpl	$165, %eax
	je	.L444
	cmpl	$8254, %eax
	je	.L445
	cmpq	%rdx, %r11
	jbe	.L64
	leal	-65377(%rax), %edx
	cmpl	$62, %edx
	jbe	.L65
	leal	-162(%rax), %edx
	cmpl	$85, %edx
	ja	.L446
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rdx,2), %rdx
	movzbl	(%rdx), %ecx
.L68:
	testb	%cl, %cl
	je	.L76
.L75:
	movb	%cl, (%r14)
	movzbl	1(%rdx), %eax
	movb	%al, 1(%r14)
.L406:
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	addb	$-128, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	addb	$-128, (%rax)
.L61:
	movq	160(%rsp), %rax
	addq	$4, %rax
	cmpq	72(%rsp), %rax
	movq	%rax, 160(%rsp)
	je	.L407
.L82:
	movl	(%rbx), %edx
	subq	72(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L447
	movq	8(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	168(%rsp), %r14
	movl	16(%r12), %r10d
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r13
	movl	%edx, (%rbx)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L428:
	leal	-913(%rdx), %esi
	cmpl	$192, %esi
	ja	.L129
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	je	.L136
.L135:
	movb	%sil, (%rbx)
	movzbl	1(%rdi), %eax
	movb	%al, 1(%rbx)
.L408:
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	addb	$-128, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	addb	$-128, (%rax)
	movq	184(%rsp), %rbx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L437:
	testb	%dil, %dil
	jns	.L112
	leal	-128(%rdi), %eax
	cmpl	$109, %eax
	jg	.L112
	cmpl	$33, %eax
	je	.L112
	movq	%rbp, %rax
	subq	%r9, %rax
	cmpq	$1, %rax
	jbe	.L243
	movzbl	2(%rdx), %eax
	leal	95(%rax), %esi
	cmpb	$93, %sil
	ja	.L112
	leal	-161(%rdi), %esi
	imull	$94, %esi, %esi
	leal	-161(%rsi,%rax), %esi
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %edi
	cmpl	%esi, %edi
	jge	.L113
.L114:
	addq	$6, %rax
	movzwl	2(%rax), %edi
	cmpl	%edi, %esi
	jg	.L114
.L113:
	movzwl	(%rax), %edi
	cmpl	%edi, %esi
	jl	.L112
	movzwl	4(%rax), %eax
	addl	%esi, %eax
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rsi
	subl	%edi, %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leaq	3(%rdx), %rsi
	testl	%eax, %eax
	jne	.L115
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L433:
	movq	48(%rsp), %rsi
	movq	%rbx, (%r12)
	movl	%r11d, %r15d
	movq	144(%rsp), %rax
	addq	%rax, (%rsi)
.L148:
	movl	280(%rsp), %eax
	testl	%eax, %eax
	je	.L13
	cmpl	$7, %r15d
	jne	.L13
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L207
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L209
.L208:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L208
.L209:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L436:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %eax
	testl	%eax, %eax
	sete	%dil
	testb	%r10b, %r10b
	setne	%sil
	testb	%sil, %dil
	jne	.L112
	cmpl	$65533, %eax
	je	.L112
	addq	$2, %rdx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%r11d, %r10d
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L434:
	movq	8(%rsp), %rax
	movl	16(%r12), %ecx
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L448
	leaq	192(%rsp), %rdi
	andl	$2, %ecx
	leaq	200(%rsp), %rbx
	movq	%r14, 192(%rsp)
	movq	%r15, 200(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
	movl	%ecx, 32(%rsp)
	movq	%rdi, 104(%rsp)
.L176:
	cmpq	%r14, %rbp
	je	.L449
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rbp
	jb	.L267
	cmpq	%rdx, %r11
	jbe	.L273
	movl	(%r14), %ecx
	leaq	1(%rdx), %r8
	leal	-144(%rcx), %edi
	cmpl	$15, %edi
	jbe	.L287
	cmpl	$141, %ecx
	jbe	.L287
	cmpl	$165, %ecx
	je	.L450
	cmpl	$8254, %ecx
	je	.L451
	cmpq	%r8, %r11
	jbe	.L273
	leal	-65377(%rcx), %edi
	cmpl	$62, %edi
	jbe	.L183
	leal	-162(%rcx), %edi
	cmpl	$85, %edi
	ja	.L452
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	movzbl	(%rdi), %r8d
.L186:
	testb	%r8b, %r8b
	jne	.L193
	cmpl	$65534, %ecx
	jbe	.L409
.L188:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L453
	cmpq	$0, 80(%rsp)
	je	.L274
	testb	$8, 16(%r12)
	jne	.L454
.L201:
	movl	32(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L455
.L274:
	movl	$6, %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L129:
	cmpl	$65534, %edx
	ja	.L130
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %edx
	jbe	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$6, %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %edx
	ja	.L132
.L131:
	movzwl	(%rsi), %edi
	cmpl	%edi, %edx
	jb	.L134
	movzwl	4(%rsi), %esi
	addl	%edx, %esi
	subl	%edi, %esi
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rdi
	movzbl	(%rdi), %esi
	testb	%sil, %sil
	jne	.L135
.L134:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rsi
	movq	%r13, %r8
	subq	%rbx, %r8
	subq	$1, %r8
	movzwl	2(%rsi), %edi
	cmpl	%edi, %edx
	jbe	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$6, %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %edx
	ja	.L138
.L137:
	movzwl	(%rsi), %edi
	cmpl	%edi, %edx
	jb	.L130
	movzwl	4(%rsi), %esi
	addl	%edx, %esi
	subl	%edi, %esi
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	je	.L130
	movb	%dil, 1(%rbx)
	movzbl	1(%rsi), %eax
	testb	%al, %al
	je	.L139
	cmpq	$1, %r8
	jbe	.L456
	movb	%al, 2(%rbx)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$-113, (%rax)
	jmp	.L408
.L442:
	cmpl	$4, %edx
	ja	.L24
	movslq	%edx, %rdx
	leaq	200(%rsp), %rcx
	xorl	%esi, %esi
	movq	%rdx, %r9
.L25:
	movzbl	4(%rbx,%rsi), %edi
	movb	%dil, (%rcx,%rsi)
	addq	$1, %rsi
	cmpq	%rdx, %rsi
	jne	.L25
	leaq	4(%r14), %r8
	cmpq	%r8, %r11
	jb	.L64
	movq	%r13, %rsi
.L26:
	addq	$1, %rsi
	movzbl	-1(%rsi), %r15d
	leaq	1(%r9), %rdi
	cmpq	$2, %rdi
	movb	%r15b, 200(%rsp,%r9)
	movl	$2, %r9d
	ja	.L278
	cmpq	%rsi, %rbp
	ja	.L26
.L278:
	movzbl	200(%rsp), %esi
	leal	-144(%rsi), %r9d
	movb	%sil, 32(%rsp)
	movl	%esi, 72(%rsp)
	cmpl	$15, %r9d
	jbe	.L217
	cmpl	$141, %esi
	jbe	.L217
	cmpl	$255, %esi
	je	.L279
	leaq	(%rcx,%rdi), %r9
	movq	%r9, %r15
	movq	%r9, 88(%rsp)
	leaq	1(%rcx), %r9
	cmpq	%r9, %r15
	jbe	.L33
	movzbl	201(%rsp), %r9d
	cmpl	$160, %r9d
	movl	%r9d, %r15d
	jle	.L279
	cmpl	$142, %esi
	je	.L457
	cmpl	$143, 72(%rsp)
	je	.L458
	cmpb	$-96, 32(%rsp)
	ja	.L459
.L38:
	cmpq	$0, 80(%rsp)
	movl	$6, %r15d
	je	.L13
	andb	$2, %r10b
	je	.L13
	movq	80(%rsp), %rdi
	movl	$1, %ecx
	addq	$1, (%rdi)
.L32:
	cmpq	%rcx, %rdx
	jge	.L460
	movq	8(%rsp), %rsi
	subq	%rdx, %rcx
	andl	$-8, %eax
	addq	%rcx, %r13
	movl	16(%r12), %r10d
	movq	%r13, (%rsi)
	movl	%eax, (%rbx)
	jmp	.L21
.L439:
	movq	80(%rsp), %rax
	movq	%r9, %rdx
	movl	$6, %r11d
	addq	$1, (%rax)
	jmp	.L116
.L448:
	cmpq	%r14, %rbp
	je	.L461
	leaq	4(%r15), %rbx
	andl	$2, %ecx
	movq	%r15, %rdx
	movl	$4, 104(%rsp)
	movl	%ecx, 32(%rsp)
	cmpq	%rbx, %r11
	jb	.L462
.L156:
	movzbl	(%r14), %eax
	leal	-144(%rax), %edi
	movl	%eax, %ecx
	cmpl	$15, %edi
	jbe	.L285
	cmpl	$141, %eax
	jbe	.L285
	cmpl	$255, %eax
	je	.L463
	leaq	1(%r14), %r9
	cmpq	%r9, %rbp
	jbe	.L264
	movzbl	1(%r14), %edi
	cmpl	$160, %edi
	movl	%edi, %r8d
	jle	.L170
	cmpl	$142, %eax
	je	.L464
	cmpl	$143, %eax
	je	.L465
	cmpb	$-96, %al
	ja	.L466
.L170:
	cmpq	$0, 80(%rsp)
	je	.L266
	movl	32(%rsp), %r8d
	testl	%r8d, %r8d
	jne	.L467
.L266:
	movl	$6, %eax
.L158:
	movq	8(%rsp), %rdi
	movq	%r14, (%rdi)
	jmp	.L175
.L80:
	movq	160(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L82
.L64:
	movl	$5, %r15d
	jmp	.L13
.L183:
	addl	$64, %ecx
	movb	%cl, 1(%rdx)
	movq	200(%rsp), %rdx
	movb	$-114, (%rdx)
	movq	200(%rsp), %rsi
	leaq	2(%rsi), %rdx
	movq	%rdx, 200(%rsp)
	.p2align 4,,10
	.p2align 3
.L180:
	movq	192(%rsp), %rsi
	leaq	4(%rsi), %r14
	movq	%r14, 192(%rsp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r8, 200(%rsp)
	movb	%cl, (%rdx)
	movq	200(%rsp), %rdx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rsp), %rax
	movl	$7, %r11d
	movq	%rdx, (%rax)
	jmp	.L440
.L423:
	cmpq	$0, 24(%rsp)
	jne	.L468
	movq	32(%r12), %rax
	xorl	%r15d, %r15d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L13
	movq	16(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	280(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	popq	%r10
	movl	%eax, %r15d
	popq	%r11
	jmp	.L13
.L285:
	addq	$1, %r14
.L161:
	movl	%eax, (%rdx)
	movq	%rbx, %rdx
.L163:
	cmpq	%r14, %rbp
	je	.L469
.L174:
	leaq	4(%rdx), %rbx
	cmpq	%rbx, %r11
	jnb	.L156
	movl	$5, %eax
	jmp	.L158
.L450:
	movq	%r8, 200(%rsp)
	movb	$92, (%rdx)
	movq	200(%rsp), %rdx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%rbp, %rax
	subq	%rdx, %rax
	cmpq	$1, %rax
	jbe	.L243
	addl	$-128, %r10d
	cmpb	$126, %r10b
	ja	.L112
	leal	-161(%rsi), %eax
	imull	$94, %eax, %eax
	leal	-161(%rdi,%rax), %eax
	cmpl	$7807, %eax
	jg	.L112
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rsi
	cltq
	movzwl	(%rsi,%rax,2), %eax
	leaq	2(%rdx), %rsi
	testw	%ax, %ax
	je	.L112
.L115:
	cmpl	$65533, %eax
	je	.L112
	movq	%rsi, %rdx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L276:
	movl	%r10d, %r15d
	jmp	.L148
.L454:
	movq	%r11, 120(%rsp)
	movl	%r10d, 116(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	120(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	%rbx, %r9
	movq	%rbp, %r8
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	popq	%rdi
	movq	192(%rsp), %r14
	movq	200(%rsp), %rdx
	movl	116(%rsp), %r10d
	movq	120(%rsp), %r11
	je	.L201
	cmpl	$5, %eax
	jne	.L176
.L273:
	movl	$5, %eax
.L177:
	movq	8(%rsp), %rsi
	movq	152(%rsp), %r11
	movq	%r14, (%rsi)
.L175:
	cmpq	%r11, %rdx
	jne	.L155
	cmpq	$5, %rax
	jne	.L154
.L198:
	cmpq	%rdx, %r15
	jne	.L149
.L157:
	subl	$1, 20(%r12)
	jmp	.L149
.L267:
	movl	$7, %eax
	jmp	.L177
.L451:
	movq	%r8, 200(%rsp)
	movb	$126, (%rdx)
	movq	200(%rsp), %rdx
	jmp	.L180
.L281:
	movq	%rdx, 168(%rsp)
	movb	%al, (%r14)
	jmp	.L61
.L464:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %eax
	testl	%eax, %eax
	sete	%cl
	testb	%r8b, %r8b
	setne	%sil
	testb	%sil, %cl
	jne	.L170
	cmpl	$65533, %eax
	je	.L170
	addq	$2, %r14
	jmp	.L161
.L429:
	movq	%rcx, 176(%rsp)
	movq	%rcx, %rax
	jmp	.L118
.L463:
	cmpq	$0, 80(%rsp)
	je	.L266
	movl	32(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L266
	movq	80(%rsp), %rax
	addq	$1, %r14
	movl	$6, 104(%rsp)
	addq	$1, (%rax)
	jmp	.L163
.L231:
	movq	%rbp, %rdx
	movq	%r15, %rbx
	movl	$4, %r11d
	jmp	.L99
.L217:
	movl	$1, %ecx
.L29:
	movl	%esi, (%r14)
	movl	(%rbx), %eax
	movq	%r8, %r14
	movl	%eax, %edx
	andl	$7, %edx
	jmp	.L32
.L443:
	movq	8(%rsp), %rax
	movq	32(%rsp), %rdi
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rdi, %rax
	cmpq	$4, %rax
	ja	.L51
	addq	$1, %r13
	cmpq	%rdi, %rax
	movq	32(%rsp), %rdx
	jbe	.L53
.L54:
	movq	%r13, 160(%rsp)
	movzbl	-1(%r13), %ecx
	addq	$1, %r13
	movb	%cl, 4(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L54
.L53:
	movl	$7, %r15d
	jmp	.L13
.L449:
	cltq
	movq	%rbp, %r14
	jmp	.L177
.L444:
	movq	%rdx, 168(%rsp)
	movb	$92, (%r14)
	jmp	.L61
.L452:
	leal	-913(%rcx), %edi
	cmpl	$192, %edi
	ja	.L187
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	movzbl	(%rdi), %r8d
	jmp	.L186
.L65:
	addl	$64, %eax
	movb	%al, 1(%r14)
	movq	168(%rsp), %rax
	movb	$-114, (%rax)
	addq	$2, 168(%rsp)
	jmp	.L61
.L465:
	testb	%dil, %dil
	jns	.L170
	leal	-128(%rdi), %eax
	cmpl	$33, %eax
	je	.L170
	cmpl	$109, %eax
	jg	.L170
	movq	%rbp, %rax
	subq	%r9, %rax
	cmpq	$1, %rax
	jbe	.L264
	movzbl	2(%r14), %eax
	testb	%al, %al
	jns	.L170
	movzbl	%al, %ecx
	addl	$95, %eax
	cmpb	$93, %al
	ja	.L170
	leal	-161(%rdi), %eax
	imull	$94, %eax, %eax
	leal	-161(%rcx,%rax), %esi
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rax
.L171:
	movzwl	2(%rax), %ecx
	cmpl	%ecx, %esi
	jle	.L470
	addq	$6, %rax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L193:
	movb	%r8b, (%rdx)
	movzbl	1(%rdi), %ecx
	movb	%cl, 1(%rdx)
.L410:
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 200(%rsp)
	addb	$-128, (%rdx)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 200(%rsp)
	addb	$-128, (%rdx)
	movq	200(%rsp), %rdx
	jmp	.L180
.L456:
	movq	176(%rsp), %rax
	movq	184(%rsp), %rbx
	movl	$5, %r11d
	jmp	.L119
.L445:
	movq	%rdx, 168(%rsp)
	movb	$126, (%r14)
	jmp	.L61
.L264:
	movl	$7, %eax
	jmp	.L158
.L430:
	movl	%r10d, 32(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	104(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r11d
	cmpl	$6, %r11d
	popq	%rax
	popq	%rdx
	movq	176(%rsp), %rax
	movq	184(%rsp), %rbx
	movl	32(%rsp), %r10d
	je	.L143
	cmpl	$5, %r11d
	jne	.L118
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L187:
	cmpl	$65534, %ecx
	ja	.L188
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rdi
	movzwl	2(%rdi), %r8d
	cmpl	%r8d, %ecx
	jbe	.L189
.L190:
	addq	$6, %rdi
	movzwl	2(%rdi), %r8d
	cmpl	%r8d, %ecx
	ja	.L190
.L189:
	movzwl	(%rdi), %r8d
	cmpl	%r8d, %ecx
	jb	.L409
	movzwl	4(%rdi), %edi
	addl	%ecx, %edi
	subl	%r8d, %edi
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	movzbl	(%rdi), %r8d
	testb	%r8b, %r8b
	jne	.L193
.L409:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rdi
	movq	%r11, %r8
	subq	%rdx, %r8
	subq	$1, %r8
	movzwl	2(%rdi), %r9d
	cmpl	%r9d, %ecx
	jbe	.L195
.L196:
	addq	$6, %rdi
	movzwl	2(%rdi), %r9d
	cmpl	%r9d, %ecx
	ja	.L196
.L195:
	movzwl	(%rdi), %r9d
	cmpl	%r9d, %ecx
	jb	.L188
	movzwl	4(%rdi), %edi
	addl	%ecx, %edi
	subl	%r9d, %edi
	movq	__jisx0212_from_ucs@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	movzbl	(%rdi), %r9d
	testb	%r9b, %r9b
	je	.L188
	movb	%r9b, 1(%rdx)
	movzbl	1(%rdi), %ecx
	testb	%cl, %cl
	je	.L139
	cmpq	$1, %r8
	jbe	.L471
	movb	%cl, 2(%rdx)
	movq	200(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 200(%rsp)
	movb	$-113, (%rdx)
	jmp	.L410
.L462:
	cmpq	%r15, %r11
	je	.L157
.L155:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L457:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rcx
	movslq	%r9d, %r9
	movl	(%rcx,%r9,4), %esi
	testl	%esi, %esi
	sete	%dil
	testb	%r15b, %r15b
	setne	%cl
	testb	%cl, %dil
	jne	.L279
	cmpl	$65533, %esi
	movl	$2, %ecx
	jne	.L29
.L279:
	cmpq	$0, 80(%rsp)
	movl	$6, %r15d
	je	.L13
	andl	$2, %r10d
	je	.L13
	movq	80(%rsp), %rsi
	movl	$1, %ecx
	addq	$1, (%rsi)
	jmp	.L32
.L481:
	movq	72(%rsp), %rax
	addq	32(%rsp), %rax
	leaq	160(%rsp), %rcx
	movl	%r10d, 104(%rsp)
	movq	%r11, 96(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, 96(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	184(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%rdi
	cmpl	$6, %eax
	movl	%eax, %r15d
	popq	%r8
	movq	96(%rsp), %r11
	movl	104(%rsp), %r10d
	je	.L472
	movq	160(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L82
	cmpl	$7, %r15d
	je	.L473
	testl	%r15d, %r15d
	jne	.L13
.L407:
	movq	8(%rsp), %rax
	movl	16(%r12), %r10d
	movq	(%rax), %r13
	jmp	.L21
.L431:
	movq	80(%rsp), %rdi
	addq	$4, %rax
	movl	$6, %r11d
	movq	%rax, 176(%rsp)
	addq	$1, (%rdi)
	jmp	.L118
.L466:
	movq	%rbp, %rax
	subq	%r14, %rax
	cmpq	$1, %rax
	jbe	.L264
	addl	$-128, %r8d
	cmpb	$126, %r8b
	ja	.L170
	leal	-161(%rcx), %eax
	imull	$94, %eax, %eax
	leal	-161(%rdi,%rax), %eax
	cmpl	$7807, %eax
	jg	.L170
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rsi
	cltq
	leaq	2(%r14), %rcx
	movzwl	(%rsi,%rax,2), %eax
	testw	%ax, %ax
	je	.L170
.L173:
	cmpl	$65533, %eax
	je	.L170
	movq	%rcx, %r14
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L467:
	movq	80(%rsp), %rax
	movq	%r9, %r14
	movl	$6, 104(%rsp)
	addq	$1, (%rax)
	jmp	.L174
.L469:
	movslq	104(%rsp), %rax
	movq	%rbp, %r14
	jmp	.L158
.L458:
	testb	%r9b, %r9b
	jns	.L38
	leal	-128(%r9), %esi
	cmpl	$109, %esi
	jg	.L38
	cmpl	$33, %esi
	je	.L38
	movq	88(%rsp), %r15
	leaq	1(%rcx), %rsi
	subq	%rsi, %r15
	cmpq	$1, %r15
	jbe	.L33
	movzbl	202(%rsp), %esi
	testb	%sil, %sil
	jns	.L38
	movzbl	%sil, %edi
	addl	$95, %esi
	cmpb	$93, %sil
	ja	.L38
	leal	-161(%r9), %esi
	imull	$94, %esi, %esi
	leal	-161(%rdi,%rsi), %r9d
	movq	__jisx0212_to_ucs_idx@GOTPCREL(%rip), %rsi
.L39:
	movzwl	2(%rsi), %edi
	cmpl	%edi, %r9d
	jle	.L474
	addq	$6, %rsi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L459:
	cmpq	$1, %rdi
	jbe	.L33
	addl	$-128, %r15d
	cmpb	$126, %r15b
	ja	.L38
	movl	72(%rsp), %esi
	subl	$161, %esi
	imull	$94, %esi, %esi
	leal	-161(%r9,%rsi), %esi
	cmpl	$7807, %esi
	jg	.L38
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %esi
	leaq	2(%rcx), %rdi
	testw	%si, %si
	je	.L38
.L41:
	cmpl	$65533, %esi
	je	.L38
	subq	%rcx, %rdi
	movq	%rdi, %rcx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	3(%rcx), %rsi
	cmpq	%rsi, 88(%rsp)
	je	.L475
	movq	%rdi, %rsi
	andl	$-8, %eax
	subq	%rdx, %rsi
	movslq	%eax, %rdx
	addq	%rsi, %r13
	movq	8(%rsp), %rsi
	cmpq	%rdx, %rdi
	movq	%r13, (%rsi)
	jle	.L476
	cmpq	$4, %rdi
	ja	.L477
	orl	%edi, %eax
	movzbl	32(%rsp), %r15d
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L53
	movb	%r15b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L53
.L478:
	movzbl	(%rcx,%rax), %r15d
	movb	%r15b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdi
	jne	.L478
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r15, %r14
	cmpq	%r14, %r11
	jne	.L155
.L154:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r11, %rcx
	subq	%r14, %rcx
	subq	$1, %rcx
	cmpl	$65534, %eax
	ja	.L70
.L210:
	movq	__jisx0212_from_ucs_idx@GOTPCREL(%rip), %rdx
	movzwl	2(%rdx), %esi
	cmpl	%esi, %eax
	jbe	.L77
.L78:
	addq	$6, %rdx
	movzwl	2(%rdx), %esi
	cmpl	%esi, %eax
	ja	.L78
.L77:
	movzwl	(%rdx), %esi
	cmpl	%esi, %eax
	jb	.L70
	movzwl	4(%rdx), %edx
	addl	%eax, %edx
	subl	%esi, %edx
	addq	%rdx, %rdx
	addq	__jisx0212_from_ucs@GOTPCREL(%rip), %rdx
	movzbl	(%rdx), %esi
	testb	%sil, %sil
	je	.L70
	movb	%sil, 1(%r14)
	movzbl	1(%rdx), %eax
	testb	%al, %al
	je	.L139
	cmpq	$1, %rcx
	jbe	.L80
	movb	%al, 2(%r14)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$-113, (%rax)
	jmp	.L406
.L446:
	leal	-913(%rax), %edx
	cmpl	$192, %edx
	ja	.L69
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rcx
	leaq	(%rcx,%rdx,2), %rdx
	movzbl	(%rdx), %ecx
	jmp	.L68
.L69:
	cmpl	$65534, %eax
	jbe	.L479
.L70:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L480
	cmpq	$0, 80(%rsp)
	je	.L83
	testb	$8, %r10b
	jne	.L481
	andb	$2, %r10b
	jne	.L88
.L83:
	movl	$6, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L479:
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rdx
	movzwl	2(%rdx), %ecx
	cmpl	%ecx, %eax
	jbe	.L71
.L72:
	addq	$6, %rdx
	movzwl	2(%rdx), %ecx
	cmpl	%ecx, %eax
	ja	.L72
.L71:
	movzwl	(%rdx), %ecx
	cmpl	%ecx, %eax
	jb	.L74
	movzwl	4(%rdx), %edx
	addl	%eax, %edx
	subl	%ecx, %edx
	addq	%rdx, %rdx
	addq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L75
.L74:
	movq	%r11, %rcx
	subq	%r14, %rcx
	subq	$1, %rcx
	jmp	.L210
.L472:
	andb	$2, %r10b
	je	.L87
.L88:
	movq	80(%rsp), %rax
	addq	$4, 160(%rsp)
	addq	$1, (%rax)
.L87:
	movq	160(%rsp), %rax
	cmpq	72(%rsp), %rax
	jne	.L82
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L473:
	movq	72(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 88(%rsp)
	je	.L482
	movl	(%rbx), %eax
	movq	32(%rsp), %rdi
	movl	%eax, %edx
	movq	%rdi, %rsi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	8(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rdi
	jle	.L483
	cmpq	$4, 32(%rsp)
	ja	.L484
	movq	32(%rsp), %rdi
	orl	%edi, %eax
	testq	%rdi, %rdi
	movl	%eax, (%rbx)
	je	.L53
	movq	72(%rsp), %rcx
	xorl	%eax, %eax
.L97:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 32(%rsp)
	jne	.L97
	jmp	.L53
.L484:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L483:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L482:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L474:
	movzwl	(%rsi), %edi
	cmpl	%edi, %r9d
	jl	.L38
	movzwl	4(%rsi), %esi
	addl	%r9d, %esi
	subl	%edi, %esi
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %esi
	testl	%esi, %esi
	je	.L38
	leaq	3(%rcx), %rdi
	jmp	.L41
.L207:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L139:
	leaq	__PRETTY_FUNCTION__.8041(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$101, %edx
	call	__assert_fail@PLT
.L468:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L480:
	movq	72(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L82
.L453:
	movq	%rsi, 192(%rsp)
	movq	%rsi, %r14
	jmp	.L176
.L455:
	movq	80(%rsp), %rax
	addq	$4, %r14
	movq	%r14, 192(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L176
.L470:
	movzwl	(%rax), %ecx
	cmpl	%ecx, %esi
	jl	.L170
	movzwl	4(%rax), %eax
	addl	%esi, %eax
	subl	%ecx, %eax
	movq	__jisx0212_to_ucs@GOTPCREL(%rip), %rcx
	cltq
	movzwl	(%rcx,%rax,2), %eax
	leaq	3(%r14), %rcx
	testl	%eax, %eax
	jne	.L173
	jmp	.L170
.L460:
	leaq	__PRETTY_FUNCTION__.9113(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L447:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L441:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L471:
	movq	152(%rsp), %rdx
	cmpq	200(%rsp), %rdx
	movq	192(%rsp), %rax
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	je	.L198
	jmp	.L155
.L51:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L48:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L24:
	leaq	__PRETTY_FUNCTION__.9113(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L477:
	leaq	__PRETTY_FUNCTION__.9113(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L476:
	leaq	__PRETTY_FUNCTION__.9113(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L475:
	leaq	__PRETTY_FUNCTION__.9113(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.8041, @object
	.size	__PRETTY_FUNCTION__.8041, 17
__PRETTY_FUNCTION__.8041:
	.string	"ucs4_to_jisx0212"
	.align 16
	.type	__PRETTY_FUNCTION__.9199, @object
	.size	__PRETTY_FUNCTION__.9199, 17
__PRETTY_FUNCTION__.9199:
	.string	"to_euc_jp_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9113, @object
	.size	__PRETTY_FUNCTION__.9113, 19
__PRETTY_FUNCTION__.9113:
	.string	"from_euc_jp_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9274, @object
	.size	__PRETTY_FUNCTION__.9274, 6
__PRETTY_FUNCTION__.9274:
	.string	"gconv"
