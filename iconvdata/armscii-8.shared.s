	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	cmpb	$-96, %sil
	jbe	.L6
	leal	94(%rsi), %edx
	movl	$-1, %eax
	cmpb	$92, %dl
	ja	.L1
	movzbl	%sil, %esi
	leaq	map_from_armscii_8(%rip), %rax
	subl	$162, %esi
	movslq	%esi, %rsi
	movzwl	(%rax,%rsi,2), %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	%sil, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ARMSCII-8//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$12, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L8
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	32(%rax), %rsi
	movl	$12, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L11
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r14d
	movq	%rsi, 64(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 32(%rsp)
	testb	$1, %r14b
	movq	%r9, 16(%rsp)
	movl	208(%rsp), %ebx
	movq	%rsi, 56(%rsp)
	movq	$0, 24(%rsp)
	jne	.L13
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rcx
	movq	%rcx, 24(%rsp)
	je	.L13
	movq	%rcx, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L13:
	testl	%ebx, %ebx
	jne	.L218
	movq	8(%rsp), %rax
	movq	32(%rsp), %rdi
	leaq	112(%rsp), %rdx
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r15
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movl	216(%rsp), %edx
	movq	%rax, 72(%rsp)
	movq	40(%rsp), %rax
	testl	%edx, %edx
	movq	96(%rax), %rax
	setne	87(%rsp)
	movzbl	87(%rsp), %ecx
	testq	%rax, %rax
	je	.L130
	testb	%cl, %cl
	je	.L130
	movq	32(%r12), %r10
	movl	(%r10), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
	jne	.L219
.L130:
	xorl	%r10d, %r10d
.L20:
	leaq	136(%rsp), %rsi
	movl	%r14d, %ecx
	movq	%rsi, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L120:
	movq	16(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L56
	addq	(%rsi), %r10
.L56:
	testq	%rax, %rax
	je	.L220
	andl	$2, %ecx
	leaq	128(%rsp), %r11
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r15, %rbx
	movq	%r13, %rax
	movl	$4, %r14d
	movl	%ecx, 48(%rsp)
.L66:
	cmpq	%rax, %rbp
	je	.L67
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L138
	cmpq	%rbx, (%rsp)
	jbe	.L139
	movl	(%rax), %edx
	cmpl	$160, %edx
	jbe	.L215
	cmpl	$171, %edx
	je	.L221
	cmpl	$187, %edx
	je	.L222
	leal	-1329(%rdx), %esi
	cmpl	$89, %esi
	ja	.L72
	leaq	map_to_armscii_8(%rip), %rcx
	movzbl	(%rcx,%rsi), %edx
	testb	%dl, %dl
	jne	.L215
.L73:
	cmpq	$0, 72(%rsp)
	je	.L141
	testb	$8, 16(%r12)
	jne	.L223
.L77:
	movl	48(%rsp), %r8d
	testl	%r8d, %r8d
	jne	.L224
.L141:
	movl	$6, %r14d
	.p2align 4,,10
	.p2align 3
.L67:
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L225
.L80:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L226
	cmpq	%rbx, %r15
	movq	%r10, 48(%rsp)
	jnb	.L142
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	32(%rsp), %r9
	movq	72(%rsp), %rsi
	movq	80(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%rsi
	popq	%rdi
	je	.L84
	movq	120(%rsp), %r14
	movq	48(%rsp), %r10
	cmpq	%rbx, %r14
	jne	.L227
.L83:
	testl	%r11d, %r11d
	jne	.L153
.L119:
	movq	40(%rsp), %rax
	movq	8(%rsp), %rdi
	movq	112(%rsp), %r10
	movl	16(%r12), %ecx
	movq	(%r12), %r15
	movq	96(%rax), %rax
	movq	(%rdi), %r13
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L215:
	movb	%dl, (%rbx)
.L69:
	movq	136(%rsp), %rax
	leaq	1(%rax), %rbx
	movq	128(%rsp), %rax
	movq	%rbx, 136(%rsp)
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L221:
	movb	$-89, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L220:
	cmpq	%r13, %rbp
	je	.L133
	movq	(%rsp), %r8
	leaq	4(%r15), %rsi
	cmpq	%r8, %rsi
	ja	.L134
	movq	%r13, %rax
	movq	%r15, %rbx
	movl	$4, %r14d
	andl	$2, %ecx
.L59:
	movzbl	(%rax), %edx
	movq	%rax, %rdi
	cmpb	$-96, %dl
	jbe	.L214
	leal	94(%rdx), %r9d
	cmpb	$92, %r9b
	ja	.L62
	subl	$162, %edx
	leaq	map_from_armscii_8(%rip), %rdi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edx
.L214:
	addq	$1, %rax
	movl	%edx, (%rbx)
	movq	%rsi, %rbx
	movq	%rax, %rdi
.L63:
	cmpq	%rax, %rbp
	je	.L58
	leaq	4(%rbx), %rsi
	cmpq	%rsi, %r8
	jnb	.L59
	movl	$5, %r14d
.L58:
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
.L231:
	cmpq	$0, 32(%rsp)
	je	.L80
.L225:
	movq	32(%rsp), %rax
	movl	%r14d, %r9d
	movq	%rbx, (%rax)
.L12:
	addq	$152, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$7, %r14d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	$5, %r14d
	movl	%r14d, %r11d
	jne	.L83
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L222:
	movb	$-90, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$5, %r14d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$8212, %edx
	je	.L228
	cmpl	$8230, %edx
	je	.L229
	shrl	$7, %edx
	cmpl	$7168, %edx
	jne	.L73
	movq	%rcx, 128(%rsp)
	movq	%rcx, %rax
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	$0, 72(%rsp)
	je	.L137
	testl	%ecx, %ecx
	jne	.L230
.L137:
	movq	8(%rsp), %rax
	movl	$6, %r14d
	movq	%rdi, (%rax)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L229:
	movb	$-82, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L224:
	movq	72(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r14d
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L226:
	movq	16(%rsp), %rdi
	movq	%rbx, (%r12)
	movl	%r14d, %r9d
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L82:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 87(%rsp)
	je	.L12
	cmpl	$7, %r9d
	jne	.L12
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L122
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L124
.L123:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L123
.L124:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L228:
	movb	$-88, (%rbx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L142:
	movl	%r14d, %r11d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L230:
	movq	72(%rsp), %rsi
	leaq	1(%rax), %rax
	movl	$6, %r14d
	movq	%rax, %rdi
	addq	$1, (%rsi)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r10, 104(%rsp)
	subq	$8, %rsp
	movq	%r11, %rcx
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	104(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r11, 112(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r14d
	popq	%r9
	cmpl	$6, %r14d
	popq	%r10
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movq	96(%rsp), %r11
	movq	104(%rsp), %r10
	je	.L77
	cmpl	$5, %r14d
	jne	.L66
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L227:
	movq	16(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L86
	movq	(%rdi), %rax
.L86:
	addq	112(%rsp), %rax
	cmpq	%r10, %rax
	movq	8(%rsp), %rax
	je	.L232
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	movl	16(%r12), %r10d
	cmpq	$0, 96(%rax)
	je	.L233
	leaq	128(%rsp), %rbx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
	andl	$2, %r10d
.L103:
	cmpq	%r13, %rbp
	je	.L234
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rbp
	jb	.L147
	cmpq	%rdx, %r14
	jbe	.L150
	movl	0(%r13), %ecx
	cmpl	$160, %ecx
	jbe	.L217
	cmpl	$171, %ecx
	je	.L235
	cmpl	$187, %ecx
	je	.L236
	leal	-1329(%rcx), %edi
	cmpl	$89, %edi
	jbe	.L237
	cmpl	$8212, %ecx
	je	.L238
	cmpl	$8230, %ecx
	je	.L239
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L240
.L110:
	cmpq	$0, 72(%rsp)
	je	.L151
	testb	$8, 16(%r12)
	jne	.L241
.L114:
	testl	%r10d, %r10d
	jne	.L242
.L151:
	movl	$6, %ecx
.L104:
	movq	8(%rsp), %rax
	movq	120(%rsp), %r14
	movq	%r13, (%rax)
.L102:
	cmpq	%r14, %rdx
	jne	.L92
	cmpq	$5, %rcx
	jne	.L91
	cmpq	%rdx, %r15
	jne	.L83
.L94:
	subl	$1, 20(%r12)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L219:
	testq	%rdi, %rdi
	jne	.L243
	cmpl	$4, %ecx
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	ja	.L22
	leaq	120(%rsp), %r11
	movslq	%ecx, %rax
	xorl	%ebx, %ebx
.L23:
	movzbl	4(%r10,%rbx), %ecx
	movb	%cl, (%r11,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L23
	movq	%r13, %rcx
	subq	%rbx, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbp
	jb	.L244
	cmpq	(%rsp), %r15
	movl	$5, %r9d
	jnb	.L12
	leaq	1(%r13), %rcx
	leaq	119(%rsp), %r8
.L31:
	movq	%rcx, 128(%rsp)
	movzbl	-1(%rcx), %esi
	addq	$1, %rbx
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	$3, %rbx
	movb	%sil, (%r8,%rbx)
	ja	.L155
	cmpq	%rdi, %rbp
	ja	.L31
.L155:
	movl	120(%rsp), %ecx
	movq	%r11, 128(%rsp)
	cmpl	$160, %ecx
	jbe	.L245
	cmpl	$171, %ecx
	je	.L246
	cmpl	$187, %ecx
	je	.L247
	leal	-1329(%rcx), %esi
	cmpl	$89, %esi
	ja	.L37
	leaq	map_to_armscii_8(%rip), %rax
	movzbl	(%rax,%rsi), %eax
	testb	%al, %al
	je	.L38
	movb	%al, (%r15)
.L34:
	movq	128(%rsp), %rax
	addq	$1, 136(%rsp)
	leaq	4(%rax), %rcx
	cmpq	%r11, %rcx
	movq	%rcx, 128(%rsp)
	je	.L48
.L213:
	movl	(%r10), %edx
	movl	%edx, %eax
	andl	$7, %eax
.L41:
	subq	%r11, %rcx
	cmpq	%rax, %rcx
	jle	.L248
	subq	%rax, %rcx
	movq	8(%rsp), %rax
	andl	$-8, %edx
	movq	136(%rsp), %r15
	movl	16(%r12), %r14d
	addq	(%rax), %rcx
	movq	%rcx, (%rax)
	movq	40(%rsp), %rax
	movq	%rcx, %r13
	movl	%edx, (%r10)
	movq	112(%rsp), %r10
	movq	96(%rax), %rax
	jmp	.L20
.L232:
	movq	40(%rsp), %rdi
	subq	%r14, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rdi)
	je	.L249
	movq	8(%rsp), %rsi
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L83
.L237:
	leaq	map_to_armscii_8(%rip), %rsi
	movzbl	(%rsi,%rdi), %ecx
	testb	%cl, %cl
	je	.L110
	.p2align 4,,10
	.p2align 3
.L217:
	movb	%cl, (%rdx)
.L106:
	movq	136(%rsp), %rsi
	movq	128(%rsp), %rcx
	leaq	1(%rsi), %rdx
	leaq	4(%rcx), %r13
	movq	%rdx, 136(%rsp)
	movq	%r13, 128(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L134:
	movq	8(%rsp), %rax
	movq	%r13, %rdi
	movq	%r15, %rbx
	movl	$5, %r14d
	movq	%rdi, (%rax)
	jmp	.L231
.L218:
	cmpq	$0, 32(%rsp)
	jne	.L250
	movq	32(%r12), %rax
	xorl	%r9d, %r9d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L12
	movq	24(%rsp), %r14
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	72(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	*%r14
	popq	%rcx
	movl	%eax, %r9d
	popq	%rsi
	jmp	.L12
.L153:
	movl	%r11d, %r9d
	jmp	.L82
.L235:
	movb	$-89, (%rdx)
	jmp	.L106
.L245:
	movb	%cl, (%r15)
	jmp	.L34
.L249:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	movq	8(%rsp), %rdi
	cmovs	%rdx, %rbx
	movq	%rbx, %rdx
	sarq	$2, %rdx
	subq	%rdx, %rax
	movq	%rax, (%rdi)
	jmp	.L83
.L133:
	movq	%rbp, %rdi
	movq	%r15, %rbx
	movl	$4, %r14d
	jmp	.L58
.L233:
	cmpq	%r13, %rbp
	je	.L251
	leaq	4(%r15), %rsi
	andl	$2, %r10d
	movq	%r15, %rdx
	movl	$4, %ecx
	cmpq	%rsi, %r14
	jb	.L252
.L95:
	movzbl	0(%r13), %eax
	movq	%r13, %rdi
	cmpb	$-96, %al
	jbe	.L216
	leal	94(%rax), %r8d
	cmpb	$92, %r8b
	ja	.L99
	subl	$162, %eax
	leaq	map_from_armscii_8(%rip), %rdi
	cltq
	movzwl	(%rdi,%rax,2), %eax
.L216:
	addq	$1, %r13
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
	movq	%r13, %rdi
.L100:
	cmpq	%r13, %rbp
	je	.L253
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r14
	jnb	.L95
	movl	$5, %ecx
.L96:
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
	jmp	.L102
.L147:
	movl	$7, %ecx
	jmp	.L104
.L241:
	movl	%r10d, 96(%rsp)
	movl	%r11d, 48(%rsp)
	subq	$8, %rsp
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%rbx, %rcx
	movq	104(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rsi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %rdx
	movl	48(%rsp), %r11d
	movl	96(%rsp), %r10d
	je	.L114
	cmpl	$5, %eax
	jne	.L103
.L150:
	movl	$5, %ecx
	jmp	.L104
.L236:
	movb	$-90, (%rdx)
	jmp	.L106
.L99:
	cmpq	$0, 72(%rsp)
	je	.L146
	testl	%r10d, %r10d
	jne	.L254
.L146:
	movl	$6, %ecx
	jmp	.L96
.L244:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rbx, %rax
	cmpq	$4, %rax
	ja	.L25
	addq	$1, %r13
	cmpq	%rax, %rbx
	jnb	.L27
.L28:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %edx
	addq	$1, %r13
	movb	%dl, 4(%r10,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L28
.L27:
	movl	$7, %r9d
	jmp	.L12
.L246:
	movb	$-89, (%r15)
	jmp	.L34
.L238:
	movb	$-88, (%rdx)
	jmp	.L106
.L254:
	movq	72(%rsp), %rax
	leaq	1(%r13), %r13
	movl	$6, %ecx
	movq	%r13, %rdi
	addq	$1, (%rax)
	jmp	.L100
.L234:
	movslq	%eax, %rcx
	movq	%rbp, %r13
	jmp	.L104
.L247:
	movb	$-90, (%r15)
	jmp	.L34
.L37:
	cmpl	$8212, %ecx
	je	.L255
	cmpl	$8230, %ecx
	je	.L256
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L257
.L38:
	cmpq	$0, 72(%rsp)
	je	.L131
	testb	$8, %r14b
	jne	.L258
	andl	$2, %r14d
	movl	$6, %r9d
	movq	%r11, %rcx
	je	.L12
.L46:
	movq	72(%rsp), %rax
	addq	$4, %rcx
	movq	%rcx, 128(%rsp)
	addq	$1, (%rax)
.L47:
	cmpq	%r11, %rcx
	jne	.L213
.L131:
	movl	$6, %r9d
	jmp	.L12
.L242:
	movq	72(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L103
.L239:
	movb	$-82, (%rdx)
	jmp	.L106
.L258:
	leaq	(%r11,%rbx), %rax
	movq	%r10, 96(%rsp)
	movq	%r11, 88(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rax, 56(%rsp)
	pushq	80(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%r13
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	je	.L259
	movq	128(%rsp), %rcx
	cmpq	%r11, %rcx
	jne	.L213
	cmpl	$7, %eax
	je	.L260
	testl	%eax, %eax
	jne	.L12
.L48:
	movq	40(%rsp), %rax
	movq	8(%rsp), %rsi
	movq	112(%rsp), %r10
	movl	16(%r12), %r14d
	movq	96(%rax), %rax
	movq	(%rsi), %r13
	jmp	.L20
.L253:
	movq	%rbp, %rdi
	jmp	.L96
.L252:
	cmpq	%r15, %r14
	je	.L94
.L92:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rsi, 128(%rsp)
	movq	%rsi, %r13
	jmp	.L103
.L255:
	movb	$-88, (%r15)
	jmp	.L34
.L260:
	leaq	4(%r11), %rax
	cmpq	%rax, 48(%rsp)
	je	.L261
	movl	(%r10), %eax
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L262
	cmpq	$4, %rbx
	ja	.L263
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%r10)
	je	.L27
	xorl	%eax, %eax
.L55:
	movzbl	(%r11,%rax), %edx
	movb	%dl, 4(%r10,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L55
	jmp	.L27
.L251:
	cmpq	%r15, %r14
	jne	.L92
.L91:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movb	$-82, (%r15)
	jmp	.L34
.L257:
	leaq	4(%r11), %rcx
	movq	%rcx, 128(%rsp)
	jmp	.L41
.L248:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L25:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L259:
	andb	$2, %r14b
	movq	128(%rsp), %rcx
	je	.L47
	jmp	.L46
.L263:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L262:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L261:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L22:
	leaq	__PRETTY_FUNCTION__.9065(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L243:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L122:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L250:
	leaq	__PRETTY_FUNCTION__.9141(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9065, @object
	.size	__PRETTY_FUNCTION__.9065, 20
__PRETTY_FUNCTION__.9065:
	.string	"to_armscii_8_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9141, @object
	.size	__PRETTY_FUNCTION__.9141, 6
__PRETTY_FUNCTION__.9141:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	map_to_armscii_8, @object
	.size	map_to_armscii_8, 90
map_to_armscii_8:
	.byte	-78
	.byte	-76
	.byte	-74
	.byte	-72
	.byte	-70
	.byte	-68
	.byte	-66
	.byte	-64
	.byte	-62
	.byte	-60
	.byte	-58
	.byte	-56
	.byte	-54
	.byte	-52
	.byte	-50
	.byte	-48
	.byte	-46
	.byte	-44
	.byte	-42
	.byte	-40
	.byte	-38
	.byte	-36
	.byte	-34
	.byte	-32
	.byte	-30
	.byte	-28
	.byte	-26
	.byte	-24
	.byte	-22
	.byte	-20
	.byte	-18
	.byte	-16
	.byte	-14
	.byte	-12
	.byte	-10
	.byte	-8
	.byte	-6
	.byte	-4
	.byte	0
	.byte	0
	.byte	0
	.byte	-2
	.byte	-80
	.byte	-81
	.byte	-86
	.byte	-79
	.byte	0
	.byte	0
	.byte	-77
	.byte	-75
	.byte	-73
	.byte	-71
	.byte	-69
	.byte	-67
	.byte	-65
	.byte	-63
	.byte	-61
	.byte	-59
	.byte	-57
	.byte	-55
	.byte	-53
	.byte	-51
	.byte	-49
	.byte	-47
	.byte	-45
	.byte	-43
	.byte	-41
	.byte	-39
	.byte	-37
	.byte	-35
	.byte	-33
	.byte	-31
	.byte	-29
	.byte	-27
	.byte	-25
	.byte	-23
	.byte	-21
	.byte	-19
	.byte	-17
	.byte	-15
	.byte	-13
	.byte	-11
	.byte	-9
	.byte	-7
	.byte	-5
	.byte	-3
	.byte	-94
	.byte	0
	.byte	-93
	.byte	-83
	.align 32
	.type	map_from_armscii_8, @object
	.size	map_from_armscii_8, 186
map_from_armscii_8:
	.value	1415
	.value	1417
	.value	41
	.value	40
	.value	187
	.value	171
	.value	8212
	.value	46
	.value	1373
	.value	44
	.value	45
	.value	1418
	.value	8230
	.value	1372
	.value	1371
	.value	1374
	.value	1329
	.value	1377
	.value	1330
	.value	1378
	.value	1331
	.value	1379
	.value	1332
	.value	1380
	.value	1333
	.value	1381
	.value	1334
	.value	1382
	.value	1335
	.value	1383
	.value	1336
	.value	1384
	.value	1337
	.value	1385
	.value	1338
	.value	1386
	.value	1339
	.value	1387
	.value	1340
	.value	1388
	.value	1341
	.value	1389
	.value	1342
	.value	1390
	.value	1343
	.value	1391
	.value	1344
	.value	1392
	.value	1345
	.value	1393
	.value	1346
	.value	1394
	.value	1347
	.value	1395
	.value	1348
	.value	1396
	.value	1349
	.value	1397
	.value	1350
	.value	1398
	.value	1351
	.value	1399
	.value	1352
	.value	1400
	.value	1353
	.value	1401
	.value	1354
	.value	1402
	.value	1355
	.value	1403
	.value	1356
	.value	1404
	.value	1357
	.value	1405
	.value	1358
	.value	1406
	.value	1359
	.value	1407
	.value	1360
	.value	1408
	.value	1361
	.value	1409
	.value	1362
	.value	1410
	.value	1363
	.value	1411
	.value	1364
	.value	1412
	.value	1365
	.value	1413
	.value	1366
	.value	1414
	.value	1370
