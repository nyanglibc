	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	movzbl	%sil, %eax
	testb	%sil, %sil
	movl	$-1, %edx
	cmovs	%edx, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EUC-CN//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L6
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"#$"
.LC2:
	.string	"#~"
.LC3:
	.string	"!j"
.LC4:
	.string	"!i"
.LC5:
	.string	"!a"
.LC6:
	.string	"!b"
.LC7:
	.string	"!n"
.LC8:
	.string	"!o"
.LC9:
	.string	"!q"
.LC10:
	.string	"!r"
.LC11:
	.string	"!p"
.LC12:
	.string	"!s"
.LC13:
	.string	"!t"
.LC14:
	.string	"!w"
.LC15:
	.string	"!x"
.LC16:
	.string	"!u"
.LC17:
	.string	"!v"
.LC18:
	.string	"!P"
.LC19:
	.string	"!%"
.LC20:
	.string	"!&"
.LC21:
	.string	"(8"
.LC22:
	.string	"(7"
.LC23:
	.string	"(6"
.LC24:
	.string	"(5"
.LC25:
	.string	"(3"
.LC26:
	.string	"(/"
.LC27:
	.string	"(+"
.LC28:
	.string	"(#"
.LC29:
	.string	"(1"
.LC30:
	.string	"(-"
.LC31:
	.string	"()"
.LC32:
	.string	"('"
.LC33:
	.string	"(%"
.LC34:
	.string	"../iconv/skeleton.c"
.LC35:
	.string	"outbufstart == NULL"
.LC36:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC38:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC39:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC40:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC41:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC42:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC43:
	.string	"./gb2312.h"
.LC44:
	.string	"cp[1] != '\\0'"
.LC45:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC47:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$184, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 32(%rsp)
	addq	$104, %rdi
	movq	%rdx, (%rsp)
	movq	%rdi, 48(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 16(%rsp)
	testb	$1, %r11b
	movq	%r9, 40(%rsp)
	movl	240(%rsp), %ebx
	movq	%rdi, 56(%rsp)
	movq	$0, 8(%rsp)
	jne	.L11
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 8(%rsp)
	je	.L11
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 8(%rsp)
.L11:
	testl	%ebx, %ebx
	jne	.L517
	movq	(%rsp), %rax
	movq	16(%rsp), %rsi
	leaq	112(%rsp), %rdx
	movq	8(%r12), %r14
	testq	%rsi, %rsi
	movq	(%rax), %r13
	movq	%rsi, %rax
	cmove	%r12, %rax
	cmpq	$0, 40(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	movq	%rax, 64(%rsp)
	movl	248(%rsp), %eax
	testl	%eax, %eax
	jne	.L518
.L130:
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L519
.L136:
	movq	%r13, 144(%rsp)
	movq	%r15, 152(%rsp)
	movq	%r15, %rbx
	movq	%r13, %rax
	movl	$4, %r10d
.L150:
	cmpq	%rax, %rbp
	je	.L151
.L223:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L360
	cmpq	%rbx, %r14
	jbe	.L515
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L152
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%rbx)
.L153:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L223
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, 16(%rsp)
	movq	(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L520
.L224:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L521
	cmpq	%rbx, %r15
	jnb	.L365
	movq	8(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r10d, 24(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	56(%rsp), %r9
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%rdi
	movl	24(%rsp), %r10d
	je	.L228
	movq	120(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L522
.L227:
	testl	%r11d, %r11d
	jne	.L382
.L323:
	movq	(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r15
	movq	(%rax), %r13
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L136
.L519:
	cmpq	%r13, %rbp
	je	.L350
	leaq	4(%r15), %rcx
	movq	%r13, %rax
	movq	%r15, %rbx
	cmpq	%rcx, %r14
	jb	.L515
	movl	$4, %r10d
	andl	$2, %r11d
.L138:
	movzbl	(%rax), %edx
	cmpl	$127, %edx
	movl	%edx, %edi
	movl	%edx, %esi
	ja	.L139
	addq	$1, %rax
.L140:
	movl	%edx, (%rbx)
	movq	%rcx, %rbx
.L144:
	cmpq	%rax, %rbp
	je	.L151
.L146:
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r14
	jnb	.L138
.L515:
	movl	$5, %r10d
.L554:
	cmpq	$0, 16(%rsp)
	movq	(%rsp), %rdi
	movq	%rax, (%rdi)
	je	.L224
.L520:
	movq	16(%rsp), %rax
	movq	%rbx, (%rax)
.L10:
	addq	$184, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	cmpl	$9371, %edx
	ja	.L155
	cmpl	$9312, %edx
	jnb	.L156
	cmpl	$472, %edx
	je	.L157
	ja	.L158
	cmpl	$333, %edx
	je	.L159
	jbe	.L523
	cmpl	$464, %edx
	je	.L166
	jbe	.L524
	cmpl	$468, %edx
	je	.L170
	cmpl	$470, %edx
	je	.L171
	cmpl	$466, %edx
	jne	.L331
	leaq	.LC26(%rip), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$9792, %edx
	je	.L185
	ja	.L186
	cmpl	$9670, %edx
	je	.L187
	jbe	.L525
	cmpl	$9678, %edx
	je	.L195
	jbe	.L526
	cmpl	$9733, %edx
	je	.L199
	cmpl	$9734, %edx
	je	.L200
	cmpl	$9679, %edx
	jne	.L331
	leaq	.LC9(%rip), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L360:
	movl	$7, %r10d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L139:
	cmpl	$160, %edx
	jbe	.L527
	cmpl	$255, %edx
	je	.L142
.L143:
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbp
	jbe	.L360
	movzbl	1(%rax), %r8d
	cmpb	$-96, %r8b
	jbe	.L528
	testb	%dil, %dil
	js	.L529
.L147:
	cmpq	$0, 64(%rsp)
	je	.L364
	testl	%r11d, %r11d
	je	.L364
	movq	64(%rsp), %rcx
	addq	$2, %rax
	movl	$6, %r10d
	addq	$1, (%rcx)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L527:
	leal	-142(%rdx), %edx
	cmpl	$1, %edx
	jbe	.L143
.L142:
	cmpq	$0, 64(%rsp)
	je	.L364
	testl	%r11d, %r11d
	jne	.L530
.L364:
	movl	$6, %r10d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	$5, %r10d
	movl	%r10d, %r11d
	jne	.L227
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	$1105, %edx
	ja	.L173
	cmpl	$1025, %edx
	jnb	.L174
	cmpl	$711, %edx
	je	.L175
	ja	.L176
	cmpl	$474, %edx
	je	.L177
	cmpl	$476, %edx
	leaq	.LC21(%rip), %rsi
	je	.L161
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L186:
	cmpl	$40864, %edx
	jbe	.L531
	cmpl	$65504, %edx
	je	.L208
	jbe	.L532
	cmpl	$65507, %edx
	je	.L211
	cmpl	$65509, %edx
	je	.L212
	cmpl	$65505, %edx
	jne	.L331
	leaq	.LC3(%rip), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L529:
	subl	$161, %esi
	cmpl	$86, %esi
	ja	.L147
	cmpb	$-1, %r8b
	movzbl	%r8b, %edx
	je	.L147
	imull	$94, %esi, %esi
	leal	-161(%rdx,%rsi), %edx
	cmpl	$8177, %edx
	jg	.L147
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	testw	%dx, %dx
	je	.L147
	cmpl	$65533, %edx
	je	.L147
	addq	$2, %rax
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L525:
	cmpl	$9632, %edx
	je	.L189
	jbe	.L533
	cmpl	$9650, %edx
	je	.L192
	cmpl	$9651, %edx
	je	.L193
	cmpl	$9633, %edx
	jne	.L331
	leaq	.LC16(%rip), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L518:
	movq	32(%r12), %rbx
	movl	(%rbx), %edx
	movl	%edx, %eax
	andl	$7, %eax
	je	.L130
	testq	%rsi, %rsi
	jne	.L534
	movq	32(%rsp), %rsi
	cmpq	$0, 96(%rsi)
	je	.L535
	cmpl	$4, %eax
	movq	%r13, 128(%rsp)
	movq	%r15, 136(%rsp)
	ja	.L41
	leaq	108(%rsp), %rdi
	cltq
	xorl	%ecx, %ecx
	movq	%rax, 24(%rsp)
	movq	%rdi, 72(%rsp)
.L42:
	movzbl	4(%rbx,%rcx), %esi
	movb	%sil, (%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L42
	movq	%r13, %rsi
	subq	%rcx, %rsi
	addq	$4, %rsi
	cmpq	%rsi, %rbp
	jb	.L536
	cmpq	%r14, %r15
	jnb	.L118
	movq	24(%rsp), %rsi
	leaq	1(%r13), %rcx
	leaq	107(%rsp), %r8
.L50:
	movq	%rcx, 128(%rsp)
	movzbl	-1(%rcx), %edi
	addq	$1, %rsi
	movq	%rcx, %r9
	addq	$1, %rcx
	cmpq	$3, %rsi
	movb	%dil, (%r8,%rsi)
	ja	.L384
	cmpq	%r9, %rbp
	ja	.L50
.L384:
	movl	108(%rsp), %ecx
	movq	%rsi, 24(%rsp)
	movq	72(%rsp), %rsi
	cmpl	$127, %ecx
	movq	%rsi, 128(%rsp)
	jbe	.L537
	cmpl	$9371, %ecx
	ja	.L55
	cmpl	$9312, %ecx
	jnb	.L56
	cmpl	$472, %ecx
	je	.L57
	ja	.L58
	cmpl	$333, %ecx
	je	.L59
	jbe	.L538
	cmpl	$464, %ecx
	je	.L66
	jbe	.L539
	cmpl	$468, %ecx
	je	.L70
	cmpl	$470, %ecx
	je	.L71
	cmpl	$466, %ecx
	jne	.L329
	leaq	.LC26(%rip), %rsi
.L61:
	movzbl	(%rsi), %edi
	movzbl	1(%rsi), %eax
.L328:
	testb	%al, %al
	je	.L217
.L117:
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$1, %rax
	jbe	.L118
	movb	%dil, (%r15)
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r15)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 136(%rsp)
	addb	$-128, (%rax)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 136(%rsp)
	addb	$-128, (%rax)
.L53:
	movq	128(%rsp), %rax
	movq	72(%rsp), %rdi
	addq	$4, %rax
	cmpq	%rdi, %rax
	movq	%rax, 128(%rsp)
	je	.L514
.L513:
	movl	(%rbx), %edx
	subq	%rdi, %rax
	movq	%rax, %r13
	movl	%edx, %eax
	andl	$7, %eax
.L120:
	cmpq	%r13, %rax
	jge	.L540
	subq	%rax, %r13
	movq	(%rsp), %rax
	andl	$-8, %edx
	movq	136(%rsp), %r15
	movl	16(%r12), %r11d
	addq	(%rax), %r13
	movq	%r13, (%rax)
	movl	%edx, (%rbx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L524:
	cmpl	$363, %edx
	je	.L168
	cmpl	$462, %edx
	leaq	.LC28(%rip), %rsi
	je	.L161
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	$9671, %edx
	je	.L197
	cmpl	$9675, %edx
	leaq	.LC11(%rip), %rsi
	je	.L161
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L523:
	cmpl	$275, %edx
	je	.L362
	jbe	.L541
	cmpl	$283, %edx
	je	.L164
	cmpl	$299, %edx
	leaq	.LC31(%rip), %rsi
	je	.L161
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	$19968, %edx
	jnb	.L203
	cmpl	$12585, %edx
	ja	.L204
	cmpl	$12288, %edx
	jnb	.L205
	cmpl	$9794, %edx
	leaq	.LC5(%rip), %rsi
	jne	.L331
	.p2align 4,,10
	.p2align 3
.L161:
	movzbl	(%rsi), %edi
	movzbl	1(%rsi), %eax
.L330:
	testb	%al, %al
	je	.L217
.L334:
	movq	%r14, %rax
	subq	%rbx, %rax
	cmpq	$1, %rax
	jbe	.L218
	movb	%dil, (%rbx)
	movzbl	1(%rsi), %eax
	movb	%al, 1(%rbx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 152(%rsp)
	addb	$-128, (%rdx)
	movq	152(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 152(%rsp)
	addb	$-128, (%rdx)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L521:
	movq	40(%rsp), %rsi
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rsi)
.L226:
	cmpl	$7, %r10d
	jne	.L10
	movl	248(%rsp), %eax
	testl	%eax, %eax
	je	.L10
	movq	(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L325
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L327
.L326:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L326
.L327:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L365:
	movl	%r10d, %r11d
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L173:
	cmpl	$8869, %edx
	ja	.L181
	cmpl	$8451, %edx
	jnb	.L182
	leal	-8213(%rdx), %esi
	cmpl	$38, %esi
	ja	.L331
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	.p2align 4,,10
	.p2align 3
.L214:
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	jne	.L542
.L331:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L543
	cmpq	$0, 64(%rsp)
	je	.L364
	testb	$8, 16(%r12)
	jne	.L544
.L221:
	testb	$2, %r11b
	je	.L364
	movq	64(%rsp), %rcx
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 144(%rsp)
	addq	$1, (%rcx)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L176:
	cmpl	$713, %edx
	je	.L179
	jb	.L331
	leal	-913(%rdx), %esi
	cmpl	$56, %esi
	ja	.L331
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r13, (%rax)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L545
	movq	%r13, 160(%rsp)
	movq	%r15, 168(%rsp)
	movq	%r15, %rdx
	movl	$4, %eax
.L248:
	cmpq	%r13, %rbp
	je	.L546
.L320:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rbp
	jb	.L375
	cmpq	%rdx, %r10
	jbe	.L379
	movl	0(%r13), %ecx
	cmpl	$127, %ecx
	ja	.L250
	leaq	1(%rdx), %rsi
	movq	%rsi, 168(%rsp)
	movb	%cl, (%rdx)
.L251:
	movq	160(%rsp), %rsi
	movq	168(%rsp), %rdx
	leaq	4(%rsi), %r13
	cmpq	%r13, %rbp
	movq	%r13, 160(%rsp)
	jne	.L320
.L546:
	cltq
	movq	%rbp, %r13
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L530:
	movq	64(%rsp), %rsi
	addq	$1, %rax
	movl	$6, %r10d
	addq	$1, (%rsi)
	jmp	.L144
.L175:
	leaq	.LC20(%rip), %rsi
	jmp	.L161
.L179:
	leaq	.LC19(%rip), %rsi
	jmp	.L161
.L177:
	leaq	.LC22(%rip), %rsi
	jmp	.L161
.L168:
	leaq	.LC29(%rip), %rsi
	jmp	.L161
.L171:
	leaq	.LC24(%rip), %rsi
	jmp	.L161
.L164:
	leaq	.LC32(%rip), %rsi
	jmp	.L161
.L208:
	leaq	.LC4(%rip), %rsi
	jmp	.L161
.L212:
	leaq	.LC1(%rip), %rsi
	jmp	.L161
.L197:
	leaq	.LC12(%rip), %rsi
	jmp	.L161
.L189:
	leaq	.LC17(%rip), %rsi
	jmp	.L161
.L200:
	leaq	.LC7(%rip), %rsi
	jmp	.L161
.L193:
	leaq	.LC14(%rip), %rsi
	jmp	.L161
.L205:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L182:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L535:
	cmpl	$4, %eax
	ja	.L21
	movzbl	4(%rbx), %ecx
	cmpl	$1, %eax
	movb	%cl, 168(%rsp)
	movl	$1, %ecx
	je	.L22
	movzbl	5(%rbx), %ecx
	movb	%cl, 169(%rsp)
	movl	$2, %ecx
.L22:
	leaq	4(%r15), %r8
	cmpq	%r8, %r14
	jb	.L118
	movzbl	0(%r13), %esi
	movb	%sil, 168(%rsp,%rcx)
	movzbl	168(%rsp), %esi
	cmpl	$127, %esi
	movl	%esi, %r10d
	movl	%esi, %r9d
	jbe	.L340
	cmpl	$160, %esi
	jbe	.L547
	cmpl	$255, %esi
	je	.L27
.L28:
	leaq	168(%rsp), %rdi
	addq	$1, %rcx
	leaq	(%rdi,%rcx), %r9
	movq	%r9, 24(%rsp)
	leaq	1(%rdi), %r9
	cmpq	%r9, 24(%rsp)
	jbe	.L31
	movzbl	169(%rsp), %ecx
	cmpb	$-96, %cl
	jbe	.L548
	testb	%r10b, %r10b
	js	.L549
.L33:
	cmpq	$0, 64(%rsp)
	movl	$6, %r10d
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	64(%rsp), %rcx
	addq	$1, (%rcx)
	leaq	2(%rdi), %rcx
.L30:
	subq	%rdi, %rcx
	cltq
	cmpq	%rax, %rcx
	movq	%rcx, %r11
	jle	.L550
	subq	%rax, %r11
	movq	(%rsp), %rax
	andl	$-8, %edx
	addq	%r11, %r13
	movl	16(%r12), %r11d
	movq	%r13, (%rax)
	movl	%edx, (%rbx)
	jmp	.L130
.L545:
	cmpq	%r13, %rbp
	je	.L551
	leaq	4(%r15), %rcx
	andl	$2, %ebx
	movq	%r15, %rdx
	movl	$4, %eax
	cmpq	%rcx, %r10
	jb	.L552
.L234:
	movzbl	0(%r13), %r8d
	cmpl	$127, %r8d
	movl	%r8d, %edi
	movl	%r8d, %esi
	ja	.L237
	addq	$1, %r13
.L238:
	movl	%r8d, (%rdx)
	movq	%rcx, %rdx
.L242:
	cmpq	%r13, %rbp
	je	.L553
.L244:
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r10
	jnb	.L234
	movl	$5, %eax
	jmp	.L236
.L118:
	movl	$5, %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L218:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movl	$5, %r10d
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L250:
	cmpl	$9371, %ecx
	ja	.L253
	cmpl	$9312, %ecx
	jnb	.L254
	cmpl	$472, %ecx
	je	.L255
	ja	.L256
	cmpl	$333, %ecx
	je	.L257
	jbe	.L555
	cmpl	$464, %ecx
	je	.L264
	jbe	.L556
	cmpl	$468, %ecx
	je	.L268
	cmpl	$470, %ecx
	jne	.L557
	leaq	.LC24(%rip), %rdi
	.p2align 4,,10
	.p2align 3
.L259:
	movzbl	(%rdi), %r8d
	movzbl	1(%rdi), %ecx
.L332:
	testb	%cl, %cl
	je	.L217
.L335:
	movq	%r10, %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jbe	.L315
	movb	%r8b, (%rdx)
	movzbl	1(%rdi), %ecx
	movb	%cl, 1(%rdx)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	addb	$-128, (%rdx)
	movq	168(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 168(%rsp)
	addb	$-128, (%rdx)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L528:
	cmpq	$0, 64(%rsp)
	je	.L364
	testl	%r11d, %r11d
	je	.L364
	movq	64(%rsp), %rax
	movl	$6, %r10d
	addq	$1, (%rax)
	movq	%rdx, %rax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L204:
	leal	-12832(%rdx), %esi
	cmpl	$9, %esi
	ja	.L331
	addl	$69, %edx
	movb	$34, 168(%rsp)
	movl	$34, %edi
	movb	%dl, 169(%rsp)
.L215:
	leaq	168(%rsp), %rsi
	jmp	.L334
.L237:
	cmpl	$160, %r8d
	jbe	.L558
	cmpl	$255, %r8d
	je	.L240
.L241:
	leaq	1(%r13), %r8
	cmpq	%r8, %rbp
	jbe	.L370
	movzbl	1(%r13), %r9d
	cmpb	$-96, %r9b
	jbe	.L559
	testb	%dil, %dil
	js	.L560
.L245:
	cmpq	$0, 64(%rsp)
	je	.L374
	testl	%ebx, %ebx
	jne	.L561
.L374:
	movl	$6, %eax
.L236:
	movq	(%rsp), %rcx
	movq	%r13, (%rcx)
	jmp	.L247
.L541:
	leal	-164(%rdx), %esi
	cmpl	$93, %esi
	ja	.L331
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L181:
	cmpl	$8978, %edx
	leaq	.LC18(%rip), %rsi
	je	.L161
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$9792, %ecx
	je	.L283
	ja	.L284
	cmpl	$9670, %ecx
	je	.L285
	jbe	.L562
	cmpl	$9678, %ecx
	je	.L293
	jbe	.L563
	cmpl	$9733, %ecx
	je	.L297
	cmpl	$9734, %ecx
	jne	.L564
	leaq	.LC7(%rip), %rdi
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L533:
	leal	-9472(%rdx), %esi
	cmpl	$75, %esi
	ja	.L331
	addl	$36, %edx
	movb	$41, 168(%rsp)
	movl	$41, %edi
	movb	%dl, 169(%rsp)
	jmp	.L215
.L532:
	leal	-65281(%rdx), %esi
	cmpl	$93, %esi
	ja	.L331
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L544:
	movl	%r11d, 24(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	pushq	72(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %rdi
	movq	%r12, %rsi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r8
	cmpl	$6, %r10d
	popq	%r9
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movl	24(%rsp), %r11d
	je	.L221
	cmpl	$5, %r10d
	jne	.L150
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	.LC6(%rip), %rsi
	jmp	.L161
.L187:
	leaq	.LC13(%rip), %rsi
	jmp	.L161
.L174:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L166:
	leaq	.LC27(%rip), %rsi
	jmp	.L161
.L211:
	leaq	.LC2(%rip), %rsi
	jmp	.L161
.L517:
	cmpq	$0, 16(%rsp)
	jne	.L565
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L10
	movq	8(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	248(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	56(%rsp), %r9
	movq	72(%rsp), %rsi
	movq	64(%rsp), %rdi
	call	*%r15
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L10
.L195:
	leaq	.LC10(%rip), %rsi
	jmp	.L161
.L156:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L157:
	leaq	.LC23(%rip), %rsi
	jmp	.L161
.L159:
	leaq	.LC30(%rip), %rsi
	jmp	.L161
.L362:
	leaq	.LC33(%rip), %rsi
	jmp	.L161
.L192:
	leaq	.LC15(%rip), %rsi
	jmp	.L161
.L199:
	leaq	.LC8(%rip), %rsi
	jmp	.L161
.L170:
	leaq	.LC25(%rip), %rsi
	jmp	.L161
.L203:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L214
.L543:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L150
.L382:
	movl	%r11d, %r10d
	jmp	.L226
.L375:
	movl	$7, %eax
.L249:
	movq	(%rsp), %rsi
	movq	120(%rsp), %r10
	movq	%r13, (%rsi)
.L247:
	cmpq	%r10, %rdx
	jne	.L233
	cmpq	$5, %rax
	jne	.L232
.L322:
	cmpq	%r15, %rdx
	jne	.L227
.L235:
	subl	$1, 20(%r12)
	jmp	.L227
.L537:
	leaq	1(%r15), %rax
	movq	%rax, 136(%rsp)
	movb	%cl, (%r15)
	jmp	.L53
.L560:
	subl	$161, %esi
	cmpl	$86, %esi
	ja	.L245
	cmpb	$-1, %r9b
	movzbl	%r9b, %edi
	je	.L245
	imull	$94, %esi, %esi
	leal	-161(%rdi,%rsi), %esi
	cmpl	$8177, %esi
	jg	.L245
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rdi
	movslq	%esi, %rsi
	movzwl	(%rdi,%rsi,2), %r8d
	testw	%r8w, %r8w
	je	.L245
	cmpl	$65533, %r8d
	je	.L245
	addq	$2, %r13
	jmp	.L238
.L562:
	cmpl	$9632, %ecx
	je	.L287
	jbe	.L566
	cmpl	$9650, %ecx
	je	.L290
	cmpl	$9651, %ecx
	jne	.L567
	leaq	.LC14(%rip), %rdi
	jmp	.L259
.L555:
	cmpl	$275, %ecx
	je	.L377
	jbe	.L568
	cmpl	$283, %ecx
	jne	.L569
	leaq	.LC32(%rip), %rdi
	jmp	.L259
.L576:
	movq	%r10, 72(%rsp)
	movl	%r11d, 24(%rsp)
	leaq	160(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	72(%rsp)
	movq	16(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	184(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	160(%rsp), %r13
	movq	168(%rsp), %rdx
	movl	24(%rsp), %r11d
	movq	72(%rsp), %r10
	je	.L318
	cmpl	$5, %eax
	jne	.L248
.L379:
	movl	$5, %eax
	jmp	.L249
.L256:
	cmpl	$1105, %ecx
	ja	.L271
	cmpl	$1025, %ecx
	jnb	.L272
	cmpl	$711, %ecx
	je	.L273
	jbe	.L570
	cmpl	$713, %ecx
	jne	.L571
	leaq	.LC19(%rip), %rdi
	jmp	.L259
.L284:
	cmpl	$40864, %ecx
	jbe	.L572
	cmpl	$65504, %ecx
	je	.L306
	jbe	.L573
	cmpl	$65507, %ecx
	je	.L309
	cmpl	$65509, %ecx
	jne	.L574
	leaq	.LC1(%rip), %rdi
	jmp	.L259
.L315:
	movq	120(%rsp), %rdx
	cmpq	%rdx, 168(%rsp)
	movq	160(%rsp), %rax
	movq	(%rsp), %rsi
	movq	%rax, (%rsi)
	je	.L322
.L233:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC34(%rip), %rsi
	leaq	.LC45(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%rbp, %rax
	movq	%r15, %rbx
	movl	$4, %r10d
	jmp	.L151
.L572:
	cmpl	$19968, %ecx
	jnb	.L301
	cmpl	$12585, %ecx
	ja	.L302
	cmpl	$12288, %ecx
	jnb	.L303
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rdi
	je	.L259
.L333:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L575
	cmpq	$0, 64(%rsp)
	je	.L380
	testb	$8, 16(%r12)
	jne	.L576
.L318:
	testb	$2, %bl
	jne	.L577
.L380:
	movl	$6, %eax
	jmp	.L249
.L55:
	cmpl	$9792, %ecx
	je	.L85
	ja	.L86
	cmpl	$9670, %ecx
	je	.L87
	jbe	.L578
	cmpl	$9678, %ecx
	je	.L95
	jbe	.L579
	cmpl	$9733, %ecx
	je	.L99
	cmpl	$9734, %ecx
	je	.L100
	cmpl	$9679, %ecx
	jne	.L329
	leaq	.LC9(%rip), %rsi
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	168(%rsp), %rdi
	leaq	1(%rdi), %rcx
.L25:
	movl	%r9d, (%r15)
	movl	(%rbx), %edx
	movq	%r8, %r15
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L30
.L558:
	leal	-142(%r8), %r8d
	cmpl	$1, %r8d
	jbe	.L241
.L240:
	cmpq	$0, 64(%rsp)
	je	.L374
	testl	%ebx, %ebx
	je	.L374
	movq	64(%rsp), %rax
	addq	$1, %r13
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L242
.L271:
	cmpl	$8869, %ecx
	ja	.L279
	cmpl	$8451, %ecx
	jnb	.L280
	leal	-8213(%rcx), %edi
	cmpl	$38, %edi
	ja	.L333
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
.L312:
	movzbl	(%rdi), %r8d
	testb	%r8b, %r8b
	je	.L333
	movzbl	1(%rdi), %ecx
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rcx, %rax
	cmpq	$4, %rax
	ja	.L44
	addq	$1, %r13
	cmpq	%rcx, %rax
	movq	24(%rsp), %rdx
	jbe	.L46
.L47:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %ecx
	addq	$1, %r13
	movb	%cl, 4(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L47
.L46:
	movl	$7, %r10d
	jmp	.L10
.L570:
	cmpl	$474, %ecx
	jne	.L580
	leaq	.LC22(%rip), %rdi
	jmp	.L259
.L556:
	cmpl	$363, %ecx
	jne	.L581
	leaq	.LC29(%rip), %rdi
	jmp	.L259
.L563:
	cmpl	$9671, %ecx
	jne	.L582
	leaq	.LC12(%rip), %rdi
	jmp	.L259
.L370:
	movl	$7, %eax
	jmp	.L236
.L58:
	cmpl	$1105, %ecx
	ja	.L73
	cmpl	$1025, %ecx
	jnb	.L74
	cmpl	$711, %ecx
	je	.L75
	jbe	.L583
	cmpl	$713, %ecx
	je	.L79
	jb	.L329
	leal	-913(%rcx), %esi
	cmpl	$56, %esi
	ja	.L329
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
.L114:
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	jne	.L584
.L329:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L585
	cmpq	$0, 64(%rsp)
	je	.L121
	testb	$8, %r11b
	jne	.L586
	andl	$2, %r11d
	jne	.L126
.L121:
	movl	$6, %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L559:
	cmpq	$0, 64(%rsp)
	je	.L374
	testl	%ebx, %ebx
	je	.L374
	movq	64(%rsp), %rax
	movq	%r8, %r13
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L244
.L86:
	cmpl	$40864, %ecx
	jbe	.L587
	cmpl	$65504, %ecx
	je	.L108
	jbe	.L588
	cmpl	$65507, %ecx
	je	.L111
	cmpl	$65509, %ecx
	je	.L112
	cmpl	$65505, %ecx
	jne	.L329
	leaq	.LC3(%rip), %rsi
	jmp	.L61
.L586:
	movq	72(%rsp), %rax
	addq	24(%rsp), %rax
	leaq	128(%rsp), %rcx
	movl	%r11d, 92(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, 88(%rsp)
	pushq	72(%rsp)
	movq	%rax, %r8
	movq	48(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r11
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%r13
	movl	92(%rsp), %r11d
	je	.L589
	movq	128(%rsp), %rax
	movq	72(%rsp), %rdi
	cmpq	%rdi, %rax
	jne	.L513
	cmpl	$7, %r10d
	je	.L590
	testl	%r10d, %r10d
	jne	.L10
.L514:
	movq	(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r13
	jmp	.L130
.L582:
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rdi
	je	.L259
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L279:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rdi
	je	.L259
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L567:
	cmpl	$9633, %ecx
	jne	.L333
	leaq	.LC16(%rip), %rdi
	jmp	.L259
.L566:
	leal	-9472(%rcx), %edi
	cmpl	$75, %edi
	ja	.L333
	addl	$36, %ecx
	movb	$41, 152(%rsp)
	movl	$41, %r8d
	movb	%cl, 153(%rsp)
.L313:
	leaq	152(%rsp), %rdi
	jmp	.L335
.L552:
	cmpq	%r15, %r10
	je	.L235
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L573:
	leal	-65281(%rcx), %edi
	cmpl	$93, %edi
	ja	.L333
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L571:
	jb	.L333
	leal	-913(%rcx), %edi
	cmpl	$56, %edi
	ja	.L333
	movq	__gb2312_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L574:
	cmpl	$65505, %ecx
	jne	.L333
	leaq	.LC3(%rip), %rdi
	jmp	.L259
.L557:
	cmpl	$466, %ecx
	jne	.L333
	leaq	.LC26(%rip), %rdi
	jmp	.L259
.L580:
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rdi
	je	.L259
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L564:
	cmpl	$9679, %ecx
	jne	.L333
	leaq	.LC9(%rip), %rdi
	jmp	.L259
.L581:
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rdi
	je	.L259
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L569:
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rdi
	je	.L259
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L568:
	leal	-164(%rcx), %edi
	cmpl	$93, %edi
	ja	.L333
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L302:
	leal	-12832(%rcx), %edi
	cmpl	$9, %edi
	ja	.L333
	addl	$69, %ecx
	movb	$34, 152(%rsp)
	movl	$34, %r8d
	movb	%cl, 153(%rsp)
	jmp	.L313
.L293:
	leaq	.LC10(%rip), %rdi
	jmp	.L259
.L377:
	leaq	.LC33(%rip), %rdi
	jmp	.L259
.L301:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-19968(%rcx), %edi
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L303:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-12288(%rcx), %edi
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L283:
	leaq	.LC6(%rip), %rdi
	jmp	.L259
.L268:
	leaq	.LC25(%rip), %rdi
	jmp	.L259
.L264:
	leaq	.LC27(%rip), %rdi
	jmp	.L259
.L297:
	leaq	.LC8(%rip), %rdi
	jmp	.L259
.L257:
	leaq	.LC30(%rip), %rdi
	jmp	.L259
.L306:
	leaq	.LC4(%rip), %rdi
	jmp	.L259
.L309:
	leaq	.LC2(%rip), %rdi
	jmp	.L259
.L285:
	leaq	.LC13(%rip), %rdi
	jmp	.L259
.L549:
	subl	$161, %esi
	cmpl	$86, %esi
	ja	.L33
	cmpb	$-1, %cl
	movzbl	%cl, %r9d
	je	.L33
	imull	$94, %esi, %esi
	leal	-161(%r9,%rsi), %ecx
	cmpl	$8177, %ecx
	jg	.L33
	movq	__gb2312_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %r9d
	testw	%r9w, %r9w
	je	.L33
	cmpl	$65533, %r9d
	leaq	2(%rdi), %rcx
	jne	.L25
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L272:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-1025(%rcx), %edi
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L273:
	leaq	.LC20(%rip), %rdi
	jmp	.L259
.L578:
	cmpl	$9632, %ecx
	je	.L89
	jbe	.L591
	cmpl	$9650, %ecx
	je	.L92
	cmpl	$9651, %ecx
	je	.L93
	cmpl	$9633, %ecx
	jne	.L329
	leaq	.LC16(%rip), %rsi
	jmp	.L61
.L287:
	leaq	.LC17(%rip), %rdi
	jmp	.L259
.L280:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8451(%rcx), %edi
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L290:
	leaq	.LC15(%rip), %rdi
	jmp	.L259
.L255:
	leaq	.LC23(%rip), %rdi
	jmp	.L259
.L254:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leal	-9312(%rcx), %edi
	leaq	(%r8,%rdi,2), %rdi
	jmp	.L312
.L538:
	cmpl	$275, %ecx
	je	.L349
	jbe	.L592
	cmpl	$283, %ecx
	je	.L64
	cmpl	$299, %ecx
	leaq	.LC31(%rip), %rsi
	je	.L61
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L577:
	movq	64(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 160(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L248
.L553:
	movq	%rbp, %r13
	jmp	.L236
.L575:
	movq	%rsi, 160(%rsp)
	movq	%rsi, %r13
	jmp	.L248
.L587:
	cmpl	$19968, %ecx
	jnb	.L103
	cmpl	$12585, %ecx
	ja	.L104
	cmpl	$12288, %ecx
	jnb	.L105
	cmpl	$9794, %ecx
	leaq	.LC5(%rip), %rsi
	je	.L61
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L583:
	cmpl	$474, %ecx
	je	.L77
	cmpl	$476, %ecx
	leaq	.LC21(%rip), %rsi
	je	.L61
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L547:
	leal	-142(%rsi), %edi
	cmpl	$1, %edi
	jbe	.L28
.L27:
	cmpq	$0, 64(%rsp)
	movl	$6, %r10d
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	64(%rsp), %rsi
	leaq	168(%rsp), %rdi
	leaq	1(%rdi), %rcx
	addq	$1, (%rsi)
	jmp	.L30
.L73:
	cmpl	$8869, %ecx
	ja	.L81
	cmpl	$8451, %ecx
	jnb	.L82
	leal	-8213(%rcx), %esi
	cmpl	$38, %esi
	ja	.L329
	movq	__gb2312_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L561:
	movq	64(%rsp), %rax
	addq	$2, %r13
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L242
.L71:
	leaq	.LC24(%rip), %rsi
	jmp	.L61
.L70:
	leaq	.LC25(%rip), %rsi
	jmp	.L61
.L57:
	leaq	.LC23(%rip), %rsi
	jmp	.L61
.L585:
	movq	72(%rsp), %rcx
	movl	$4, %r13d
	addq	$4, %rcx
	movq	%rcx, 128(%rsp)
	jmp	.L120
.L590:
	movq	72(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 80(%rsp)
	je	.L593
	movl	(%rbx), %eax
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %rdi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jle	.L594
	cmpq	$4, 24(%rsp)
	ja	.L595
	movq	24(%rsp), %rsi
	orl	%esi, %eax
	testq	%rsi, %rsi
	movl	%eax, (%rbx)
	je	.L46
	movq	72(%rsp), %rcx
	xorl	%eax, %eax
.L135:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 24(%rsp)
	jne	.L135
	jmp	.L46
.L79:
	leaq	.LC19(%rip), %rsi
	jmp	.L61
.L104:
	leal	-12832(%rcx), %esi
	cmpl	$9, %esi
	ja	.L329
	addl	$69, %ecx
	movb	$34, 168(%rsp)
	movl	$34, %edi
	movb	%cl, 169(%rsp)
.L115:
	leaq	168(%rsp), %rsi
	jmp	.L117
.L103:
	movq	__gb2312_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L548:
	cmpq	$0, 64(%rsp)
	movl	$6, %r10d
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	64(%rsp), %rcx
	addq	$1, (%rcx)
	movq	%r9, %rcx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC32(%rip), %rsi
	jmp	.L61
.L592:
	leal	-164(%rcx), %esi
	cmpl	$93, %esi
	ja	.L329
	movq	__gb2312_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L349:
	leaq	.LC33(%rip), %rsi
	jmp	.L61
.L56:
	movq	__gb2312_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-9312(%rcx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L31:
	leaq	2(%rdi), %rsi
	cmpq	%rsi, 24(%rsp)
	je	.L596
	cltq
	movq	%rcx, %rsi
	andl	$-8, %edx
	subq	%rax, %rsi
	movq	(%rsp), %rax
	addq	%rsi, %r13
	movq	%r13, (%rax)
	movslq	%edx, %rax
	cmpq	%rax, %rcx
	jle	.L597
	cmpq	$4, %rcx
	ja	.L598
	orl	%ecx, %edx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	movl	%edx, (%rbx)
	je	.L46
	movb	%r10b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L46
.L599:
	movzbl	(%rdi,%rax), %r10d
	movb	%r10b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L599
	jmp	.L46
.L77:
	leaq	.LC22(%rip), %rsi
	jmp	.L61
.L105:
	movq	__gb2312_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-12288(%rcx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L59:
	leaq	.LC30(%rip), %rsi
	jmp	.L61
.L108:
	leaq	.LC4(%rip), %rsi
	jmp	.L61
.L111:
	leaq	.LC2(%rip), %rsi
	jmp	.L61
.L588:
	leal	-65281(%rcx), %esi
	cmpl	$93, %esi
	ja	.L329
	movq	__gb2312_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L539:
	cmpl	$363, %ecx
	je	.L68
	cmpl	$462, %ecx
	leaq	.LC28(%rip), %rsi
	je	.L61
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC27(%rip), %rsi
	jmp	.L61
.L68:
	leaq	.LC29(%rip), %rsi
	jmp	.L61
.L112:
	leaq	.LC1(%rip), %rsi
	jmp	.L61
.L85:
	leaq	.LC6(%rip), %rsi
	jmp	.L61
.L100:
	leaq	.LC7(%rip), %rsi
	jmp	.L61
.L99:
	leaq	.LC8(%rip), %rsi
	jmp	.L61
.L579:
	cmpl	$9671, %ecx
	je	.L97
	cmpl	$9675, %ecx
	leaq	.LC11(%rip), %rsi
	je	.L61
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC10(%rip), %rsi
	jmp	.L61
.L97:
	leaq	.LC12(%rip), %rsi
	jmp	.L61
.L217:
	leaq	__PRETTY_FUNCTION__.8043(%rip), %rcx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	movl	$220, %edx
	call	__assert_fail@PLT
.L93:
	leaq	.LC14(%rip), %rsi
	jmp	.L61
.L92:
	leaq	.LC15(%rip), %rsi
	jmp	.L61
.L591:
	leal	-9472(%rcx), %esi
	cmpl	$75, %esi
	ja	.L329
	addl	$36, %ecx
	movb	$41, 168(%rsp)
	movl	$41, %edi
	movb	%cl, 169(%rsp)
	jmp	.L115
.L89:
	leaq	.LC17(%rip), %rsi
	jmp	.L61
.L75:
	leaq	.LC20(%rip), %rsi
	jmp	.L61
.L74:
	movq	__gb2312_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-1025(%rcx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L87:
	leaq	.LC13(%rip), %rsi
	jmp	.L61
.L551:
	cmpq	%r15, %r10
	jne	.L233
.L232:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC34(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movq	__gb2312_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8451(%rcx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L114
.L81:
	cmpl	$8978, %ecx
	leaq	.LC18(%rip), %rsi
	je	.L61
	jmp	.L329
.L534:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC34(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L325:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC34(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L598:
	leaq	__PRETTY_FUNCTION__.9114(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC41(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L595:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC41(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L594:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L593:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L589:
	andb	$2, %r11b
	jne	.L126
.L125:
	movq	128(%rsp), %rax
	movq	72(%rsp), %rcx
	cmpq	%rcx, %rax
	je	.L121
	movl	(%rbx), %edx
	subq	%rcx, %rax
	movq	%rax, %r13
	movl	%edx, %eax
	andl	$7, %eax
	jmp	.L120
.L126:
	movq	64(%rsp), %rax
	addq	$4, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L125
.L21:
	leaq	__PRETTY_FUNCTION__.9114(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC37(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L584:
	movzbl	1(%rsi), %eax
	jmp	.L328
.L597:
	leaq	__PRETTY_FUNCTION__.9114(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC40(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L596:
	leaq	__PRETTY_FUNCTION__.9114(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC39(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L565:
	leaq	__PRETTY_FUNCTION__.9274(%rip), %rcx
	leaq	.LC34(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L550:
	leaq	__PRETTY_FUNCTION__.9114(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L44:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC42(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L540:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC38(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L41:
	leaq	__PRETTY_FUNCTION__.9199(%rip), %rcx
	leaq	.LC36(%rip), %rsi
	leaq	.LC37(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L542:
	movzbl	1(%rsi), %eax
	jmp	.L330
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8043, @object
	.size	__PRETTY_FUNCTION__.8043, 15
__PRETTY_FUNCTION__.8043:
	.string	"ucs4_to_gb2312"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9199, @object
	.size	__PRETTY_FUNCTION__.9199, 17
__PRETTY_FUNCTION__.9199:
	.string	"to_euc_cn_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9114, @object
	.size	__PRETTY_FUNCTION__.9114, 19
__PRETTY_FUNCTION__.9114:
	.string	"from_euc_cn_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9274, @object
	.size	__PRETTY_FUNCTION__.9274, 6
__PRETTY_FUNCTION__.9274:
	.string	"gconv"
