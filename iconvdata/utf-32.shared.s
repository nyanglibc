	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UTF-32//"
.LC1:
	.string	"UTF-32BE//"
.LC2:
	.string	"UTF-32LE//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	pushq	%r12
	pushq	%rbp
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	movq	24(%rdi), %rbp
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L9
	movq	32(%rbx), %r12
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L18
	movl	$1, %ebp
.L3:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L8
	movl	$1, (%rax)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %ebp
.L2:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L6
.L8:
	movl	$3, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$2, (%rax)
.L16:
	movl	%ebp, 4(%rax)
	movq	%rax, 96(%rbx)
	movabsq	$17179869188, %rax
	movq	%rax, 72(%rbx)
	movq	%rax, 80(%rbx)
	xorl	%eax, %eax
	movl	$0, 88(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L11
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L12
	leaq	.LC2(%rip), %rsi
	movq	%rbp, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L13
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L14
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$3, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$3, %ebp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$2, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$2, %ebp
	jmp	.L3
	.size	gconv_init, .-gconv_init
	.p2align 4,,15
	.globl	gconv_end
	.type	gconv_end, @function
gconv_end:
	movq	96(%rdi), %rdi
	jmp	free@PLT
	.size	gconv_end, .-gconv_end
	.section	.rodata.str1.1
.LC3:
	.string	"../iconv/skeleton.c"
.LC4:
	.string	"outbufstart == NULL"
.LC5:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC7:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC8:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC9:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC10:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC11:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC12:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC14:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$168, %rsp
	movl	16(%rsi), %r14d
	movq	%rdi, 104(%rsp)
	addq	$104, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rdi, 72(%rsp)
	leaq	48(%rsi), %rdi
	movq	%r8, 40(%rsp)
	testb	$1, %r14b
	movq	%r9, 24(%rsp)
	movl	224(%rsp), %ebx
	movq	%rdi, 80(%rsp)
	movq	$0, 64(%rsp)
	jne	.L21
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 64(%rsp)
	je	.L21
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 64(%rsp)
.L21:
	testl	%ebx, %ebx
	jne	.L285
	movq	8(%rsp), %rax
	leaq	128(%rsp), %rdx
	movl	20(%r12), %esi
	movq	(%rax), %rcx
	movq	40(%rsp), %rax
	testq	%rax, %rax
	cmove	%r12, %rax
	cmpq	$0, 24(%rsp)
	movq	(%rax), %r13
	movq	8(%r12), %rax
	movq	$0, 128(%rsp)
	movq	%rax, 16(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movq	%rax, 96(%rsp)
	movq	104(%rsp), %rax
	movq	96(%rax), %rax
	movl	(%rax), %edi
	movl	4(%rax), %eax
	cmpl	$1, %eax
	movl	%edi, 60(%rsp)
	sete	%dl
	cmpl	$2, %edi
	jne	.L28
	testb	%dl, %dl
	je	.L28
	testl	%esi, %esi
	je	.L29
.L37:
	movl	%r14d, %r11d
	andl	$4, %r11d
.L30:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L281
	movq	32(%r12), %rbx
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$7, %eax
	je	.L281
	cmpq	$0, 40(%rsp)
	jne	.L286
	cmpl	$2, 60(%rsp)
	movq	8(%rsp), %rdi
	movq	(%rdi), %rdx
	je	.L287
	cmpl	$4, %eax
	movq	%rdx, 144(%rsp)
	movq	%r13, 152(%rsp)
	ja	.L61
	leaq	136(%rsp), %r8
	cltq
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r8, 32(%rsp)
.L62:
	movzbl	4(%rbx,%rsi), %edi
	movb	%dil, (%r8,%rsi)
	addq	$1, %rsi
	cmpq	%rax, %rsi
	jne	.L62
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	addq	$4, %rdi
	cmpq	%rdi, %rbp
	jb	.L288
	leaq	4(%r13), %r8
	cmpq	%r8, 16(%rsp)
	jb	.L151
	leaq	1(%rdx), %rsi
	leaq	135(%rsp), %r9
.L70:
	movq	%rsi, 144(%rsp)
	movzbl	-1(%rsi), %edi
	addq	$1, %r15
	movq	%rsi, %r10
	addq	$1, %rsi
	cmpq	$3, %r15
	movb	%dil, (%r9,%r15)
	ja	.L185
	cmpq	%r10, %rbp
	ja	.L70
.L185:
	movl	136(%rsp), %esi
	movq	32(%rsp), %rdi
	cmpl	$1114111, %esi
	movq	%rdi, 144(%rsp)
	ja	.L289
	leal	-55296(%rsi), %edx
	cmpl	$2047, %edx
	jbe	.L290
	movl	%esi, %eax
	testl	%r11d, %r11d
	bswap	%eax
	cmovne	%eax, %esi
	movq	32(%rsp), %rax
	movl	%esi, 0(%r13)
	movl	(%rbx), %ecx
	movq	%r8, 152(%rsp)
	leaq	4(%rax), %r15
	movl	%ecx, %eax
	movq	%r15, 144(%rsp)
	movq	%r15, %rdx
	andl	$7, %eax
.L81:
	subq	32(%rsp), %rdx
	cmpq	%rax, %rdx
	movq	%rdx, %r15
	jle	.L291
	subq	%rax, %r15
	movq	8(%rsp), %rax
	andl	$-8, %ecx
	movq	152(%rsp), %r13
	movl	16(%r12), %r14d
	addq	(%rax), %r15
	movq	%r15, (%rax)
	movq	128(%rsp), %rax
	movl	%ecx, (%rbx)
	movq	%rax, 32(%rsp)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$2, 60(%rsp)
	je	.L36
	testb	%dl, %dl
	je	.L36
	movl	24(%r12), %eax
	testl	%eax, %eax
	jne	.L37
	testl	%esi, %esi
	jne	.L37
	leaq	4(%r13), %rax
	cmpq	16(%rsp), %rax
	ja	.L151
	movl	$65279, 0(%r13)
	movl	16(%r12), %r14d
	movq	%rax, %r13
	movl	%r14d, %r11d
	andl	$4, %r11d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L281:
	movq	8(%rsp), %rax
	movq	$0, 32(%rsp)
	movq	(%rax), %r15
.L42:
	leaq	152(%rsp), %rax
	movq	%rax, 112(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 120(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L85:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L91
	movq	(%rax), %rax
	addq	%rax, 32(%rsp)
.L91:
	cmpl	$2, 60(%rsp)
	je	.L292
	movl	$4, %r10d
	movq	%r15, 144(%rsp)
	movq	%r13, 152(%rsp)
	movq	%r13, %rbx
	movq	%r15, %rdx
	andl	$2, %r14d
	movl	%r10d, %eax
.L101:
	cmpq	%rdx, %rbp
	je	.L282
.L109:
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %rbp
	jb	.L160
	leaq	4(%rbx), %rdi
	cmpq	%rdi, 16(%rsp)
	jb	.L161
	movl	(%rdx), %ecx
	cmpl	$1114111, %ecx
	ja	.L293
	leal	-55296(%rcx), %r8d
	cmpl	$2047, %r8d
	jbe	.L294
	movl	%ecx, %edx
	testl	%r11d, %r11d
	bswap	%edx
	cmovne	%edx, %ecx
	movq	%rsi, %rdx
	cmpq	%rdx, %rbp
	movl	%ecx, (%rbx)
	movq	%rdi, 152(%rsp)
	movq	%rsi, 144(%rsp)
	movq	%rdi, %rbx
	jne	.L109
.L282:
	movl	%eax, %r10d
	.p2align 4,,10
	.p2align 3
.L102:
	cmpq	$0, 40(%rsp)
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
	jne	.L295
.L110:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L296
	cmpq	%rbx, %r13
	jnb	.L166
	movq	64(%rsp), %r14
	movq	(%r12), %rax
	movl	%r10d, 56(%rsp)
	movl	%r11d, 48(%rsp)
	movq	%r14, %rdi
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rax
	pushq	$0
	movq	40(%rsp), %r9
	movq	104(%rsp), %rdx
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r14
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r14d
	popq	%rdi
	movl	48(%rsp), %r11d
	movl	56(%rsp), %r10d
	je	.L114
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L297
.L113:
	testl	%r14d, %r14d
	jne	.L181
.L143:
	movq	128(%rsp), %rax
	movl	16(%r12), %r14d
	movq	(%r12), %r13
	movq	%rax, 32(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %r15
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L292:
	cmpq	%r15, %rbp
	je	.L153
	leaq	4(%r15), %rdx
	cmpq	%rdx, %rbp
	jb	.L154
	movq	16(%rsp), %r8
	leaq	4(%r13), %rcx
	movq	%r13, %rbx
	cmpq	%rcx, %r8
	jb	.L155
	movl	$4, %r10d
	andl	$2, %r14d
.L94:
	movl	-4(%rdx), %eax
	testl	%r11d, %r11d
	leaq	-4(%rdx), %rdi
	movl	%eax, %esi
	bswap	%esi
	cmovne	%esi, %eax
	leal	-55296(%rax), %esi
	cmpl	$2047, %esi
	jbe	.L186
	cmpl	$1114111, %eax
	ja	.L186
	movl	%eax, (%rbx)
	movq	%rcx, %rbx
.L98:
	cmpq	%rbp, %rdx
	je	.L102
	leaq	4(%rdx), %rax
	cmpq	%rax, %rbp
	jb	.L160
	leaq	4(%rbx), %rcx
	cmpq	%rcx, %r8
	jb	.L161
	movq	%rax, %rdx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L160:
	cmpq	$0, 40(%rsp)
	movq	8(%rsp), %rax
	movl	$7, %r10d
	movq	%rdx, (%rax)
	je	.L110
.L295:
	movq	40(%rsp), %rax
	movq	%rbx, (%rax)
.L20:
	addq	$168, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$5, %r10d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$5, %r10d
	movl	%r10d, %r14d
	jne	.L113
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L293:
	cmpq	$0, 96(%rsp)
	je	.L165
	testb	$8, 16(%r12)
	jne	.L298
.L104:
	testl	%r14d, %r14d
	jne	.L299
.L165:
	movl	$6, %r10d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L294:
	cmpq	$0, 96(%rsp)
	je	.L165
	testl	%r14d, %r14d
	je	.L165
	movq	96(%rsp), %rax
	movq	%rsi, 144(%rsp)
	movq	%rsi, %rdx
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L186:
	cmpq	$0, 96(%rsp)
	je	.L159
	testl	%r14d, %r14d
	jne	.L300
.L159:
	movq	%rdi, %rdx
	movl	$6, %r10d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rbx, (%r12)
	movq	24(%rsp), %rbx
	movq	128(%rsp), %rax
	addq	%rax, (%rbx)
.L112:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L20
	cmpl	$7, %r10d
	jne	.L20
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L145
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L147
.L146:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L146
.L147:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L166:
	movl	%r10d, %r14d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%r11d, 48(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	120(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r8
	cmpl	$6, %eax
	popq	%r9
	movq	144(%rsp), %rdx
	movq	152(%rsp), %rbx
	movl	48(%rsp), %r11d
	je	.L104
	cmpl	$5, %eax
	jne	.L101
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%esi, %esi
	jne	.L37
	cmpl	$3, %eax
	jne	.L37
	orl	$4, %r14d
	movl	$4, %r11d
	movl	%r14d, 16(%r12)
	jmp	.L30
.L287:
	cmpl	$4, %eax
	ja	.L46
	leaq	152(%rsp), %rdi
	movslq	%eax, %rcx
	xorl	%eax, %eax
.L47:
	movzbl	4(%rbx,%rax), %esi
	movb	%sil, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L47
	movq	%rdx, %rsi
	subq	%rax, %rsi
	addq	$4, %rsi
	cmpq	%rsi, %rbp
	jb	.L301
	leaq	4(%r13), %rdi
	cmpq	%rdi, 16(%rsp)
	jb	.L151
	leaq	151(%rsp), %r8
	movq	%rdx, %rax
.L53:
	addq	$1, %rax
	movzbl	-1(%rax), %esi
	addq	$1, %rcx
	cmpq	$3, %rcx
	movb	%sil, (%r8,%rcx)
	ja	.L183
	cmpq	%rax, %rbp
	ja	.L53
.L183:
	movl	152(%rsp), %eax
	testl	%r11d, %r11d
	movl	%eax, %ecx
	bswap	%ecx
	cmovne	%ecx, %eax
	leal	-55296(%rax), %ecx
	cmpl	$2047, %ecx
	jbe	.L184
	cmpl	$1114111, %eax
	ja	.L184
	movl	%eax, 0(%r13)
	movq	%rdi, %r13
.L59:
	movl	(%rbx), %eax
	movl	%eax, %ecx
	andl	$7, %ecx
	cmpl	$3, %ecx
	movslq	%ecx, %rsi
	jg	.L302
	subq	%rsi, %rdx
	movq	8(%rsp), %rdi
	andl	$-8, %eax
	leaq	4(%rdx), %r15
	movl	16(%r12), %r14d
	movq	%r15, (%rdi)
	movl	%eax, (%rbx)
	movq	128(%rsp), %rax
	movq	%rax, 32(%rsp)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$5, %r10d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L297:
	movq	24(%rsp), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L116
	movq	(%rdi), %rax
.L116:
	addq	128(%rsp), %rax
	cmpq	32(%rsp), %rax
	je	.L303
	cmpl	$2, 60(%rsp)
	movq	8(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r15, (%rax)
	je	.L304
	movq	%r15, 144(%rsp)
	movq	%r13, 152(%rsp)
	movq	%r13, %rdx
	movl	$4, %eax
	andl	$2, %ebx
.L132:
	cmpq	%r15, %rbp
	je	.L305
.L140:
	leaq	4(%r15), %rsi
	cmpq	%rsi, %rbp
	jb	.L173
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r10
	jb	.L176
	movl	(%r15), %ecx
	cmpl	$1114111, %ecx
	ja	.L306
	leal	-55296(%rcx), %r8d
	cmpl	$2047, %r8d
	jbe	.L307
	movl	%ecx, %r8d
	testl	%r11d, %r11d
	movq	%rsi, %r15
	bswap	%r8d
	cmovne	%r8d, %ecx
	cmpq	%r15, %rbp
	movl	%ecx, (%rdx)
	movq	%rdi, 152(%rsp)
	movq	%rdi, %rdx
	movq	%rsi, 144(%rsp)
	jne	.L140
.L305:
	movslq	%eax, %rcx
	movq	%rbp, %r15
.L133:
	movq	8(%rsp), %rax
	movq	136(%rsp), %r10
	movq	%r15, (%rax)
.L131:
	cmpq	%r10, %rdx
	jne	.L121
	cmpq	$5, %rcx
	jne	.L120
	cmpq	%rdx, %r13
	jne	.L113
.L124:
	subl	$1, 20(%r12)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L299:
	movq	96(%rsp), %rax
	addq	$4, %rdx
	movq	%rdx, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r13, %rbx
	movq	%r15, %rdx
	movl	$7, %r10d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L300:
	movq	96(%rsp), %rax
	movl	$6, %r10d
	addq	$1, (%rax)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%r15, %rdx
	movl	$5, %r10d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L285:
	cmpq	$0, 40(%rsp)
	jne	.L308
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L20
	movq	64(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	40(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r15
	movl	%eax, %r10d
	popq	%rax
	popq	%rdx
	jmp	.L20
.L181:
	movl	%r14d, %r10d
	jmp	.L112
.L303:
	movq	8(%rsp), %rax
	subq	%r10, %rbx
	subq	%rbx, (%rax)
	jmp	.L113
.L29:
	leaq	4(%rcx), %rax
	cmpq	%rbp, %rax
	jbe	.L31
	xorl	%r10d, %r10d
	cmpq	%rbp, %rcx
	setne	%r10b
	leal	4(%r10,%r10,2), %r10d
	jmp	.L20
.L304:
	cmpq	%r15, %rbp
	je	.L284
	addq	$4, %r15
	cmpq	%r15, %rbp
	jb	.L284
	leaq	4(%r13), %rsi
	andl	$2, %ebx
	movq	%r13, %rdx
	movl	$4, %ecx
	cmpq	%rsi, %r10
	jb	.L309
.L123:
	movl	-4(%r15), %eax
	testl	%r11d, %r11d
	leaq	-4(%r15), %r8
	movl	%eax, %edi
	bswap	%edi
	cmovne	%edi, %eax
	leal	-55296(%rax), %edi
	cmpl	$2047, %edi
	jbe	.L187
	cmpl	$1114111, %eax
	ja	.L187
	movl	%eax, (%rdx)
	movq	%rsi, %rdx
.L129:
	cmpq	%r15, %rbp
	je	.L310
	leaq	4(%r15), %rax
	cmpq	%rax, %rbp
	jb	.L169
	leaq	4(%rdx), %rsi
	cmpq	%rsi, %r10
	jb	.L170
	movq	%rax, %r15
	jmp	.L123
.L153:
	movq	%r13, %rbx
	movq	%rbp, %rdx
	movl	$4, %r10d
	jmp	.L102
.L173:
	movl	$7, %ecx
	jmp	.L133
.L312:
	movq	%r10, 48(%rsp)
	movl	%r11d, 32(%rsp)
	subq	$8, %rsp
	pushq	104(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	128(%rsp), %r9
	movq	136(%rsp), %rcx
	movq	%r12, %rsi
	movq	120(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	144(%rsp), %r15
	movq	152(%rsp), %rdx
	movl	32(%rsp), %r11d
	movq	48(%rsp), %r10
	je	.L135
	cmpl	$5, %eax
	jne	.L132
.L176:
	movl	$5, %ecx
	jmp	.L133
.L31:
	movl	(%rcx), %edx
	cmpl	$65279, %edx
	je	.L311
	cmpl	$-131072, %edx
	jne	.L37
	movq	8(%rsp), %rbx
	orl	$4, %r14d
	movl	$4, %r11d
	movl	%r14d, 16(%r12)
	movq	%rax, (%rbx)
	jmp	.L30
.L306:
	cmpq	$0, 96(%rsp)
	je	.L179
	testb	$8, 16(%r12)
	jne	.L312
.L135:
	testl	%ebx, %ebx
	jne	.L313
.L179:
	movl	$6, %ecx
	jmp	.L133
.L307:
	cmpq	$0, 96(%rsp)
	je	.L179
	testl	%ebx, %ebx
	je	.L179
	movq	96(%rsp), %rax
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r15
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L132
.L288:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%rdx, %rax
	addq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L64
	addq	$1, %rdx
	cmpq	%rsi, %rax
	jbe	.L66
.L67:
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %rdx
	movb	%cl, 4(%rbx,%r15)
	addq	$1, %r15
	cmpq	%r15, %rax
	jne	.L67
.L66:
	movl	$7, %r10d
	jmp	.L20
.L187:
	cmpq	$0, 96(%rsp)
	je	.L172
	testl	%ebx, %ebx
	jne	.L314
.L172:
	movq	%r8, %r15
	movl	$6, %ecx
.L125:
	movq	8(%rsp), %rax
	movq	%r15, (%rax)
	jmp	.L131
.L169:
	movl	$7, %ecx
	jmp	.L125
.L170:
	movl	$5, %ecx
	jmp	.L125
.L311:
	movq	8(%rsp), %rbx
	movl	%r14d, %r11d
	andl	$4, %r11d
	movq	%rax, (%rbx)
	jmp	.L30
.L301:
	movq	%rbp, %rcx
	movq	8(%rsp), %rdi
	subq	%rdx, %rcx
	addq	%rax, %rcx
	cmpq	$4, %rcx
	movq	%rbp, (%rdi)
	ja	.L49
	subq	%rax, %rdx
	cmpq	%rax, %rcx
	jbe	.L66
.L50:
	movzbl	(%rdx,%rax), %esi
	movb	%sil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L50
	jmp	.L66
.L290:
	cmpq	$0, 96(%rsp)
	je	.L77
	andl	$2, %r14d
	jne	.L315
.L77:
	movl	$6, %r10d
	jmp	.L20
.L289:
	cmpq	$0, 96(%rsp)
	je	.L77
	testb	$8, %r14b
	jne	.L316
	andl	$2, %r14d
	je	.L77
.L79:
	movq	96(%rsp), %rax
	addq	$4, 144(%rsp)
	addq	$1, (%rax)
.L78:
	movq	144(%rsp), %rdx
	cmpq	32(%rsp), %rdx
	je	.L77
.L280:
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$7, %eax
	jmp	.L81
.L284:
	cmpq	%r13, %r10
	jne	.L121
.L120:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L313:
	movq	96(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 144(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L132
.L314:
	movq	96(%rsp), %rax
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L129
.L310:
	movq	%rbp, %r15
	jmp	.L125
.L309:
	cmpq	%r13, %r10
	je	.L124
.L121:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	cmpq	$0, 96(%rsp)
	je	.L77
	andl	$2, %r14d
	je	.L77
	movq	96(%rsp), %rax
	addq	$1, (%rax)
	jmp	.L59
.L316:
	movq	32(%rsp), %rax
	movl	%r11d, 56(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %rsi
	addq	%r15, %rax
	movq	%rax, 56(%rsp)
	pushq	104(%rsp)
	movq	%rax, %r8
	movq	120(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r11
	cmpl	$6, %r10d
	popq	%rax
	movl	56(%rsp), %r11d
	je	.L317
	movq	144(%rsp), %rdx
	cmpq	32(%rsp), %rdx
	jne	.L280
	cmpl	$7, %r10d
	jne	.L86
	movq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 48(%rsp)
	je	.L318
	movl	(%rbx), %eax
	movq	%r15, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %r15
	jle	.L319
	cmpq	$4, %r15
	ja	.L320
	orl	%r15d, %eax
	testq	%r15, %r15
	movl	%eax, (%rbx)
	je	.L66
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L90:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r15
	jne	.L90
	jmp	.L66
.L315:
	movq	32(%rsp), %rdi
	leaq	4(%rdi), %rdx
	movq	96(%rsp), %rdi
	movq	%rdx, 144(%rsp)
	addq	$1, (%rdi)
	jmp	.L81
.L317:
	andb	$2, %r14b
	je	.L78
	jmp	.L79
.L86:
	testl	%r10d, %r10d
	jne	.L20
	movq	128(%rsp), %rax
	movl	16(%r12), %r14d
	movq	%rax, 32(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %r15
	jmp	.L42
.L302:
	leaq	__PRETTY_FUNCTION__.9123(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L49:
	leaq	__PRETTY_FUNCTION__.9123(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L308:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L291:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L145:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L64:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L319:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L318:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L61:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L286:
	leaq	__PRETTY_FUNCTION__.9192(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L46:
	leaq	__PRETTY_FUNCTION__.9123(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L320:
	leaq	__PRETTY_FUNCTION__.9040(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9040, @object
	.size	__PRETTY_FUNCTION__.9040, 21
__PRETTY_FUNCTION__.9040:
	.string	"to_utf32_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9123, @object
	.size	__PRETTY_FUNCTION__.9123, 23
__PRETTY_FUNCTION__.9123:
	.string	"from_utf32_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9192, @object
	.size	__PRETTY_FUNCTION__.9192, 6
__PRETTY_FUNCTION__.9192:
	.string	"gconv"
