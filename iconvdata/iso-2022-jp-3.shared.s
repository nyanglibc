	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-2022-JP-3//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$16, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$17179869185, %rdx
	movabsq	$34359738372, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$16, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$17179869188, %rdx
	movabsq	$25769803777, %rdi
	movq	$-1, 96(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdi, 80(%rax)
	movl	$1, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"(data->__statep->__count & CURRENT_SEL_MASK) == JISX0208_1983_set"
	.section	.rodata.str1.1
.LC4:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC10:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC11:
	.string	"set == JISX0208_1983_set"
.LC12:
	.string	"(jch & 0x8000) == 0"
.LC13:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC15:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rbx
	subq	$200, %rsp
	movl	16(%rsi), %r10d
	movq	%rdx, 8(%rsp)
	movq	%rbx, 72(%rsp)
	leaq	48(%rsi), %rbx
	movq	%rdi, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	%r10d, %edx
	movq	%r9, 64(%rsp)
	movl	256(%rsp), %ebp
	andl	$1, %edx
	movq	%rbx, 80(%rsp)
	movq	$0, 32(%rsp)
	jne	.L8
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rbx
	movq	%rbx, 32(%rsp)
	je	.L8
	movq	%rbx, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rsp)
.L8:
	testl	%ebp, %ebp
	jne	.L933
	movq	8(%rsp), %rax
	leaq	144(%rsp), %rdx
	movl	264(%rsp), %ebx
	movq	8(%r12), %r11
	movq	(%rax), %r15
	movq	40(%rsp), %rax
	testq	%rax, %rax
	cmove	%r12, %rax
	cmpq	$0, 64(%rsp)
	movq	(%rax), %r13
	movl	$0, %eax
	movq	$0, 144(%rsp)
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 88(%rsp)
	movq	32(%r12), %rax
	movq	%rax, 16(%rsp)
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	jne	.L31
.L874:
	movq	48(%rsp), %rax
	movq	96(%rax), %rdx
.L32:
	leaq	168(%rsp), %rax
	testq	%rdx, %rdx
	leaq	comp_table_data(%rip), %rbx
	movq	%rax, 112(%rsp)
	leaq	160(%rsp), %rax
	movq	%rax, 120(%rsp)
	movq	%r11, %rax
	movq	%r13, %r11
	movq	%rax, %r13
	je	.L934
	.p2align 4,,10
	.p2align 3
.L185:
	movl	24(%rsp), %eax
	movq	%r15, 160(%rsp)
	movq	%r11, %rbp
	movq	%r11, 168(%rsp)
	movl	$4, (%rsp)
	movl	%eax, %esi
	sarl	$6, %eax
	movl	%eax, %ecx
	andl	$56, %esi
	movq	%r15, %rax
.L220:
	cmpq	%rax, %r14
	je	.L935
.L309:
	leaq	4(%rax), %rdi
	cmpq	%rdi, %r14
	jb	.L936
	cmpq	%rbp, %r13
	jbe	.L882
	testl	%ecx, %ecx
	movl	(%rax), %edx
	je	.L224
	cmpl	$741, %edx
	je	.L554
	cmpl	$745, %edx
	je	.L555
	cmpl	$768, %edx
	je	.L556
	cmpl	$769, %edx
	je	.L557
	cmpl	$12442, %edx
	je	.L937
.L226:
	movl	%ecx, %edx
	shrl	$16, %edx
	testl	%edx, %edx
	jne	.L233
	leaq	2(%rbp), %rdx
	cmpq	%rdx, %r13
	jb	.L882
.L234:
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	%ch, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%cl, (%rax)
	xorl	%ecx, %ecx
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	cmpq	%rax, %r14
	jne	.L309
.L935:
	sall	$6, %ecx
	movq	%r14, %rax
	orl	%ecx, %esi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L224:
	testl	%esi, %esi
	jne	.L237
	cmpl	$127, %edx
	jbe	.L938
.L238:
	cmpl	$165, %edx
	je	.L939
	cmpl	$8254, %edx
	je	.L275
.L877:
	leal	-65377(%rdx), %r9d
	movl	%r9d, 96(%rsp)
.L473:
	cmpl	$62, 96(%rsp)
	ja	.L277
.L276:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %r9
	movl	%edx, %r8d
	shrl	$6, %r8d
	movswl	(%r9,%r8,2), %r8d
	testl	%r8d, %r8d
	js	.L566
	movl	%edx, %r9d
	sall	$6, %r8d
	andl	$63, %r9d
	addl	%r8d, %r9d
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %r8
	movzwl	(%r8,%r9,2), %r9d
	leal	-162(%rdx), %r8d
	cmpl	$85, %r8d
	movw	%r9w, 56(%rsp)
	movl	%r9d, 104(%rsp)
	jbe	.L940
.L280:
	leal	-913(%rdx), %r8d
	cmpl	$192, %r8d
	jbe	.L941
	cmpl	$65534, %edx
	ja	.L283
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %r8
	movzwl	2(%r8), %r9d
	cmpl	%r9d, %edx
	jbe	.L284
	.p2align 4,,10
	.p2align 3
.L285:
	addq	$6, %r8
	movzwl	2(%r8), %r9d
	cmpl	%r9d, %edx
	ja	.L285
.L284:
	movzwl	(%r8), %r9d
	cmpl	%r9d, %edx
	jnb	.L942
	.p2align 4,,10
	.p2align 3
.L283:
	movl	104(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L290
	movzwl	56(%rsp), %edi
	movl	$48, %edx
	testw	%di, %di
	js	.L291
	movl	%edi, %edx
	shrw	$8, %dx
	cmpw	$79, %dx
	je	.L292
	ja	.L293
	cmpw	$46, %dx
	je	.L294
	cmpw	$12158, %di
	sete	%dil
	cmpw	$47, %dx
	movzbl	%dil, %edi
	jne	.L570
.L298:
	cmpl	$1, %edi
	sbbl	%edx, %edx
	andl	$-16, %edx
	addl	$56, %edx
.L291:
	cmpl	%edx, %esi
	je	.L299
	leaq	4(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L881
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$36, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$40, (%rax)
	movq	168(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 168(%rsp)
	leal	-40(%rdx), %eax
	sarl	$3, %eax
	addl	$79, %eax
	movb	%al, (%rsi)
.L299:
	movzwl	56(%rsp), %eax
	testb	$-128, %al
	je	.L300
	testw	%ax, %ax
	js	.L301
	movl	%eax, %ecx
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	andl	$32639, %ecx
	movl	%edx, %esi
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L237:
	cmpl	$24, %esi
	je	.L943
	cmpl	$32, %esi
	je	.L944
	cmpl	$16, %esi
	je	.L945
	cmpl	$173759, %edx
	ja	.L238
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %r9
	movl	%edx, %r8d
	shrl	$6, %r8d
	movswl	(%r9,%r8,2), %r8d
	testl	%r8d, %r8d
	js	.L247
	movl	%edx, %r9d
	sall	$6, %r8d
	andl	$63, %r9d
	addl	%r8d, %r9d
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %r8
	movzwl	(%r8,%r9,2), %r9d
	testl	%r9d, %r9d
	movl	%r9d, %r8d
	movl	%r9d, 56(%rsp)
	je	.L247
	cmpl	$48, %esi
	sete	%r9b
	testw	%r8w, %r8w
	js	.L262
	cmpl	$56, %esi
	je	.L263
	cmpl	$40, %esi
	je	.L946
	.p2align 4,,10
	.p2align 3
.L247:
	cmpl	$127, %edx
	ja	.L238
.L243:
	leaq	3(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L881
.L961:
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$40, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$66, (%rax)
	movq	168(%rsp), %rbp
	cmpq	%rbp, %r13
	jbe	.L947
	leaq	1(%rbp), %rax
	xorl	%esi, %esi
	movq	%rax, 168(%rsp)
	movb	%dl, 0(%rbp)
.L274:
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	5(%rbp), %rdx
	cmpq	%rdx, %r13
	jb	.L882
	cmpl	$16, %esi
	jne	.L384
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$36, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	$66, (%rax)
	movq	168(%rsp), %rbp
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$11108, %edi
	movl	$1, %r8d
	xorl	%edx, %edx
.L225:
	movl	%ecx, %r9d
	addl	%edx, %r8d
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L948:
	addl	$1, %edx
	cmpl	%r8d, %edx
	je	.L226
	movl	%edx, %edi
	movzwl	(%rbx,%rdi,4), %edi
.L228:
	cmpw	%di, %r9w
	jne	.L948
	movl	%ecx, %edi
	shrl	$16, %edi
	testl	%edi, %edi
	jne	.L471
	movl	%esi, %edi
	andl	$-17, %edi
	cmpl	$40, %edi
	jne	.L471
	leaq	2(%rbp), %rdi
	cmpq	%rdi, %r13
	jnb	.L230
	.p2align 4,,10
	.p2align 3
.L882:
	sall	$6, %ecx
	movl	$5, (%rsp)
	orl	%ecx, %esi
.L222:
	movq	8(%rsp), %rcx
	cmpq	$0, 40(%rsp)
	movq	%rax, (%rcx)
	movq	16(%rsp), %rax
	movl	%esi, (%rax)
	jne	.L949
.L310:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L950
	cmpq	%rbp, %r11
	movq	%r11, 56(%rsp)
	jnb	.L575
	movq	32(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 152(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %esi
	leaq	152(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbp, %rcx
	pushq	%rsi
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	48(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%r8
	popq	%r9
	je	.L314
	movq	152(%rsp), %rax
	movq	56(%rsp), %r11
	cmpq	%rbp, %rax
	movq	%rax, (%rsp)
	jne	.L951
.L313:
	testl	%r10d, %r10d
	jne	.L602
.L459:
	movq	48(%rsp), %rax
	movl	16(%r12), %r10d
	movq	(%r12), %r11
	movq	96(%rax), %rdx
	movq	8(%rsp), %rax
	movq	(%rax), %r15
	movq	16(%rsp), %rax
	testq	%rdx, %rdx
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	jne	.L185
.L934:
	cmpq	%r15, %r14
	je	.L534
	leaq	4(%r11), %rsi
	movq	%r15, %rdx
	movl	24(%rsp), %ecx
	movq	%r11, %rbp
	cmpq	%rsi, %r13
	jb	.L536
	movl	$4, (%rsp)
	movl	$64, %r8d
	andl	$2, %r10d
.L187:
	movl	%ecx, %eax
	sarl	$6, %eax
	testl	%eax, %eax
	jne	.L952
	movzbl	(%rdx), %eax
	cmpl	$27, %eax
	movl	%eax, %edi
	je	.L953
	cmpl	$127, %eax
	jbe	.L199
.L210:
	cmpq	$0, 88(%rsp)
	je	.L553
	testl	%r10d, %r10d
	jne	.L954
.L553:
	movl	$6, (%rsp)
.L186:
	movq	8(%rsp), %rax
	cmpq	$0, 40(%rsp)
	movq	%rdx, (%rax)
	movq	16(%rsp), %rax
	movl	%ecx, (%rax)
	je	.L310
.L949:
	movq	40(%rsp), %rax
	movq	%rbp, (%rax)
.L7:
	movl	(%rsp), %eax
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	testl	%ecx, %ecx
	je	.L207
	cmpl	$32, %eax
	jbe	.L207
	cmpl	$127, %eax
	je	.L207
	cmpl	$24, %ecx
	je	.L955
	cmpl	$32, %ecx
	je	.L956
	leal	-8(%rcx), %edi
	andl	$-9, %edi
	jne	.L209
	cmpl	$32, %eax
	jle	.L210
	movq	%r14, %rdi
	subq	%rdx, %rdi
	cmpq	$1, %rdi
	jbe	.L549
	movzbl	1(%rdx), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L210
	subl	$33, %eax
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$7807, %eax
	jg	.L210
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %r9
	cltq
	leaq	2(%rdx), %rdi
	movzwl	(%r9,%rax,2), %eax
	testw	%ax, %ax
	je	.L210
	cmpl	$65533, %eax
	movq	%rdi, %rdx
	jne	.L202
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r14
	jbe	.L549
	movzbl	1(%rdx), %edi
	cmpb	$36, %dil
	je	.L957
	cmpb	$40, %dil
	jne	.L207
	movzbl	2(%rdx), %edi
	cmpb	$66, %dil
	je	.L958
	cmpb	$74, %dil
	je	.L959
	cmpb	$73, %dil
	je	.L960
	.p2align 4,,10
	.p2align 3
.L207:
	addq	$1, %rdx
.L202:
	movl	%eax, 0(%rbp)
	movq	%rsi, %rbp
.L189:
	cmpq	%rdx, %r14
	je	.L186
.L217:
	leaq	4(%rbp), %rsi
	cmpq	%rsi, %r13
	jnb	.L187
.L536:
	movl	$5, (%rsp)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L314:
	movl	(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L313
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L936:
	sall	$6, %ecx
	movl	$7, (%rsp)
	orl	%ecx, %esi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	6(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L882
	leaq	1(%rbp), %rax
	movl	$40, %esi
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	$36, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	$40, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	$79, (%rax)
	movq	168(%rsp), %rbp
.L230:
	movzwl	2(%rbx,%rdx,4), %edx
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movzbl	%dh, %eax
	movb	%al, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 168(%rsp)
	movb	%dl, (%rax)
	xorl	%ecx, %ecx
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L943:
	cmpl	$165, %edx
	je	.L558
	cmpl	$8254, %edx
	je	.L559
	cmpl	$125, %edx
	ja	.L241
	cmpl	$92, %edx
	je	.L241
	cmpl	$32, %edx
	movl	%edx, %r8d
	ja	.L242
	leaq	3(%rbp), %rdi
	cmpq	%rdi, %r13
	jnb	.L961
	.p2align 4,,10
	.p2align 3
.L881:
	movl	$5, (%rsp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L555:
	movl	$11104, %edi
	movl	$1, %r8d
	movl	$1, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L938:
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	%dl, 0(%rbp)
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L556:
	movl	$10588, %edi
	movl	$5, %r8d
	movl	$2, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L31:
	movl	24(%rsp), %eax
	andl	$7, %eax
	je	.L874
	cmpq	$0, 40(%rsp)
	jne	.L962
	movq	48(%rsp), %rbx
	movq	96(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L963
	cmpl	$4, %eax
	movq	%r15, 176(%rsp)
	movq	%r13, 184(%rsp)
	ja	.L71
	movq	16(%rsp), %rcx
	leaq	168(%rsp), %rbp
	cltq
	xorl	%ebx, %ebx
.L72:
	movzbl	4(%rcx,%rbx), %edx
	movb	%dl, 0(%rbp,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rax
	jne	.L72
	movq	%r15, %rax
	subq	%rbx, %rax
	addq	$4, %rax
	cmpq	%rax, %r14
	jb	.L964
	cmpq	%r11, %r13
	jnb	.L503
	leaq	1(%r15), %rax
	leaq	167(%rsp), %rsi
.L80:
	movq	%rax, 176(%rsp)
	movzbl	-1(%rax), %edx
	addq	$1, %rbx
	movq	%rax, %rcx
	addq	$1, %rax
	cmpq	$3, %rbx
	movb	%dl, (%rsi,%rbx)
	ja	.L613
	cmpq	%rcx, %r14
	ja	.L80
.L613:
	movl	24(%rsp), %eax
	movq	%rbp, 176(%rsp)
	movl	168(%rsp), %edx
	movl	%eax, %ecx
	sarl	$6, %eax
	andl	$56, %ecx
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L965
	testl	%ecx, %ecx
	jne	.L97
	cmpl	$127, %edx
	jbe	.L966
.L98:
	cmpl	$165, %edx
	je	.L967
	cmpl	$8254, %edx
	je	.L141
.L869:
	leal	-65377(%rdx), %r8d
.L470:
	cmpl	$62, %r8d
	ja	.L143
.L142:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rsi
	movl	%edx, %eax
	shrl	$6, %eax
	movswl	(%rsi,%rax,2), %eax
	testl	%eax, %eax
	js	.L528
	movl	%edx, %esi
	sall	$6, %eax
	andl	$63, %esi
	addl	%eax, %esi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rsi,2), %edi
	movl	%edi, %eax
.L146:
	leal	-162(%rdx), %esi
	cmpl	$85, %esi
	ja	.L147
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %r9
	leaq	(%r9,%rsi,2), %rsi
	movzbl	(%rsi), %r9d
.L148:
	testb	%r9b, %r9b
	je	.L150
	testb	$-128, %al
	movzbl	1(%rsi), %edx
	jne	.L136
	cmpl	$16, %ecx
	je	.L153
	leaq	3(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
	leaq	2(%rax), %rcx
	cmpq	%rcx, %r11
	jb	.L487
.L154:
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%r9b, (%rax)
	movq	184(%rsp), %rax
.L872:
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
.L914:
	movq	176(%rsp), %rax
.L918:
	addq	$4, %rax
	cmpq	%rbp, %rax
	movq	%rax, 176(%rsp)
	je	.L873
.L91:
	movq	16(%rsp), %rbx
	subq	%rbp, %rax
	movl	(%rbx), %ecx
	movl	%ecx, %edx
	andl	$7, %edx
	cmpq	%rdx, %rax
	jle	.L968
	movq	8(%rsp), %rbx
	subq	%rdx, %rax
	movq	184(%rsp), %r13
	addq	(%rbx), %rax
	movq	%rax, %r15
	movq	%rax, (%rbx)
	movl	%ecx, %eax
.L875:
	movq	16(%rsp), %rbx
	andl	$-8, %eax
	movl	16(%r12), %r10d
	movl	%eax, 24(%rsp)
	movl	%eax, (%rbx)
	movq	48(%rsp), %rbx
	movq	96(%rbx), %rdx
	jmp	.L32
.L244:
	cmpl	$127, %edx
	jbe	.L243
	.p2align 4,,10
	.p2align 3
.L277:
	xorl	%r8d, %r8d
	cmpl	$173759, %edx
	movl	$0, 104(%rsp)
	movw	%r8w, 56(%rsp)
	jbe	.L276
.L279:
	leal	-162(%rdx), %r8d
	cmpl	$85, %r8d
	ja	.L280
.L940:
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
.L281:
	testb	%r9b, %r9b
	je	.L283
	testb	$-128, 56(%rsp)
	jne	.L969
	cmpl	$16, %esi
	movzbl	1(%r8), %edx
	je	.L288
	leaq	3(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L881
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$36, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$66, (%rax)
	movq	168(%rsp), %rbp
.L288:
	leaq	2(%rbp), %rax
	cmpq	%rax, %r13
	jb	.L970
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	%r9b, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	%dl, (%rax)
	movl	$16, %esi
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L558:
	movl	$92, %r8d
.L240:
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
.L878:
	movb	%r8b, 0(%rbp)
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L944:
	cmpl	$165, %edx
	je	.L247
	cmpl	$8254, %edx
	je	.L247
	cmpl	$125, %edx
	ja	.L248
	cmpl	$92, %edx
	movl	%edx, %r8d
	je	.L248
.L249:
	testb	%r8b, %r8b
	jns	.L247
	leaq	1(%rbp), %rax
	addl	$-128, %r8d
	movq	%rax, 168(%rsp)
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$11064, %edi
	movl	$4, %r8d
	movl	$7, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L954:
	movq	88(%rsp), %rax
	addq	$1, %rdx
	movl	$6, (%rsp)
	addq	$1, (%rax)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L939:
	movl	$92, %edx
.L492:
	leaq	3(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L881
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$40, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$74, (%rax)
	movq	168(%rsp), %rbp
	cmpq	%rbp, %r13
	jbe	.L971
.L278:
	leaq	1(%rbp), %rax
	movl	$24, %esi
	movq	%rax, 168(%rsp)
	movb	%dl, 0(%rbp)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L950:
	movq	64(%rsp), %rbx
	movq	%rbp, (%r12)
	movq	144(%rsp), %rax
	addq	%rax, (%rbx)
.L312:
	cmpl	$7, (%rsp)
	jne	.L7
	movl	264(%rsp), %eax
	testl	%eax, %eax
	je	.L7
	movq	8(%rsp), %rax
	movq	%r14, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L462
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rcx
	je	.L464
.L463:
	movzbl	(%rdi,%rax), %esi
	movb	%sil, 4(%rcx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L463
.L464:
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	movl	(%rcx), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rcx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L575:
	movl	(%rsp), %r10d
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L937:
	movl	$9259, %edi
	movl	$14, %r8d
	movl	$11, %edx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L945:
	movq	%r13, %r8
	subq	%rbp, %r8
	cmpq	$1, %r8
	jbe	.L252
	leal	-162(%rdx), %r8d
	cmpl	$85, %r8d
	ja	.L253
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
.L254:
	testb	%r9b, %r9b
	je	.L247
	movb	%r9b, 0(%rbp)
	movzbl	1(%r8), %eax
	cmpl	$173759, %edx
	movb	%al, 1(%rbp)
	jbe	.L258
	movq	168(%rsp), %rbp
	movq	160(%rsp), %rax
.L259:
	addq	$2, %rbp
	addq	$4, %rax
	movq	%rbp, 168(%rsp)
	movq	%rax, 160(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L952:
	movl	%eax, 0(%rbp)
	andl	$63, %ecx
	movq	%rsi, %rbp
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L290:
	cmpl	$62, 96(%rsp)
	jbe	.L972
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L973
	cmpq	$0, 88(%rsp)
	je	.L574
	movq	16(%rsp), %rcx
	movl	%esi, (%rcx)
	testb	$8, 16(%r12)
	jne	.L974
.L306:
	testb	$2, %r10b
	jne	.L308
	movl	104(%rsp), %edx
	movl	$6, (%rsp)
	sall	$6, %edx
	orl	%edx, %esi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L951:
	movq	8(%rsp), %rax
	movl	24(%rsp), %esi
	movq	%r15, (%rax)
	movq	16(%rsp), %rax
	movl	%esi, (%rax)
	movq	48(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L975
	movl	16(%r12), %eax
	leaq	184(%rsp), %rsi
	movq	%r15, 176(%rsp)
	movq	%r11, 184(%rsp)
	movq	%rsi, 104(%rsp)
	leaq	176(%rsp), %rsi
	movl	%eax, 56(%rsp)
	movl	24(%rsp), %eax
	movq	%rsi, 128(%rsp)
	movl	$4, 24(%rsp)
	movl	%eax, %ebp
	sarl	$6, %eax
	movl	%eax, %r8d
	andl	$56, %ebp
	movq	%r11, %rax
.L368:
	cmpq	%r15, %r14
	je	.L976
.L457:
	leaq	4(%r15), %rcx
	cmpq	%rcx, %r14
	jb	.L977
	cmpq	%rax, (%rsp)
	jbe	.L889
	testl	%r8d, %r8d
	movl	(%r15), %edx
	je	.L372
	cmpl	$741, %edx
	je	.L580
	cmpl	$745, %edx
	je	.L581
	cmpl	$768, %edx
	je	.L582
	cmpl	$769, %edx
	je	.L583
	cmpl	$12442, %edx
	je	.L978
.L374:
	movl	%r8d, %edx
	shrl	$16, %edx
	testl	%edx, %edx
	jne	.L381
	leaq	2(%rax), %rdx
	cmpq	%rdx, (%rsp)
	jb	.L889
.L382:
	leaq	1(%rax), %rdx
	movl	%r8d, %ecx
	movq	%rdx, 184(%rsp)
	movb	%ch, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	%r8b, (%rax)
	xorl	%r8d, %r8d
	movq	176(%rsp), %r15
	movq	184(%rsp), %rax
	cmpq	%r15, %r14
	jne	.L457
.L976:
	movl	%r8d, %ecx
	movslq	24(%rsp), %rdx
	movq	%r14, %r15
	sall	$6, %ecx
	orl	%ecx, %ebp
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L275:
	cmpl	$24, %esi
	movl	$126, %edx
	je	.L278
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L941:
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
	jmp	.L281
.L300:
	movq	168(%rsp), %rbp
	leaq	1(%rbp), %rax
	cmpq	%rax, %r13
	jbe	.L979
	movq	%rax, 168(%rsp)
	movl	104(%rsp), %eax
	shrl	$8, %eax
	andl	$127, %eax
	movb	%al, 0(%rbp)
	movq	168(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 168(%rsp)
	movzbl	56(%rsp), %eax
	andl	$127, %eax
	movb	%al, (%rsi)
	movl	%edx, %esi
	jmp	.L274
.L252:
	cmpl	$173759, %edx
	ja	.L881
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %r8
	movl	%edx, %ecx
	shrl	$6, %ecx
	movswl	(%r8,%rcx,2), %ecx
	testl	%ecx, %ecx
	js	.L881
	andl	$63, %edx
	sall	$6, %ecx
	addl	%edx, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	testb	$-128, %dl
	je	.L881
.L497:
	movl	%edx, %ecx
	movq	%rdi, %rax
	movq	%rdi, 160(%rsp)
	andl	$32639, %ecx
	jmp	.L220
.L972:
	cmpl	$32, %esi
	je	.L304
	leaq	3(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L881
	leaq	1(%rbp), %rax
	movq	%rax, 168(%rsp)
	movb	$27, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$40, (%rax)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 168(%rsp)
	movb	$73, (%rax)
	movq	168(%rsp), %rbp
	cmpq	%rbp, %r13
	ja	.L304
	movq	160(%rsp), %rax
	movl	$32, %esi
	movl	$5, (%rsp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L241:
	leal	-65377(%rdx), %r9d
	cmpl	$62, %r9d
	movl	%r9d, 96(%rsp)
	ja	.L244
	leal	64(%rdx), %r8d
.L242:
	testb	%r8b, %r8b
	jns	.L240
	cmpl	$127, %edx
	jbe	.L243
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L957:
	movzbl	2(%rdx), %edi
	cmpb	$40, %dil
	je	.L980
	cmpb	$64, %dil
	je	.L981
	cmpb	$66, %dil
	jne	.L207
	addq	$3, %rdx
	movl	$16, %ecx
	jmp	.L189
.L956:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rax
	addl	$-128, %edi
	movzbl	%dil, %r9d
	movl	(%rax,%r9,4), %eax
.L929:
	testl	%eax, %eax
	sete	%r9b
	testb	%dil, %dil
	setne	%dil
	testb	%dil, %r9b
	jne	.L210
	cmpl	$65533, %eax
	jne	.L207
	jmp	.L210
.L963:
	cmpl	$4, %eax
	ja	.L36
	movq	16(%rsp), %r8
	movslq	%eax, %rbx
	leaq	184(%rsp), %rax
	movq	%rbx, %rcx
	xorl	%esi, %esi
.L37:
	movzbl	4(%r8,%rsi), %edi
	movb	%dil, (%rax,%rsi)
	addq	$1, %rsi
	cmpq	%rbx, %rsi
	jne	.L37
	leaq	4(%r13), %rbp
	cmpq	%rbp, %r11
	jb	.L503
	leaq	183(%rsp), %r9
	movq	%r15, %rsi
.L38:
	addq	$1, %rsi
	movzbl	-1(%rsi), %edi
	addq	$1, %rcx
	cmpq	$3, %rcx
	setbe	%r8b
	cmpq	%rsi, %r14
	movb	%dil, (%r9,%rcx)
	seta	%dil
	testb	%dil, %r8b
	jne	.L38
	movl	24(%rsp), %esi
	leaq	(%rax,%rcx), %r8
	sarl	$6, %esi
	testl	%esi, %esi
	jne	.L982
	movzbl	184(%rsp), %edx
	cmpl	$27, %edx
	movl	%edx, %esi
	je	.L983
	cmpl	$127, %edx
	jbe	.L984
.L60:
	cmpq	$0, 88(%rsp)
	je	.L177
	andl	$2, %r10d
	jne	.L985
.L177:
	movl	$6, (%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L975:
	cmpq	%r15, %r14
	movl	16(%r12), %r9d
	je	.L986
	leaq	4(%r11), %rcx
	andl	$2, %r9d
	cmpq	%rcx, (%rsp)
	movq	%r11, %rbp
	movl	$4, %edx
	jb	.L987
	movq	88(%rsp), %r8
	movl	24(%rsp), %esi
	movq	%r11, 56(%rsp)
.L320:
	movl	%esi, %eax
	sarl	$6, %eax
	testl	%eax, %eax
	jne	.L988
	movzbl	(%r15), %eax
	cmpl	$27, %eax
	movl	%eax, %edi
	je	.L989
	cmpl	$127, %eax
	jbe	.L335
.L352:
	testq	%r8, %r8
	je	.L891
	testl	%r9d, %r9d
	jne	.L355
.L891:
	movq	8(%rsp), %rax
	movq	(%rsp), %r10
	movq	%r15, (%rax)
	movq	16(%rsp), %rax
	cmpq	%r10, %rbp
	movl	%esi, (%rax)
	jne	.L319
.L318:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	leal	-65377(%rdx), %r9d
	cmpl	$62, %r9d
	movl	%r9d, 96(%rsp)
	ja	.L250
	leal	64(%rdx), %r8d
	jmp	.L249
.L97:
	cmpl	$24, %ecx
	je	.L990
	cmpl	$32, %ecx
	je	.L991
	cmpl	$16, %ecx
	je	.L992
	cmpl	$173759, %edx
	ja	.L98
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rsi
	movl	%edx, %eax
	shrl	$6, %eax
	movswl	(%rsi,%rax,2), %eax
	testl	%eax, %eax
	js	.L109
	movl	%edx, %esi
	sall	$6, %eax
	andl	$63, %esi
	addl	%eax, %esi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rsi,2), %edi
	testl	%edi, %edi
	movl	%edi, %eax
	je	.L109
	cmpl	$48, %ecx
	sete	%sil
	testw	%di, %di
	js	.L126
	cmpl	$56, %ecx
	je	.L127
	cmpl	$40, %ecx
	je	.L993
.L109:
	cmpl	$127, %edx
	ja	.L98
.L104:
	leaq	3(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %r11
	ja	.L872
.L487:
	movq	176(%rsp), %rax
	cmpq	%rbp, %rax
	jne	.L91
.L503:
	movl	$5, (%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L372:
	testl	%ebp, %ebp
	jne	.L385
	cmpl	$127, %edx
	jbe	.L994
	cmpl	$165, %edx
	je	.L995
.L424:
	cmpl	$8254, %edx
	je	.L610
.L883:
	leal	-65377(%rdx), %esi
	movl	%esi, 140(%rsp)
.L476:
	cmpl	$62, 140(%rsp)
	ja	.L426
.L425:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%edx, %esi
	shrl	$6, %esi
	movswl	(%rdi,%rsi,2), %esi
	testl	%esi, %esi
	js	.L592
	movl	%edx, %edi
	sall	$6, %esi
	andl	$63, %edi
	addl	%esi, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rsi
	movzwl	(%rsi,%rdi,2), %edi
	movl	%edi, %esi
	movl	%edi, 96(%rsp)
.L428:
	leal	-162(%rdx), %edi
	cmpl	$85, %edi
	ja	.L429
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	movzbl	(%rdi), %r9d
.L430:
	testb	%r9b, %r9b
	je	.L432
	testb	$-128, %sil
	jne	.L996
	cmpl	$16, %ebp
	movzbl	1(%rdi), %edx
	je	.L437
	leaq	3(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L887
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
.L437:
	leaq	2(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L997
	leaq	1(%rax), %rcx
	movl	$16, %ebp
	movq	%rcx, 184(%rsp)
	movb	%r9b, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
.L422:
	movq	176(%rsp), %rax
	leaq	4(%rax), %r15
	movq	184(%rsp), %rax
	movq	%r15, 176(%rsp)
	jmp	.L368
.L559:
	movl	$126, %r8d
	jmp	.L240
.L566:
	xorl	%r9d, %r9d
	movl	$0, 104(%rsp)
	movw	%r9w, 56(%rsp)
	jmp	.L279
.L946:
	movl	%r8d, %r9d
	shrw	$8, %r9w
	cmpw	$79, %r9w
	movw	%r9w, 96(%rsp)
	je	.L264
	ja	.L265
	cmpw	$46, %r9w
	je	.L266
	xorl	%r9d, %r9d
	cmpw	$12158, %r8w
	sete	%r9b
	cmpw	$47, 96(%rsp)
	jne	.L263
.L270:
	xorl	$1, %r9d
.L262:
	testb	%r9b, %r9b
	je	.L247
	testb	$-128, %r8b
	je	.L271
	testw	%r8w, %r8w
	jns	.L272
.L301:
	leaq	__PRETTY_FUNCTION__.9195(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	testb	$-128, %r8b
	jne	.L272
.L271:
	leaq	1(%rbp), %rdx
	cmpq	%rdx, %r13
	jbe	.L881
	movl	56(%rsp), %eax
	movq	%rdx, 168(%rsp)
	andl	$127, %r8d
	shrl	$8, %eax
	andl	$127, %eax
	movb	%al, 0(%rbp)
	movq	168(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 168(%rsp)
	movb	%r8b, (%rax)
	movq	160(%rsp), %rax
	movq	168(%rsp), %rbp
	addq	$4, %rax
	movq	%rax, 160(%rsp)
	jmp	.L220
.L947:
	movq	160(%rsp), %rax
	xorl	%esi, %esi
	movl	$5, (%rsp)
	jmp	.L222
.L955:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %r9
	cltq
	movl	(%r9,%rax,4), %eax
	jmp	.L929
.L969:
	cmpl	$16, %esi
	movl	$65536, %eax
	movq	%rdi, 160(%rsp)
	cmovne	%eax, %ecx
	movl	56(%rsp), %eax
	movl	$16, %esi
	andl	$32639, %eax
	orl	%eax, %ecx
	movq	%rdi, %rax
	jmp	.L220
.L933:
	cmpq	$0, 40(%rsp)
	jne	.L998
	cmpl	$1, %ebp
	movq	32(%r12), %r15
	jne	.L11
	movl	(%r15), %ebp
	movq	(%r12), %rcx
	testl	%ebp, %ebp
	je	.L12
	movq	48(%rsp), %rax
	movq	8(%r12), %rdi
	cmpq	$0, 96(%rax)
	je	.L999
	movl	%ebp, %edx
	xorl	%eax, %eax
	sarl	$6, %edx
	testl	%edx, %edx
	je	.L16
	movl	%ebp, %eax
	sarl	$22, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$-3, %eax
	addl	$5, %eax
.L16:
	leal	3(%rax), %r8d
	movl	%ebp, %esi
	andl	$56, %esi
	cmovne	%r8d, %eax
	cltq
	addq	%rcx, %rax
	cmpq	%rax, %rdi
	jb	.L503
	testl	%edx, %edx
	je	.L504
	movl	%edx, %eax
	shrl	$16, %eax
	testl	%eax, %eax
	je	.L505
	cmpl	$16, %esi
	jne	.L1000
	movl	$9243, %edi
	leaq	3(%rcx), %rax
	movb	$66, 2(%rcx)
	movw	%di, (%rcx)
.L19:
	movb	%dh, (%rax)
	movb	%dl, 1(%rax)
	leaq	2(%rax), %r13
	movq	32(%r12), %rdx
	movl	(%rdx), %eax
	movl	%eax, %esi
	andl	$56, %esi
.L18:
	testl	%esi, %esi
	je	.L21
	movl	$10267, %esi
	movb	$66, 2(%r13)
	addq	$3, %r13
	movw	%si, -3(%r13)
	movq	32(%r12), %rdx
	movl	(%rdx), %eax
.L21:
	andl	$7, %eax
	movl	%eax, (%rdx)
.L15:
	testb	$1, 16(%r12)
	jne	.L465
	cmpq	%rcx, %r13
	jbe	.L24
	movq	32(%rsp), %rbx
	movq	%rcx, 184(%rsp)
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	leaq	184(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%rbx
	cmpl	$4, %eax
	movl	%eax, 16(%rsp)
	popq	%r14
	popq	%rdx
	je	.L24
	cmpq	%r13, 184(%rsp)
	jne	.L1001
.L26:
	movl	(%rsp), %r13d
	testl	%r13d, %r13d
	jne	.L7
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L385:
	cmpl	$24, %ebp
	je	.L1002
	cmpl	$32, %ebp
	je	.L1003
	cmpl	$16, %ebp
	je	.L1004
	cmpl	$173759, %edx
	jbe	.L1005
.L409:
	cmpl	$165, %edx
	movl	$92, %esi
	jne	.L424
.L423:
	movq	(%rsp), %rcx
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.L887
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$74, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %rcx
	jbe	.L1006
	leaq	1(%rax), %rdx
	movl	$24, %ebp
	movq	%rdx, 184(%rsp)
	movb	%sil, (%rax)
	jmp	.L422
.L253:
	leal	-913(%rdx), %r8d
	cmpl	$192, %r8d
	ja	.L255
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
	jmp	.L254
.L272:
	movl	%r8d, %ecx
	movq	%rdi, 160(%rsp)
	movq	%rdi, %rax
	andl	$32639, %ecx
	jmp	.L220
.L209:
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r14
	movq	%rdi, 56(%rsp)
	jbe	.L549
	movl	%r8d, %r9d
	movzbl	1(%rdx), %edi
	subl	%ecx, %r9d
	sall	$5, %r9d
	addl	%r9d, %eax
	leal	-289(%rax), %r9d
	cmpl	$93, %r9d
	jbe	.L211
	cmpl	$545, %eax
	je	.L550
	leal	-547(%rax), %r9d
	cmpl	$2, %r9d
	ja	.L212
	leal	-452(%rax), %r9d
.L211:
	movl	%edi, %eax
	subl	$33, %eax
	cmpl	$93, %eax
	ja	.L214
	imull	$94, %r9d, %r9d
	addl	%eax, %r9d
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movzwl	(%rax,%r9,2), %eax
	movzbl	%ah, %edi
	movzbl	%al, %eax
	movq	%rdi, %r9
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rdi
	addl	(%rdi,%r9,4), %eax
	je	.L214
	cmpl	$65533, %eax
	je	.L214
	addq	$2, %rdx
	cmpl	$127, %eax
	ja	.L202
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rdi
	subl	$1, %eax
	movzwl	2(%rdi,%rax,4), %r9d
	movzwl	(%rdi,%rax,4), %eax
	movl	%eax, 0(%rbp)
	leaq	8(%rbp), %rax
	cmpq	%rax, %r13
	jnb	.L1007
	sall	$6, %r9d
	movq	%rsi, %rbp
	movl	$5, (%rsp)
	orl	%r9d, %ecx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L214:
	cmpq	$0, 88(%rsp)
	je	.L553
	testl	%r10d, %r10d
	je	.L553
	movq	88(%rsp), %rax
	movq	56(%rsp), %rdx
	movl	$6, (%rsp)
	addq	$1, (%rax)
	jmp	.L217
.L381:
	leaq	5(%rax), %rdx
	cmpq	%rdx, (%rsp)
	jb	.L889
	cmpl	$16, %ebp
	jne	.L384
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
	jmp	.L382
.L1022:
	movl	%r10d, 140(%rsp)
	movq	%r11, 96(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	24(%rsp), %rax
	movq	%r14, %r8
	movq	120(%rsp), %r9
	movq	144(%rsp), %rcx
	movq	%r12, %rsi
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	32(%rsp), %rsi
	movl	%eax, 40(%rsp)
	popq	%rdx
	popq	%rcx
	movl	(%rsi), %r8d
	movq	176(%rsp), %r15
	movq	96(%rsp), %r11
	movl	140(%rsp), %r10d
	movl	%r8d, %ebp
	sarl	$6, %r8d
	andl	$56, %ebp
	cmpl	$6, %eax
	movq	184(%rsp), %rax
	je	.L1008
	cmpl	$5, 24(%rsp)
	jne	.L368
.L889:
	movl	%r8d, %edx
	sall	$6, %edx
	orl	%edx, %ebp
	movl	$5, %edx
.L370:
	movq	8(%rsp), %rsi
	movq	152(%rsp), %rcx
	movq	%r15, (%rsi)
	movq	16(%rsp), %rsi
	movl	%ebp, (%rsi)
.L367:
	cmpq	%rcx, %rax
	jne	.L319
	cmpq	$5, %rdx
	jne	.L318
.L322:
	cmpq	%rax, %r11
	jne	.L313
.L321:
	subl	$1, 20(%r12)
	jmp	.L313
.L293:
	cmpw	$116, %dx
	je	.L296
	cmpw	$126, %dx
	jne	.L570
	movzwl	56(%rsp), %edi
	leal	-32378(%rdi), %edx
	xorl	%edi, %edi
	cmpw	$4, %dx
	setbe	%dil
	jmp	.L298
.L602:
	movl	%r10d, (%rsp)
	jmp	.L312
.L304:
	leaq	1(%rbp), %rax
	subl	$64, %edx
	movl	$32, %esi
	movq	%rax, 168(%rsp)
	movb	%dl, 0(%rbp)
	jmp	.L274
.L971:
	movq	160(%rsp), %rax
	movl	$24, %esi
	movl	$5, (%rsp)
	jmp	.L222
.L250:
	cmpl	$127, %edx
	jbe	.L243
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L335:
	testl	%esi, %esi
	je	.L347
	cmpl	$32, %eax
	jbe	.L347
	cmpl	$127, %eax
	je	.L347
	cmpl	$24, %esi
	je	.L1009
	cmpl	$32, %esi
	je	.L1010
	leal	-8(%rsi), %edi
	andl	$-9, %edi
	jne	.L351
	cmpl	$32, %eax
	jle	.L352
	movq	%r14, %rdi
	subq	%r15, %rdi
	cmpq	$1, %rdi
	jbe	.L891
	movzbl	1(%r15), %edi
	subl	$33, %edi
	cmpl	$93, %edi
	ja	.L352
	subl	$33, %eax
	imull	$94, %eax, %eax
	addl	%edi, %eax
	cmpl	$7807, %eax
	jg	.L352
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rdi
	cltq
	leaq	2(%r15), %r11
	movzwl	(%rdi,%rax,2), %eax
	testw	%ax, %ax
	je	.L352
	cmpl	$65533, %eax
	movq	%r11, %r15
	jne	.L340
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L347:
	addq	$1, %r15
.L340:
	movl	%eax, 0(%rbp)
	movq	%rcx, %rbp
.L324:
	cmpq	%r15, %r14
	je	.L1011
.L365:
	leaq	4(%rbp), %rcx
	cmpq	%rcx, (%rsp)
	jnb	.L320
	movq	8(%rsp), %rcx
	movq	(%rsp), %rax
	movq	56(%rsp), %r11
	movq	%r15, (%rcx)
	movq	16(%rsp), %rcx
	cmpq	%rax, %rbp
	movl	%esi, (%rcx)
	je	.L322
.L319:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	cmpl	$65534, %edx
	ja	.L247
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %r8
	movzwl	2(%r8), %r9d
	cmpl	%r9d, %edx
	jbe	.L256
.L257:
	addq	$6, %r8
	movzwl	2(%r8), %r9d
	cmpl	%r9d, %edx
	ja	.L257
.L256:
	movzwl	(%r8), %r9d
	cmpl	%r9d, %edx
	jb	.L247
	movzwl	4(%r8), %r8d
	addl	%edx, %r8d
	subl	%r9d, %r8d
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
	jmp	.L254
.L977:
	movl	%r8d, %edx
	sall	$6, %edx
	orl	%edx, %ebp
	movl	$7, %edx
	jmp	.L370
.L11:
	movq	$0, (%r15)
	testb	$1, 16(%r12)
	movl	$0, (%rsp)
	jne	.L7
	movq	32(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	pushq	%rax
	pushq	%rbp
	jmp	.L905
.L965:
	cmpl	$741, %edx
	je	.L519
	cmpl	$745, %edx
	je	.L520
	cmpl	$768, %edx
	je	.L521
	cmpl	$769, %edx
	je	.L522
	cmpl	$12442, %edx
	je	.L1012
.L84:
	movl	%edi, %eax
	shrl	$16, %eax
	testl	%eax, %eax
	jne	.L93
	leaq	2(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	movq	%r13, %rax
.L94:
	leaq	1(%rax), %rdx
	movl	%edi, %ebx
	movq	%rdx, 184(%rsp)
	movb	%bh, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	%dil, (%rax)
	movq	176(%rsp), %rax
	cmpq	%rbp, %rax
	jne	.L91
.L873:
	movq	48(%rsp), %rax
	movl	16(%r12), %r10d
	movq	96(%rax), %rdx
	movq	8(%rsp), %rax
	movq	(%rax), %r15
	movq	16(%rsp), %rax
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	jmp	.L32
.L942:
	movzwl	4(%r8), %r8d
	addl	%edx, %r8d
	subl	%r9d, %r8d
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %r9
	leaq	(%r9,%r8,2), %r8
	movzbl	(%r8), %r9d
	jmp	.L281
.L12:
	testl	%edx, %edx
	jne	.L1013
.L24:
	movq	32(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	264(%rsp), %eax
	pushq	%rax
	pushq	$1
.L905:
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	xorl	%r8d, %r8d
	movq	88(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	*%rbx
	movl	%eax, 16(%rsp)
	popq	%rbp
	popq	%r12
	jmp	.L7
.L294:
	xorl	%edi, %edi
	cmpw	$11809, 56(%rsp)
	sete	%dil
	jmp	.L298
.L296:
	xorl	%edi, %edi
	cmpw	$29735, 56(%rsp)
	sete	%dil
	jmp	.L298
.L1003:
	cmpl	$165, %edx
	je	.L395
	cmpl	$8254, %edx
	je	.L395
	cmpl	$125, %edx
	ja	.L396
	cmpl	$92, %edx
	movl	%edx, %esi
	je	.L396
.L397:
	testb	%sil, %sil
	jns	.L395
	leaq	1(%rax), %rdx
	addl	$-128, %esi
	movq	%rdx, 184(%rsp)
.L884:
	movb	%sil, (%rax)
	movq	176(%rsp), %rax
	leaq	4(%rax), %r15
	movq	184(%rsp), %rax
	movq	%r15, 176(%rsp)
	jmp	.L368
.L995:
	movl	$92, %esi
	jmp	.L423
.L355:
	addq	$1, %r15
	addq	$1, (%r8)
	movl	$6, %edx
	jmp	.L324
.L308:
	movq	88(%rsp), %rcx
	addq	$4, %rax
	movl	$6, (%rsp)
	movq	%rax, 160(%rsp)
	addq	$1, (%rcx)
	movl	104(%rsp), %ecx
	jmp	.L220
.L960:
	addq	$3, %rdx
	movl	$32, %ecx
	jmp	.L189
.L981:
	addq	$3, %rdx
	movl	$8, %ecx
	jmp	.L189
.L549:
	movl	$7, (%rsp)
	jmp	.L186
.L974:
	movl	%r10d, 96(%rsp)
	movq	%r11, 56(%rsp)
	subq	$8, %rsp
	pushq	96(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	136(%rsp), %rcx
	movq	128(%rsp), %r9
	movq	%r14, %r8
	movq	64(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movq	32(%rsp), %rsi
	movl	%eax, 16(%rsp)
	popq	%r10
	popq	%r11
	movl	(%rsi), %ecx
	movq	56(%rsp), %r11
	movl	96(%rsp), %r10d
	movq	168(%rsp), %rbp
	movl	%ecx, %esi
	sarl	$6, %ecx
	andl	$56, %esi
	cmpl	$6, %eax
	movq	160(%rsp), %rax
	je	.L1014
	cmpl	$5, (%rsp)
	jne	.L220
	sall	$6, %ecx
	orl	%ecx, %esi
	jmp	.L222
.L580:
	movl	$11108, %ecx
	movl	$1, %esi
	xorl	%edx, %edx
.L373:
	movl	%r8d, %edi
	addl	%edx, %esi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L1015:
	addl	$1, %edx
	cmpl	%edx, %esi
	je	.L374
	movl	%edx, %ecx
	movzwl	(%rbx,%rcx,4), %ecx
.L376:
	cmpw	%cx, %di
	jne	.L1015
	movl	%r8d, %ecx
	shrl	$16, %ecx
	testl	%ecx, %ecx
	jne	.L474
	movl	%ebp, %ecx
	andl	$-17, %ecx
	cmpl	$40, %ecx
	jne	.L474
	leaq	2(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L889
.L378:
	movzwl	2(%rbx,%rdx,4), %edx
	leaq	1(%rax), %rcx
	xorl	%r8d, %r8d
	movq	%rcx, 184(%rsp)
	movzbl	%dh, %ecx
	movb	%cl, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
	movq	176(%rsp), %rax
	leaq	4(%rax), %r15
	movq	184(%rsp), %rax
	movq	%r15, 176(%rsp)
	jmp	.L368
.L474:
	leaq	6(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L889
	leaq	1(%rax), %rcx
	movl	$40, %ebp
	movq	%rcx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$79, (%rax)
	movq	184(%rsp), %rax
	jmp	.L378
.L395:
	cmpl	$127, %edx
	ja	.L409
.L391:
	movq	(%rsp), %rsi
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rsi
	jb	.L887
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %rsi
	jbe	.L1016
	leaq	1(%rax), %rcx
	xorl	%ebp, %ebp
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
	jmp	.L422
.L973:
	movq	%rdi, 160(%rsp)
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	jmp	.L220
.L581:
	movl	$11104, %ecx
	movl	$1, %esi
	movl	$1, %edx
	jmp	.L373
.L994:
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
	movq	176(%rsp), %rax
	leaq	4(%rax), %r15
	movq	184(%rsp), %rax
	movq	%r15, 176(%rsp)
	jmp	.L368
.L1002:
	cmpl	$165, %edx
	je	.L584
	cmpl	$8254, %edx
	je	.L585
	cmpl	$125, %edx
	ja	.L389
	cmpl	$92, %edx
	je	.L389
	cmpl	$32, %edx
	movl	%edx, %esi
	jbe	.L391
.L390:
	testb	%sil, %sil
	jns	.L388
	cmpl	$127, %edx
	jbe	.L391
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r14, %rdx
	movl	24(%rsp), %ecx
	movq	%r11, %rbp
	movl	$4, (%rsp)
	jmp	.L186
.L570:
	movl	$40, %edx
	jmp	.L291
.L292:
	movzwl	56(%rsp), %edx
	cmpw	$20308, %dx
	sete	%dil
	cmpw	$20350, %dx
	sete	%dl
	orl	%edx, %edi
	movzbl	%dil, %edi
	jmp	.L298
.L582:
	movl	$10588, %ecx
	movl	$5, %esi
	movl	$2, %edx
	jmp	.L373
.L980:
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r14
	jbe	.L549
	movzbl	3(%rdx), %edi
	leal	-79(%rdi), %r9d
	andl	$253, %r9d
	je	.L1017
	cmpb	$80, %dil
	jne	.L207
	addq	$4, %rdx
	movl	$48, %ecx
	jmp	.L189
.L392:
	cmpl	$127, %edx
	jbe	.L391
.L426:
	xorl	%esi, %esi
	cmpl	$173759, %edx
	movl	$0, 96(%rsp)
	ja	.L428
	jmp	.L425
.L432:
	movl	96(%rsp), %edi
	testl	%edi, %edi
	je	.L439
	testw	%si, %si
	movl	$48, %edx
	js	.L440
	movl	%esi, %ecx
	shrw	$8, %cx
	cmpw	$79, %cx
	je	.L441
	jbe	.L1018
	cmpw	$116, %cx
	je	.L445
	cmpw	$126, %cx
	jne	.L596
	leal	-32378(%rsi), %edx
	cmpw	$4, %dx
	setbe	%dl
	movzbl	%dl, %edx
.L447:
	cmpl	$1, %edx
	sbbl	%edx, %edx
	andl	$-16, %edx
	addl	$56, %edx
.L440:
	cmpl	%edx, %ebp
	je	.L448
	leaq	4(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jb	.L887
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rcx
	leaq	1(%rcx), %rax
	movq	%rax, 184(%rsp)
	leal	-40(%rdx), %eax
	sarl	$3, %eax
	addl	$79, %eax
	movb	%al, (%rcx)
.L448:
	testb	$-128, %sil
	je	.L449
	testw	%si, %si
	js	.L301
	movq	176(%rsp), %rax
	movl	%esi, %r8d
	movl	%edx, %ebp
	andl	$32639, %r8d
	leaq	4(%rax), %r15
	movq	184(%rsp), %rax
	movq	%r15, 176(%rsp)
	jmp	.L368
.L1013:
	movq	%rcx, %r13
.L465:
	movq	%r13, (%r12)
	movl	$0, (%rsp)
	jmp	.L7
.L429:
	leal	-913(%rdx), %edi
	cmpl	$192, %edi
	ja	.L431
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	movzbl	(%rdi), %r9d
	jmp	.L430
.L583:
	movl	$11064, %ecx
	movl	$4, %esi
	movl	$7, %edx
	jmp	.L373
.L970:
	movq	160(%rsp), %rax
	movl	$16, %esi
	movl	$5, (%rsp)
	jmp	.L222
.L265:
	movzwl	96(%rsp), %r9d
	cmpw	$116, %r9w
	je	.L268
	cmpw	$126, %r9w
	jne	.L263
	leal	-32378(%r8), %r9d
	cmpw	$4, %r9w
	setbe	%r9b
	movzbl	%r9b, %r9d
	jmp	.L270
.L978:
	movl	$9259, %ecx
	movl	$14, %esi
	movl	$11, %edx
	jmp	.L373
.L958:
	addq	$3, %rdx
	xorl	%ecx, %ecx
	jmp	.L189
.L964:
	movq	%r14, %rdx
	movq	8(%rsp), %rax
	subq	%r15, %rdx
	addq	%rbx, %rdx
	cmpq	$4, %rdx
	movq	%r14, (%rax)
	ja	.L74
	cmpq	%rdx, %rbx
	leaq	1(%r15), %rax
	movq	16(%rsp), %rsi
	jnb	.L78
.L77:
	movq	%rax, 176(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rax
	movb	%cl, 4(%rsi,%rbx)
	addq	$1, %rbx
	cmpq	%rbx, %rdx
	jne	.L77
.L78:
	movl	$7, (%rsp)
	jmp	.L7
.L1014:
	movl	%ecx, 104(%rsp)
	jmp	.L306
.L1005:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%edx, %esi
	shrl	$6, %esi
	movswl	(%rdi,%rsi,2), %esi
	testl	%esi, %esi
	js	.L395
	movl	%edx, %edi
	sall	$6, %esi
	andl	$63, %edi
	addl	%esi, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rsi
	movzwl	(%rsi,%rdi,2), %edi
	testl	%edi, %edi
	movl	%edi, %esi
	movl	%edi, 96(%rsp)
	je	.L395
	cmpl	$48, %ebp
	sete	%dil
	testw	%si, %si
	js	.L411
	cmpl	$56, %ebp
	je	.L412
	cmpl	$40, %ebp
	jne	.L395
	movl	%esi, %r9d
	shrw	$8, %r9w
	cmpw	$79, %r9w
	je	.L413
	jbe	.L1019
	cmpw	$116, %r9w
	je	.L417
	cmpw	$126, %r9w
	jne	.L412
	leal	-32378(%rsi), %edi
	cmpw	$4, %di
	setbe	%dil
	movzbl	%dil, %edi
.L419:
	xorl	$1, %edi
.L411:
	testb	%dil, %dil
	je	.L395
	testb	$-128, %sil
	je	.L420
	testw	%si, %si
	js	.L301
.L495:
	movl	%esi, %r8d
	movq	%rcx, 176(%rsp)
	movq	%rcx, %r15
	andl	$32639, %r8d
	jmp	.L368
.L1004:
	movq	(%rsp), %rsi
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L400
	leal	-162(%rdx), %esi
	cmpl	$85, %esi
	ja	.L401
	movq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movzbl	(%rsi), %edi
.L402:
	testb	%dil, %dil
	je	.L395
	movb	%dil, (%rax)
	movzbl	1(%rsi), %ecx
	cmpl	$173759, %edx
	movb	%cl, 1(%rax)
	jbe	.L406
	movq	184(%rsp), %rax
	movq	176(%rsp), %r15
.L407:
	addq	$2, %rax
	addq	$4, %r15
	movq	%rax, 184(%rsp)
	movq	%r15, 176(%rsp)
	jmp	.L368
.L988:
	movl	%eax, 0(%rbp)
	andl	$63, %esi
	movq	%rcx, %rbp
	jmp	.L324
.L979:
	movq	160(%rsp), %rax
	movl	%edx, %esi
	movl	$5, (%rsp)
	jmp	.L222
.L439:
	cmpl	$62, 140(%rsp)
	jbe	.L1020
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L1021
	cmpq	$0, 88(%rsp)
	je	.L600
	movq	16(%rsp), %rsi
	movl	%ebp, (%rsi)
	testb	$8, 16(%r12)
	jne	.L1022
.L454:
	testb	$2, 56(%rsp)
	jne	.L456
	movl	96(%rsp), %edx
	sall	$6, %edx
	orl	%edx, %ebp
	movl	$6, %edx
	jmp	.L370
.L519:
	movl	$11108, %edx
	movl	$1, %esi
	xorl	%eax, %eax
.L83:
	movl	%edi, %r8d
	addl	%eax, %esi
	leaq	comp_table_data(%rip), %r9
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L1023:
	addl	$1, %eax
	cmpl	%esi, %eax
	je	.L84
	movl	%eax, %edx
	movzwl	(%r9,%rdx,4), %edx
.L86:
	cmpw	%r8w, %dx
	jne	.L1023
	shrl	$16, %edi
	testl	%edi, %edi
	jne	.L468
	movl	24(%rsp), %edx
	andl	$40, %edx
	cmpl	$40, %edx
	jne	.L468
	leaq	2(%r13), %rdx
	cmpq	%rdx, %r11
	jb	.L503
	movq	%r13, %rdx
.L89:
	leaq	comp_table_data(%rip), %rcx
	movzwl	2(%rcx,%rax,4), %ecx
	leaq	1(%rdx), %rax
	movq	%rax, 184(%rsp)
	movzbl	%ch, %eax
	movb	%al, (%rdx)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	%cl, (%rax)
	jmp	.L914
.L585:
	movl	$126, %esi
.L388:
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	jmp	.L884
.L584:
	movl	$92, %esi
	jmp	.L388
.L959:
	addq	$3, %rdx
	movl	$24, %ecx
	jmp	.L189
.L989:
	leaq	2(%r15), %rdi
	cmpq	%rdi, %r14
	jbe	.L891
	movzbl	1(%r15), %edi
	cmpb	$36, %dil
	je	.L1024
	cmpb	$40, %dil
	jne	.L347
	movzbl	2(%r15), %edi
	cmpb	$66, %dil
	je	.L1025
	cmpb	$74, %dil
	je	.L1026
	cmpb	$73, %dil
	jne	.L347
	addq	$3, %r15
	movl	$32, %esi
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	6(%r13), %rdx
	cmpq	%rdx, %r11
	jb	.L503
	leaq	1(%r13), %rdx
	movq	%rdx, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	$36, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rdx)
	movq	184(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	$79, (%rdx)
	movq	184(%rsp), %rdx
	jmp	.L89
.L212:
	cmpl	$552, %eax
	je	.L551
	leal	-556(%rax), %r9d
	cmpl	$3, %r9d
	ja	.L213
	leal	-457(%rax), %r9d
	jmp	.L211
.L550:
	movl	$94, %r9d
	jmp	.L211
.L93:
	leaq	5(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	cmpl	$16, %ecx
	jne	.L1027
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$36, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	movb	$66, (%rax)
	movq	184(%rsp), %rax
	jmp	.L94
.L431:
	cmpl	$65534, %edx
	ja	.L432
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rdi
	movzwl	2(%rdi), %r9d
	cmpl	%edx, %r9d
	jnb	.L433
.L434:
	addq	$6, %rdi
	movzwl	2(%rdi), %r9d
	cmpl	%r9d, %edx
	ja	.L434
.L433:
	movzwl	(%rdi), %r9d
	cmpl	%r9d, %edx
	jb	.L432
	movzwl	4(%rdi), %edi
	addl	%edx, %edi
	subl	%r9d, %edi
	movq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %r9
	leaq	(%r9,%rdi,2), %rdi
	movzbl	(%rdi), %r9d
	jmp	.L430
.L592:
	movl	$0, 96(%rsp)
	xorl	%esi, %esi
	jmp	.L428
.L268:
	xorl	%r9d, %r9d
	cmpw	$29735, %r8w
	sete	%r9b
	jmp	.L270
.L504:
	movl	%ebp, %eax
	movq	%r15, %rdx
	movq	%rcx, %r13
	jmp	.L18
.L999:
	leaq	4(%rcx), %r13
	cmpq	%rdi, %r13
	ja	.L503
	movl	%ebp, %eax
	sarl	$6, %eax
	movl	%eax, (%rcx)
	movl	$0, (%r15)
	jmp	.L15
.L266:
	xorl	%r9d, %r9d
	cmpw	$11809, %r8w
	sete	%r9b
	jmp	.L270
.L264:
	cmpw	$20308, %r8w
	sete	96(%rsp)
	cmpw	$20350, %r8w
	sete	%r9b
	orb	96(%rsp), %r9b
	movzbl	%r9b, %r9d
	jmp	.L270
.L389:
	leal	-65377(%rdx), %esi
	cmpl	$62, %esi
	movl	%esi, 140(%rsp)
	ja	.L392
	leal	64(%rdx), %esi
	jmp	.L390
.L887:
	movl	$5, %edx
	jmp	.L370
.L520:
	movl	$11104, %edx
	movl	$1, %esi
	movl	$1, %eax
	jmp	.L83
.L987:
	movq	8(%rsp), %rax
	cmpq	%r11, (%rsp)
	movl	24(%rsp), %esi
	movq	%r15, (%rax)
	movq	16(%rsp), %rax
	movl	%esi, (%rax)
	je	.L321
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L990:
	cmpl	$165, %edx
	je	.L523
	cmpl	$8254, %edx
	je	.L524
	cmpl	$125, %edx
	ja	.L102
	cmpl	$92, %edx
	je	.L102
	cmpl	$32, %edx
	movl	%edx, %eax
	jbe	.L104
.L103:
	testb	%al, %al
	jns	.L101
	cmpl	$127, %edx
	jbe	.L104
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L521:
	movl	$10588, %edx
	movl	$5, %esi
	movl	$2, %eax
	jmp	.L83
.L984:
	cmpl	$32, %edx
	jbe	.L513
	cmpl	$127, %edx
	je	.L513
	cmpl	$24, 24(%rsp)
	je	.L1028
	cmpl	$32, 24(%rsp)
	je	.L1029
	movl	24(%rsp), %edi
	subl	$8, %edi
	andl	$-9, %edi
	jne	.L54
	cmpl	$32, %edx
	jle	.L55
	cmpq	$1, %rcx
	jbe	.L42
	movzbl	185(%rsp), %ecx
	subl	$33, %ecx
	cmpl	$93, %ecx
	jbe	.L1030
.L55:
	cmpq	$0, 88(%rsp)
	je	.L177
	andb	$2, %r10b
	movq	%rax, %rcx
	je	.L177
.L485:
	movq	88(%rsp), %rsi
	addq	$1, %rcx
	addq	$1, (%rsi)
.L47:
	subq	%rax, %rcx
	cmpq	%rbx, %rcx
	jle	.L1031
	movq	8(%rsp), %rax
	subq	%rbx, %rcx
	addq	%rcx, %r15
	movq	%r15, (%rax)
	movl	24(%rsp), %eax
	jmp	.L875
.L400:
	cmpl	$173759, %edx
	ja	.L887
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rdi
	movl	%edx, %esi
	shrl	$6, %esi
	movswl	(%rdi,%rsi,2), %esi
	testl	%esi, %esi
	js	.L887
	andl	$63, %edx
	sall	$6, %esi
	addl	%edx, %esi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rsi,2), %edx
	testb	$-128, %dl
	je	.L887
.L498:
	movl	%edx, %r8d
	movq	%rcx, %r15
	movq	%rcx, 176(%rsp)
	andl	$32639, %r8d
	jmp	.L368
.L966:
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	%dl, 0(%r13)
	jmp	.L914
.L1020:
	cmpl	$32, %ebp
	je	.L452
	movq	(%rsp), %rsi
	leaq	3(%rax), %rcx
	cmpq	%rcx, %rsi
	jb	.L887
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$27, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$73, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %rsi
	jbe	.L1032
.L452:
	leaq	1(%rax), %rcx
	subl	$64, %edx
	movl	$32, %ebp
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
	jmp	.L422
.L449:
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	cmpq	%rcx, (%rsp)
	jbe	.L1033
	movq	%rcx, 184(%rsp)
	movl	96(%rsp), %ecx
	andl	$127, %esi
	movl	%edx, %ebp
	shrl	$8, %ecx
	andl	$127, %ecx
	movb	%cl, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	%sil, (%rax)
	jmp	.L422
.L996:
	cmpl	$16, %ebp
	movl	$65536, %edx
	movq	%rcx, 176(%rsp)
	cmovne	%edx, %r8d
	andl	$32639, %esi
	movq	%rcx, %r15
	orl	%esi, %r8d
	movl	$16, %ebp
	jmp	.L368
.L1016:
	movq	176(%rsp), %r15
	xorl	%ebp, %ebp
	movl	$5, %edx
	jmp	.L370
.L1009:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %r11
	cltq
	movl	(%r11,%rax,4), %eax
	testl	%eax, %eax
	sete	%r11b
	testb	%dil, %dil
	setne	%dil
	testb	%dil, %r11b
	jne	.L616
	cmpl	$65533, %eax
	jne	.L347
.L616:
	testq	%r8, %r8
	je	.L899
	testl	%r9d, %r9d
	jne	.L355
.L899:
	movq	8(%rsp), %rax
	movq	(%rsp), %r10
	movq	%r15, (%rax)
	movq	16(%rsp), %rax
	cmpq	%r10, %rbp
	movl	$24, (%rax)
	je	.L318
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L1018:
	cmpw	$46, %cx
	je	.L443
	xorl	%edx, %edx
	cmpw	$12158, %si
	sete	%dl
	cmpw	$47, %cx
	je	.L447
.L596:
	movl	$40, %edx
	jmp	.L440
.L1010:
	leal	-128(%rax), %r11d
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rax
	movzbl	%r11b, %edi
	movl	(%rax,%rdi,4), %eax
	testl	%eax, %eax
	sete	24(%rsp)
	testb	%r11b, %r11b
	setne	%dil
	testb	%dil, 24(%rsp)
	jne	.L617
	cmpl	$65533, %eax
	jne	.L347
.L617:
	testq	%r8, %r8
	je	.L897
	testl	%r9d, %r9d
	jne	.L355
.L897:
	movq	8(%rsp), %rax
	movq	(%rsp), %r10
	movq	%r15, (%rax)
	movq	16(%rsp), %rax
	cmpq	%r10, %rbp
	movl	$32, (%rax)
	je	.L318
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	1(%r15), %rdi
	cmpq	%rdi, %r14
	movq	%rdi, 24(%rsp)
	jbe	.L891
	movl	$64, %edi
	movzbl	1(%r15), %r11d
	subl	%esi, %edi
	sall	$5, %edi
	addl	%edi, %eax
	leal	-289(%rax), %edi
	cmpl	$93, %edi
	jbe	.L357
	cmpl	$545, %eax
	je	.L578
	leal	-547(%rax), %edi
	cmpl	$2, %edi
	ja	.L358
	leal	-452(%rax), %edi
.L357:
	leal	-33(%r11), %eax
	cmpl	$93, %eax
	ja	.L360
	imull	$94, %edi, %edi
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %r11
	addl	%eax, %edi
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rax
	movzwl	(%rax,%rdi,2), %eax
	movzbl	%ah, %edi
	movzbl	%al, %eax
	addl	(%r11,%rdi,4), %eax
	cmpl	$65533, %eax
	je	.L360
	testl	%eax, %eax
	je	.L360
	addq	$2, %r15
	cmpl	$127, %eax
	ja	.L340
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %r11
	subl	$1, %eax
	movzwl	2(%r11,%rax,4), %edi
	movzwl	(%r11,%rax,4), %eax
	movl	%eax, 0(%rbp)
	leaq	8(%rbp), %rax
	cmpq	%rax, (%rsp)
	jnb	.L1034
	movq	8(%rsp), %rdx
	sall	$6, %edi
	movq	(%rsp), %rax
	movq	56(%rsp), %r11
	movq	%r15, (%rdx)
	movl	%edi, %edx
	orl	%esi, %edx
	movq	16(%rsp), %rsi
	cmpq	%rax, %rcx
	movl	%edx, (%rsi)
	je	.L322
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L1024:
	movzbl	2(%r15), %edi
	cmpb	$40, %dil
	je	.L1035
	cmpb	$64, %dil
	je	.L1036
	cmpb	$66, %dil
	jne	.L347
	addq	$3, %r15
	movl	$16, %esi
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L505:
	movq	%rcx, %rax
	jmp	.L19
.L213:
	leal	-622(%rax), %r9d
	cmpl	$16, %r9d
	ja	.L214
	leal	-519(%rax), %r9d
	jmp	.L211
.L551:
	movl	$98, %r9d
	jmp	.L211
.L396:
	leal	-65377(%rdx), %esi
	cmpl	$62, %esi
	movl	%esi, 140(%rsp)
	ja	.L398
	leal	64(%rdx), %esi
	jmp	.L397
.L1019:
	cmpw	$46, %r9w
	je	.L415
	xorl	%edi, %edi
	cmpw	$12158, %si
	sete	%dil
	cmpw	$47, %r9w
	je	.L419
.L412:
	testb	$-128, %sil
	jne	.L495
.L420:
	leaq	1(%rax), %rdx
	cmpq	%rdx, (%rsp)
	jbe	.L887
	movq	%rdx, 184(%rsp)
	movl	96(%rsp), %edx
	andl	$127, %esi
	shrl	$8, %edx
	andl	$127, %edx
	movb	%dl, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 184(%rsp)
	jmp	.L884
.L522:
	movl	$11064, %edx
	movl	$4, %esi
	movl	$7, %eax
	jmp	.L83
.L985:
	movq	88(%rsp), %rsi
	leaq	1(%rax), %rcx
	addq	$1, (%rsi)
	jmp	.L47
.L983:
	leaq	2(%rax), %rdi
	cmpq	%rdi, %r8
	jbe	.L42
	movzbl	185(%rsp), %edi
	cmpb	$36, %dil
	je	.L1037
	cmpb	$40, %dil
	leaq	1(%rax), %rcx
	je	.L1038
.L46:
	movq	16(%rsp), %rbx
	movl	%edx, 0(%r13)
	movq	%rbp, %r13
	movl	(%rbx), %esi
	movl	%esi, %ebx
	movl	%esi, 24(%rsp)
	andl	$7, %ebx
	jmp	.L47
.L982:
	movq	16(%rsp), %rax
	movl	%esi, 0(%r13)
	movl	16(%r12), %r10d
	movl	(%rax), %eax
	movl	%eax, 24(%rsp)
	jmp	.L32
.L401:
	leal	-913(%rdx), %esi
	cmpl	$192, %esi
	ja	.L403
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movzbl	(%rsi), %edi
	jmp	.L402
.L991:
	cmpl	$8254, %edx
	je	.L109
	cmpl	$165, %edx
	je	.L109
	cmpl	$125, %edx
	ja	.L110
	cmpl	$92, %edx
	movl	%edx, %eax
	je	.L110
.L111:
	testb	%al, %al
	jns	.L109
	leaq	1(%r13), %rdx
	addl	$-128, %eax
	movq	%rdx, 184(%rsp)
.L912:
	movb	%al, 0(%r13)
	jmp	.L914
.L967:
	movl	$92, %edx
.L489:
	leaq	3(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$74, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %r11
	ja	.L872
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L1017:
	addq	$4, %rdx
	movl	$56, %ecx
	jmp	.L189
.L150:
	testl	%edi, %edi
	je	.L155
	testw	%ax, %ax
	movl	$48, %edx
	js	.L156
	movl	%eax, %esi
	shrw	$8, %si
	cmpw	$79, %si
	je	.L157
	jbe	.L1039
	cmpw	$116, %si
	je	.L161
	cmpw	$126, %si
	jne	.L531
	leal	-32378(%rax), %edx
	cmpw	$4, %dx
	setbe	%dl
	movzbl	%dl, %edx
.L163:
	cmpl	$1, %edx
	sbbl	%edx, %edx
	andl	$-16, %edx
	addl	$56, %edx
.L156:
	cmpl	%edx, %ecx
	je	.L164
	leaq	4(%r13), %rcx
	cmpq	%rcx, %r11
	jb	.L503
	leaq	1(%r13), %rcx
	subl	$40, %edx
	sarl	$3, %edx
	movq	%rcx, 184(%rsp)
	movb	$27, 0(%r13)
	addl	$79, %edx
	movq	184(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 184(%rsp)
	movb	$36, (%rcx)
	movq	184(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 184(%rsp)
	movb	$40, (%rcx)
	movq	184(%rsp), %rcx
	leaq	1(%rcx), %rsi
	movq	%rsi, 184(%rsp)
	movb	%dl, (%rcx)
.L164:
	testb	$-128, %al
	je	.L165
	testw	%ax, %ax
	jns	.L914
.L166:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	leal	-913(%rdx), %esi
	cmpl	$192, %esi
	ja	.L149
	movq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %r9
	leaq	(%r9,%rsi,2), %rsi
	movzbl	(%rsi), %r9d
	jmp	.L148
.L105:
	cmpl	$127, %edx
	jbe	.L104
.L143:
	xorl	%edi, %edi
	xorl	%eax, %eax
	cmpl	$173759, %edx
	ja	.L146
	jmp	.L142
.L1039:
	cmpw	$46, %si
	je	.L159
	xorl	%edx, %edx
	cmpw	$12158, %ax
	sete	%dl
	cmpw	$47, %si
	je	.L163
.L531:
	movl	$40, %edx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$62, %r8d
	jbe	.L1040
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L136
	cmpq	$0, 88(%rsp)
	je	.L177
	movq	16(%rsp), %rax
	movl	%ecx, (%rax)
	testb	$8, 16(%r12)
	jne	.L1041
	andb	$2, %r10b
	je	.L177
	movq	176(%rsp), %rdx
.L175:
	movq	88(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 176(%rsp)
.L871:
	cmpq	%rbp, %rax
	jne	.L91
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L528:
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L146
.L149:
	cmpl	$65534, %edx
	ja	.L150
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rsi
	movzwl	2(%rsi), %r9d
	cmpl	%r9d, %edx
	jbe	.L151
.L152:
	addq	$6, %rsi
	movzwl	2(%rsi), %r9d
	cmpl	%r9d, %edx
	ja	.L152
.L151:
	movzwl	(%rsi), %r9d
	cmpl	%r9d, %edx
	jb	.L150
	movzwl	4(%rsi), %esi
	addl	%edx, %esi
	subl	%r9d, %esi
	addq	%rsi, %rsi
	addq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rsi
	movzbl	(%rsi), %r9d
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L1040:
	cmpl	$32, %ecx
	je	.L533
	leaq	3(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	leaq	1(%r13), %rax
	movq	%rax, 184(%rsp)
	movb	$27, 0(%r13)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$40, (%rax)
	movq	184(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 184(%rsp)
	movb	$73, (%rax)
	movq	184(%rsp), %rax
	cmpq	%rax, %r11
	jbe	.L487
.L170:
	leaq	1(%rax), %rcx
	subl	$64, %edx
	movq	%rcx, 184(%rsp)
	movb	%dl, (%rax)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L398:
	cmpl	$127, %edx
	jbe	.L391
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L441:
	cmpw	$20308, %si
	sete	%dl
	cmpw	$20350, %si
	sete	%cl
	orl	%ecx, %edx
	movzbl	%dl, %edx
	jmp	.L447
.L1021:
	movq	%rcx, 176(%rsp)
	movq	%rcx, %r15
	xorl	%r8d, %r8d
	jmp	.L368
.L524:
	movl	$126, %eax
.L101:
	leaq	1(%r13), %rdx
	movq	%rdx, 184(%rsp)
	jmp	.L912
.L1028:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rcx
	movslq	%edx, %rdx
	movl	(%rcx,%rdx,4), %edx
.L907:
	testl	%edx, %edx
	sete	%dil
	testb	%sil, %sil
	setne	%cl
	testb	%cl, %dil
	jne	.L612
	cmpl	$65533, %edx
	leaq	1(%rax), %rcx
	sete	%sil
	testb	%sil, %sil
	je	.L46
.L612:
	cmpq	$0, 88(%rsp)
	je	.L177
	andb	$2, %r10b
	je	.L177
	movq	88(%rsp), %rbx
	leaq	1(%rax), %rcx
	addq	$1, (%rbx)
	xorl	%ebx, %ebx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	1(%rax), %rcx
	jmp	.L46
.L1011:
	movq	8(%rsp), %rax
	movq	56(%rsp), %r11
	movq	(%rsp), %rcx
	movq	%r14, (%rax)
	movq	16(%rsp), %rax
	movl	%esi, (%rax)
	movq	%rbp, %rax
	jmp	.L367
.L42:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %r8
	je	.L1042
	movq	%rcx, %rdx
	movl	24(%rsp), %r14d
	subq	%rbx, %rdx
	movq	8(%rsp), %rbx
	addq	%r15, %rdx
	andl	$-8, %r14d
	movq	%rdx, (%rbx)
	movslq	%r14d, %rdx
	cmpq	%rdx, %rcx
	jle	.L1043
	cmpq	$4, %rcx
	ja	.L1044
	movq	16(%rsp), %rbx
	orl	%ecx, %r14d
	testq	%rcx, %rcx
	movl	%r14d, (%rbx)
	je	.L78
	xorl	%edx, %edx
	jmp	.L70
.L1045:
	movzbl	(%rax,%rdx), %esi
.L70:
	movq	16(%rsp), %rbx
	movb	%sil, 4(%rbx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L1045
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L986:
	movq	8(%rsp), %rax
	movq	(%rsp), %r10
	movq	%r14, (%rax)
	movq	16(%rsp), %rax
	cmpq	%r11, %r10
	movl	%esi, (%rax)
	je	.L318
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L997:
	movq	176(%rsp), %r15
	movl	$16, %ebp
	movl	$5, %edx
	jmp	.L370
.L1035:
	leaq	3(%r15), %rdi
	cmpq	%rdi, %r14
	jbe	.L891
	movzbl	3(%r15), %edi
	leal	-79(%rdi), %r11d
	andb	$-3, %r11b
	je	.L1046
	cmpb	$80, %dil
	jne	.L347
	addq	$4, %r15
	movl	$48, %esi
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L141:
	cmpl	$24, %ecx
	jne	.L1047
	movq	%r13, %rax
	movl	$126, %edx
	jmp	.L872
.L1012:
	movl	$9259, %edx
	movl	$14, %esi
	movl	$11, %eax
	jmp	.L83
.L574:
	movl	$6, (%rsp)
	jmp	.L222
.L1007:
	movl	%r9d, 4(%rbp)
	movq	%rax, %rbp
	jmp	.L189
.L445:
	xorl	%edx, %edx
	cmpw	$29735, %si
	sete	%dl
	jmp	.L447
.L443:
	xorl	%edx, %edx
	cmpw	$11809, %si
	sete	%dl
	jmp	.L447
.L360:
	testq	%r8, %r8
	je	.L891
	testl	%r9d, %r9d
	je	.L891
	addq	$1, (%r8)
	movq	24(%rsp), %r15
	movl	$6, %edx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%r11, %rax
	subq	%r13, %rax
	cmpq	$1, %rax
	jbe	.L115
	leal	-162(%rdx), %eax
	cmpl	$85, %eax
	ja	.L116
	addq	%rax, %rax
	addq	__jisx0208_from_ucs4_lat1@GOTPCREL(%rip), %rax
	movzbl	(%rax), %esi
.L117:
	testb	%sil, %sil
	je	.L109
	movb	%sil, 0(%r13)
	movzbl	1(%rax), %eax
	cmpl	$173759, %edx
	movb	%al, 1(%r13)
	jbe	.L121
	movq	176(%rsp), %rax
.L122:
	addq	$2, 184(%rsp)
	jmp	.L918
.L403:
	cmpl	$65534, %edx
	ja	.L395
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edx, %edi
	jnb	.L404
.L405:
	addq	$6, %rsi
	movzwl	2(%rsi), %edi
	cmpl	%edi, %edx
	ja	.L405
.L404:
	movzwl	(%rsi), %edi
	cmpl	%edi, %edx
	jb	.L395
	movzwl	4(%rsi), %esi
	addl	%edx, %esi
	subl	%edi, %esi
	addq	%rsi, %rsi
	addq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rsi
	movzbl	(%rsi), %edi
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	176(%rsp), %r15
	movl	$24, %ebp
	movl	$5, %edx
	jmp	.L370
.L523:
	movl	$92, %eax
	jmp	.L101
.L102:
	leal	-65377(%rdx), %r8d
	cmpl	$62, %r8d
	ja	.L105
	leal	64(%rdx), %eax
	jmp	.L103
.L161:
	xorl	%edx, %edx
	cmpw	$29735, %ax
	sete	%dl
	jmp	.L163
.L159:
	xorl	%edx, %edx
	cmpw	$11809, %ax
	sete	%dl
	jmp	.L163
.L54:
	leaq	1(%rax), %rdi
	cmpq	%rdi, %r8
	jbe	.L42
	movl	$64, %ecx
	subl	24(%rsp), %ecx
	movzbl	185(%rsp), %esi
	sall	$5, %ecx
	addl	%edx, %ecx
	leal	-289(%rcx), %edx
	cmpl	$93, %edx
	jbe	.L57
	cmpl	$545, %ecx
	je	.L516
	leal	-547(%rcx), %edx
	cmpl	$2, %edx
	ja	.L58
	leal	-452(%rcx), %edx
.L57:
	subl	$33, %esi
	cmpl	$93, %esi
	ja	.L60
	imull	$94, %edx, %edx
	leal	(%rdx,%rsi), %ecx
	movq	__jisx0213_to_ucs_main@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	movq	__jisx0213_to_ucs_pagestart@GOTPCREL(%rip), %rcx
	movzbl	%dh, %esi
	movzbl	%dl, %edx
	addl	(%rcx,%rsi,4), %edx
	cmpl	$65533, %edx
	je	.L60
	testl	%edx, %edx
	je	.L60
	cmpl	$127, %edx
	ja	.L518
	movq	__jisx0213_to_ucs_combining@GOTPCREL(%rip), %rcx
	subl	$1, %edx
	movzwl	2(%rcx,%rdx,4), %esi
	movzwl	(%rcx,%rdx,4), %edx
	movl	%edx, 0(%r13)
	leaq	8(%r13), %rdx
	cmpq	%rdx, %r11
	jnb	.L63
	movq	16(%rsp), %rbx
	movq	%rbp, %r13
	leaq	2(%rax), %rcx
	movl	(%rbx), %esi
	movl	%esi, %ebx
	movl	%esi, 24(%rsp)
	andl	$7, %ebx
	jmp	.L47
.L1029:
	movq	__jisx0201_to_ucs4@GOTPCREL(%rip), %rdx
	addl	$-128, %esi
	movzbl	%sil, %ecx
	movl	(%rdx,%rcx,4), %edx
	jmp	.L907
.L63:
	movq	16(%rsp), %rbx
	movl	%esi, 4(%r13)
	leaq	2(%rax), %rcx
	movq	%rdx, %r13
	movl	(%rbx), %esi
	movl	%esi, %ebx
	movl	%esi, 24(%rsp)
	andl	$7, %ebx
	jmp	.L47
.L518:
	leaq	2(%rax), %rcx
	jmp	.L46
.L58:
	cmpl	$552, %ecx
	je	.L517
	leal	-556(%rcx), %edx
	cmpl	$3, %edx
	ja	.L59
	leal	-457(%rcx), %edx
	jmp	.L57
.L516:
	movl	$94, %edx
	jmp	.L57
.L59:
	leal	-622(%rcx), %edx
	cmpl	$16, %edx
	ja	.L60
	leal	-519(%rcx), %edx
	jmp	.L57
.L517:
	movl	$98, %edx
	jmp	.L57
.L600:
	movl	$6, %edx
	jmp	.L370
.L456:
	movq	88(%rsp), %rsi
	addq	$4, %r15
	movl	96(%rsp), %r8d
	movq	%r15, 176(%rsp)
	movl	$6, 24(%rsp)
	addq	$1, (%rsi)
	jmp	.L368
.L127:
	testb	$-128, %al
	je	.L135
.L136:
	leaq	4(%rbp), %rax
	movq	%rax, 176(%rsp)
	jmp	.L91
.L135:
	leaq	1(%r13), %rdx
	cmpq	%rdx, %r11
	jbe	.L503
	shrl	$8, %edi
	movq	%rdx, 184(%rsp)
	movl	%edi, %edx
	andl	$127, %edx
	movb	%dl, 0(%r13)
.L931:
	movq	184(%rsp), %rdx
	andl	$127, %eax
	leaq	1(%rdx), %rcx
	movq	%rcx, 184(%rsp)
	movb	%al, (%rdx)
	jmp	.L914
.L993:
	movl	%edi, %esi
	shrw	$8, %si
	cmpw	$79, %si
	je	.L128
	ja	.L129
	cmpw	$46, %si
	je	.L130
	cmpw	$47, %si
	jne	.L127
	xorl	%esi, %esi
	cmpw	$12158, %di
	sete	%sil
.L134:
	xorl	$1, %esi
.L126:
	testb	%sil, %sil
	je	.L109
	testb	$-128, %al
	je	.L135
	testw	%ax, %ax
	jns	.L136
	jmp	.L166
.L36:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L406:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rsi
	movl	%edx, %ecx
	movq	176(%rsp), %r15
	shrl	$6, %ecx
	movq	184(%rsp), %rax
	movswl	(%rsi,%rcx,2), %ecx
	testl	%ecx, %ecx
	js	.L407
	andl	$63, %edx
	sall	$6, %ecx
	addl	%edx, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	leaq	4(%r15), %rcx
	testb	$-128, %dl
	jne	.L498
	jmp	.L407
.L153:
	leaq	2(%r13), %rax
	cmpq	%rax, %r11
	jb	.L503
	movq	%r13, %rax
	jmp	.L154
.L1031:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L533:
	movq	%r13, %rax
	jmp	.L170
.L110:
	leal	-65377(%rdx), %r8d
	cmpl	$62, %r8d
	ja	.L112
	leal	64(%rdx), %eax
	jmp	.L111
.L415:
	xorl	%edi, %edi
	cmpw	$11809, %si
	sete	%dil
	jmp	.L419
.L112:
	cmpl	$127, %edx
	jbe	.L104
	jmp	.L470
.L610:
	movl	$126, %esi
	jmp	.L423
.L413:
	cmpw	$20308, %si
	sete	%r9b
	cmpw	$20350, %si
	sete	%dil
	orl	%r9d, %edi
	movzbl	%dil, %edi
	jmp	.L419
.L417:
	xorl	%edi, %edi
	cmpw	$29735, %si
	sete	%dil
	jmp	.L419
.L1046:
	addq	$4, %r15
	movl	$56, %esi
	jmp	.L324
.L1038:
	movzbl	186(%rsp), %edx
	leal	-73(%rdx), %ecx
	cmpb	$1, %cl
	jbe	.L511
	cmpb	$66, %dl
	je	.L511
.L48:
	movq	16(%rsp), %rbx
	movl	$27, 0(%r13)
	leaq	1(%rax), %rcx
	movq	%rbp, %r13
	movl	(%rbx), %esi
	movl	%esi, %ebx
	movl	%esi, 24(%rsp)
	andl	$7, %ebx
	jmp	.L47
.L1037:
	movzbl	186(%rsp), %edi
	cmpb	$40, %dil
	jne	.L45
	leaq	3(%rax), %rdx
	cmpq	%rdx, %r8
	jbe	.L42
.L45:
	movl	%edi, %edx
	andl	$-3, %edx
	cmpb	$64, %dl
	je	.L511
	cmpb	$40, %dil
	jne	.L48
	movzbl	187(%rsp), %esi
	leaq	4(%rax), %rcx
	leal	-79(%rsi), %edx
	cmpb	$2, %dl
	ja	.L48
	jmp	.L47
.L511:
	leaq	3(%rax), %rcx
	jmp	.L47
.L74:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L1047:
	movl	$126, %edx
	jmp	.L489
.L121:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rcx
	movl	%edx, %eax
	shrl	$6, %eax
	movswl	(%rcx,%rax,2), %ecx
	movq	176(%rsp), %rax
	testl	%ecx, %ecx
	js	.L122
	andl	$63, %edx
	sall	$6, %ecx
	addl	%edx, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	testb	$-128, (%rdx,%rcx,2)
	jne	.L918
	jmp	.L122
.L1008:
	movl	%r8d, 96(%rsp)
	jmp	.L454
.L116:
	leal	-913(%rdx), %eax
	cmpl	$192, %eax
	ja	.L118
	addq	%rax, %rax
	addq	__jisx0208_from_ucs4_greek@GOTPCREL(%rip), %rax
	movzbl	(%rax), %esi
	jmp	.L117
.L115:
	cmpl	$173759, %edx
	ja	.L503
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %rcx
	movl	%edx, %eax
	shrl	$6, %eax
	movswl	(%rcx,%rax,2), %ecx
	movq	%rbp, %rax
	testl	%ecx, %ecx
	js	.L487
	andl	$63, %edx
	sall	$6, %ecx
	addl	%edx, %ecx
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	testb	$-128, (%rdx,%rcx,2)
	jne	.L918
	jmp	.L487
.L118:
	cmpl	$65534, %edx
	ja	.L109
	movq	__jisx0208_from_ucs_idx@GOTPCREL(%rip), %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	jbe	.L119
.L120:
	addq	$6, %rax
	movzwl	2(%rax), %esi
	cmpl	%esi, %edx
	ja	.L120
.L119:
	movzwl	(%rax), %esi
	cmpl	%esi, %edx
	jb	.L109
	movzwl	4(%rax), %eax
	addl	%edx, %eax
	subl	%esi, %eax
	addq	%rax, %rax
	addq	__jisx0208_from_ucs_tab@GOTPCREL(%rip), %rax
	movzbl	(%rax), %esi
	jmp	.L117
.L157:
	cmpw	$20308, %ax
	sete	%dl
	cmpw	$20350, %ax
	sete	%sil
	orl	%esi, %edx
	movzbl	%dl, %edx
	jmp	.L163
.L1041:
	leaq	0(%rbp,%rbx), %rax
	movl	%r10d, 96(%rsp)
	movq	%r11, 56(%rsp)
	leaq	176(%rsp), %rcx
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%rax, 32(%rsp)
	pushq	96(%rsp)
	movq	%rax, %r8
	movq	64(%rsp), %rdi
	movq	%r12, %rsi
	leaq	200(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 16(%rsp)
	cmpl	$6, %eax
	popq	%r9
	popq	%r10
	movq	56(%rsp), %r11
	movl	96(%rsp), %r10d
	movq	176(%rsp), %rax
	je	.L173
	cmpq	%rbp, %rax
	jne	.L91
	cmpl	$7, (%rsp)
	jne	.L180
	leaq	4(%rbp), %rax
	cmpq	%rax, 24(%rsp)
	je	.L1048
	movq	16(%rsp), %rax
	movq	%rbx, %rsi
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
	movq	8(%rsp), %rsi
	addq	%rdx, (%rsi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rbx
	jle	.L1049
	cmpq	$4, %rbx
	ja	.L1050
	movq	16(%rsp), %rsi
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%rsi)
	je	.L78
	xorl	%eax, %eax
.L184:
	movzbl	0(%rbp,%rax), %edx
	movq	16(%rsp), %rsi
	movb	%dl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L184
	jmp	.L78
.L1050:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L1049:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1048:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L180:
	cmpl	$0, (%rsp)
	je	.L873
	jmp	.L7
.L173:
	andb	$2, %r10b
	movq	%rax, %rdx
	je	.L871
	jmp	.L175
.L165:
	movq	184(%rsp), %rcx
	leaq	1(%rcx), %rdx
	cmpq	%rdx, %r11
	jbe	.L487
	shrl	$8, %edi
	movq	%rdx, 184(%rsp)
	movl	%edi, %edx
	andl	$127, %edx
	movb	%dl, (%rcx)
	jmp	.L931
.L358:
	cmpl	$552, %eax
	je	.L579
	leal	-556(%rax), %edi
	cmpl	$3, %edi
	ja	.L359
	leal	-457(%rax), %edi
	jmp	.L357
.L578:
	movl	$94, %edi
	jmp	.L357
.L359:
	leal	-622(%rax), %edi
	cmpl	$16, %edi
	ja	.L360
	leal	-519(%rax), %edi
	jmp	.L357
.L579:
	movl	$98, %edi
	jmp	.L357
.L1025:
	addq	$3, %r15
	xorl	%esi, %esi
	jmp	.L324
.L1000:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$441, %edx
	call	__assert_fail@PLT
.L384:
	leaq	__PRETTY_FUNCTION__.9195(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$336, %edx
	call	__assert_fail@PLT
.L962:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L258:
	movq	__jisx0213_from_ucs_level1@GOTPCREL(%rip), %r8
	movl	%edx, %edi
	movq	160(%rsp), %rax
	shrl	$6, %edi
	movq	168(%rsp), %rbp
	movswl	(%r8,%rdi,2), %edi
	testl	%edi, %edi
	js	.L259
	andl	$63, %edx
	sall	$6, %edi
	addl	%edx, %edi
	movq	__jisx0213_from_ucs_level2@GOTPCREL(%rip), %rdx
	movzwl	(%rdx,%rdi,2), %edx
	leaq	4(%rax), %rdi
	testb	$-128, %dl
	jne	.L497
	jmp	.L259
.L1030:
	subl	$33, %edx
	imull	$94, %edx, %edx
	addl	%ecx, %edx
	cmpl	$7807, %edx
	jg	.L55
	movq	__jis0208_to_ucs@GOTPCREL(%rip), %rcx
	movslq	%edx, %rdx
	movzwl	(%rcx,%rdx,2), %edx
	testw	%dx, %dx
	je	.L55
	cmpl	$65533, %edx
	jne	.L518
	cmpq	$0, 88(%rsp)
	je	.L1051
	andb	$2, %r10b
	leaq	2(%rax), %rcx
	je	.L47
	jmp	.L485
.L462:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L1051:
	leaq	2(%rax), %rcx
	jmp	.L47
.L1032:
	movq	176(%rsp), %r15
	movl	$32, %ebp
	movl	$5, %edx
	jmp	.L370
.L1001:
	movl	%ebp, (%r15)
	jmp	.L26
.L968:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L71:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L998:
	leaq	__PRETTY_FUNCTION__.9315(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L1033:
	movl	%edx, %ebp
	movq	176(%rsp), %r15
	movl	$5, %edx
	jmp	.L370
.L1036:
	addq	$3, %r15
	movl	$8, %esi
	jmp	.L324
.L1034:
	movl	%edi, 4(%rbp)
	movq	%rax, %rbp
	jmp	.L324
.L1026:
	addq	$3, %r15
	movl	$24, %esi
	jmp	.L324
.L1027:
	leaq	__PRETTY_FUNCTION__.9225(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$448, %edx
	call	__assert_fail@PLT
.L1044:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L130:
	xorl	%esi, %esi
	cmpw	$11809, %di
	sete	%sil
	jmp	.L134
.L129:
	cmpw	$116, %si
	je	.L132
	cmpw	$126, %si
	jne	.L127
	leal	-32378(%rdi), %esi
	cmpw	$4, %si
	setbe	%sil
	movzbl	%sil, %esi
	jmp	.L134
.L128:
	cmpw	$20308, %di
	sete	%sil
	cmpw	$20350, %di
	sete	%r8b
	orl	%r8d, %esi
	movzbl	%sil, %esi
	jmp	.L134
.L132:
	xorl	%esi, %esi
	cmpw	$29735, %di
	sete	%sil
	jmp	.L134
.L1043:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L1042:
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9195, @object
	.size	__PRETTY_FUNCTION__.9195, 19
__PRETTY_FUNCTION__.9195:
	.string	"to_iso2022jp3_loop"
	.align 16
	.type	__PRETTY_FUNCTION__.9225, @object
	.size	__PRETTY_FUNCTION__.9225, 26
__PRETTY_FUNCTION__.9225:
	.string	"to_iso2022jp3_loop_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9117, @object
	.size	__PRETTY_FUNCTION__.9117, 28
__PRETTY_FUNCTION__.9117:
	.string	"from_iso2022jp3_loop_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9315, @object
	.size	__PRETTY_FUNCTION__.9315, 6
__PRETTY_FUNCTION__.9315:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	comp_table_data, @object
	.size	comp_table_data, 100
comp_table_data:
	.value	11108
	.value	11109
	.value	11104
	.value	11110
	.value	10588
	.value	11076
	.value	11064
	.value	11080
	.value	11063
	.value	11082
	.value	11056
	.value	11084
	.value	11075
	.value	11086
	.value	11064
	.value	11081
	.value	11063
	.value	11083
	.value	11056
	.value	11085
	.value	11075
	.value	11087
	.value	9259
	.value	9335
	.value	9261
	.value	9336
	.value	9263
	.value	9337
	.value	9265
	.value	9338
	.value	9267
	.value	9339
	.value	9515
	.value	9591
	.value	9517
	.value	9592
	.value	9519
	.value	9593
	.value	9521
	.value	9594
	.value	9523
	.value	9595
	.value	9531
	.value	9596
	.value	9540
	.value	9597
	.value	9544
	.value	9598
	.value	9845
	.value	9848
