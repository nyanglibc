	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	testb	%sil, %sil
	movl	$-1, %eax
	js	.L1
	movzbl	%sil, %eax
	cmpb	$92, %sil
	movl	$8361, %edx
	cmove	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.p2align 4,,15
	.type	johab_sym_hanja_to_ucs, @function
johab_sym_hanja_to_ucs:
	imulq	$188, %rsi, %rsi
	cmpq	$145, %rdx
	sbbq	%rax, %rax
	andq	$-18, %rax
	subq	%rax, %rsi
	cmpq	$57086, %rdi
	ja	.L7
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rax
	leaq	-40863(%rsi,%rdx), %rdx
	movzwl	(%rax,%rdx,2), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rax
	leaq	-42179(%rsi,%rdx), %rdx
	movzwl	(%rax,%rdx,2), %eax
	ret
	.size	johab_sym_hanja_to_ucs, .-johab_sym_hanja_to_ucs
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"JOHAB//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L14
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	32(%rax), %rsi
	movl	$8, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L17
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movq	%rdi, 32(%rsp)
	leaq	48(%r13), %rdi
	movq	%rdx, 8(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r9, 40(%rsp)
	movq	%rdi, 64(%rsp)
	movl	16(%r13), %edi
	movl	208(%rsp), %ebx
	movq	%rsi, 56(%rsp)
	movq	$0, 48(%rsp)
	movl	%edi, 24(%rsp)
	andl	$1, %edi
	jne	.L19
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rdi
	movq	%rdi, 48(%rsp)
	je	.L19
	movq	%rdi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 48(%rsp)
.L19:
	testl	%ebx, %ebx
	jne	.L592
	movq	8(%rsp), %rax
	movq	16(%rsp), %rsi
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %edi
	movq	8(%r13), %r14
	testq	%rsi, %rsi
	movq	(%rax), %rbx
	movq	%rsi, %rax
	cmove	%r13, %rax
	cmpq	$0, 40(%rsp)
	movq	(%rax), %r11
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	testl	%edi, %edi
	movq	%rax, 72(%rsp)
	jne	.L593
.L26:
	movq	32(%rsp), %rax
	movq	%r14, (%rsp)
	movq	%r11, %r14
	movq	%rbx, %r11
	cmpq	$0, 96(%rax)
	je	.L594
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	final_to_bit(%rip), %rbx
	movq	%r11, 128(%rsp)
	movq	%r14, 136(%rsp)
	movq	%r14, %r12
	movq	%r11, %rax
	movl	$4, %r15d
.L146:
	cmpq	%rax, %rbp
	je	.L147
.L179:
	leaq	4(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L297
	cmpq	%r12, (%rsp)
	jbe	.L305
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L148
	cmpl	$92, %edx
	je	.L148
	leaq	1(%r12), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%r12)
	movq	136(%rsp), %r12
.L149:
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L179
	.p2align 4,,10
	.p2align 3
.L147:
	cmpq	$0, 16(%rsp)
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jne	.L595
.L180:
	addl	$1, 20(%r13)
	testb	$1, 16(%r13)
	jne	.L596
	cmpq	%r12, %r14
	movq	%r11, 24(%rsp)
	jnb	.L308
	movq	48(%rsp), %rbx
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	pushq	%rax
	pushq	$0
	movq	56(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%rbx
	cmpl	$4, %eax
	movl	%eax, %ebx
	popq	%r8
	popq	%r9
	je	.L184
	movq	120(%rsp), %r15
	movq	24(%rsp), %r11
	cmpq	%r12, %r15
	jne	.L597
.L183:
	testl	%ebx, %ebx
	jne	.L337
.L251:
	movq	8(%rsp), %rax
	movq	0(%r13), %r14
	movq	(%rax), %r11
	movl	16(%r13), %eax
	movl	%eax, 24(%rsp)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L121
.L594:
	cmpq	%r11, %rbp
	je	.L282
	leaq	4(%r14), %r8
	cmpq	%r8, (%rsp)
	movq	%r11, %rcx
	movq	%r14, %r12
	jb	.L573
	movl	24(%rsp), %r9d
	movl	$4, %r15d
	movl	$8361, %r10d
	movl	%r15d, 24(%rsp)
	andl	$2, %r9d
	movl	%r9d, %r15d
	movq	%r14, %r9
.L123:
	movzbl	(%rcx), %eax
	cmpl	$127, %eax
	movq	%rax, %rsi
	ja	.L124
	cmpl	$92, %eax
	cmove	%r10d, %eax
	addq	$1, %rcx
.L126:
	movl	%eax, (%r12)
	movq	%r8, %r12
.L130:
	cmpq	%rcx, %rbp
	je	.L598
.L143:
	leaq	4(%r12), %r8
	cmpq	%r8, (%rsp)
	jnb	.L123
	movq	%r9, %r14
.L573:
	movl	$5, %r15d
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L148:
	leal	-44032(%rdx), %ecx
	cmpl	$11171, %ecx
	ja	.L150
	leaq	2(%r12), %rdx
	cmpq	%rdx, (%rsp)
	jb	.L305
	movl	%ecx, %esi
	movl	$613566757, %edx
	shrl	$2, %esi
	movl	%esi, %eax
	mull	%edx
	movl	%edx, %esi
	movl	$818089009, %edx
	movl	%esi, %eax
	movl	%esi, %edi
	imull	%edx
	imull	$28, %esi, %esi
	shrl	$2, %edx
	leal	(%rdx,%rdx,4), %eax
	leal	(%rdx,%rax,4), %eax
	movl	$-555131827, %edx
	subl	%eax, %edi
	movl	%ecx, %eax
	subl	%esi, %ecx
	mull	%edx
	leaq	init_to_bit(%rip), %rax
	shrl	$9, %edx
	movl	(%rax,%rdx,4), %eax
	leaq	mid_to_bit(%rip), %rdx
	addl	(%rdx,%rdi,4), %eax
	leaq	1(%r12), %rdx
	addl	(%rbx,%rcx,4), %eax
	movq	%rdx, 136(%rsp)
	movzbl	%ah, %ecx
	movb	%cl, (%r12)
	movq	136(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	movq	136(%rsp), %r12
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	leal	-12593(%rdx), %ecx
	cmpl	$50, %ecx
	ja	.L151
	leaq	jamo_from_ucs_table(%rip), %rsi
	movzwl	(%rsi,%rcx,2), %esi
	leaq	2(%r12), %rcx
	cmpq	%rcx, (%rsp)
	movl	%esi, %edx
	jb	.L305
	leaq	1(%r12), %rax
	movq	%rax, 136(%rsp)
	movzbl	%dh, %eax
	movb	%al, (%r12)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%sil, (%rax)
	movq	136(%rsp), %r12
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$5, %r15d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L124:
	leal	124(%rax), %edx
	cmpb	$117, %dl
	ja	.L127
	cmpl	$223, %eax
	je	.L127
	cmpl	$211, %eax
	ja	.L599
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %rbp
	movq	%rsi, 96(%rsp)
	jbe	.L567
	movzbl	1(%rcx), %edi
	sall	$8, %eax
	leaq	init(%rip), %rsi
	leaq	mid(%rip), %rbx
	leaq	final(%rip), %r14
	addl	%edi, %eax
	movq	%rax, %rdx
	movq	%rax, %rdi
	shrq	$5, %rax
	shrq	$10, %rdx
	andl	$31, %eax
	andl	$31, %edi
	andl	$31, %edx
	movslq	(%r14,%rdi,4), %rdi
	movslq	(%rsi,%rdx,4), %rdx
	movslq	(%rbx,%rax,4), %rsi
	movl	%edi, 88(%rsp)
	cmpq	$-1, %rsi
	movl	%edx, 108(%rsp)
	movq	%rsi, %rax
	sete	80(%rsp)
	movzbl	80(%rsp), %ebx
	cmpq	$-1, %rdx
	sete	%r14b
	orb	%r14b, %bl
	jne	.L138
	cmpq	$-1, %rdi
	je	.L138
	testq	%rdx, %rdx
	setg	%r14b
	testq	%rsi, %rsi
	setg	%bl
	testb	%r14b, %r14b
	je	.L134
	testb	%bl, %bl
	je	.L134
	subq	$1, %rdx
	movl	88(%rsp), %ebx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rdx,%rax,4), %rax
	leaq	-1(%rsi,%rax), %rax
	imull	$28, %eax, %eax
	leal	44032(%rbx,%rax), %eax
.L135:
	testl	%eax, %eax
	je	.L600
	addq	$2, %rcx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L151:
	leal	-19968(%rdx), %ecx
	cmpl	$20901, %ecx
	jbe	.L345
	leal	-63744(%rdx), %ecx
	cmpl	$267, %ecx
	jbe	.L345
	cmpl	$8361, %edx
	je	.L601
	xorl	%edi, %edi
	movl	$988, %esi
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L602:
	leal	-1(%rcx), %esi
	cmpl	%edi, %esi
	jl	.L171
.L163:
	leal	(%rdi,%rsi), %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	movzwl	(%r9,%r8,4), %r10d
	cmpl	%r10d, %edx
	jb	.L602
	jbe	.L166
	leal	1(%rcx), %edi
	cmpl	%edi, %esi
	jge	.L163
.L171:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L603
	cmpq	$0, 72(%rsp)
	je	.L604
	testb	$8, 16(%r13)
	jne	.L605
.L174:
	testb	$2, 24(%rsp)
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
	jne	.L606
.L306:
	movl	$6, %r15d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$7, %r15d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$5, %r15d
	movl	%r15d, %ebx
	jne	.L183
	jmp	.L251
.L599:
	cmpl	$216, %eax
	ja	.L129
	.p2align 4,,10
	.p2align 3
.L127:
	cmpq	$0, 72(%rsp)
	je	.L296
	testl	%r15d, %r15d
	jne	.L607
.L296:
	movq	%r9, %r14
	movl	$6, %r15d
.L122:
	cmpq	$0, 16(%rsp)
	movq	8(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L180
.L595:
	movq	16(%rsp), %rax
	movq	%r12, (%rax)
.L18:
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	movq	32(%r13), %rax
	movl	(%rax), %ecx
	movq	%rax, (%rsp)
	movl	%ecx, %r9d
	andl	$7, %r9d
	je	.L26
	testq	%rsi, %rsi
	jne	.L608
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L609
	cmpl	$4, %r9d
	movq	%rbx, 128(%rsp)
	movq	%r11, 136(%rsp)
	ja	.L60
	movq	(%rsp), %rcx
	movslq	%r9d, %rsi
	leaq	120(%rsp), %r10
	movq	%rsi, 80(%rsp)
	xorl	%eax, %eax
.L61:
	movzbl	4(%rcx,%rax), %edx
	movb	%dl, (%r10,%rax)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L61
	movq	%rbx, %rax
	subq	80(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L610
	cmpq	%r14, %r11
	jnb	.L74
	movq	80(%rsp), %rdx
	leaq	1(%rbx), %rax
	leaq	119(%rsp), %rsi
.L69:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %rdx
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	$3, %rdx
	movb	%cl, (%rsi,%rdx)
	ja	.L342
	cmpq	%rdi, %rbp
	ja	.L69
.L342:
	movl	120(%rsp), %eax
	movq	%rdx, 80(%rsp)
	movq	%r10, 128(%rsp)
	cmpl	$127, %eax
	ja	.L71
	cmpl	$92, %eax
	je	.L71
	leaq	1(%r11), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%r11)
.L72:
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%r10, %rax
	movq	%rax, 128(%rsp)
	je	.L572
.L102:
	movq	(%rsp), %rbx
	subq	%r10, %rax
	movl	(%rbx), %edx
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L611
	movq	8(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r11
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %rbx
	movq	(%rsp), %rax
	movl	%edx, (%rax)
	movl	16(%r13), %eax
	movl	%eax, 24(%rsp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L601:
	leaq	1(%r12), %rax
	movq	%rax, 136(%rsp)
	movb	$92, (%r12)
	movq	136(%rsp), %r12
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L345:
	xorl	%edi, %edi
	movl	$4887, %esi
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r9
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L613:
	leal	-1(%rcx), %esi
	cmpl	%edi, %esi
	jl	.L612
.L159:
	leal	(%rdi,%rsi), %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	movzwl	(%r9,%r8,4), %r10d
	cmpl	%r10d, %edx
	jb	.L613
	jbe	.L156
	leal	1(%rcx), %edi
	cmpl	%edi, %esi
	jge	.L159
.L612:
	cmpq	$0, 72(%rsp)
	je	.L306
	testb	$8, 16(%r13)
	jne	.L614
.L160:
	testb	$2, 24(%rsp)
	je	.L306
	movq	72(%rsp), %rsi
	addq	$4, %rax
	movl	$6, %r15d
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L596:
	movq	40(%rsp), %rbx
	movq	%r12, 0(%r13)
	movq	112(%rsp), %rax
	addq	%rax, (%rbx)
.L182:
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L18
	cmpl	$7, %r15d
	jne	.L18
	movq	8(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L253
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r13), %rsi
	je	.L255
.L254:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L254
.L255:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%r15d, %ebx
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L607:
	movq	72(%rsp), %rax
	addq	$1, %rcx
	movl	$6, 24(%rsp)
	addq	$1, (%rax)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L597:
	movq	8(%rsp), %rax
	movq	%r11, (%rax)
	movl	16(%r13), %eax
	movl	%eax, 24(%rsp)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L615
	movq	%r11, 128(%rsp)
	movq	%r14, 136(%rsp)
	movq	%r14, %r12
	movl	$4, %r10d
.L215:
	cmpq	%r11, %rbp
	je	.L616
.L248:
	leaq	4(%r11), %rax
	cmpq	%rax, %rbp
	jb	.L323
	cmpq	%r12, %r15
	jbe	.L333
	movl	(%r11), %eax
	cmpl	$127, %eax
	ja	.L217
	cmpl	$92, %eax
	je	.L217
	leaq	1(%r12), %rdx
	movq	%rdx, 136(%rsp)
	movb	%al, (%r12)
	movq	136(%rsp), %r12
.L218:
	movq	128(%rsp), %rax
	leaq	4(%rax), %r11
	cmpq	%r11, %rbp
	movq	%r11, 128(%rsp)
	jne	.L248
.L616:
	movslq	%r10d, %rax
	movq	%rbp, %r11
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L134:
	testq	%rsi, %rsi
	jne	.L136
	testb	%r14b, %r14b
	je	.L136
	testq	%rdi, %rdi
	je	.L617
.L137:
	movl	108(%rsp), %ebx
	orl	%eax, %ebx
	jne	.L138
	testq	%rdi, %rdi
	jle	.L138
	leaq	final_to_ucs(%rip), %rax
	movl	-4(%rax,%rdi,4), %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L138:
	cmpq	$0, 72(%rsp)
	je	.L296
	testl	%r15d, %r15d
	je	.L296
	movq	72(%rsp), %rax
	movq	96(%rsp), %rcx
	movl	$6, 24(%rsp)
	addq	$1, (%rax)
	jmp	.L143
.L609:
	cmpl	$4, %r9d
	ja	.L29
	movq	(%rsp), %rsi
	cmpl	$1, %r9d
	movl	$1, %edx
	movzbl	4(%rsi), %eax
	movb	%al, 136(%rsp)
	je	.L30
	movzbl	5(%rsi), %eax
	movl	$2, %edx
	movb	%al, 137(%rsp)
.L30:
	leaq	4(%r11), %rax
	cmpq	%rax, %r14
	movq	%rax, 80(%rsp)
	jb	.L74
	movzbl	(%rbx), %eax
	movb	%al, 136(%rsp,%rdx)
	movzbl	136(%rsp), %eax
	cmpl	$127, %eax
	movq	%rax, %rsi
	ja	.L33
	cmpl	$92, %eax
	jne	.L570
	movl	$8361, %eax
.L570:
	leaq	136(%rsp), %r8
	leaq	1(%r8), %rdx
.L34:
	movl	%eax, (%r11)
	movq	(%rsp), %rax
	movq	80(%rsp), %r11
	movl	(%rax), %ecx
	movl	%ecx, %r9d
	andl	$7, %r9d
.L40:
	subq	%r8, %rdx
	movslq	%r9d, %rax
	cmpq	%rax, %rdx
	jle	.L618
	subq	%rax, %rdx
	movq	8(%rsp), %rax
	andl	$-8, %ecx
	addq	%rdx, %rbx
	movq	%rbx, (%rax)
	movq	(%rsp), %rax
	movl	%ecx, (%rax)
	movl	16(%r13), %eax
	movl	%eax, 24(%rsp)
	jmp	.L26
.L615:
	cmpq	%r11, %rbp
	je	.L619
	leaq	4(%r14), %r10
	movq	%r14, %r12
	movl	$4, %r9d
	cmpq	%r10, %r15
	jb	.L555
	movl	24(%rsp), %r8d
	movl	%ebx, 24(%rsp)
	movq	72(%rsp), %rbx
	andl	$2, %r8d
.L190:
	movzbl	(%r11), %eax
	cmpl	$127, %eax
	movl	%eax, %ecx
	ja	.L193
	cmpl	$92, %eax
	movl	$8361, %edi
	cmove	%edi, %eax
	addq	$1, %r11
.L195:
	movl	%eax, (%r12)
	movq	%r10, %r12
.L199:
	cmpq	%r11, %rbp
	je	.L620
.L212:
	leaq	4(%r12), %r10
	cmpq	%r10, %r15
	jnb	.L190
	movl	24(%rsp), %ebx
	movl	$5, %eax
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$5, %r15d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L217:
	leal	-44032(%rax), %ecx
	cmpl	$11171, %ecx
	ja	.L219
	leaq	2(%r12), %rax
	cmpq	%rax, %r15
	jb	.L333
	movl	%ecx, %esi
	movl	$613566757, %edx
	shrl	$2, %esi
	movl	%esi, %eax
	mull	%edx
	movl	%edx, %esi
	movl	$818089009, %edx
	movl	%esi, %eax
	movl	%esi, %edi
	imull	%edx
	imull	$28, %esi, %esi
	shrl	$2, %edx
	leal	(%rdx,%rdx,4), %eax
	leal	(%rdx,%rax,4), %eax
	movl	$-555131827, %edx
	subl	%eax, %edi
	movl	%ecx, %eax
	subl	%esi, %ecx
	mull	%edx
	shrl	$9, %edx
	movl	%edx, %eax
	leaq	init_to_bit(%rip), %rdx
	movl	(%rdx,%rax,4), %eax
	leaq	mid_to_bit(%rip), %rdx
	addl	(%rdx,%rdi,4), %eax
	leaq	final_to_bit(%rip), %rdi
	leaq	1(%r12), %rdx
	addl	(%rdi,%rcx,4), %eax
	movq	%rdx, 136(%rsp)
	movzbl	%ah, %ecx
	movb	%cl, (%r12)
	movq	136(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	movq	136(%rsp), %r12
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%rsp), %rdx
	subq	%r12, %rdx
	cmpq	$1, %rdx
	jbe	.L305
	movzbl	2(%r9,%r8,4), %eax
	movl	$-1370734243, %edx
	movb	%al, (%r12)
	movzbl	3(%r9,%r8,4), %eax
	movb	%al, 1(%r12)
	movq	136(%rsp), %rax
	subb	$74, (%rax)
	movq	136(%rsp), %rax
	subb	$33, 1(%rax)
	movq	136(%rsp), %rsi
	movzbl	(%rsi), %ecx
	movzbl	1(%rsi), %eax
	imull	$94, %ecx, %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	mull	%edx
	shrl	$7, %edx
	leal	-32(%rdx), %eax
	imull	$188, %edx, %edx
	movb	%al, (%rsi)
	movq	136(%rsp), %rax
	subl	%edx, %ecx
	movb	%cl, 1(%rax)
	movq	136(%rsp), %rcx
	movzbl	1(%rcx), %eax
	cmpb	$78, %al
	sbbl	%edx, %edx
	andl	$-18, %edx
	leal	67(%rax,%rdx), %eax
	movb	%al, 1(%rcx)
	movq	136(%rsp), %rax
	leaq	2(%rax), %r12
	movq	%r12, 136(%rsp)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L166:
	movq	(%rsp), %rcx
	subq	%r12, %rcx
	cmpq	$1, %rcx
	jbe	.L305
	movzbl	2(%r9,%r8,4), %eax
	movb	%al, (%r12)
	movzbl	3(%r9,%r8,4), %eax
	movb	%al, 1(%r12)
	movq	136(%rsp), %rcx
	movzbl	(%rcx), %esi
	cmpb	$34, %sil
	je	.L621
	movzbl	%sil, %edi
	cmpb	$73, %sil
	movzbl	1(%rcx), %eax
	leal	374(%rdi), %edx
	jbe	.L172
.L176:
	leal	94(%rax), %esi
	testb	$1, %dl
	cmovne	%esi, %eax
	movb	%al, 1(%rcx)
	movq	136(%rsp), %rsi
	movzbl	1(%rsi), %eax
	cmpb	$111, %al
	sbbl	%ecx, %ecx
	shrl	%edx
	andl	$-18, %ecx
	leal	34(%rax,%rcx), %eax
	movb	%al, 1(%rsi)
	movq	136(%rsp), %rax
	movb	%dl, (%rax)
	movq	136(%rsp), %rax
	leaq	2(%rax), %r12
	movq	%r12, 136(%rsp)
	jmp	.L149
.L621:
	movzbl	1(%rcx), %eax
	movl	$34, %edi
	cmpb	$104, %al
	ja	.L171
.L172:
	leal	401(%rdi), %edx
	jmp	.L176
.L219:
	leal	-12593(%rax), %edx
	cmpl	$50, %edx
	ja	.L220
	leaq	jamo_from_ucs_table(%rip), %rax
	leaq	2(%r12), %rcx
	movzwl	(%rax,%rdx,2), %eax
	cmpq	%rcx, %r15
	movl	%eax, %edx
	jb	.L333
	leaq	1(%r12), %rcx
	movzbl	%ah, %eax
	movq	%rcx, 136(%rsp)
	movb	%al, (%r12)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	movq	136(%rsp), %r12
	jmp	.L218
.L592:
	cmpq	$0, 16(%rsp)
	jne	.L622
	movq	32(%r13), %rax
	xorl	%r15d, %r15d
	movq	$0, (%rax)
	testb	$1, 16(%r13)
	jne	.L18
	movq	48(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	56(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	popq	%r8
	movl	%eax, %r15d
	popq	%r9
	jmp	.L18
.L337:
	movl	%ebx, %r15d
	jmp	.L182
.L136:
	testq	%rdx, %rdx
	sete	%dl
	testb	%dl, %bl
	je	.L137
	testq	%rdi, %rdi
	jne	.L137
	addl	$12622, %eax
	jmp	.L135
.L193:
	leal	124(%rax), %edx
	cmpb	$117, %dl
	ja	.L196
	cmpl	$223, %eax
	je	.L196
	cmpl	$211, %eax
	ja	.L623
	leaq	1(%r11), %rsi
	cmpq	%rsi, %rbp
	movq	%rsi, 88(%rsp)
	jbe	.L568
	movzbl	1(%r11), %edx
	sall	$8, %eax
	leaq	init(%rip), %rsi
	leaq	mid(%rip), %rdi
	leal	(%rdx,%rax), %ecx
	movq	%rcx, %rax
	movq	%rcx, %rdx
	shrq	$5, %rcx
	shrq	$10, %rax
	andl	$31, %ecx
	andl	$31, %edx
	andl	$31, %eax
	movslq	(%rdi,%rcx,4), %rcx
	movslq	(%rsi,%rax,4), %rax
	leaq	final(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rdx
	movl	%ecx, 80(%rsp)
	cmpq	$-1, %rax
	movl	%eax, 96(%rsp)
	sete	%dil
	cmpq	$-1, %rcx
	sete	%sil
	movl	%edx, 108(%rsp)
	orb	%sil, %dil
	jne	.L207
	cmpq	$-1, %rdx
	je	.L207
	testq	%rax, %rax
	setg	%sil
	testq	%rcx, %rcx
	setg	%dil
	testb	%sil, %sil
	je	.L203
	testb	%dil, %dil
	je	.L203
	subq	$1, %rax
	movl	108(%rsp), %esi
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	-1(%rcx,%rax), %rax
	imull	$28, %eax, %eax
	leal	44032(%rsi,%rax), %eax
.L204:
	testl	%eax, %eax
	je	.L624
	addq	$2, %r11
	jmp	.L195
.L628:
	cmpq	$0, 72(%rsp)
	je	.L334
	testb	$8, 16(%r13)
	je	.L587
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rsi
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %rdi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rsi
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%rdi
	movq	128(%rsp), %r11
	movq	136(%rsp), %r12
	je	.L587
.L589:
	cmpl	$5, %r10d
	jne	.L215
.L333:
	movl	$5, %eax
.L216:
	movq	8(%rsp), %rdi
	movq	120(%rsp), %r15
	movq	%r11, (%rdi)
.L214:
	cmpq	%r15, %r12
	jne	.L189
	cmpq	$5, %rax
	jne	.L188
	cmpq	%r12, %r14
	jne	.L183
.L191:
	subl	$1, 20(%r13)
	jmp	.L183
.L567:
	movq	%r9, %r14
	movl	$7, %r15d
	jmp	.L122
.L220:
	leal	-19968(%rax), %edx
	cmpl	$20901, %edx
	jbe	.L347
	leal	-63744(%rax), %edx
	cmpl	$267, %edx
	jbe	.L347
	cmpl	$8361, %eax
	jne	.L331
	leaq	1(%r12), %rax
	movq	%rax, 136(%rsp)
	movb	$92, (%r12)
	movq	136(%rsp), %r12
	jmp	.L218
.L323:
	movl	$7, %eax
	jmp	.L216
.L617:
	leaq	init_to_ucs(%rip), %rax
	movl	-4(%rax,%rdx,4), %eax
	jmp	.L135
.L129:
	leaq	1(%rcx), %rbx
	cmpq	%rbx, %rbp
	jbe	.L567
	movzbl	1(%rcx), %edx
	cmpl	$48, %edx
	movl	%edx, %r14d
	jbe	.L139
	leal	-127(%rdx), %edi
	cmpl	$17, %edi
	jbe	.L139
	cmpb	$-1, %r14b
	je	.L139
	cmpb	$-39, %sil
	je	.L625
	cmpb	$-38, %sil
	je	.L626
	cmpb	$-34, %sil
	jne	.L141
	cmpl	$241, %edx
	ja	.L139
.L141:
	sall	$8, %eax
	leal	(%rax,%rdx), %edi
	movzbl	%r14b, %edx
	call	johab_sym_hanja_to_ucs
	jmp	.L135
.L196:
	testq	%rbx, %rbx
	je	.L322
	testl	%r8d, %r8d
	jne	.L627
.L322:
	movl	24(%rsp), %ebx
	movl	$6, %eax
.L192:
	movq	8(%rsp), %rdi
	movq	%r11, (%rdi)
	jmp	.L214
.L605:
	movq	%r11, 80(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %rdi
	movq	%r13, %rsi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r15d
	popq	%r11
	movq	80(%rsp), %r11
	je	.L174
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
.L575:
	cmpl	$5, %r15d
	jne	.L146
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L71:
	leal	-44032(%rax), %ecx
	cmpl	$11171, %ecx
	ja	.L73
	leaq	2(%r11), %rax
	cmpq	%rax, %r14
	jb	.L74
	movl	%ecx, %esi
	movl	$613566757, %edx
	shrl	$2, %esi
	movl	%esi, %eax
	mull	%edx
	movl	%edx, %esi
	movl	$818089009, %edx
	movl	%esi, %eax
	movl	%esi, %edi
	imull	%edx
	imull	$28, %esi, %esi
	movl	%edx, %eax
	shrl	$2, %eax
	leal	(%rax,%rax,4), %edx
	leal	(%rax,%rdx,4), %eax
	movl	$-555131827, %edx
	subl	%eax, %edi
	movl	%ecx, %eax
	subl	%esi, %ecx
	mull	%edx
	movl	%edx, %eax
	leaq	init_to_bit(%rip), %rdx
	shrl	$9, %eax
	movl	(%rdx,%rax,4), %eax
	leaq	mid_to_bit(%rip), %rdx
	addl	(%rdx,%rdi,4), %eax
	leaq	final_to_bit(%rip), %rdx
	addl	(%rdx,%rcx,4), %eax
	leaq	1(%r11), %rdx
	movq	%rdx, 136(%rsp)
	movzbl	%ah, %ebx
	movb	%bl, (%r11)
	movq	136(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	jmp	.L72
.L606:
	movq	72(%rsp), %rdi
	addq	$4, %rax
	movl	$6, %r15d
	movq	%rax, 128(%rsp)
	addq	$1, (%rdi)
	jmp	.L146
.L614:
	movq	%r11, 80(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%rbp, %r8
	movq	48(%rsp), %rdi
	movq	%r13, %rsi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r15d
	popq	%r12
	cmpl	$6, %r15d
	popq	%rax
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
	movq	80(%rsp), %r11
	jne	.L575
	jmp	.L160
.L347:
	xorl	%esi, %esi
	movl	$4887, %ecx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L228
.L629:
	leal	-1(%rdx), %ecx
.L224:
	cmpl	%esi, %ecx
	jl	.L628
.L228:
	leal	(%rsi,%rcx), %edx
	sarl	%edx
	movslq	%edx, %rdi
	movzwl	(%r8,%rdi,4), %r9d
	cmpl	%r9d, %eax
	jb	.L629
	jbe	.L225
	leal	1(%rdx), %esi
	jmp	.L224
.L604:
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
	movl	$6, %r15d
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%r9, %r14
	movl	24(%rsp), %r15d
	jmp	.L122
.L600:
	cmpq	$0, 72(%rsp)
	je	.L296
	testl	%r15d, %r15d
	je	.L296
	movq	72(%rsp), %rax
	addq	$2, %rcx
	movl	$6, 24(%rsp)
	addq	$1, (%rax)
	jmp	.L130
.L282:
	movq	%rbp, %rcx
	movq	%r14, %r12
	movl	$4, %r15d
	jmp	.L122
.L331:
	xorl	%esi, %esi
	movl	$988, %ecx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L232
.L630:
	leal	-1(%rdx), %ecx
.L234:
	cmpl	%esi, %ecx
	jl	.L240
.L232:
	leal	(%rsi,%rcx), %edx
	sarl	%edx
	movslq	%edx, %rdi
	movzwl	(%r8,%rdi,4), %r9d
	cmpl	%r9d, %eax
	jb	.L630
	jbe	.L235
	leal	1(%rdx), %esi
	jmp	.L234
.L603:
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L146
.L625:
	cmpl	$232, %edx
	jbe	.L141
.L139:
	cmpq	$0, 72(%rsp)
	je	.L296
	testl	%r15d, %r15d
	je	.L296
	movq	72(%rsp), %rax
	movq	%rbx, %rcx
	movl	$6, 24(%rsp)
	addq	$1, (%rax)
	jmp	.L143
.L73:
	leal	-12593(%rax), %edx
	cmpl	$50, %edx
	ja	.L75
	leaq	jamo_from_ucs_table(%rip), %rax
	leaq	2(%r11), %rcx
	movzwl	(%rax,%rdx,2), %eax
	cmpq	%rcx, %r14
	movl	%eax, %edx
	jb	.L74
	leaq	1(%r11), %rcx
	movzbl	%ah, %eax
	movq	%rcx, 136(%rsp)
	movb	%al, (%r11)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movb	%dl, (%rax)
	jmp	.L72
.L33:
	leal	124(%rax), %edi
	cmpb	$117, %dil
	ja	.L36
	cmpl	$223, %eax
	je	.L36
	leaq	136(%rsp), %r8
	addq	$1, %rdx
	cmpl	$211, %eax
	leaq	(%r8,%rdx), %r10
	ja	.L631
	leaq	1(%r8), %rdi
	cmpq	%rdi, %r10
	jbe	.L41
	movzbl	137(%rsp), %edx
	sall	$8, %eax
	leaq	init(%rip), %rdi
	leal	(%rdx,%rax), %esi
	movq	%rsi, %rax
	movq	%rsi, %rdx
	shrq	$5, %rsi
	shrq	$10, %rax
	andl	$31, %esi
	andl	$31, %edx
	andl	$31, %eax
	movslq	(%rdi,%rax,4), %rax
	leaq	mid(%rip), %rdi
	movslq	(%rdi,%rsi,4), %r10
	leaq	final(%rip), %rsi
	movslq	(%rsi,%rdx,4), %rdx
	movl	%eax, 88(%rsp)
	cmpq	$-1, %r10
	movq	%r10, %r12
	sete	%dil
	cmpq	$-1, %rax
	sete	%sil
	movq	%rdx, %r15
	orb	%sil, %dil
	jne	.L49
	cmpq	$-1, %rdx
	je	.L49
	testq	%rax, %rax
	setg	%sil
	testq	%r10, %r10
	setg	%dil
	testb	%sil, %sil
	je	.L44
	testb	%dil, %dil
	je	.L44
	subq	$1, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	-1(%r10,%rax), %rax
	imull	$28, %eax, %eax
	leal	44032(%r15,%rax), %eax
.L45:
	testl	%eax, %eax
	leaq	2(%r8), %rdx
	jne	.L34
	cmpq	$0, 72(%rsp)
	movl	$6, %r15d
	je	.L18
	testb	$2, 24(%rsp)
	je	.L18
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L640:
	movzbl	1(%rcx), %edx
	movl	$34, %esi
	cmpb	$104, %dl
	jbe	.L241
.L240:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L632
	cmpq	$0, 72(%rsp)
	je	.L633
	testb	$8, 16(%r13)
	jne	.L634
.L243:
	movq	128(%rsp), %r11
	movq	136(%rsp), %r12
.L587:
	testb	$2, 24(%rsp)
	jne	.L635
.L334:
	movl	$6, %eax
	jmp	.L216
.L610:
	movq	80(%rsp), %rsi
	movq	%rbp, %rdx
	movq	8(%rsp), %rax
	subq	%rbx, %rdx
	addq	%rsi, %rdx
	movq	%rbp, (%rax)
	cmpq	$4, %rdx
	ja	.L63
	cmpq	%rsi, %rdx
	leaq	1(%rbx), %rax
	jbe	.L65
	movq	(%rsp), %rdi
	movq	80(%rsp), %rcx
.L66:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %esi
	addq	$1, %rax
	movb	%sil, 4(%rdi,%rcx)
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	jne	.L66
.L65:
	movl	$7, %r15d
	jmp	.L18
.L627:
	addq	$1, %r11
	addq	$1, (%rbx)
	movl	$6, %r9d
	jmp	.L199
.L36:
	cmpq	$0, 72(%rsp)
	movl	$6, %r15d
	je	.L18
	testb	$2, 24(%rsp)
	je	.L18
	movq	72(%rsp), %rax
	leaq	136(%rsp), %r8
	leaq	1(%r8), %rdx
	addq	$1, (%rax)
	jmp	.L40
.L75:
	movq	80(%rsp), %rsi
	leal	-19968(%rax), %edx
	cmpl	$20901, %edx
	leaq	(%r10,%rsi), %r12
	jbe	.L343
	leal	-63744(%rax), %edx
	cmpl	$267, %edx
	jbe	.L343
	cmpl	$8361, %eax
	je	.L636
	xorl	%edi, %edi
	movl	$988, %esi
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L91
.L638:
	leal	-1(%rdx), %esi
.L93:
	cmpl	%esi, %edi
	jg	.L637
.L91:
	leal	(%rdi,%rsi), %edx
	sarl	%edx
	movslq	%edx, %rcx
	movzwl	(%r8,%rcx,4), %r9d
	cmpl	%r9d, %eax
	jb	.L638
	jbe	.L94
	leal	1(%rdx), %edi
	jmp	.L93
.L203:
	testq	%rcx, %rcx
	jne	.L205
	testb	%sil, %sil
	je	.L205
	testq	%rdx, %rdx
	jne	.L206
	leaq	init_to_ucs(%rip), %rdi
	movl	-4(%rdi,%rax,4), %eax
	jmp	.L204
.L105:
	movq	%r10, 96(%rsp)
	movq	%r11, 88(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r12, %r8
	movq	%r13, %rsi
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r15d
	cmpl	$6, %r15d
	popq	%rax
	popq	%rdx
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	je	.L107
.L86:
	movq	128(%rsp), %rax
	cmpq	%r10, %rax
	jne	.L102
	cmpl	$7, %r15d
	je	.L639
	testl	%r15d, %r15d
	jne	.L18
.L572:
	movq	8(%rsp), %rax
	movq	(%rax), %rbx
	movl	16(%r13), %eax
	movl	%eax, 24(%rsp)
	jmp	.L26
.L207:
	testq	%rbx, %rbx
	je	.L322
	testl	%r8d, %r8d
	je	.L322
	addq	$1, (%rbx)
	movq	88(%rsp), %r11
	movl	$6, %r9d
	jmp	.L212
.L225:
	movq	%r15, %rax
	subq	%r12, %rax
	cmpq	$1, %rax
	jbe	.L333
	movzbl	2(%r8,%rdi,4), %eax
	movl	$-1370734243, %edx
	movb	%al, (%r12)
	movzbl	3(%r8,%rdi,4), %eax
	movb	%al, 1(%r12)
	movq	136(%rsp), %rax
	subb	$74, (%rax)
	movq	136(%rsp), %rax
	subb	$33, 1(%rax)
	movq	136(%rsp), %rsi
	movzbl	(%rsi), %ecx
	movzbl	1(%rsi), %eax
	imull	$94, %ecx, %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	mull	%edx
	movl	%edx, %eax
	shrl	$7, %eax
	leal	-32(%rax), %edx
	imull	$188, %eax, %eax
	movb	%dl, (%rsi)
	subl	%eax, %ecx
	movq	136(%rsp), %rax
	movb	%cl, 1(%rax)
	movq	136(%rsp), %rcx
	movzbl	1(%rcx), %eax
	cmpb	$78, %al
	sbbl	%edx, %edx
	andl	$-18, %edx
	leal	67(%rax,%rdx), %eax
	movb	%al, 1(%rcx)
	movq	136(%rsp), %rax
	leaq	2(%rax), %r12
	movq	%r12, 136(%rsp)
	jmp	.L218
.L235:
	movq	%r15, %rdx
	subq	%r12, %rdx
	cmpq	$1, %rdx
	jbe	.L333
	movzbl	2(%r8,%rdi,4), %edx
	movb	%dl, (%r12)
	movzbl	3(%r8,%rdi,4), %edx
	movb	%dl, 1(%r12)
	movq	136(%rsp), %rcx
	movzbl	(%rcx), %edi
	cmpb	$34, %dil
	je	.L640
	movzbl	%dil, %esi
	cmpb	$73, %dil
	movzbl	1(%rcx), %edx
	leal	374(%rsi), %eax
	jbe	.L241
.L245:
	leal	94(%rdx), %esi
	testb	$1, %al
	cmovne	%esi, %edx
	movb	%dl, 1(%rcx)
	movq	136(%rsp), %rsi
	movzbl	1(%rsi), %edx
	cmpb	$111, %dl
	sbbl	%ecx, %ecx
	shrl	%eax
	andl	$-18, %ecx
	leal	34(%rdx,%rcx), %edx
	movb	%dl, 1(%rsi)
	movq	136(%rsp), %rdx
	movb	%al, (%rdx)
	movq	136(%rsp), %rax
	leaq	2(%rax), %r12
	movq	%r12, 136(%rsp)
	jmp	.L218
.L555:
	cmpq	%r14, %r15
	je	.L191
.L189:
	leaq	__PRETTY_FUNCTION__.9256(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	leal	401(%rsi), %eax
	jmp	.L245
.L206:
	movl	96(%rsp), %eax
	orl	80(%rsp), %eax
	jne	.L207
	testq	%rdx, %rdx
	jle	.L207
	leaq	final_to_ucs(%rip), %rax
	movl	-4(%rax,%rdx,4), %eax
	jmp	.L204
.L343:
	xorl	%edi, %edi
	movl	$4887, %esi
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L83
.L642:
	leal	-1(%rdx), %esi
.L79:
	cmpl	%edi, %esi
	jl	.L641
.L83:
	leal	(%rdi,%rsi), %edx
	sarl	%edx
	movslq	%edx, %rcx
	movzwl	(%r8,%rcx,4), %r9d
	cmpl	%r9d, %eax
	jb	.L642
	jbe	.L80
	leal	1(%rdx), %edi
	jmp	.L79
.L641:
	cmpq	$0, 72(%rsp)
	je	.L84
	testb	$8, 24(%rsp)
	jne	.L643
	testb	$2, 24(%rsp)
	jne	.L90
.L84:
	movl	$6, %r15d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L626:
	leal	-161(%rdx), %edi
	cmpl	$50, %edi
	jbe	.L139
	jmp	.L141
.L205:
	testq	%rax, %rax
	sete	%al
	testb	%dil, %al
	je	.L206
	testq	%rdx, %rdx
	jne	.L206
	movl	80(%rsp), %eax
	addl	$12622, %eax
	jmp	.L204
.L623:
	cmpl	$216, %eax
	jbe	.L196
	leaq	1(%r11), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 80(%rsp)
	jbe	.L568
	movzbl	1(%r11), %edx
	cmpl	$48, %edx
	movl	%edx, %esi
	jbe	.L208
	leal	-127(%rdx), %edi
	cmpl	$17, %edi
	jbe	.L208
	cmpb	$-1, %sil
	je	.L208
	cmpb	$-39, %cl
	je	.L644
	cmpb	$-38, %cl
	je	.L645
	cmpb	$-34, %cl
	jne	.L210
	cmpl	$241, %edx
	ja	.L208
.L210:
	sall	$8, %eax
	leal	(%rax,%rdx), %edi
	movzbl	%sil, %edx
	movzbl	%cl, %esi
	call	johab_sym_hanja_to_ucs
	jmp	.L204
.L620:
	movl	24(%rsp), %ebx
	movslq	%r9d, %rax
	movq	%rbp, %r11
	jmp	.L192
.L44:
	testq	%r10, %r10
	jne	.L46
	testb	%sil, %sil
	je	.L46
	testq	%rdx, %rdx
	jne	.L47
	leaq	init_to_ucs(%rip), %rdx
	movl	-4(%rdx,%rax,4), %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L637:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L646
.L99:
	cmpq	$0, 72(%rsp)
	je	.L647
	testb	$8, 16(%r13)
	jne	.L105
.L107:
	testb	$2, 24(%rsp)
	movq	128(%rsp), %rax
	je	.L88
	movq	72(%rsp), %rbx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rbx)
.L88:
	cmpq	%r10, %rax
	jne	.L102
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L568:
	movl	24(%rsp), %ebx
	movl	$7, %eax
	jmp	.L192
.L636:
	leaq	1(%r11), %rax
	movq	%rax, 136(%rsp)
	movb	$92, (%r11)
	jmp	.L72
.L94:
	movq	%r14, %rdx
	subq	%r11, %rdx
	cmpq	$1, %rdx
	jbe	.L74
	movzbl	2(%r8,%rcx,4), %edx
	movb	%dl, (%r11)
	movzbl	3(%r8,%rcx,4), %edx
	movb	%dl, 1(%r11)
	movq	136(%rsp), %rcx
	movzbl	(%rcx), %edi
	cmpb	$34, %dil
	je	.L648
	movzbl	%dil, %esi
	cmpb	$73, %dil
	movzbl	1(%rcx), %edx
	leal	374(%rsi), %eax
	jbe	.L101
.L109:
	leal	94(%rdx), %esi
	testb	$1, %al
	cmovne	%esi, %edx
	movb	%dl, 1(%rcx)
	movq	136(%rsp), %rsi
	movzbl	1(%rsi), %edx
	cmpb	$111, %dl
	sbbl	%ecx, %ecx
	shrl	%eax
	andl	$-18, %ecx
	leal	34(%rdx,%rcx), %edx
	movb	%dl, 1(%rsi)
	movq	136(%rsp), %rdx
	movb	%al, (%rdx)
	addq	$2, 136(%rsp)
	jmp	.L72
.L631:
	cmpl	$216, %eax
	jbe	.L36
	leaq	1(%r8), %rdi
	cmpq	%rdi, %r10
	ja	.L649
.L41:
	leaq	2(%r8), %rax
	cmpq	%rax, %r10
	je	.L650
	movslq	%r9d, %rax
	movq	%rdx, %rdi
	andl	$-8, %ecx
	subq	%rax, %rdi
	movq	%rdi, %rax
	addq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jle	.L651
	cmpq	$4, %rdx
	ja	.L652
	movq	(%rsp), %rax
	orl	%edx, %ecx
	movl	%ecx, (%rax)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L65
.L58:
	movq	(%rsp), %rbx
	movb	%sil, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L65
	movzbl	(%r8,%rax), %esi
	jmp	.L58
.L649:
	movzbl	137(%rsp), %edx
	cmpl	$48, %edx
	movl	%edx, %r10d
	jbe	.L49
	leal	-127(%rdx), %edi
	cmpl	$17, %edi
	jbe	.L49
	cmpb	$-1, %r10b
	je	.L49
	cmpb	$-39, %sil
	je	.L653
	cmpb	$-38, %sil
	je	.L654
	cmpl	$241, %edx
	jbe	.L51
	cmpb	$-34, %sil
	je	.L49
.L51:
	sall	$8, %eax
	movq	%r11, 88(%rsp)
	leal	(%rax,%rdx), %edi
	movzbl	%r10b, %edx
	call	johab_sym_hanja_to_ucs
	movq	88(%rsp), %r11
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L47:
	movl	88(%rsp), %eax
	orl	%r12d, %eax
	jne	.L49
	testq	%rdx, %rdx
	jle	.L49
	leaq	final_to_ucs(%rip), %rax
	movl	-4(%rax,%rdx,4), %eax
	jmp	.L45
.L653:
	cmpl	$232, %edx
	jbe	.L51
.L49:
	cmpq	$0, 72(%rsp)
	movl	$6, %r15d
	je	.L18
	testb	$2, 24(%rsp)
	je	.L18
	movq	72(%rsp), %rax
	leaq	1(%r8), %rdx
	addq	$1, (%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L624:
	testq	%rbx, %rbx
	je	.L322
	testl	%r8d, %r8d
	je	.L322
	addq	$2, %r11
	addq	$1, (%rbx)
	movl	$6, %r9d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r14, %rax
	subq	%r11, %rax
	cmpq	$1, %rax
	jbe	.L74
	movzbl	2(%r8,%rcx,4), %eax
	movl	$-1370734243, %edx
	movb	%al, (%r11)
	movzbl	3(%r8,%rcx,4), %eax
	movb	%al, 1(%r11)
	movq	136(%rsp), %rax
	subb	$74, (%rax)
	movq	136(%rsp), %rax
	subb	$33, 1(%rax)
	movq	136(%rsp), %rsi
	movzbl	(%rsi), %ecx
	movzbl	1(%rsi), %eax
	imull	$94, %ecx, %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	mull	%edx
	movl	%edx, %eax
	shrl	$7, %eax
	leal	-32(%rax), %edx
	imull	$188, %eax, %eax
	movb	%dl, (%rsi)
	subl	%eax, %ecx
	movq	136(%rsp), %rax
	movb	%cl, 1(%rax)
	movq	136(%rsp), %rcx
	movzbl	1(%rcx), %eax
	cmpb	$78, %al
	sbbl	%edx, %edx
	andl	$-18, %edx
	leal	67(%rax,%rdx), %eax
	movb	%al, 1(%rcx)
	addq	$2, 136(%rsp)
	jmp	.L72
.L644:
	cmpl	$232, %edx
	jbe	.L210
.L208:
	testq	%rbx, %rbx
	je	.L322
	testl	%r8d, %r8d
	je	.L322
	addq	$1, (%rbx)
	movq	80(%rsp), %r11
	movl	$6, %r9d
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L619:
	cmpq	%r14, %r15
	jne	.L189
.L188:
	leaq	__PRETTY_FUNCTION__.9256(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	leal	401(%rsi), %eax
	jmp	.L109
.L648:
	movzbl	1(%rcx), %edx
	movl	$34, %esi
	cmpb	$104, %dl
	jbe	.L101
	shrl	$7, %eax
	cmpl	$7168, %eax
	jne	.L99
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L635:
	movq	72(%rsp), %rax
	addq	$4, %r11
	movl	$6, %r10d
	movq	%r11, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L215
.L634:
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	80(%rsp)
	movq	24(%rsp), %rax
	movq	%r13, %rsi
	movq	48(%rsp), %rdi
	leaq	152(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	cmpl	$6, %eax
	movl	%eax, %r10d
	popq	%rdx
	popq	%rcx
	je	.L243
	movq	128(%rsp), %r11
	movq	136(%rsp), %r12
	jmp	.L589
.L632:
	movq	128(%rsp), %rax
	movq	136(%rsp), %r12
	leaq	4(%rax), %r11
	movq	%r11, 128(%rsp)
	jmp	.L215
.L645:
	leal	-161(%rdx), %edi
	cmpl	$50, %edi
	jbe	.L208
	jmp	.L210
.L633:
	movq	128(%rsp), %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	movq	120(%rsp), %rax
	cmpq	%rax, 136(%rsp)
	je	.L188
	jmp	.L189
.L618:
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	leaq	4(%r10), %rax
	cmpq	%rax, %r12
	je	.L655
	movq	(%rsp), %rax
	movq	80(%rsp), %rbx
	movq	8(%rsp), %rsi
	movl	(%rax), %eax
	movq	%rbx, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rsi)
	cmpq	%rdx, %rbx
	jle	.L656
	cmpq	$4, 80(%rsp)
	ja	.L657
	movq	80(%rsp), %rbx
	movq	(%rsp), %rdi
	orl	%ebx, %eax
	testq	%rbx, %rbx
	movl	%eax, (%rdi)
	je	.L65
	xorl	%eax, %eax
.L120:
	movzbl	(%r10,%rax), %edx
	movq	(%rsp), %rbx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 80(%rsp)
	jne	.L120
	jmp	.L65
.L647:
	movq	128(%rsp), %rax
	cmpq	%r10, %rax
	jne	.L102
	jmp	.L84
.L657:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L656:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L655:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L646:
	movq	128(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L102
.L63:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L60:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L29:
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L652:
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L654:
	leal	-161(%rdx), %edi
	cmpl	$50, %edi
	jbe	.L49
	jmp	.L51
.L46:
	testq	%rax, %rax
	sete	%al
	testb	%dil, %al
	je	.L47
	testq	%rdx, %rdx
	jne	.L47
	leal	12622(%r12), %eax
	jmp	.L45
.L651:
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L650:
	leaq	__PRETTY_FUNCTION__.9086(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L611:
	leaq	__PRETTY_FUNCTION__.9178(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L622:
	leaq	__PRETTY_FUNCTION__.9256(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L90:
	movq	72(%rsp), %rax
	addq	$4, 128(%rsp)
	addq	$1, (%rax)
	movq	128(%rsp), %rax
	jmp	.L88
.L643:
	movq	%r10, 96(%rsp)
	movq	%r11, 88(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %r8
	pushq	80(%rsp)
	movq	48(%rsp), %rdi
	movq	%rbx, %rdx
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%rcx
	cmpl	$6, %eax
	movl	%eax, %r15d
	popq	%rsi
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	jne	.L86
	testb	$2, 24(%rsp)
	jne	.L90
	movq	128(%rsp), %rax
	jmp	.L88
.L253:
	leaq	__PRETTY_FUNCTION__.9256(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L608:
	leaq	__PRETTY_FUNCTION__.9256(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9178, @object
	.size	__PRETTY_FUNCTION__.9178, 16
__PRETTY_FUNCTION__.9178:
	.string	"to_johab_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9086, @object
	.size	__PRETTY_FUNCTION__.9086, 18
__PRETTY_FUNCTION__.9086:
	.string	"from_johab_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9256, @object
	.size	__PRETTY_FUNCTION__.9256, 6
__PRETTY_FUNCTION__.9256:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	jamo_from_ucs_table, @object
	.size	jamo_from_ucs_table, 102
jamo_from_ucs_table:
	.value	-30655
	.value	-29631
	.value	-31676
	.value	-28607
	.value	-31674
	.value	-31673
	.value	-27583
	.value	-26559
	.value	-25535
	.value	-31670
	.value	-31669
	.value	-31668
	.value	-31667
	.value	-31666
	.value	-31665
	.value	-31664
	.value	-24511
	.value	-23487
	.value	-22463
	.value	-31660
	.value	-21439
	.value	-20415
	.value	-19391
	.value	-18367
	.value	-17343
	.value	-16319
	.value	-15295
	.value	-14271
	.value	-13247
	.value	-12223
	.value	-31647
	.value	-31615
	.value	-31583
	.value	-31551
	.value	-31519
	.value	-31423
	.value	-31391
	.value	-31359
	.value	-31327
	.value	-31295
	.value	-31263
	.value	-31167
	.value	-31135
	.value	-31103
	.value	-31071
	.value	-31039
	.value	-31007
	.value	-30911
	.value	-30879
	.value	-30847
	.value	-30815
	.align 32
	.type	final_to_bit, @object
	.size	final_to_bit, 112
final_to_bit:
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.align 32
	.type	mid_to_bit, @object
	.size	mid_to_bit, 84
mid_to_bit:
	.long	96
	.long	128
	.long	160
	.long	192
	.long	224
	.long	320
	.long	352
	.long	384
	.long	416
	.long	448
	.long	480
	.long	576
	.long	608
	.long	640
	.long	672
	.long	704
	.long	736
	.long	832
	.long	864
	.long	896
	.long	928
	.align 32
	.type	init_to_bit, @object
	.size	init_to_bit, 76
init_to_bit:
	.long	34816
	.long	35840
	.long	36864
	.long	37888
	.long	38912
	.long	39936
	.long	40960
	.long	41984
	.long	43008
	.long	44032
	.long	45056
	.long	46080
	.long	47104
	.long	48128
	.long	49152
	.long	50176
	.long	51200
	.long	52224
	.long	53248
	.align 32
	.type	final_to_ucs, @object
	.size	final_to_ucs, 124
final_to_ucs:
	.long	0
	.long	0
	.long	12595
	.long	0
	.long	12597
	.long	12598
	.long	0
	.long	0
	.long	12602
	.long	12603
	.long	12604
	.long	12605
	.long	12606
	.long	12607
	.long	12608
	.long	0
	.long	0
	.long	12612
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.align 32
	.type	init_to_ucs, @object
	.size	init_to_ucs, 76
init_to_ucs:
	.long	12593
	.long	12594
	.long	12596
	.long	12599
	.long	12600
	.long	12601
	.long	12609
	.long	12610
	.long	12611
	.long	12613
	.long	12614
	.long	12615
	.long	12616
	.long	12617
	.long	12618
	.long	12619
	.long	12620
	.long	12621
	.long	12622
	.align 32
	.type	final, @object
	.size	final, 128
final:
	.long	-1
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	-1
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	-1
	.long	-1
	.align 32
	.type	mid, @object
	.size	mid, 128
mid:
	.long	-1
	.long	-1
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	-1
	.long	-1
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	-1
	.long	-1
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	-1
	.long	-1
	.long	18
	.long	19
	.long	20
	.long	21
	.long	-1
	.long	-1
	.align 32
	.type	init, @object
	.size	init, 128
init:
	.long	-1
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.long	-1
	.zero	4
