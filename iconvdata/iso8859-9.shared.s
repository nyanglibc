	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %esi
	movl	(%rax,%rsi,4), %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO-8859-9//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L4
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	32(%rax), %rsi
	movl	$13, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L7
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r15d
	movq	%rsi, 72(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 56(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 48(%rsp)
	testb	$1, %r15b
	movq	%r9, 24(%rsp)
	movl	208(%rsp), %ebx
	movq	%rsi, 64(%rsp)
	movq	$0, 32(%rsp)
	jne	.L9
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 32(%rsp)
	je	.L9
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rsp)
.L9:
	testl	%ebx, %ebx
	jne	.L190
	movq	16(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %ebx
	testq	%rdi, %rdi
	movq	(%rax), %r13
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 24(%rsp)
	movq	(%rax), %r14
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 80(%rsp)
	movq	56(%rsp), %rax
	setne	107(%rsp)
	movzbl	107(%rsp), %esi
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L103
	testb	%sil, %sil
	je	.L103
	movq	32(%r12), %rbx
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
	jne	.L191
.L103:
	movq	$0, 40(%rsp)
.L16:
	leaq	136(%rsp), %rcx
	leaq	to_ucs4(%rip), %r15
	movq	%rcx, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L93:
	movq	24(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L47
	movq	(%rcx), %rdi
	addq	%rdi, 40(%rsp)
.L47:
	testq	%rax, %rax
	je	.L192
	movl	16(%r12), %r11d
	leaq	128(%rsp), %r10
	movq	%r14, %rbx
	movq	%r13, 128(%rsp)
	movq	%r13, %rax
	movq	%r14, 136(%rsp)
	movl	$4, 12(%rsp)
	andl	$2, %r11d
.L53:
	cmpq	%rax, %rbp
	je	.L54
.L61:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L109
	cmpq	%rbx, (%rsp)
	jbe	.L110
	movl	(%rax), %edx
	cmpl	$351, %edx
	ja	.L55
	leaq	from_ucs4(%rip), %rdi
	movl	%edx, %esi
	testl	%edx, %edx
	movzbl	(%rdi,%rsi), %esi
	je	.L56
	testb	%sil, %sil
	je	.L55
.L56:
	leaq	1(%rbx), %rax
	movq	%rax, 136(%rsp)
	movb	%sil, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L61
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	$0, 48(%rsp)
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L193
.L62:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L194
	cmpq	%rbx, %r14
	jnb	.L113
	movq	32(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	40(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	movq	48(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rsi
	popq	%rdi
	je	.L66
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L195
.L65:
	testl	%r10d, %r10d
	jne	.L122
.L92:
	movq	112(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	(%r12), %r14
	movq	%rax, 40(%rsp)
	movq	56(%rsp), %rax
	movq	(%rdi), %r13
	movq	96(%rax), %rax
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L55:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L196
	cmpq	$0, 80(%rsp)
	je	.L112
	testb	$8, 16(%r12)
	jne	.L197
.L59:
	testl	%r11d, %r11d
	jne	.L198
.L112:
	movl	$6, 12(%rsp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L192:
	cmpq	%r13, %rbp
	je	.L106
	movq	(%rsp), %rax
	leaq	4(%r14), %rdx
	cmpq	%rax, %rdx
	ja	.L107
	subq	%r14, %rax
	subq	$4, %rax
	shrq	$2, %rax
	leaq	1(%r13,%rax), %rcx
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rdx, %rbx
	movzbl	(%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %rbp
	movl	(%r15,%rdx,4), %edx
	movl	%edx, -4(%rbx)
	je	.L199
	cmpq	%rax, %rcx
	leaq	4(%rbx), %rdx
	jne	.L50
	movl	$5, 12(%rsp)
.L49:
	cmpq	$0, 48(%rsp)
	movq	16(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L62
.L193:
	movq	48(%rsp), %rax
	movq	%rbx, (%rax)
.L8:
	movl	12(%rsp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$7, 12(%rsp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L66:
	movl	12(%rsp), %r10d
	cmpl	$5, %r10d
	jne	.L65
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$5, 12(%rsp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rbp, %rcx
	movl	$4, 12(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L194:
	movq	24(%rsp), %rcx
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rcx)
.L64:
	movq	56(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 107(%rsp)
	je	.L8
	cmpl	$7, 12(%rsp)
	jne	.L8
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L95
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L97
.L96:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L96
.L97:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L113:
	movl	12(%rsp), %r10d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L197:
	movl	%r11d, 108(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%r10, %rcx
	movq	104(%rsp), %r9
	movq	72(%rsp), %rdi
	movq	%r12, %rsi
	movq	%r10, 112(%rsp)
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 28(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	96(%rsp), %r10
	movl	108(%rsp), %r11d
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	je	.L59
	cmpl	$5, 12(%rsp)
	jne	.L53
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rcx, 128(%rsp)
	movq	%rcx, %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L198:
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movl	$6, 12(%rsp)
	movq	%rax, 128(%rsp)
	addq	$1, (%rcx)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L195:
	movq	24(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L68
	movq	(%rsi), %rax
.L68:
	addq	112(%rsp), %rax
	cmpq	40(%rsp), %rax
	movq	16(%rsp), %rax
	je	.L200
	movq	%r13, (%rax)
	movq	56(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L201
	movl	16(%r12), %ecx
	leaq	128(%rsp), %rbx
	movq	%r13, 128(%rsp)
	movq	%r14, 136(%rsp)
	movq	%r14, %rdx
	movl	$4, %eax
	andl	$2, %ecx
	movl	%ecx, 12(%rsp)
.L81:
	cmpq	%r13, %rbp
	je	.L202
.L89:
	leaq	4(%r13), %rdi
	cmpq	%rdi, %rbp
	jb	.L116
	cmpq	%rdx, %r11
	jbe	.L119
	movl	0(%r13), %ecx
	cmpl	$351, %ecx
	ja	.L83
	leaq	from_ucs4(%rip), %r9
	movl	%ecx, %esi
	movzbl	(%r9,%rsi), %esi
	testb	%sil, %sil
	jne	.L84
	testl	%ecx, %ecx
	jne	.L83
.L84:
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%sil, (%rdx)
	movq	128(%rsp), %rcx
	movq	136(%rsp), %rdx
	leaq	4(%rcx), %r13
	cmpq	%r13, %rbp
	movq	%r13, 128(%rsp)
	jne	.L89
.L202:
	cltq
	movq	%rbp, %r13
.L82:
	movq	16(%rsp), %rsi
	movq	120(%rsp), %r11
	movq	%r13, (%rsi)
.L80:
	cmpq	%r11, %rdx
	jne	.L74
	cmpq	$5, %rax
	jne	.L73
	cmpq	%rdx, %r14
	jne	.L65
.L76:
	subl	$1, 20(%r12)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L191:
	testq	%rdi, %rdi
	jne	.L203
	cmpl	$4, %edx
	movq	%r13, 128(%rsp)
	movq	%r14, 136(%rsp)
	ja	.L18
	movslq	%edx, %rdx
	leaq	120(%rsp), %r11
	xorl	%eax, %eax
	movq	%rdx, %r10
.L19:
	movzbl	4(%rbx,%rax), %ecx
	movb	%cl, (%r11,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L19
	movq	%r13, %rax
	subq	%rdx, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L204
	cmpq	(%rsp), %r14
	jb	.L25
	movl	$5, 12(%rsp)
	jmp	.L8
.L200:
	movq	56(%rsp), %rsi
	subq	%r11, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rsi)
	je	.L205
	movq	16(%rsp), %rsi
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r13, %rcx
	movq	%r14, %rbx
	movl	$5, 12(%rsp)
	jmp	.L49
.L190:
	cmpq	$0, 48(%rsp)
	jne	.L206
	movq	32(%r12), %rax
	movl	$0, 12(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L8
	movq	32(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	40(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	*%r15
	movl	%eax, 28(%rsp)
	popq	%rbp
	popq	%r12
	jmp	.L8
.L106:
	movq	%rbp, %rcx
	movq	%r14, %rbx
	movl	$4, 12(%rsp)
	jmp	.L49
.L122:
	movl	%r10d, 12(%rsp)
	jmp	.L64
.L25:
	leaq	1(%r13), %rax
	leaq	119(%rsp), %r8
.L27:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r10
	movq	%rax, %rdi
	addq	$1, %rax
	cmpq	$3, %r10
	movb	%cl, (%r8,%r10)
	ja	.L124
	cmpq	%rdi, %rbp
	ja	.L27
.L124:
	movl	120(%rsp), %eax
	movq	%r11, 128(%rsp)
	cmpl	$351, %eax
	ja	.L29
	leaq	from_ucs4(%rip), %rdi
	movl	%eax, %ecx
	movzbl	(%rdi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L30
	testl	%eax, %eax
	je	.L30
.L29:
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L207
	cmpq	$0, 80(%rsp)
	je	.L104
	testb	$8, %r15b
	jne	.L208
	andl	$2, %r15d
	movl	$6, 12(%rsp)
	movq	%r11, %rax
	je	.L8
.L37:
	movq	80(%rsp), %rcx
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rcx)
.L38:
	cmpq	%r11, %rax
	jne	.L189
.L104:
	movl	$6, 12(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	1(%r14), %rax
	movq	%rax, 136(%rsp)
	movb	%cl, (%r14)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%r11, %rax
	movq	%rax, 128(%rsp)
	je	.L39
.L189:
	movl	(%rbx), %esi
	movl	%esi, %edx
	andl	$7, %edx
.L32:
	subq	%r11, %rax
	cmpq	%rdx, %rax
	jle	.L209
	movq	16(%rsp), %rcx
	subq	%rdx, %rax
	andl	$-8, %esi
	movq	136(%rsp), %r14
	addq	(%rcx), %rax
	movq	%rax, (%rcx)
	movq	%rax, %r13
	movq	112(%rsp), %rax
	movl	%esi, (%rbx)
	movq	%rax, 40(%rsp)
	movq	56(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L16
.L205:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	movq	16(%rsp), %rsi
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rsi)
	jmp	.L65
.L83:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L210
	cmpq	$0, 80(%rsp)
	je	.L120
	testb	$8, 16(%r12)
	jne	.L211
.L87:
	movl	12(%rsp), %eax
	testl	%eax, %eax
	jne	.L212
.L120:
	movl	$6, %eax
	jmp	.L82
.L201:
	cmpq	%r13, %rbp
	je	.L213
	leaq	4(%r14), %rax
	cmpq	%r11, %rax
	ja	.L214
	movq	%r11, %rdx
	subq	%r14, %rdx
	subq	$4, %rdx
	shrq	$2, %rdx
	leaq	1(%r13,%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rax, %rdx
	movzbl	0(%r13), %eax
	addq	$1, %r13
	cmpq	%r13, %rbp
	movl	(%r15,%rax,4), %eax
	movl	%eax, -4(%rdx)
	je	.L215
	cmpq	%r13, %rcx
	leaq	4(%rdx), %rax
	jne	.L77
	movl	$5, %eax
.L78:
	movq	16(%rsp), %rsi
	movq	%rcx, (%rsi)
	jmp	.L80
.L116:
	movl	$7, %eax
	jmp	.L82
.L211:
	movq	%r11, 96(%rsp)
	movl	%r10d, 40(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	32(%rsp), %rax
	movq	%rbx, %rcx
	movq	104(%rsp), %r9
	movq	72(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rsi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r13
	movq	136(%rsp), %rdx
	movl	40(%rsp), %r10d
	movq	96(%rsp), %r11
	je	.L87
	cmpl	$5, %eax
	jne	.L81
.L119:
	movl	$5, %eax
	jmp	.L82
.L204:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rdx, %rax
	cmpq	$4, %rax
	ja	.L21
	addq	$1, %r13
	cmpq	%rax, %rdx
	jnb	.L23
.L24:
	movq	%r13, 128(%rsp)
	movzbl	-1(%r13), %edx
	addq	$1, %r13
	movb	%dl, 4(%rbx,%r10)
	addq	$1, %r10
	cmpq	%r10, %rax
	jne	.L24
.L23:
	movl	$7, 12(%rsp)
	jmp	.L8
.L215:
	movq	%rbp, %rcx
	movl	$4, %eax
	jmp	.L78
.L212:
	movq	80(%rsp), %rax
	addq	$4, %r13
	movq	%r13, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L81
.L210:
	movq	%rdi, 128(%rsp)
	movq	%rdi, %r13
	jmp	.L81
.L42:
	cmpl	$0, 12(%rsp)
	jne	.L8
.L39:
	movq	112(%rsp), %rax
	movq	16(%rsp), %rcx
	movq	%rax, 40(%rsp)
	movq	56(%rsp), %rax
	movq	(%rcx), %r13
	movq	96(%rax), %rax
	jmp	.L16
.L214:
	cmpq	%r14, %r11
	je	.L76
.L74:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	cmpq	%r14, %r11
	jne	.L74
.L73:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	(%r11,%r10), %rax
	movq	%r10, 96(%rsp)
	movq	%r11, 88(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rax, 48(%rsp)
	pushq	88(%rsp)
	movq	%rax, %r8
	movq	72(%rsp), %rdi
	movq	%r12, %rsi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 28(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	88(%rsp), %r11
	movq	96(%rsp), %r10
	je	.L216
	movq	128(%rsp), %rax
	cmpq	%r11, %rax
	jne	.L189
	cmpl	$7, 12(%rsp)
	jne	.L42
	leaq	4(%r11), %rax
	cmpq	%rax, 40(%rsp)
	je	.L217
	movl	(%rbx), %eax
	movq	16(%rsp), %rsi
	movq	%r10, %rcx
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rcx
	movslq	%eax, %rdx
	addq	%rcx, (%rsi)
	cmpq	%rdx, %r10
	jle	.L218
	cmpq	$4, %r10
	ja	.L219
	orl	%r10d, %eax
	testq	%r10, %r10
	movl	%eax, (%rbx)
	je	.L23
	xorl	%eax, %eax
.L46:
	movzbl	(%r11,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L46
	jmp	.L23
.L207:
	leaq	4(%r11), %rax
	movq	%rax, 128(%rsp)
	jmp	.L32
.L216:
	andl	$2, %r15d
	movq	128(%rsp), %rax
	je	.L38
	jmp	.L37
.L95:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L206:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L18:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L218:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L217:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L203:
	leaq	__PRETTY_FUNCTION__.9138(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L209:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L21:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L219:
	leaq	__PRETTY_FUNCTION__.9064(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9064, @object
	.size	__PRETTY_FUNCTION__.9064, 18
__PRETTY_FUNCTION__.9064:
	.string	"to_generic_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9138, @object
	.size	__PRETTY_FUNCTION__.9138, 6
__PRETTY_FUNCTION__.9138:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 352
from_ucs4:
	.zero	1
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
	.byte	64
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	92
	.byte	93
	.byte	94
	.byte	95
	.byte	96
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	123
	.byte	124
	.byte	125
	.byte	126
	.byte	127
	.byte	-128
	.byte	-127
	.byte	-126
	.byte	-125
	.byte	-124
	.byte	-123
	.byte	-122
	.byte	-121
	.byte	-120
	.byte	-119
	.byte	-118
	.byte	-117
	.byte	-116
	.byte	-115
	.byte	-114
	.byte	-113
	.byte	-112
	.byte	-111
	.byte	-110
	.byte	-109
	.byte	-108
	.byte	-107
	.byte	-106
	.byte	-105
	.byte	-104
	.byte	-103
	.byte	-102
	.byte	-101
	.byte	-100
	.byte	-99
	.byte	-98
	.byte	-97
	.byte	-96
	.byte	-95
	.byte	-94
	.byte	-93
	.byte	-92
	.byte	-91
	.byte	-90
	.byte	-89
	.byte	-88
	.byte	-87
	.byte	-86
	.byte	-85
	.byte	-84
	.byte	-83
	.byte	-82
	.byte	-81
	.byte	-80
	.byte	-79
	.byte	-78
	.byte	-77
	.byte	-76
	.byte	-75
	.byte	-74
	.byte	-73
	.byte	-72
	.byte	-71
	.byte	-70
	.byte	-69
	.byte	-68
	.byte	-67
	.byte	-66
	.byte	-65
	.byte	-64
	.byte	-63
	.byte	-62
	.byte	-61
	.byte	-60
	.byte	-59
	.byte	-58
	.byte	-57
	.byte	-56
	.byte	-55
	.byte	-54
	.byte	-53
	.byte	-52
	.byte	-51
	.byte	-50
	.byte	-49
	.zero	1
	.byte	-47
	.byte	-46
	.byte	-45
	.byte	-44
	.byte	-43
	.byte	-42
	.byte	-41
	.byte	-40
	.byte	-39
	.byte	-38
	.byte	-37
	.byte	-36
	.zero	2
	.byte	-33
	.byte	-32
	.byte	-31
	.byte	-30
	.byte	-29
	.byte	-28
	.byte	-27
	.byte	-26
	.byte	-25
	.byte	-24
	.byte	-23
	.byte	-22
	.byte	-21
	.byte	-20
	.byte	-19
	.byte	-18
	.byte	-17
	.zero	1
	.byte	-15
	.byte	-14
	.byte	-13
	.byte	-12
	.byte	-11
	.byte	-10
	.byte	-9
	.byte	-8
	.byte	-7
	.byte	-6
	.byte	-5
	.byte	-4
	.zero	2
	.byte	-1
	.zero	30
	.byte	-48
	.byte	-16
	.zero	16
	.byte	-35
	.byte	-3
	.zero	44
	.byte	-34
	.byte	-2
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.zero	4
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	33
	.long	34
	.long	35
	.long	36
	.long	37
	.long	38
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.long	63
	.long	64
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.long	92
	.long	93
	.long	94
	.long	95
	.long	96
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.long	123
	.long	124
	.long	125
	.long	126
	.long	127
	.long	128
	.long	129
	.long	130
	.long	131
	.long	132
	.long	133
	.long	134
	.long	135
	.long	136
	.long	137
	.long	138
	.long	139
	.long	140
	.long	141
	.long	142
	.long	143
	.long	144
	.long	145
	.long	146
	.long	147
	.long	148
	.long	149
	.long	150
	.long	151
	.long	152
	.long	153
	.long	154
	.long	155
	.long	156
	.long	157
	.long	158
	.long	159
	.long	160
	.long	161
	.long	162
	.long	163
	.long	164
	.long	165
	.long	166
	.long	167
	.long	168
	.long	169
	.long	170
	.long	171
	.long	172
	.long	173
	.long	174
	.long	175
	.long	176
	.long	177
	.long	178
	.long	179
	.long	180
	.long	181
	.long	182
	.long	183
	.long	184
	.long	185
	.long	186
	.long	187
	.long	188
	.long	189
	.long	190
	.long	191
	.long	192
	.long	193
	.long	194
	.long	195
	.long	196
	.long	197
	.long	198
	.long	199
	.long	200
	.long	201
	.long	202
	.long	203
	.long	204
	.long	205
	.long	206
	.long	207
	.long	286
	.long	209
	.long	210
	.long	211
	.long	212
	.long	213
	.long	214
	.long	215
	.long	216
	.long	217
	.long	218
	.long	219
	.long	220
	.long	304
	.long	350
	.long	223
	.long	224
	.long	225
	.long	226
	.long	227
	.long	228
	.long	229
	.long	230
	.long	231
	.long	232
	.long	233
	.long	234
	.long	235
	.long	236
	.long	237
	.long	238
	.long	239
	.long	287
	.long	241
	.long	242
	.long	243
	.long	244
	.long	245
	.long	246
	.long	247
	.long	248
	.long	249
	.long	250
	.long	251
	.long	252
	.long	305
	.long	351
	.long	255
