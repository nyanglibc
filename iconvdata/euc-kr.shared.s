	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	movzbl	%sil, %eax
	cmpb	$-97, %sil
	movl	$-1, %edx
	cmova	%edx, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EUC-KR//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L6
	movabsq	$8589934593, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$17179869188, %rdi
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbx
	subq	$152, %rsp
	movl	16(%rbp), %r12d
	movq	%rsi, 56(%rsp)
	leaq	48(%rbp), %rsi
	movq	%rdi, 32(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 24(%rsp)
	testb	$1, %r12b
	movq	%r9, 40(%rsp)
	movl	208(%rsp), %r13d
	movq	%rsi, 64(%rsp)
	movq	$0, 48(%rsp)
	jne	.L11
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 48(%rsp)
	je	.L11
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 48(%rsp)
.L11:
	testl	%r13d, %r13d
	jne	.L345
	movq	16(%rsp), %rax
	movq	24(%rsp), %rsi
	leaq	112(%rsp), %rdx
	movl	216(%rsp), %r13d
	movq	8(%rbp), %r14
	testq	%rsi, %rsi
	movq	(%rax), %r15
	movq	%rsi, %rax
	cmove	%rbp, %rax
	cmpq	$0, 40(%rsp)
	movq	(%rax), %rax
	movq	$0, 112(%rsp)
	movq	%rax, (%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	testl	%r13d, %r13d
	movq	%rax, 72(%rsp)
	jne	.L346
.L18:
	leaq	136(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 88(%rsp)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L347
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rsp), %rax
	movq	%r15, 128(%rsp)
	movq	%r15, %rsi
	movl	$4, 8(%rsp)
	movq	%rax, 136(%rsp)
	movq	%rax, %r13
.L102:
	cmpq	%rsi, %rbx
	je	.L103
	leaq	4(%rsi), %r9
	cmpq	%r9, %rbx
	jb	.L215
	cmpq	%r13, %r14
	jbe	.L216
	movl	(%rsi), %ecx
	cmpl	$159, %ecx
	jbe	.L104
	cmpl	$8361, %ecx
	je	.L105
	leal	-44032(%rcx), %eax
	cmpl	$11171, %eax
	jbe	.L348
	leal	-19968(%rcx), %eax
	xorl	%edi, %edi
	cmpl	$20991, %eax
	jbe	.L244
	leal	-63744(%rcx), %eax
	cmpl	$267, %eax
	jbe	.L244
	movl	$988, %edx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L349:
	leal	-1(%rax), %edx
	cmpl	%edi, %edx
	jl	.L112
.L113:
	leal	(%rdi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r10
	movzwl	(%r8,%r10,4), %r11d
	cmpl	%r11d, %ecx
	jb	.L349
	jbe	.L121
	leal	1(%rax), %edi
	cmpl	%edi, %edx
	jge	.L113
	.p2align 4,,10
	.p2align 3
.L112:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L350
	cmpq	$0, 72(%rsp)
	je	.L219
	testb	$8, 16(%rbp)
	jne	.L351
.L127:
	testb	$2, %r12b
	jne	.L352
.L219:
	movl	$6, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	$0, 24(%rsp)
	movq	16(%rsp), %rax
	movq	%rsi, (%rax)
	jne	.L353
.L132:
	addl	$1, 20(%rbp)
	testb	$1, 16(%rbp)
	jne	.L354
	cmpq	%r13, (%rsp)
	jnb	.L220
	movq	48(%rsp), %r12
	movq	0(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	$0
	movq	56(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r12
	cmpl	$4, %eax
	movl	%eax, %r12d
	popq	%rsi
	popq	%rdi
	je	.L136
	movq	120(%rsp), %r10
	cmpq	%r13, %r10
	jne	.L355
.L135:
	testl	%r12d, %r12d
	jne	.L235
.L187:
	movq	16(%rsp), %rax
	movl	16(%rbp), %r12d
	movq	(%rax), %r15
	movq	0(%rbp), %rax
	movq	%rax, (%rsp)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L89
.L347:
	cmpq	%r15, %rbx
	movq	(%rsp), %r13
	je	.L207
	leaq	4(%r13), %rcx
	cmpq	%rcx, %r14
	jb	.L208
	movq	%r15, %rax
	movl	$4, 8(%rsp)
	andl	$2, %r12d
.L91:
	movzbl	(%rax), %edx
	cmpl	$159, %edx
	ja	.L92
	addq	$1, %rax
.L93:
	movl	%edx, 0(%r13)
	movq	%rcx, %r13
.L95:
	cmpq	%rax, %rbx
	je	.L90
	leaq	4(%r13), %rcx
	cmpq	%rcx, %r14
	jnb	.L91
	movl	$5, 8(%rsp)
.L90:
	movq	16(%rsp), %rsi
	movq	%rax, (%rsi)
	.p2align 4,,10
	.p2align 3
.L362:
	cmpq	$0, 24(%rsp)
	je	.L132
.L353:
	movq	24(%rsp), %rax
	movq	%r13, (%rax)
.L10:
	movl	8(%rsp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	testl	%ecx, %ecx
	je	.L356
	leaq	1(%r13), %rax
	movq	%rax, 136(%rsp)
	movb	%cl, 0(%r13)
.L129:
	movq	128(%rsp), %rax
	movq	136(%rsp), %r13
	leaq	4(%rax), %rsi
	movq	%rsi, 128(%rsp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L348:
	xorl	%r10d, %r10d
	movl	$2349, %r8d
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r11
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L357:
	leal	-1(%rdi), %r8d
.L108:
	cmpl	%r10d, %r8d
	jl	.L112
.L111:
	leal	(%r10,%r8), %edx
	movl	%edx, %edi
	sarl	%edi
	movslq	%edi, %rax
	movzwl	(%r11,%rax,2), %eax
	cmpl	%eax, %ecx
	jb	.L357
	jbe	.L109
	leal	1(%rdi), %r10d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$4887, %edx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L358:
	leal	-1(%rax), %edx
.L116:
	cmpl	%edi, %edx
	jl	.L112
.L118:
	leal	(%rdi,%rdx), %eax
	sarl	%eax
	movslq	%eax, %r10
	movzwl	(%r8,%r10,4), %r11d
	cmpl	%r11d, %ecx
	jb	.L358
	jbe	.L121
	leal	1(%rax), %edi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	$160, %edx
	je	.L359
	movq	%rbx, %rdi
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jbe	.L212
	testb	%dl, %dl
	js	.L360
.L96:
	cmpq	$0, 72(%rsp)
	je	.L214
	testl	%r12d, %r12d
	jne	.L361
.L214:
	movq	16(%rsp), %rsi
	movl	$6, 8(%rsp)
	movq	%rax, (%rsi)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L215:
	movl	$7, 8(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L136:
	movl	8(%rsp), %r12d
	cmpl	$5, %r12d
	jne	.L135
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$5, 8(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L212:
	movq	16(%rsp), %rsi
	movl	$7, 8(%rsp)
	movq	%rax, (%rsi)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L121:
	movzbl	2(%r8,%r10,4), %ecx
	movzbl	3(%r8,%r10,4), %edi
.L110:
	leaq	1(%r13), %rax
	orl	$-128, %ecx
	orl	$-128, %edi
	movq	%rax, 136(%rsp)
	movb	%cl, 0(%r13)
	movq	136(%rsp), %r13
	cmpq	%r13, %r14
	jbe	.L363
.L130:
	leaq	1(%r13), %rax
	movq	%rax, 136(%rsp)
	movb	%dil, 0(%r13)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L360:
	leal	-128(%rdx), %esi
	subl	$161, %edx
	cmpl	$92, %edx
	ja	.L96
	cmpl	$73, %esi
	je	.L96
	movzbl	1(%rax), %esi
	leal	95(%rsi), %edi
	cmpb	$93, %dil
	ja	.L96
	imull	$94, %edx, %edx
	leal	-161(%rdx,%rsi), %edx
	leal	-1410(%rdx), %esi
	cmpl	$2349, %esi
	ja	.L97
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rdx
	movslq	%esi, %rsi
	movzwl	(%rdx,%rsi,2), %edx
	testw	%dx, %dx
	je	.L96
.L98:
	addq	$2, %rax
	cmpl	$65533, %edx
	jne	.L93
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L346:
	movq	32(%rbp), %rax
	movl	(%rax), %r10d
	movq	%rax, 80(%rsp)
	movl	%r10d, %edx
	andl	$7, %edx
	je	.L18
	testq	%rsi, %rsi
	jne	.L364
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L365
	movq	(%rsp), %rax
	cmpl	$4, %edx
	movq	%r15, 128(%rsp)
	movq	%rax, 136(%rsp)
	ja	.L39
	movq	80(%rsp), %rsi
	movslq	%edx, %rdx
	leaq	120(%rsp), %r11
	movq	%rdx, %r13
	xorl	%eax, %eax
.L40:
	movzbl	4(%rsi,%rax), %ecx
	movb	%cl, (%r11,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L40
	movq	%r15, %rcx
	subq	%rax, %rcx
	addq	$4, %rcx
	cmpq	%rcx, %rbx
	jb	.L366
	cmpq	%r14, (%rsp)
	jnb	.L78
	leaq	119(%rsp), %rsi
	movq	%r15, %rax
.L47:
	addq	$1, %rax
	addq	$1, %r13
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	cmpq	$3, %r13
	movb	%cl, (%rsi,%r13)
	ja	.L242
	cmpq	%rax, %rbx
	ja	.L47
.L242:
	movl	120(%rsp), %r9d
	movq	%r11, 128(%rsp)
	cmpl	$159, %r9d
	jbe	.L49
	cmpl	$8361, %r9d
	je	.L50
	leal	-44032(%r9), %eax
	cmpl	$11171, %eax
	ja	.L51
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rax
	movl	%r12d, 8(%rsp)
	xorl	%edi, %edi
	movl	$2349, %esi
	movq	%rax, %r12
	jmp	.L56
.L368:
	leal	-1(%rcx), %esi
.L53:
	cmpl	%edi, %esi
	jl	.L367
.L56:
	leal	(%rdi,%rsi), %eax
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %r8
	movzwl	(%r12,%r8,2), %r8d
	cmpl	%r8d, %r9d
	jb	.L368
	jbe	.L54
	leal	1(%rcx), %edi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	1(%r13), %rax
	movq	%rax, 136(%rsp)
	movb	$0, 0(%r13)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	1(%r13), %rax
	movl	$-36, %edi
	movq	%rax, 136(%rsp)
	movb	$-93, 0(%r13)
	movq	136(%rsp), %r13
	cmpq	%r13, %r14
	ja	.L130
	.p2align 4,,10
	.p2align 3
.L363:
	subq	$1, %r13
	movq	128(%rsp), %rsi
	movl	$5, 8(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$-1370734243, %eax
	mull	%edx
	movl	$-1370734243, %eax
	shrl	$7, %edx
	leal	48(%rdx), %ecx
	mull	%edi
	shrl	$6, %edx
	imull	$94, %edx, %edx
	subl	%edx, %edi
	addl	$33, %edi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L354:
	movq	40(%rsp), %rsi
	movq	%r13, 0(%rbp)
	movq	112(%rsp), %rax
	addq	%rax, (%rsi)
.L134:
	cmpl	$7, 8(%rsp)
	jne	.L10
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L10
	movq	16(%rsp), %rax
	movq	%rbx, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L189
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%rbp), %rsi
	je	.L191
.L190:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L190
.L191:
	movq	16(%rsp), %rax
	movq	%rbx, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L220:
	movl	8(%rsp), %r12d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L359:
	cmpq	$0, 72(%rsp)
	je	.L214
	testl	%r12d, %r12d
	je	.L214
	movq	72(%rsp), %rsi
	addq	$1, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rsi)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L355:
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	movl	16(%rbp), %eax
	movl	%eax, 8(%rsp)
	movq	32(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L369
	movq	(%rsp), %rax
	movq	%r15, 128(%rsp)
	movl	$4, 96(%rsp)
	movq	%rax, 136(%rsp)
	movq	%rax, %r11
.L155:
	cmpq	%r15, %rbx
	je	.L370
	leaq	4(%r15), %rdi
	cmpq	%rdi, %rbx
	jb	.L228
	cmpq	%r11, %r10
	jbe	.L232
	movl	(%r15), %edx
	cmpl	$159, %edx
	jbe	.L157
	cmpl	$8361, %edx
	je	.L158
	leal	-44032(%rdx), %eax
	cmpl	$11171, %eax
	jbe	.L371
	leal	-19968(%rdx), %eax
	xorl	%esi, %esi
	cmpl	$20991, %eax
	jbe	.L245
	leal	-63744(%rdx), %eax
	cmpl	$267, %eax
	jbe	.L245
	movl	$988, %ecx
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L372:
	leal	-1(%rax), %ecx
.L173:
	cmpl	%esi, %ecx
	jl	.L165
.L166:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %r9
	movzwl	(%r8,%r9,4), %r13d
	cmpl	%r13d, %edx
	jb	.L372
	jbe	.L174
	leal	1(%rax), %esi
	jmp	.L173
.L352:
	movq	72(%rsp), %rax
	addq	$4, %rsi
	movl	$6, 8(%rsp)
	movq	%rsi, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L102
.L361:
	movq	72(%rsp), %rsi
	addq	$2, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rsi)
	jmp	.L95
.L365:
	cmpl	$4, %edx
	ja	.L21
	movq	80(%rsp), %rsi
	cmpl	$1, %edx
	movl	$1, %r8d
	movzbl	4(%rsi), %eax
	movb	%al, 136(%rsp)
	je	.L22
	movzbl	5(%rsi), %eax
	movl	$2, %r8d
	movb	%al, 137(%rsp)
.L22:
	movq	(%rsp), %rax
	leaq	4(%rax), %rsi
	cmpq	%rsi, %r14
	jb	.L78
	movzbl	(%r15), %eax
	movb	%al, 136(%rsp,%r8)
	movzbl	136(%rsp), %ecx
	cmpl	$159, %ecx
	jbe	.L25
	cmpl	$160, %ecx
	je	.L373
	addq	$1, %r8
	leaq	136(%rsp), %rax
	cmpq	$1, %r8
	leaq	(%rax,%r8), %r11
	movq	%rax, %rdi
	jbe	.L29
	testb	%cl, %cl
	js	.L374
.L30:
	cmpq	$0, 72(%rsp)
	je	.L88
	andl	$2, %r12d
	je	.L88
.L198:
	movq	72(%rsp), %rsi
	addq	$2, %rax
	addq	$1, (%rsi)
.L28:
	subq	%rdi, %rax
	movslq	%edx, %rdx
	cmpq	%rdx, %rax
	jle	.L375
	subq	%rdx, %rax
	andl	$-8, %r10d
	movl	16(%rbp), %r12d
	addq	%rax, %r15
	movq	16(%rsp), %rax
	movq	%r15, (%rax)
	movq	80(%rsp), %rax
	movl	%r10d, (%rax)
	jmp	.L18
.L369:
	cmpq	%r15, %rbx
	je	.L376
	movq	(%rsp), %r11
	movl	8(%rsp), %r13d
	movl	$4, %ecx
	leaq	4(%r11), %rdx
	andl	$2, %r13d
	cmpq	%rdx, %r10
	jb	.L377
.L142:
	movzbl	(%r15), %eax
	cmpl	$159, %eax
	ja	.L145
	addq	$1, %r15
.L146:
	movl	%eax, (%r11)
	movq	%rdx, %r11
.L148:
	cmpq	%r15, %rbx
	je	.L378
	leaq	4(%r11), %rdx
	cmpq	%rdx, %r10
	jnb	.L142
	movl	$5, %eax
	jmp	.L144
.L396:
	movq	128(%rsp), %rax
	subq	$1, %rdx
	movq	%rdx, 136(%rsp)
	cmpq	%r11, %rax
	jne	.L342
.L78:
	movl	$5, 8(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L157:
	testl	%edx, %edx
	je	.L379
	leaq	1(%r11), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%r11)
.L182:
	movq	128(%rsp), %rax
	movq	136(%rsp), %r11
	leaq	4(%rax), %r15
	movq	%r15, 128(%rsp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L145:
	cmpl	$160, %eax
	je	.L380
	movq	%rbx, %rdi
	subq	%r15, %rdi
	cmpq	$1, %rdi
	jbe	.L225
	testb	%al, %al
	js	.L381
.L149:
	cmpq	$0, 72(%rsp)
	je	.L227
	testl	%r13d, %r13d
	jne	.L382
.L227:
	movl	$6, %eax
.L144:
	movq	16(%rsp), %rsi
	movq	%r15, (%rsi)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L208:
	movq	16(%rsp), %rsi
	movq	%r15, %rax
	movq	(%rsp), %r13
	movl	$5, 8(%rsp)
	movq	%rax, (%rsi)
	jmp	.L362
.L351:
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%rbp, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
	movq	48(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	128(%rsp), %rsi
	movq	136(%rsp), %r13
	je	.L127
	cmpl	$5, 8(%rsp)
	jne	.L102
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L345:
	cmpq	$0, 24(%rsp)
	jne	.L383
	movq	32(%rbp), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%rbp)
	jne	.L10
	movq	48(%rsp), %rbx
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%r13
	movq	56(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%rbx
	movl	%eax, 24(%rsp)
	popq	%r14
	popq	%r15
	jmp	.L10
.L235:
	movl	%r12d, 8(%rsp)
	jmp	.L134
.L350:
	movq	%r9, 128(%rsp)
	movq	%r9, %rsi
	jmp	.L102
.L371:
	xorl	%r8d, %r8d
	movl	$2349, %esi
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r13
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L384:
	leal	-1(%rcx), %esi
.L161:
	cmpl	%r8d, %esi
	jl	.L165
.L164:
	leal	(%r8,%rsi), %eax
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %r9
	movzwl	0(%r13,%r9,2), %r9d
	cmpl	%r9d, %edx
	jb	.L384
	jbe	.L162
	leal	1(%rcx), %r8d
	jmp	.L161
.L245:
	movl	$4887, %ecx
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %r8
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L385:
	leal	-1(%rax), %ecx
.L169:
	cmpl	%ecx, %esi
	jg	.L165
.L171:
	leal	(%rsi,%rcx), %eax
	sarl	%eax
	movslq	%eax, %r9
	movzwl	(%r8,%r9,4), %r13d
	cmpl	%r13d, %edx
	jb	.L385
	jbe	.L174
	leal	1(%rax), %esi
	jmp	.L169
.L228:
	movl	$7, %eax
.L156:
	movq	16(%rsp), %rsi
	movq	120(%rsp), %r10
	movq	%r15, (%rsi)
.L154:
	cmpq	%r11, %r10
	jne	.L141
	cmpq	$5, %rax
	jne	.L140
.L184:
	cmpq	(%rsp), %r10
	jne	.L135
.L143:
	subl	$1, 20(%rbp)
	jmp	.L135
.L49:
	testl	%r9d, %r9d
	je	.L386
	movq	(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 136(%rsp)
	movb	%r9b, (%rsi)
.L76:
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	%r11, %rax
	movq	%rax, 128(%rsp)
	je	.L343
.L342:
	movq	80(%rsp), %rsi
	movl	(%rsi), %r10d
	movl	%r10d, %edx
	andl	$7, %edx
.L71:
	subq	%r11, %rax
	cmpq	%rdx, %rax
	jle	.L387
	movq	16(%rsp), %rsi
	subq	%rdx, %rax
	andl	$-8, %r10d
	movl	16(%rbp), %r12d
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r15
	movq	136(%rsp), %rax
	movq	%rax, (%rsp)
	movq	80(%rsp), %rax
	movl	%r10d, (%rax)
	jmp	.L18
.L381:
	leal	-128(%rax), %esi
	subl	$161, %eax
	cmpl	$92, %eax
	ja	.L149
	cmpl	$73, %esi
	je	.L149
	movzbl	1(%r15), %esi
	leal	95(%rsi), %edi
	cmpb	$93, %dil
	ja	.L149
	imull	$94, %eax, %eax
	leal	-161(%rax,%rsi), %eax
	leaq	2(%r15), %rsi
	leal	-1410(%rax), %edi
	cmpl	$2349, %edi
	ja	.L150
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	testw	%ax, %ax
	je	.L149
.L151:
	cmpl	$65533, %eax
	movq	%rsi, %r15
	jne	.L146
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r10, 104(%rsp)
	subq	$8, %rsp
	movq	%rbx, %r8
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	%rbp, %rsi
	movq	96(%rsp), %r9
	movq	104(%rsp), %rcx
	movq	48(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 112(%rsp)
	cmpl	$6, %eax
	popq	%rdx
	popq	%rcx
	movq	128(%rsp), %r15
	movq	136(%rsp), %r11
	movq	104(%rsp), %r10
	je	.L180
	cmpl	$5, 96(%rsp)
	jne	.L155
.L232:
	movl	$5, %eax
	jmp	.L156
.L225:
	movl	$7, %eax
	jmp	.L144
.L174:
	movzbl	2(%r8,%r9,4), %esi
	movzbl	3(%r8,%r9,4), %eax
.L163:
	leaq	1(%r11), %rdx
	orl	$-128, %esi
	orl	$-128, %eax
	movq	%rdx, 136(%rsp)
	movb	%sil, (%r11)
.L175:
	movq	136(%rsp), %rdx
	cmpq	%rdx, %r10
	jbe	.L388
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	jmp	.L182
.L207:
	movq	%rbx, %rax
	movl	$4, 8(%rsp)
	jmp	.L90
.L162:
	movl	$-1370734243, %esi
	mull	%esi
	movl	%edx, %eax
	shrl	$7, %eax
	leal	48(%rax), %esi
	movl	$-1370734243, %eax
	mull	%ecx
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$33, %eax
	jmp	.L163
.L379:
	leaq	1(%r11), %rax
	movq	%rax, 136(%rsp)
	movb	$0, (%r11)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L165:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L389
	cmpq	$0, 72(%rsp)
	je	.L233
	testb	$8, 16(%rbp)
	jne	.L390
.L180:
	testb	$2, 8(%rsp)
	jne	.L391
.L233:
	movl	$6, %eax
	jmp	.L156
.L158:
	leaq	1(%r11), %rax
	movq	%rax, 136(%rsp)
	movb	$-93, (%r11)
	movl	$-36, %eax
	jmp	.L175
.L388:
	movq	120(%rsp), %r10
	movq	128(%rsp), %rax
	subq	$1, %rdx
	movq	16(%rsp), %rsi
	cmpq	%rdx, %r10
	movq	%rax, (%rsi)
	je	.L184
.L141:
	leaq	__PRETTY_FUNCTION__.9230(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	136(%rsp), %rdi
	leaq	1(%rdi), %rax
.L193:
	movq	(%rsp), %rdx
	movq	%rsi, (%rsp)
	movl	%ecx, (%rdx)
	movq	80(%rsp), %rcx
	movl	(%rcx), %r10d
	movl	%r10d, %edx
	andl	$7, %edx
	jmp	.L28
.L51:
	leal	-19968(%r9), %eax
	xorl	%esi, %esi
	cmpl	$20991, %eax
	jbe	.L243
	leal	-63744(%r9), %eax
	cmpl	$267, %eax
	jbe	.L243
	movq	__ksc5601_sym_from_ucs@GOTPCREL(%rip), %rax
	movl	$988, %ecx
	movq	%rax, 8(%rsp)
	jmp	.L58
.L392:
	leal	-1(%rax), %ecx
.L65:
	cmpl	%ecx, %esi
	jg	.L57
.L58:
	leal	(%rsi,%rcx), %eax
	movq	8(%rsp), %r8
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r8,%rdi,4), %r8d
	cmpl	%r8d, %r9d
	jb	.L392
	jbe	.L66
	leal	1(%rax), %esi
	jmp	.L65
.L366:
	movq	16(%rsp), %rsi
	movq	%rbx, (%rsi)
	subq	%r15, %rbx
	movq	%rbx, %rcx
	addq	%rax, %rcx
	cmpq	$4, %rcx
	ja	.L42
	cmpq	%rax, %rcx
	leaq	1(%r15), %rdx
	movq	80(%rsp), %rsi
	jbe	.L344
.L45:
	movq	%rdx, 128(%rsp)
	movzbl	-1(%rdx), %eax
	addq	$1, %rdx
	movb	%al, 4(%rsi,%r13)
	addq	$1, %r13
	cmpq	%r13, %rcx
	jne	.L45
.L344:
	movl	$7, 8(%rsp)
	jmp	.L10
.L380:
	cmpq	$0, 72(%rsp)
	je	.L227
	testl	%r13d, %r13d
	je	.L227
	movq	72(%rsp), %rax
	addq	$1, %r15
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L148
.L370:
	movslq	96(%rsp), %rax
	movq	%rbx, %r15
	jmp	.L156
.L367:
	movl	8(%rsp), %r12d
.L57:
	movl	%r9d, %eax
	shrl	$7, %eax
	cmpl	$7168, %eax
	je	.L393
	cmpq	$0, 72(%rsp)
	je	.L88
	testb	$8, %r12b
	jne	.L394
	andl	$2, %r12d
	je	.L88
	movq	%r11, %rdx
.L197:
	movq	72(%rsp), %rax
	addq	$1, (%rax)
	leaq	4(%rdx), %rax
	movq	%rax, 128(%rsp)
.L75:
	cmpq	%r11, %rax
	jne	.L342
.L88:
	movl	$6, 8(%rsp)
	jmp	.L10
.L243:
	movq	__ksc5601_hanja_from_ucs@GOTPCREL(%rip), %rax
	movl	$4887, %ecx
	movq	%rax, 8(%rsp)
	jmp	.L63
.L395:
	leal	-1(%rax), %ecx
.L61:
	cmpl	%esi, %ecx
	jl	.L57
.L63:
	leal	(%rsi,%rcx), %eax
	movq	8(%rsp), %r8
	sarl	%eax
	movslq	%eax, %rdi
	movzwl	(%r8,%rdi,4), %r8d
	cmpl	%r8d, %r9d
	jb	.L395
	jbe	.L66
	leal	1(%rax), %esi
	jmp	.L61
.L83:
	cmpl	$0, 8(%rsp)
	jne	.L10
.L343:
	movq	16(%rsp), %rax
	movl	16(%rbp), %r12d
	movq	(%rax), %r15
	jmp	.L18
.L377:
	cmpq	%r11, %r10
	je	.L143
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L378:
	movslq	%ecx, %rax
	movq	%rbx, %r15
	jmp	.L144
.L54:
	movl	$-1370734243, %edi
	mull	%edi
	movl	%edx, %eax
	shrl	$7, %eax
	leal	48(%rax), %esi
	movl	%ecx, %eax
	mull	%edi
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$94, %eax, %eax
	subl	%eax, %ecx
	movl	%ecx, %eax
	addl	$33, %eax
.L55:
	movq	(%rsp), %rcx
	orl	$-128, %esi
	orl	$-128, %eax
	leaq	1(%rcx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%sil, (%rcx)
.L67:
	movq	136(%rsp), %rdx
	cmpq	%rdx, %r14
	jbe	.L396
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%al, (%rdx)
	jmp	.L76
.L66:
	movq	8(%rsp), %rax
	movzbl	2(%rax,%rdi,4), %esi
	movzbl	3(%rax,%rdi,4), %eax
	jmp	.L55
.L382:
	movq	72(%rsp), %rax
	addq	$2, %r15
	movl	$6, %ecx
	addq	$1, (%rax)
	jmp	.L148
.L391:
	movq	72(%rsp), %rax
	addq	$4, %r15
	movl	$6, 96(%rsp)
	movq	%r15, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L155
.L386:
	movq	(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 136(%rsp)
	movb	$0, (%rsi)
	jmp	.L76
.L389:
	movq	%rdi, 128(%rsp)
	movq	%rdi, %r15
	jmp	.L155
.L97:
	cmpl	$3853, %edx
	jle	.L99
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rsi
	subl	$3854, %edx
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	testw	%dx, %dx
	je	.L96
	jmp	.L98
.L374:
	leal	-128(%rcx), %r8d
	subl	$161, %ecx
	cmpl	$92, %ecx
	ja	.L30
	cmpl	$73, %r8d
	je	.L30
	movzbl	137(%rsp), %r8d
	testb	%r8b, %r8b
	jns	.L30
	movzbl	%r8b, %r9d
	addl	$95, %r8d
	cmpb	$93, %r8b
	ja	.L30
	imull	$94, %ecx, %ecx
	leal	-161(%r9,%rcx), %r8d
	leal	-1410(%r8), %ecx
	cmpl	$2349, %ecx
	ja	.L31
	movq	__ksc5601_hangul_to_ucs@GOTPCREL(%rip), %r9
	movslq	%ecx, %rcx
	movzwl	(%r9,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L30
.L32:
	cmpl	$65533, %ecx
	je	.L192
	addq	$2, %rax
	jmp	.L193
.L50:
	movq	(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 136(%rsp)
	movb	$-93, (%rsi)
	movl	$-36, %eax
	jmp	.L67
.L29:
	addq	$2, %rax
	cmpq	%rax, %r11
	je	.L397
	movslq	%edx, %rdx
	movq	%r8, %rax
	movq	16(%rsp), %rbx
	subq	%rdx, %rax
	andl	$-8, %r10d
	addq	%r15, %rax
	movq	%rax, (%rbx)
	movslq	%r10d, %rax
	cmpq	%rax, %r8
	jle	.L398
	movq	80(%rsp), %rax
	orl	%r8d, %r10d
	testq	%r8, %r8
	movl	%r10d, (%rax)
	je	.L344
	movq	80(%rsp), %rax
	movb	%cl, 4(%rax)
	jmp	.L344
.L99:
	cmpl	$1114, %edx
	jg	.L96
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rsi
	movslq	%edx, %rdx
	movzwl	(%rsi,%rdx,2), %edx
	testw	%dx, %dx
	jne	.L98
	jmp	.L96
.L373:
	cmpq	$0, 72(%rsp)
	movl	$6, 8(%rsp)
	je	.L10
	andl	$2, %r12d
	je	.L10
	movq	72(%rsp), %rax
	leaq	136(%rsp), %rdi
	addq	$1, (%rax)
	leaq	1(%rdi), %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r11, 96(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	pushq	80(%rsp)
	leaq	(%r11,%r13), %r10
	movq	48(%rsp), %rdi
	movq	%r15, %rdx
	movq	%rbp, %rsi
	leaq	152(%rsp), %r9
	movq	%r10, %r8
	movq	%r10, 104(%rsp)
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	88(%rsp), %r10
	movq	96(%rsp), %r11
	movq	128(%rsp), %rax
	je	.L399
	cmpq	%r11, %rax
	jne	.L342
	cmpl	$7, 8(%rsp)
	jne	.L83
	leaq	4(%r11), %rax
	cmpq	%rax, %r10
	je	.L400
	movq	80(%rsp), %rax
	movq	%r13, %rbx
	movl	(%rax), %eax
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rbx
	movq	%rbx, %rdx
	movq	16(%rsp), %rbx
	addq	%rdx, (%rbx)
	movslq	%eax, %rdx
	cmpq	%rdx, %r13
	jle	.L401
	cmpq	$4, %r13
	ja	.L402
	movq	80(%rsp), %rbx
	orl	%r13d, %eax
	testq	%r13, %r13
	movl	%eax, (%rbx)
	je	.L344
	xorl	%eax, %eax
.L87:
	movzbl	(%r11,%rax), %edx
	movq	80(%rsp), %rbx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r13
	jne	.L87
	jmp	.L344
.L376:
	cmpq	(%rsp), %r10
	jne	.L141
.L140:
	leaq	__PRETTY_FUNCTION__.9230(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	4(%r11), %rax
	movq	%rax, 128(%rsp)
	jmp	.L71
.L375:
	leaq	__PRETTY_FUNCTION__.9071(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L39:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L364:
	leaq	__PRETTY_FUNCTION__.9230(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L398:
	leaq	__PRETTY_FUNCTION__.9071(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L397:
	leaq	__PRETTY_FUNCTION__.9071(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L189:
	leaq	__PRETTY_FUNCTION__.9230(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L387:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L383:
	leaq	__PRETTY_FUNCTION__.9230(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L150:
	cmpl	$3853, %eax
	jle	.L152
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %rdi
	subl	$3854, %eax
	cltq
	movzwl	(%rdi,%rax,2), %eax
	testw	%ax, %ax
	je	.L149
	jmp	.L151
.L402:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L152:
	cmpl	$1114, %eax
	jg	.L149
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rdi
	cltq
	movzwl	(%rdi,%rax,2), %eax
	testw	%ax, %ax
	je	.L149
	jmp	.L151
.L42:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L21:
	leaq	__PRETTY_FUNCTION__.9071(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L192:
	cmpq	$0, 72(%rsp)
	je	.L403
	addq	$2, %rax
	andb	$2, %r12b
	je	.L28
	jmp	.L198
.L31:
	cmpl	$3853, %r8d
	jle	.L33
	leal	-3854(%r8), %ecx
	movq	__ksc5601_hanja_to_ucs@GOTPCREL(%rip), %r8
	movslq	%ecx, %rcx
	movzwl	(%r8,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L30
	jmp	.L32
.L403:
	addq	$2, %rax
	jmp	.L28
.L33:
	cmpl	$1114, %r8d
	jg	.L30
	movq	__ksc5601_sym_to_ucs@GOTPCREL(%rip), %rcx
	movslq	%r8d, %r8
	movzwl	(%rcx,%r8,2), %ecx
	testw	%cx, %cx
	je	.L30
	jmp	.L32
.L401:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L400:
	leaq	__PRETTY_FUNCTION__.9155(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L399:
	andb	$2, %r12b
	movq	%rax, %rdx
	je	.L75
	jmp	.L197
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9155, @object
	.size	__PRETTY_FUNCTION__.9155, 17
__PRETTY_FUNCTION__.9155:
	.string	"to_euc_kr_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9071, @object
	.size	__PRETTY_FUNCTION__.9071, 19
__PRETTY_FUNCTION__.9071:
	.string	"from_euc_kr_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9230, @object
	.size	__PRETTY_FUNCTION__.9230, 6
__PRETTY_FUNCTION__.9230:
	.string	"gconv"
