	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GBK//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$6, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L2
	movabsq	$8589934593, %rdx
	movq	$0, 96(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdx, 80(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	32(%rax), %rsi
	movl	$6, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L5
	movabsq	$8589934593, %rdx
	movq	$-1, 96(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 72(%rax)
	movq	%rdx, 80(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC6:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC7:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC8:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC9:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC11:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %r13
	subq	$152, %rsp
	movl	16(%r12), %r10d
	movq	%rsi, 56(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 24(%rsp)
	testb	$1, %r10b
	movq	%r9, 48(%rsp)
	movl	208(%rsp), %ebx
	movq	%rsi, 64(%rsp)
	movq	$0, 16(%rsp)
	jne	.L8
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 16(%rsp)
	je	.L8
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 16(%rsp)
.L8:
	testl	%ebx, %ebx
	jne	.L194
	movq	8(%rsp), %rax
	movq	24(%rsp), %rdi
	leaq	112(%rsp), %rdx
	movq	8(%r12), %rbp
	testq	%rdi, %rdi
	movq	(%rax), %r14
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 48(%rsp)
	movq	(%rax), %r15
	movl	$0, %eax
	movq	$0, 112(%rsp)
	cmovne	%rdx, %rax
	movq	%rax, 80(%rsp)
	movl	216(%rsp), %eax
	testl	%eax, %eax
	jne	.L195
.L15:
	leaq	136(%rsp), %rax
	movq	%rax, 88(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 72(%rsp)
	.p2align 4,,10
	.p2align 3
.L53:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L54
	cmpq	%r13, %r14
	je	.L111
	cmpq	%r15, %rbp
	jbe	.L112
	movq	%r14, %rax
	movq	%r15, %rbx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L68:
	cmpq	%rcx, %r13
	leaq	1(%rbx), %rsi
	movb	%dl, (%rbx)
	je	.L196
	movq	%rsi, %rbx
	movq	%rcx, %rax
	cmpq	%rbx, %rbp
	jbe	.L118
.L56:
	movzbl	(%rax), %edx
	leaq	1(%rax), %rcx
	testb	%dl, %dl
	jns	.L68
	leaq	2(%rax), %rsi
	cmpq	%rsi, %r13
	jbe	.L119
	movq	%rbp, %rdi
	subq	%rbx, %rdi
	cmpq	$1, %rdi
	jle	.L120
	movb	%dl, (%rbx)
	movzbl	1(%rax), %eax
	addq	$2, %rbx
	movb	%al, -1(%rbx)
	cmpq	%rbx, %rbp
	movq	%rsi, %rax
	ja	.L56
.L118:
	movq	%rax, %rcx
	movl	$5, %r11d
.L55:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rax
	movq	%rcx, (%rax)
	je	.L70
.L197:
	movq	24(%rsp), %rax
	movq	%rbx, (%rax)
.L7:
	addq	$152, %rsp
	movl	%r11d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movl	16(%r12), %r10d
	movq	%r14, 128(%rsp)
	movq	%r14, %rax
	movq	%r15, 136(%rsp)
	movq	%r15, %rbx
	movl	$4, %r11d
	andl	$2, %r10d
.L57:
	cmpq	%rax, %r13
	je	.L58
.L66:
	cmpq	%rbx, %rbp
	jbe	.L115
	cmpb	$0, (%rax)
	leaq	1(%rax), %rdx
	js	.L59
	movq	%rdx, 128(%rsp)
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movzbl	(%rax), %edx
	movb	%dl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	cmpq	%rax, %r13
	jne	.L66
	.p2align 4,,10
	.p2align 3
.L58:
	cmpq	$0, 24(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L197
.L70:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L198
	cmpq	%rbx, %r15
	jnb	.L122
	movq	16(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r11d, 32(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %edi
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	64(%rsp), %r9
	movq	88(%rsp), %rdx
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	32(%rsp), %rax
	call	*%rax
	popq	%rsi
	cmpl	$4, %eax
	movl	%eax, %r10d
	popq	%rdi
	movl	32(%rsp), %r11d
	je	.L74
	movq	120(%rsp), %r11
	cmpq	%rbx, %r11
	jne	.L199
.L73:
	testl	%r10d, %r10d
	jne	.L126
.L98:
	movq	8(%rsp), %rax
	movq	(%r12), %r15
	movq	(%rax), %r14
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L59:
	cmpq	%rdx, %r13
	jbe	.L114
	movq	%rbp, %rcx
	subq	%rbx, %rcx
	cmpq	$1, %rcx
	jle	.L115
	movzwl	(%rax), %ecx
	movzbl	1(%rax), %edi
	rolw	$8, %cx
	cmpw	$-22460, %cx
	movzwl	%cx, %esi
	je	.L61
	leal	-41377(%rsi), %ecx
	cmpl	$22109, %ecx
	ja	.L62
	cmpb	$-96, %dil
	jbe	.L62
	cmpl	$41632, %esi
	ja	.L200
.L63:
	movq	%rdx, 128(%rsp)
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movzbl	(%rax), %eax
	movb	%al, (%rbx)
	movq	128(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$5, %r11d
	movl	%r11d, %r10d
	jne	.L73
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$5, %r11d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L195:
	movq	32(%r12), %rbx
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$7, %edx
	je	.L15
	testq	%rdi, %rdi
	jne	.L201
	movq	40(%rsp), %rsi
	cmpq	$0, 96(%rsi)
	je	.L202
	cmpl	$4, %edx
	ja	.L47
	movzbl	4(%rbx), %ecx
	cmpl	$1, %edx
	movb	%cl, 136(%rsp)
	movl	$1, %ecx
	je	.L48
	movzbl	5(%rbx), %ecx
	movb	%cl, 137(%rsp)
	movl	$2, %ecx
.L48:
	cmpq	%rbp, %r15
	jnb	.L49
	movzbl	(%r14), %esi
	movb	%sil, 136(%rsp,%rcx)
	movzbl	136(%rsp), %edi
	leaq	136(%rsp), %rsi
	leaq	1(%rsi), %r14
	testb	%dil, %dil
	js	.L203
.L50:
	movb	%dil, (%r15)
	movl	(%rbx), %eax
	addq	$1, %r15
	movl	%eax, %edx
	andl	$7, %edx
.L51:
	subq	%rsi, %r14
	movslq	%edx, %rdx
	cmpq	%rdx, %r14
	jle	.L204
	movq	8(%rsp), %rsi
	subq	%rdx, %r14
	andl	$-8, %eax
	addq	(%rsi), %r14
	movq	%r14, (%rsi)
	movl	%eax, (%rbx)
	jmp	.L15
.L200:
	cmpl	$41642, %esi
	jbe	.L62
	cmpl	$42719, %esi
	jbe	.L63
	cmpl	$42741, %esi
	jbe	.L62
	subl	$43195, %esi
	cmpl	$5, %esi
	ja	.L63
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	$0, 80(%rsp)
	je	.L117
	testb	$8, 16(%r12)
	jne	.L205
.L64:
	testl	%r10d, %r10d
	jne	.L206
.L117:
	movl	$6, %r11d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L119:
	movl	$7, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rsi, %rbx
	movl	$4, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$5, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L198:
	movq	48(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L72:
	movl	216(%rsp), %eax
	testl	%eax, %eax
	je	.L7
	cmpl	$7, %r11d
	jne	.L7
	movq	8(%rsp), %rax
	movq	%r13, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L100
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L102
.L101:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L101
.L102:
	movq	8(%rsp), %rax
	movq	%r13, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%r11d, %r10d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r15, %rbx
	movq	%r14, %rcx
	movl	$5, %r11d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$7, %r11d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L199:
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L76
	cmpq	%r13, %r14
	je	.L207
	cmpq	%r15, %r11
	movq	%r15, %rax
	ja	.L80
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	%rcx, %r13
	leaq	1(%rax), %rsi
	movb	%dl, (%rax)
	je	.L209
	movq	%rcx, %r14
	movq	%rsi, %rax
.L97:
	cmpq	%rax, %r11
	jbe	.L210
.L80:
	movzbl	(%r14), %edx
	leaq	1(%r14), %rcx
	testb	%dl, %dl
	jns	.L94
	leaq	2(%r14), %rsi
	cmpq	%rsi, %r13
	jbe	.L211
	movq	%r11, %rdi
	subq	%rax, %rdi
	cmpq	$1, %rdi
	jle	.L212
	movb	%dl, (%rax)
	movzbl	1(%r14), %edx
	addq	$2, %rax
	movq	%rsi, %r14
	movb	%dl, -1(%rax)
	jmp	.L97
.L202:
	cmpl	$4, %edx
	movq	%r14, 128(%rsp)
	movq	%r15, 136(%rsp)
	ja	.L18
	movzbl	4(%rbx), %eax
	cmpl	$1, %edx
	movb	%al, 120(%rsp)
	movl	$1, %eax
	je	.L19
	movzbl	5(%rbx), %eax
	movb	%al, 121(%rsp)
	movl	$2, %eax
.L19:
	cmpq	%rbp, %r15
	jnb	.L49
	leaq	1(%r14), %rdx
	leaq	120(%rsp), %rdi
	movq	%rdx, 128(%rsp)
	movzbl	(%r14), %edx
	movq	%rdi, 32(%rsp)
	movq	%rdi, 128(%rsp)
	movb	%dl, 120(%rsp,%rax)
	movzbl	120(%rsp), %edx
	testb	%dl, %dl
	js	.L22
	leaq	121(%rsp), %rax
	movq	%rax, 128(%rsp)
	leaq	1(%r15), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%r15)
	movq	128(%rsp), %rax
	cmpq	%rdi, %rax
	je	.L193
.L23:
	movl	(%rbx), %edx
	subq	32(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L213
	movq	8(%rsp), %rdi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r15
	addq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	%rax, %r14
	movl	%edx, (%rbx)
	jmp	.L15
.L76:
	movl	16(%r12), %ebx
	movq	%r14, 128(%rsp)
	movq	%r15, %rdx
	movq	%r15, 136(%rsp)
	andl	$2, %ebx
.L82:
	cmpq	%r14, %r13
	je	.L86
.L92:
	cmpq	%rdx, %r11
	jbe	.L83
	cmpb	$0, (%r14)
	leaq	1(%r14), %rax
	js	.L84
	movq	%rax, 128(%rsp)
	leaq	1(%rdx), %rax
	movq	%rax, 136(%rsp)
	movzbl	(%r14), %eax
	movb	%al, (%rdx)
	movq	128(%rsp), %r14
	movq	136(%rsp), %rdx
	cmpq	%r14, %r13
	jne	.L92
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$5, %r11d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L205:
	movl	%r10d, 32(%rsp)
	subq	$8, %rsp
	movq	%r13, %r8
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	104(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r11d
	popq	%r8
	cmpl	$6, %r11d
	popq	%r9
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movl	32(%rsp), %r10d
	je	.L64
	cmpl	$5, %r11d
	jne	.L57
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L61:
	cmpb	$-96, %dil
	ja	.L63
	jmp	.L62
.L206:
	movq	80(%rsp), %rsi
	addq	$2, %rax
	movl	$6, %r11d
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
	jmp	.L57
.L111:
	movq	%r15, %rbx
	movq	%r14, %rcx
	movl	$4, %r11d
	jmp	.L55
.L84:
	cmpq	%rax, %r13
	jbe	.L86
	movq	%r11, %rcx
	subq	%rdx, %rcx
	cmpq	$1, %rcx
	jle	.L83
	movzwl	(%r14), %ecx
	movzbl	1(%r14), %edi
	rolw	$8, %cx
	cmpw	$-22460, %cx
	movzwl	%cx, %esi
	je	.L87
	leal	-41377(%rsi), %ecx
	cmpl	$22109, %ecx
	ja	.L88
	cmpb	$-96, %dil
	jbe	.L88
	cmpl	$41632, %esi
	ja	.L214
.L89:
	movq	%rax, 128(%rsp)
	leaq	1(%rdx), %rax
	movq	%rax, 136(%rsp)
	movzbl	(%r14), %eax
	movb	%al, (%rdx)
	movq	128(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movq	128(%rsp), %r14
	movq	136(%rsp), %rdx
	jmp	.L82
.L194:
	cmpq	$0, 24(%rsp)
	jne	.L215
	movq	32(%r12), %rax
	xorl	%r11d, %r11d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L7
	movq	16(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r15
	movl	%eax, %r11d
	popq	%rax
	popq	%rdx
	jmp	.L7
.L126:
	movl	%r10d, %r11d
	jmp	.L72
.L210:
	movq	120(%rsp), %rsi
	movq	8(%rsp), %rdi
	cmpq	%rax, %rsi
	movq	%r14, (%rdi)
	jne	.L79
.L93:
	cmpq	%r15, %rsi
	jne	.L73
.L81:
	subl	$1, 20(%r12)
	jmp	.L73
.L214:
	cmpl	$41642, %esi
	jbe	.L88
	cmpl	$42719, %esi
	jbe	.L89
	cmpl	$42741, %esi
	jbe	.L88
	subl	$43195, %esi
	cmpl	$5, %esi
	ja	.L89
.L88:
	cmpq	$0, 80(%rsp)
	je	.L86
	testb	$8, 16(%r12)
	jne	.L216
.L90:
	testl	%ebx, %ebx
	jne	.L217
.L86:
	cmpq	%rdx, 120(%rsp)
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	je	.L78
.L79:
	leaq	__PRETTY_FUNCTION__.9160(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%r10d, 108(%rsp)
	movq	%r11, 32(%rsp)
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	24(%rsp), %rax
	movq	%r13, %r8
	movq	104(%rsp), %r9
	movq	112(%rsp), %rcx
	movq	%r12, %rsi
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r14
	movq	136(%rsp), %rdx
	movq	32(%rsp), %r11
	movl	108(%rsp), %r10d
	je	.L90
	cmpl	$5, %eax
	jne	.L82
.L83:
	movq	120(%rsp), %rsi
	movq	8(%rsp), %rax
	cmpq	%rdx, %rsi
	movq	%r14, (%rax)
	je	.L93
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	2(%rsi), %r14
	leaq	1(%rsi,%rcx), %rcx
	cmpq	%rcx, %r14
	jnb	.L110
	movq	%rbp, %rcx
	subq	%r15, %rcx
	cmpq	$1, %rcx
	jle	.L110
	movb	%dil, (%r15)
	addq	$1, %r15
	movzbl	137(%rsp), %edi
	jmp	.L50
.L211:
	cmpq	%rax, 120(%rsp)
	movq	8(%rsp), %rdi
	movq	%rcx, (%rdi)
	jne	.L79
.L78:
	leaq	__PRETTY_FUNCTION__.9160(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L209:
	cmpq	%rsi, 120(%rsp)
	movq	8(%rsp), %rax
	movq	%r13, (%rax)
	je	.L78
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L212:
	movq	120(%rsp), %rsi
	movq	8(%rsp), %rdi
	cmpq	%rax, %rsi
	movq	%rcx, (%rdi)
	je	.L93
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$1, %rax
	movq	%rax, %rdi
	movq	%rax, 72(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, %rsi
	addq	$1, %rax
	addq	%rdi, %rsi
	cmpq	%rax, %rsi
	movq	%rsi, 88(%rsp)
	jbe	.L41
	movq	%rbp, %rax
	subq	%r15, %rax
	cmpq	$1, %rax
	jle	.L49
	movzwl	120(%rsp), %ecx
	movzbl	121(%rsp), %esi
	rolw	$8, %cx
	cmpw	$-22460, %cx
	movzwl	%cx, %eax
	je	.L218
	leal	-41377(%rax), %ecx
	cmpl	$22109, %ecx
	ja	.L30
	cmpb	$-96, %sil
	jbe	.L30
	leal	-41633(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L30
	cmpl	$42719, %eax
	ja	.L219
.L31:
	movq	32(%rsp), %rsi
	leaq	1(%rsi), %rax
	movq	%rax, 128(%rsp)
	leaq	1(%r15), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, (%r15)
	movq	128(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	136(%rsp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, 136(%rsp)
	movzbl	(%rdx), %edx
	movb	%dl, (%rax)
	movq	128(%rsp), %rax
	cmpq	%rsi, %rax
	jne	.L23
.L193:
	movq	8(%rsp), %rax
	movq	(%rax), %r14
	jmp	.L15
.L208:
	je	.L81
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L87:
	cmpb	$-96, %dil
	ja	.L89
	jmp	.L88
.L217:
	movq	80(%rsp), %rax
	addq	$2, %r14
	movq	%r14, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L82
.L110:
	leaq	1(%rsi), %r14
	jmp	.L51
.L207:
	cmpq	%r15, %r11
	je	.L78
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L219:
	cmpl	$42741, %eax
	jbe	.L30
	subl	$43195, %eax
	cmpl	$5, %eax
	ja	.L31
.L30:
	cmpq	$0, 80(%rsp)
	je	.L32
	testb	$8, %r10b
	jne	.L220
	andl	$2, %r10d
	jne	.L37
.L32:
	movl	$6, %r11d
	jmp	.L7
.L220:
	movl	%r10d, 96(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	pushq	88(%rsp)
	movq	104(%rsp), %r8
	movq	%r14, %rdx
	movq	56(%rsp), %rdi
	movq	%r12, %rsi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	popq	%r10
	cmpl	$6, %eax
	movl	%eax, %r11d
	popq	%r14
	movl	96(%rsp), %r10d
	je	.L221
	movq	128(%rsp), %rax
	cmpq	32(%rsp), %rax
	jne	.L23
	cmpl	$7, %r11d
	jne	.L222
.L41:
	movq	32(%rsp), %rax
	addq	$2, %rax
	cmpq	%rax, 88(%rsp)
	je	.L223
	movl	(%rbx), %eax
	movq	72(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %rdi
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	8(%rsp), %rdi
	addq	%rdx, (%rdi)
	movslq	%eax, %rdx
	cmpq	%rdx, %rsi
	jle	.L224
	cmpq	$4, 72(%rsp)
	ja	.L225
	movq	72(%rsp), %rdi
	orl	%edi, %eax
	testq	%rdi, %rdi
	movl	%eax, (%rbx)
	je	.L45
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L46:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, 72(%rsp)
	jne	.L46
.L45:
	movl	$7, %r11d
	jmp	.L7
.L218:
	cmpb	$-96, %sil
	ja	.L31
	jmp	.L30
.L204:
	leaq	__PRETTY_FUNCTION__.9087(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L213:
	leaq	__PRETTY_FUNCTION__.9010(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L47:
	leaq	__PRETTY_FUNCTION__.9087(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L201:
	leaq	__PRETTY_FUNCTION__.9160(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L215:
	leaq	__PRETTY_FUNCTION__.9160(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L225:
	leaq	__PRETTY_FUNCTION__.9010(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L224:
	leaq	__PRETTY_FUNCTION__.9010(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L223:
	leaq	__PRETTY_FUNCTION__.9010(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L222:
	testl	%r11d, %r11d
	je	.L193
	jmp	.L7
.L221:
	andb	$2, %r10b
	jne	.L37
.L36:
	movq	128(%rsp), %rax
	cmpq	32(%rsp), %rax
	jne	.L23
	jmp	.L32
.L37:
	movq	80(%rsp), %rax
	addq	$2, 128(%rsp)
	addq	$1, (%rax)
	jmp	.L36
.L100:
	leaq	__PRETTY_FUNCTION__.9160(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L18:
	leaq	__PRETTY_FUNCTION__.9010(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9087, @object
	.size	__PRETTY_FUNCTION__.9087, 22
__PRETTY_FUNCTION__.9087:
	.string	"from_gb_to_gbk_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9010, @object
	.size	__PRETTY_FUNCTION__.9010, 22
__PRETTY_FUNCTION__.9010:
	.string	"from_gbk_to_gb_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9160, @object
	.size	__PRETTY_FUNCTION__.9160, 6
__PRETTY_FUNCTION__.9160:
	.string	"gconv"
