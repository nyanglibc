	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	movzbl	%sil, %eax
	testb	%sil, %sil
	movl	$-1, %edx
	cmovs	%edx, %eax
	ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"EUC-TW//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L6
	movabsq	$17179869185, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	32(%rax), %rsi
	movl	$9, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L9
	movabsq	$17179869188, %rdi
	movabsq	$17179869185, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"\"d"
.LC2:
	.string	"\"g"
.LC3:
	.string	"\"f"
.LC4:
	.string	"\"!"
.LC5:
	.string	"!&"
.LC6:
	.string	"\"J"
.LC7:
	.string	"\"G"
.LC8:
	.string	"\"k"
.LC9:
	.string	"\"\""
.LC10:
	.string	"\"j"
.LC11:
	.string	"\017XL"
.LC12:
	.string	"../iconv/skeleton.c"
.LC13:
	.string	"outbufstart == NULL"
.LC14:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC16:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC17:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC18:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC19:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.align 8
.LC20:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC21:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC23:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	leaq	104(%rdi), %rcx
	subq	$168, %rsp
	movl	16(%rsi), %r11d
	movq	%rdi, 40(%rsp)
	leaq	48(%rsi), %rdi
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 48(%rsp)
	testb	$1, %r11b
	movl	224(%rsp), %ebx
	movq	%rcx, 56(%rsp)
	movq	%rdi, 64(%rsp)
	movq	$0, 24(%rsp)
	jne	.L11
	cmpq	$0, 104(%rax)
	movq	144(%rax), %rcx
	movq	%rcx, 24(%rsp)
	je	.L11
	movq	%rcx, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L11:
	testl	%ebx, %ebx
	jne	.L719
	movq	16(%rsp), %rax
	movq	32(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movl	232(%rsp), %ebx
	movq	8(%r12), %r15
	testq	%rsi, %rsi
	movq	(%rax), %r13
	movq	%rsi, %rax
	cmove	%r12, %rax
	cmpq	$0, 48(%rsp)
	movq	(%rax), %r14
	movl	$0, %eax
	movq	$0, 128(%rsp)
	cmovne	%rdx, %rax
	testl	%ebx, %ebx
	movq	%rax, 72(%rsp)
	jne	.L720
.L151:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L721
.L157:
	leaq	.LC3(%rip), %r10
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rbx
	movq	%r13, %rax
	movl	$4, 8(%rsp)
.L184:
	cmpq	%rax, %rbp
	je	.L185
.L267:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L492
	cmpq	%rbx, %r15
	jbe	.L261
	movl	(%rax), %edx
	cmpl	$127, %edx
	ja	.L186
	leaq	1(%rbx), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%rbx)
.L187:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 144(%rsp)
	jne	.L267
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movq	%rax, (%rdi)
	jne	.L722
.L268:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L723
	cmpq	%rbx, %r14
	jnb	.L497
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movq	%rax, 136(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %edi
	leaq	136(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rdi
	pushq	$0
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%rsi
	popq	%rdi
	je	.L272
	movq	136(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L724
.L271:
	testl	%r11d, %r11d
	jne	.L517
.L391:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%r12), %r14
	movq	(%rax), %r13
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	jne	.L157
.L721:
	cmpq	%r13, %rbp
	je	.L478
	leaq	4(%r14), %rdx
	movq	%r13, %rax
	movq	%r14, %rbx
	cmpq	%rdx, %r15
	jb	.L261
	leaq	.L169(%rip), %r10
	movl	$4, 8(%rsp)
	andl	$2, %r11d
.L159:
	movzbl	(%rax), %ecx
	cmpl	$127, %ecx
	movl	%ecx, %edi
	movl	%ecx, %esi
	ja	.L160
	addq	$1, %rax
.L161:
	movl	%ecx, (%rbx)
	movq	%rdx, %rbx
.L163:
	cmpq	%rax, %rbp
	je	.L185
.L182:
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r15
	jnb	.L159
	.p2align 4,,10
	.p2align 3
.L261:
	cmpq	$0, 32(%rsp)
	movq	16(%rsp), %rdi
	movl	$5, 8(%rsp)
	movq	%rax, (%rdi)
	je	.L268
.L722:
	movq	32(%rsp), %rax
	movq	%rbx, (%rax)
.L10:
	movl	8(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r15, %r8
	subq	%rbx, %r8
	cmpl	$9249, %edx
	ja	.L189
	cmpl	$9216, %edx
	jnb	.L190
	cmpl	$8457, %edx
	je	.L191
	ja	.L192
	cmpl	$969, %edx
	ja	.L193
	cmpl	$913, %edx
	jnb	.L194
	cmpl	$167, %edx
	jb	.L229
	cmpl	$247, %edx
	ja	.L725
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	.p2align 4,,10
	.p2align 3
.L226:
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	jne	.L401
.L229:
	cmpl	$9794, %edx
	ja	.L428
	cmpl	$9472, %edx
	jnb	.L244
	cmpl	$8553, %edx
	ja	.L429
	cmpl	$8544, %edx
	jnb	.L235
	cmpl	$969, %edx
	jbe	.L726
	cmpl	$8451, %edx
	je	.L431
	jbe	.L727
	cmpl	$8453, %edx
	movl	$2, %edi
	jne	.L728
	leaq	.LC9(%rip), %rsi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L189:
	cmpl	$13269, %edx
	ja	.L209
	cmpl	$13198, %edx
	jnb	.L210
	cmpl	$12329, %edx
	ja	.L211
	cmpl	$12288, %edx
	jnb	.L212
	cmpl	$9312, %edx
	jb	.L229
	cmpl	$9341, %edx
	ja	.L729
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L160:
	leal	-161(%rcx), %ecx
	cmpl	$93, %ecx
	jbe	.L162
	cmpl	$142, %edi
	je	.L162
	cmpq	$0, 72(%rsp)
	je	.L496
	testl	%r11d, %r11d
	jne	.L730
.L496:
	movl	$6, 8(%rsp)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$7, 8(%rsp)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L192:
	cmpl	$8601, %edx
	ja	.L201
	cmpl	$8592, %edx
	jnb	.L202
	cmpl	$8544, %edx
	jb	.L229
	cmpl	$8553, %edx
	ja	.L731
	subl	$53, %edx
	leaq	136(%rsp), %rsi
	movb	$36, 136(%rsp)
	movb	%dl, 137(%rsp)
	movl	$36, %edi
.L754:
	cmpq	$1, %r8
	ja	.L732
.L228:
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	movl	$5, 8(%rsp)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L272:
	movl	8(%rsp), %r11d
	cmpl	$5, %r11d
	jne	.L271
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	$65373, %edx
	ja	.L219
	cmpl	$65281, %edx
	jnb	.L220
	cmpl	$19968, %edx
	jb	.L229
	cmpl	$40860, %edx
	ja	.L733
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	1(%rax), %r8
	cmpq	%r8, %rbp
	jbe	.L492
	movzbl	1(%rax), %ecx
	leal	95(%rcx), %r9d
	cmpb	$93, %r9b
	jbe	.L164
.L167:
	cmpq	$0, 72(%rsp)
	je	.L496
	testl	%r11d, %r11d
	je	.L496
	movq	72(%rsp), %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rax)
	movq	%r8, %rax
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L211:
	cmpl	$12585, %edx
	ja	.L215
	cmpl	$12549, %edx
	jnb	.L216
	cmpl	$12539, %edx
	leaq	.LC5(%rip), %rsi
	jne	.L229
	.p2align 4,,10
	.p2align 3
.L197:
	movzbl	(%rsi), %edi
.L401:
	cmpq	$1, %r8
	jbe	.L228
.L732:
	movb	%dil, (%rbx)
	movzbl	1(%rsi), %eax
	movb	%al, 1(%rbx)
.L709:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L193:
	cmpl	$8451, %edx
	je	.L494
	ja	.L198
	leal	-8211(%rdx), %esi
	cmpl	$43, %esi
	ja	.L229
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L201:
	cmpl	$8869, %edx
	je	.L205
	ja	.L206
	leal	-8725(%rdx), %esi
	cmpl	$82, %esi
	ja	.L229
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L219:
	cmpl	$65505, %edx
	je	.L223
	cmpl	$65509, %edx
	je	.L224
	cmpl	$65504, %edx
	jne	.L229
	movq	%r10, %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L720:
	movq	32(%r12), %rbx
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$7, %eax
	je	.L151
	testq	%rsi, %rsi
	jne	.L734
	movq	40(%rsp), %rdi
	cmpq	$0, 96(%rdi)
	je	.L735
	cmpl	$4, %eax
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	ja	.L52
	leaq	136(%rsp), %rdi
	cltq
	xorl	%edx, %edx
	movq	%rax, %r10
	movq	%rdi, 80(%rsp)
.L53:
	movzbl	4(%rbx,%rdx), %esi
	movb	%sil, (%rdi,%rdx)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L53
	movq	%r13, %rsi
	subq	%rdx, %rsi
	addq	$4, %rsi
	cmpq	%rsi, %rbp
	jb	.L736
	cmpq	%r15, %r14
	jnb	.L105
	leaq	1(%r13), %rdx
	leaq	135(%rsp), %r8
.L61:
	movq	%rdx, 144(%rsp)
	movzbl	-1(%rdx), %esi
	addq	$1, %r10
	movq	%rdx, %rdi
	addq	$1, %rdx
	cmpq	$3, %r10
	movb	%sil, (%r8,%r10)
	ja	.L524
	cmpq	%rdi, %rbp
	ja	.L61
.L524:
	movl	136(%rsp), %edx
	movq	80(%rsp), %rdi
	cmpl	$127, %edx
	movq	%rdi, 144(%rsp)
	jbe	.L737
	movq	%r15, %rdi
	subq	%r14, %rdi
	cmpl	$9249, %edx
	ja	.L66
	cmpl	$9216, %edx
	jnb	.L67
	cmpl	$8457, %edx
	je	.L68
	ja	.L69
	cmpl	$969, %edx
	jbe	.L738
	cmpl	$8451, %edx
	je	.L477
	ja	.L75
	leal	-8211(%rdx), %esi
	cmpl	$43, %esi
	ja	.L106
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %r8
	leaq	(%r8,%rsi,2), %rsi
.L103:
	movzbl	(%rsi), %r8d
	testb	%r8b, %r8b
	jne	.L396
.L106:
	cmpl	$9794, %edx
	ja	.L411
	cmpl	$9472, %edx
	jnb	.L121
	cmpl	$8553, %edx
	ja	.L412
	cmpl	$8544, %edx
	jnb	.L112
	cmpl	$969, %edx
	jbe	.L739
	cmpl	$8451, %edx
	je	.L414
	jbe	.L740
	cmpl	$8453, %edx
	movl	$2, %r9d
	jne	.L741
	leaq	.LC9(%rip), %rsi
.L118:
	movzbl	(%rsi), %r8d
.L399:
	subq	$1, %rdi
	cmpq	%r9, %rdi
	jb	.L105
	movb	%r8b, 1(%r14)
	movzbl	1(%rsi), %eax
	cmpq	$3, %r9
	movb	%al, 2(%r14)
	je	.L742
.L139:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$-114, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	subb	$96, (%rax)
.L706:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%rbp, %r9
	subq	%rax, %r9
	cmpl	$142, %esi
	je	.L743
	testb	%dil, %dil
	js	.L744
.L180:
	cmpq	$0, 72(%rsp)
	je	.L496
	testl	%r11d, %r11d
	je	.L496
	movq	72(%rsp), %rdi
	addq	$2, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rdi)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L730:
	movq	72(%rsp), %rcx
	addq	$1, %rax
	movl	$6, 8(%rsp)
	addq	$1, (%rcx)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L428:
	cmpl	$40869, %edx
	jbe	.L745
	cmpl	$65504, %edx
	je	.L255
	jbe	.L746
	cmpl	$173782, %edx
	ja	.L441
	cmpl	$131072, %edx
	jnb	.L258
	cmpl	$65505, %edx
	movl	$2, %edi
	jne	.L747
	leaq	.LC2(%rip), %rsi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L429:
	cmpl	$8807, %edx
	jbe	.L748
	cmpl	$9249, %edx
	ja	.L435
	cmpl	$9216, %edx
	jnb	.L242
	cmpl	$8869, %edx
	movl	$2, %edi
	jne	.L749
	leaq	.LC7(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L241:
	movzbl	(%rsi), %r9d
.L404:
	subq	$1, %r8
	cmpq	%rdi, %r8
	jb	.L261
	movb	%r9b, 1(%rbx)
	movzbl	1(%rsi), %eax
	cmpq	$3, %rdi
	movb	%al, 2(%rbx)
	je	.L750
.L262:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$-114, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	subb	$96, (%rax)
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L745:
	cmpl	$40861, %edx
	jnb	.L251
	cmpl	$12963, %edx
	je	.L248
	ja	.L437
	cmpl	$12539, %edx
	je	.L246
	ja	.L438
	leal	-12288(%rdx), %esi
	cmpl	$41, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	.p2align 4,,10
	.p2align 3
.L231:
	movzbl	(%rsi), %r9d
	testb	%r9b, %r9b
	jne	.L404
.L427:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L751
	cmpq	$0, 72(%rsp)
	je	.L496
	testb	$8, 16(%r12)
	jne	.L752
.L265:
	testb	$2, %r11b
	je	.L496
	movq	72(%rsp), %rcx
	addq	$4, %rax
	movl	$6, 8(%rsp)
	movq	%rax, 144(%rsp)
	addq	$1, (%rcx)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L723:
	movq	48(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	128(%rsp), %rax
	addq	%rax, (%rdi)
.L270:
	movl	232(%rsp), %eax
	testl	%eax, %eax
	je	.L10
	cmpl	$7, 8(%rsp)
	jne	.L10
	movq	16(%rsp), %rax
	movq	%rbp, %rdx
	movq	(%rax), %rdi
	subq	%rdi, %rdx
	cmpq	$4, %rdx
	ja	.L393
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movq	32(%r12), %rsi
	je	.L395
.L394:
	movzbl	(%rdi,%rax), %ecx
	movb	%cl, 4(%rsi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L394
.L395:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movl	(%rsi), %eax
	andl	$-8, %eax
	orl	%eax, %edx
	movl	%edx, (%rsi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L497:
	movl	8(%rsp), %r11d
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L726:
	cmpl	$913, %edx
	jnb	.L233
	cmpl	$167, %edx
	jb	.L427
	cmpl	$247, %edx
	ja	.L753
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L729:
	leal	-9472(%rdx), %esi
	cmpl	$322, %esi
	ja	.L229
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L731:
	leal	-8560(%rdx), %esi
	cmpl	$9, %esi
	ja	.L229
	subl	$59, %edx
	movb	$38, 136(%rsp)
	movl	$38, %edi
	movb	%dl, 137(%rsp)
	leaq	136(%rsp), %rsi
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L725:
	leal	-711(%rdx), %esi
	cmpl	$18, %esi
	ja	.L229
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L748:
	cmpl	$8725, %edx
	jnb	.L239
	cmpl	$8560, %edx
	jb	.L427
	cmpl	$8569, %edx
	ja	.L755
	subl	$59, %edx
	leaq	136(%rsp), %rsi
	movb	$38, 136(%rsp)
	movb	%dl, 137(%rsp)
	movl	$38, %edi
.L236:
	subq	$1, %r8
	cmpq	$1, %r8
	jbe	.L261
	movb	%dil, 1(%rbx)
	movzbl	1(%rsi), %eax
	movb	%al, 2(%rbx)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L733:
	leal	-65072(%rdx), %esi
	cmpl	$59, %esi
	ja	.L229
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L746:
	cmpl	$65131, %edx
	ja	.L440
	cmpl	$65072, %edx
	jnb	.L253
	cmpl	$64040, %edx
	movl	$3, %edi
	leaq	.LC11(%rip), %rsi
	je	.L241
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$12963, %edx
	leaq	.LC4(%rip), %rsi
	je	.L197
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L198:
	cmpl	$8453, %edx
	leaq	.LC9(%rip), %rsi
	je	.L197
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L206:
	cmpl	$8895, %edx
	leaq	.LC6(%rip), %rsi
	je	.L197
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L494:
	leaq	.LC10(%rip), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	.LC7(%rip), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC2(%rip), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	.LC1(%rip), %rsi
	jmp	.L197
.L212:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
.L202:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
.L194:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
.L220:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
.L727:
	leal	-8211(%rdx), %esi
	cmpl	$43, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L744:
	subl	$161, %esi
	cmpl	$92, %esi
	ja	.L180
	cmpq	$1, %r9
	jbe	.L489
	imull	$94, %esi, %esi
	leal	-161(%rsi,%rcx), %ecx
	cmpl	$8690, %ecx
	jg	.L180
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	testw	%cx, %cx
	je	.L180
	cmpl	$65533, %ecx
	je	.L180
.L181:
	addq	$2, %rax
	jmp	.L161
.L440:
	leal	-65281(%rdx), %esi
	cmpl	$92, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L735:
	cmpl	$4, %eax
	ja	.L21
	leaq	152(%rsp), %rsi
	cltq
	xorl	%edx, %edx
.L22:
	movzbl	4(%rbx,%rdx), %edi
	movb	%dil, (%rsi,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L22
	leaq	4(%r14), %r8
	cmpq	%r8, %r15
	jb	.L105
	leaq	151(%rsp), %r9
	movq	%r13, %rdi
.L23:
	addq	$1, %rdi
	movzbl	-1(%rdi), %r10d
	addq	$1, %rdx
	cmpq	$3, %rdx
	movb	%r10b, (%r9,%rdx)
	ja	.L523
	cmpq	%rdi, %rbp
	ja	.L23
.L523:
	movzbl	152(%rsp), %edi
	cmpl	$127, %edi
	movl	%edi, %r10d
	movl	%edi, 8(%rsp)
	jbe	.L464
	leal	-161(%rdi), %r9d
	cmpl	$93, %r9d
	jbe	.L27
	cmpl	$142, %edi
	je	.L27
.L33:
	cmpq	$0, 72(%rsp)
	movl	$6, 8(%rsp)
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	72(%rsp), %rdi
	leaq	1(%rsi), %rdx
	addq	$1, (%rdi)
.L29:
	subq	%rsi, %rdx
	cmpq	%rax, %rdx
	jle	.L756
	subq	%rax, %rdx
	movq	16(%rsp), %rax
	andl	$-8, %ecx
	addq	%rdx, %r13
	movl	16(%r12), %r11d
	movq	%r13, (%rax)
	movl	%ecx, (%rbx)
	jmp	.L151
.L105:
	movl	$5, 8(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L724:
	movq	16(%rsp), %rax
	movl	16(%r12), %ebx
	movq	%r13, (%rax)
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	je	.L757
	cmpq	%r13, %rbp
	movq	%r13, 144(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r14, %rdx
	movl	$4, %r9d
	movl	%ebx, 8(%rsp)
	je	.L758
.L389:
	leaq	4(%r13), %rsi
	cmpq	%rsi, %rbp
	jb	.L511
	cmpq	%rdx, %r10
	jbe	.L512
	movl	0(%r13), %ecx
	cmpl	$127, %ecx
	ja	.L307
	leaq	1(%rdx), %rax
	movq	%rax, 152(%rsp)
	movb	%cl, (%rdx)
.L308:
	movq	144(%rsp), %rax
	leaq	4(%rax), %r13
	movq	%r13, 144(%rsp)
.L386:
	cmpq	%r13, %rbp
	movq	152(%rsp), %rdx
	jne	.L389
.L758:
	movslq	%r9d, %rax
	movq	%rbp, %r13
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r10, %r8
	subq	%rdx, %r8
	cmpl	$9249, %ecx
	ja	.L310
	cmpl	$9216, %ecx
	jnb	.L311
	cmpl	$8457, %ecx
	je	.L312
	ja	.L313
	cmpl	$969, %ecx
	ja	.L314
	cmpl	$913, %ecx
	jnb	.L315
	cmpl	$167, %ecx
	jb	.L351
	cmpl	$247, %ecx
	ja	.L759
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L437:
	cmpl	$19967, %edx
	ja	.L250
	cmpl	$13312, %edx
	jnb	.L251
	leal	-13198(%rdx), %esi
	cmpl	$71, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L210:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
.L190:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	.LC8(%rip), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L216:
	addl	$66, %edx
	movb	$37, 136(%rsp)
	movl	$37, %edi
	movb	%dl, 137(%rsp)
	leaq	136(%rsp), %rsi
	jmp	.L754
.L743:
	leal	-161(%rcx), %esi
	cmpl	$15, %esi
	ja	.L167
	subq	$1, %r9
	cmpq	$2, %r9
	jbe	.L492
	movzbl	2(%rax), %esi
	subl	$161, %esi
	cmpl	$93, %esi
	ja	.L167
	movzbl	3(%rax), %edi
	subl	$161, %edi
	cmpl	$93, %edi
	ja	.L167
	imull	$94, %esi, %esi
	subl	$160, %ecx
	addl	%edi, %esi
	cmpl	$15, %ecx
	ja	.L167
	movslq	(%r10,%rcx,4), %rcx
	addq	%r10, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L169:
	.long	.L167-.L169
	.long	.L168-.L169
	.long	.L170-.L169
	.long	.L171-.L169
	.long	.L172-.L169
	.long	.L173-.L169
	.long	.L174-.L169
	.long	.L175-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L176-.L169
	.text
	.p2align 4,,10
	.p2align 3
.L310:
	cmpl	$13269, %ecx
	ja	.L330
	cmpl	$13198, %ecx
	jnb	.L331
	cmpl	$12329, %ecx
	ja	.L332
	cmpl	$12288, %ecx
	jnb	.L333
	cmpl	$9312, %ecx
	jb	.L351
	cmpl	$9341, %ecx
	ja	.L760
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leal	-9312(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
.L347:
	movzbl	(%rax), %edi
	testb	%dil, %dil
	jne	.L405
.L351:
	cmpl	$9794, %ecx
	ja	.L445
	cmpl	$9472, %ecx
	jnb	.L366
	cmpl	$8553, %ecx
	ja	.L446
	cmpl	$8544, %ecx
	jnb	.L357
	cmpl	$969, %ecx
	jbe	.L761
	cmpl	$8451, %ecx
	je	.L448
	jbe	.L762
	cmpl	$8453, %ecx
	movl	$2, %eax
	jne	.L763
	leaq	.LC9(%rip), %rdi
.L363:
	movzbl	(%rdi), %ecx
	movl	%ecx, %ebx
.L408:
	subq	$1, %r8
	cmpq	%rax, %r8
	jb	.L383
	movb	%bl, 1(%rdx)
	movzbl	1(%rdi), %ecx
	cmpq	$3, %rax
	movb	%cl, 2(%rdx)
	jne	.L384
	movzbl	2(%rdi), %eax
	movb	%al, 3(%rdx)
.L384:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movb	$-114, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	subb	$96, (%rax)
	.p2align 4,,10
	.p2align 3
.L710:
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	movq	152(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 152(%rsp)
	addb	$-128, (%rax)
	jmp	.L308
.L719:
	cmpq	$0, 32(%rsp)
	jne	.L764
	movq	32(%r12), %rax
	movl	$0, 8(%rsp)
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L10
	movq	24(%rsp), %r14
	movq	%r14, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	232(%rsp), %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	64(%rsp), %r9
	movq	80(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	*%r14
	movl	%eax, 24(%rsp)
	popq	%rbp
	popq	%r12
	jmp	.L10
.L753:
	leal	-711(%rdx), %esi
	cmpl	$18, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L755:
	leal	-8592(%rdx), %esi
	cmpl	$9, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L517:
	movl	%r11d, 8(%rsp)
	jmp	.L270
.L749:
	cmpl	$8895, %edx
	leaq	.LC6(%rip), %rsi
	je	.L241
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L747:
	cmpl	$65509, %edx
	leaq	.LC1(%rip), %rsi
	je	.L241
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L728:
	cmpl	$8457, %edx
	leaq	.LC8(%rip), %rsi
	je	.L241
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L435:
	leal	-9312(%rdx), %esi
	cmpl	$29, %esi
	ja	.L427
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L438:
	leal	-12549(%rdx), %esi
	cmpl	$36, %esi
	ja	.L427
	addl	$66, %edx
	movb	$37, 136(%rsp)
	movl	$37, %edi
	movb	%dl, 137(%rsp)
	leaq	136(%rsp), %rsi
	jmp	.L236
.L441:
	leal	-194560(%rdx), %esi
	cmpl	$541, %esi
	ja	.L427
	leaq	(%rsi,%rsi,2), %rsi
	movl	$3, %edi
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %rsi
	jmp	.L231
.L431:
	movl	$2, %edi
	leaq	.LC10(%rip), %rsi
	jmp	.L241
.L757:
	cmpq	%r13, %rbp
	je	.L765
	leaq	4(%r14), %rax
	andl	$2, %ebx
	movq	%r14, %rdx
	movl	$4, %r8d
	cmpq	%rax, %r10
	jb	.L766
.L278:
	movzbl	0(%r13), %edi
	cmpl	$127, %edi
	movl	%edi, %esi
	movl	%edi, %ecx
	ja	.L281
	addq	$1, %r13
.L282:
	movl	%edi, (%rdx)
	movq	%rax, %rdx
.L284:
	cmpq	%r13, %rbp
	je	.L767
.L303:
	leaq	4(%rdx), %rax
	cmpq	%rax, %r10
	jnb	.L278
	movl	$5, %eax
	jmp	.L280
.L281:
	leal	-161(%rdi), %edi
	cmpl	$93, %edi
	jbe	.L283
	cmpl	$142, %esi
	je	.L283
	cmpq	$0, 72(%rsp)
	je	.L510
	testl	%ebx, %ebx
	jne	.L768
.L510:
	movl	$6, %eax
.L280:
	movq	16(%rsp), %rcx
	movq	%r13, (%rcx)
	jmp	.L304
.L511:
	movl	$7, %eax
.L306:
	movq	16(%rsp), %rdi
	movq	136(%rsp), %r10
	movq	%r13, (%rdi)
.L304:
	cmpq	%r10, %rdx
	jne	.L277
	cmpq	$5, %rax
	jne	.L276
.L350:
	cmpq	%rdx, %r14
	jne	.L271
.L279:
	subl	$1, 20(%r12)
	jmp	.L271
.L313:
	cmpl	$8601, %ecx
	ja	.L322
	cmpl	$8592, %ecx
	jnb	.L323
	cmpl	$8544, %ecx
	jb	.L351
	cmpl	$8553, %ecx
	ja	.L769
	subl	$53, %ecx
	movb	$36, 126(%rsp)
	movl	$36, %edi
	movb	%cl, 127(%rsp)
.L348:
	leaq	126(%rsp), %rax
	jmp	.L405
.L330:
	cmpl	$65373, %ecx
	ja	.L340
	cmpl	$65281, %ecx
	jnb	.L341
	cmpl	$19968, %ecx
	jb	.L351
	cmpl	$40860, %ecx
	ja	.L770
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L246:
	movl	$2, %edi
	leaq	.LC5(%rip), %rsi
	jmp	.L241
.L242:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L258:
	leal	-131072(%rdx), %esi
	movl	$3, %edi
	leaq	(%rsi,%rsi,2), %rsi
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %rsi
	jmp	.L231
.L737:
	leaq	1(%r14), %rax
	movq	%rax, 152(%rsp)
	movb	%dl, (%r14)
.L64:
	movq	144(%rsp), %rax
	movq	80(%rsp), %rcx
	addq	$4, %rax
	cmpq	%rcx, %rax
	movq	%rax, 144(%rsp)
	je	.L708
.L707:
	subq	%rcx, %rax
	movl	(%rbx), %ecx
	movq	%rax, %r13
	movl	%ecx, %eax
	andl	$7, %eax
.L141:
	cmpq	%r13, %rax
	jge	.L771
	subq	%rax, %r13
	movq	16(%rsp), %rax
	andl	$-8, %ecx
	movq	152(%rsp), %r14
	movl	16(%r12), %r11d
	addq	(%rax), %r13
	movq	%r13, (%rax)
	movl	%ecx, (%rbx)
	jmp	.L151
.L750:
	movzbl	2(%rsi), %eax
	movb	%al, 3(%rbx)
	jmp	.L262
.L233:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L239:
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-8725(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L332:
	cmpl	$12585, %ecx
	ja	.L336
	cmpl	$12549, %ecx
	jnb	.L337
	cmpl	$12539, %ecx
	leaq	.LC5(%rip), %rax
	jne	.L351
.L318:
	movzbl	(%rax), %edi
.L405:
	cmpq	$1, %r8
	jbe	.L349
	movb	%dil, (%rdx)
	movzbl	1(%rax), %eax
	movb	%al, 1(%rdx)
	jmp	.L710
.L314:
	cmpl	$8451, %ecx
	je	.L513
	ja	.L319
	leal	-8211(%rcx), %eax
	cmpl	$43, %eax
	ja	.L351
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L322:
	cmpl	$8869, %ecx
	je	.L326
	ja	.L327
	leal	-8725(%rcx), %eax
	cmpl	$82, %eax
	ja	.L351
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L340:
	cmpl	$65505, %ecx
	je	.L344
	cmpl	$65509, %ecx
	je	.L345
	cmpl	$65504, %ecx
	jne	.L351
	leaq	.LC3(%rip), %rax
	jmp	.L318
.L283:
	leaq	1(%r13), %rdi
	cmpq	%rdi, %rbp
	movq	%rdi, 8(%rsp)
	jbe	.L505
	movzbl	1(%r13), %edi
	leal	95(%rdi), %r9d
	cmpb	$93, %r9b
	ja	.L288
	movq	%rbp, %r9
	subq	%r13, %r9
	cmpl	$142, %ecx
	je	.L772
	testb	%sil, %sil
	js	.L773
.L301:
	cmpq	$0, 72(%rsp)
	je	.L510
	testl	%ebx, %ebx
	je	.L510
	movq	72(%rsp), %rax
	addq	$2, %r13
	movl	$6, %r8d
	addq	$1, (%rax)
	jmp	.L284
.L751:
	movq	%rcx, 144(%rsp)
	movq	%rcx, %rax
	jmp	.L184
.L445:
	cmpl	$40869, %ecx
	jbe	.L774
	cmpl	$65504, %ecx
	je	.L377
	jbe	.L775
	cmpl	$173782, %ecx
	ja	.L458
	cmpl	$131072, %ecx
	jnb	.L380
	cmpl	$65505, %ecx
	movl	$2, %eax
	jne	.L776
	leaq	.LC2(%rip), %rdi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L512:
	movl	$5, %eax
	jmp	.L306
.L349:
	movq	136(%rsp), %rdx
	cmpq	%rdx, 152(%rsp)
	movq	144(%rsp), %rax
	movq	16(%rsp), %rcx
	movq	%rax, (%rcx)
	je	.L350
.L277:
	leaq	__PRETTY_FUNCTION__.9323(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rbp, %rax
	movq	%r14, %rbx
	movl	$4, 8(%rsp)
	jmp	.L185
.L446:
	cmpl	$8807, %ecx
	jbe	.L777
	cmpl	$9249, %ecx
	ja	.L452
	cmpl	$9216, %ecx
	jnb	.L364
	cmpl	$8869, %ecx
	movl	$2, %eax
	jne	.L778
	leaq	.LC7(%rip), %rdi
	jmp	.L363
.L774:
	cmpl	$40861, %ecx
	jnb	.L373
	cmpl	$12963, %ecx
	je	.L370
	ja	.L454
	cmpl	$12539, %ecx
	je	.L368
	ja	.L455
	leal	-12288(%rcx), %eax
	cmpl	$41, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
.L353:
	movzbl	(%rdi), %ebx
	testb	%bl, %bl
	jne	.L408
.L444:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	je	.L779
	cmpq	$0, 72(%rsp)
	je	.L515
	testb	$8, 16(%r12)
	jne	.L780
.L387:
	testb	$2, 8(%rsp)
	jne	.L781
.L515:
	movl	$6, %eax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L288:
	cmpq	$0, 72(%rsp)
	je	.L510
	testl	%ebx, %ebx
	je	.L510
	movq	72(%rsp), %rax
	movq	8(%rsp), %r13
	movl	$6, %r8d
	addq	$1, (%rax)
	jmp	.L303
.L250:
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	jne	.L236
.L251:
	leal	-13312(%rdx), %esi
	movl	$3, %edi
	leaq	(%rsi,%rsi,2), %rsi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rsi
	jmp	.L231
.L66:
	cmpl	$13269, %edx
	ja	.L86
	cmpl	$13198, %edx
	jnb	.L87
	cmpl	$12329, %edx
	jbe	.L782
	cmpl	$12585, %edx
	ja	.L92
	cmpl	$12549, %edx
	jnb	.L93
	cmpl	$12539, %edx
	leaq	.LC5(%rip), %rsi
	jne	.L106
.L74:
	movzbl	(%rsi), %r8d
.L396:
	cmpq	$1, %rdi
	jbe	.L105
	movb	%r8b, (%r14)
	movzbl	1(%rsi), %eax
	movb	%al, 1(%r14)
	jmp	.L706
.L464:
	leaq	1(%rsi), %rdx
.L26:
	movl	%edi, (%r14)
	movl	(%rbx), %ecx
	movq	%r8, %r14
	movl	%ecx, %eax
	andl	$7, %eax
	jmp	.L29
.L736:
	movq	16(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r13, %rax
	addq	%rdx, %rax
	cmpq	$4, %rax
	ja	.L55
	addq	$1, %r13
	cmpq	%rdx, %rax
	jbe	.L57
.L58:
	movq	%r13, 144(%rsp)
	movzbl	-1(%r13), %edx
	addq	$1, %r13
	movb	%dl, 4(%rbx,%r10)
	addq	$1, %r10
	cmpq	%r10, %rax
	jne	.L58
.L57:
	movl	$7, 8(%rsp)
	jmp	.L10
.L244:
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leal	-9472(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L248:
	movl	$2, %edi
	leaq	.LC4(%rip), %rsi
	jmp	.L241
.L235:
	subl	$53, %edx
	movb	$36, 136(%rsp)
	movl	$36, %edi
	movb	%dl, 137(%rsp)
	leaq	136(%rsp), %rsi
	jmp	.L236
.L255:
	movl	$2, %edi
	movq	%r10, %rsi
	jmp	.L241
.L253:
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leal	-65072(%rdx), %esi
	leaq	(%rdi,%rsi,2), %rsi
	movl	$2, %edi
	jmp	.L231
.L777:
	cmpl	$8725, %ecx
	jnb	.L361
	cmpl	$8560, %ecx
	jb	.L444
	cmpl	$8569, %ecx
	ja	.L783
	subl	$59, %ecx
	leaq	126(%rsp), %rax
	movb	$38, 126(%rsp)
	movb	%cl, 127(%rsp)
	movl	$38, %edi
.L358:
	subq	$1, %r8
	cmpq	$1, %r8
	jbe	.L383
	movb	%dil, 1(%rdx)
	movzbl	1(%rax), %eax
	movb	%al, 2(%rdx)
	jmp	.L384
.L759:
	leal	-711(%rcx), %eax
	cmpl	$18, %eax
	ja	.L351
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L768:
	movq	72(%rsp), %rax
	addq	$1, %r13
	movl	$6, %r8d
	addq	$1, (%rax)
	jmp	.L284
.L770:
	leal	-65072(%rcx), %eax
	cmpl	$59, %eax
	ja	.L351
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L760:
	leal	-9472(%rcx), %eax
	cmpl	$322, %eax
	ja	.L351
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L775:
	cmpl	$65131, %ecx
	ja	.L457
	cmpl	$65072, %ecx
	jnb	.L375
	cmpl	$64040, %ecx
	movl	$3, %eax
	leaq	.LC11(%rip), %rdi
	je	.L363
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L761:
	cmpl	$913, %ecx
	jnb	.L355
	cmpl	$167, %ecx
	jb	.L444
	cmpl	$247, %ecx
	ja	.L784
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rdi
	leal	-167(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L769:
	leal	-8560(%rcx), %eax
	cmpl	$9, %eax
	ja	.L351
	subl	$59, %ecx
	movb	$38, 126(%rsp)
	movl	$38, %edi
	movb	%cl, 127(%rsp)
	jmp	.L348
.L319:
	cmpl	$8453, %ecx
	leaq	.LC9(%rip), %rax
	je	.L318
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L327:
	cmpl	$8895, %ecx
	leaq	.LC6(%rip), %rax
	je	.L318
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L454:
	cmpl	$19967, %ecx
	ja	.L372
	cmpl	$13312, %ecx
	jnb	.L373
	leal	-13198(%rcx), %eax
	cmpl	$71, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L752:
	movl	%r11d, 88(%rsp)
	movq	%r10, 80(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r8
	popq	%r9
	movq	80(%rsp), %r10
	movl	88(%rsp), %r11d
	movq	144(%rsp), %rax
	movq	152(%rsp), %rbx
	je	.L265
	cmpl	$5, 8(%rsp)
	jne	.L184
	jmp	.L185
.L345:
	leaq	.LC1(%rip), %rax
	jmp	.L318
.L344:
	leaq	.LC2(%rip), %rax
	jmp	.L318
.L323:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leal	-8592(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L513:
	leaq	.LC10(%rip), %rax
	jmp	.L318
.L337:
	addl	$66, %ecx
	movb	$37, 126(%rsp)
	movl	$37, %edi
	movb	%cl, 127(%rsp)
	jmp	.L348
.L326:
	leaq	.LC7(%rip), %rax
	jmp	.L318
.L336:
	cmpl	$12963, %ecx
	leaq	.LC4(%rip), %rax
	je	.L318
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L331:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rdi
	leal	-13198(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L333:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rdi
	leal	-12288(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L341:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leal	-65281(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L312:
	leaq	.LC8(%rip), %rax
	jmp	.L318
.L311:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L315:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	jmp	.L347
.L411:
	cmpl	$40869, %edx
	jbe	.L785
	cmpl	$65504, %edx
	je	.L132
	jbe	.L786
	cmpl	$173782, %edx
	ja	.L424
	cmpl	$131072, %edx
	jnb	.L135
	cmpl	$65505, %edx
	movl	$2, %r9d
	jne	.L787
	leaq	.LC2(%rip), %rsi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$65373, %edx
	jbe	.L788
	cmpl	$65505, %edx
	je	.L100
	cmpl	$65509, %edx
	je	.L101
	cmpl	$65504, %edx
	jne	.L106
	leaq	.LC3(%rip), %rsi
	jmp	.L74
.L69:
	cmpl	$8601, %edx
	jbe	.L789
	cmpl	$8869, %edx
	je	.L82
	ja	.L83
	leal	-8725(%rdx), %esi
	cmpl	$82, %esi
	ja	.L106
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %r8
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L27:
	leaq	(%rsi,%rdx), %rdi
	movq	%rdi, %r9
	movq	%rdi, 80(%rsp)
	leaq	1(%rsi), %rdi
	cmpq	%rdi, %r9
	movq	%rdi, 88(%rsp)
	ja	.L790
.L30:
	leaq	4(%rsi), %rdi
	cmpq	%rdi, 80(%rsp)
	je	.L791
	movq	%rdx, %rdi
	andl	$-8, %ecx
	subq	%rax, %rdi
	movq	16(%rsp), %rax
	addq	%rdi, %r13
	movq	%r13, (%rax)
	movslq	%ecx, %rax
	cmpq	%rax, %rdx
	jle	.L792
	cmpq	$4, %rdx
	ja	.L793
	orl	%edx, %ecx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movl	%ecx, (%rbx)
	je	.L57
	movb	%r10b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L57
.L794:
	movzbl	(%rsi,%rax), %r10d
	movb	%r10b, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L794
	jmp	.L57
.L806:
	movq	80(%rsp), %rax
	movl	%r11d, 108(%rsp)
	leaq	144(%rsp), %rcx
	movq	%r10, 96(%rsp)
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r12, %rsi
	addq	%r10, %rax
	movq	%rax, 96(%rsp)
	pushq	80(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, 24(%rsp)
	cmpl	$6, %eax
	popq	%r10
	popq	%r11
	movq	96(%rsp), %r10
	movl	108(%rsp), %r11d
	je	.L795
	movq	144(%rsp), %rax
	movq	80(%rsp), %rcx
	cmpq	%rcx, %rax
	jne	.L707
	cmpl	$7, 8(%rsp)
	je	.L796
	cmpl	$0, 8(%rsp)
	jne	.L10
.L708:
	movq	16(%rsp), %rax
	movl	16(%r12), %r11d
	movq	(%rax), %r13
	jmp	.L151
.L489:
	xorl	%ecx, %ecx
	jmp	.L181
.L766:
	cmpq	%r14, %r10
	je	.L279
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L772:
	movzbl	%dil, %esi
	subl	$161, %esi
	cmpl	$15, %esi
	ja	.L288
	subq	$1, %r9
	cmpq	$2, %r9
	jbe	.L505
	movzbl	2(%r13), %ecx
	subl	$161, %ecx
	cmpl	$93, %ecx
	ja	.L288
	movzbl	3(%r13), %edi
	subl	$161, %edi
	cmpl	$93, %edi
	ja	.L288
	imull	$94, %ecx, %ecx
	addl	%edi, %ecx
	cmpl	$14, %esi
	ja	.L288
	leaq	.L290(%rip), %rdi
	movslq	(%rdi,%rsi,4), %rsi
	addq	%rdi, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L290:
	.long	.L289-.L290
	.long	.L291-.L290
	.long	.L292-.L290
	.long	.L293-.L290
	.long	.L294-.L290
	.long	.L295-.L290
	.long	.L296-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L288-.L290
	.long	.L297-.L290
	.text
	.p2align 4,,10
	.p2align 3
.L738:
	cmpl	$913, %edx
	jnb	.L71
	cmpl	$167, %edx
	jb	.L106
	cmpl	$247, %edx
	ja	.L797
	movq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %r8
	leal	-167(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L788:
	cmpl	$65281, %edx
	jnb	.L97
	cmpl	$19968, %edx
	jb	.L106
	cmpl	$40860, %edx
	ja	.L798
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %r8
	leal	-19968(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L763:
	cmpl	$8457, %ecx
	leaq	.LC8(%rip), %rdi
	je	.L363
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L784:
	leal	-711(%rcx), %eax
	cmpl	$18, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L789:
	cmpl	$8592, %edx
	jnb	.L79
	cmpl	$8544, %edx
	jb	.L106
	cmpl	$8553, %edx
	ja	.L799
	subl	$53, %edx
	movb	$36, 126(%rsp)
	movl	$36, %r8d
	movb	%dl, 127(%rsp)
.L104:
	leaq	126(%rsp), %rsi
	jmp	.L396
.L782:
	cmpl	$12288, %edx
	jnb	.L89
	cmpl	$9312, %edx
	jb	.L106
	cmpl	$9341, %edx
	ja	.L800
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %r8
	leal	-9312(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L776:
	cmpl	$65509, %ecx
	leaq	.LC1(%rip), %rdi
	je	.L363
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L790:
	movzbl	153(%rsp), %r9d
	leal	95(%r9), %edi
	cmpb	$93, %dil
	jbe	.L31
	cmpq	$0, 72(%rsp)
	movl	$6, 8(%rsp)
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	72(%rsp), %rdi
	movq	88(%rsp), %rdx
	addq	$1, (%rdi)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L783:
	leal	-8592(%rcx), %eax
	cmpl	$9, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L383:
	cmpq	136(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	je	.L350
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L778:
	cmpl	$8895, %ecx
	leaq	.LC6(%rip), %rdi
	je	.L363
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L458:
	leal	-194560(%rcx), %eax
	cmpl	$541, %eax
	ja	.L444
	leaq	(%rax,%rax,2), %rdi
	movl	$3, %eax
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %rdi
	jmp	.L353
.L457:
	leal	-65281(%rcx), %eax
	cmpl	$92, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L773:
	subl	$161, %ecx
	cmpl	$92, %ecx
	ja	.L301
	cmpq	$1, %r9
	jbe	.L508
	imull	$94, %ecx, %ecx
	leal	-161(%rcx,%rdi), %ecx
	cmpl	$8690, %ecx
	jg	.L301
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %edi
	testw	%di, %di
	je	.L301
	cmpl	$65533, %edi
	je	.L301
.L302:
	addq	$2, %r13
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L455:
	leal	-12549(%rcx), %eax
	cmpl	$36, %eax
	ja	.L444
	addl	$66, %ecx
	movb	$37, 126(%rsp)
	movl	$37, %edi
	movb	%cl, 127(%rsp)
	leaq	126(%rsp), %rax
	jmp	.L358
.L762:
	leal	-8211(%rcx), %eax
	cmpl	$43, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L785:
	cmpl	$40861, %edx
	jnb	.L128
	cmpl	$12963, %edx
	je	.L125
	ja	.L420
	cmpl	$12539, %edx
	je	.L123
	jbe	.L801
	leal	-12549(%rdx), %esi
	cmpl	$36, %esi
	ja	.L410
	addl	$66, %edx
	leaq	126(%rsp), %rsi
	movb	$37, 126(%rsp)
	movb	%dl, 127(%rsp)
.L113:
	subq	$1, %rdi
	movzbl	(%rsi), %eax
	cmpq	$1, %rdi
	jbe	.L105
	movb	%al, 1(%r14)
	movzbl	1(%rsi), %eax
	movb	%al, 2(%r14)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L452:
	leal	-9312(%rcx), %eax
	cmpl	$29, %eax
	ja	.L444
	movq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rdi
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L448:
	movl	$2, %eax
	leaq	.LC10(%rip), %rdi
	jmp	.L363
.L412:
	cmpl	$8807, %edx
	jbe	.L802
	cmpl	$9249, %edx
	ja	.L418
	cmpl	$9216, %edx
	jnb	.L119
	cmpl	$8869, %edx
	movl	$2, %r9d
	jne	.L803
	leaq	.LC7(%rip), %rsi
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L739:
	cmpl	$913, %edx
	jnb	.L110
	cmpl	$167, %edx
	jb	.L410
	cmpl	$247, %edx
	ja	.L804
	leal	-167(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab1@GOTPCREL(%rip), %rsi
.L108:
	movzbl	(%rsi), %r8d
	testb	%r8b, %r8b
	jne	.L399
.L410:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L805
	cmpq	$0, 72(%rsp)
	je	.L142
	testb	$8, %r11b
	jne	.L806
	andb	$2, %r11b
	jne	.L147
.L142:
	movl	$6, 8(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L799:
	leal	-8560(%rdx), %esi
	cmpl	$9, %esi
	ja	.L106
	subl	$59, %edx
	movb	$38, 126(%rsp)
	movl	$38, %r8d
	movb	%dl, 127(%rsp)
	jmp	.L104
.L31:
	cmpl	$142, 8(%rsp)
	je	.L807
	testb	%r10b, %r10b
	js	.L808
.L45:
	cmpq	$0, 72(%rsp)
	movl	$6, 8(%rsp)
	je	.L10
	andl	$2, %r11d
	je	.L10
	movq	72(%rsp), %rdi
	leaq	2(%rsi), %rdx
	addq	$1, (%rdi)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L786:
	cmpl	$65131, %edx
	ja	.L423
	cmpl	$65072, %edx
	jnb	.L130
	cmpl	$64040, %edx
	movl	$3, %r9d
	leaq	.LC11(%rip), %rsi
	je	.L118
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L800:
	leal	-9472(%rdx), %esi
	cmpl	$322, %esi
	ja	.L106
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %r8
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L797:
	leal	-711(%rdx), %esi
	cmpl	$18, %esi
	ja	.L106
	movq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %r8
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L798:
	leal	-65072(%rdx), %esi
	cmpl	$59, %esi
	ja	.L106
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %r8
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L97:
	movq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %r8
	leal	-65281(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L741:
	cmpl	$8457, %edx
	leaq	.LC8(%rip), %rsi
	je	.L118
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L740:
	leal	-8211(%rdx), %esi
	cmpl	$43, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab4@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L803:
	cmpl	$8895, %edx
	leaq	.LC6(%rip), %rsi
	je	.L118
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L418:
	leal	-9312(%rdx), %esi
	cmpl	$29, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab8@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L802:
	cmpl	$8725, %edx
	jnb	.L116
	cmpl	$8560, %edx
	jb	.L410
	cmpl	$8569, %edx
	ja	.L809
	subl	$59, %edx
	movb	$38, 126(%rsp)
	leaq	126(%rsp), %rsi
	movb	%dl, 127(%rsp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L370:
	movl	$2, %eax
	leaq	.LC4(%rip), %rdi
	jmp	.L363
.L372:
	movq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rdi
	leal	-19968(%rcx), %eax
	leaq	(%rdi,%rax,2), %rax
	movzbl	(%rax), %edi
	testb	%dil, %dil
	jne	.L358
.L373:
	leal	-13312(%rcx), %eax
	leaq	(%rax,%rax,2), %rdi
	movl	$3, %eax
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rdi
	jmp	.L353
.L368:
	movl	$2, %eax
	leaq	.LC5(%rip), %rdi
	jmp	.L363
.L809:
	leal	-8592(%rdx), %esi
	cmpl	$9, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L79:
	movq	__cns11643l1_from_ucs4_tab5@GOTPCREL(%rip), %r8
	leal	-8592(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L361:
	movq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rdi
	leal	-8725(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L424:
	leal	-194560(%rdx), %esi
	cmpl	$541, %esi
	ja	.L410
	leaq	(%rsi,%rsi,2), %rsi
	movl	$3, %r9d
	addq	__cns11643_from_ucs4p2c_tab@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L765:
	cmpq	%r14, %r10
	jne	.L277
.L276:
	leaq	__PRETTY_FUNCTION__.9323(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %r8
	leal	-12288(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L71:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %r8
	leal	-913(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L375:
	movq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rdi
	leal	-65072(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L101:
	leaq	.LC1(%rip), %rsi
	jmp	.L74
.L75:
	cmpl	$8453, %edx
	leaq	.LC9(%rip), %rsi
	je	.L74
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	.LC10(%rip), %rsi
	jmp	.L74
.L505:
	movl	$7, %eax
	jmp	.L280
.L366:
	movq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rdi
	leal	-9472(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L423:
	leal	-65281(%rdx), %esi
	cmpl	$92, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab14@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L804:
	leal	-711(%rdx), %esi
	cmpl	$18, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab2@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L92:
	cmpl	$12963, %edx
	leaq	.LC4(%rip), %rsi
	je	.L74
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L380:
	leal	-131072(%rcx), %eax
	leaq	(%rax,%rax,2), %rdi
	movl	$3, %eax
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %rdi
	jmp	.L353
.L87:
	movq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %r8
	leal	-13198(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L801:
	leal	-12288(%rdx), %esi
	cmpl	$41, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab10@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L420:
	cmpl	$19967, %edx
	ja	.L127
	cmpl	$13312, %edx
	jnb	.L128
	leal	-13198(%rdx), %esi
	cmpl	$71, %esi
	ja	.L410
	addq	%rsi, %rsi
	movl	$2, %r9d
	addq	__cns11643l1_from_ucs4_tab11@GOTPCREL(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L364:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rdi
	leal	-9216(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L68:
	leaq	.LC8(%rip), %rsi
	jmp	.L74
.L67:
	movq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %r8
	leal	-9216(%rdx), %esi
	leaq	(%r8,%rsi,2), %rsi
	jmp	.L103
.L787:
	cmpl	$65509, %edx
	leaq	.LC1(%rip), %rsi
	je	.L118
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	.LC2(%rip), %rsi
	jmp	.L74
.L807:
	subl	$161, %r9d
	cmpl	$15, %r9d
	ja	.L33
	leaq	-1(%rdx), %rdi
	cmpq	$2, %rdi
	jbe	.L30
	movzbl	154(%rsp), %edx
	subl	$161, %edx
	cmpl	$93, %edx
	ja	.L33
	movzbl	155(%rsp), %edi
	subl	$161, %edi
	cmpl	$93, %edi
	ja	.L33
	imull	$94, %edx, %edx
	addl	%edi, %edx
	cmpl	$14, %r9d
	ja	.L33
	leaq	.L35(%rip), %rdi
	movslq	(%rdi,%r9,4), %r9
	addq	%r9, %rdi
	jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L35:
	.long	.L34-.L35
	.long	.L36-.L35
	.long	.L37-.L35
	.long	.L38-.L35
	.long	.L39-.L35
	.long	.L40-.L35
	.long	.L41-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L33-.L35
	.long	.L42-.L35
	.text
	.p2align 4,,10
	.p2align 3
.L355:
	movq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rdi
	leal	-913(%rcx), %eax
	leaq	(%rdi,%rax,2), %rdi
	movl	$2, %eax
	jmp	.L353
.L82:
	leaq	.LC7(%rip), %rsi
	jmp	.L74
.L93:
	addl	$66, %edx
	movb	$37, 126(%rsp)
	movl	$37, %r8d
	movb	%dl, 127(%rsp)
	jmp	.L104
.L83:
	cmpl	$8895, %edx
	leaq	.LC6(%rip), %rsi
	je	.L74
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L357:
	subl	$53, %ecx
	movb	$36, 126(%rsp)
	movl	$36, %edi
	movb	%cl, 127(%rsp)
	leaq	126(%rsp), %rax
	jmp	.L358
.L377:
	movl	$2, %eax
	leaq	.LC3(%rip), %rdi
	jmp	.L363
.L767:
	movslq	%r8d, %rax
	movq	%rbp, %r13
	jmp	.L280
.L127:
	leal	-19968(%rdx), %esi
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab12@GOTPCREL(%rip), %rsi
	cmpb	$0, (%rsi)
	jne	.L113
.L128:
	leal	-13312(%rdx), %esi
	movl	$3, %r9d
	leaq	(%rsi,%rsi,2), %rsi
	addq	__cns11643_from_ucs4p0_tab@GOTPCREL(%rip), %rsi
	jmp	.L108
.L795:
	andb	$2, %r11b
	je	.L146
.L147:
	movq	72(%rsp), %rax
	addq	$4, 144(%rsp)
	addq	$1, (%rax)
.L146:
	movq	144(%rsp), %rax
	movq	80(%rsp), %rcx
	cmpq	%rcx, %rax
	jne	.L707
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L796:
	movq	80(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 88(%rsp)
	je	.L810
	movl	(%rbx), %eax
	movq	16(%rsp), %rcx
	movq	%r10, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rcx)
	cmpq	%rdx, %r10
	jle	.L811
	cmpq	$4, %r10
	ja	.L812
	orl	%r10d, %eax
	testq	%r10, %r10
	movl	%eax, (%rbx)
	je	.L57
	movq	80(%rsp), %rcx
	xorl	%eax, %eax
.L156:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L156
	jmp	.L57
.L812:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L811:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L810:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L793:
	leaq	__PRETTY_FUNCTION__.9161(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L792:
	leaq	__PRETTY_FUNCTION__.9161(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L742:
	movzbl	2(%rsi), %eax
	movb	%al, 3(%r14)
	jmp	.L139
.L130:
	leal	-65072(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab13@GOTPCREL(%rip), %rsi
	jmp	.L108
.L121:
	leal	-9472(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab9@GOTPCREL(%rip), %rsi
	jmp	.L108
.L52:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L132:
	movl	$2, %r9d
	leaq	.LC3(%rip), %rsi
	jmp	.L118
.L808:
	movl	8(%rsp), %edi
	subl	$161, %edi
	cmpl	$92, %edi
	ja	.L45
	cmpq	$1, %rdx
	jbe	.L472
	imull	$94, %edi, %edx
	leal	-161(%rdx,%r9), %edx
	cmpl	$8690, %edx
	jg	.L45
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movzwl	(%rdi,%rdx,2), %edi
	testw	%di, %di
	je	.L45
	cmpl	$65533, %edi
	leaq	2(%rsi), %rdx
	jne	.L26
	jmp	.L45
.L123:
	movl	$2, %r9d
	leaq	.LC5(%rip), %rsi
	jmp	.L118
.L135:
	leal	-131072(%rdx), %esi
	movl	$3, %r9d
	leaq	(%rsi,%rsi,2), %rsi
	addq	__cns11643_from_ucs4p2_tab@GOTPCREL(%rip), %rsi
	jmp	.L108
.L472:
	xorl	%edi, %edi
	leaq	2(%rsi), %rdx
	jmp	.L26
.L110:
	leal	-913(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab3@GOTPCREL(%rip), %rsi
	jmp	.L108
.L125:
	movl	$2, %r9d
	leaq	.LC4(%rip), %rsi
	jmp	.L118
.L771:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L42:
	cmpl	$7168, %edx
	jg	.L33
	movq	__cns11643l15_to_ucs4_tab@GOTPCREL(%rip), %rdi
	movslq	%edx, %rdx
	movl	(%rdi,%rdx,4), %edi
.L43:
	cmpl	$65533, %edi
	je	.L33
	testl	%edi, %edi
	je	.L33
	leaq	4(%rsi), %rdx
	jmp	.L26
.L41:
	cmpl	$6538, %edx
	jg	.L33
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edi
	jmp	.L43
.L40:
	cmpl	$6387, %edx
	jg	.L33
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edi
	jmp	.L43
.L39:
	cmpl	$8602, %edx
	jg	.L33
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edi
	jmp	.L43
.L38:
	cmpl	$7297, %edx
	jg	.L33
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edi
	jmp	.L43
.L37:
	cmpl	$6589, %edx
	jg	.L33
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movl	(%r9,%rdx,4), %edi
	jmp	.L43
.L36:
	cmpl	$7649, %edx
	jg	.L33
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edi
	jmp	.L43
.L34:
	cmpl	$8690, %edx
	jg	.L33
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %r9
	movslq	%edx, %rdx
	movzwl	(%r9,%rdx,2), %edi
	jmp	.L43
.L764:
	leaq	__PRETTY_FUNCTION__.9323(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L756:
	leaq	__PRETTY_FUNCTION__.9161(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L779:
	movq	%rsi, 144(%rsp)
	movq	%rsi, %r13
	jmp	.L386
.L508:
	xorl	%edi, %edi
	jmp	.L302
.L791:
	leaq	__PRETTY_FUNCTION__.9161(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L55:
	leaq	__PRETTY_FUNCTION__.9248(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L116:
	leal	-8725(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab6@GOTPCREL(%rip), %rsi
	jmp	.L108
.L805:
	movq	80(%rsp), %rdx
	movl	$4, %r13d
	addq	$4, %rdx
	movq	%rdx, 144(%rsp)
	jmp	.L141
.L176:
	cmpl	$7168, %esi
	jg	.L167
	movq	__cns11643l15_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
.L177:
	testl	%ecx, %ecx
	je	.L167
	cmpl	$65533, %ecx
	je	.L167
	addq	$4, %rax
	jmp	.L161
.L175:
	cmpl	$6538, %esi
	jg	.L167
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
	jmp	.L177
.L174:
	cmpl	$6387, %esi
	jg	.L167
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
	jmp	.L177
.L173:
	cmpl	$8602, %esi
	jg	.L167
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
	jmp	.L177
.L172:
	cmpl	$7297, %esi
	jg	.L167
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
	jmp	.L177
.L171:
	cmpl	$6589, %esi
	jg	.L167
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movl	(%rcx,%rsi,4), %ecx
	jmp	.L177
.L170:
	cmpl	$7649, %esi
	jg	.L167
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movzwl	(%rcx,%rsi,2), %ecx
	jmp	.L177
.L168:
	cmpl	$8690, %esi
	jg	.L167
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rcx
	movslq	%esi, %rsi
	movzwl	(%rcx,%rsi,2), %ecx
	jmp	.L177
.L21:
	leaq	__PRETTY_FUNCTION__.9161(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L734:
	leaq	__PRETTY_FUNCTION__.9323(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
.L112:
	subl	$53, %edx
	movb	$36, 126(%rsp)
	leaq	126(%rsp), %rsi
	movb	%dl, 127(%rsp)
	jmp	.L113
.L414:
	movl	$2, %r9d
	leaq	.LC10(%rip), %rsi
	jmp	.L118
.L119:
	leal	-9216(%rdx), %esi
	movl	$2, %r9d
	addq	%rsi, %rsi
	addq	__cns11643l1_from_ucs4_tab7@GOTPCREL(%rip), %rsi
	jmp	.L108
.L393:
	leaq	__PRETTY_FUNCTION__.9323(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L781:
	movq	72(%rsp), %rax
	addq	$4, %r13
	movl	$6, %r9d
	movq	%r13, 144(%rsp)
	addq	$1, (%rax)
	jmp	.L386
.L780:
	movq	%r10, 88(%rsp)
	movl	%r11d, 80(%rsp)
	leaq	144(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbp, %r8
	movq	%r12, %rsi
	pushq	80(%rsp)
	movq	32(%rsp), %rax
	movq	56(%rsp), %rdi
	leaq	168(%rsp), %r9
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	movl	%eax, %r9d
	popq	%rcx
	movq	144(%rsp), %r13
	movl	80(%rsp), %r11d
	movq	88(%rsp), %r10
	je	.L813
	cmpl	$5, %eax
	jne	.L386
	movq	136(%rsp), %rdx
	cmpq	%rdx, 152(%rsp)
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	je	.L350
	jmp	.L277
.L297:
	cmpl	$7168, %ecx
	jg	.L288
	movq	__cns11643l15_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
.L298:
	testl	%edi, %edi
	je	.L288
	cmpl	$65533, %edi
	je	.L288
	addq	$4, %r13
	jmp	.L282
.L293:
	cmpl	$7297, %ecx
	jg	.L288
	movq	__cns11643l4_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	jmp	.L298
.L292:
	cmpl	$6589, %ecx
	jg	.L288
	movq	__cns11643l3_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	jmp	.L298
.L291:
	cmpl	$7649, %ecx
	jg	.L288
	movq	__cns11643l2_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %edi
	jmp	.L298
.L289:
	cmpl	$8690, %ecx
	jg	.L288
	movq	__cns11643l1_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %edi
	jmp	.L298
.L295:
	cmpl	$6387, %ecx
	jg	.L288
	movq	__cns11643l6_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	jmp	.L298
.L294:
	cmpl	$8602, %ecx
	jg	.L288
	movq	__cns11643l5_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	jmp	.L298
.L296:
	cmpl	$6538, %ecx
	jg	.L288
	movq	__cns11643l7_to_ucs4_tab@GOTPCREL(%rip), %rsi
	movslq	%ecx, %rcx
	movl	(%rsi,%rcx,4), %edi
	jmp	.L298
.L813:
	movq	152(%rsp), %rdx
	jmp	.L387
	.size	gconv, .-gconv
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9248, @object
	.size	__PRETTY_FUNCTION__.9248, 17
__PRETTY_FUNCTION__.9248:
	.string	"to_euc_tw_single"
	.align 16
	.type	__PRETTY_FUNCTION__.9161, @object
	.size	__PRETTY_FUNCTION__.9161, 19
__PRETTY_FUNCTION__.9161:
	.string	"from_euc_tw_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9323, @object
	.size	__PRETTY_FUNCTION__.9323, 6
__PRETTY_FUNCTION__.9323:
	.string	"gconv"
