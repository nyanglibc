	.text
	.p2align 4,,15
	.type	gconv_btowc, @function
gconv_btowc:
	leaq	to_ucs4(%rip), %rax
	movzbl	%sil, %edx
	testb	%sil, %sil
	movl	(%rax,%rdx,4), %eax
	je	.L1
	testl	%eax, %eax
	movl	$-1, %edx
	cmove	%edx, %eax
.L1:
	rep ret
	.size	gconv_btowc, .-gconv_btowc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"INIS//"
	.text
	.p2align 4,,15
	.globl	gconv_init
	.type	gconv_init, @function
gconv_init:
	movq	24(%rdi), %rsi
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %rax
	movl	$7, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L10
	movabsq	$4294967297, %rdx
	movabsq	$17179869188, %rdi
	movq	$0, 96(%rax)
	movq	%rdx, 72(%rax)
	leaq	gconv_btowc(%rip), %rdx
	movq	%rdi, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rax), %rsi
	movl	$7, %ecx
	movq	%rdx, %rdi
	repz cmpsb
	jne	.L13
	movabsq	$17179869188, %rdi
	movabsq	$4294967297, %rdx
	movq	$-1, 96(%rax)
	movq	%rdi, 72(%rax)
	movq	%rdx, 80(%rax)
	movl	$0, 88(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	ret
	.size	gconv_init, .-gconv_init
	.section	.rodata.str1.1
.LC1:
	.string	"../iconv/skeleton.c"
.LC2:
	.string	"outbufstart == NULL"
.LC3:
	.string	"../iconv/loop.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"(state->__count & 7) <= sizeof (state->__value)"
	.align 8
.LC5:
	.string	"inlen_after <= sizeof (state->__value.__wchb)"
	.align 8
.LC6:
	.string	"inptr - bytebuf > (state->__count & 7)"
	.align 8
.LC7:
	.string	"inend != &bytebuf[MAX_NEEDED_INPUT]"
	.align 8
.LC8:
	.string	"inend - inptr > (state->__count & ~7)"
	.align 8
.LC9:
	.string	"inend - inptr <= sizeof (state->__value.__wchb)"
	.section	.rodata.str1.1
.LC10:
	.string	"outbuf == outerr"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"nstatus == __GCONV_FULL_OUTPUT"
	.align 8
.LC12:
	.string	"cnt_after <= sizeof (data->__statep->__value.__wchb)"
	.text
	.p2align 4,,15
	.globl	gconv
	.type	gconv, @function
gconv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	104(%rdi), %rsi
	movq	%rcx, %rbp
	subq	$152, %rsp
	movl	16(%r12), %r14d
	movq	%rsi, 80(%rsp)
	leaq	48(%r12), %rsi
	movq	%rdi, 40(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%r8, 48(%rsp)
	testb	$1, %r14b
	movq	%r9, 16(%rsp)
	movl	208(%rsp), %ebx
	movq	%rsi, 72(%rsp)
	movq	$0, 24(%rsp)
	jne	.L15
	cmpq	$0, 104(%rdi)
	movq	144(%rdi), %rsi
	movq	%rsi, 24(%rsp)
	je	.L15
	movq	%rsi, %rax
#APP
# 410 "../iconv/skeleton.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rsp)
.L15:
	testl	%ebx, %ebx
	jne	.L215
	movq	8(%rsp), %rax
	movq	48(%rsp), %rdi
	leaq	112(%rsp), %rdx
	testq	%rdi, %rdi
	movq	(%rax), %r15
	movq	%rdi, %rax
	cmove	%r12, %rax
	cmpq	$0, 16(%rsp)
	movq	(%rax), %r13
	movq	8(%r12), %rax
	movq	$0, 112(%rsp)
	movq	%rax, 56(%rsp)
	movl	$0, %eax
	cmovne	%rdx, %rax
	movl	216(%rsp), %edx
	movq	%rax, 64(%rsp)
	movq	40(%rsp), %rax
	testl	%edx, %edx
	movq	96(%rax), %rax
	setne	95(%rsp)
	movzbl	95(%rsp), %esi
	testq	%rax, %rax
	je	.L118
	testb	%sil, %sil
	je	.L118
	movq	32(%r12), %rbx
	movl	(%rbx), %r11d
	andl	$7, %r11d
	jne	.L216
.L118:
	movq	$0, 32(%rsp)
.L22:
	leaq	136(%rsp), %rdi
	movl	%r14d, %r11d
	movq	%r13, %r14
	movq	56(%rsp), %r13
	movq	%rdi, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L107:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rdi
	addq	%rdi, 32(%rsp)
.L53:
	testq	%rax, %rax
	je	.L217
	leaq	128(%rsp), %rdi
	movq	%r15, 128(%rsp)
	movq	%r14, %rbx
	movq	%r14, 136(%rsp)
	movq	%r15, %rax
	movl	$4, %r10d
	andl	$2, %r11d
	movq	%rdi, 56(%rsp)
.L61:
	cmpq	%rax, %rbp
	je	.L62
.L71:
	leaq	4(%rax), %rcx
	cmpq	%rcx, %rbp
	jb	.L125
	cmpq	%rbx, %r13
	jbe	.L126
	movl	(%rax), %edx
	cmpl	$65534, %edx
	ja	.L63
	cmpl	$127, %edx
	jbe	.L65
	cmpl	$65534, %edx
	jbe	.L66
.L65:
	leaq	from_ucs4(%rip), %rsi
	movl	%edx, %ecx
	movzbl	(%rsi,%rcx), %ecx
	testb	%cl, %cl
	jne	.L144
	testl	%edx, %edx
	jne	.L66
.L144:
	leaq	1(%rbx), %rdx
	movq	%rdx, 136(%rsp)
	movb	%cl, (%rbx)
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	addq	$4, %rax
	cmpq	%rax, %rbp
	movq	%rax, 128(%rsp)
	jne	.L71
	.p2align 4,,10
	.p2align 3
.L62:
	cmpq	$0, 48(%rsp)
	movq	8(%rsp), %rsi
	movq	%rax, (%rsi)
	jne	.L218
.L72:
	addl	$1, 20(%r12)
	testb	$1, 16(%r12)
	jne	.L219
	cmpq	%rbx, %r14
	jnb	.L129
	movq	24(%rsp), %rdi
	movq	(%r12), %rax
	movl	%r10d, 56(%rsp)
	movq	%rax, 120(%rsp)
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %esi
	leaq	120(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	pushq	%rsi
	pushq	$0
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	movq	40(%rsp), %rax
	call	*%rax
	popq	%r8
	cmpl	$4, %eax
	movl	%eax, %r11d
	popq	%r9
	movl	56(%rsp), %r10d
	je	.L76
	movq	120(%rsp), %r10
	cmpq	%rbx, %r10
	jne	.L220
.L75:
	testl	%r11d, %r11d
	jne	.L140
.L106:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rsi
	movl	16(%r12), %r11d
	movq	(%r12), %r14
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rsi), %r15
	movq	96(%rax), %rax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L63:
	shrl	$7, %edx
	cmpl	$7168, %edx
	je	.L221
.L66:
	cmpq	$0, 64(%rsp)
	je	.L128
	testb	$8, 16(%r12)
	jne	.L222
.L69:
	testl	%r11d, %r11d
	jne	.L223
.L128:
	movl	$6, %r10d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L217:
	cmpq	%r15, %rbp
	je	.L120
	leaq	4(%r14), %rdx
	cmpq	%rdx, %r13
	jb	.L121
	movq	%r15, %rax
	movq	%r14, %rbx
	movl	$4, %r10d
	andl	$2, %r11d
	.p2align 4,,10
	.p2align 3
.L56:
	movzbl	(%rax), %ecx
	leaq	to_ucs4(%rip), %r9
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movl	(%r9,%rcx,4), %ecx
	testl	%ecx, %ecx
	jne	.L57
	testb	%sil, %sil
	jne	.L224
.L57:
	addq	$1, %rax
	movl	%ecx, (%rbx)
	movq	%rdx, %rbx
	movq	%rax, %rdi
.L58:
	cmpq	%rax, %rbp
	je	.L55
	leaq	4(%rbx), %rdx
	cmpq	%rdx, %r13
	jnb	.L56
	movl	$5, %r10d
.L55:
	cmpq	$0, 48(%rsp)
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
	je	.L72
.L218:
	movq	48(%rsp), %rax
	movq	%rbx, (%rax)
.L14:
	addq	$152, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$5, %r10d
	movl	%r10d, %r11d
	jne	.L75
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$7, %r10d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$5, %r10d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L222:
	movl	%r11d, 104(%rsp)
	subq	$8, %rsp
	movq	%rbp, %r8
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%r12, %rsi
	movq	112(%rsp), %r9
	movq	72(%rsp), %rcx
	movq	56(%rsp), %rdi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r11
	cmpl	$6, %r10d
	popq	%rbx
	movq	128(%rsp), %rax
	movq	136(%rsp), %rbx
	movl	104(%rsp), %r11d
	je	.L69
	cmpl	$5, %r10d
	jne	.L61
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L223:
	movq	64(%rsp), %rdi
	addq	$4, %rax
	movl	$6, %r10d
	movq	%rax, 128(%rsp)
	addq	$1, (%rdi)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L219:
	movq	16(%rsp), %rdi
	movq	%rbx, (%r12)
	movq	112(%rsp), %rax
	addq	%rax, (%rdi)
.L74:
	movq	40(%rsp), %rax
	cmpq	$0, 96(%rax)
	setne	%al
	testb	%al, 95(%rsp)
	je	.L14
	cmpl	$7, %r10d
	jne	.L14
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	movq	%rbp, %rax
	subq	%rsi, %rax
	cmpq	$4, %rax
	ja	.L109
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	32(%r12), %rcx
	je	.L111
.L110:
	movzbl	(%rsi,%rdx), %edi
	movb	%dil, 4(%rcx,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L110
.L111:
	movl	(%rcx), %edx
	movq	8(%rsp), %rsi
	andl	$-8, %edx
	movq	%rbp, (%rsi)
	orl	%edx, %eax
	movl	%eax, (%rcx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%r10d, %r11d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L224:
	cmpq	$0, 64(%rsp)
	je	.L124
	testl	%r11d, %r11d
	jne	.L225
.L124:
	movl	$6, %r10d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L220:
	movq	16(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L78
	movq	(%rsi), %rax
.L78:
	addq	112(%rsp), %rax
	cmpq	32(%rsp), %rax
	movq	8(%rsp), %rax
	je	.L226
	movq	%r15, (%rax)
	movq	40(%rsp), %rax
	movl	16(%r12), %r9d
	cmpq	$0, 96(%rax)
	je	.L227
	andl	$2, %r9d
	leaq	128(%rsp), %rbx
	movq	%r15, 128(%rsp)
	movq	%r14, 136(%rsp)
	movq	%r14, %rdx
	movl	$4, %eax
	movl	%r9d, 32(%rsp)
.L93:
	cmpq	%r15, %rbp
	je	.L228
.L103:
	leaq	4(%r15), %rsi
	cmpq	%rsi, %rbp
	jb	.L134
	cmpq	%rdx, %r10
	jbe	.L137
	movl	(%r15), %ecx
	cmpl	$65534, %ecx
	ja	.L95
	cmpl	$127, %ecx
	jbe	.L97
	cmpl	$65534, %ecx
	jbe	.L98
.L97:
	leaq	from_ucs4(%rip), %rdi
	movl	%ecx, %esi
	testl	%ecx, %ecx
	movzbl	(%rdi,%rsi), %esi
	je	.L145
	testb	%sil, %sil
	je	.L98
.L145:
	leaq	1(%rdx), %rcx
	movq	%rcx, 136(%rsp)
	movb	%sil, (%rdx)
	movq	128(%rsp), %rdi
	movq	136(%rsp), %rdx
	leaq	4(%rdi), %r15
	cmpq	%r15, %rbp
	movq	%r15, 128(%rsp)
	jne	.L103
.L228:
	cltq
	movq	%rbp, %r15
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L216:
	testq	%rdi, %rdi
	jne	.L229
	cmpl	$4, %r11d
	movq	%r15, 128(%rsp)
	movq	%r13, 136(%rsp)
	ja	.L24
	leaq	120(%rsp), %rcx
	movslq	%r11d, %r11
	xorl	%eax, %eax
	movq	%rcx, 32(%rsp)
.L25:
	movzbl	4(%rbx,%rax), %edx
	movb	%dl, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%r11, %rax
	jne	.L25
	movq	%r15, %rax
	subq	%r11, %rax
	addq	$4, %rax
	cmpq	%rax, %rbp
	jb	.L230
	cmpq	56(%rsp), %r13
	movl	$5, %r10d
	jnb	.L14
	leaq	1(%r15), %rax
	leaq	119(%rsp), %rsi
.L33:
	movq	%rax, 128(%rsp)
	movzbl	-1(%rax), %ecx
	addq	$1, %r11
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	$3, %r11
	movb	%cl, (%rsi,%r11)
	ja	.L142
	cmpq	%rdx, %rbp
	ja	.L33
.L142:
	movq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	movl	120(%rsp), %eax
	cmpl	$65534, %eax
	ja	.L35
	cmpl	$127, %eax
	jbe	.L37
	cmpl	$65534, %eax
	jbe	.L38
.L37:
	leaq	from_ucs4(%rip), %rcx
	movl	%eax, %edx
	testl	%eax, %eax
	movzbl	(%rcx,%rdx), %edx
	je	.L143
	testb	%dl, %dl
	jne	.L143
.L38:
	cmpq	$0, 64(%rsp)
	je	.L119
	testb	$8, %r14b
	jne	.L231
	andl	$2, %r14d
	je	.L119
	movq	32(%rsp), %rax
.L113:
	movq	64(%rsp), %rsi
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	addq	$1, (%rsi)
.L45:
	cmpq	32(%rsp), %rax
	jne	.L39
.L119:
	movl	$6, %r10d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%rcx, 128(%rsp)
	movq	%rcx, %rax
	jmp	.L61
.L226:
	movq	40(%rsp), %rsi
	subq	%r10, %rbx
	movq	(%rax), %rax
	cmpq	$0, 96(%rsi)
	je	.L232
	movq	8(%rsp), %rdi
	salq	$2, %rbx
	subq	%rbx, %rax
	movq	%rax, (%rdi)
	jmp	.L75
.L143:
	leaq	1(%r13), %rax
	movq	%rax, 136(%rsp)
	movb	%dl, 0(%r13)
	movq	128(%rsp), %rax
	addq	$4, %rax
	cmpq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	je	.L233
.L39:
	movl	(%rbx), %edx
	subq	32(%rsp), %rax
	movl	%edx, %ecx
	andl	$7, %ecx
	cmpq	%rcx, %rax
	jle	.L234
	movq	8(%rsp), %rsi
	subq	%rcx, %rax
	andl	$-8, %edx
	movq	136(%rsp), %r13
	movl	16(%r12), %r14d
	addq	(%rsi), %rax
	movq	%rax, (%rsi)
	movq	%rax, %r15
	movq	112(%rsp), %rax
	movl	%edx, (%rbx)
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	96(%rax), %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r15, %rdi
	movq	%r14, %rbx
	movl	$5, %r10d
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L225:
	movq	64(%rsp), %rsi
	leaq	1(%rax), %rax
	movl	$6, %r10d
	movq	%rax, %rdi
	addq	$1, (%rsi)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L215:
	cmpq	$0, 48(%rsp)
	jne	.L235
	movq	32(%r12), %rax
	xorl	%r10d, %r10d
	movq	$0, (%rax)
	testb	$1, 16(%r12)
	jne	.L14
	movq	24(%rsp), %r15
	movq	%r15, %rdi
	call	_dl_mcount_wrapper_check@PLT
	movl	216(%rsp), %eax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	pushq	%rax
	pushq	%rbx
	movq	32(%rsp), %r9
	movq	88(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	*%r15
	popq	%rcx
	movl	%eax, %r10d
	popq	%rsi
	jmp	.L14
.L140:
	movl	%r11d, %r10d
	jmp	.L74
.L98:
	cmpq	$0, 64(%rsp)
	je	.L138
	testb	$8, 16(%r12)
	jne	.L236
.L101:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	jne	.L237
.L138:
	movl	$6, %eax
.L94:
	movq	8(%rsp), %rdi
	movq	120(%rsp), %r10
	movq	%r15, (%rdi)
.L92:
	cmpq	%r10, %rdx
	jne	.L84
	cmpq	$5, %rax
	jne	.L83
	cmpq	%rdx, %r14
	jne	.L75
.L86:
	subl	$1, 20(%r12)
	jmp	.L75
.L120:
	movq	%rbp, %rdi
	movq	%r14, %rbx
	movl	$4, %r10d
	jmp	.L55
.L236:
	movq	%r10, 104(%rsp)
	movl	%r11d, 56(%rsp)
	subq	$8, %rsp
	pushq	72(%rsp)
	movq	24(%rsp), %rax
	movq	%rbx, %rcx
	movq	112(%rsp), %r9
	movq	56(%rsp), %rdi
	movq	%rbp, %r8
	movq	%r12, %rsi
	movq	(%rax), %rdx
	call	__gconv_transliterate@PLT
	popq	%rdx
	cmpl	$6, %eax
	popq	%rcx
	movq	128(%rsp), %r15
	movq	136(%rsp), %rdx
	movl	56(%rsp), %r11d
	movq	104(%rsp), %r10
	je	.L101
	cmpl	$5, %eax
	jne	.L93
.L137:
	movl	$5, %eax
	jmp	.L94
.L227:
	cmpq	%r15, %rbp
	je	.L238
	leaq	4(%r14), %rcx
	cmpq	%r10, %rcx
	ja	.L239
	movq	64(%rsp), %rbx
	andl	$2, %r9d
	movq	%r14, %rdx
	movl	$4, %eax
	movq	%r15, %rdi
	movl	%r9d, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L87:
	movzbl	(%rdi), %r8d
	leaq	to_ucs4(%rip), %r9
	movq	%rdi, %rsi
	movq	%r8, %r15
	movl	(%r9,%r8,4), %r8d
	testl	%r8d, %r8d
	jne	.L89
	testb	%r15b, %r15b
	jne	.L240
.L89:
	addq	$1, %rdi
	movl	%r8d, (%rdx)
	movq	%rcx, %rdx
	movq	%rdi, %rsi
.L90:
	cmpq	%rdi, %rbp
	je	.L241
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r10
	jnb	.L87
	movl	$5, %eax
.L88:
	movq	8(%rsp), %rbx
	movq	%rsi, (%rbx)
	jmp	.L92
.L134:
	movl	$7, %eax
	jmp	.L94
.L232:
	leaq	3(%rbx), %rdx
	testq	%rbx, %rbx
	cmovs	%rdx, %rbx
	sarq	$2, %rbx
	subq	%rbx, %rax
	movq	8(%rsp), %rbx
	movq	%rax, (%rbx)
	jmp	.L75
.L95:
	shrl	$7, %ecx
	cmpl	$7168, %ecx
	jne	.L98
	movq	%rsi, 128(%rsp)
	movq	%rsi, %r15
	jmp	.L93
.L230:
	movq	8(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%rbp, %rax
	subq	%r15, %rax
	addq	%r11, %rax
	cmpq	$4, %rax
	ja	.L27
	cmpq	%r11, %rax
	leaq	1(%r15), %rdx
	jbe	.L29
.L30:
	movq	%rdx, 128(%rsp)
	movzbl	-1(%rdx), %ecx
	addq	$1, %rdx
	movb	%cl, 4(%rbx,%r11)
	addq	$1, %r11
	cmpq	%r11, %rax
	jne	.L30
.L29:
	movl	$7, %r10d
	jmp	.L14
.L237:
	movq	64(%rsp), %rax
	addq	$4, %r15
	movq	%r15, 128(%rsp)
	addq	$1, (%rax)
	movl	$6, %eax
	jmp	.L93
.L241:
	movq	%rbp, %rsi
	jmp	.L88
.L35:
	shrl	$7, %eax
	cmpl	$7168, %eax
	jne	.L38
	movq	32(%rsp), %rax
	addq	$4, %rax
	movq	%rax, 128(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L240:
	testq	%rbx, %rbx
	je	.L133
	movl	32(%rsp), %edi
	testl	%edi, %edi
	jne	.L242
.L133:
	movl	$6, %eax
	jmp	.L88
.L242:
	leaq	1(%rsi), %rdi
	addq	$1, (%rbx)
	movl	$6, %eax
	movq	%rdi, %rsi
	jmp	.L90
.L231:
	movq	32(%rsp), %rax
	movq	%r11, 104(%rsp)
	leaq	128(%rsp), %rcx
	subq	$8, %rsp
	movq	%r15, %rdx
	movq	%r12, %rsi
	addq	%r11, %rax
	movq	%rax, 104(%rsp)
	pushq	72(%rsp)
	movq	%rax, %r8
	movq	56(%rsp), %rdi
	leaq	152(%rsp), %r9
	call	__gconv_transliterate@PLT
	movl	%eax, %r10d
	popq	%r15
	cmpl	$6, %r10d
	popq	%rax
	movq	128(%rsp), %rax
	movq	104(%rsp), %r11
	je	.L243
	cmpq	32(%rsp), %rax
	jne	.L39
	cmpl	$7, %r10d
	jne	.L48
	movq	32(%rsp), %rax
	addq	$4, %rax
	cmpq	%rax, 96(%rsp)
	je	.L244
	movl	(%rbx), %eax
	movq	8(%rsp), %rsi
	movq	%r11, %rdi
	movl	%eax, %edx
	andl	$-8, %eax
	andl	$7, %edx
	subq	%rdx, %rdi
	movslq	%eax, %rdx
	addq	%rdi, (%rsi)
	cmpq	%rdx, %r11
	jle	.L245
	cmpq	$4, %r11
	ja	.L246
	orl	%r11d, %eax
	testq	%r11, %r11
	movl	%eax, (%rbx)
	je	.L29
	movq	32(%rsp), %rcx
	xorl	%eax, %eax
.L52:
	movzbl	(%rcx,%rax), %edx
	movb	%dl, 4(%rbx,%rax)
	addq	$1, %rax
	cmpq	%rax, %r11
	jne	.L52
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L239:
	cmpq	%r14, %r10
	je	.L86
.L84:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$746, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	movq	112(%rsp), %rax
	movq	8(%rsp), %rbx
	movl	16(%r12), %r14d
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rbx), %r15
	movq	96(%rax), %rax
	jmp	.L22
.L245:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$487, %edx
	call	__assert_fail@PLT
.L244:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$477, %edx
	call	__assert_fail@PLT
.L246:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$488, %edx
	call	__assert_fail@PLT
.L234:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$459, %edx
	call	__assert_fail@PLT
.L48:
	testl	%r10d, %r10d
	jne	.L14
	movq	112(%rsp), %rax
	movq	8(%rsp), %rdi
	movl	16(%r12), %r14d
	movq	%rax, 32(%rsp)
	movq	40(%rsp), %rax
	movq	(%rdi), %r15
	movq	96(%rax), %rax
	jmp	.L22
.L243:
	andb	$2, %r14b
	je	.L45
	jmp	.L113
.L27:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L238:
	cmpq	%r14, %r10
	jne	.L84
.L83:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$747, %edx
	call	__assert_fail@PLT
.L109:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L235:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$420, %edx
	call	__assert_fail@PLT
.L24:
	leaq	__PRETTY_FUNCTION__.9074(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$395, %edx
	call	__assert_fail@PLT
.L229:
	leaq	__PRETTY_FUNCTION__.9153(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$564, %edx
	call	__assert_fail@PLT
	.size	gconv, .-gconv
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9074, @object
	.size	__PRETTY_FUNCTION__.9074, 14
__PRETTY_FUNCTION__.9074:
	.string	"to_gap_single"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9153, @object
	.size	__PRETTY_FUNCTION__.9153, 6
__PRETTY_FUNCTION__.9153:
	.string	"gconv"
	.section	.rodata
	.align 32
	.type	from_ucs4, @object
	.size	from_ucs4, 128
from_ucs4:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	0
	.byte	0
	.byte	0
	.byte	36
	.byte	37
	.byte	0
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	0
	.byte	0
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	91
	.byte	0
	.byte	93
	.byte	0
	.byte	0
	.byte	0
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
	.byte	0
	.byte	124
	.byte	0
	.byte	0
	.byte	127
	.align 32
	.type	to_ucs4, @object
	.size	to_ucs4, 1024
to_ucs4:
	.zero	4
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.zero	12
	.long	36
	.long	37
	.zero	4
	.long	39
	.long	40
	.long	41
	.long	42
	.long	43
	.long	44
	.long	45
	.long	46
	.long	47
	.long	48
	.long	49
	.long	50
	.long	51
	.long	52
	.long	53
	.long	54
	.long	55
	.long	56
	.long	57
	.long	58
	.long	59
	.long	60
	.long	61
	.long	62
	.zero	8
	.long	65
	.long	66
	.long	67
	.long	68
	.long	69
	.long	70
	.long	71
	.long	72
	.long	73
	.long	74
	.long	75
	.long	76
	.long	77
	.long	78
	.long	79
	.long	80
	.long	81
	.long	82
	.long	83
	.long	84
	.long	85
	.long	86
	.long	87
	.long	88
	.long	89
	.long	90
	.long	91
	.zero	4
	.long	93
	.zero	12
	.long	97
	.long	98
	.long	99
	.long	100
	.long	101
	.long	102
	.long	103
	.long	104
	.long	105
	.long	106
	.long	107
	.long	108
	.long	109
	.long	110
	.long	111
	.long	112
	.long	113
	.long	114
	.long	115
	.long	116
	.long	117
	.long	118
	.long	119
	.long	120
	.long	121
	.long	122
	.zero	4
	.long	124
	.zero	8
	.long	127
	.zero	512
