	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	callback, @function
callback:
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	cmpb	$0, (%rcx)
	jne	.L1
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
	movl	$1, %eax
.L1:
	rep ret
	.size	callback, .-callback
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GMON_OUT_PREFIX"
.LC1:
	.string	"gmon.out"
.LC2:
	.string	"%s.%u"
.LC3:
	.string	"_mcleanup: gmon.out: %s\n"
	.text
	.p2align 4,,15
	.type	write_gmon, @function
write_gmon:
	pushq	%rbp
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1768, %rsp
	call	__GI_getenv
	testq	%rax, %rax
	je	.L9
	movq	%rax, %rbx
	movq	__libc_enable_secure@GOTPCREL(%rip), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L50
.L9:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movl	$438, %edx
	movl	$131649, %esi
	call	__GI___open_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L51
.L8:
	leaq	-1776(%rbp), %rsi
	movq	$0, -1768(%rbp)
	movl	$20, %edx
	movl	%ebx, %edi
	movl	$1852796263, -1776(%rbp)
	movl	$1, -1772(%rbp)
	movl	$0, 16(%rsi)
	call	__GI___write_nocancel
	leaq	callback(%rip), %rdi
	leaq	-1784(%rbp), %rsi
	movq	$0, -1784(%rbp)
	call	__GI___dl_iterate_phdr
	movq	-1784(%rbp), %rax
	movb	$0, -1744(%rbp)
	movq	%rax, %rdi
	movq	16+_gmonparam(%rip), %rax
	testq	%rax, %rax
	jne	.L48
	leaq	-1072(%rbp), %r13
	leaq	-1744(%rbp), %r12
	leaq	-1712(%rbp), %r15
.L11:
	leaq	-48(%rbp), %rcx
	leaq	-1785(%rbp), %r10
	movb	$1, -1785(%rbp)
	movq	%r13, %rax
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rdx, 16(%rax)
	movq	%r10, (%rax)
	addq	$32, %rax
	movq	$1, -24(%rax)
	movq	$20, -8(%rax)
	addq	$20, %rdx
	cmpq	%rax, %rcx
	jne	.L12
	movq	32+_gmonparam(%rip), %rax
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	shrq	$3, %rax
	testq	%rax, %rax
	movq	%rax, -1800(%rbp)
	je	.L14
	movq	%r10, -1808(%rbp)
	movq	%rdi, %r10
	.p2align 4,,10
	.p2align 3
.L13:
	movq	24+_gmonparam(%rip), %rax
	movq	(%rax,%r14,8), %rax
	testq	%rax, %rax
	je	.L15
	movq	88+_gmonparam(%rip), %rdi
	movq	64+_gmonparam(%rip), %rcx
	movq	40+_gmonparam(%rip), %rsi
	leaq	0(,%rdi,8), %r9
	subq	%r10, %rcx
	imulq	%r14, %r9
	addq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	(%rax,%rax,2), %r8
	movq	%r9, -1744(%rbp)
	salq	$3, %r8
	leaq	(%rsi,%r8), %rax
	movq	(%rax), %rcx
	subq	%r10, %rcx
	movq	%rcx, -1736(%rbp)
	movq	8(%rax), %rcx
	movl	%ecx, -1728(%rbp)
	movslq	%edx, %rcx
	movl	16(%r12), %edi
	leaq	(%rcx,%rcx,4), %rcx
	movdqa	(%r12), %xmm0
	addl	$1, %edx
	leaq	(%r15,%rcx,4), %rcx
	cmpl	$32, %edx
	movups	%xmm0, (%rcx)
	movl	%edi, 16(%rcx)
	jne	.L16
	movl	$64, %edx
	movq	%r13, %rsi
	movl	%ebx, %edi
	movl	$20, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	40+_gmonparam(%rip), %rsi
	xorl	%edx, %edx
	leaq	(%rsi,%r8), %rax
.L16:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L17
.L15:
	addq	$1, %r14
	cmpq	%r14, -1800(%rbp)
	jne	.L13
	testl	%edx, %edx
	movq	-1808(%rbp), %r10
	je	.L14
	addl	%edx, %edx
	movq	%r13, %rsi
	movl	%ebx, %edi
	movl	$20, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
.L14:
	movq	__bb_head(%rip), %r9
	movb	$2, -1785(%rbp)
	movq	%r10, -1712(%rbp)
	movq	$1, -1704(%rbp)
	movq	%r12, -1696(%rbp)
	movq	$8, -1688(%rbp)
	testq	%r9, %r9
	movq	$8, -1064(%rbp)
	movq	$8, -1048(%rbp)
	movq	$8, -1032(%rbp)
	movq	$8, -1016(%rbp)
	movq	$8, -1000(%rbp)
	movq	$8, -984(%rbp)
	movq	$8, -968(%rbp)
	movq	$8, -952(%rbp)
	je	.L19
	movl	$20, %r10d
	.p2align 4,,10
	.p2align 3
.L24:
	movq	24(%r9), %rax
	movl	$2, %edx
	movq	%r15, %rsi
	movl	%ebx, %edi
	movq	%rax, -1744(%rbp)
	movl	%r10d, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	-1744(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L20
	movq	40(%r9), %r11
	movq	16(%r9), %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	0(,%r8,8), %rax
	leaq	1(%rdx), %rcx
	movq	%rdx, %r12
	addq	$1, %r8
	salq	$4, %r12
	addq	$2, %rdx
	leaq	(%r11,%rax), %r14
	salq	$4, %rcx
	addq	%rdi, %rax
	cmpq	%rsi, %r8
	movq	%r14, -1072(%rbp,%r12)
	movq	%rax, -1072(%rbp,%rcx)
	jnb	.L52
	cmpq	$6, %rdx
	jbe	.L21
	movq	%r13, %rsi
	movl	%ebx, %edi
	movl	%r10d, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	xorl	%edx, %edx
	movq	40(%r9), %r11
	movq	16(%r9), %rdi
	movq	-1744(%rbp), %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r13, %rsi
	movl	%ebx, %edi
	movl	%r10d, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
.L20:
	movq	32(%r9), %r9
	testq	%r9, %r9
	jne	.L24
.L19:
	movl	%ebx, %edi
	call	__GI___close_nocancel
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	8+_gmonparam(%rip), %rdx
	leaq	-1712(%rbp), %r15
	leaq	-1744(%rbp), %r12
	movq	%rax, -1032(%rbp)
	leaq	-1072(%rbp), %r13
	shrq	%rax
	movq	%r12, -1072(%rbp)
	movq	$1, -1064(%rbp)
	movq	%rdx, -1040(%rbp)
	movq	64+_gmonparam(%rip), %rdx
	movq	%r15, -1056(%rbp)
	movq	$40, -1048(%rbp)
	movl	%eax, -1696(%rbp)
	subq	%rdi, %rdx
	movq	%rdx, -1712(%rbp)
	movq	72+_gmonparam(%rip), %rdx
	subq	%rdi, %rdx
	movq	%rdx, -1704(%rbp)
	call	__GI___profile_frequency
	movabsq	$32480047799690611, %rdi
	movl	%eax, -1692(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1688(%rbp)
	movl	$3, %edx
	movw	%ax, 36(%r15)
	movl	$0, 32(%r15)
	movb	$0, 38(%r15)
	movq	%r13, %rsi
	movb	$115, -1673(%rbp)
	movl	%ebx, %edi
	movl	$20, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	-1784(%rbp), %rax
	movq	%rax, %rdi
	jmp	.L11
.L50:
	movq	%rbx, %rdi
	movq	%rsp, %r12
	call	__GI_strlen
	leaq	20(%rax), %r13
	addq	$35, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	call	__GI___getpid
	leaq	.LC2(%rip), %rdx
	movl	%eax, %r8d
	movq	%rbx, %rcx
	movq	%rsp, %rdi
	movq	%r13, %rsi
	xorl	%eax, %eax
	call	__GI___snprintf
	movq	%rsp, %rdi
	xorl	%eax, %eax
	movl	$438, %edx
	movl	$131649, %esi
	call	__GI___open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movq	%r12, %rsp
	jne	.L8
	jmp	.L9
.L51:
	movq	__libc_errno@gottpoff(%rip), %rax
	leaq	-1072(%rbp), %rsi
	movl	$300, %edx
	movl	%fs:(%rax), %edi
	call	__GI___strerror_r
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	write_gmon, .-write_gmon
	.p2align 4,,15
	.type	__GI___moncontrol.part.0, @function
__GI___moncontrol.part.0:
	subq	$8, %rsp
	movl	s_scale(%rip), %ecx
	movq	64+_gmonparam(%rip), %rdx
	movq	16+_gmonparam(%rip), %rsi
	movq	8+_gmonparam(%rip), %rdi
	call	__profil
	movq	$0, _gmonparam(%rip)
	addq	$8, %rsp
	ret
	.size	__GI___moncontrol.part.0, .-__GI___moncontrol.part.0
	.p2align 4,,15
	.globl	__GI___moncontrol
	.hidden	__GI___moncontrol
	.type	__GI___moncontrol, @function
__GI___moncontrol:
	cmpq	$2, _gmonparam(%rip)
	je	.L59
	testl	%edi, %edi
	jne	.L61
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__profil
	movq	$3, _gmonparam(%rip)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	jmp	__GI___moncontrol.part.0
	.p2align 4,,10
	.p2align 3
.L59:
	rep ret
	.size	__GI___moncontrol, .-__GI___moncontrol
	.globl	__moncontrol
	.set	__moncontrol,__GI___moncontrol
	.weak	moncontrol
	.set	moncontrol,__moncontrol
	.section	.rodata.str1.1
.LC4:
	.string	"monstartup: out of memory\n"
	.text
	.p2align 4,,15
	.globl	__monstartup
	.type	__monstartup, @function
__monstartup:
	pushq	%r13
	pushq	%r12
	andq	$-4, %rdi
	pushq	%rbp
	leaq	3(%rsi), %rbp
	pushq	%rbx
	movq	%rdi, %r13
	movabsq	$2951479051793528259, %rcx
	andq	$-4, %rbp
	subq	$8, %rsp
	movq	%rdi, 64+_gmonparam(%rip)
	movq	%rbp, %rax
	movq	%rbp, 72+_gmonparam(%rip)
	movq	$2, 88+_gmonparam(%rip)
	subq	%rdi, %rax
	movq	$4, 96+_gmonparam(%rip)
	leaq	(%rax,%rax,2), %rdx
	movq	%rax, %rdi
	movq	%rax, 80+_gmonparam(%rip)
	shrq	%rdi
	shrq	$2, %rdx
	leaq	7(%rdi), %rbx
	movq	%rdi, 32+_gmonparam(%rip)
	movq	%rdx, %rax
	mulq	%rcx
	andq	$-8, %rbx
	movq	%rbx, 16+_gmonparam(%rip)
	shrq	$2, %rdx
	cmpq	$49, %rdx
	ja	.L63
	movq	$50, 56+_gmonparam(%rip)
	movl	$1200, %r12d
.L64:
	addq	%rbx, %rdi
	movl	$1, %esi
	movq	%r12, 48+_gmonparam(%rip)
	addq	%r12, %rdi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L73
	subl	%r13d, %ebp
	addq	%rax, %r12
	movq	%rax, 40+_gmonparam(%rip)
	movq	$0, 16(%rax)
	movslq	%ebp, %rax
	movq	%r12, 8+_gmonparam(%rip)
	addq	%rbx, %r12
	cmpq	%rax, %rbx
	movq	%r12, 24+_gmonparam(%rip)
	jnb	.L68
	testq	%rbx, %rbx
	js	.L69
	pxor	%xmm0, %xmm0
	cvtsi2ssq	%rbx, %xmm0
.L70:
	cmpq	$2, _gmonparam(%rip)
	pxor	%xmm1, %xmm1
	cvtsi2ss	%ebp, %xmm1
	divss	%xmm1, %xmm0
	mulss	.LC5(%rip), %xmm0
	cvttss2si	%xmm0, %eax
	movl	%eax, s_scale(%rip)
	jne	.L74
.L62:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$1048576, %rdx
	jbe	.L75
	movq	$1048576, 56+_gmonparam(%rip)
	movl	$25165824, %r12d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	cmpq	$2, _gmonparam(%rip)
	movl	$65536, s_scale(%rip)
	je	.L62
.L74:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI___moncontrol.part.0
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	(%rdx,%rdx,2), %r12
	movq	%rdx, 56+_gmonparam(%rip)
	salq	$3, %r12
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L69:
	pxor	%xmm0, %xmm0
	shrq	%rbx
	cvtsi2ssq	%rbx, %xmm0
	addss	%xmm0, %xmm0
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC4(%rip), %rsi
	movl	$26, %edx
	movl	$2, %edi
	call	__GI___write_nocancel
	movq	$0, 40+_gmonparam(%rip)
	movq	$2, _gmonparam(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__monstartup, .-__monstartup
	.weak	monstartup
	.set	monstartup,__monstartup
	.p2align 4,,15
	.globl	__write_profiling
	.type	__write_profiling, @function
__write_profiling:
	pushq	%rbx
	movq	_gmonparam(%rip), %rbx
	movq	$3, _gmonparam(%rip)
	testl	%ebx, %ebx
	jne	.L77
	call	write_gmon
.L77:
	movslq	%ebx, %rbx
	movq	%rbx, _gmonparam(%rip)
	popq	%rbx
	ret
	.size	__write_profiling, .-__write_profiling
	.p2align 4,,15
	.globl	_mcleanup
	.type	_mcleanup, @function
_mcleanup:
	subq	$8, %rsp
	xorl	%edi, %edi
	call	__GI___moncontrol
	cmpq	$2, _gmonparam(%rip)
	je	.L80
	call	write_gmon
.L80:
	movq	40+_gmonparam(%rip), %rdi
	addq	$8, %rsp
	jmp	free@PLT
	.size	_mcleanup, .-_mcleanup
	.local	s_scale
	.comm	s_scale,4,4
	.hidden	_gmonparam
	.globl	_gmonparam
	.data
	.align 32
	.type	_gmonparam, @object
	.size	_gmonparam, 104
_gmonparam:
	.quad	3
	.zero	96
	.hidden	__bb_head
	.comm	__bb_head,8,8
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	1199570944
	.hidden	__profil
	.hidden	__fxprintf
