.globl _mcount
.type _mcount,@function
.align 1<<4
_mcount:
 subq $56,%rsp
 movq %rax,(%rsp)
 movq %rcx,8(%rsp)
 movq %rdx,16(%rsp)
 movq %rsi,24(%rsp)
 movq %rdi,32(%rsp)
 movq %r8,40(%rsp)
 movq %r9,48(%rsp)
 movq 56(%rsp),%rsi
 movq 8(%rbp),%rdi
 call __mcount_internal
 movq 48(%rsp),%r9
 movq 40(%rsp),%r8
 movq 32(%rsp),%rdi
 movq 24(%rsp),%rsi
 movq 16(%rsp),%rdx
 movq 8(%rsp),%rcx
 movq (%rsp),%rax
 addq $56,%rsp
 ret
.weak mcount
mcount = _mcount
.globl __fentry__
.type __fentry__,@function
.align 1<<4
__fentry__:
 subq $64,%rsp
 movq %rax,(%rsp)
 movq %rcx,8(%rsp)
 movq %rdx,16(%rsp)
 movq %rsi,24(%rsp)
 movq %rdi,32(%rsp)
 movq %r8,40(%rsp)
 movq %r9,48(%rsp)
 movq 64(%rsp),%rsi
 movq 72(%rsp),%rdi
 call __mcount_internal
 movq 48(%rsp),%r9
 movq 40(%rsp),%r8
 movq 32(%rsp),%rdi
 movq 24(%rsp),%rsi
 movq 16(%rsp),%rdx
 movq 8(%rsp),%rcx
 movq (%rsp),%rax
 addq $64,%rsp
 ret
.size __fentry__,.-__fentry__
