	.text
	.p2align 4,,15
	.type	__profil_counter, @function
__profil_counter:
	movq	168(%rdx), %rdx
	subq	pc_offset(%rip), %rdx
	movl	pc_scale(%rip), %eax
	movq	%rdx, %rcx
	shrq	$17, %rdx
	shrq	%rcx
	movzwl	%cx, %ecx
	imulq	%rax, %rcx
	imulq	%rax, %rdx
	shrq	$16, %rcx
	leaq	(%rdx,%rcx), %rax
	cmpq	nsamples(%rip), %rax
	jnb	.L2
	movq	samples(%rip), %rdx
	addw	$1, (%rdx,%rax,2)
.L2:
	ret
	.size	__profil_counter, .-__profil_counter
	.p2align 4,,15
	.globl	__profil
	.hidden	__profil
	.type	__profil, @function
__profil:
	pushq	%rbx
	shrq	%rsi
	leaq	__profil_counter(%rip), %rax
	movq	$-1, %rbx
	subq	$192, %rsp
	movq	%rsi, nsamples(%rip)
	movq	%rdi, samples(%rip)
	leaq	32(%rsp), %rsi
	movq	%rdx, pc_offset(%rip)
	movl	$27, %edi
	xorl	%edx, %edx
	movl	%ecx, pc_scale(%rip)
	movq	%rax, 32(%rsp)
	movl	$268435460, 168(%rsp)
	movq	%rbx, 40(%rsp)
	call	__sigaction
	testl	%eax, %eax
	js	.L6
	movq	$0, 16(%rsp)
	call	__profile_frequency
	movl	%eax, %ecx
	movl	$1000000, %eax
	movq	%rsp, %rsi
	cltd
	movl	$2, %edi
	idivl	%ecx
	xorl	%edx, %edx
	cltq
	movq	%rax, 24(%rsp)
	movdqa	16(%rsp), %xmm0
	movaps	%xmm0, (%rsp)
	call	__setitimer
.L4:
	addq	$192, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ebx, %eax
	jmp	.L4
	.size	__profil, .-__profil
	.weak	profil
	.set	profil,__profil
	.local	pc_scale
	.comm	pc_scale,4,4
	.local	pc_offset
	.comm	pc_offset,8,8
	.local	nsamples
	.comm	nsamples,8,8
	.local	samples
	.comm	samples,8,8
	.hidden	__setitimer
	.hidden	__profile_frequency
	.hidden	__sigaction
