	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___profile_frequency
	.hidden	__GI___profile_frequency
	.type	__GI___profile_frequency, @function
__GI___profile_frequency:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	56(%rax), %eax
	ret
	.size	__GI___profile_frequency, .-__GI___profile_frequency
	.globl	__profile_frequency
	.set	__profile_frequency,__GI___profile_frequency
