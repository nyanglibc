	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mcount_internal
	.type	__mcount_internal, @function
__mcount_internal:
	pushq	%rbp
	xorl	%eax, %eax
	movl	$1, %edx
	movq	%rsp, %rbp
	lock cmpxchgq	%rdx, _gmonparam(%rip)
	jne	.L1
	subq	64+_gmonparam(%rip), %rdi
	cmpq	%rdi, 80+_gmonparam(%rip)
	jb	.L3
	movq	96+_gmonparam(%rip), %rcx
	movq	24+_gmonparam(%rip), %rax
	shrq	%cl, %rdi
	movslq	%edi, %rdi
	leaq	(%rax,%rdi,8), %r8
	movq	40+_gmonparam(%rip), %rdi
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L4
	movq	16(%rdi), %rax
	addq	$1, %rax
	cmpq	56+_gmonparam(%rip), %rax
	movq	%rax, 16(%rdi)
	jb	.L15
.L5:
	movq	$2, _gmonparam(%rip)
.L1:
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	16(%rdx), %rsi
	addq	$1, 8(%rdx)
	movq	%rsi, 16(%rcx)
	movq	(%r8), %rcx
	movq	%rcx, 16(%rdx)
	movq	%rax, (%r8)
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$0, _gmonparam(%rip)
.L17:
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	cmpq	%rsi, (%rcx)
	jne	.L6
	addq	$1, 8(%rcx)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpq	%rsi, (%rdx)
	je	.L16
	movq	%rdx, %rcx
.L6:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	jne	.L7
	movq	16(%rdi), %rax
	addq	$1, %rax
	cmpq	56+_gmonparam(%rip), %rax
	movq	%rax, 16(%rdi)
	jnb	.L5
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rsi, (%rdx)
	movq	$1, 8(%rdx)
	movq	(%r8), %rcx
	movq	%rcx, 16(%rdx)
	movq	%rax, (%r8)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rax, (%r8)
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	%rsi, (%rax)
	movq	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, _gmonparam(%rip)
	jmp	.L17
	.size	__mcount_internal, .-__mcount_internal
	.hidden	_gmonparam
