	.text
	.p2align 4,,15
	.type	__profil_counter, @function
__profil_counter:
	movq	168(%rdx), %rdx
	subq	pc_offset(%rip), %rdx
	movl	pc_scale(%rip), %eax
	movq	%rdx, %rcx
	shrq	$17, %rdx
	shrq	%rcx
	movzwl	%cx, %ecx
	imulq	%rax, %rcx
	imulq	%rax, %rdx
	shrq	$16, %rcx
	leaq	(%rdx,%rcx), %rax
	cmpq	nsamples(%rip), %rax
	jnb	.L2
	movq	samples(%rip), %rdx
	addw	$1, (%rdx,%rax,2)
.L2:
	ret
	.size	__profil_counter, .-__profil_counter
	.p2align 4,,15
	.globl	__profil
	.hidden	__profil
	.type	__profil, @function
__profil:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	testq	%rdi, %rdi
	movq	samples(%rip), %rdx
	je	.L16
	testq	%rdx, %rdx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	movl	%ecx, %r13d
	je	.L11
	leaq	otimer.4217(%rip), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__setitimer
	testl	%eax, %eax
	js	.L9
	leaq	oact.4216(%rip), %rsi
	xorl	%edx, %edx
	movl	$27, %edi
	call	__sigaction
	testl	%eax, %eax
	js	.L9
.L11:
	leaq	__profil_counter(%rip), %rax
	leaq	32(%rsp), %rsi
	leaq	oact.4216(%rip), %rdx
	shrq	%rbx
	movl	$27, %edi
	movq	%rbp, samples(%rip)
	movq	%rbx, nsamples(%rip)
	movq	%r12, pc_offset(%rip)
	movl	%r13d, pc_scale(%rip)
	movq	%rax, 32(%rsp)
	movl	$268435460, 168(%rsp)
	movq	$-1, 40(%rsp)
	call	__sigaction
	testl	%eax, %eax
	js	.L9
	movq	$0, 16(%rsp)
	call	__profile_frequency
	movl	%eax, %ecx
	movl	$1000000, %eax
	movq	%rsp, %rsi
	cltd
	movl	$2, %edi
	idivl	%ecx
	leaq	otimer.4217(%rip), %rdx
	cltq
	movq	%rax, 24(%rsp)
	movdqa	16(%rsp), %xmm0
	movaps	%xmm0, (%rsp)
	call	__setitimer
.L4:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L4
	leaq	otimer.4217(%rip), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__setitimer
	testl	%eax, %eax
	js	.L9
	leaq	oact.4216(%rip), %rsi
	xorl	%edx, %edx
	movl	$27, %edi
	movq	$0, samples(%rip)
	call	__sigaction
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$-1, %eax
	jmp	.L4
	.size	__profil, .-__profil
	.weak	profil
	.set	profil,__profil
	.local	oact.4216
	.comm	oact.4216,152,32
	.local	otimer.4217
	.comm	otimer.4217,32,32
	.local	pc_scale
	.comm	pc_scale,4,4
	.local	pc_offset
	.comm	pc_offset,8,8
	.local	nsamples
	.comm	nsamples,8,8
	.local	samples
	.comm	samples,8,8
	.hidden	__profile_frequency
	.hidden	__sigaction
	.hidden	__setitimer
