	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	profil_count, @function
profil_count:
	movq	16+prof_info(%rip), %rdx
	cmpq	%rdi, 32(%rdx)
	ja	.L2
	cmpq	%rdi, 40(%rdx)
	movq	%rdx, %rax
	ja	.L3
.L2:
	movl	prof_info(%rip), %eax
	movq	8+prof_info(%rip), %r9
	xorl	%ecx, %ecx
	leal	-1(%rax), %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	cmpq	%rdi, 40(%rax)
	ja	.L28
	leaq	1(%rdx), %rcx
	cmpq	%r8, %rcx
	ja	.L6
.L8:
	leaq	(%rcx,%r8), %rdx
	shrq	%rdx
	leaq	(%rdx,%rdx,2), %rax
	salq	$4, %rax
	addq	%r9, %rax
	cmpq	%rdi, 32(%rax)
	jbe	.L29
	leaq	-1(%rdx), %r8
	cmpq	%r8, %rcx
	jbe	.L8
.L6:
	movq	24+prof_info(%rip), %rdx
.L3:
	subq	(%rdx), %rdi
	testl	%esi, %esi
	movl	16(%rdx), %r8d
	movq	8(%rax), %rcx
	je	.L30
	movq	%rdi, %rdx
	shrq	$18, %rdi
	shrq	$2, %rdx
	movzwl	%dx, %edx
	imulq	%r8, %rdx
	imulq	%r8, %rdi
	shrq	$16, %rdx
	addq	%rdx, %rdi
	cmpq	%rdi, %rcx
	jbe	.L31
	movq	24(%rax), %rax
	leaq	(%rax,%rdi,4), %rdx
	movl	(%rdx), %eax
	cmpl	$-1, %eax
	je	.L1
	addl	$1, %eax
	movl	%eax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rdi, %rdx
	shrq	$17, %rdi
	shrq	%rdx
	movzwl	%dx, %edx
	imulq	%r8, %rdx
	imulq	%r8, %rdi
	shrq	$16, %rdx
	addq	%rdx, %rdi
	cmpq	%rdi, %rcx
	jbe	.L32
	movq	24(%rax), %rax
	leaq	(%rax,%rdi,2), %rdx
	movzwl	(%rdx), %eax
	cmpw	$-1, %ax
	je	.L1
	addl	$1, %eax
	movw	%ax, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	24+prof_info(%rip), %rax
	movq	24(%rax), %rax
	addw	$1, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	24+prof_info(%rip), %rax
	movq	24(%rax), %rax
	addl	$1, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rax, 16+prof_info(%rip)
	movq	24+prof_info(%rip), %rdx
	jmp	.L3
	.size	profil_count, .-profil_count
	.p2align 4,,15
	.type	__profil_counter_ushort, @function
__profil_counter_ushort:
	movq	168(%rdx), %rdi
	xorl	%esi, %esi
	call	profil_count
	ret
	.size	__profil_counter_ushort, .-__profil_counter_ushort
	.p2align 4,,15
	.type	__profil_counter_uint, @function
__profil_counter_uint:
	movq	168(%rdx), %rdi
	movl	$1, %esi
	call	profil_count
	ret
	.size	__profil_counter_uint, .-__profil_counter_uint
	.p2align 4,,15
	.type	pcmp, @function
pcmp:
	movq	(%rdi), %rcx
	movq	(%rsi), %rdx
	movl	$1, %eax
	movq	16(%rdx), %rdx
	cmpq	%rdx, 16(%rcx)
	jb	.L35
	seta	%al
	movzbl	%al, %eax
	negl	%eax
.L35:
	rep ret
	.size	pcmp, .-pcmp
	.p2align 4,,15
	.type	insert.part.2, @function
insert.part.2:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$24, %rsp
	movl	prof_info(%rip), %eax
	movl	%r8d, 12(%rsp)
	testl	%eax, %eax
	jne	.L39
	movl	$48, %edi
	call	malloc@PLT
	movl	12(%rsp), %r8d
	movq	%rax, %r14
.L40:
	testq	%r14, %r14
	je	.L44
	movl	prof_info(%rip), %edi
	movslq	%r15d, %rax
	leaq	(%rax,%rax,2), %rax
	movl	%edi, %edx
	salq	$4, %rax
	subl	%r15d, %edx
	leaq	(%r14,%rax), %rbx
	jne	.L57
.L42:
	cmpl	$1, %r8d
	movq	8(%rbp), %rax
	movq	16(%rbp), %rcx
	sbbq	%rsi, %rsi
	xorl	%edx, %edx
	addl	$1, %edi
	andq	$-2, %rsi
	addq	$4, %rsi
	movq	%rcx, (%rbx)
	divq	%rsi
	movq	0(%rbp), %rdx
	movl	%edi, prof_info(%rip)
	movq	%rax, 8(%rbx)
	movq	24(%rbp), %rax
	movq	%r14, 8+prof_info(%rip)
	movq	%rdx, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r12, 40(%rbx)
	cmpq	$2, %rax
	movl	%eax, 16(%rbx)
	jne	.L46
	testq	%rcx, %rcx
	je	.L58
.L46:
	xorl	%eax, %eax
.L38:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	addl	$1, %eax
	movq	8+prof_info(%rip), %rdi
	leaq	(%rax,%rax,2), %rsi
	salq	$4, %rsi
	call	realloc@PLT
	movl	12(%rsp), %r8d
	movq	%rax, %r14
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r14, 24+prof_info(%rip)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%edx, %edx
	leaq	48(%r14,%rax), %rdi
	movq	%rbx, %rsi
	leaq	(%rdx,%rdx,2), %rdx
	movl	%r8d, 12(%rsp)
	salq	$4, %rdx
	call	__GI_memmove
	movl	prof_info(%rip), %edi
	movl	12(%rsp), %r8d
	jmp	.L42
.L44:
	movl	$-1, %eax
	jmp	.L38
	.size	insert.part.2, .-insert.part.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../sysdeps/posix/sprofil.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"pc_to_index (pc - 1, offset, scale, prof_uint) < n && pc_to_index (pc, offset, scale, prof_uint) >= n"
	.text
	.p2align 4,,15
	.globl	__sprofil
	.type	__sprofil, @function
__sprofil:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movslq	%esi, %r12
	pushq	%rbx
	leaq	22(,%r12,8), %rax
	movq	%r12, %r13
	subq	$232, %rsp
	andq	$-16, %rax
	movl	%ecx, -256(%rbp)
	subq	%rax, %rsp
	testq	%rdx, %rdx
	movq	%rsp, %rbx
	je	.L60
	movq	%rdx, %r14
	movq	%rdi, -248(%rbp)
	call	__GI___profile_frequency
	movl	%eax, %ecx
	movl	$1000000, %eax
	movq	-248(%rbp), %rdi
	cltd
	idivl	%ecx
	movabsq	$4835703278458516699, %rdx
	movslq	%eax, %rcx
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$18, %rdx
	movq	%rdx, (%r14)
	imulq	$1000000, %rdx, %rdx
	subq	%rdx, %rcx
	movq	%rcx, 8(%r14)
.L60:
	movl	prof_info(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L92
	leaq	default_overflow_region(%rip), %rax
	testl	%r13d, %r13d
	movq	$0, 8+prof_info(%rip)
	movq	%rax, 24+prof_info(%rip)
	jle	.L65
	leal	-1(%r13), %r8d
	movq	%rbx, %r14
	movq	%r8, %rax
	salq	$5, %rax
	leaq	32(%rdi,%rax), %rdx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rdi, (%rax)
	addq	$32, %rdi
	addq	$8, %rax
	cmpq	%rdi, %rdx
	jne	.L66
	leaq	pcmp(%rip), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -248(%rbp)
	call	__GI_qsort
	movl	-256(%rbp), %eax
	movq	-248(%rbp), %r8
	andl	$1, %eax
	cmpl	$1, %eax
	movl	%eax, -252(%rbp)
	sbbq	%rax, %rax
	andq	$-2, %rax
	addq	$4, %rax
	movq	%rax, -272(%rbp)
	leaq	8(%rbx,%r8,8), %rax
	movq	%rax, -264(%rbp)
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%r14), %r15
	movq	24(%r15), %rcx
	cmpq	$1, %rcx
	jbe	.L68
	movq	-272(%rbp), %rbx
	movq	8(%r15), %rax
	xorl	%edx, %edx
	movl	%ecx, %ecx
	movq	16(%r15), %rsi
	divq	%rbx
	xorl	%edx, %edx
	movq	%rax, %r8
	movq	%rbx, %rax
	imulq	%r8, %rax
	divq	%rcx
	salq	$16, %rax
	leaq	(%rax,%rsi), %rdi
	movq	%rdx, %rax
	xorl	%edx, %edx
	salq	$16, %rax
	divq	%rcx
	xorl	%edx, %edx
	leaq	(%rdi,%rax), %r12
	movq	%r12, %rax
	subq	%rsi, %rax
	divq	%rbx
	movzwl	%ax, %edi
	shrq	$16, %rax
	imulq	%rcx, %rdi
	imulq	%rcx, %rax
	shrq	$16, %rdi
	addq	%rdi, %rax
	cmpq	%r8, %rax
	movq	%rsi, %rax
	adcq	$0, %r12
	notq	%rax
	xorl	%edx, %edx
	addq	%r12, %rax
	divq	%rbx
	movzwl	%ax, %edi
	shrq	$16, %rax
	imulq	%rcx, %rdi
	imulq	%rcx, %rax
	shrq	$16, %rdi
	addq	%rdi, %rax
	cmpq	%rax, %r8
	jbe	.L70
	movq	%r12, %rax
	xorl	%edx, %edx
	subq	%rsi, %rax
	divq	%rbx
	movzwl	%ax, %edi
	shrq	$16, %rax
	imulq	%rcx, %rdi
	imulq	%rax, %rcx
	shrq	$16, %rdi
	addq	%rdi, %rcx
	cmpq	%rcx, %r8
	ja	.L70
	movl	prof_info(%rip), %edi
	xorl	%ebx, %ebx
	leaq	prof_info(%rip), %r13
	movq	8+prof_info(%rip), %rcx
	testl	%edi, %edi
	je	.L93
	.p2align 4,,10
	.p2align 3
.L75:
	movl	%ebx, %eax
	leaq	(%rax,%rax,2), %r9
	salq	$4, %r9
	leaq	(%rcx,%r9), %rax
	movq	%r9, -248(%rbp)
	movq	32(%rax), %rdx
	cmpq	%rsi, %rdx
	jbe	.L73
	cmpq	%r12, %rdx
	movl	%ebx, %edi
	ja	.L72
	movl	-252(%rbp), %r8d
	movq	%r15, %rcx
	call	insert.part.2
	testl	%eax, %eax
	movq	-248(%rbp), %r9
	js	.L76
	movq	8+prof_info(%rip), %rcx
	leaq	(%rcx,%r9), %rax
.L73:
	addl	$1, %ebx
	cmpl	0(%r13), %ebx
	movq	40(%rax), %rsi
	jb	.L75
	movl	%ebx, %edi
.L72:
	cmpq	%rsi, %r12
	ja	.L94
.L68:
	addq	$8, %r14
	cmpq	%r14, -264(%rbp)
	jne	.L77
	movl	prof_info(%rip), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L59
.L95:
	movq	8+prof_info(%rip), %rax
	testb	$1, -256(%rbp)
	leaq	__profil_counter_uint(%rip), %rdx
	leaq	-208(%rbp), %rsi
	movl	$27, %edi
	movl	$268435460, -72(%rbp)
	movq	$-1, -200(%rbp)
	movq	%rax, 16+prof_info(%rip)
	leaq	__profil_counter_ushort(%rip), %rax
	cmovne	%rdx, %rax
	leaq	64+prof_info(%rip), %rdx
	movq	%rax, -208(%rbp)
	call	__GI___sigaction
	testl	%eax, %eax
	js	.L91
	movq	$0, -224(%rbp)
	movq	$1, -216(%rbp)
	leaq	-240(%rbp), %rsi
	movdqa	-224(%rbp), %xmm0
	leaq	32+prof_info(%rip), %rdx
	movl	$2, %edi
	movaps	%xmm0, -240(%rbp)
	call	__setitimer
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movl	-252(%rbp), %r8d
	movq	%r15, %rcx
	movq	%r12, %rdx
	call	insert.part.2
	testl	%eax, %eax
	jns	.L68
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8+prof_info(%rip), %rdi
	call	free@PLT
	movl	$0, prof_info(%rip)
	movq	$0, 8+prof_info(%rip)
.L91:
	movl	$-1, %eax
.L59:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	32+prof_info(%rip), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__setitimer
	testl	%eax, %eax
	js	.L91
	leaq	64+prof_info(%rip), %rsi
	xorl	%edx, %edx
	movl	$27, %edi
	call	__GI___sigaction
	testl	%eax, %eax
	js	.L91
	movq	8+prof_info(%rip), %rdi
	call	free@PLT
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L93:
	xorl	%edi, %edi
	jmp	.L72
.L65:
	leaq	pcmp(%rip), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_qsort
	movl	prof_info(%rip), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L95
	jmp	.L59
.L70:
	leaq	__PRETTY_FUNCTION__.5743(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$102, %edx
	call	__GI___assert_fail
	.size	__sprofil, .-__sprofil
	.weak	sprofil
	.set	sprofil,__sprofil
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.5743, @object
	.size	__PRETTY_FUNCTION__.5743, 12
__PRETTY_FUNCTION__.5743:
	.string	"index_to_pc"
	.local	prof_info
	.comm	prof_info,216,32
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	default_overflow_region, @object
	.size	default_overflow_region, 48
default_overflow_region:
	.quad	0
	.quad	1
	.long	2
	.zero	4
	.quad	overflow_counter
	.quad	0
	.quad	-1
	.local	overflow_counter
	.comm	overflow_counter,4,4
	.hidden	__setitimer
