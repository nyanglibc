#!/bin/sh
set -e
################################################################################
#### CONFIGURATION
conf_prefix=/nyan/glibc/nyan/1
# does not account for the terminating 0
conf_prefix_bytes_n=$(printf "$conf_prefix" | wc -c)
as="/nyan/toolchains/current/bin/as"
ld="/nyan/toolchains/current/bin/ld"
ar="/nyan/toolchains/current/bin/ar"
cpp="/nyan/toolchains/current/bin/x86_64-nyan-linux-gnu-gcc -E"
################################################################################

src_dir=$(readlink -f $(dirname $0))
printf "SRC_DIR=$src_dir\n"
build_dir=$(readlink -f .)
printf "BUILD_DIR=$build_dir\n"

# order matters

. $src_dir/make/csu.sh
. $src_dir/make/iconv.sh
. $src_dir/make/locale.sh
. $src_dir/make/assert.sh
. $src_dir/make/ctype.sh
. $src_dir/make/intl.sh
. $src_dir/make/catgets.sh
. $src_dir/make/math.sh
. $src_dir/make/setjmp.sh
. $src_dir/make/signal.sh
. $src_dir/make/stdlib.sh
. $src_dir/make/stdio-common.sh
. $src_dir/make/libio.sh
. $src_dir/make/dlfcn.sh
. $src_dir/make/nptl.sh
. $src_dir/make/malloc.sh
. $src_dir/make/string.sh
. $src_dir/make/wcsmbs.sh
. $src_dir/make/time.sh
. $src_dir/make/dirent.sh
. $src_dir/make/grp.sh
. $src_dir/make/pwd.sh
. $src_dir/make/posix.sh
. $src_dir/make/io.sh
. $src_dir/make/termios.sh
. $src_dir/make/resource.sh
. $src_dir/make/misc.sh
. $src_dir/make/socket.sh
. $src_dir/make/sysvipc.sh
. $src_dir/make/gmon.sh
. $src_dir/make/wctype.sh
. $src_dir/make/shadow.sh
. $src_dir/make/gshadow.sh
. $src_dir/make/argp.sh
. $src_dir/make/debug.sh
. $src_dir/make/inet.sh
. $src_dir/make/resolv.sh
. $src_dir/make/nss.sh
# sunrpc is dead, should remove it
. $src_dir/make/sunrpc.sh
. $src_dir/make/login.sh
. $src_dir/make/elf.sh
printf 'CREATING LIBC PIC/SHARED ARCHIVE\n'
cd $build_dir
$ar cruv $build_dir/libc_pic.a $(cat csu/stamp.os iconv/stamp.os locale/stamp.os assert/stamp.os ctype/stamp.os intl/stamp.os catgets/stamp.os math/stamp.os setjmp/stamp.os signal/stamp.os stdlib/stamp.os stdio-common/stamp.os libio/stamp.os dlfcn/stamp.os nptl/stamp.os malloc/stamp.os string/stamp.os wcsmbs/stamp.os time/stamp.os dirent/stamp.os grp/stamp.os pwd/stamp.os posix/stamp.os io/stamp.os termios/stamp.os resource/stamp.os misc/stamp.os socket/stamp.os sysvipc/stamp.os gmon/stamp.os wctype/stamp.os shadow/stamp.os gshadow/stamp.os argp/stamp.os debug/stamp.os inet/stamp.os resolv/stamp.os nss/stamp.os sunrpc/stamp.os login/stamp.os elf/stamp.os)
#-------------------------------------------------------------------------------
. $src_dir/make/rtld/csu.sh
. $src_dir/make/rtld/dirent.sh
. $src_dir/make/rtld/elf.sh
. $src_dir/make/rtld/gmon.sh
. $src_dir/make/rtld/io.sh
. $src_dir/make/rtld/malloc.sh
. $src_dir/make/rtld/misc.sh
. $src_dir/make/rtld/nptl.sh
. $src_dir/make/rtld/posix.sh
. $src_dir/make/rtld/setjmp.sh
. $src_dir/make/rtld/signal.sh
. $src_dir/make/rtld/stdlib.sh
. $src_dir/make/rtld/string.sh
. $src_dir/make/rtld/time.sh
#-------------------------------------------------------------------------------
printf 'CREATING RTLD-LIBC.A\n'
$ar cqv $build_dir/elf/rtld-libc.a $build_dir/csu/rtld-check_fds.os $build_dir/csu/rtld-errno.os $build_dir/dirent/rtld-closedir.os $build_dir/dirent/rtld-rewinddir.os $build_dir/dirent/rtld-readdir64.os $build_dir/dirent/rtld-fdopendir.os $build_dir/dirent/rtld-getdents64.os $build_dir/elf/rtld-dl-addr-obj.os $build_dir/gmon/rtld-profil.os $build_dir/gmon/rtld-prof-freq.os $build_dir/io/rtld-stat64.os $build_dir/io/rtld-fstat64.os $build_dir/io/rtld-lstat64.os $build_dir/io/rtld-fstatat64.os $build_dir/io/rtld-openat64.os $build_dir/io/rtld-lseek64.os $build_dir/io/rtld-access.os $build_dir/io/rtld-close_nocancel.os $build_dir/io/rtld-fcntl_nocancel.os $build_dir/io/rtld-open64_nocancel.os $build_dir/io/rtld-read_nocancel.os $build_dir/io/rtld-pread64_nocancel.os $build_dir/io/rtld-write_nocancel.os $build_dir/malloc/rtld-scratch_buffer_set_array_size.os $build_dir/misc/rtld-mmap64.os $build_dir/misc/rtld-munmap.os $build_dir/misc/rtld-mprotect.os $build_dir/nptl/rtld-libc-lowlevellock.os $build_dir/nptl/rtld-libc-cancellation.os $build_dir/nptl/rtld-forward.os $build_dir/posix/rtld-uname.os $build_dir/posix/rtld-_exit.os $build_dir/posix/rtld-getpid.os $build_dir/posix/rtld-environ.os $build_dir/setjmp/rtld-setjmp.os $build_dir/setjmp/rtld-__longjmp.os $build_dir/signal/rtld-sigaction.os $build_dir/stdlib/rtld-exit.os $build_dir/stdlib/rtld-cxa_atexit.os $build_dir/stdlib/rtld-cxa_thread_atexit_impl.os $build_dir/string/rtld-strchr.os $build_dir/string/rtld-strcmp.os $build_dir/string/rtld-strcspn.os $build_dir/string/rtld-strdup.os $build_dir/string/rtld-strlen.os $build_dir/string/rtld-strnlen.os $build_dir/string/rtld-strncmp.os $build_dir/string/rtld-memchr.os $build_dir/string/rtld-memcmp.os $build_dir/string/rtld-memmove.os $build_dir/string/rtld-memset.os $build_dir/string/rtld-stpcpy.os $build_dir/string/rtld-rawmemchr.os $build_dir/string/rtld-cacheinfo.os $build_dir/time/rtld-setitimer.os
printf 'LINKING LIBRTLD.OS\n'
#XXX: we may need to extract some code from libgcc
$ld -nostdlib -r -o $build_dir/elf/librtld.os '-(' $build_dir/elf/dl-allobjs.os $build_dir/elf/rtld-libc.a '-)' -Map $build_dir/elf/librtld.os.map
printf 'CREATING LD.SO/ld-linux-x86-64.so.2\n'
$ld -s -nostdlib -shared -o $build_dir/elf/ld-linux-x86-64.so.2 \
	-z combreloc --hash-style=both -z defs -z relro \
	$build_dir/elf/librtld.os \
	--version-script=$src_dir/ld.versions.map \
	-soname=ld-linux-x86-64.so.2 \
	-defsym=_begin=0
#-------------------------------------------------------------------------------
. $src_dir/make/elf-extra.sh
#-------------------------------------------------------------------------------
$ld -nostdlib -r -o $build_dir/libc_pic.os \
	-d --whole-archive $build_dir/libc_pic.a
#-------------------------------------------------------------------------------
# the bits we need from libgcc (which has bits from glibc...)
. $src_dir/make/libgcc.sh
#-------------------------------------------------------------------------------
printf 'CREATING LIBC.SO\n'
$ld \
	-s \
	-shared -O1 -z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libc.versions.map \
	-soname=libc.so.6 \
	-z combreloc -z relro --hash-style=both \
	-nostdlib -e __libc_main \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-L$build_dir/libgcc \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/libc.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	$build_dir/libc_pic.os \
	$build_dir/elf/interp.os \
	$build_dir/elf/ld-linux-x86-64.so.2 \
	-lgcc \
	$build_dir/elf/sofini.os
#-------------------------------------------------------------------------------
cd $build_dir
$ar cruv libc_nonshared.a $(cat csu/stamp.oS iconv/stamp.oS locale/stamp.oS localedata/stamp.oS iconvdata/stamp.oS assert/stamp.oS ctype/stamp.oS intl/stamp.oS catgets/stamp.oS math/stamp.oS setjmp/stamp.oS signal/stamp.oS stdlib/stamp.oS stdio-common/stamp.oS libio/stamp.oS dlfcn/stamp.oS nptl/stamp.oS malloc/stamp.oS string/stamp.oS wcsmbs/stamp.oS timezone/stamp.oS time/stamp.oS dirent/stamp.oS grp/stamp.oS pwd/stamp.oS posix/stamp.oS io/stamp.oS termios/stamp.oS resource/stamp.oS misc/stamp.oS socket/stamp.oS sysvipc/stamp.oS gmon/stamp.oS gnulib/stamp.oS wctype/stamp.oS manual/stamp.oS shadow/stamp.oS gshadow/stamp.oS po/stamp.oS argp/stamp.oS rt/stamp.oS conform/stamp.oS debug/stamp.oS mathvec/stamp.oS support/stamp.oS crypt/stamp.oS nptl_db/stamp.oS inet/stamp.oS resolv/stamp.oS nss/stamp.oS hesiod/stamp.oS sunrpc/stamp.oS nis/stamp.oS nscd/stamp.oS login/stamp.oS elf/stamp.oS stamp.oS)
#-------------------------------------------------------------------------------
$ld -s -shared --dynamic-linker=/lib64/ld-linux-x86-64.so.2 -z defs \
	-L$build_dir/csu \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/elf/sotruss-lib.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/elf/sotruss-lib.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
$ar cruv libc.a $(cat csu/stamp.o iconv/stamp.o locale/stamp.o localedata/stamp.o iconvdata/stamp.o assert/stamp.o ctype/stamp.o intl/stamp.o catgets/stamp.o math/stamp.o setjmp/stamp.o signal/stamp.o stdlib/stamp.o stdio-common/stamp.o libio/stamp.o dlfcn/stamp.o nptl/stamp.o malloc/stamp.o string/stamp.o wcsmbs/stamp.o timezone/stamp.o time/stamp.o dirent/stamp.o grp/stamp.o pwd/stamp.o posix/stamp.o io/stamp.o termios/stamp.o resource/stamp.o misc/stamp.o socket/stamp.o sysvipc/stamp.o gmon/stamp.o gnulib/stamp.o wctype/stamp.o manual/stamp.o shadow/stamp.o gshadow/stamp.o po/stamp.o argp/stamp.o rt/stamp.o conform/stamp.o debug/stamp.o mathvec/stamp.o support/stamp.o crypt/stamp.o nptl_db/stamp.o inet/stamp.o resolv/stamp.o nss/stamp.o hesiod/stamp.o sunrpc/stamp.o nis/stamp.o nscd/stamp.o login/stamp.o elf/stamp.o stamp.o)
#-------------------------------------------------------------------------------
. $src_dir/make/iconv-others.sh
. $src_dir/make/locale-others.sh
#-------------------------------------------------------------------------------
. $src_dir/make/iconvdata.sh
cp -f $src_dir/gconv-modules $build_dir/gconv-modules
#-------------------------------------------------------------------------------
. $src_dir/make/libgcc-libm.sh
. $src_dir/make/math-libm.sh
#-------------------------------------------------------------------------------
. $src_dir/make/dlfcn-libdl.sh
#-------------------------------------------------------------------------------
# crtbeginS.o crtendS.o
. $src_dir/make/libgcc-libpthread.sh
. $src_dir/make/libpthread.sh
. $src_dir/make/libmemusage.sh
. $src_dir/make/rt.sh
. $src_dir/make/libSegFault.sh
. $src_dir/make/libpcprofile.sh
. $src_dir/make/mathvec.sh
. $src_dir/make/crypt.sh
. $src_dir/make/nptl_db.sh
. $src_dir/make/resolv-libresolv.sh
. $src_dir/make/resolv-libnss_dns.sh
. $src_dir/make/resolv-libanl.sh
. $src_dir/make/nss-libnss_files.sh
. $src_dir/make/nss-libnss_db.sh
. $src_dir/make/nss-libnss_compat.sh
. $src_dir/make/hesiod.sh
# nis/libnsl (NIS/Network Information System) is dead, skipping
# libutils from login may be useless in the end
. $src_dir/make/login-libutil.sh
#-------------------------------------------------------------------------------
mkdir -p $build_dir/include
cp -rf $src_dir/include/* $build_dir/include
#-------------------------------------------------------------------------------
mkdir -p $build_dir
sed -e "s@CONF_PREFIX@$conf_prefix@g" $src_dir/libc.so.in >$build_dir/libc.so.ld
