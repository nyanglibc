	.text
	.p2align 4,,15
	.globl	sgetsgent
	.type	sgetsgent, @function
sgetsgent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
#APP
# 41 "sgetsgent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer.7634(%rip), %rdx
	movq	buffer_size.7635(%rip), %rbx
	testq	%rdx, %rdx
	je	.L22
.L5:
	leaq	8(%rsp), %r14
	movq	__libc_errno@gottpoff(%rip), %rbp
	leaq	resbuf.7636(%rip), %r13
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer.7634(%rip)
.L15:
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__sgetsgent_r
	testl	%eax, %eax
	je	.L23
	movl	%fs:0(%rbp), %ebx
	movq	buffer.7634(%rip), %r15
	cmpl	$34, %ebx
	jne	.L10
	movq	buffer_size.7635(%rip), %rax
	movq	%r15, %rdi
	leaq	1024(%rax), %rbx
	movq	%rbx, %rsi
	movq	%rbx, buffer_size.7635(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L7
	movl	%fs:0(%rbp), %ebx
	movq	%r15, %rdi
	call	free@PLT
	movq	$0, buffer.7634(%rip)
	movl	%ebx, %fs:0(%rbp)
.L16:
	movq	$0, 8(%rsp)
.L12:
#APP
# 73 "sgetsgent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L13
	subl	$1, lock(%rip)
.L14:
	movl	%ebx, %fs:0(%rbp)
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	buffer.7634(%rip), %r15
	movl	%fs:0(%rbp), %ebx
.L10:
	testq	%r15, %r15
	jne	.L12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1024, %edi
	movq	$1024, buffer_size.7635(%rip)
	movl	$1024, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, buffer.7634(%rip)
	jne	.L5
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%fs:0(%rbp), %ebx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
#APP
# 73 "sgetsgent.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L14
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 73 "sgetsgent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	sgetsgent, .-sgetsgent
	.local	resbuf.7636
	.comm	resbuf.7636,32,32
	.local	buffer_size.7635
	.comm	buffer_size.7635,8,8
	.local	buffer.7634
	.comm	buffer.7634,8,8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__sgetsgent_r
