	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	","
.LC2:
	.string	"%s:%s:"
.LC3:
	.string	"%s%s"
	.text
	.p2align 4,,15
	.globl	putsgent
	.type	putsgent, @function
putsgent:
.LFB64:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L4
	movq	%rdi, %rbp
	movq	%rax, %rdi
	movq	%rsi, %rbx
	call	__nss_valid_field
	testb	%al, %al
	je	.L4
	movq	8(%rbp), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L4
	movq	16(%rbp), %rdi
	call	__nss_valid_list_field
	testb	%al, %al
	je	.L4
	movq	24(%rbp), %rdi
	call	__nss_valid_list_field
	testb	%al, %al
	movl	%eax, %r15d
	je	.L4
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L5
	movq	136(%rbx), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L6
#APP
# 43 "putsgent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L6:
	addl	$1, 4(%rdi)
.L5:
	movq	8(%rbp), %rcx
	movq	0(%rbp), %rdx
	leaq	.LC0(%rip), %r12
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	leaq	.LC1(%rip), %r14
	testq	%rcx, %rcx
	cmove	%r12, %rcx
	xorl	%eax, %eax
	call	fprintf
	movq	16(%rbp), %r13
	shrl	$31, %eax
	movl	%r15d, %edx
	movl	%eax, 12(%rsp)
	movl	%eax, 8(%rsp)
	testq	%r13, %r13
	jne	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$8, %r13
	leaq	.LC3(%rip), %rsi
	testb	%dl, %dl
	movq	%r12, %rdx
	movq	%rbx, %rdi
	cmove	%r14, %rdx
	xorl	%eax, %eax
	call	fprintf
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L48
.L10:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	jne	.L13
.L11:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L49
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L16:
	movq	24(%rbp), %rbp
	leaq	.LC1(%rip), %r14
	leaq	.LC0(%rip), %r13
	leaq	.LC3(%rip), %r12
	testq	%rbp, %rbp
	jne	.L17
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rbp
	movq	%r13, %rdx
	testb	%r15b, %r15b
	cmove	%r14, %rdx
	movq	%r12, %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	fprintf
	testl	%eax, %eax
	js	.L50
.L17:
	movq	0(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L20
.L18:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L51
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$10, (%rax)
.L23:
	testl	$32768, (%rbx)
	jne	.L25
.L24:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L25
	movq	$0, 8(%rdi)
#APP
# 78 "putsgent.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L26
	subl	$1, (%rdi)
.L25:
	movl	8(%rsp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	negl	%eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	addl	$1, 8(%rsp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	movl	12(%rsp), %eax
	addl	$1, %eax
	movl	%eax, 8(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L23
	testl	$32768, (%rbx)
	jne	.L1
	addl	$1, 8(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	sete	%al
	movzbl	%al, %eax
	addl	%eax, 8(%rsp)
	jmp	.L16
.L26:
#APP
# 78 "putsgent.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L25
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 78 "putsgent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L8
	call	__lll_lock_wait_private
	jmp	.L8
.LFE64:
	.size	putsgent, .-putsgent
	.hidden	__lll_lock_wait_private
	.hidden	__overflow
	.hidden	fprintf
	.hidden	__nss_valid_list_field
	.hidden	__nss_valid_field
