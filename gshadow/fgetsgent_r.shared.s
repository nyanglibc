	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fgetsgent_r
	.hidden	__fgetsgent_r
	.type	__fgetsgent_r, @function
__fgetsgent_r:
.LFB73:
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	leaq	__GI__nss_files_parse_sgent(%rip), %r8
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__nss_fgetent_r
	testl	%eax, %eax
	movl	$0, %edx
	cmovne	%rdx, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE73:
	.size	__fgetsgent_r, .-__fgetsgent_r
	.weak	fgetsgent_r
	.set	fgetsgent_r,__fgetsgent_r
	.hidden	__nss_fgetent_r
