	.text
	.p2align 4,,15
	.globl	_nss_files_parse_sgent
	.hidden	_nss_files_parse_sgent
	.type	_nss_files_parse_sgent, @function
_nss_files_parse_sgent:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	leaq	(%rdx,%rcx), %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%r8, %rbp
	subq	$8, %rsp
	cmpq	%rdi, %r13
	jbe	.L43
	cmpq	%rdi, %rdx
	jbe	.L114
.L43:
	movq	%r14, %r15
.L2:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L3
	movb	$0, (%rax)
.L3:
	movq	%rbx, (%r12)
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L4
	testb	%al, %al
	jne	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L115:
	cmpb	$58, %al
	je	.L4
.L5:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L115
.L4:
	testb	%al, %al
	je	.L7
	movb	$0, (%rbx)
	addq	$1, %rbx
.L7:
	cmpb	$0, (%rbx)
	jne	.L8
	movq	(%r12), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L116
.L8:
	movq	%rbx, 8(%r12)
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L10
	testb	%al, %al
	jne	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L117:
	cmpb	$58, %al
	je	.L10
.L11:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L117
.L10:
	testb	%al, %al
	jne	.L118
.L13:
	leaq	7(%r15), %rcx
	movabsq	$288247968337756161, %rdx
	andq	$-8, %rcx
	movq	%rcx, %r8
	leaq	16(%r8), %rax
	cmpq	%rax, %r13
	jb	.L32
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L17
	cmpb	$58, %al
	je	.L18
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movq	%fs:(%rsi), %rdi
	movsbq	%al, %rsi
	testb	$32, 1(%rdi,%rsi,2)
	je	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$1, %rbx
	movsbq	(%rbx), %rsi
	testb	$32, 1(%rdi,%rsi,2)
	movq	%rsi, %rax
	jne	.L19
.L20:
	cmpb	$58, %al
	movq	%rbx, %rsi
	ja	.L24
	btq	%rax, %rdx
	movq	%rbx, %rdi
	jnc	.L24
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rdi, %rsi
.L24:
	movzbl	1(%rsi), %eax
	leaq	1(%rsi), %rdi
	cmpb	$58, %al
	ja	.L25
	btq	%rax, %rdx
	jnc	.L25
	cmpq	%rbx, %rdi
	jbe	.L22
	movq	%rbx, (%r8)
	movzbl	1(%rsi), %eax
	addq	$8, %r8
.L22:
	testb	%al, %al
	je	.L23
	cmpb	$58, %al
	leaq	1(%rdi), %rbx
	movb	$0, (%rdi)
	je	.L17
	movq	%rbx, %rdi
.L23:
	leaq	16(%r8), %rax
	movq	%rdi, %rbx
	cmpq	%rax, %r13
	jnb	.L15
.L32:
	movl	$34, 0(%rbp)
	movl	$-1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L18:
	addq	$1, %rbx
.L17:
	testq	%rcx, %rcx
	movq	$0, (%r8)
	je	.L41
	cmpq	$0, (%rcx)
	movq	%rcx, 16(%r12)
	je	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$8, %rcx
	cmpq	$0, (%rcx)
	jne	.L29
.L28:
	leaq	8(%rcx), %r15
	testq	%r15, %r15
	je	.L119
.L30:
	addq	$7, %r15
	andq	$-8, %r15
	leaq	16(%r15), %rdi
	movq	%r15, %rsi
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%rdi, %r13
	jb	.L32
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L33
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rcx
	movsbq	%al, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L35
	cmpb	$44, %dl
	je	.L36
	testb	%dl, %dl
	je	.L36
.L112:
	movq	%rbx, %rcx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L120:
	testb	%al, %al
	je	.L39
.L37:
	movq	%rbx, %rdx
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	cmpb	$44, %al
	jne	.L120
.L39:
	cmpq	%rcx, %rbx
	jbe	.L36
	movq	%rcx, (%rsi)
	movzbl	1(%rdx), %eax
	addq	$8, %rsi
	leaq	16(%rsi), %rdi
.L36:
	testb	%al, %al
	je	.L31
.L42:
	movb	$0, (%rbx)
	addq	$1, %rbx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	cmpb	$44, %al
	jne	.L112
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L118:
	movb	$0, (%rbx)
	addq	$1, %rbx
	jmp	.L13
.L116:
	testq	%r15, %r15
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	jne	.L30
.L119:
	cmpq	%rbx, %r13
	jbe	.L46
	cmpq	%rbx, %r14
	jbe	.L121
.L46:
	movq	%r14, %r15
	jmp	.L30
.L114:
	call	strlen@PLT
	leaq	1(%rbx,%rax), %r15
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L33:
	testq	%r15, %r15
	movq	$0, (%rsi)
	je	.L41
	movq	%r15, 24(%r12)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L121:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %r15
	jmp	.L30
.L41:
	movl	$-1, %eax
	jmp	.L1
	.size	_nss_files_parse_sgent, .-_nss_files_parse_sgent
	.p2align 4,,15
	.globl	__sgetsgent_r
	.hidden	__sgetsgent_r
	.type	__sgetsgent_r, @function
__sgetsgent_r:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r9
	pushq	%rbx
	movq	%r8, %r12
	movq	%rsi, %rbx
	subq	$16, %rsp
	cmpq	%rdx, %rdi
	jb	.L123
	leaq	(%rdx,%rcx), %rax
	cmpq	%rax, %rdi
	jnb	.L123
.L124:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %r8
	addq	%fs:0, %r8
	call	_nss_files_parse_sgent
	testl	%eax, %eax
	jle	.L131
	xorl	%eax, %eax
	testq	%rbx, %rbx
	movq	%rbx, (%r12)
	je	.L127
.L122:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	-1(%r9,%rcx), %rbp
	movq	%rcx, %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	movq	%rcx, 8(%rsp)
	movb	$0, 0(%rbp)
	call	strncpy
	cmpb	$0, 0(%rbp)
	movq	%rax, %r9
	movq	%rax, %rdi
	movq	8(%rsp), %rcx
	movl	$34, %eax
	je	.L124
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	movq	$0, (%r12)
.L127:
	movl	%fs:0(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__sgetsgent_r, .-__sgetsgent_r
	.weak	sgetsgent_r
	.set	sgetsgent_r,__sgetsgent_r
	.hidden	strncpy
