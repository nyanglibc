	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	fgetsgent
	.type	fgetsgent, @function
fgetsgent:
.LFB64:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	leaq	32(%rsp), %rbp
	movq	%rbp, %rsi
	call	_IO_new_fgetpos@PLT
	testl	%eax, %eax
	jne	.L16
#APP
# 47 "fgetsgent.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L3
	movl	%edx, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L4:
	movq	buffer(%rip), %rdx
	testq	%rdx, %rdx
	je	.L19
.L5:
	movq	__libc_errno@gottpoff(%rip), %r13
	leaq	24(%rsp), %r12
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, buffer(%rip)
	call	_IO_new_fsetpos@PLT
	testl	%eax, %eax
	jne	.L8
	movq	buffer(%rip), %rdx
.L9:
	testq	%rdx, %rdx
	je	.L13
	movq	buffer_size.8524(%rip), %rcx
	leaq	resbuf.8525(%rip), %rsi
	movq	%r12, %r8
	movq	%rbx, %rdi
	call	__fgetsgent_r
	cmpl	$34, %eax
	jne	.L20
	movq	buffer_size.8524(%rip), %rax
	movq	buffer(%rip), %r14
	leaq	1024(%rax), %rsi
	movq	%r14, %rdi
	movq	%rsi, buffer_size.8524(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L7
	movl	%fs:0(%r13), %r15d
	movq	%r14, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	movl	%r15d, %fs:0(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movq	$0, buffer(%rip)
.L13:
	movq	$0, 24(%rsp)
.L11:
	movl	%fs:0(%r13), %r8d
#APP
# 83 "fgetsgent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	subl	$1, lock(%rip)
.L15:
	movq	24(%rsp), %rax
	movl	%r8d, %fs:0(%r13)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	$0, buffer(%rip)
	jne	.L11
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1024, %edi
	movq	$1024, buffer_size.8524(%rip)
	call	malloc@PLT
	movq	%rax, %rdx
	movq	%rax, buffer(%rip)
	jmp	.L5
.L14:
	xorl	%eax, %eax
#APP
# 83 "fgetsgent.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L15
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 83 "fgetsgent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L15
.L3:
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L4
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
.L16:
	xorl	%eax, %eax
	jmp	.L1
.LFE64:
	.size	fgetsgent, .-fgetsgent
	.local	resbuf.8525
	.comm	resbuf.8525,32,32
	.local	buffer_size.8524
	.comm	buffer_size.8524,8,8
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__fgetsgent_r
