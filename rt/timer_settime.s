	.text
	.p2align 4,,15
	.globl	__timer_settime_new
	.type	__timer_settime_new, @function
__timer_settime_new:
	testq	%rdi, %rdi
	movq	%rdi, %rax
	movq	%rcx, %r10
	jns	.L3
	addq	%rax, %rax
	movl	(%rax), %edi
.L3:
	movl	$223, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/x86_64/timer_settime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__timer_settime_new, .-__timer_settime_new
	.weak	timer_settime
	.set	timer_settime,__timer_settime_new
