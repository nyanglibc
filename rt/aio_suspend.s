	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/pthread/aio_suspend.c"
	.align 8
.LC1:
	.string	"status == 0 || status == EAGAIN"
	.text
	.p2align 4,,15
	.type	do_aio_misc_wait, @function
do_aio_misc_wait:
.LFB89:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$8, %rsp
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	jne	.L22
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rdi, %r12
	leaq	__aio_requests_mutex(%rip), %rdi
	movq	%rsi, %r13
	call	pthread_mutex_unlock@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	movl	(%r12), %ebx
	testl	%ebx, %ebx
	je	.L23
.L4:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$1, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	__futex_abstimed_wait_cancelable64@PLT
	cmpl	$11, %eax
	movl	%eax, %ebp
	je	.L24
	cmpl	$4, %eax
	je	.L5
	cmpl	$110, %eax
	jne	.L25
	movl	$11, %ebp
.L5:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%ebp, %ebp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$75, %eax
	je	.L5
	testl	%eax, %eax
	je	.L5
	leaq	__PRETTY_FUNCTION__.9112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$101, %edx
	call	__assert_fail@PLT
.LFE89:
	.size	do_aio_misc_wait, .-do_aio_misc_wait
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"requestlist[cnt] != NULL"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"param->requestlist[cnt] != NULL"
	.text
	.p2align 4,,15
	.globl	__aio_suspend
	.type	__aio_suspend, @function
__aio_suspend:
.LFB90:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	testl	%esi, %esi
	movl	%esi, -96(%rbp)
	movq	%rdx, -112(%rbp)
	js	.L90
	movslq	-96(%rbp), %rsi
	movq	%rdi, %rbx
	leaq	__aio_requests_mutex(%rip), %rdi
	movl	$1, -68(%rbp)
	movq	%rsi, %rax
	movq	%rsi, %r15
	movq	%rsi, -128(%rbp)
	salq	$5, %rax
	movq	%rax, -136(%rbp)
	addq	$16, %rax
	subq	%rax, %rsp
	leaq	22(,%rsi,8), %rax
	movq	%rsp, -120(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	call	pthread_mutex_lock@PLT
	testl	%r15d, %r15d
	movq	%rsp, %r14
	je	.L91
	movl	-96(%rbp), %eax
	movq	-120(%rbp), %r15
	movb	$0, -89(%rbp)
	leal	-1(%rax), %r13d
	leaq	1(%r13), %rax
	xorl	%r13d, %r13d
	movq	%rax, -88(%rbp)
	leaq	-68(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%rbx,%r13,8), %rdi
	movl	%r13d, %r12d
	testq	%rdi, %rdi
	je	.L32
	cmpl	$115, 112(%rdi)
	jne	.L88
.LEHB0:
	call	__aio_find_req@PLT
	testq	%rax, %rax
	movq	%rax, (%r14,%r13,8)
	je	.L88
	movq	48(%rax), %rsi
	movq	-104(%rbp), %rcx
	movq	$0, 8(%r15)
	movq	$0, 24(%r15)
	movb	$1, -89(%rbp)
	movq	%rsi, (%r15)
	movq	%rcx, 16(%r15)
	movq	%r15, 48(%rax)
.L32:
	leal	1(%r13), %r12d
	addq	$32, %r15
	addq	$1, %r13
	cmpq	-88(%rbp), %r13
	jne	.L36
.L88:
	cmpl	%r12d, -96(%rbp)
	movzbl	-89(%rbp), %r15d
	sete	%al
	andl	%eax, %r15d
	cmpq	$0, -112(%rbp)
	je	.L37
	leaq	-64(%rbp), %r13
	movl	$1, %edi
	movq	%r13, %rsi
	call	__clock_gettime@PLT
.LEHE0:
	movq	-112(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	addq	-56(%rbp), %rax
	addq	-64(%rbp), %rdx
	cmpq	$999999999, %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	jle	.L89
	subq	$1000000000, %rax
	addq	$1, %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -64(%rbp)
.L89:
	testb	%r15b, %r15b
	je	.L31
	movq	%r13, -112(%rbp)
.L39:
	movq	-112(%rbp), %rsi
	leaq	-68(%rbp), %rdi
.LEHB1:
	call	do_aio_misc_wait
.LEHE1:
	movl	%eax, %r13d
.L41:
	movslq	%r12d, %r8
	movq	-120(%rbp), %r10
	subl	$1, %r12d
	movq	%r8, %rsi
	movslq	%r12d, %r12
	salq	$3, %r8
	negq	%rsi
	addq	%r8, %rbx
	leaq	(%r14,%r12,8), %r9
	salq	$3, %rsi
	xorl	%eax, %eax
	subq	$32, %r10
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L42:
	subq	$8, %rax
.L47:
	cmpq	%rax, %rsi
	je	.L92
	movq	-8(%rbx,%rax), %rdx
	testq	%rdx, %rdx
	je	.L42
	cmpl	$115, 112(%rdx)
	jne	.L42
	movq	(%r9,%rax), %rdx
	testq	%rdx, %rdx
	je	.L93
	movq	48(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L42
	leaq	(%r8,%rax), %rdi
	leaq	(%r10,%rdi,4), %rdi
	cmpq	%rdi, %rcx
	jne	.L45
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rdx, %rcx
.L45:
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L42
	cmpq	%rdi, %rdx
	jne	.L58
	movq	%rcx, %rdi
.L46:
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L37:
	testb	%r15b, %r15b
	jne	.L39
.L31:
	xorl	%r13d, %r13d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L92:
	testl	%r13d, %r13d
	je	.L49
	movq	errno@gottpoff(%rip), %rax
	movl	%r13d, %fs:(%rax)
	movl	$-1, %r13d
.L49:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L26:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L94:
	leaq	48(%rdx), %rdi
	movq	%rcx, %rdx
	jmp	.L46
.L90:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r13d
	movl	$22, %fs:(%rax)
	jmp	.L26
.L91:
	xorl	%r12d, %r12d
	cmpq	$0, -112(%rbp)
	je	.L31
	leaq	-64(%rbp), %rsi
	movl	$1, %edi
	xorl	%r12d, %r12d
.LEHB2:
	call	__clock_gettime@PLT
	movq	-112(%rbp), %rax
	movq	-64(%rbp), %rdx
	addq	(%rax), %rdx
	movq	8(%rax), %rax
	addq	-56(%rbp), %rax
	movq	%rdx, -64(%rbp)
	cmpq	$999999999, %rax
	movq	%rax, -56(%rbp)
	jle	.L31
	subq	$1000000000, %rax
	addq	$1, %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -64(%rbp)
	jmp	.L31
.L93:
	leaq	__PRETTY_FUNCTION__.9131(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$207, %edx
	call	__assert_fail@PLT
.L60:
	leaq	__aio_requests_mutex(%rip), %rdi
	movq	%rax, %r12
	call	pthread_mutex_lock@PLT
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %rcx
	leaq	-32(%rax,%rcx), %rdx
.L51:
	cmpl	$0, -128(%rbp)
	jle	.L95
	movq	-128(%rbp), %rax
	movq	-8(%rbx,%rax,8), %rax
	testq	%rax, %rax
	je	.L52
	cmpl	$115, 112(%rax)
	je	.L96
.L52:
	subq	$1, -128(%rbp)
	subq	$32, %rdx
	jmp	.L51
.L95:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movq	%r12, %rdi
	call	_Unwind_Resume@PLT
.LEHE2:
.L96:
	movq	-128(%rbp), %rax
	movq	-8(%r14,%rax,8), %rax
	testq	%rax, %rax
	je	.L97
	leaq	48(%rax), %rcx
	movq	48(%rax), %rax
.L54:
	testq	%rax, %rax
	je	.L52
	cmpq	%rdx, %rax
	movq	(%rax), %rsi
	je	.L98
	movq	%rax, %rcx
	movq	%rsi, %rax
	jmp	.L54
.L97:
	leaq	__PRETTY_FUNCTION__.9095(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$73, %edx
	call	__assert_fail@PLT
.L98:
	movq	%rsi, (%rcx)
	jmp	.L52
.LFE90:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA90:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE90-.LLSDACSB90
.LLSDACSB90:
	.uleb128 .LEHB0-.LFB90
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB90
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L60-.LFB90
	.uleb128 0
	.uleb128 .LEHB2-.LFB90
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE90:
	.text
	.size	__aio_suspend, .-__aio_suspend
	.weak	aio_suspend
	.set	aio_suspend,__aio_suspend
	.weak	aio_suspend64
	.set	aio_suspend64,aio_suspend
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9112, @object
	.size	__PRETTY_FUNCTION__.9112, 17
__PRETTY_FUNCTION__.9112:
	.string	"do_aio_misc_wait"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9095, @object
	.size	__PRETTY_FUNCTION__.9095, 8
__PRETTY_FUNCTION__.9095:
	.string	"cleanup"
	.align 8
	.type	__PRETTY_FUNCTION__.9131, @object
	.size	__PRETTY_FUNCTION__.9131, 14
__PRETTY_FUNCTION__.9131:
	.string	"__aio_suspend"
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
