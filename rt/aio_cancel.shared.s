	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/pthread/aio_cancel.c"
	.align 8
.LC1:
	.string	"req->running == yes || req->running == queued"
	.text
	.p2align 4,,15
	.globl	aio_cancel
	.type	aio_cancel, @function
aio_cancel:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$3, %esi
	movl	%edi, %ebx
	subq	$8, %rsp
	call	fcntl@PLT
	testl	%eax, %eax
	js	.L33
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testq	%rbp, %rbp
	je	.L4
	cmpl	%ebx, 0(%rbp)
	jne	.L8
	cmpl	$115, 112(%rbp)
	je	.L34
.L6:
	movl	$2, %r13d
.L11:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ebx, %edi
	call	__aio_find_req_fd
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L6
	cmpl	$3, (%rax)
	jne	.L13
	movq	24(%rax), %rbp
	movq	$0, 24(%rax)
	testq	%rbp, %rbp
	je	.L14
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	__aio_remove_request
	movq	%rbp, %rbx
	movl	$1, %r13d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%ebx, %edi
	call	__aio_find_req_fd
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L8
	xorl	%edi, %edi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movq	24(%rbx), %rax
	movq	%rbx, %rdi
	testq	%rax, %rax
	je	.L8
	movq	%rax, %rbx
.L7:
	cmpq	%rbp, 40(%rbx)
	jne	.L9
	cmpl	$3, (%rbx)
	jne	.L10
.L14:
	movl	$1, %r13d
	jmp	.L11
.L8:
	leaq	__aio_requests_mutex(%rip), %rdi
	movl	$-1, %r13d
	call	pthread_mutex_unlock@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L1
.L13:
	movl	$1, %edx
	movq	%rax, %rsi
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	call	__aio_remove_request
.L12:
	movl	(%rbx), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L18
	movq	$-1, %r12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	40(%rbx), %rax
	movq	%rbx, %rdi
	movl	$125, 112(%rax)
	movq	%r12, 120(%rax)
	call	__aio_notify
	movq	24(%rbx), %rbp
	movq	%rbx, %rdi
	call	__aio_free_request
	testq	%rbp, %rbp
	je	.L11
	movl	0(%rbp), %eax
	movq	%rbp, %rbx
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L16
.L18:
	leaq	__PRETTY_FUNCTION__.9163(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$141, %edx
	call	__assert_fail@PLT
.L10:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%r13d, %r13d
	call	__aio_remove_request
	movq	$0, 24(%rbx)
	jmp	.L12
.L33:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r13d
	movl	$9, %fs:(%rax)
	jmp	.L1
	.size	aio_cancel, .-aio_cancel
	.weak	aio_cancel64
	.set	aio_cancel64,aio_cancel
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9163, @object
	.size	__PRETTY_FUNCTION__.9163, 11
__PRETTY_FUNCTION__.9163:
	.string	"aio_cancel"
	.hidden	__aio_free_request
	.hidden	__aio_notify
	.hidden	__aio_remove_request
	.hidden	__aio_find_req_fd
	.hidden	__aio_requests_mutex
