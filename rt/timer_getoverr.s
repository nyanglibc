	.text
	.p2align 4,,15
	.globl	__timer_getoverrun_new
	.type	__timer_getoverrun_new, @function
__timer_getoverrun_new:
	testq	%rdi, %rdi
	movq	%rdi, %rax
	jns	.L3
	addq	%rax, %rax
	movl	(%rax), %edi
.L3:
	movl	$225, %eax
#APP
# 35 "../sysdeps/unix/sysv/linux/timer_getoverr.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__timer_getoverrun_new, .-__timer_getoverrun_new
	.weak	timer_getoverrun
	.set	timer_getoverrun,__timer_getoverrun_new
