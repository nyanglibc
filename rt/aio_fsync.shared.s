	.text
	.p2align 4,,15
	.globl	aio_fsync
	.type	aio_fsync, @function
aio_fsync:
	movl	%edi, %eax
	andl	$-1048577, %eax
	cmpl	$4096, %eax
	jne	.L11
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	xorl	%eax, %eax
	movl	$3, %esi
	subq	$8, %rsp
	movl	0(%rbp), %edi
	call	fcntl@PLT
	cmpl	$-1, %eax
	je	.L12
	xorl	%esi, %esi
	cmpl	$1052672, %ebx
	movq	%rbp, %rdi
	sete	%sil
	addl	$3, %esi
	call	__aio_enqueue_request
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	negl	%eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rdx
	movl	$9, %fs:(%rdx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	aio_fsync, .-aio_fsync
	.weak	aio_fsync64
	.set	aio_fsync64,aio_fsync
	.hidden	__aio_enqueue_request
