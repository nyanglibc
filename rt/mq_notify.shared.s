	.text
	.p2align 4,,15
	.type	reset_once, @function
reset_once:
	movl	$0, once(%rip)
	ret
	.size	reset_once, .-reset_once
	.p2align 4,,15
	.type	helper_thread, @function
helper_thread:
	pushq	%r12
	pushq	%rbp
	leaq	notification_function(%rip), %rbp
	pushq	%rbx
	subq	$48, %rsp
	leaq	16(%rsp), %rbx
	leaq	8(%rsp), %r12
	.p2align 4,,10
	.p2align 3
.L5:
	movl	netlink_socket(%rip), %edi
	movl	$16640, %ecx
	movl	$32, %edx
	movq	%rbx, %rsi
	call	__recv@PLT
	cmpq	$31, %rax
	jle	.L5
	movzbl	47(%rsp), %eax
	cmpb	$1, %al
	je	.L10
	cmpb	$2, %al
	jne	.L5
	movq	32(%rsp), %rdi
	call	free@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rsp), %rsi
	movq	%rbx, %rcx
	movq	%rbp, %rdx
	movq	%r12, %rdi
	call	pthread_create@PLT
	testl	%eax, %eax
	jne	.L5
	leaq	notify_barrier(%rip), %rdi
	call	__pthread_barrier_wait@PLT
	jmp	.L5
	.size	helper_thread, .-helper_thread
	.p2align 4,,15
	.type	change_sigmask, @function
change_sigmask:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	%edi, %ebp
	addq	$-128, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	sigfillset@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	pthread_sigmask@PLT
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	change_sigmask, .-change_sigmask
	.p2align 4,,15
	.type	notification_function, @function
notification_function:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbx
	movq	8(%rdi), %rbp
	leaq	notify_barrier(%rip), %rdi
	call	__pthread_barrier_wait@PLT
	call	pthread_self@PLT
	movq	%rax, %rdi
	call	pthread_detach@PLT
	xorl	%esi, %esi
	movl	$1, %edi
	call	change_sigmask
	movq	%rbp, %rdi
	call	*%rbx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	notification_function, .-notification_function
	.p2align 4,,15
	.type	init_mq_netlink, @function
init_mq_netlink:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$216, %rsp
	cmpl	$-1, netlink_socket(%rip)
	je	.L16
.L20:
	leaq	notify_barrier(%rip), %rdi
	xorl	%esi, %esi
	movl	$2, %edx
	call	__pthread_barrier_init@PLT
	testl	%eax, %eax
	je	.L31
.L18:
	movl	netlink_socket(%rip), %edi
	call	__close_nocancel@PLT
	movl	$-1, netlink_socket(%rip)
.L15:
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%edx, %edx
	movl	$524291, %esi
	movl	$16, %edi
	call	__socket@PLT
	cmpl	$-1, %eax
	movl	%eax, netlink_socket(%rip)
	jne	.L20
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	16(%rsp), %rbx
	leaq	80(%rsp), %rbp
	movq	%rbx, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	pthread_attr_setdetachstate@PLT
	movq	%rbx, %rdi
	call	__pthread_get_minstack@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	pthread_attr_setstacksize@PLT
	xorl	%edi, %edi
	movq	%rbp, %rsi
	call	change_sigmask
	leaq	8(%rsp), %rdi
	leaq	helper_thread(%rip), %rdx
	movl	%eax, %r13d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	call	pthread_create@PLT
	testl	%r13d, %r13d
	movl	%eax, %r12d
	je	.L32
.L21:
	movq	%rbx, %rdi
	call	pthread_attr_destroy@PLT
	testl	%r12d, %r12d
	jne	.L18
	movl	added_atfork.9066(%rip), %eax
	testl	%eax, %eax
	je	.L33
.L23:
	movl	$1, added_atfork.9066(%rip)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	reset_once(%rip), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	pthread_atfork@PLT
	testl	%eax, %eax
	je	.L23
	movq	8(%rsp), %rdi
	call	pthread_cancel@PLT
	jmp	.L18
	.size	init_mq_netlink, .-init_mq_netlink
	.p2align 4,,15
	.globl	mq_notify
	.type	mq_notify, @function
mq_notify:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$96, %rsp
	testq	%rsi, %rsi
	je	.L35
	cmpl	$2, 12(%rsi)
	je	.L36
.L35:
	movl	$244, %eax
#APP
# 226 "../sysdeps/unix/sysv/linux/mq_notify.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L37
	movl	%eax, %ebx
.L34:
	addq	$96, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rsi, %rbp
	movl	%edi, %r12d
	leaq	init_mq_netlink(%rip), %rsi
	leaq	once(%rip), %rdi
	call	pthread_once@PLT
	movl	netlink_socket(%rip), %ebx
	cmpl	$-1, %ebx
	je	.L52
	movq	16(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsp)
	movq	0(%rbp), %rax
	movq	24(%rbp), %rbp
	movaps	%xmm0, 16(%rsp)
	movq	%rax, 8(%rsp)
	testq	%rbp, %rbp
	je	.L41
	movl	$56, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L44
	movdqu	0(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movdqu	32(%rbp), %xmm0
	movups	%xmm0, 32(%rax)
	movq	48(%rbp), %rdx
	movq	%rdx, 48(%rax)
.L41:
	movq	%rsp, %rax
	movl	$2, 44(%rsp)
	movl	%ebx, 40(%rsp)
	movq	%rax, 32(%rsp)
	leaq	32(%rsp), %rsi
	movl	%r12d, %edi
	movl	$244, %eax
#APP
# 269 "../sysdeps/unix/sysv/linux/mq_notify.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L53
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L34
	.p2align 4,,10
	.p2align 3
.L43:
	movq	16(%rsp), %rdi
	call	free@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %ebx
	movl	%eax, %fs:(%rdx)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L52:
	movq	errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L53:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %ebx
	movl	%eax, %fs:(%rdx)
	jmp	.L43
.L44:
	movl	$-1, %ebx
	jmp	.L34
	.size	mq_notify, .-mq_notify
	.local	added_atfork.9066
	.comm	added_atfork.9066,4,4
	.local	notify_barrier
	.comm	notify_barrier,32,32
	.data
	.align 4
	.type	netlink_socket, @object
	.size	netlink_socket, 4
netlink_socket:
	.long	-1
	.local	once
	.comm	once,4,4
