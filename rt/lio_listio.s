	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/wordsize-64/../../../../pthread/lio_listio.c"
	.align 8
.LC1:
	.string	"requests[cnt] == NULL || list[cnt] != NULL"
	.align 8
.LC2:
	.string	"status == 0 || status == EAGAIN"
	.text
	.p2align 4,,15
	.globl	__lio_listio_item_notify
	.type	__lio_listio_item_notify, @function
__lio_listio_item_notify:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	cmpl	$1, %edi
	movl	%edi, -148(%rbp)
	movq	%rcx, -144(%rbp)
	ja	.L64
	movslq	%edx, %rax
	movq	%rsp, -160(%rbp)
	movq	%rsi, %r15
	movq	%rax, -168(%rbp)
	leaq	22(,%rax,8), %rax
	movl	%edx, %r12d
	movl	$0, -120(%rbp)
	movl	$0, -116(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	cmpq	$0, -144(%rbp)
	movq	%rsp, %r13
	je	.L65
.L4:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testl	%r12d, %r12d
	jle	.L5
	leal	-1(%r12), %eax
	movq	%r15, %rdx
	movq	%r13, %r14
	leaq	8(%r15,%rax,8), %rbx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rdx, -136(%rbp)
	call	__aio_enqueue_request@PLT
	testq	%rax, %rax
	movq	%rax, (%r14)
	movq	-136(%rbp), %rdx
	je	.L7
	movl	-120(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -120(%rbp)
.L8:
	addq	$8, %rdx
	addq	$8, %r14
	cmpq	%rbx, %rdx
	je	.L5
.L9:
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L6
	movl	4(%rdi), %esi
	cmpl	$2, %esi
	jne	.L66
.L6:
	addq	$8, %rdx
	movq	$0, (%r14)
	addq	$8, %r14
	cmpq	%rbx, %rdx
	jne	.L9
.L5:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	je	.L67
	movq	-168(%rbp), %rbx
	movl	-148(%rbp), %eax
	salq	$5, %rbx
	testl	%eax, %eax
	jne	.L13
	addq	$16, %rbx
	movq	%rsp, %r14
	movl	$0, -120(%rbp)
	subq	%rbx, %rsp
	testl	%r12d, %r12d
	movq	%rsp, %rax
	jle	.L14
	leal	-1(%r12), %edx
	leaq	-116(%rbp), %r11
	leaq	-120(%rbp), %r10
	salq	$5, %rdx
	leaq	32(%rax,%rdx), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L17:
	movq	0(%r13,%rdx), %rcx
	testq	%rcx, %rcx
	je	.L15
	movq	(%r15,%rdx), %rsi
	testq	%rsi, %rsi
	je	.L68
	cmpl	$2, 4(%rsi)
	je	.L15
	movq	48(%rcx), %rsi
	movq	%r11, 8(%rax)
	movq	%r10, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rsi, (%rax)
	movq	%rax, 48(%rcx)
	movl	-120(%rbp), %ecx
	addl	$1, %ecx
	movl	%ecx, -120(%rbp)
.L15:
	addq	$32, %rax
	addq	$8, %rdx
	cmpq	%rax, %rdi
	jne	.L17
.L14:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	jne	.L69
.L18:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L26
	movl	$5, %edx
	cmpl	$4, %eax
	movl	$-1, -116(%rbp)
	cmovne	%edx, %eax
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
.L26:
	movq	%r14, %rsp
.L28:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L62:
	movl	-116(%rbp), %eax
	movq	-160(%rbp), %rsp
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-1, -116(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	72(%rbx), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L70
	testl	%r12d, %r12d
	movl	$0, -120(%rbp)
	jle	.L30
	leal	-1(%r12), %ecx
	leaq	72(%rax), %rdx
	leaq	8(%rax), %r11
	salq	$5, %rcx
	leaq	104(%rax,%rcx), %r10
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L33:
	movq	0(%r13,%rcx), %rsi
	testq	%rsi, %rsi
	je	.L31
	movq	(%r15,%rcx), %rdi
	testq	%rdi, %rdi
	je	.L71
	cmpl	$2, 4(%rdi)
	je	.L31
	movq	48(%rsi), %rdi
	movq	$0, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	%r11, 24(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 48(%rsi)
	movl	-120(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -120(%rbp)
.L31:
	addq	$32, %rdx
	addq	$8, %rcx
	cmpq	%r10, %rdx
	jne	.L33
.L30:
	movq	-144(%rbp), %rbx
	movl	-120(%rbp), %edx
	movdqu	(%rbx), %xmm0
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 24(%rax)
	movdqu	32(%rbx), %xmm0
	movups	%xmm0, 40(%rax)
	movdqu	48(%rbx), %xmm0
	movups	%xmm0, 56(%rax)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	cmpl	$1, -148(%rbp)
	jne	.L62
	movq	-144(%rbp), %rdi
	call	__aio_notify_only@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	-112(%rbp), %rax
	movl	$1, -100(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L70:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, -116(%rbp)
	movl	$11, %fs:(%rax)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	__aio_requests_mutex(%rip), %rdi
	movl	%esi, -136(%rbp)
	leaq	-120(%rbp), %rbx
	call	pthread_mutex_unlock@PLT
	movl	-136(%rbp), %esi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L72:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	je	.L21
.L20:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$11, %eax
	je	.L72
	cmpl	$4, %eax
	jne	.L22
	movl	$4, -116(%rbp)
.L21:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L64:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
.L22:
	cmpl	$110, %eax
	jne	.L23
	movl	$11, -116(%rbp)
	jmp	.L21
.L68:
	leaq	__PRETTY_FUNCTION__.9940(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$125, %edx
	call	__assert_fail@PLT
.L71:
	leaq	__PRETTY_FUNCTION__.9940(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$187, %edx
	call	__assert_fail@PLT
.L23:
	cmpl	$75, %eax
	jne	.L73
	movl	$75, -116(%rbp)
	jmp	.L21
.L73:
	testl	%eax, %eax
	je	.L21
	leaq	__PRETTY_FUNCTION__.9940(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$142, %edx
	call	__assert_fail@PLT
	.size	__lio_listio_item_notify, .-__lio_listio_item_notify
	.globl	__lio_listio64_item_notify
	.set	__lio_listio64_item_notify,__lio_listio_item_notify
	.weak	lio_listio64
	.set	lio_listio64,__lio_listio64_item_notify
	.weak	lio_listio
	.set	lio_listio,__lio_listio_item_notify
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9940, @object
	.size	__PRETTY_FUNCTION__.9940, 20
__PRETTY_FUNCTION__.9940:
	.string	"lio_listio_internal"
