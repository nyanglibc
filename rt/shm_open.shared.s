	.text
	.p2align 4,,15
	.globl	shm_open
	.type	shm_open, @function
shm_open:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	movl	%edx, %r15d
	subq	$40, %rsp
	call	__shm_directory@PLT
	testq	%rax, %rax
	je	.L2
	cmpb	$47, (%rbx)
	movq	%rax, %r14
	jne	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rbx
	cmpb	$47, (%rbx)
	je	.L3
.L4:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rax), %r12
	cmpq	$1, %r12
	je	.L6
	cmpq	$254, %r12
	ja	.L6
	movl	$47, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L6
	movq	-56(%rbp), %rdx
	movq	%r14, %rsi
	leaq	30(%rdx,%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	movq	%rcx, %rdi
	movq	%rcx, -72(%rbp)
	call	__mempcpy@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	leaq	-60(%rbp), %rsi
	movl	$1, %edi
	call	pthread_setcancelstate@PLT
	movq	-72(%rbp), %rcx
	movl	%r13d, %esi
	xorl	%eax, %eax
	orl	$655360, %esi
	movl	%r15d, %edx
	movq	%rcx, %rdi
	call	open@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L13
.L8:
	movl	-60(%rbp), %edi
	xorl	%esi, %esi
	call	pthread_setcancelstate@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$21, %fs:(%rax)
	jne	.L8
	movl	$22, %fs:(%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L2:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$38, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L1
	.size	shm_open, .-shm_open
