	.text
	.p2align 4,,15
	.globl	shm_unlink
	.type	shm_unlink, @function
shm_unlink:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	-40(%rbp), %rdi
	subq	$16, %rsp
	call	__shm_directory@PLT
	testq	%rax, %rax
	je	.L2
	cmpb	$47, (%rbx)
	movq	%rax, %r14
	jne	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rbx
	cmpb	$47, (%rbx)
	je	.L3
.L4:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rax), %r12
	cmpq	$1, %r12
	je	.L6
	cmpq	$254, %r12
	ja	.L6
	movl	$47, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L6
	movq	-40(%rbp), %rdx
	movq	%r14, %rsi
	leaq	30(%rdx,%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	movq	%r13, %rdi
	call	__mempcpy@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r13, %rdi
	call	unlink@PLT
	testl	%eax, %eax
	js	.L12
.L1:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rdx
	cmpl	$1, %fs:(%rdx)
	jne	.L1
	movl	$13, %fs:(%rdx)
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	leaq	-32(%rbp), %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.size	shm_unlink, .-shm_unlink
