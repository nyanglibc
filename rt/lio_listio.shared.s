	.text
#APP
	.symver __lio_listio_21,lio_listio@GLIBC_2.2.5
	.symver __lio_listio_item_notify,lio_listio@@GLIBC_2.4
	.symver __lio_listio64_21,lio_listio64@GLIBC_2.2.5
	.symver __lio_listio64_item_notify,lio_listio64@@GLIBC_2.4
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/wordsize-64/../../../../pthread/lio_listio.c"
	.align 8
.LC1:
	.string	"requests[cnt] == NULL || list[cnt] != NULL"
	.align 8
.LC2:
	.string	"status == 0 || status == EAGAIN"
#NO_APP
	.text
	.p2align 4,,15
	.type	lio_listio_internal, @function
lio_listio_internal:
	pushq	%rbp
	movslq	%edx, %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbx
	subq	$120, %rsp
	movl	%eax, -140(%rbp)
	movq	%rax, -152(%rbp)
	leaq	22(,%rax,8), %rax
	movl	%edi, -136(%rbp)
	movq	%rcx, -160(%rbp)
	movl	$0, -120(%rbp)
	movl	$0, -116(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	testq	%rcx, %rcx
	movq	%rsp, %r15
	je	.L66
.L2:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	jle	.L3
	subl	$1, %eax
	movq	%r14, %r13
	movq	%r15, %r12
	leaq	8(%r14,%rax,8), %rbx
	movl	-136(%rbp), %eax
	andl	$128, %eax
	movl	%eax, -132(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L67:
	movl	4(%rdi), %esi
	cmpl	$2, %esi
	je	.L4
	movl	-132(%rbp), %eax
	testl	%eax, %eax
	je	.L5
	movl	$1, 44(%rdi)
.L5:
	call	__aio_enqueue_request
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L6
	movl	-120(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -120(%rbp)
.L7:
	addq	$8, %r13
	addq	$8, %r12
	cmpq	%rbx, %r13
	je	.L3
.L8:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L67
.L4:
	addq	$8, %r13
	movq	$0, (%r12)
	addq	$8, %r12
	cmpq	%rbx, %r13
	jne	.L8
.L3:
	movl	-120(%rbp), %eax
	movl	-136(%rbp), %ebx
	andl	$127, %ebx
	testl	%eax, %eax
	je	.L68
	movq	-152(%rbp), %r12
	salq	$5, %r12
	testl	%ebx, %ebx
	jne	.L12
	movl	-140(%rbp), %ecx
	addq	$16, %r12
	movq	%rsp, %rbx
	subq	%r12, %rsp
	movl	$0, -120(%rbp)
	movq	%rsp, %rax
	testl	%ecx, %ecx
	jle	.L13
	leal	-1(%rcx), %edx
	leaq	-116(%rbp), %r11
	leaq	-120(%rbp), %r10
	salq	$5, %rdx
	leaq	32(%rax,%rdx), %r9
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r15,%rdx), %rsi
	testq	%rsi, %rsi
	je	.L14
	movq	(%r14,%rdx), %rdi
	testq	%rdi, %rdi
	je	.L69
	cmpl	$2, 4(%rdi)
	je	.L14
	movq	48(%rsi), %rdi
	movq	%r11, 8(%rax)
	movq	%r10, 16(%rax)
	movq	$0, 24(%rax)
	movq	%rdi, (%rax)
	movq	%rax, 48(%rsi)
	movl	-120(%rbp), %esi
	addl	$1, %esi
	movl	%esi, -120(%rbp)
.L14:
	addq	$32, %rax
	addq	$8, %rdx
	cmpq	%rax, %r9
	jne	.L16
.L13:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	jne	.L70
.L17:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L25
	movl	$5, %edx
	cmpl	$4, %eax
	movl	$-1, -116(%rbp)
	cmovne	%edx, %eax
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
.L25:
	movq	%rbx, %rsp
.L27:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L64:
	movl	-116(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, -116(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	72(%r12), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L71
	movl	-140(%rbp), %ebx
	movl	$0, -120(%rbp)
	testl	%ebx, %ebx
	jle	.L29
	leal	-1(%rbx), %esi
	leaq	72(%rax), %rdx
	leaq	8(%rax), %r11
	salq	$5, %rsi
	leaq	104(%rax,%rsi), %r10
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%r15,%rsi), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%r14,%rsi), %r9
	testq	%r9, %r9
	je	.L72
	cmpl	$2, 4(%r9)
	je	.L30
	movq	48(%rdi), %r9
	movq	$0, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	%r11, 24(%rdx)
	movq	%r9, (%rdx)
	movq	%rdx, 48(%rdi)
	movl	-120(%rbp), %edi
	addl	$1, %edi
	movl	%edi, -120(%rbp)
.L30:
	addq	$32, %rdx
	addq	$8, %rsi
	cmpq	%r10, %rdx
	jne	.L32
.L29:
	movq	-160(%rbp), %rcx
	movl	-120(%rbp), %edx
	movdqu	(%rcx), %xmm0
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movdqu	16(%rcx), %xmm0
	movups	%xmm0, 24(%rax)
	movdqu	32(%rcx), %xmm0
	movups	%xmm0, 40(%rax)
	movdqu	48(%rcx), %xmm0
	movups	%xmm0, 56(%rax)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	cmpl	$1, %ebx
	jne	.L64
	movq	-160(%rbp), %rdi
	call	__aio_notify_only
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	-112(%rbp), %rax
	movl	$1, -100(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L71:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, -116(%rbp)
	movl	$11, %fs:(%rax)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	__aio_requests_mutex(%rip), %rdi
	movl	%esi, -132(%rbp)
	leaq	-120(%rbp), %r12
	call	pthread_mutex_unlock@PLT
	movl	-132(%rbp), %esi
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L73:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	je	.L20
.L19:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$11, %eax
	je	.L73
	cmpl	$4, %eax
	jne	.L21
	movl	$4, -116(%rbp)
.L20:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	jmp	.L17
.L21:
	cmpl	$110, %eax
	jne	.L22
	movl	$11, -116(%rbp)
	jmp	.L20
.L69:
	leaq	__PRETTY_FUNCTION__.9942(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$125, %edx
	call	__assert_fail@PLT
.L72:
	leaq	__PRETTY_FUNCTION__.9942(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$187, %edx
	call	__assert_fail@PLT
.L22:
	cmpl	$75, %eax
	jne	.L74
	movl	$75, -116(%rbp)
	jmp	.L20
.L74:
	testl	%eax, %eax
	je	.L20
	leaq	__PRETTY_FUNCTION__.9942(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$142, %edx
	call	__assert_fail@PLT
	.size	lio_listio_internal, .-lio_listio_internal
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__lio_listio_21
	.type	__lio_listio_21, @function
__lio_listio_21:
	cmpl	$1, %edi
	ja	.L79
	orb	$-128, %dil
	jmp	lio_listio_internal
	.p2align 4,,10
	.p2align 3
.L79:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__lio_listio_21, .-__lio_listio_21
	.globl	__lio_listio64_21
	.set	__lio_listio64_21,__lio_listio_21
	.text
	.p2align 4,,15
	.globl	__lio_listio_item_notify
	.type	__lio_listio_item_notify, @function
__lio_listio_item_notify:
	cmpl	$1, %edi
	ja	.L84
	jmp	lio_listio_internal
	.p2align 4,,10
	.p2align 3
.L84:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__lio_listio_item_notify, .-__lio_listio_item_notify
	.globl	__lio_listio64_item_notify
	.set	__lio_listio64_item_notify,__lio_listio_item_notify
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9942, @object
	.size	__PRETTY_FUNCTION__.9942, 20
__PRETTY_FUNCTION__.9942:
	.string	"lio_listio_internal"
	.hidden	__aio_notify_only
	.hidden	__aio_enqueue_request
	.hidden	__aio_requests_mutex
