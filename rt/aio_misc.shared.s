	.text
	.p2align 4,,15
	.type	add_request_to_runlist, @function
add_request_to_runlist:
	movq	runlist(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2
	movq	40(%rdi), %rax
	movl	104(%rax), %esi
	movq	40(%rdx), %rax
	cmpl	%esi, 104(%rax)
	jge	.L3
.L2:
	movq	%rdx, 32(%rdi)
	movq	%rdi, runlist(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	40(%rax), %rcx
	cmpl	%esi, 104(%rcx)
	jl	.L5
	movq	%rax, %rdx
.L3:
	movq	32(%rdx), %rax
	testq	%rax, %rax
	jne	.L15
.L5:
	movq	%rax, 32(%rdi)
	movq	%rdi, 32(%rdx)
	ret
	.size	add_request_to_runlist, .-add_request_to_runlist
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_res, @function
free_res:
	movq	pool_max_size(%rip), %rax
	pushq	%r12
	movq	pool(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	testq	%rax, %rax
	je	.L17
	leaq	(%r12,%rax,8), %rbp
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbp, %rbx
	jne	.L18
.L17:
	popq	%rbx
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	free@PLT
	.size	free_res, .-free_res
	.text
	.p2align 4,,15
	.globl	__aio_free_request
	.hidden	__aio_free_request
	.type	__aio_free_request, @function
__aio_free_request:
	movq	freelist(%rip), %rax
	movl	$0, (%rdi)
	movq	%rdi, freelist(%rip)
	movq	%rax, 24(%rdi)
	ret
	.size	__aio_free_request, .-__aio_free_request
	.p2align 4,,15
	.globl	__aio_find_req
	.hidden	__aio_find_req
	.type	__aio_find_req, @function
__aio_find_req:
	movq	requests(%rip), %rax
	movl	(%rdi), %esi
	testq	%rax, %rax
	jne	.L42
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L43:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L25
.L42:
	movq	40(%rax), %rdx
	movl	(%rdx), %ecx
	cmpl	%esi, %ecx
	jl	.L43
	cmpl	%ecx, %esi
	je	.L30
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L46:
	movq	40(%rax), %rdx
.L30:
	cmpq	%rdx, %rdi
	je	.L45
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L46
.L25:
	rep ret
	.p2align 4,,10
	.p2align 3
.L45:
	rep ret
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%eax, %eax
	ret
	.size	__aio_find_req, .-__aio_find_req
	.p2align 4,,15
	.globl	__aio_find_req_fd
	.hidden	__aio_find_req_fd
	.type	__aio_find_req_fd, @function
__aio_find_req_fd:
	movq	requests(%rip), %rax
	testq	%rax, %rax
	je	.L47
	movq	40(%rax), %rdx
	movl	(%rdx), %edx
	cmpl	%edx, %edi
	jg	.L50
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	movq	40(%rax), %rdx
	movl	(%rdx), %edx
	cmpl	%edi, %edx
	jge	.L49
.L50:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L59
.L47:
	rep ret
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	%edx, %edi
	movl	$0, %edx
	cmovne	%rdx, %rax
	ret
	.size	__aio_find_req_fd, .-__aio_find_req_fd
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../sysdeps/pthread/aio_misc.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"req->running == yes || req->running == queued || req->running == done"
	.text
	.p2align 4,,15
	.globl	__aio_remove_request
	.hidden	__aio_remove_request
	.type	__aio_remove_request, @function
__aio_remove_request:
	movl	(%rsi), %eax
	leal	-1(%rax), %ecx
	cmpl	$1, %ecx
	jbe	.L61
	cmpl	$4, %eax
	jne	.L90
.L61:
	testq	%rdi, %rdi
	je	.L62
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L63
	movq	24(%rsi), %rax
.L63:
	movq	%rax, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	testl	%edx, %edx
	movq	8(%rsi), %rcx
	jne	.L65
	movq	24(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L65
	testq	%rcx, %rcx
	je	.L70
	movq	%rdx, 16(%rcx)
.L71:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L72
	movq	%rdx, 8(%rax)
	movq	8(%rsi), %rcx
.L72:
	movq	%rax, 16(%rdx)
	movl	$2, (%rdx)
	movl	(%rsi), %eax
	movq	%rcx, 8(%rdx)
	cmpl	$2, %eax
	je	.L91
.L60:
	rep ret
	.p2align 4,,10
	.p2align 3
.L65:
	testq	%rcx, %rcx
	movq	16(%rsi), %rdx
	je	.L67
	movq	%rdx, 16(%rcx)
	movq	16(%rsi), %rdx
.L68:
	testq	%rdx, %rdx
	je	.L69
	movq	%rcx, 8(%rdx)
.L69:
	cmpl	$2, %eax
	jne	.L60
.L91:
	movq	runlist(%rip), %rdx
	testq	%rdx, %rdx
	je	.L60
	cmpq	%rdx, %rsi
	jne	.L73
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	%rax, %rsi
	je	.L93
	movq	%rax, %rdx
.L73:
	movq	32(%rdx), %rax
	testq	%rax, %rax
	jne	.L74
	rep ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rdx, requests(%rip)
	jmp	.L68
.L90:
	leaq	__PRETTY_FUNCTION__.9981(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$210, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rdx, requests(%rip)
	jmp	.L71
.L92:
	movq	32(%rsi), %rax
	movq	%rax, runlist(%rip)
	ret
	.size	__aio_remove_request, .-__aio_remove_request
	.section	.rodata.str1.1
.LC2:
	.string	"runp->running == allocated"
.LC3:
	.string	"runp->running == yes"
	.text
	.p2align 4,,15
	.type	handle_fildes_io, @function
handle_fildes_io:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	__aio_requests_mutex(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$136, %rsp
	leaq	40(%rsp), %r14
	call	pthread_self@PLT
	leaq	44(%rsp), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	pthread_getschedparam@PLT
	leaq	48(%rsp), %rax
	testq	%rbx, %rbx
	movq	%rax, 8(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 16(%rsp)
	je	.L163
	.p2align 4,,10
	.p2align 3
.L95:
	cmpl	$3, (%rbx)
	jne	.L164
	movq	40(%rbx), %r15
	movl	104(%r15), %eax
	cmpl	40(%rsp), %eax
	movl	(%r15), %ebp
	movl	108(%r15), %esi
	jne	.L98
	cmpl	%esi, 44(%rsp)
	je	.L99
.L98:
	movq	%r14, %rdx
	movq	%r13, %rdi
	movl	%eax, 40(%rsp)
	movl	%esi, 44(%rsp)
	call	pthread_setschedparam@PLT
.L99:
	movl	4(%r15), %eax
	movl	%eax, %edx
	andl	$127, %edx
	je	.L102
	cmpl	$1, %edx
	je	.L109
	cmpl	$3, %eax
	je	.L115
	cmpl	$4, %eax
	je	.L118
	movq	errno@gottpoff(%rip), %rax
	movq	$-1, 120(%r15)
	movl	$22, %fs:(%rax)
.L106:
	movq	%r12, %rdi
	call	pthread_mutex_lock@PLT
	cmpq	$-1, 120(%r15)
	je	.L165
.L119:
	movl	$0, 112(%r15)
.L120:
	movq	%rbx, %rdi
	call	__aio_notify
	cmpl	$3, (%rbx)
	jne	.L166
	xorl	%edi, %edi
	xorl	%edx, %edx
	movl	$4, (%rbx)
	movq	%rbx, %rsi
	call	__aio_remove_request
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	add_request_to_runlist
.L122:
	movq	freelist(%rip), %rax
	movl	$0, (%rbx)
	movq	%rbx, freelist(%rip)
	movq	%rax, 24(%rbx)
	movq	runlist(%rip), %rbx
	testq	%rbx, %rbx
	je	.L167
.L123:
	cmpl	$2, (%rbx)
	jne	.L168
	movq	32(%rbx), %rax
	movl	$3, (%rbx)
	testq	%rax, %rax
	movq	%rax, runlist(%rip)
	je	.L129
	movl	idle_thread_count(%rip), %eax
	testl	%eax, %eax
	jg	.L169
	movl	nthreads(%rip), %eax
	cmpl	%eax, optim(%rip)
	jg	.L170
.L129:
	movq	%r12, %rdi
	call	pthread_mutex_unlock@PLT
	testq	%rbx, %rbx
	jne	.L95
.L163:
	movq	%r12, %rdi
	call	pthread_mutex_lock@PLT
	movq	runlist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L123
	.p2align 4,,10
	.p2align 3
.L167:
	movl	24+optim(%rip), %edx
	testl	%edx, %edx
	js	.L127
	movq	8(%rsp), %rsi
	xorl	%edi, %edi
	addl	$1, idle_thread_count(%rip)
	call	__clock_gettime@PLT
	movslq	24+optim(%rip), %rax
	addq	48(%rsp), %rax
	movq	56(%rsp), %rdx
	cmpq	$999999999, %rdx
	movq	%rax, 64(%rsp)
	jg	.L125
	movq	%rdx, 72(%rsp)
.L126:
	movq	16(%rsp), %rdx
	movq	__aio_new_request_notification@GOTPCREL(%rip), %rdi
	movq	%r12, %rsi
	call	pthread_cond_timedwait@PLT
	movq	runlist(%rip), %rbx
	subl	$1, idle_thread_count(%rip)
	testq	%rbx, %rbx
	jne	.L123
.L127:
	leaq	__aio_requests_mutex(%rip), %rdi
	subl	$1, nthreads(%rip)
	call	pthread_mutex_unlock@PLT
	addq	$136, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movq	errno@gottpoff(%rip), %rcx
	movl	%fs:(%rcx), %edx
	cmpl	$4, %edx
	jne	.L171
.L102:
	movq	128(%r15), %rcx
	movq	24(%r15), %rdx
	movl	%ebp, %edi
	movq	16(%r15), %rsi
	call	__libc_pread@PLT
	cmpq	$-1, %rax
	je	.L172
.L108:
	movq	%rax, 120(%r15)
.L176:
	movq	%r12, %rdi
	call	pthread_mutex_lock@PLT
	cmpq	$-1, 120(%r15)
	jne	.L119
.L165:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 112(%r15)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L174:
	movq	errno@gottpoff(%rip), %rcx
	movl	%fs:(%rcx), %edx
	cmpl	$4, %edx
	jne	.L173
.L109:
	movq	128(%r15), %rcx
	movq	24(%r15), %rdx
	movl	%ebp, %edi
	movq	16(%r15), %rsi
	call	__libc_pwrite@PLT
	cmpq	$-1, %rax
	je	.L174
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L175:
	movq	errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L108
.L115:
	movl	%ebp, %edi
	call	fdatasync@PLT
	cltq
	cmpq	$-1, %rax
	je	.L175
	movq	%rax, 120(%r15)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L177:
	movq	errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L108
.L118:
	movl	%ebp, %edi
	call	fsync@PLT
	cltq
	cmpq	$-1, %rax
	je	.L177
	movq	%rax, 120(%r15)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L169:
	movq	__aio_new_request_notification@GOTPCREL(%rip), %rdi
	call	pthread_cond_signal@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L171:
	cmpl	$29, %edx
	movq	%rax, 120(%r15)
	jne	.L106
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L178:
	movq	24(%rsp), %rcx
	cmpl	$4, %fs:(%rcx)
	jne	.L108
.L105:
	movq	24(%r15), %rdx
	movq	16(%r15), %rsi
	movl	%ebp, %edi
	movq	%rcx, 24(%rsp)
	call	read@PLT
	cmpq	$-1, %rax
	je	.L178
	movq	%rax, 120(%r15)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L125:
	subq	$1000000000, %rdx
	addq	$1, %rax
	movq	%rdx, 72(%rsp)
	movq	%rax, 64(%rsp)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L170:
	movq	16(%rsp), %r15
	movq	%r15, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	pthread_attr_setdetachstate@PLT
	movq	8(%rsp), %rdi
	leaq	handle_fildes_io(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	pthread_create@PLT
	testl	%eax, %eax
	jne	.L129
	addl	$1, nthreads(%rip)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L173:
	cmpl	$29, %edx
	movq	%rax, 120(%r15)
	jne	.L106
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L179:
	movq	24(%rsp), %rcx
	cmpl	$4, %fs:(%rcx)
	jne	.L108
.L112:
	movq	24(%r15), %rdx
	movq	16(%r15), %rsi
	movl	%ebp, %edi
	movq	%rcx, 24(%rsp)
	call	write@PLT
	cmpq	$-1, %rax
	je	.L179
	movq	%rax, 120(%r15)
	jmp	.L176
.L164:
	leaq	__PRETTY_FUNCTION__.10021(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$502, %edx
	call	__assert_fail@PLT
.L166:
	leaq	__PRETTY_FUNCTION__.10021(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$599, %edx
	call	__assert_fail@PLT
.L168:
	leaq	__PRETTY_FUNCTION__.10021(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$640, %edx
	call	__assert_fail@PLT
	.size	handle_fildes_io, .-handle_fildes_io
	.p2align 4,,15
	.globl	__aio_init
	.type	__aio_init, @function
__aio_init:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	cmpq	$0, pool(%rip)
	je	.L189
.L181:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L183
	movl	%eax, 24+optim(%rip)
.L183:
	popq	%rbx
	leaq	__aio_requests_mutex(%rip), %rdi
	jmp	pthread_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	movl	(%rbx), %eax
	movl	4(%rbx), %edx
	testl	%eax, %eax
	movl	$1, %eax
	cmovg	(%rbx), %eax
	movl	%eax, optim(%rip)
	movl	%edx, %eax
	andl	$-32, %eax
	cmpl	$31, %edx
	movl	$32, %edx
	cmovle	%edx, %eax
	movl	%eax, 4+optim(%rip)
	jmp	.L181
	.size	__aio_init, .-__aio_init
	.weak	aio_init
	.set	aio_init,__aio_init
	.p2align 4,,15
	.globl	__aio_enqueue_request
	.hidden	__aio_enqueue_request
	.type	__aio_enqueue_request, @function
__aio_enqueue_request:
	pushq	%r15
	leal	-3(%rsi), %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$376, %rsp
	cmpl	$1, %eax
	jbe	.L239
	cmpl	$20, 8(%rdi)
	ja	.L240
.L192:
	movq	%rdi, %r13
	movl	%esi, 4(%rsp)
	call	pthread_self@PLT
	leaq	36(%rsp), %rdx
	leaq	32(%rsp), %rsi
	movq	%rax, %rdi
	call	pthread_getschedparam@PLT
	movl	36(%rsp), %ecx
	subl	8(%r13), %ecx
	leaq	__aio_requests_mutex(%rip), %rdi
	movl	%ecx, %r14d
	call	pthread_mutex_lock@PLT
	movq	requests(%rip), %r8
	testq	%r8, %r8
	je	.L213
	movq	40(%r8), %rax
	movl	0(%r13), %edx
	cmpl	(%rax), %edx
	jle	.L214
	movq	%r8, %r12
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L241:
	movq	40(%rbx), %rax
	cmpl	%edx, (%rax)
	jge	.L194
	movq	%rbx, %r12
.L195:
	movq	16(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L241
.L194:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	je	.L242
.L196:
	movq	24(%rbp), %rax
	movq	%r13, 40(%rbp)
	testq	%rbx, %rbx
	movq	$0, 48(%rbp)
	movl	%r14d, 104(%r13)
	movl	$115, 112(%r13)
	movq	$0, 120(%r13)
	movq	%rax, freelist(%rip)
	movl	32(%rsp), %eax
	movl	%eax, 108(%r13)
	movl	4(%rsp), %eax
	movl	%eax, 4(%r13)
	je	.L202
	movq	40(%rbx), %rax
	movl	0(%r13), %ecx
	cmpl	%ecx, (%rax)
	je	.L204
.L202:
	testq	%r12, %r12
	je	.L243
	movq	16(%r12), %rax
	movq	%r12, 8(%rbp)
	movq	%rax, 16(%rbp)
	movq	%rbp, 16(%r12)
	movq	16(%rbp), %rax
	testq	%rax, %rax
	je	.L208
	movq	%rbp, 8(%rax)
.L208:
	movl	nthreads(%rip), %eax
	cmpl	%eax, optim(%rip)
	movq	$0, 24(%rbp)
	movl	idle_thread_count(%rip), %r8d
	jg	.L244
.L209:
	movq	%rbp, %rdi
	movl	$2, %ebx
	call	add_request_to_runlist
	testl	%r8d, %r8d
	jg	.L245
.L205:
	movl	%ebx, 0(%rbp)
.L212:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L190:
	addq	$376, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	movq	40(%rax), %rdx
	cmpl	%r14d, 104(%rdx)
	jl	.L203
	movq	%rax, %rbx
.L204:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L246
.L203:
	movq	%rax, 24(%rbp)
	movq	%rbp, 24(%rbx)
	movl	$1, %ebx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L243:
	testq	%r8, %r8
	movq	$0, 8(%rbp)
	movq	%r8, 16(%rbp)
	je	.L207
	movq	%rbp, 8(%r8)
.L207:
	movq	%rbp, requests(%rip)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$0, 8(%rdi)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L244:
	testl	%r8d, %r8d
	jne	.L209
	leaq	48(%rsp), %r12
	movl	$3, 0(%rbp)
	leaq	112(%rsp), %rbx
	leaq	240(%rsp), %r15
	movl	$14, %r14d
	movq	%r12, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	pthread_attr_setdetachstate@PLT
	movq	%r12, %rdi
	call	__pthread_get_minstack@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	pthread_attr_setstacksize@PLT
	movq	%rbx, %rdi
	call	sigfillset@PLT
	movl	$8, %r10d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	movl	%r14d, %eax
#APP
# 56 "../sysdeps/unix/sysv/linux/aio_misc.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	handle_fildes_io(%rip), %rdx
	leaq	40(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rcx
	call	pthread_create@PLT
	movl	$8, %r10d
	movl	%eax, %ebx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$2, %edi
	movl	%r14d, %eax
#APP
# 62 "../sysdeps/unix/sysv/linux/aio_misc.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	%r12, %rdi
	call	pthread_attr_destroy@PLT
	testl	%ebx, %ebx
	jne	.L210
	addl	$1, nthreads(%rip)
	movl	$3, %ebx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L242:
	movq	pool_size(%rip), %rdx
	movq	pool_max_size(%rip), %rax
	leaq	1(%rdx), %r9
	cmpq	%rax, %r9
	jnb	.L247
.L197:
	testq	%rdx, %rdx
	je	.L248
	movl	$32, %edi
	movl	$32, %r15d
.L199:
	movl	$56, %esi
	movq	%r9, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	je	.L198
	movq	16(%rsp), %rdx
	movq	24(%rsp), %r9
	movq	%rax, %rbp
	movq	pool(%rip), %rsi
	movq	8(%rsp), %r8
	movq	%r9, pool_size(%rip)
	movq	%rax, (%rsi,%rdx,8)
	xorl	%edx, %edx
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%rax, %rbp
.L200:
	subl	$1, %r15d
	movq	%rdx, 24(%rbp)
	leaq	56(%rbp), %rax
	testl	%r15d, %r15d
	movq	%rbp, %rdx
	jg	.L217
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L245:
	movq	__aio_new_request_notification@GOTPCREL(%rip), %rdi
	call	pthread_cond_signal@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L248:
	movslq	4+optim(%rip), %rdi
	movq	%rdi, %r15
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	8(%rax), %r15
	movq	pool(%rip), %rdi
	movq	%r9, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 8(%rsp)
	leaq	0(,%r15,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L198
	movq	%r15, pool_max_size(%rip)
	movq	%rax, pool(%rip)
	movq	24(%rsp), %r9
	movq	16(%rsp), %rdx
	movq	8(%rsp), %r8
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L213:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r8, %rbx
	xorl	%r12d, %r12d
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L210:
	movl	nthreads(%rip), %eax
	movl	$2, 0(%rbp)
	testl	%eax, %eax
	je	.L211
	movl	idle_thread_count(%rip), %r8d
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%rbp, %rsi
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	__aio_remove_request
	movq	freelist(%rip), %rax
	movl	$0, 0(%rbp)
	movq	%rbp, freelist(%rip)
	movq	%rax, 24(%rbp)
	movq	errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	%ebx, 112(%r13)
	movl	%ebx, %fs:(%rax)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L240:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, 112(%rdi)
	xorl	%ebp, %ebp
	movq	$-1, 120(%rdi)
	movl	$22, %fs:(%rax)
	jmp	.L190
.L198:
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$11, %fs:(%rax)
	jmp	.L190
	.size	__aio_enqueue_request, .-__aio_enqueue_request
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10021, @object
	.size	__PRETTY_FUNCTION__.10021, 17
__PRETTY_FUNCTION__.10021:
	.string	"handle_fildes_io"
	.align 16
	.type	__PRETTY_FUNCTION__.9981, @object
	.size	__PRETTY_FUNCTION__.9981, 21
__PRETTY_FUNCTION__.9981:
	.string	"__aio_remove_request"
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_res__, @object
	.size	__elf_set___libc_subfreeres_element_free_res__, 8
__elf_set___libc_subfreeres_element_free_res__:
	.quad	free_res
	.globl	__aio_new_request_notification
	.bss
	.align 32
	.type	__aio_new_request_notification, @object
	.size	__aio_new_request_notification, 48
__aio_new_request_notification:
	.zero	48
	.hidden	__aio_requests_mutex
	.globl	__aio_requests_mutex
	.data
	.align 32
	.type	__aio_requests_mutex, @object
	.size	__aio_requests_mutex, 40
__aio_requests_mutex:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.align 32
	.type	optim, @object
	.size	optim, 32
optim:
	.long	20
	.long	64
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	0
	.local	idle_thread_count
	.comm	idle_thread_count,4,4
	.local	nthreads
	.comm	nthreads,4,4
	.local	requests
	.comm	requests,8,8
	.local	runlist
	.comm	runlist,8,8
	.local	freelist
	.comm	freelist,8,8
	.local	pool_size
	.comm	pool_size,8,8
	.local	pool_max_size
	.comm	pool_max_size,8,8
	.local	pool
	.comm	pool,8,8
	.hidden	__aio_notify
