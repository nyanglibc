	.text
	.p2align 4,,15
	.globl	aio_error
	.type	aio_error, @function
aio_error:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	movl	112(%rbx), %ebx
	leaq	__aio_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	aio_error, .-aio_error
	.weak	aio_error64
	.set	aio_error64,aio_error
	.hidden	__aio_requests_mutex
