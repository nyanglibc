	.text
	.p2align 4,,15
	.globl	aio_write
	.type	aio_write, @function
aio_write:
	subq	$8, %rsp
	movl	$1, %esi
	call	__aio_enqueue_request@PLT
	testq	%rax, %rax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	aio_write, .-aio_write
	.weak	aio_write64
	.set	aio_write64,aio_write
