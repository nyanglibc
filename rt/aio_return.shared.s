	.text
	.p2align 4,,15
	.globl	aio_return
	.type	aio_return, @function
aio_return:
	movq	120(%rdi), %rax
	ret
	.size	aio_return, .-aio_return
	.weak	aio_return64
	.set	aio_return64,aio_return
