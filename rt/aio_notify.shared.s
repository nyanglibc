	.text
	.p2align 4,,15
	.type	notify_func_wrapper, @function
notify_func_wrapper:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	call	sigemptyset@PLT
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/aio_misc.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	8(%rbx), %r12
	movq	%rbx, %rdi
	movq	(%rbx), %rbp
	call	free@PLT
	movq	%r12, %rdi
	call	*%rbp
	subq	$-128, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	notify_func_wrapper, .-notify_func_wrapper
	.p2align 4,,15
	.globl	__aio_notify_only
	.hidden	__aio_notify_only
	.type	__aio_notify_only, @function
__aio_notify_only:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$80, %rsp
	movl	12(%rdi), %edx
	cmpl	$2, %edx
	je	.L14
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L4
	call	getpid@PLT
	movl	8(%rbx), %edi
	movq	(%rbx), %rsi
	movl	%eax, %edx
	call	__aio_sigqueue
	sarl	$31, %eax
.L4:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L15
.L6:
	movl	$16, %edi
	call	malloc@PLT
	movq	%rax, %rbp
	movl	$-1, %eax
	testq	%rbp, %rbp
	je	.L4
	movq	16(%rbx), %rax
	leaq	notify_func_wrapper(%rip), %rdx
	leaq	8(%rsp), %rdi
	movq	%rbp, %rcx
	movq	%r12, %rsi
	movq	%rax, 0(%rbp)
	movq	(%rbx), %rax
	movq	%rax, 8(%rbp)
	call	pthread_create@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jns	.L4
	movq	%rbp, %rdi
	call	free@PLT
	movl	$-1, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	16(%rsp), %r12
	movq	%r12, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	pthread_attr_setdetachstate@PLT
	jmp	.L6
	.size	__aio_notify_only, .-__aio_notify_only
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__aio_notify
	.hidden	__aio_notify
	.type	__aio_notify, @function
__aio_notify:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rbp
	leaq	32(%rbp), %rdi
	call	__aio_notify_only
	testl	%eax, %eax
	je	.L17
	movq	errno@gottpoff(%rip), %rax
	movq	$-1, 120(%rbp)
	movl	%fs:(%rax), %eax
	movl	%eax, 112(%rbp)
.L17:
	movq	48(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L16
	movl	$202, %r12d
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L19:
	subl	$1, (%rcx)
	je	.L37
.L22:
	testq	%r13, %r13
	movq	%r13, %rbx
	je	.L16
.L25:
	movq	24(%rbx), %rdi
	movq	(%rbx), %r13
	movq	16(%rbx), %rcx
	testq	%rdi, %rdi
	jne	.L19
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L20
	cmpq	$-1, 120(%rbp)
	jne	.L20
	movl	$-1, (%rax)
.L20:
	movl	(%rcx), %eax
	testl	%eax, %eax
	je	.L22
	subl	$1, (%rcx)
	jne	.L22
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	%r12d, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L22
	cmpl	$-22, %eax
	je	.L22
	cmpl	$-14, %eax
	je	.L22
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	call	__aio_notify_only
	movq	16(%rbx), %rdi
	movq	%r13, %rbx
	call	free@PLT
	testq	%r13, %r13
	jne	.L25
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__aio_notify, .-__aio_notify
	.hidden	__aio_sigqueue
