	.text
#APP
	.symver __timer_create_new,timer_create@@GLIBC_2.3.3
	.symver __timer_create_old,timer_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__timer_create_new
	.type	__timer_create_new, @function
__timer_create_new:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$-6, %ebx
	subq	$96, %rsp
	cmpl	$2, %edi
	je	.L2
	movl	%edi, %ebx
	cmpl	$3, %edi
	movl	$-2, %eax
	cmove	%eax, %ebx
.L2:
	testq	%rbp, %rbp
	je	.L3
	cmpl	$2, 12(%rbp)
	je	.L22
.L4:
	leaq	28(%rsp), %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$222, %eax
#APP
# 68 "../sysdeps/unix/sysv/linux/timer_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L23
	movslq	28(%rsp), %rax
	movq	%rax, (%r12)
	xorl	%eax, %eax
.L1:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$0, 32(%rsp)
	movq	$14, 40(%rsp)
	leaq	32(%rsp), %rbp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L23:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	addq	$96, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	__start_helper_thread(%rip), %rsi
	leaq	__helper_once(%rip), %rdi
	call	pthread_once@PLT
	movl	__helper_tid(%rip), %eax
	testl	%eax, %eax
	je	.L24
	movl	$88, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L9
	movq	0(%rbp), %rax
	leaq	24(%r13), %r14
	movq	%r14, %rdi
	movq	%rax, 16(%r13)
	movq	16(%rbp), %rax
	movq	%rax, 8(%r13)
	call	pthread_attr_init@PLT
	movq	24(%rbp), %rax
	testq	%rax, %rax
	je	.L10
	movl	(%rax), %edx
	movl	%edx, 24(%r13)
	movl	4(%rax), %edx
	movl	%edx, 28(%r13)
	movl	8(%rax), %edx
	movl	%edx, 32(%r13)
	movq	16(%rax), %rdx
	movq	%rdx, 40(%r13)
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	movq	%rdx, 48(%r13)
	movq	%rax, 56(%r13)
.L10:
	movq	%r14, %rdi
	movl	$1, %esi
	call	pthread_attr_setdetachstate@PLT
	movabsq	$17179869216, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 40(%rsp)
	movl	__helper_tid(%rip), %eax
	leaq	32(%rsp), %rsi
	movq	$0, 84(%rsp)
	movl	$0, 92(%rsp)
	movq	%r13, %rdx
	movups	%xmm0, 52(%rsp)
	movl	%eax, 48(%rsp)
	movq	%r13, 32(%rsp)
	movl	%ebx, %edi
	movups	%xmm0, 68(%rsp)
	movl	$222, %eax
#APP
# 125 "../sysdeps/unix/sysv/linux/timer_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L25
	leaq	__active_timer_sigev_thread_lock(%rip), %rdi
	call	pthread_mutex_lock@PLT
	movq	__active_timer_sigev_thread(%rip), %rax
	leaq	__active_timer_sigev_thread_lock(%rip), %rdi
	movq	%r13, __active_timer_sigev_thread(%rip)
	movq	%rax, 80(%r13)
	shrq	%r13
	call	pthread_mutex_unlock@PLT
	movabsq	$-9223372036854775808, %rax
	orq	%rax, %r13
	xorl	%eax, %eax
	movq	%r13, (%r12)
	jmp	.L1
.L24:
	movq	errno@gottpoff(%rip), %rax
	movl	$11, %fs:(%rax)
.L9:
	movl	$-1, %eax
	jmp	.L1
.L25:
	movq	%r13, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	jmp	.L9
	.size	__timer_create_new, .-__timer_create_new
	.p2align 4,,15
	.globl	__timer_create_old
	.type	__timer_create_old, @function
__timer_create_old:
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$16, %rsp
	leaq	8(%rsp), %rdx
	call	__timer_create_new@PLT
	testl	%eax, %eax
	movl	%eax, %r8d
	jne	.L26
	movq	8(%rsp), %rdi
	xorl	%edx, %edx
	leaq	__compat_timer_list(%rip), %rcx
	xorl	%r9d, %r9d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$1, %rdx
	cmpq	$256, %rdx
	je	.L34
.L30:
	cmpq	$0, (%rcx,%rdx,8)
	leaq	0(,%rdx,8), %rsi
	jne	.L28
	movq	%r9, %rax
	lock cmpxchgq	%rdi, (%rcx,%rsi)
	jne	.L35
	movl	%edx, (%rbx)
.L26:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	call	__timer_delete_new@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	jmp	.L26
.L35:
	movq	8(%rsp), %rdi
	jmp	.L28
	.size	__timer_create_old, .-__timer_create_old
	.hidden	__compat_timer_list
	.comm	__compat_timer_list,2048,32
	.hidden	__active_timer_sigev_thread
	.hidden	__active_timer_sigev_thread_lock
	.hidden	__helper_tid
	.hidden	__helper_once
	.hidden	__start_helper_thread
