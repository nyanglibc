	.text
	.p2align 4,,15
	.globl	__mq_timedreceive
	.type	__mq_timedreceive, @function
__mq_timedreceive:
	movq	%rcx, %r10
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedreceive.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$243, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedreceive.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L3
	cltq
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r13
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__librt_enable_asynccancel@PLT
	movq	%r14, %r8
	movl	%eax, %r9d
	movq	%r13, %r10
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$243, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedreceive.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	cltq
.L6:
	movl	%r9d, %edi
	movq	%rax, 8(%rsp)
	call	__librt_disable_asynccancel@PLT
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L5:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L6
	.size	__mq_timedreceive, .-__mq_timedreceive
	.weak	mq_timedreceive
	.set	mq_timedreceive,__mq_timedreceive
