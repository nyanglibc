	.text
#APP
	.symver __timer_delete_new,timer_delete@@GLIBC_2.3.3
	.symver __timer_delete_old,timer_delete@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__timer_delete_new
	.type	__timer_delete_new, @function
__timer_delete_new:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rdx
	subq	$8, %rsp
	testq	%rdi, %rdi
	jns	.L3
	leaq	(%rdi,%rdi), %rax
	movl	(%rax), %edi
.L3:
	movl	$226, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/timer_delete.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L9
	testq	%rdx, %rdx
	js	.L15
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	__active_timer_sigev_thread_lock(%rip), %rdi
	leaq	(%rdx,%rdx), %rbx
	call	pthread_mutex_lock@PLT
	movq	__active_timer_sigev_thread(%rip), %rdx
	cmpq	%rdx, %rbx
	jne	.L6
	movq	80(%rbx), %rax
	movq	%rax, __active_timer_sigev_thread(%rip)
.L7:
	leaq	__active_timer_sigev_thread_lock(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movq	%rbx, %rdi
	call	free@PLT
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpq	%rax, %rbx
	je	.L16
	movq	%rax, %rdx
.L6:
	movq	80(%rdx), %rax
	testq	%rax, %rax
	jne	.L8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %ebp
	movl	%eax, %fs:(%rdx)
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	80(%rbx), %rax
	movq	%rax, 80(%rdx)
	jmp	.L7
.L9:
	orl	$-1, %ebp
	jmp	.L1
	.size	__timer_delete_new, .-__timer_delete_new
	.p2align 4,,15
	.globl	__timer_delete_old
	.type	__timer_delete_old, @function
__timer_delete_old:
	pushq	%rbp
	leaq	__compat_timer_list(%rip), %rbp
	pushq	%rbx
	movslq	%edi, %rbx
	subq	$8, %rsp
	movq	0(%rbp,%rbx,8), %rdi
	call	__timer_delete_new@PLT
	testl	%eax, %eax
	jne	.L17
	movq	$0, 0(%rbp,%rbx,8)
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__timer_delete_old, .-__timer_delete_old
	.hidden	__compat_timer_list
	.hidden	__active_timer_sigev_thread
	.hidden	__active_timer_sigev_thread_lock
