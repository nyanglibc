	.text
	.p2align 4,,15
	.globl	mq_getattr
	.type	mq_getattr, @function
mq_getattr:
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	__GI_mq_setattr
	.size	mq_getattr, .-mq_getattr
