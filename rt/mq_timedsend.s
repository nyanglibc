	.text
	.p2align 4,,15
	.globl	__mq_timedsend
	.type	__mq_timedsend, @function
__mq_timedsend:
	movl	%ecx, %r10d
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedsend.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$242, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedsend.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r13d
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__librt_enable_asynccancel@PLT
	movq	%r14, %r8
	movl	%eax, %r9d
	movl	%r13d, %r10d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$242, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/mq_timedsend.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r9d, %edi
	movl	%eax, 12(%rsp)
	call	__librt_disable_asynccancel@PLT
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__mq_timedsend, .-__mq_timedsend
	.weak	mq_timedsend
	.set	mq_timedsend,__mq_timedsend
