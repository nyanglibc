	.text
	.p2align 4,,15
	.globl	__mq_open
	.type	__mq_open, @function
__mq_open:
	movq	%rdx, -32(%rsp)
	movq	%rcx, -24(%rsp)
	cmpb	$47, (%rdi)
	jne	.L14
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	testb	$64, %sil
	jne	.L15
.L4:
	addq	$1, %rdi
	movl	$240, %eax
#APP
# 50 "../sysdeps/unix/sysv/linux/mq_open.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L16
	rep ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	8(%rsp), %rax
	movl	$24, -72(%rsp)
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movl	16(%rax), %edx
	movq	24(%rax), %r10
	movq	%rax, -56(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L16:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__mq_open, .-__mq_open
	.globl	mq_open
	.set	mq_open,__mq_open
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"invalid mq_open call: O_CREAT without mode and attr"
	.text
	.p2align 4,,15
	.globl	__mq_open_2
	.type	__mq_open_2, @function
__mq_open_2:
	testb	$64, %sil
	jne	.L22
	xorl	%eax, %eax
	jmp	__mq_open
.L22:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__fortify_fail@PLT
	.size	__mq_open_2, .-__mq_open_2
