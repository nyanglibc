	.text
	.p2align 4,,15
	.globl	__aio_sigqueue
	.hidden	__aio_sigqueue
	.type	__aio_sigqueue, @function
__aio_sigqueue:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movl	%edi, %ebx
	movl	$15, %ecx
	movq	%rsi, %r12
	addq	$-128, %rsp
	movq	%rsp, %rbp
	movq	$0, 4(%rsp)
	movq	$0, 120(%rsp)
	leaq	8(%rbp), %rdi
	rep stosq
	movl	%edx, 16(%rsp)
	movl	%ebx, (%rsp)
	movl	$-4, 8(%rsp)
	call	getuid@PLT
	movq	%r12, 24(%rsp)
	movl	%eax, 20(%rsp)
	movq	%rbp, %rdx
	movl	%ebx, %esi
	movl	16(%rsp), %edi
	movl	$129, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/aio_sigqueue.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L6
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	subq	$-128, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__aio_sigqueue, .-__aio_sigqueue
