.text
.globl mq_setattr
.type mq_setattr,@function
.align 1<<4
mq_setattr:
	movl $245, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size mq_setattr,.-mq_setattr
