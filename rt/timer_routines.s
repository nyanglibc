	.text
	.p2align 4,,15
	.type	reset_helper_control, @function
reset_helper_control:
	movl	$0, __helper_once(%rip)
	movl	$0, __helper_tid(%rip)
	ret
	.size	reset_helper_control, .-reset_helper_control
	.p2align 4,,15
	.type	timer_helper_thread, @function
timer_helper_thread:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	timer_sigev_thread(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	leaq	__active_timer_sigev_thread_lock(%rip), %r12
	leaq	sigtimer_set(%rip), %rbx
	subq	$152, %rsp
	leaq	16(%rsp), %rbp
	leaq	8(%rsp), %r14
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	sigwaitinfo@PLT
	testl	%eax, %eax
	js	.L4
	movl	24(%rsp), %eax
	cmpl	$-2, %eax
	je	.L17
	cmpl	$-6, %eax
	jne	.L4
	xorl	%edi, %edi
	call	pthread_exit@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rdi
	movq	40(%rsp), %r15
	call	pthread_mutex_lock@PLT
	movq	__active_timer_sigev_thread(%rip), %rax
	testq	%rax, %rax
	jne	.L10
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L7
.L10:
	cmpq	%rax, %r15
	jne	.L18
	movl	$16, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L7
	movq	8(%r15), %rdx
	leaq	24(%r15), %rsi
	movq	%rax, %rcx
	movq	%r14, %rdi
	movq	%rdx, (%rax)
	movq	16(%r15), %rdx
	movq	%rdx, 8(%rax)
	movq	%r13, %rdx
	call	pthread_create@PLT
.L7:
	movq	%r12, %rdi
	call	pthread_mutex_unlock@PLT
	jmp	.L4
	.size	timer_helper_thread, .-timer_helper_thread
	.p2align 4,,15
	.type	timer_sigev_thread, @function
timer_sigev_thread:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r8
	movl	$8, %r10d
	xorl	%edx, %edx
	leaq	sigtimer_set(%rip), %rsi
	subq	$8, %rsp
	movl	$1, %edi
	movl	$14, %eax
#APP
# 97 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	8(%r8), %rbp
	movq	%r8, %rdi
	movq	(%r8), %rbx
	call	free@PLT
	movq	%rbp, %rdi
	call	*%rbx
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	timer_sigev_thread, .-timer_sigev_thread
	.p2align 4,,15
	.globl	__start_helper_thread
	.type	__start_helper_thread, @function
__start_helper_thread:
	pushq	%rbx
	subq	$208, %rsp
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	call	pthread_attr_init@PLT
	movq	%rbx, %rdi
	call	__pthread_get_minstack@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	pthread_attr_setstacksize@PLT
	leaq	80(%rsp), %rsi
	movabsq	$-4294967297, %rax
	movq	%rbx, %rdi
	movq	%rax, 80(%rsp)
	call	__pthread_attr_setsigmask_internal@PLT
	testl	%eax, %eax
	jne	.L26
	leaq	8(%rsp), %rdi
	leaq	timer_helper_thread(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	call	pthread_create@PLT
	testl	%eax, %eax
	je	.L27
.L24:
	movq	%rbx, %rdi
	call	pthread_attr_destroy@PLT
	leaq	reset_helper_control(%rip), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	pthread_atfork@PLT
	addq	$208, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	8(%rsp), %rax
	movl	720(%rax), %eax
	movl	%eax, __helper_tid(%rip)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rbx, %rdi
	call	pthread_attr_destroy@PLT
	addq	$208, %rsp
	popq	%rbx
	ret
	.size	__start_helper_thread, .-__start_helper_thread
	.comm	__helper_tid,4,4
	.comm	__helper_once,4,4
	.globl	__active_timer_sigev_thread_lock
	.bss
	.align 32
	.type	__active_timer_sigev_thread_lock, @object
	.size	__active_timer_sigev_thread_lock, 40
__active_timer_sigev_thread_lock:
	.zero	40
	.comm	__active_timer_sigev_thread,8,8
	.section	.rodata
	.align 32
	.type	sigtimer_set, @object
	.size	sigtimer_set, 128
sigtimer_set:
	.quad	2147483648
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
