	.text
	.p2align 4,,15
	.globl	mq_unlink
	.type	mq_unlink, @function
mq_unlink:
	cmpb	$47, (%rdi)
	jne	.L7
	movl	$241, %ecx
	addq	$1, %rdi
	movl	%ecx, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/mq_unlink.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	movq	%rax, %rdx
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	negl	%eax
	cmpl	$-1, %edx
	movl	$13, %edx
	cmove	%edx, %eax
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	mq_unlink, .-mq_unlink
