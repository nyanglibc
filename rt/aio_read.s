	.text
	.p2align 4,,15
	.globl	aio_read
	.type	aio_read, @function
aio_read:
	subq	$8, %rsp
	xorl	%esi, %esi
	call	__aio_enqueue_request@PLT
	testq	%rax, %rax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	aio_read, .-aio_read
	.weak	aio_read64
	.set	aio_read64,aio_read
