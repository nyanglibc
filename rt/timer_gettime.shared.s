	.text
#APP
	.symver __timer_gettime_new,timer_gettime@@GLIBC_2.3.3
	.symver __timer_gettime_old,timer_gettime@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__timer_gettime_new
	.type	__timer_gettime_new, @function
__timer_gettime_new:
	testq	%rdi, %rdi
	movq	%rdi, %rax
	jns	.L3
	addq	%rax, %rax
	movl	(%rax), %edi
.L3:
	movl	$224, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/x86_64/timer_gettime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__timer_gettime_new, .-__timer_gettime_new
	.p2align 4,,15
	.globl	__timer_gettime_old
	.type	__timer_gettime_old, @function
__timer_gettime_old:
	leaq	__compat_timer_list(%rip), %rax
	movslq	%edi, %rdi
	movq	(%rax,%rdi,8), %rdi
	jmp	__timer_gettime_new@PLT
	.size	__timer_gettime_old, .-__timer_gettime_old
	.hidden	__compat_timer_list
