	.text
	.p2align 4,,15
	.globl	mq_close
	.type	mq_close, @function
mq_close:
	movl	$3, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/mq_close.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	mq_close, .-mq_close
